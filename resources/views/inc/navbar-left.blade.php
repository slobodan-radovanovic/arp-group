<!-- Main sidebar -->
<div class="sidebar sidebar-dark sidebar-main sidebar-expand-md">

    <!-- Sidebar mobile toggler -->
    <div class="sidebar-mobile-toggler text-center">
        <a href="#" class="sidebar-mobile-main-toggle">
            <i class="icon-arrow-left8"></i>
        </a>
        Navigation
        <a href="#" class="sidebar-mobile-expand">
            <i class="icon-screen-full"></i>
            <i class="icon-screen-normal"></i>
        </a>
    </div>
    <!-- /sidebar mobile toggler -->


    <!-- Sidebar content -->
    <div class="sidebar-content">

        <!-- Main navigation -->
        <div class="card card-sidebar-mobile">
            <ul class="nav nav-sidebar" data-nav-type="accordion">

                <!-- Main

                -->

                <li class="nav-item">
                    <a href="/" class="nav-link">
                        <i class="icon-home4"></i>
                        <span>
									Pocetna
                            <!--<span class="d-block font-weight-normal opacity-50">No active orders</span>-->
								</span>
                    </a>
                </li>


                <li class="nav-item nav-item-submenu" id="orders">
                    <a href="#" class="nav-link"><i class="icon-copy"></i> <span>SBB nalog</span></a>

                    <ul class="nav nav-group-sub" data-submenu-title="Starter kit">
                        <li class="nav-item"><a href="/orders/create" class="nav-link">Nov nalog</a></li>
                        {{--<li class="nav-item"><a href="/orders/create_with_status" class="nav-link">Nov nalog (sa statusima)</a></li>--}}
                        <li class="nav-item"><a href="/all_orders" class="nav-link">Pregled svih naloga</a></li>
                        <li class="nav-item"><a href="/orders" class="nav-link">Pregled naloga po statusu</a></li>
                        <li class="nav-item"><a href="/group_search" class="nav-link">Grupna pretraga</a></li>
                    </ul>
                </li>
                {{--DIGITALIZACIJA--}}
                <li class="nav-item nav-item-submenu" id="digitization">
                    <a href="#" class="nav-link"><i class="icon-copy"></i> <span>Digitalizacija</span></a>

                    <ul class="nav nav-group-sub" data-submenu-title="Starter kit">

                        <li class="nav-item"><a href="/digitization/create" class="nav-link">Nov nalog</a></li>
                        <li class="nav-item"><a href="/digitization" class="nav-link">Pregled naloga</a></li>

                    </ul>
                </li>
                {{--REKLAMACIJE--}}
                <li class="nav-item nav-item-submenu" id="reclamation">
                    <a href="#" class="nav-link"><i class="icon-copy"></i> <span>Reklamacije</span></a>

                    <ul class="nav nav-group-sub" data-submenu-title="Starter kit">

                        <li class="nav-item"><a href="/reclamation/create" class="nav-link">Nov nalog</a></li>
                        <li class="nav-item"><a href="/reclamation" class="nav-link">Pregled naloga</a></li>

                    </ul>
                </li>

                {{--TEHNIČARI--}}
                <li class="nav-item nav-item-submenu" id="technicians">
                    <a href="#" class="nav-link"><i class="icon-users"></i>
                        <span>Tehničari</span></a>

                    <ul class="nav nav-group-sub" data-submenu-title="Starter kit">

                        <li class="nav-item"><a href="/technicians" class="nav-link">Pregled tehničara</a></li>

                        <li class="nav-item"><a href="/technicians/assigned" class="nav-link">Pregled zaduživanja</a>
                        </li>
                        <li class="nav-item"><a href="/technicians/deleted" class="nav-link">Pregled izbrisanih
                                tehničara</a></li>

                    </ul>
                </li>

                {{--ZAKAZIVANJE--}}

                {{--<li class="nav-item">
                    <a href="/scheduling" class="nav-link"><i class="icon-color-sampler"></i> <span>Zakazivanje</span></a>


                </li>--}}

                {{--OPREMA--}}


                <li class="nav-item nav-item-submenu" id="equipment">
                    <a href="#" class="nav-link"><i class="icon-lan"></i> <span>Oprema</span></a>

                    <ul class="nav nav-group-sub" data-submenu-title="Oprema">
                        <li class="nav-item"><a href="/equipment" class="nav-link">Pregled opreme</a></li>


                            <li class="nav-item"><a href="/equipment/create" class="nav-link">Dodavanje opreme</a></li>
                            <li class="nav-item"><a href="/equipment/dispatch_note" class="nav-link">Oprema po broju otpremnice</a></li>

                        <li class="nav-item"><a href="/equipment/assignment" class="nav-link">Zaduživanje opreme</a></li>
                        <li class="nav-item"><a href="/equipment/assigned" class="nav-link">Zadužena oprema</a></li>
                        <li class="nav-item"><a href="/equipment/discharge" class="nav-link">Razaduživanje opreme</a></li>
                        <li class="nav-item"><a href="/equipment/discharged_equipment" class="nav-link">Razdužena opreme</a></li>

                      {{--  @dd(Auth::user())--}}
                        @if (Auth::user()->role == 'Kordinator')
                            <li class="nav-item"><a href="/equipment/add" class="nav-link">Dodavanje tipa i modela</a></li>
                            <li class="nav-item"><a href="/equipment/edit" class="nav-link">Izmena tipova i modela</a></li>
                        @endif




                        <li class="nav-item"><a href="/equipment/installed_equipment" class="nav-link">Ugradjena
                                oprema</a></li>
                        <li class="nav-item"><a href="/equipment/dismantled" class="nav-link">Demontirana oprema</a>
                        </li>
                        <li class="nav-item"><a href="/equipment/sbb_warehouse" class="nav-link">Oprema vraćena u SBB magacin</a>
                        </li>
                        <li class="nav-item"><a href="/equipment/deleted_equipment" class="nav-link">Izbrisana
                                oprema</a></li>

                    </ul>

                </li>


                {{--MATERIJAL--}}

                <li class="nav-item nav-item-submenu" id="wares">
                    <a href="#" class="nav-link"><i class="icon-wrench"></i> <span>Materijal</span></a>

                    <ul class="nav nav-group-sub" data-submenu-title="Starter kit">
                        <li class="nav-item"><a href="/wares" class="nav-link">Pregled vrsta materijala</a></li>
                        {{--<li class="nav-item"><a href="/wares/create" class="nav-link">Dodavanje nove vrste materijala</a></li>--}}

                    </ul>

                </li>

                {{--MAGACIN--}}


                <li class="nav-item nav-item-submenu" id="warehouse">
                    <a href="#" class="nav-link"><i class="icon-stack2"></i> <span>Magacin</span></a>

                    <ul class="nav nav-group-sub" data-submenu-title="Starter kit">
                        <li class="nav-item"><a href="/warehouse" class="nav-link">Magacin u firmi</a></li>
                        <li class="nav-item"><a href="/warehouse/external" class="nav-link">Spoljni magacin</a></li>
                        <li class="nav-item"><a href="/warehouse/add" class="nav-link">Unos materijala u magacin</a>
                        </li>
                        <li class="nav-item"><a href="/warehouse/dispatch_note" class="nav-link">Materijal po broju otpremnice</a></li>
                        <li class="nav-item"><a href="/warehouse/assignment" class="nav-link">Zaduživanje materijala</a>
                        </li>

                    </ul>

                </li>

                @if (Auth::user()->role == 'Kordinator')


                <li class="nav-item nav-item-submenu" id="report">
                    <a href="#" class="nav-link"><i class="icon-stack"></i> <span>Izveštaji</span></a>

                    <ul class="nav nav-group-sub" data-submenu-title="Starter kit">
                        {{-- <li class="nav-item"><a href="#" class="nav-link">Finansijski izveštaj</a></li>--}}
                        <li class="nav-item"><a href="/techniciansreport" class="nav-link">Izveštaj za tehničara</a>
                        <li class="nav-item"><a href="/techniciansreport_all" class="nav-link">Izveštaj za sve tehničare</a>
                        <li class="nav-item"><a href="/sbbreport_completed" class="nav-link">Izveštaj za SBB - Završeni</a>
                        <li class="nav-item"><a href="/sbbreport_canceled" class="nav-link">Izveštaj za SBB - Otkazani</a>
                        <li class="nav-item"><a href="/sbbreport_in_progress" class="nav-link">Izveštaj za SBB - U radu</a>
                        <li class="nav-item"><a href="/financereport" class="nav-link">Finansijski izveštaj</a>
                        <li class="nav-item"><a href="/waresreport" class="nav-link">Izveštaj za utrošeni materijal</a>
                        </li>
                    </ul>
                </li>

                <li class="nav-item nav-item-submenu" id="service">
                    <a href="#" class="nav-link"><i class="icon-wrench3"></i> <span>Tipovi posla</span></a>

                    <ul class="nav nav-group-sub" data-submenu-title="Starter kit">
                        <li class="nav-item"><a href="/service" class="nav-link">Pregled i dovadanje vrste posla</a>
                        </li>
                    </ul>
                </li>

                <li class="nav-item nav-item-submenu" id="add_select">
                        <a href="#" class="nav-link"><i class="icon-wrench3"></i> <span>Gradovi, opštine i regioni</span></a>

                        <ul class="nav nav-group-sub" data-submenu-title="Starter kit">
                            <li class="nav-item"><a href="/townships" class="nav-link">Dodavanje gradova, opština i regiona</a>
                            </li>
                            <li class="nav-item"><a href="/active_townships" class="nav-link">Aktivni gradovi, opštine i regioni</a>
                            </li>
                        </ul>
                </li>


                <li class="nav-item nav-item-submenu" id="users">
                    <a href="#" class="nav-link"><i class="icon-users"></i>
                        <span>Korisnički nalozi</span></a>

                    <ul class="nav nav-group-sub" data-submenu-title="Starter kit">

                        {{--<li class="nav-item"><a href="/register" class="nav-link">Otvaranje korisničkog naloga</a></li>--}}

                        <li class="nav-item"><a href="/users" class="nav-link">Pregled korisničkih naloga</a>
                        </li>
                       {{-- <li class="nav-item"><a href="/users/deleted" class="nav-link">Izbrisani korisnikči naloyi</a></li>--}}

                    </ul>
                </li>

            @endif
                <!-- /main -->

                <!-- Layout -->

                <!-- /layout -->

            </ul>
        </div>
        <!-- /main navigation -->

    </div>
    <!-- /sidebar content -->

</div>
<!-- /main sidebar -->

