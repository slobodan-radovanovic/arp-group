
<script src="{{ asset('js/app.js') }}" defer></script>

<script type="text/javascript" src="{{ asset('assets/js/app.js') }}"></script>
<script type="text/javascript" src="{{ asset('global_assets/js/main/jquery.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('global_assets/js/main/bootstrap.bundle.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('global_assets/js/main/preloader.js') }}"></script>
<script type="text/javascript" src="{{ asset('global_assets/js/plugins/loaders/blockui.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('global_assets/js/main/activelink.js') }}"></script>
<script type="text/javascript" src="{{ asset('global_assets/js/plugins/notifications/pnotify.min.js') }}"></script>

<script>
            @if (\Session::has('success'))
    var poruka = '{!! \Session::get('success') !!}';
    $(function(){
        /*new Noty({
        text: poruka,
        type: 'success'
        }).show();*/
        new PNotify({
            title: '',
            text: poruka,
            icon: 'icon-checkmark3',
            addclass: 'bg-success border-success',
            delay: 4000,
            width: '400px'
        });
    });
            @elseif(\Session::has('delete'))
    var poruka = '{!! \Session::get('delete') !!}';
    $(function(){
        new PNotify({
            title: '',
            text: poruka,
            icon: 'icon-trash',
            addclass: 'bg-danger border-danger',
            delay: 4000,
            width: '400px'
        });
    });

            @elseif(\Session::has('error'))
    var poruka = '{!! \Session::get('error') !!}';
    $(function(){
        new PNotify({
            title: '',
            text: poruka,
            icon: 'icon-close2',
            addclass: 'bg-danger border-danger',
            delay: 4000,
            width: '400px'
        });
    });

    @endif

</script>

<!-- /theme JS files -->