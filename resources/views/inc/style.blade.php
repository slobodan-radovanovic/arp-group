<link href="{{ asset('css/app.css') }}" rel="stylesheet">
<!-- Global stylesheets -->
<link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
<link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet" type="text/css">
{{--<link href="../global_assets/css/icons/icomoon/styles.css" rel="stylesheet" type="text/css">
<link href="../assets/css/bootstrap.min.css" rel="stylesheet" type="text/css">
<link href="../assets/css/bootstrap_limitless.min.css" rel="stylesheet" type="text/css">
<link href="../assets/css/layout.min.css" rel="stylesheet" type="text/css">
<link href="../assets/css/components.min.css" rel="stylesheet" type="text/css">
<link href="../assets/css/colors.min.css" rel="stylesheet" type="text/css">
<link href="../assets/css/preloader.css" rel="stylesheet" type="text/css">--}}

<link rel="stylesheet" href="{{ asset('/global_assets/css/icons/icomoon/styles.css') }}"/>
<link rel="stylesheet" href="{{ asset('/assets/css/bootstrap.min.css') }}"/>
<link rel="stylesheet" href="{{ asset('/assets/css/bootstrap_limitless.min.css') }}"/>
<link rel="stylesheet" href="{{ asset('/assets/css/layout.min.css') }}"/>
<link rel="stylesheet" href="{{ asset('/assets/css/components.min.css') }}"/>
<link rel="stylesheet" href="{{ asset('/assets/css/colors.min.css') }}"/>
<link rel="stylesheet" href="{{ asset('/assets/css/preloader.css') }}"/>

<style>
    input:focus {
        background-color: lightgrey;
    }

    /*
    http://arp-group.local/assets/css/components.min.css
    .select2-selection--single:not([class*=bg-]){background-color:#fff}
    disabled;
    */
</style>



<!-- /global stylesheets -->