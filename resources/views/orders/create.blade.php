@extends('layouts.app')

@push ('style')
  <link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
  <link href="../global_assets/css/icons/icomoon/styles.css" rel="stylesheet" type="text/css">
  <link href="../assets/css/bootstrap.min.css" rel="stylesheet" type="text/css">
  <link href="../assets/css/bootstrap_limitless.min.css" rel="stylesheet" type="text/css">
  <link href="../assets/css/layout.min.css" rel="stylesheet" type="text/css">
  <link href="../assets/css/components.min.css" rel="stylesheet" type="text/css">
  <link href="../assets/css/colors.min.css" rel="stylesheet" type="text/css">

  <style>
    input.parsley-success,
    select.parsley-success,
    textarea.parsley-success {
     /* color: #468847;*/
      background-color: #DFF0D8;
      border: 1px solid #D6E9C6;
    }

    input.parsley-error,
    select.parsley-error,
    textarea.parsley-error {
     /* color: #B94A48;*/
      background-color: #F2DEDE;
      border: 1px solid #EED3D7;
    }

    .parsley-errors-list {
      margin: 2px 0 3px;
      padding: 0;
      list-style-type: none;
      font-size: 0.9em;
      line-height: 0.9em;
      opacity: 0;
      color: #B94A48;

      transition: all .3s ease-in;
      -o-transition: all .3s ease-in;
      -moz-transition: all .3s ease-in;
      -webkit-transition: all .3s ease-in;
    }

    .parsley-errors-list.filled {
      opacity: 1;
    }


  </style>


@endpush

@push('scripts')
  <!-- scripts push-->
  <script src="../global_assets/js/plugins/ui/moment/moment.min.js"></script>
  <script src="../global_assets/js/plugins/pickers/anytime.min.js"></script>

  <script src="../assets/js/custom_datepicker.js"></script>

  <!-- Load select2 -->
  <script type="text/javascript" src="../global_assets/js/plugins/forms/selects/select2.min.js"></script>
  <script type="text/javascript" src="../global_assets/js/plugins/extensions/jquery_ui/interactions.min.js"></script>
  <script src="../assets/js/custom_select2.js"></script>

  <script>

      // iskljeceno menanje vrednost u number poljima pomocu skrola
      $(document).on("wheel", "input[type=number]", function (e) {
          $(this).blur();
      });
  </script>


  <script>

      var start_status = $("#order_status option:selected").val();
      var start_scheduled_datetime = $("#scheduled_at_datetime").val();
      var start_uncomplete = $("#uncomplete").val();
      var start_order_comment = $("#order_comment").val();


      function onStatusChange() {
          // hide all form-duration-divs
          /*$('.order_details').hide();*/
          var status = $("#order_status option:selected").val();

          if (status == 'Otvoren' || status == 'Zakazan' || status == 'Odložen' || status == 'Otkazan' || status == 'Nekompletan') {
              $(".order_details").hide();
              $(".save_changes").show();
              console.log(status);
          } else {
              $(".order_details").show();
              $(".save_changes").show();
              console.log(status);
          }

          if (start_status == status) {
              $(".save_changes").hide();
          }

          if (status == 'Zakazan') {
              $(".div_status").show();
              if (start_scheduled_datetime != $("#scheduled_at_datetime").val()) {
                  $(".save_changes").show();
              }
          } else {
              $(".div_status").hide();
          }

          if (status == 'Završen') {
              $(".uncomplete").show();
              if (start_uncomplete != $("#uncomplete").val()) {
                  $(".save_changes").show();
              }
          } else {
              $(".uncomplete").hide();
          }

          if (status == 'Nerealizovan') {
              $(".free").show();
          } else {
              $(".free").hide();
          }

          /* Otvoren, Zakazan, Odložen, Otkazan, Nerealizovan, Nekompletan, Završen*/

      }


      $("#order_status").change(function () {
          setTimeout(function () {
              onStatusChange();
              checkCommentChange();
          }, 600);
      });

      $("#scheduled_at_datetime").change(function () {
          $(".save_changes").show();
      });

      function checkCommentChange() {
          if (start_order_comment != $("#order_comment").val()) {
              $(".save_changes").show();
          }
      }

      $("#order_comment").keyup(function () {
          if (start_order_comment != $("#order_comment").val()) {
              $(".save_changes").show();
          } else {
              $(".save_changes").hide();
          }
      });

      onStatusChange();

      if (start_status == "Otkazan") {
          /* $(".status_row").hide();*/
          $(".canceled").show();
      } else if(start_status == "Nerealizovan" || start_status == "Završen"){
          $(".status_row").hide();
          $(".order_details").hide();
          $(".unrealized").show();
      }


      //

      var json_equipment_select = {!! $data['equipment_for_select'] !!};
      var json_dismantled_eq_select = {!! $data['dismantled_eq_for_select'] !!};
      var json_all_serials = {!! json_encode($data['all_serials']) !!};

      function filterEquipment(key, value) {
          return json_equipment_select.filter(function (equipment) {
              return equipment[key] == value;
          });
      } // filtriranje opreme na osnovu tipa (subtype)

      function filterDismantledEquipment(key, value) {
          return json_dismantled_eq_select.filter(function (equipment) {
              return equipment[key] == value;
          });
      } // filtriranje demontirane opreme na osnovu tipa (subtype)

      function selectSerial(serial, subtype) {
          var selected_subtype = $('#' + subtype + ' option:selected').val();
          var filtered_equipment = filterEquipment('equipment_subtype_id', selected_subtype);
          //equipment_serial select
          var filtered_equipment_serial = {};
          $.each(filtered_equipment, function (index) {
              filtered_equipment_serial[index] = this['equipment_id'];
          });
          var serial_html = '<option value="" selected="selected"></option>';
          $.each(json_all_serials, function (serial_key, serial_value) {
              $.each(filtered_equipment_serial, function (key, equipment_id) {
                  var split_key = serial_key.split("_");
                  if (split_key['1'] == equipment_id) {
                      serial_html += '<option value="' + serial_key + '">' + serial_value + '</option>';
                  }
              });
          });
          $('#' + serial).html(serial_html);
      }

      $('#equipment_subtype_1').change(function () {
          selectSerial('equipment_serial_1', 'equipment_subtype_1');
      });

      $('#equipment_subtype_2').change(function () {
          selectSerial('equipment_serial_2', 'equipment_subtype_2');
      });

      $('#equipment_subtype_3').change(function () {
          selectSerial('equipment_serial_3', 'equipment_subtype_3');
      });

      $('#equipment_subtype_4').change(function () {
          selectSerial('equipment_serial_4', 'equipment_subtype_4');
      });

      $('#equipment_subtype_5').change(function () {
          selectSerial('equipment_serial_5', 'equipment_subtype_5');
      });

      function selectSubtype(serial, subtype) {
          var equipment_serial = $('#' + serial + ' option:selected').val();
          var slit_equipment_serial = equipment_serial.split("_");
          var equipment_id = slit_equipment_serial['1'];
          var filtered_equipment = filterEquipment('equipment_id', equipment_id);
          var equipment_subtype = filtered_equipment["0"].equipment_subtype_id;
          //equipment_subtype_1 select
          $('#' + subtype + ' option:selected').removeAttr("selected");
          $('#' + subtype + ' option[value="' + equipment_subtype + '"]').attr('selected', 'selected');
          var selected_subtype_text = $('#' + subtype + ' option:selected').text();
          $('#select2-' + subtype + '-container').prop('title', selected_subtype_text);
          $('#select2-' + subtype + '-container').prop('innerText', selected_subtype_text);
      } // selektovanje tipa na osnovu serijskog

      $('#equipment_serial_1').change(function () {
          selectSubtype('equipment_serial_1', 'equipment_subtype_1');
      });

      $('#equipment_serial_2').change(function () {
          selectSubtype('equipment_serial_2', 'equipment_subtype_2');
      });

      $('#equipment_serial_3').change(function () {
          selectSubtype('equipment_serial_3', 'equipment_subtype_3');
      });

      $('#equipment_serial_4').change(function () {
          selectSubtype('equipment_serial_4', 'equipment_subtype_4');
      });

      $('#equipment_serial_5').change(function () {
          selectSubtype('equipment_serial_5', 'equipment_subtype_5');
      });

     /* function notDefaultSelect(id) {
          var wares_id = $('#not_default_select_' + id + ' option:selected').val();
          // var slit_wares_value = wares_value.split("_");
          //var wares_id = slit_wares_value['1'];
          $('#hidden_not_default_input_' + id).attr('name', wares_id);
          console.log('notDefaultSelect is changed ' + id);
          console.log('wares id is ' + wares_id);
          console.log('not_default_input_' + id + 'name is ' + $('#not_default_input_' + id).attr('name'));
      }

      function notDefaultInput(id) {
          var wares_value = $('#not_default_input_' + id).val();

          $('#hidden_not_default_input_' + id).attr('name', wares_id);
          console.log('Value of not_default_input_ ' + id + '  is changed.');
          console.log('Value is ' + wares_value);
          console.log('Value of hiden_not_default_input_' + id + ' is ' + $('#hidden_not_default_input_' + id).val());
          console.log('Name of hiden_not_default_input_' + id + ' is ' + $('#hidden_not_default_input_' + id).attr('name'));
      }*/





      function selectDismantledModel(model, subtype) {

          var selected_subtype = $('#' + subtype + ' option:selected').val();
          console.log(selected_subtype);
          var filtered_equipment = filterDismantledEquipment('dismantled_eq_subtype_id', selected_subtype);

          //equipment_model_id select
          var filtered_equipment_model = {};
          $.each(filtered_equipment, function () {
              filtered_equipment_model[this['dismantled_eq_model_id']] = this['dismantled_eq_model_name'];
          });
          var model_html = '<option value="" selected="selected"> ';
          $.each(filtered_equipment_model, function (key, value) {
              if (key != 'null') {
                  model_html += '<option value="' + key + '">' + value + ' ';
              }
          });
          console.log(model_html);
          console.log('#'+model);
          $('#'+model).html(model_html);

      } // filter modela na osnovu subtype


      $('#dismantled_eq_subtype_id_1').change(function () {
          selectDismantledModel('dismantled_eq_model_id_1', 'dismantled_eq_subtype_id_1');
      });

      $('#dismantled_eq_subtype_id_2').change(function () {
          selectDismantledModel('dismantled_eq_model_id_2', 'dismantled_eq_subtype_id_2');
      });

      $('#dismantled_eq_subtype_id_3').change(function () {
          selectDismantledModel('dismantled_eq_model_id_3', 'dismantled_eq_subtype_id_3');
      });

      $('#dismantled_eq_subtype_id_4').change(function () {
          selectDismantledModel('dismantled_eq_model_id_4', 'dismantled_eq_subtype_id_4');
      });

      $('#dismantled_eq_subtype_id_5').change(function () {
          selectDismantledModel('dismantled_eq_model_id_5', 'dismantled_eq_subtype_id_5');
      });

      /*  $('#dismantled_eq_subtype_id_1').change(function () {
            var selected_subtype = $("#dismantled_eq_subtype_id_1 option:selected").val();
            var filtered_equipment = filterDismantledEquipment('dismantled_eq_subtype_id', selected_subtype);

            //equipment_model_id select
            var filtered_equipment_model = {};
            $.each(filtered_equipment, function () {
                filtered_equipment_model[this['dismantled_eq_model_id']] = this['dismantled_eq_model_name'];
            });
            var model_html = '<option value="" selected="selected"> ';
            $.each(filtered_equipment_model, function (key, value) {
                if (key != 'null') {
                    model_html += '<option value="' + key + '">' + value + ' ';
                }
            });

            $('#dismantled_eq_model_id_1').html(model_html);

        });*/
      function selectDismantledSubtype(model, subtype) {

          var selected_model = $('#'+model+' option:selected').val();

          var filtered_equipment = filterDismantledEquipment('dismantled_eq_model_id', selected_model);

          var equipment_subtype = filtered_equipment["0"].dismantled_eq_subtype_id;

          //equipment_subtype_id select
          $('#'+subtype+' option:selected').removeAttr("selected");
          $('#'+subtype+' option[value="' + equipment_subtype + '"]').attr('selected', 'selected');
          var selected_subtype_text = $('#'+subtype+' option:selected').text();
          $('#select2-'+subtype+'-container').prop('title', selected_subtype_text);
          $('#select2-'+subtype+'-container').prop('innerText', selected_subtype_text);

      } // select subtype na osnovu modela


      $('#dismantled_eq_model_id_1').change(function () {
          selectDismantledSubtype('dismantled_eq_model_id_1', 'dismantled_eq_subtype_id_1');
      });

      $('#dismantled_eq_model_id_2').change(function () {
          selectDismantledSubtype('dismantled_eq_model_id_2', 'dismantled_eq_subtype_id_2');
      });

      $('#dismantled_eq_model_id_3').change(function () {
          selectDismantledSubtype('dismantled_eq_model_id_3', 'dismantled_eq_subtype_id_3');
      });

      $('#dismantled_eq_model_id_4').change(function () {
          selectDismantledSubtype('dismantled_eq_model_id_4', 'dismantled_eq_subtype_id_4');
      });

      $('#dismantled_eq_model_id_5').change(function () {
          selectDismantledSubtype('dismantled_eq_model_id_5', 'dismantled_eq_subtype_id_5');
      });
      //equipment_model_id select
      /*$('#dismantled_eq_model_id_1').change(function () {
          var selected_model = $("#dismantled_eq_model_id_1 option:selected").val();
          console.log(selected_model);
          var filtered_equipment = filterDismantledEquipment('dismantled_eq_model_id', selected_model);
          //console.log(filtered_equipment);
          var equipment_subtype = filtered_equipment["0"].dismantled_eq_subtype_id;
          console.log(equipment_subtype);
          //equipment_subtype_id select
          $('#dismantled_eq_subtype_id_1 option:selected').removeAttr("selected");
          $('#dismantled_eq_subtype_id_1 option[value="' + equipment_subtype + '"]').attr('selected', 'selected');
          var selected_subtype_text = $("#dismantled_eq_subtype_id_1 option:selected").text();
          $('#select2-dismantled_eq_subtype_id_1-container').prop('title', selected_subtype_text);
          $('#select2-dismantled_eq_subtype_id_1-container').prop('innerText', selected_subtype_text);

      });*/


  </script>
@endpush

@section('content')
  <div class="title m-b-md mt-3">
    <h1 style="text-align: center;">Kreiranje SBB naloga</h1>
  </div>

  @if ($errors->any())
    <div class="alert alert-danger">
        @foreach ($errors->all() as $error)
          <div>- {{ $error }}</div>
        @endforeach
    </div>
  @endif

<div class="card mt-1">

  <div class="card-header header-elements-inline">
    <h4 class="card-title">Osnovni podaci naloga</h4>

    <div class="header-elements">
      <div class="list-icons">
        <a class="list-icons-item" data-action="collapse"></a>
      </div>
    </div>
  </div>

  <div class="card-body">
    {!! Form::open(['action' => 'OrdersController@store', 'method' => 'POST', 'id' => 'create_sbb_order', 'data-parsley-validate' => '']) !!}
      <div class="row">

        <div class="col-12">
          <fieldset>
            <legend class="font-weight-semibold"><i class="icon-truck mr-2"></i> Podaci o nalogu</legend>

              <div class="row">
                  <p style="font-size: 14px" class="ml-2 mb-3">Redni broj naloga: <span style="font-weight: bold;">{{ newSbbOrdinalNumber() }}</span></p>
                {{--{{Form::hidden ('ordinal_number', '18-03-00134')}}--}}
               {{-- {{Form::hidden ('order_type', 'sbb_order')}}--}}
               {{-- {{Form::hidden ('order_status', 1)}}--}}
              </div>

            <div class="row">

              {{ Form::textGroup2('order_number', 'Broj naloga:', null, null, 'form-control', ['placeholder' => 'Broj naloga', 'data-parsly-trigger'=> 'keyup'], 'col-3', true) }}

             {{-- 'name', 'labelvalue', 'classlabel', 'value' => null, 'classtext', 'attributes' => ['required' => 'required'], 'col'--}}
              {{ Form::datetime_picker('opened_at_datetime', 'Datum i vreme prijave:', null, null, 'form-control', ['placeholder' => 'Izaberi datum'], 'col-4', true) }}

             {{-- {{ Form::datetime_picker('started_at_datetime', 'Predvidjeni pocetak rada:', null, null, 'form-control', ['placeholder' => 'Izaberi datum'], 'col-4', true) }}--}}


            </div>


            <div class="row">

                {{ Form::textGroup2('buyer_id', 'Broj kupca:', null, null, 'form-control', ['placeholder' => 'Broj kupca'], 'col-3') }}

                {{ Form::textGroup2('treaty_id', 'Broj ugovora:', null, null, 'form-control', ['placeholder' => 'Broj ugovora'], 'col-3') }}


                {{ Form::textGroup2('buyer', 'Ime i prezime kupca:', null, null, 'form-control', ['placeholder' => 'Ime i prezime kupca'], 'col-6', true) }}

              </div>

            {{-- POST GRAD Opština --}}
            {{-- U redu tri inputa sa selectom--}}
            <div class="row">

              {{ Form::textGroup2('area_code', 'Poštanski broj:', null, null, 'form-control', ['placeholder' => 'Poštanski broj'], 'col-3') }}


              {{--{{ Form::textGroup2('city', 'Grad:', null, 'Beograd', 'form-control', ['readonly'], 'col-3', true) }}--}}

                {{ Form::selectGroupSearch('city', true, 'Grad:', null,
              $data['city'], 'Beograd',
                 ['data-placeholder' => 'Odaberite grad',
                 'class'=> 'form-control select-search township'], 'col-3', true) }}


              {{ Form::selectGroupSearch('region', true, 'Region:', null,
              $data['region'], null,
                 ['data-placeholder' => 'Odaberite region',
                 'class'=> 'form-control select-search township'], 'col-3', true) }}



              {{ Form::selectGroupSearch('township', true, 'Odaberite opštinu:', null,
              $data['township'], null,
               ['data-placeholder' => 'Odaberite opštinu',
               'class'=> 'form-control select-search township'], 'col-3', true) }}

            </div>
            {{-- POST GRAD Opština --}}

            {{-- ADRESA --}}
            {{-- U redu cetiri inputa --}}
            <div class="row">

              {{ Form::textGroup2('address', 'Adresa:', null, null, 'form-control', ['placeholder' => 'Naziv ulice'], 'col-6', true) }}

              {{ Form::textGroup2('address_number', 'broj:', null, null, 'form-control', ['placeholder' => 'broj'], 'col-2', true) }}

              {{ Form::textGroup2('floor', 'sprat:', null, null, 'form-control', ['placeholder' => 'sprat'], 'col-2') }}

              {{ Form::textGroup2('apartment', 'stan:', null, null, 'form-control', ['placeholder' => 'stan'], 'col-2') }}

            </div>
            {{-- /ADRESA --}}

            {{-- TELEFONI --}}
            {{-- U redu dva inputa --}}
            <div class="row">

              {{ Form::textGroup2('phone_number', 'Broj telefona:', null, null, 'form-control', ['placeholder' => 'Broj telefona'], 'col-6', true) }}

              {{ Form::textGroup2('mobile_number', 'Mobilni:', null, null, 'form-control', ['placeholder' => 'Mobilni'], 'col-6') }}

            </div>
            {{-- /TELEFONI --}}

          </fieldset>
        </div>

          </fieldset>
        </div>
      </div>

    {{--</form>--}}
  </div>


{{--   ************************************************************************************************************************************** --}}
<div class="card">

<div class="card-header header-elements-inline">
    <h4 class="card-title">Tip posla, ugradjena oprema i materijal</h4>
    <div class="header-elements">
      <div class="list-icons">
        <a class="list-icons-item" data-action="collapse"></a>
       {{-- <a class="list-icons-item" data-action="reload"></a>--}}
        {{--  NE FUNKCIONISE RELOAD DUGME --}}
      </div>
    </div>
  </div>

<div class="card-body">

      <div class="row">

        {{-- CELA STRANA --}}
        <div class="col-md-12">
          <fieldset>


              <legend class="font-weight-semibold"><i class="{{-- NASE IKONICE --}}"></i> Tip posla
              </legend>


            <div class="row">

                {{ Form::selectGroupSearch('service_type', true ,'Tip posla:', null,
               $data['services'], null,
                ['data-placeholder' => 'Odaberite tip posla',
                'class'=> 'form-control select-search'], 'form-group col-5', true) }}

            </div>

                  <legend class="font-weight-semibold"><i class="{{-- NASE IKONICE --}}"></i> Naziv opreme izdate na korišćenje
                  </legend>

                  {{-- AKTIVNOSTI NA NOVOJ OPREMI --}}
                  {{-- U redu tri SELECTA sa pet redova --}}

                  <div class="row">

                      {{ Form::selectGroupSearch('equipment_subtype_1',
                                      true,
                                      'Tip opreme:',
                                      null,
                                      $data['subtype'], null,
                                      ['data-placeholder' => 'Izaberite tip opreme',
                                       'class'=> 'form-control select-search equipment_subtype',
                                        ], 'col-6 form-row mb-2 mr-1') }}




                      {{ Form::selectGroupSearch('equipment_serial_1',
                                                      true,
                                                      'Serijski broj opreme:',
                                                      null,
                                                      $data['all_serials'], null,
                                                      ['data-placeholder' => 'Izaberite serijski broj',
                                                       'class'=> 'form-control select-search equipment_model',
                                                        ], 'col-6 form-row mb-2') }}

                  </div>

                  <div class="row">

                      {{ Form::selectGroupSearch('equipment_subtype_2',
                                      false,
                                      null,
                                      null,
                                      $data['subtype'], null,
                                      ['data-placeholder' => 'Izaberite tip opreme',
                                      'id' => 'equipment_subtype_2',
                                       'class'=> 'form-control select-search equipment_subtype',
                                        ], 'col-6 form-row mb-2 mr-1') }}




                      {{ Form::selectGroupSearch('equipment_serial_2',
                                                      false,
                                                      null,
                                                      null,
                                                      $data['all_serials'], null,
                                                      ['data-placeholder' => 'Izaberite serijski broj',
                                                      'id' => 'equipment_serial_2',
                                                       'class'=> 'form-control select-search',
                                                        ], 'col-6 form-row mb-2') }}

                  </div>

                  <div class="row">

                      {{ Form::selectGroupSearch('equipment_subtype_3',
                                      false,
                                      null,
                                      null,
                                      $data['subtype'], null,
                                      ['data-placeholder' => 'Izaberite tip opreme',
                                      'id' => 'equipment_subtype_3',
                                       'class'=> 'form-control select-search equipment_subtype',
                                        ], 'col-6 form-row mb-2 mr-1') }}




                      {{ Form::selectGroupSearch('equipment_serial_3',
                                                      false,
                                                      null,
                                                      null,
                                                      $data['all_serials'], null,
                                                      ['data-placeholder' => 'Izaberite serijski broj',
                                                      'id' => 'equipment_serial_3',
                                                       'class'=> 'form-control select-search',
                                                        ], 'col-6 form-row mb-2') }}

                  </div>

                  <div class="row">

                      {{ Form::selectGroupSearch('equipment_subtype_4',
                                      false,
                                      null,
                                      null,
                                      $data['subtype'], null,
                                      ['data-placeholder' => 'Izaberite tip opreme',
                                      'id' => 'equipment_subtype_4',
                                       'class'=> 'form-control select-search equipment_subtype',
                                        ], 'col-6 form-row mb-2 mr-1') }}




                      {{ Form::selectGroupSearch('equipment_serial_4',
                                                      false,
                                                      null,
                                                      null,
                                                      $data['all_serials'], null,
                                                      ['data-placeholder' => 'Izaberite serijski broj',
                                                      'id' => 'equipment_serial_4',
                                                       'class'=> 'form-control select-search',
                                                        ], 'col-6 form-row mb-2') }}

                  </div>

                  <div class="row">

                      {{ Form::selectGroupSearch('equipment_subtype_5',
                                      false,
                                      null,
                                      null,
                                      $data['subtype'], null,
                                      ['data-placeholder' => 'Izaberite tip opreme',
                                      'id' => 'equipment_subtype_5',
                                       'class'=> 'form-control select-search equipment_subtype',
                                        ], 'col-6 form-row mb-2 mr-1') }}




                      {{ Form::selectGroupSearch('equipment_serial_5',
                                                      false,
                                                      null,
                                                      null,
                                                      $data['all_serials'], null,
                                                      ['data-placeholder' => 'Izaberite serijski broj',
                                                      'id' => 'equipment_serial_5',
                                                       'class'=> 'form-control select-search',
                                                        ], 'col-6 form-row mb-2') }}

                  </div>


                  {{-- AKTIVNOSTI NA NOVOJ OPREMI --}}

                  <legend class="font-weight-semibold"><i class="{{-- NASE IKONICE --}}"></i> Naziv opreme koja se demontira i vraća
                  </legend>

                  {{-- AKTIVNOSTI NA POSTOJECOJ OPREMI --}}
                  {{-- U redu tri SELECTA sa pet redova --}}

                  <div class="row">

                      {{ Form::selectGroupSearch('dismantled_eq_subtype_id_1',
                                      true,
                                      'Tip opreme:',
                                      null,
                                      $data['dismantled_subtype'], null,
                                      ['data-placeholder' => 'Izaberite tip opreme',
                                       'class'=> 'form-control select-search equipment_subtype',
                                        ], 'col-3 form-row mb-2 mr-1') }}

                      {{ Form::selectGroupSearch('dismantled_eq_model_id_1',
                                      true,
                                      'Model opreme:',
                                      null,
                                      $data['dismantled_model'], null,
                                      ['data-placeholder' => 'Izaberite tip opreme',
                                       'class'=> 'form-control select-search equipment_subtype',
                                       'id'=> 'dismantled_eq_model_id_1',
                                        ], 'col-3 form-row mb-2 mr-1') }}


                      <div class="form-group col-3 mb-2">
                          <label for="dismantled_eq1_serial1">Serijski broj 1:</label>
                          <input class="form-control" placeholder="Upisite serijski broj"
                                 name="dismantled_eq1_serial1" type="text" id="dismantled_eq1_serial1" value="{{old('dismantled_eq1_serial1')}}">
                      </div>

                      <div class="form-group col-3 mb-2">
                          <label for="dismantled_eq1_serial2">Serijski broj 2:</label>
                          <input class="form-control" placeholder="Upisite serijski broj"
                                 name="dismantled_eq1_serial2" type="text" id="dismantled_eq1_serial2" value="{{old('dismantled_eq1_serial2')}}">
                      </div>


                  </div>

                  <div class="row">

                      {{ Form::selectGroupSearch('dismantled_eq_subtype_id_2',
                                      false,
                                      null,
                                      null,
                                      $data['dismantled_subtype'], null,
                                      ['data-placeholder' => 'Izaberite tip opreme',
                                       'class'=> 'form-control select-search equipment_subtype',
                                       'id'=> 'dismantled_eq_subtype_id_2',
                                        ], 'col-3 form-row mb-2 mr-1') }}

                      {{ Form::selectGroupSearch('dismantled_eq_model_id_2',
                                      false,
                                      null,
                                      null,
                                      $data['dismantled_model'], null,
                                      ['data-placeholder' => 'Izaberite tip opreme',
                                       'class'=> 'form-control select-search equipment_subtype',
                                       'id'=> 'dismantled_eq_model_id_2',
                                        ], 'col-3 form-row mb-2 mr-1') }}

                      <div class="form-group col-3 mb-2">
                          <input class="form-control" placeholder="Upisite serijski broj"
                                 name="dismantled_eq2_serial1" type="text" id="dismantled_eq2_serial1" value="{{old('dismantled_eq2_serial1')}}">
                      </div>
                      <div class="form-group col-3 mb-2">
                          <input class="form-control" placeholder="Upisite serijski broj"
                                 name="dismantled_eq2_serial2" type="text" id="dismantled_eq2_serial2" value="{{old('dismantled_eq2_serial2')}}">
                      </div>
                  </div>

                  <div class="row">

                      {{ Form::selectGroupSearch('dismantled_eq_subtype_id_3',
                                      false,
                                      null,
                                      null,
                                      $data['dismantled_subtype'], null,
                                      ['data-placeholder' => 'Izaberite tip opreme',
                                       'class'=> 'form-control select-search equipment_subtype',
                                       'id'=> 'dismantled_eq_subtype_id_3',
                                        ], 'col-3 form-row mb-2 mr-1') }}

                      {{ Form::selectGroupSearch('dismantled_eq_model_id_3',
                                      false,
                                      null,
                                      null,
                                      $data['dismantled_model'], null,
                                      ['data-placeholder' => 'Izaberite tip opreme',
                                       'class'=> 'form-control select-search equipment_subtype',
                                       'id'=> 'dismantled_eq_model_id_3',
                                        ], 'col-3 form-row mb-2 mr-1') }}

                      <div class="form-group col-3 mb-2">
                          <input class="form-control" placeholder="Upisite serijski broj"
                                 name="dismantled_eq3_serial1" type="text" id="dismantled_eq3_serial1" value="{{old('dismantled_eq3_serial1')}}">
                      </div>
                      <div class="form-group col-3 mb-2">
                          <input class="form-control" placeholder="Upisite serijski broj"
                                 name="dismantled_eq3_serial2" type="text" id="dismantled_eq3_serial2" value="{{old('dismantled_eq3_serial2')}}">
                      </div>
                  </div>

                  <div class="row">

                      {{ Form::selectGroupSearch('dismantled_eq_subtype_id_4',
                                      false,
                                      null,
                                      null,
                                      $data['dismantled_subtype'], null,
                                      ['data-placeholder' => 'Izaberite tip opreme',
                                       'class'=> 'form-control select-search equipment_subtype',
                                       'id'=> 'dismantled_eq_subtype_id_4',
                                        ], 'col-3 form-row mb-2 mr-1') }}

                      {{ Form::selectGroupSearch('dismantled_eq_model_id_4',
                                      false,
                                      null,
                                      null,
                                      $data['dismantled_model'], null,
                                      ['data-placeholder' => 'Izaberite tip opreme',
                                       'class'=> 'form-control select-search equipment_subtype',
                                       'id'=> 'dismantled_eq_model_id_4',
                                        ], 'col-3 form-row mb-2 mr-1') }}

                      <div class="form-group col-3 mb-2">
                          <input class="form-control" placeholder="Upisite serijski broj"
                                 name="dismantled_eq4_serial1" type="text" id="dismantled_eq4_serial1" value="{{old('dismantled_eq4_serial1')}}">
                      </div>
                      <div class="form-group col-3 mb-2">
                          <input class="form-control" placeholder="Upisite serijski broj"
                                 name="dismantled_eq4_serial2" type="text" id="dismantled_eq4_serial2" value="{{old('dismantled_eq4_serial2')}}">
                      </div>
                  </div>

                  <div class="row">

                      {{ Form::selectGroupSearch('dismantled_eq_subtype_id_5',
                                      false,
                                      null,
                                      null,
                                      $data['dismantled_subtype'], null,
                                      ['data-placeholder' => 'Izaberite tip opreme',
                                       'class'=> 'form-control select-search equipment_subtype',
                                       'id'=> 'dismantled_eq_subtype_id_5',
                                        ], 'col-3 form-row mb-2 mr-1') }}

                      {{ Form::selectGroupSearch('dismantled_eq_model_id_5',
                                      false,
                                      null,
                                      null,
                                      $data['dismantled_model'], null,
                                      ['data-placeholder' => 'Izaberite tip opreme',
                                       'class'=> 'form-control select-search equipment_subtype',
                                       'id'=> 'dismantled_eq_model_id_5',
                                        ], 'col-3 form-row mb-2 mr-1') }}

                      <div class="form-group col-3 mb-2">
                          <input class="form-control" placeholder="Upisite serijski broj"
                                 name="dismantled_eq5_serial1" type="text" id="dismantled_eq5_serial1" value="{{old('dismantled_eq5_serial1')}}">
                      </div>
                      <div class="form-group col-3 mb-2">
                          <input class="form-control" placeholder="Upisite serijski broj"
                                 name="dismantled_eq5_serial2" type="text" id="dismantled_eq5_serial2" value="{{old('dismantled_eq5_serial2')}}">
                      </div>
                  </div>

                  {{-- Odabir materijala --}}

                  <legend class="font-weight-semibold"><i class="{{-- NASE IKONICE --}}"></i> Utrošeni
                      materijal
                  </legend>
                  <div class="row">

                      <div class="col-6">
                          <table class="table datatable-basic table-bordered table-striped">
                              <thead>
                              <tr>
                                  <th>Šifra i naziv materijala</th>
                                  <th>Količina</th>
                              </tr>
                              </thead>
                              {{--@dd($default_wares)--}}
                              {{--{{ default wares }}--}}
                              <tbody>
                              @foreach($data['default_wares'] as $wares)

                                  <tr>
                                      <td class="lesspadding">{{ $wares->wares_code }}
                                          - {{ $wares->wares_name }}</td>
                                      <td style="width: 140px" class="lesspadding">
                                          <input style="width: 50px" type="number" min="1" step="1"
                                                  name="wares_{{ $wares->wares_id }}" value="{{old('wares_'.$wares->wares_id) }}"> {{ $wares->wares_type }}
                                      </td>
                                  </tr>
                              @endforeach
                              </tbody>
                              {{--{{ /$default wares }}--}}
                          </table>

                      </div>

                      <div class="col-6">

                          <table class="table datatable-basic table-bordered table-striped">
                              <thead>
                              <tr>
                                  <th>Šifra i naziv materijala</th>
                                  <th>Količina</th>
                              </tr>
                              </thead>
                              {{--@dd($default_wares)--}}
                              {{--{{ default wares }}--}}
                              <tbody>
                              @for($i = 1; $i <= 20; $i++)
                                  {{--@foreach($data['default_wares'] as $wares)--}}
                                  <tr>
                                      <td class="lesspadding">{{ Form::selectGroupSearch('not_default_select_'.$i, false, null, null,
                                                             $data['wares'], null,
                                                                 ['data-placeholder' => 'Odaberite materijal',
                                                                 'id' => 'not_default_select_'.$i,
                                                                 'data-id'=> $i,
                                                                 'class'=> 'form-control select-search',                                                                                         /* 'onchange'=> 'notDefaultSelect('.$i.')',*/
                                                                  ], 'col-12') }}</td>
                                      <td style="width: 140px" class="lesspadding">


                                          <input id="not_default_input_{{ $i }}" style="width: 50px"
                                                 type="number" min="1" step="1" name="not_default_input_{{ $i }}" value="{{old('not_default_input_'. $i )}}" {{--onchange="notDefaultInput({{ $i }})"--}}>
                                          {{--{{Form::hidden ('hidden_not_default_input_'. $i, 'hidden_not_default_input_'. $i, ['id' => 'hidden_not_default_input_'. $i ])}}--}}
                                      </td>
                                  </tr>
                              @endfor
                              </tbody>
                              {{--{{ /$default wares }}--}}
                          </table>


                      </div>

                  </div>
                  {{-- /odabir materijala --}}


                  {{-- TEHNICARI --}}
                  <legend class="font-weight-semibold"><i class="{{-- NASE IKONICE --}}"></i> Izbor
                      tehnčara
                  </legend>
                  <div class="row mb-3">
                      {{--Prvi tehnicar--}}
                      {{ Form::selectGroupSearch('technician_1',
                                  true,
                                  'Prvi tehničar:',
                                  null,
                                  $data['technicians'], null,
                                  ['data-placeholder' => 'Izaberite prvog tehničara',
                                   'class'=> 'form-control select-search technician_1',
                                   'data-focus'], 'col-6 form-row mb-2', true) }}

                      {{--Drugi tehnicar--}}
                      {{ Form::selectGroupSearch('technician_2',
                                  true,
                                  'Drugi tehničar:',
                                  null,
                                  $data['technicians'], null,
                                  ['data-placeholder' => 'Izaberite drugog tehničara',
                                   'class'=> 'form-control select-search technician_2',
                                   'data-focus'], 'col-6 form-row mb-2') }}
                  </div>

                  {{-- /TEHNICARI --}}



                  {{--VRSTA OBJEKTA--}}
                  <legend class="font-weight-semibold"><i class="{{-- NASE IKONICE --}}"></i> Vrsta
                      objekta i privatno vozilo
                  </legend>
                  <div class="row">
                      <div class=" col-6">
                          <div class="form-group">

                              <label class="col-form-label mr-2" for="service_home">Vrsta objekta<span class="text-danger">*</span></label>
                              <div class="form-check form-check-inline">
                                  <label class="form-check-label">
                                      <input type="radio" class="form-check-input" value="apartment"
                                             name="service_home" @if(old('service_home')) checked @endif>Stan</label>
                              </div>

                              <div class="form-check form-check-inline">
                                  <label class="form-check-label">
                                      <input type="radio" class="form-check-input" value="house"
                                             name="service_home" @if(old('service_home')) checked @endif>Kuca</label>
                              </div>

                          </div>
                      </div>

                      {{--Privatno vozilo--}}
                      <div class=" col-6">
                          <div class="form-check form-check-inline form-check-right">
                              <label class="form-check-label">
                                  Tehničar koristio privatno vozilo
                                  <input type="checkbox" class="form-check-input" name="private_vehicle"
                                         value="1" @if(old('private_vehicle')) checked @endif>
                              </label>
                          </div>
                      </div>

                  </div>



                  {{-- vreme zatvaranja --}}
                  <legend class="font-weight-semibold"><i class="{{-- NASE IKONICE --}}"></i> Vreme zavrsavanja naloga</legend>
                  <div class="row mb-3">
                      {{--vreme zatvaranja--}}
                      {{ Form::datetime_picker('completed_at', 'Datum i vreme zatvaranja naloga:', null, null, 'form-control', ['placeholder' => 'Izaberi datum'], 'col-4', true) }}

                      <div class="col-8"></div>
                  </div>

                  {{-- /vreme zatvaranja --}}

            {{-- KOMENTAR --}}

            {{ Form::textareaGroup('order_note', 'Komentar:', null, null, 'form-control', ['placeholder' => 'Upisite dodatne komentare', 'rows' => '5', 'cols' => '5'], 'col-12') }}


            {{--<div class="form-group">
              <label>Komentar:</label>
              <textarea rows="5" cols="5" id="order_note" class="form-control" placeholder="Upisite dodatne komentare"></textarea>
            </div>--}}
            {{-- /KOMENTAR --}}

          </fieldset>
        </div>
      </div>

        <div class="text-right">
        <button type="submit" class="btn bg-teal-300">Kreiraj nalog <i class="icon-paperplane ml-2"></i></button>
      </div>

    </form>
  </div>
</div>
{{--   ************************************************************************************************************************************** --}}

@endsection