@extends('layouts.app')

@push ('style')
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet"
          type="text/css">
    <link href="../global_assets/css/icons/icomoon/styles.css" rel="stylesheet" type="text/css">
    <link href="../assets/css/bootstrap.min.css" rel="stylesheet" type="text/css">
    <link href="../assets/css/bootstrap_limitless.min.css" rel="stylesheet" type="text/css">
    <link href="../assets/css/layout.min.css" rel="stylesheet" type="text/css">
    <link href="../assets/css/components.min.css" rel="stylesheet" type="text/css">
    <link href="../assets/css/colors.min.css" rel="stylesheet" type="text/css">

    <style>
        input.parsley-success,
        select.parsley-success,
        textarea.parsley-success {
            /* color: #468847;*/
            background-color: #DFF0D8;
            border: 1px solid #D6E9C6;
        }

        input.parsley-error,
        select.parsley-error,
        textarea.parsley-error {
            /* color: #B94A48;*/
            background-color: #F2DEDE;
            border: 1px solid #EED3D7;
        }

        .parsley-errors-list {
            margin: 2px 0 3px;
            padding: 0;
            list-style-type: none;
            font-size: 0.9em;
            line-height: 0.9em;
            opacity: 0;
            color: #B94A48;

            transition: all .3s ease-in;
            -o-transition: all .3s ease-in;
            -moz-transition: all .3s ease-in;
            -webkit-transition: all .3s ease-in;
        }

        .parsley-errors-list.filled {
            opacity: 1;
        }
    </style>
@endpush

@push('scripts')

    <script src="../global_assets/js/plugins/tables/datatables/datatables.min.js"></script>
    <script src="../global_assets/js/demo_pages/datatables_basic.js"></script>
    <!-- Load select2 -->
    <script type="text/javascript" src="../global_assets/js/plugins/forms/selects/select2.min.js"></script>
    <script type="text/javascript" src="../global_assets/js/plugins/extensions/jquery_ui/interactions.min.js"></script>
    <script src="../assets/js/custom_select2.js"></script>
    <script src="../global_assets/js/plugins/forms/selects/select2.min.js"></script>
    {{--anytime--}}
    <script src="../global_assets/js/plugins/ui/moment/moment.min.js"></script>
    <script src="../global_assets/js/plugins/pickers/anytime.min.js"></script>
    <script src="../../../../global_assets/js/plugins/pickers/daterangepicker.js"></script>
    <script src="../assets/js/custom_datepicker.js"></script>

    <script src="../global_assets/js/main/jquery.min.js"></script>
    <script src="../global_assets/js/main/bootstrap.bundle.min.js"></script>
    <script src="../global_assets/js/plugins/loaders/blockui.min.js"></script>
    <script src="../global_assets/js/plugins/tables/datatables/datatables.min.js"></script>
    <script src="../global_assets/js/plugins/forms/selects/select2.min.js"></script>
    <script src="../global_assets/js/plugins/tables/datatables/extensions/jszip/jszip.min.js"></script>
    <script src="../global_assets/js/plugins/tables/datatables/extensions/buttons.min.js"></script>

    {{--export datatable--}}

    {{--anytime--}}
    <script src="../global_assets/js/plugins/ui/moment/moment.min.js"></script>
    <script src="../global_assets/js/plugins/pickers/anytime.min.js"></script>
    <script src="../../../../global_assets/js/plugins/pickers/daterangepicker.js"></script>
    <script src="../assets/js/custom_datepicker.js"></script>



    <script>

        var today = new Date();
        var dd = String(today.getDate()).padStart(2, '0');
        var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
        var yyyy = today.getFullYear();

        today = dd + '-' + mm + '-' + yyyy;
        console.log(today);


        $('.datatable-basic1').DataTable({
            //autoWidth: false,
            columnDefs: [{
                width: 400,
                targets: [7]
            }],
            dom: '<"datatable-header"fBl><"datatable-scroll-wrap"t><"datatable-footer"ip>',
            language: {
                search: '<span>Pretraga:</span> _INPUT_',
                lengthMenu: '<span>Prikaz:</span> _MENU_',
                info: '_START_ do _END_ od ukupno _TOTAL_',
                paginate: {'first': 'First', 'last': 'Last', 'next': '→', 'previous': '←'}
            },
            buttons: {
                buttons: [
                    {
                        extend: 'excelHtml5',
                        className: 'btn btn-light',
                        title: 'Grupna pretraga ' + today,
                        sheetName: 'Grupna pretraga',
                        exportOptions: {
                            columns: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
                        }
                    }
                ]
            }
        });

        $('.datatable-basic2').DataTable({
            autoWidth: false,
            dom: '<"datatable-header"fl><"datatable-scroll"t><"datatable-footer"ip>',
            language: {
                search: '<span>Pretraga:</span> _INPUT_',
                lengthMenu: '<span>Prikaz:</span> _MENU_',
                info: '_START_ do _END_ od ukupno _TOTAL_',
                paginate: {'first': 'First', 'last': 'Last', 'next': '→', 'previous': '←'}
            }
        });

    </script>
@endpush

@section('content')
    <div class="title m-b-md mt-3">
        <h1 style="text-align: center;">Grupna pretraga naloga</h1>
    </div>

    @if ($errors->any())
        <div class="alert alert-danger">
            @foreach ($errors->all() as $error)
                <div>- {{ $error }}</div>
            @endforeach
        </div>
    @endif

    <div class="card">

        <div class="card-body">

            @if(empty($orders) and empty($not_found))
                {!! Form::open(['action' => 'OrdersController@group_search', 'method' => 'POST', 'id' => 'group_search', 'data-parsley-validate' => '']) !!}

                <div class="row">

                    <div class="col-md-12">
                        <fieldset>
                            {{ Form::textareaGroup('group_search', 'Pretraga:', null, null, 'form-control', ['placeholder' => 'Pretraga', 'rows' => '15', 'cols' => '5'], 'col-5') }}
                        </fieldset>
                    </div>
                </div>

                <div class="text-left">
                    <button type="submit" class="btn bg-teal-300">Pretraga naloga <i class="icon-search4 ml-2"></i>
                    </button>
                </div>

                </form>

            @else
                <div class="text-left">
                    <a class="btn bg-teal-300" href="/group_search"><i class="icon-arrow-left15 ml-2"></i>Grupna
                        pretraga </a>
                </div>
                <table class="table datatable-basic1 table-bordered table-striped">
                    <thead>
                    <tr>
                        <th>Redni broj naloga</th>
                        <th>Broj naloga</th>
                        <th>Ime i preizme</th>
                        <th>Opština</th>
                        <th>Region</th>
                        <th>Adresa</th>
                        <th>Tip posla</th>
                        <th>Napomena</th>
                        <th>Poslednji izmenio</th>
                        <th>Datum i vreme prijave</th>
                        <th>Status naloga</th>
                     {{--   <th>Zakazan</th>
                        <th>Odložen</th>
                        <th>Otkazan</th>
                        <th>Završen</th>--}}
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($orders as $order)
                            <tr>
                                <td><a class="text-secondary"
                                       href="orders/{{ $order->order_id }}/show">{{ $order->ordinal_number }}</a>
                                </td>
                                <td>{{ $order->order_number }}</td>
                                <td>{{ $order->buyer }}</td>
                                <td>{{ $order->township }}</td>
                                <td>{{ $order->region }}</td>
                                <td>{{ $order->address }} {{ $order->address_number }}</td>
                                <td>{{ $order->service->service_name }}</td>
                                <td>{{ $order->note }}</td>
                                <td>{{ $order->order_updated_by }}</td>
                                <td>{{ datetimeForView($order->opened_at) }}</td>
                                <td class="text-center">
                                    @if($order->order_status=='Otvoren')
                                        <span class="badge badge-success p-2"
                                              style="font-size: 12px;">Otvoren</span></td>
                                @elseif($order->order_status=='Zakazan')
                                    <span class="badge badge-primary p-2"
                                          style="font-size: 12px;">Zakazan</span></td>
                                @elseif($order->order_status=='Završen' and $order->uncomplete == 1)
                                    <span class="badge badge-danger p-2"
                                          style="font-size: 12px;">Nekompletan</span></td>
                                @elseif($order->order_status=='Odložen')
                                    <span class="badge badge-warning p-2"
                                          style="font-size: 12px;">Odlozen</span></td>
                                @elseif($order->order_status=='Završen')
                                    <span class="badge badge-dark p-2"
                                          style="font-size: 12px;">Završen</span></td>
                                @elseif($order->order_status=='Otkazan')
                                    <span class="badge badge-secondary p-2"
                                          style="font-size: 12px;">Otkazan</span></td>
                                @elseif($order->order_status=='Nerealizovan')
                                    <span class="badge badge-secondary p-2"
                                          style="font-size: 12px;">Otkazan</span></td>
                                @endif

                               {{-- <td>
                                    {{ $order->order_status=='Zakazan' ? datetimeForView($order->scheduled_at) : '/' }}

                                </td>

                                <td>
                                    {{ $order->order_status=='Odložen' ? datetimeForView($order->updated_at) : '/' }}

                                </td>

                                <td>
                                    {{ ($order->order_status=='Otkazan' or $order->order_status=='Nerealizovan')  ? datetimeForView($order->completed_at) : '/' }}

                                </td>

                                <td>
                                    {{ $order->order_status=='Završen' ? datetimeForView($order->completed_at) : '/' }}
                                </td>--}}

                                <td>
                                    <a class="btn btn-outline-dark" href="orders/{{ $order->order_id }}/show"
                                       role="button">Prikaži nalog</a>
                                </td>
                            </tr>
                    @endforeach

                    </tbody>
                </table>

<hr>
                <div class="title m-b-md mt-3">
                    <h1 style="text-align: center;">Nalozi koji nisu pronađeni</h1>
                </div>
                <table class="table datatable-basic2 table-bordered table-striped">
                    <thead>
                    <tr>
                        <th>Broj naloga</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($not_found as $order)
                        <tr>
                            <td>{{ $order }}</td>
                        </tr>
                    @endforeach

                    </tbody>
                </table>


            @endif


        </div>
    </div>
    {{--   ************************************************************************************************************************************** --}}

@endsection