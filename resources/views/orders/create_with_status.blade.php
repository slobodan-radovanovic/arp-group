@extends('layouts.app')

@push ('style')
  <link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
  <link href="../global_assets/css/icons/icomoon/styles.css" rel="stylesheet" type="text/css">
  <link href="../assets/css/bootstrap.min.css" rel="stylesheet" type="text/css">
  <link href="../assets/css/bootstrap_limitless.min.css" rel="stylesheet" type="text/css">
  <link href="../assets/css/layout.min.css" rel="stylesheet" type="text/css">
  <link href="../assets/css/components.min.css" rel="stylesheet" type="text/css">
  <link href="../assets/css/colors.min.css" rel="stylesheet" type="text/css">

  <style>
    input.parsley-success,
    select.parsley-success,
    textarea.parsley-success {
     /* color: #468847;*/
      background-color: #DFF0D8;
      border: 1px solid #D6E9C6;
    }

    input.parsley-error,
    select.parsley-error,
    textarea.parsley-error {
     /* color: #B94A48;*/
      background-color: #F2DEDE;
      border: 1px solid #EED3D7;
    }

    .parsley-errors-list {
      margin: 2px 0 3px;
      padding: 0;
      list-style-type: none;
      font-size: 0.9em;
      line-height: 0.9em;
      opacity: 0;
      color: #B94A48;

      transition: all .3s ease-in;
      -o-transition: all .3s ease-in;
      -moz-transition: all .3s ease-in;
      -webkit-transition: all .3s ease-in;
    }

    .parsley-errors-list.filled {
      opacity: 1;
    }


  </style>
@endpush

@push('scripts')
  <!-- scripts push-->
  <script src="../global_assets/js/plugins/ui/moment/moment.min.js"></script>
  <script src="../global_assets/js/plugins/pickers/anytime.min.js"></script>

  <script src="../assets/js/custom_datepicker.js"></script>

  <!-- Load select2 -->
  <script type="text/javascript" src="../global_assets/js/plugins/forms/selects/select2.min.js"></script>
  <script type="text/javascript" src="../global_assets/js/plugins/extensions/jquery_ui/interactions.min.js"></script>
  <script src="../assets/js/custom_select2.js"></script>

  <script>



  /*$('form').on('focus', 'input[type=number]', function (e) {
  $(this).on('wheel.disableScroll', function (e) {
  e.preventDefault()
  })
  })
  $('form').on('blur', 'input[type=number]', function (e) {
  $(this).off('wheel.disableScroll')
  })*/
  </script>

  <!-- Load validation scripts
  https://www.webslesson.info/2018/09/client-side-form-validation-using-parsleyjs-with-php-ajax.html
  -->
  {{--<script src="../assets/js/parsley.min.js"></script>--}}
  {{--<script src="../assets/js/form_validation.js"></script>--}}
  <!-- /scripts push-->
@endpush

@section('content')
  <div class="title m-b-md mt-3">
    <h1 style="text-align: center;">Kreiranje SBB naloga</h1>
  </div>

  @if ($errors->any())
    <div class="alert alert-danger">
        @foreach ($errors->all() as $error)
          <div>- {{ $error }}</div>
        @endforeach
    </div>
  @endif

<div class="card mt-1">

  <div class="card-header header-elements-inline">
    <h4 class="card-title">Osnovni podaci naloga</h4>

    <div class="header-elements">
      <div class="list-icons">
        <a class="list-icons-item" data-action="collapse"></a>
      </div>
    </div>
  </div>

  <div class="card-body">
    {!! Form::open(['action' => 'OrdersController@store_with_status', 'method' => 'POST', 'id' => 'create_sbb_order', 'data-parsley-validate' => '']) !!}
      <div class="row">

        <div class="col-12">
          <fieldset>
            <legend class="font-weight-semibold"><i class="icon-truck mr-2"></i> Podaci o nalogu</legend>

              <div class="row">
                  <p style="font-size: 14px" class="ml-2 mb-3">Redni broj naloga: <span style="font-weight: bold;">{{ newSbbOrdinalNumber() }}</span></p>
                {{--{{Form::hidden ('ordinal_number', '18-03-00134')}}--}}
               {{-- {{Form::hidden ('order_type', 'sbb_order')}}--}}
               {{-- {{Form::hidden ('order_status', 1)}}--}}
              </div>

            <div class="row">

              {{ Form::textGroup2('order_number', 'Broj naloga:', null, null, 'form-control', ['placeholder' => 'Broj naloga', 'data-parsly-trigger'=> 'keyup'], 'col-3', true) }}

             {{-- 'name', 'labelvalue', 'classlabel', 'value' => null, 'classtext', 'attributes' => ['required' => 'required'], 'col'--}}
              {{ Form::datetime_picker('opened_at_datetime', 'Datum i vreme prijave:', null, null, 'form-control', ['placeholder' => 'Izaberi datum'], 'col-4', true) }}

             {{-- {{ Form::datetime_picker('started_at_datetime', 'Predvidjeni pocetak rada:', null, null, 'form-control', ['placeholder' => 'Izaberi datum'], 'col-4') }}--}}


            </div>


            <div class="row">

                {{ Form::textGroup2('buyer_id', 'Broj kupca:', null, null, 'form-control', ['placeholder' => 'Broj kupca'], 'col-3') }}

                {{ Form::textGroup2('treaty_id', 'Broj ugovora:', null, null, 'form-control', ['placeholder' => 'Broj ugovora'], 'col-3') }}


                {{ Form::textGroup2('buyer', 'Ime i prezime kupca:', null, null, 'form-control', ['placeholder' => 'Ime i prezime kupca'], 'col-6', true) }}

              </div>

            {{-- POST GRAD Opština --}}
            {{-- U redu tri inputa sa selectom--}}
            <div class="row">

              {{ Form::textGroup2('area_code', 'Poštanski broj:', null, null, 'form-control', ['placeholder' => 'Poštanski broj'], 'col-3') }}


              {{ Form::textGroup2('city', 'Grad:', null, 'Beograd', 'form-control', ['readonly'], 'col-3', true) }}



              {{ Form::selectGroupSearch('region', true, 'Region:', null,
              $select['region'], null,
                 ['data-placeholder' => 'Odaberite region',
                 'class'=> 'form-control select-search township'], 'col-3', true) }}



              {{ Form::selectGroupSearch('township', true, 'Odaberite opštinu:', null,
              $select['township'], null,
               ['data-placeholder' => 'Odaberite opštinu',
               'class'=> 'form-control select-search township'], 'col-3', true) }}

            </div>
            {{-- POST GRAD Opština --}}

            {{-- ADRESA --}}
            {{-- U redu cetiri inputa --}}
            <div class="row">

              {{ Form::textGroup2('address', 'Adresa:', null, null, 'form-control', ['placeholder' => 'Naziv ulice'], 'col-6', true) }}

              {{ Form::textGroup2('address_number', 'broj:', null, null, 'form-control', ['placeholder' => 'broj'], 'col-2', true) }}

              {{ Form::textGroup2('floor', 'sprat:', null, null, 'form-control', ['placeholder' => 'sprat'], 'col-2') }}

              {{ Form::textGroup2('apartment', 'stan:', null, null, 'form-control', ['placeholder' => 'stan'], 'col-2') }}

            </div>
            {{-- /ADRESA --}}

            {{-- TELEFONI --}}
            {{-- U redu dva inputa --}}
            <div class="row">

              {{ Form::textGroup2('phone_number', 'Broj telefona:', null, null, 'form-control', ['placeholder' => 'Broj telefona'], 'col-6', true) }}

              {{ Form::textGroup2('mobile_number', 'Mobilni:', null, null, 'form-control', ['placeholder' => 'Mobilni'], 'col-6') }}

            </div>
            {{-- /TELEFONI --}}

          </fieldset>
        </div>

          </fieldset>
        </div>
      </div>

    {{--</form>--}}
  </div>


{{--   ************************************************************************************************************************************** --}}
<div class="card">

<div class="card-header header-elements-inline">
    <h4 class="card-title">Opis radova i podaci o opremi - planirano</h4>
    <div class="header-elements">
      <div class="list-icons">
        <a class="list-icons-item" data-action="collapse"></a>
       {{-- <a class="list-icons-item" data-action="reload"></a>--}}
        {{--  NE FUNKCIONISE RELOAD DUGME --}}
      </div>
    </div>
  </div>

<div class="card-body">

      <div class="row">

        {{-- CELA STRANA --}}
        <div class="col-md-12">
          <fieldset>
            <legend class="font-weight-semibold"><i class="{{-- NASE IKONICE --}}"></i> Opis radova</legend>

            {{-- OPIS RADOVA --}}
            {{-- U redu tri SELECTA sa pet redova --}}
            <div class="row">

                {{ Form::selectGroupSearch('service_type', true ,'Tip posla:', null,
               $select['services'], null,
                ['data-placeholder' => 'Odaberite tip posla',
                'class'=> 'form-control select-search'], 'form-group col-5', true) }}


            </div>
            {{-- OPIS RADOVA --}}
              <div class="row">
                <div class="col-6">

                    <legend class="font-weight-semibold"><i class="{{-- NASE IKONICE --}}"></i> Aktivnosti na novoj opremi</legend>

            {{-- AKTIVNOSTI NA NOVOJ OPREMI --}}
            {{-- U redu tri SELECTA sa pet redova --}}
                  <div class="col-11">

                    {{ Form::selectGroupSearch('m_eq_type1', true ,'Tip opreme:', null,
                    $select['subtype'], null,
                     ['data-placeholder' => 'Odaberite tip opreme',
                     'class'=> 'form-control select-search',
                     'data-fouc'], 'form-row mb-2') }}


                    {{ Form::selectGroupSearch('m_eq_type2', false , null, null,
                   $select['subtype'], null,
                    ['data-placeholder' => 'Odaberite tip opreme',
                    'class'=> 'form-control select-search',
                    'data-fouc'], 'form-row mb-2') }}

                    {{ Form::selectGroupSearch('m_eq_type3', false , null, null,
                   $select['subtype'], null,
                    ['data-placeholder' => 'Odaberite tip opreme',
                    'class'=> 'form-control select-search',
                    'data-fouc'], 'form-row mb-2') }}

                    {{ Form::selectGroupSearch('m_eq_type4', false , null, null,
                   $select['subtype'], null,
                    ['data-placeholder' => 'Odaberite tip opreme',
                    'class'=> 'form-control select-search',
                    'data-fouc'], 'form-row mb-2') }}

                    {{ Form::selectGroupSearch('m_eq_type5', false , null, null,
                   $select['subtype'], null,
                    ['data-placeholder' => 'Odaberite tip opreme',
                    'class'=> 'form-control select-search',
                    'data-fouc'], 'form-row mb-2') }}

                  </div>

              </div>
              {{-- AKTIVNOSTI NA NOVOJ OPREMI --}}
              <div class="col-6">
              <legend class="font-weight-semibold"><i class="{{-- NASE IKONICE --}}"></i> Aktivnosti na postojecoj opremi</legend>

              {{-- AKTIVNOSTI NA POSTOJECOJ OPREMI --}}


                <div class="col-11">

                  {{ Form::selectGroupSearch('d_eq_type1', true ,'Tip opreme:', null,
                  $select['dismantled_subtype'], null,
                   ['data-placeholder' => 'Odaberite tip opreme',
                   'class'=> 'form-control select-search',
                   'data-fouc'], 'form-row mb-2') }}


                  {{ Form::selectGroupSearch('d_eq_type2', false , null, null,
                 $select['dismantled_subtype'], null,
                  ['data-placeholder' => 'Odaberite tip opreme',
                  'class'=> 'form-control select-search',
                  'data-fouc'], 'form-row mb-2') }}

                  {{ Form::selectGroupSearch('d_eq_type3', false , null, null,
                 $select['dismantled_subtype'], null,
                  ['data-placeholder' => 'Odaberite tip opreme',
                  'class'=> 'form-control select-search',
                  'data-fouc'], 'form-row mb-2') }}

                  {{ Form::selectGroupSearch('d_eq_type4', false , null, null,
                 $select['dismantled_subtype'], null,
                  ['data-placeholder' => 'Odaberite tip opreme',
                  'class'=> 'form-control select-search',
                  'data-fouc'], 'form-row mb-2') }}

                  {{ Form::selectGroupSearch('d_eq_type5', false , null, null,
                 $select['dismantled_subtype'], null,
                  ['data-placeholder' => 'Odaberite tip opreme',
                  'class'=> 'form-control select-search',
                  'data-fouc'], 'form-row mb-2') }}

                </div>
            </div>
                {{-- /AKTIVNOSTI NA POSTOJECOJ OPREMI --}}
            </div>

            {{-- KOMENTAR --}}

            {{ Form::textareaGroup('order_note', 'Komentar:', null, null, 'form-control', ['placeholder' => 'Upisite dodatne komentare', 'rows' => '5', 'cols' => '5'], 'col-12') }}


            {{--<div class="form-group">
              <label>Komentar:</label>
              <textarea rows="5" cols="5" id="order_note" class="form-control" placeholder="Upisite dodatne komentare"></textarea>
            </div>--}}
            {{-- /KOMENTAR --}}

          </fieldset>
        </div>
      </div>

        <div class="text-right">
        <button type="submit" class="btn bg-teal-300">Kreiraj nalog <i class="icon-paperplane ml-2"></i></button>
      </div>

    </form>
  </div>
</div>
{{--   ************************************************************************************************************************************** --}}

@endsection