@extends('layouts.app')

@section('content')


    <div class="flex-center position-ref full-height">
        <div class="content">
            <div class="title m-b-md">
                Nov nalog
            </div>
        </div>


<!--  ****************   GLAVNA FORMA 1  ***********************  -->
<div class="inputforma">

  <div class="inputformaleft">
    <h4>Korisnicki podaci</h4>
      <form>
<!-- Extended default form grid -->

    <!-- Grid row -->
    <div class="form-row">
      <!-- Default input -->
      <div class="form-group col-md-6">
        <label for="order_id">Broj ugovora:</label>
        <input type="text" class="form-control" id="order_id" placeholder="Broj ugovora">
      </div>
      <!-- Default input -->
      <div class="form-group col-md-6">
          <label for="term_datetime">broj kupca:</label>
          <input type="text" class="form-control" id="term_datetime" placeholder="broj kupca">
        </div>
    </div>
    <!-- Grid row -->

    <!-- Grid row -->
    <div class="form-row">
        <!-- Default input -->
        <div class="form-group col-md-12">
          <label for="order_id">Korisnik:</label>
          <input type="text" class="form-control" id="order_id" placeholder="Puno ime i prezime">
        </div>
      </div>
      <!-- Grid row -->

    <!-- Grid row -->
    <div class="form-row">
        <!-- Default input -->
        <div class="form-group col-md-6">
          <label for="inputCity">Grad</label>
          <input type="text" class="form-control" id="inputCity" placeholder="Beograd">
        </div>
        <!-- Default input -->
        <div class="form-group col-md-6">
          <label for="inputZip">Postanski broj</label>
          <input type="text" class="form-control" id="inputZip" placeholder="11200">
        </div>
      </div>
      <!-- Grid row -->
  
    <!-- Default input -->
    <div class="form-row">
      <div class="form-group col-md-6">
      <label for="inputAddress">adresa</label>
      <input type="text" class="form-control" id="inputAddress" placeholder="Kajuhova 123">
    </div>
     <div class="form-group col-md-2">
      <label for="inputAddress">broj</label>
      <input type="text" class="form-control" id="inputAddress" placeholder="12">
  </div>
  <div class="form-group col-md-2">
      <label for="inputAddress">broj sprata</label>
      <input type="text" class="form-control" id="inputAddress" placeholder="drugi sprat">
    </div>
  <div class="form-group col-md-2">
      <label for="inputAddress">broj stana</label>
      <input type="text" class="form-control" id="inputAddress" placeholder="15">
    </div>
  </div>

    <div class="form-row">
        <!-- Default input -->
        <div class="form-group col-md-6">
          <label for="order_id">Telefon:</label>
          <input type="text" class="form-control" id="order_id" placeholder="Telefon">
        </div>
      <!-- Grid row -->
    <div class="form-group col-md-6">
      <label for="order_id">Mobilni telefon:</label>
      <input type="text" class="form-control" id="order_id" placeholder="Mobilni telefon">
    </div>
  </div>
  <!-- Grid row --> 

  </form>
  </div>
    <!-- Grid row --> 


        
  <!-- Extended default form grid -->    

<!-- Default column sizing form /*****************************************************-->

    <!-- Grid row -->
    <div class="inputformaright">
        <h4>Podaci naloga</h4>
      <form>

    <div class="form-row">
      <!-- Grid column -->
      <div class="col">
        <!-- Default input -->
        <label for="order_id">broj naloga:</label>
        <input type="text" class="form-control" placeholder="upisite broj naloga">
      </div>
    </div>
      <!-- Grid column -->

    <div class="form-row">
        <!-- Grid column -->
        <div class="col">
          <!-- Default input -->
          <label for="order_id">datum i vreme prijave:</label>
          <input type="text" class="form-control" placeholder="odaberite vreme i datum prijave">
        </div>
      </div>
        <!-- Grid column -->

    <div class="form-row">
        <!-- Grid column -->
        <div class="col">
          <!-- Default input -->
          <label for="order_id">NALOGODAVAC:</label>
          <input type="text" class="form-control" placeholder="upisite nalogodavca">
        </div>
      </div>
        <!-- Grid column -->

    <div class="form-row">
        <!-- Grid column -->
        <div class="col">
          <!-- Default input -->
          <label for="order_id">SERVISNI TIM:</label>
          <input type="text" class="form-control" placeholder="odaberite servisni tim">
        </div>
      </div>
        <!-- Grid column -->  

    </form>
    </div>
    <!-- Grid row -->
  </div>


<!--   KRAJ FORMA 1 **************************************************   -->

<div class="inputforma2 inputformaleft" style="width: 97%">

      <form>
<!-- Extended default form grid -->
    <h5 class="h5order">OPREMA KOD KORISNIKA</h5>
    <!-- Grid row OPREMA KOD KORISNIKA-->
    <div class="form-row">
      <!-- Default input -->
      <div class="form-group col-md-4">
        <label>Naziv opreme:</label>
        <input type="text" class="form-control" id="xxx" placeholder="naziv opreme">
        <input type="text" class="form-control" id="xxx" placeholder="naziv opreme">
        <input type="text" class="form-control" id="xxx" placeholder="naziv opreme">
        <input type="text" class="form-control" id="xxx" placeholder="naziv opreme">
        <input type="text" class="form-control" id="xxx" placeholder="naziv opreme">
      </div>
      <!-- Default input -->
      <div class="form-group col-md-4">
          <label >Serijski broj:</label>
          <input type="text" class="form-control" id="xxx" placeholder="serijski broj">
          <input type="text" class="form-control" id="xxx" placeholder="serijski broj">
          <input type="text" class="form-control" id="xxx" placeholder="serijski broj">
          <input type="text" class="form-control" id="xxx" placeholder="serijski broj">
          <input type="text" class="form-control" id="xxx" placeholder="serijski broj">
        </div>
              <!-- Default input -->
      <div class="form-group col-md-4">
          <label >MAC adresa:</label>
          <input type="text" class="form-control" id="xxx" placeholder="MAC">
          <input type="text" class="form-control" id="xxx" placeholder="MAC">
          <input type="text" class="form-control" id="xxx" placeholder="MAC">
          <input type="text" class="form-control" id="xxx" placeholder="MAC">
          <input type="text" class="form-control" id="xxx" placeholder="MAC">
        </div>
    </div>

    <hr>
    <h5 class="h5order">OPIS RADOVA</h5>
    <!-- Grid row OPIS RADOVA -->
    <div class="form-row">
    <!-- Default input -->
      <!-- Default input -->
      <div class="form-group col-md-2">
          <label>Stavka:</label>
          <input type="text" class="form-control" id="xxx" placeholder="Stavka">
          <input type="text" class="form-control" id="xxx" placeholder="Stavka">
        </div>     
      <div class="form-group col-md-4">
          <label>Usluga:</label>
          <input type="text" class="form-control" id="xxx" placeholder="Usluga">
          <input type="text" class="form-control" id="xxx" placeholder="Usluga">
        </div>
      <div class="form-group col-md-6">
          <label>Ime/Tip posla:</label>
          <input type="text" class="form-control" id="xxx" placeholder="Ime/Tip posla">
          <input type="text" class="form-control" id="xxx" placeholder="Ime/Tip posla">
        </div>
    </div>

    <hr>
    <h4 class="h4order">AKTIVNOSTI NA NOVOJ OPREMI</h4>
    <!-- Grid row OPIS RADOVA -->
    <div class="form-row">
    <!-- Default input -->
      <!-- Default input -->
      <div class="form-group col-md-3">
          <label>Postavka:</label>
          <input type="text" class="form-control" id="xxx" placeholder="Postavka">
          <input type="text" class="form-control" id="xxx" placeholder="Postavka">
          <input type="text" class="form-control" id="xxx" placeholder="Postavka">
          <input type="text" class="form-control" id="xxx" placeholder="Postavka">
          <input type="text" class="form-control" id="xxx" placeholder="Postavka">
        </div>     
      <div class="form-group col-md-4">
          <label>Tip opreme:</label>
          <input type="text" class="form-control" id="xxx" placeholder="Tip opreme">
          <input type="text" class="form-control" id="xxx" placeholder="Tip opreme">
          <input type="text" class="form-control" id="xxx" placeholder="Tip opreme">
          <input type="text" class="form-control" id="xxx" placeholder="Tip opreme">
          <input type="text" class="form-control" id="xxx" placeholder="Tip opreme">
        </div>
      <div class="form-group col-md-5">
          <label>Opis aktivnosti:</label>
          <input type="text" class="form-control" id="xxx" placeholder="Opis aktivnosti">
          <input type="text" class="form-control" id="xxx" placeholder="Opis aktivnosti">
          <input type="text" class="form-control" id="xxx" placeholder="Opis aktivnosti">
          <input type="text" class="form-control" id="xxx" placeholder="Opis aktivnosti">
          <input type="text" class="form-control" id="xxx" placeholder="Opis aktivnosti">
        </div>
    </div>


    <hr>
    <h4 class="h4order">AKTIVNOSTI NA POSTOJECOJ OPREMI</h4>
    <!-- Grid row OPIS RADOVA -->
    <div class="form-row">
    <!-- Default input -->
      <!-- Default input -->
      <div class="form-group col-md-3">
          <label>Vrsta opreme:</label>
          <input type="text" class="form-control" id="xxx" placeholder="Vrsta opreme">
          <input type="text" class="form-control" id="xxx" placeholder="Vrsta opreme">
          <input type="text" class="form-control" id="xxx" placeholder="Vrsta opreme">
          <input type="text" class="form-control" id="xxx" placeholder="Vrsta opreme">
          <input type="text" class="form-control" id="xxx" placeholder="Vrsta opreme">
        </div>     
      <div class="form-group col-md-4">
          <label>Tip opreme:</label>
          <input type="text" class="form-control" id="xxx" placeholder="Tip opreme">
          <input type="text" class="form-control" id="xxx" placeholder="Tip opreme">
          <input type="text" class="form-control" id="xxx" placeholder="Tip opreme">
          <input type="text" class="form-control" id="xxx" placeholder="Tip opreme">
          <input type="text" class="form-control" id="xxx" placeholder="Tip opreme">
        </div>
      <div class="form-group col-md-5">
          <label>Opis aktivnosti:</label>
          <input type="text" class="form-control" id="xxx" placeholder="Opis aktivnosti">
          <input type="text" class="form-control" id="xxx" placeholder="Opis aktivnosti">
          <input type="text" class="form-control" id="xxx" placeholder="Opis aktivnosti">
          <input type="text" class="form-control" id="xxx" placeholder="Opis aktivnosti">
          <input type="text" class="form-control" id="xxx" placeholder="Opis aktivnosti">
        </div>
    </div>
    </div>
  </form>
</div>


    </div>
    <br>
    <br>

    <div class="btnsuccess">
        <button type="submit" class="btn btn-success btn-md">Potvrdi nalog</button>
    </div>





@endsection



