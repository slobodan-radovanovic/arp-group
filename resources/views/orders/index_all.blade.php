@extends('layouts.app')

@push('style')
    <style>
        .nav-pills-bordered .nav-link.active {
            border-color: #4db6ac !important;
        }

        .nav-pills .nav-link.active, .nav-pills .show > .nav-link {
            color: #fff;
            background-color: #4db6ac !important;
        }


    </style>

    <!-- Global stylesheets -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet"
          type="text/css">
    <link href="../../../../global_assets/css/icons/icomoon/styles.css" rel="stylesheet" type="text/css">
    <link href="../assets/css/bootstrap.min.css" rel="stylesheet" type="text/css">
    <link href="../assets/css/bootstrap_limitless.min.css" rel="stylesheet" type="text/css">
    <link href="../assets/css/layout.min.css" rel="stylesheet" type="text/css">
    <link href="../assets/css/components.min.css" rel="stylesheet" type="text/css">
    <link href="../assets/css/colors.min.css" rel="stylesheet" type="text/css">
    <!-- /global stylesheets -->
@endpush
@push('scripts')

    <script src="../global_assets/js/plugins/tables/datatables/datatables.min.js"></script>
    <script src="../global_assets/js/demo_pages/datatables_basic.js"></script>
    <!-- Load select2 -->
    <script type="text/javascript" src="../global_assets/js/plugins/forms/selects/select2.min.js"></script>
    <script type="text/javascript" src="../global_assets/js/plugins/extensions/jquery_ui/interactions.min.js"></script>
    <script src="../assets/js/custom_select2.js"></script>
    <script src="../global_assets/js/plugins/forms/selects/select2.min.js"></script>
    {{--anytime--}}
    <script src="../global_assets/js/plugins/ui/moment/moment.min.js"></script>
    <script src="../global_assets/js/plugins/pickers/anytime.min.js"></script>
    <script src="../../../../global_assets/js/plugins/pickers/daterangepicker.js"></script>
    <script src="../assets/js/custom_datepicker.js"></script>

    <script src="../global_assets/js/main/jquery.min.js"></script>
    <script src="../global_assets/js/main/bootstrap.bundle.min.js"></script>
    <script src="../global_assets/js/plugins/loaders/blockui.min.js"></script>
    <script src="../global_assets/js/plugins/tables/datatables/datatables.min.js"></script>
    <script src="../global_assets/js/plugins/forms/selects/select2.min.js"></script>
    <script src="../global_assets/js/plugins/tables/datatables/extensions/jszip/jszip.min.js"></script>
    <script src="../global_assets/js/plugins/tables/datatables/extensions/buttons.min.js"></script>

    {{--export datatable--}}

    {{--anytime--}}
    <script src="../global_assets/js/plugins/ui/moment/moment.min.js"></script>
    <script src="../global_assets/js/plugins/pickers/anytime.min.js"></script>
    <script src="../../../../global_assets/js/plugins/pickers/daterangepicker.js"></script>
    <script src="../assets/js/custom_datepicker.js"></script>



    <script>

        var today = new Date();
        var dd = String(today.getDate()).padStart(2, '0');
        var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
        var yyyy = today.getFullYear();

        today = dd + '-' + mm + '-' + yyyy;
        console.log(today);


       /* $('.datatable-basic1').DataTable({
            autoWidth: false,
            columnDefs: [{
                width: 10,
                targets: [ 7 ]
            }],
            dom: '<"datatable-header"fBl><"datatable-scroll-wrap"t><"datatable-footer"ip>',
            language: {
                search: '<span>Pretraga:</span> _INPUT_',
                lengthMenu: '<span>Prikaz:</span> _MENU_',
                info: '_START_ do _END_ od ukupno _TOTAL_',
                paginate: {'first': 'First', 'last': 'Last', 'next': '→', 'previous': '←'}
            },
            buttons: {
                buttons: [
                    {
                        extend: 'excelHtml5',
                        className: 'btn btn-light',
                        title: 'SBB nalozi (Aktivni nalozi) ' + today,
                        sheetName: 'SBB nalozi',
                        exportOptions: {
                            columns: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
                        }
                    }
                ]
            }
        });*/


       /* $('.daterange-basic').daterangepicker({
            @if( empty($date_filter))
            startDate: moment().subtract(1, 'month').startOf('month'),
            endDate: moment().subtract(1, 'month').endOf('month'),
            @endif
            maxDate: moment(),
            locale: {
                format: 'DD/MM/YYYY'
            },
            applyClass: 'bg-teal-300',
            cancelClass: 'btn-light',

        });*/

        var table2 =  $('.datatable-basic2').DataTable({
            autoWidth: false,
            processing: true,
            dom: '<"datatable-header"fBl><"datatable-scroll-wrap"t><"datatable-footer"ip>',
            ajax: {
                url:"{{ route('svi_nalozi') }}"
            },
            columns:[

                {
                    data: 'ordinal_number',
                    name: 'ordinal_number'
                },
                {
                    data: 'order_number',
                    name: 'order_number'
                },
                {
                    data: 'buyer',
                    name: 'buyer'
                },
                {
                    data: 'township',
                    name: 'township'
                },
                {
                    data: 'region',
                    name: 'region'
                },
                {
                    data: 'address',
                    name: 'address'
                },
                {
                    data: 'service_name',
                    name: 'service_name'
                },
                {
                    data: 'note',
                    name: 'note'
                },
                {
                    data: 'order_updated_by',
                    name: 'order_updated_by'
                },
                {
                    data: 'opened_at',
                    name: 'opened_at'
                },
                {
                    data: 'order_status',
                    name: 'order_status'
                },
                {
                    data: 'action',
                    name: 'action'
                }
            ],
            columnDefs: [
                {
                    targets: 5,
                    render: function ( data, type, row ) {
                        return data +' '+ row.address_number;
                    }
                },
                {
                    targets: -1,
                    data: null,
                    defaultContent: '<button class="btn btn-outline-dark">Prikaži nalog</button>'
                }
            ],
            lengthMenu: [
                [ 10, 25, 50, 100, -1 ],
                [ '10', '25', '50', '100', 'Show all' ]
            ],
            language: {
                search: '<span>Pretraga:</span> _INPUT_',
                lengthMenu: '<span>Prikaz:</span> _MENU_',
                info: '_START_ do _END_ od ukupno _TOTAL_',
                paginate: {'first': 'First', 'last': 'Last', 'next': '→', 'previous': '←'}
            },
            buttons: {
                buttons: [
                    {
                        extend: 'excelHtml5',
                        className: 'btn btn-light',
                        title: 'Pregled svih naloga '+today,
                        sheetName:'Pregled svih naloga',
                        exportOptions: {
                            columns: [ 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 ]
                        }
                    }
                ]
            }
        });


       /* console.log($('.datatable-basic2 tbody'));*/
        $('.datatable-basic2 tbody').on( 'click', 'button', function () {
           data = table2.row( $(this).parents('tr') ).data();
           console.log(data.order_id);

            var hostname = window.location.hostname;
            /*console.log(site['0']);*/
            /*console.log(window.location.hostname);*/
            /*return re.exec(window.location.href);*/

            window.location = "/orders/" + data.order_id + "/show";

            /*console.log(data);*/
            /*alert( data['equipment_id'] );*/
        } );


      /*  $('.datatable-basic2 tbody').on( 'click', 'button', function () {
            var data = table.row( $(this).parents('tr') ).data();
            console.log('prikazi nalog');
        } );*/





        // hide all form-duration-divs
        /*$('.order_details').hide();*/
        {{--@if( !empty($date_filter))
        $(".daterangecheckbox").hide();
        $(".daterange").show();
        @endif--}}


     {{--   $(".daterangecheckbox").change(function () {
            $(".daterangecheckbox").hide(500);

            $(".daterange").show();
            console.log('datarange');
        });--}}

    </script>
@endpush

@section('content')
   {{-- @if(isset($orders['region_filter']))
        @dd($orders['region_filter'])
    @endif--}}
    <div>
        <div class="content">
            <div class="title m-b-md">
                <h1 style="text-align: center;">Pregled SBB naloga</h1>
            </div>

            {{--<div class="card mb-1">
                <div class="form-group card-body  mb-0">
                    {!! Form::open(['action' => 'OrdersController@index_all', 'method' => 'POST']) !!}
                    <div class="daterangecheckbox">
                        <label>Filter za period: </label>
                        <input type="checkbox">
                    </div>

                    <div class="row daterange" style="display: none">

                        <div class="input-group col-4" --}}{{--style="display: block !important;"--}}{{-->

                            <span class="input-group-text"><i class="icon-calendar3"></i></span>
                            <input name="date_filter" style="width:200px" type="text"
                                   class="form-control daterange-basic" data-placeholder="Izaberite period"
                                   --}}{{--value="20/03/2019 - 22/03/2019"--}}{{--
                                   @if( ! empty($date_filter))
                                   value="{{$date_filter}}"
                                    @endif
                            >
                        </div>

                        <div class="col"></div>
                        --}}{{--@if(isset($orders['region_filter']))

                            {{ Form::selectGroupSearch('region', false, 'Region:', null,
                  $orders['region'], $orders['region_filter'],
                     ['data-placeholder' => 'Filter za region',
                     'class'=> 'form-control select-search region'], 'col-2') }}

                        @else

                            {{ Form::selectGroupSearch('region', false, 'Region:', null,
                  $orders['region'], null,
                     ['data-placeholder' => 'Filter za region',
                     'class'=> 'form-control select-search region'], 'col-2') }}

                        @endif--}}{{--


                        <button type="submit" class="btn bg-teal-300 ml-3 col-2">Primeni filtere</button>
                        <div class="col-1"></div>
                        <a class="btn bg-teal-300 ml-3 col-2" href="/all_orders">Poništi filtere</a>
                    </div>

                </div>
            </div>

            {!! Form::close() !!}
            <div>--}}
            {{--<button type="button" class="btn bg-teal-300" data-toggle="modal" data-target="#modal_add">Dodavanje nove opreme <i class="icon-plus3 ml-1"></i></button>--}}

            <!-- Bordered pills -->
                <div class="card">

                    <div class="card-body" style="border-bottom: 1px solid #bbbfc3;">


                           {{-- <table class="table datatable-basic1 table-bordered table-striped">
                                <thead>
                                <tr>
                                    <th>Redni broj naloga</th>
                                    <th>Broj naloga</th>
                                    <th>Ime i preizme</th>
                                    <th>Opština</th>
                                    <th>Region</th>
                                    <th>Adresa</th>
                                    <th>Tip posla</th>
                                    <th>Napomena</th>
                                    <th>Poslednji izmenio</th>
                                    <th>Datum i vreme prijave</th>
                                    <th>Status naloga</th>
                                    <th></th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($orders['all_orders'] as $order)
                                    <tr>
                                        <td><a class="text-secondary" href="orders/{{ $order->order_id }}/show">{{ $order->ordinal_number }}</a></td>
                                        <td>{{ $order->order_number }}</td>
                                        <td>{{ $order->buyer }}</td>
                                        <td>{{ $order->township }}</td>
                                        <td>{{ $order->region }}</td>
                                        <td>{{ $order->address }} {{ $order->address_number }}</td>
                                        <td>{{ $order->service->service_name }}</td>
                                        <td>{{ $order->note }}</td>
                                        <td>{{ $order->order_updated_by }}</td>
                                        <td>{{ datetimeForView($order->opened_at) }}</td>
                                        <td class="text-center">
                                            @if($order->order_status=='Otvoren')
                                                <span class="badge badge-success p-2"
                                                      style="font-size: 12px;">Otvoren</span></td>
                                        @elseif($order->order_status=='Zakazan')
                                            <span class="badge badge-primary p-2"
                                                  style="font-size: 12px;">Zakazan</span></td>
                                        @elseif($order->order_status=='Nekompletan')
                                            <span class="badge badge-danger p-2"
                                                  style="font-size: 12px;">Nekompletan</span></td>
                                        @elseif($order->order_status=='Odložen')
                                            <span class="badge badge-warning p-2"
                                                  style="font-size: 12px;">Odlozen</span></td>
                                        @elseif($order->order_status=='Završen')
                                            <span class="badge badge-dark p-2"
                                                  style="font-size: 12px;">Završen</span></td>
                                        @elseif($order->order_status=='Otkazan')
                                            <span class="badge badge-secondary p-2"
                                                  style="font-size: 12px;">Otkazan</span></td>
                                        @elseif($order->order_status=='Nerealizovan')
                                            <span class="badge badge-info p-2"
                                                  style="font-size: 12px;">Nerealizovan</span></td>
                                        @endif
                                        <td><a class="btn btn-outline-dark" href="orders/{{ $order->order_id }}/show"
                                               role="button">Prikaži nalog</a>
                                        </td>
                                    </tr>
                                @endforeach


                                </tbody>
                            </table>--}}

                            <!-- /Prikaz svih naloga datatable -->

                        <table class="table datatable-basic2 table-bordered table-striped">

                            <thead>
                            <tr>
                                <th>Redni broj naloga</th>
                                <th>Broj naloga</th>
                                <th>Ime i preizme</th>
                                <th>Opština</th>
                                <th>Region</th>
                                <th>Adresa</th>
                                <th>Tip posla</th>
                                <th>Napomena</th>
                                <th>Poslednji izmenio</th>
                                <th>Datum i vreme prijave</th>
                                <th>Status naloga</th>
                                <th></th>
                            </tr>
                            </thead>
                        </table>

                     </div>



            </div>
        </div>
    </div>

@endsection



