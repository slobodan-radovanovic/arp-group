@extends('layouts.app')



@push('scripts')


    <script type="text/javascript" src="{{ asset('global_assets/js/plugins/ui/moment/moment.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('global_assets/js/plugins/pickers/anytime.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/custom_datepicker.js') }}"></script>
    <!-- Load select2 -->
    <script type="text/javascript" src="{{ asset('global_assets/js/plugins/forms/selects/select2.min.js') }}"></script>
    <script type="text/javascript"
            src="{{ asset('global_assets/js/plugins/extensions/jquery_ui/interactions.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/custom_select2.js') }}"></script>

    <script>
        $('form').on('focus', 'input[type=number]', function (e) {
            $(this).on('wheel.disableScroll', function (e) {
                e.preventDefault()
            })
        });

        $('form').on('blur', 'input[type=number]', function (e) {
            $(this).off('wheel.disableScroll')
        });
    </script>

    <script>
        var json_dismantled_eq_select = {!! $data['dismantled_eq_for_select'] !!};


        function filterDismantledEquipment(key, value) {
            return json_dismantled_eq_select.filter(function (equipment) {
                return equipment[key] == value;
            });
        } // filtriranje demontirane opreme na osnovu tipa (subtype)


        var start_status = $("#order_status option:selected").val();
        var start_scheduled_datetime = $("#scheduled_at_datetime").val();
        var start_order_comment = $("#order_comment").val();

        function onStatusChange() {
            // hide all form-duration-divs
            /*$('.order_details').hide();*/
            var status = $("#order_status option:selected").val();

            if (status == 'Otvoren' || status == 'Zakazan' || status == 'Odložen' || status == 'Otkazan' || status == 'Nekompletan') {
                $(".order_details").hide();
                $(".save_changes").show();
                console.log(status);
            } else {
                $(".order_details").show();
                $(".save_changes").show();
                console.log(status);
            }

            if (start_status == status) {
                $(".save_changes").hide();
            }

            if (status == 'Zakazan') {
                $(".div_status").show();
                if (start_scheduled_datetime != $("#scheduled_at_datetime").val()) {
                    $(".save_changes").show();
                }
            } else {
                $(".div_status").hide();
            }

            /* Otvoren, Zakazan, Odložen, Otkazan, Nerealizovan, Nekompletan, Završen*/

        }


        $("#order_status").change(function () {
            setTimeout(function () {
                onStatusChange();
                checkCommentChange();
            }, 600);
        });

        $("#scheduled_at_datetime").change(function () {
            $(".save_changes").show();
        });

        function checkCommentChange() {
            if (start_order_comment != $("#order_comment").val()) {
                $(".save_changes").show();
            }
        }

        $("#order_comment").keyup(function () {
            if (start_order_comment != $("#order_comment").val()) {
                $(".save_changes").show();
            } else {
                $(".save_changes").hide();
            }
        });

        /* onStatusChange();*/

        /*if (start_status == "Otkazan") {
            $(".status_row").hide();
            $(".canceled").show();
        } else if(start_status == "Nerealizovan" || start_status == "Završen"){
            $(".status_row").hide();
            $(".order_details").hide();
            $(".unrealized").show();
        }*/


        //

        var json_equipment_select = {!! $data['equipment_for_select'] !!};
        var json_all_serials = {!! json_encode($data['all_serials']) !!};

        function filterEquipment(key, value) {
            return json_equipment_select.filter(function (equipment) {
                return equipment[key] == value;
            });
        } // filtriranje opreme na osnovu tipa (subtype)

        function selectSerial(serial, subtype) {
            var selected_subtype = $('#' + subtype + ' option:selected').val();
            var filtered_equipment = filterEquipment('equipment_subtype_id', selected_subtype);
            //equipment_serial select
            var filtered_equipment_serial = {};
            $.each(filtered_equipment, function (index) {
                filtered_equipment_serial[index] = this['equipment_id'];
            });
            var serial_html = '<option value="" selected="selected"></option>';
            $.each(json_all_serials, function (serial_key, serial_value) {
                $.each(filtered_equipment_serial, function (key, equipment_id) {
                    var split_key = serial_key.split("_");
                    if (split_key['1'] == equipment_id) {
                        serial_html += '<option value="' + serial_key + '">' + serial_value + '</option>';
                    }
                });
            });
            $('#' + serial).html(serial_html);
        }

        $('#equipment_subtype_1').change(function () {
            selectSerial('equipment_serial_1', 'equipment_subtype_1');
        });

        $('#equipment_subtype_2').change(function () {
            selectSerial('equipment_serial_2', 'equipment_subtype_2');
        });

        $('#equipment_subtype_3').change(function () {
            selectSerial('equipment_serial_3', 'equipment_subtype_3');
        });

        $('#equipment_subtype_4').change(function () {
            selectSerial('equipment_serial_4', 'equipment_subtype_4');
        });

        $('#equipment_subtype_5').change(function () {
            selectSerial('equipment_serial_5', 'equipment_subtype_5');
        });


        function selectSubtype(serial, subtype) {
            var equipment_serial = $('#' + serial + ' option:selected').val();
            var slit_equipment_serial = equipment_serial.split("_");
            var equipment_id = slit_equipment_serial['1'];
            var filtered_equipment = filterEquipment('equipment_id', equipment_id);
            var equipment_subtype = filtered_equipment["0"].equipment_subtype_id;
            //equipment_subtype_1 select
            $('#' + subtype + ' option:selected').removeAttr("selected");
            $('#' + subtype + ' option[value="' + equipment_subtype + '"]').attr('selected', 'selected');
            var selected_subtype_text = $('#' + subtype + ' option:selected').text();
            $('#select2-' + subtype + '-container').prop('title', selected_subtype_text);
            $('#select2-' + subtype + '-container').prop('innerText', selected_subtype_text);
        } // selektovanje tipa na osnovu serijskog

        $('#equipment_serial_1').change(function () {
            selectSubtype('equipment_serial_1', 'equipment_subtype_1');
        });

        $('#equipment_serial_2').change(function () {
            selectSubtype('equipment_serial_2', 'equipment_subtype_2');
        });

        $('#equipment_serial_3').change(function () {
            selectSubtype('equipment_serial_3', 'equipment_subtype_3');
        });

        $('#equipment_serial_4').change(function () {
            selectSubtype('equipment_serial_4', 'equipment_subtype_4');
        });

        $('#equipment_serial_5').change(function () {
            selectSubtype('equipment_serial_5', 'equipment_subtype_5');
        });


        function notDefaultSelect(id) {
            var wares_id = $('#not_default_select_' + id + ' option:selected').val();
            // var slit_wares_value = wares_value.split("_");
            //var wares_id = slit_wares_value['1'];
            $('#not_default_input_' + id).attr('name', wares_id);
            console.log('notDefaultSelect is changed ' + id);
            console.log('wares id is ' + wares_id);
            console.log('not_default_input_' + id + 'name is ' + $('#not_default_input_' + id).attr('name'));
        }


        function selectDismantledModel(model, subtype) {

            var selected_subtype = $('#' + subtype + ' option:selected').val();
            console.log(selected_subtype);
            var filtered_equipment = filterDismantledEquipment('dismantled_eq_subtype_id', selected_subtype);

            //equipment_model_id select
            var filtered_equipment_model = {};
            $.each(filtered_equipment, function () {
                filtered_equipment_model[this['dismantled_eq_model_id']] = this['dismantled_eq_model_name'];
            });
            var model_html = '<option value="" selected="selected"> ';
            $.each(filtered_equipment_model, function (key, value) {
                if (key != 'null') {
                    model_html += '<option value="' + key + '">' + value + ' ';
                }
            });
            console.log(model_html);
            console.log('#' + model);
            $('#' + model).html(model_html);

        } // filter modela na osnovu subtype


        $('#dismantled_eq_subtype_id_1').change(function () {
            selectDismantledModel('dismantled_eq_model_id_1', 'dismantled_eq_subtype_id_1');
        });

        $('#dismantled_eq_subtype_id_2').change(function () {
            selectDismantledModel('dismantled_eq_model_id_2', 'dismantled_eq_subtype_id_2');
        });

        $('#dismantled_eq_subtype_id_3').change(function () {
            selectDismantledModel('dismantled_eq_model_id_3', 'dismantled_eq_subtype_id_3');
        });

        $('#dismantled_eq_subtype_id_4').change(function () {
            selectDismantledModel('dismantled_eq_model_id_4', 'dismantled_eq_subtype_id_4');
        });

        $('#dismantled_eq_subtype_id_5').change(function () {
            selectDismantledModel('dismantled_eq_model_id_5', 'dismantled_eq_subtype_id_5');
        });


        function selectDismantledSubtype(model, subtype) {

            var selected_model = $('#' + model + ' option:selected').val();

            var filtered_equipment = filterDismantledEquipment('dismantled_eq_model_id', selected_model);

            var equipment_subtype = filtered_equipment["0"].dismantled_eq_subtype_id;

            //equipment_subtype_id select
            $('#' + subtype + ' option:selected').removeAttr("selected");
            $('#' + subtype + ' option[value="' + equipment_subtype + '"]').attr('selected', 'selected');
            var selected_subtype_text = $('#' + subtype + ' option:selected').text();
            $('#select2-' + subtype + '-container').prop('title', selected_subtype_text);
            $('#select2-' + subtype + '-container').prop('innerText', selected_subtype_text);

        } // select subtype na osnovu modela


        $('#dismantled_eq_model_id_1').change(function () {
            selectDismantledSubtype('dismantled_eq_model_id_1', 'dismantled_eq_subtype_id_1');
        });

        $('#dismantled_eq_model_id_2').change(function () {
            selectDismantledSubtype('dismantled_eq_model_id_2', 'dismantled_eq_subtype_id_2');
        });

        $('#dismantled_eq_model_id_3').change(function () {
            selectDismantledSubtype('dismantled_eq_model_id_3', 'dismantled_eq_subtype_id_3');
        });

        $('#dismantled_eq_model_id_4').change(function () {
            selectDismantledSubtype('dismantled_eq_model_id_4', 'dismantled_eq_subtype_id_4');
        });

        $('#dismantled_eq_model_id_5').change(function () {
            selectDismantledSubtype('dismantled_eq_model_id_5', 'dismantled_eq_subtype_id_5');
        });


    </script>

@endpush

@push('style')
    <style>
        .lesspadding {
            padding: .3rem 1.0rem !important;
        }
    </style>
@endpush

@section('content')
    {{--{{ $id }}--}}

    @if ($errors->any())
        @push('scripts')
            <script>
                $(document).ready(function () {
                    $(".status_row").show();
                    onStatusChange();
                    $(".save_changes").show();
                });
            </script>
        @endpush
        <div class="alert alert-danger">
            @foreach ($errors->all() as $error)
                <div>- {{ $error }}</div>
            @endforeach
        </div>
    @endif

    {{--ZATVARANJE NALOGA--}}

    <div class="card">


        {{--  <div class="card-header header-elements-inline">
            <h4 class="card-title">Izmene naloga</h4
        </div>>--}}

        <div class="card-body">
            {!! Form::open(['action' => ['OrdersController@update', $data['order']->order_id], 'method' => 'POST']) !!}
            {{ csrf_field() }}
            {{ method_field('PUT')}}

            {{Form::hidden ('order_status', $data['order']->order_status)}}
            {{Form::hidden ('edit', 1)}}


            <div class="row">

                {{-- CELA STRANA --}}
                <div class="col-12">
                    <fieldset>
                        {{--@if($data['order']->order_status == 'Otkazan')
                            <div class="row col-12 mb-1 canceled">
                                <p>Status: Otkazan <br>
                                    Vreme zatvaranja naloga: {{ datetimeForView($data['order']->completed_at)}}</p>
                            </div>
                        @elseif($data['order']->order_status == 'Nerealizovan' or $data['order']->order_status == 'Završen' )
                            <div class="col-12 mb-3 unrealized">
                                <p>Status: {{$data['order']->order_status}}</p>
                                <p>Vreme zatvaranja naloga: {{ datetimeForView($data['order']->completed_at)}} </p>


                                <legend class="font-weight-semibold mt-3"><i class=" NASE IKONICE "></i> Naziv opreme izdate na korišćenje
                                </legend>
                                <table class="table datatable-basic table-bordered table-striped">
                                    <thead>
                                    <tr>
                                        <th>Kôd</th>
                                        <th>Tip opreme</th>
                                        <th>Model</th>
                                        <th>S/N 1</th>
                                        <th>S/N 2</th>
                                        <th>S/N 3</th>

                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($data['used_equipment'] as $equipment)

                                        <tr>
                                            <td>{{ $equipment->equipment_code }}</td>
                                            <td> {{ $equipment->equipment_subtype_name }}</td>
                                            @if($equipment->equipment_model_id != null)
                                                <td> {{ $equipment->equipment_model_name }}</td>
                                            @else
                                                <td></td>
                                            @endif
                                            <td>{{ $equipment->equipment_serial1 }}</td>
                                            <td>{{ $equipment->equipment_serial2 }}</td>
                                            <td>{{ $equipment->equipment_serial3 }}</td>

                                        </tr>
                                    @endforeach

                                    </tbody>
                                </table>


                                <legend class="font-weight-semibold mt-3"><i class=" NASE IKONICE "></i> Demontirana oprema
                                </legend>
                                <table class="table datatable-basic table-bordered table-striped">
                                    <thead>
                                    <tr>
                                        <th>Tip opreme</th>
                                        <th>S/N 1</th>
                                        <th>S/N 2</th>

                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($data['removed_equipment'] as $removed_equipment)

                                        <tr>
                                            <td>{{ $removed_equipment->dismantled_eq_subtype_name }}</td>
                                            <td>{{ $removed_equipment->removed_equipment_serial1 }}</td>
                                            <td>{{ $removed_equipment->removed_equipment_serial2 }}</td>

                                        </tr>
                                    @endforeach

                                    </tbody>
                                </table>
                                <legend class="font-weight-semibold mt-3"><i class=" NASE IKONICE "></i> Upotrebljeni materijal
                                </legend>
                                <table class="table datatable-basic table-bordered table-striped">
                                    <thead>
                                    <tr>
                                        <th>Kod materijala</th>
                                        <th>Ime materijala</th>
                                        <th>Kolicina</th>

                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($data['used_wares'] as $used_wares)

                                        <tr>
                                            <td>{{ $used_wares->wares_code }}</td>
                                            <td>{{ $used_wares->wares_name }}</td>
                                            <td>{{ $used_wares->quantity }}</td>

                                        </tr>
                                    @endforeach

                                    </tbody>
                                </table>

                                <legend class="font-weight-semibold mt-3"><i class=" NASE IKONICE "></i> Tehničari koji su radili na nalogu
                                </legend>

                                <legend class="font-weight-semibold mt-3"><i class=" NASE IKONICE "></i> Vrsta
                                    objekta i privatno vozilo
                                </legend>
                                <p>Vrsta objekta:
                                    @if($data['order']->service_home == 'house')
                                        Kuća
                                    @elseif($data['order']->service_home == 'apartment')
                                        Stan
                                    @endif
                                </p>
                                <p>Tehničar koristio privatno vozilo:
                                    @if($data['order']->private_vehicle == 0)
                                        Ne
                                    @elseif($data['order']->private_vehicle == 1)
                                        Da
                                    @endif
                                </p>
                            </div>

                        @endif--}}

                        <div class="order_details">

                            @if($data['order']->order_status == 'Završen')
                            <legend class="font-weight-semibold"><i class="{{-- NASE IKONICE --}}"></i> Nekompletan
                            </legend>

                                <div class="uncomplete form-check form-check-inline form-check-right mb-4">
                                    <label class="form-check-label">
                                        Nekompletan
                                        <input type="checkbox" class="form-check-input" id="uncomplete" name="uncomplete" value="1" @if($data['order']->order_status == 'Završen' and $data['order']->uncomplete == 1)
                                        checked="checked"
                             @endif >
                                    </label>
                                </div>
                            @endif



                            <legend class="font-weight-semibold"><i class="{{-- NASE IKONICE --}}"></i> Tip posla
                            </legend>

                            {{ Form::selectGroupSearch('service_type', true ,'Tip posla:', null,
               $data['services'], $data['service']->service_select,
                ['data-placeholder' => 'Odaberite tip posla',
                'class'=> 'form-control select-search'], 'form-group col-5', true) }}


                            <legend class="font-weight-semibold"><i class="{{-- NASE IKONICE --}}"></i> Oprema izdata na
                                korišćenje
                            </legend>

                            {{-- AKTIVNOSTI NA NOVOJ OPREMI --}}
                            {{-- U redu tri SELECTA sa pet redova --}}


                            <div class="row">

                                <div class="col-6 form-row mb-2 mr-1">
                                    <label for="equipment_subtype_1">Tip opreme:</label>
                                </div>

                                <div class="col-6 form-row mb-2 mr-1">
                                    <label for="equipment_subtype_1">Serijski broj opreme:</label>
                                </div>
                            </div>

                            @foreach($data['used_equipment'] as $key => $used_equipment)
                                {{--@dd($used_equipment)--}}
                                <div class="row">

                                    {{ Form::selectGroupSearch('equipment_subtype_'.($key+1),
                                                    false,
                                                    null,
                                                    null,
                                                    $data['subtype'], $used_equipment->equipment_subtype_id,
                                                    ['data-placeholder' => 'Izaberite tip opreme',
                                                    'id' => 'equipment_subtype_'.($key+1),
                                                     'class'=> 'form-control select-search equipment_subtype',
                                                      ], 'col-6 form-row mb-2 mr-1') }}




                                    {{ Form::selectGroupSearch('equipment_serial_'.($key+1),
                                                                    false,
                                                                    null,
                                                                    null,
                                                                    $data['all_serials'],
                                                                    'serial1_'.$used_equipment->equipment_id,
                                                                    ['data-placeholder' => 'Izaberite serijski broj',
                                                                    'id' => 'equipment_serial_'.($key+1),
                                                                     'class'=> 'form-control select-search',
                                                                      ], 'col-6 form-row mb-2') }}

                                </div>

                            @endforeach
                            {{--@php($start =  1+$data['count_used_equipment'])
                            @dd($i)--}}
                            @for ($i = 1+$data['count_used_equipment']; $i <= 5; $i++)

                                <div class="row">

                                    {{ Form::selectGroupSearch('equipment_subtype_'.$i,
                                                    false,
                                                    null,
                                                    null,
                                                    $data['subtype'], null,
                                                    ['data-placeholder' => 'Izaberite tip opreme',
                                                    'id' => 'equipment_subtype_'.$i,
                                                     'class'=> 'form-control select-search equipment_subtype',
                                                      ], 'col-6 form-row mb-2 mr-1') }}




                                    {{ Form::selectGroupSearch('equipment_serial_'.$i,
                                                                    false,
                                                                    null,
                                                                    null,
                                                                    $data['all_serials'], null,
                                                                    ['data-placeholder' => 'Izaberite serijski broj',
                                                                    'id' => 'equipment_serial_'.$i,
                                                                     'class'=> 'form-control select-search',
                                                                      ], 'col-6 form-row mb-2') }}

                                </div>

                            @endfor

                            {{-- AKTIVNOSTI NA NOVOJ OPREMI --}}


                            <legend class="font-weight-semibold"><i class="{{-- NASE IKONICE --}}"></i> Oprema koja se
                                demontira i vraća
                            </legend>

                            <div class="row">

                                <div class="col-3 form-row mb-2 mr-1">
                                    <label>Tip opreme:</label>
                                </div>
                                <div class="col-3 form-row mb-2 mr-1">
                                    <label>Model opreme:</label>
                                </div>

                                <div class="col-3 form-row mb-2 mr-1">
                                    <label>Serijski broj 1:</label>
                                </div>
                                <div class="col-3 form-row mb-2 mr-1">
                                    <label>Serijski broj 2:</label>
                                </div>

                            </div>


                            @foreach($data['removed_equipment'] as $key => $removed_equipment)

                                {{--@dd($removed_equipment)--}}
                                <div class="row">

                                    {{ Form::selectGroupSearch('dismantled_eq_subtype_id_'.($key+1),
                                                    false,
                                                    null,
                                                    null,
                                                    $data['dismantled_subtype'],
                                                    $removed_equipment->removed_equipment_subtype,
                                                    ['data-placeholder' => 'Izaberite tip opreme',
                                                     'class'=> 'form-control select-search equipment_subtype',
                                                     'id'=> 'dismantled_eq_subtype_id_'.($key+1),
                                                     ($removed_equipment->removed_equipment_status == 'razduzeno') ? 'disabled' : ''
                                                      ], 'col-3 form-row mb-2 mr-1') }}

                                    {{ Form::selectGroupSearch('dismantled_eq_model_id_'.($key+1),
                                                    false,
                                                    null,
                                                    null,
                                                    $data['dismantled_model'],
                                                    $removed_equipment->removed_equipment_model,
                                                    ['data-placeholder' => 'Izaberite model opreme',
                                                     'class'=> 'form-control select-search equipment_subtype',
                                                     'id'=> 'dismantled_eq_model_id_'.($key+1),
                                                     ($removed_equipment->removed_equipment_status == 'razduzeno') ? 'disabled' : ''
                                                      ], 'col-3 form-row mb-2 mr-1') }}

                                    <div class="form-group col-3 mb-2">
                                        <input class="form-control" placeholder="Upisite serijski broj"
                                               type="text" id="dismantled_eq{{$key+1}}_serial1"
                                               @if($removed_equipment->removed_equipment_serial1 != null)
                                               value="{{$removed_equipment->removed_equipment_serial1}}"
                                               @endif
                                               @if($removed_equipment->removed_equipment_status == 'razduzeno')
                                               disabled
                                               @endif
                                               @if($removed_equipment->removed_equipment_status == 'razduzeno')
                                               name="dismantled_eq_serial1_{{$key+1}}"
                                               @else
                                               name="dismantled_eq{{$key+1}}_serial1"
                                                @endif
                                        >
                                    </div>

                                    <div class="form-group col-3 mb-2">
                                        <input class="form-control" placeholder="Upisite serijski broj"
                                               name="dismantled_eq{{$key+1}}_serial2" type="text"
                                               id="dismantled_eq{{$key+1}}_serial2"
                                               @if($removed_equipment->removed_equipment_serial2 != null)
                                               value="{{$removed_equipment->removed_equipment_serial2}}"
                                               @endif
                                               @if($removed_equipment->removed_equipment_status == 'razduzeno')
                                               disabled
                                                @endif>
                                    </div>


                                </div>
                            @endforeach


                            @for ($i = 1+$data['count_removed_equipment']; $i <= 5; $i++)
                                <div class="row">

                                    {{ Form::selectGroupSearch('dismantled_eq_subtype_id_'.$i,
                                                    false,
                                                    null,
                                                    null,
                                                    $data['dismantled_subtype'], null,
                                                    ['data-placeholder' => 'Izaberite tip opreme',
                                                     'class'=> 'form-control select-search equipment_subtype',
                                                     'id'=> 'dismantled_eq_subtype_id_'.$i
                                                      ], 'col-3 form-row mb-2 mr-1') }}

                                    {{ Form::selectGroupSearch('dismantled_eq_model_id_'.$i,
                                                    false,
                                                    null,
                                                    null,
                                                    $data['dismantled_model'], null,
                                                    ['data-placeholder' => 'Izaberite model opreme',
                                                     'class'=> 'form-control select-search equipment_subtype',
                                                     'id'=> 'dismantled_eq_model_id_'.$i,
                                                      ], 'col-3 form-row mb-2 mr-1') }}

                                    <div class="form-group col-3 mb-2">
                                        <input class="form-control" placeholder="Upisite serijski broj"
                                               name="dismantled_eq{{$i}}_serial1" type="text"
                                               id="dismantled_eq{{$i}}_serial1">
                                    </div>

                                    <div class="form-group col-3 mb-2">
                                        <input class="form-control" placeholder="Upisite serijski broj"
                                               name="dismantled_eq{{$i}}_serial2" type="text"
                                               id="dismantled_eq{{$i}}_serial2">
                                    </div>


                                </div>

                            @endfor

                            {{-- Odabir materijala --}}

                            <legend class="font-weight-semibold"><i class="{{-- NASE IKONICE --}}"></i> Utrošeni
                                materijal
                            </legend>
                            <div class="row">

                                <div class="col-6">
                                    <table class="table datatable-basic table-bordered table-striped">
                                        <thead>
                                        <tr>
                                            <th>Šifra i naziv materijala</th>
                                            <th>Količina</th>
                                        </tr>
                                        </thead>
                                        {{--@dd($default_wares)--}}
                                        {{--{{ default wares }}--}}
                                        <tbody>
                                        @php($used_wares_ids = [])
                                        @foreach($data['used_wares'] as $used_wares)
                                            <tr>
                                                <td class="lesspadding">{{ $used_wares->wares_code }}
                                                    - {{ $used_wares->wares_name }}</td>
                                                <td style="width: 140px" class="lesspadding"><input
                                                            style="width: 40px" type="number" min="1" step="1"
                                                            name="wares_{{ $used_wares->wares_id }}"
                                                            value="{{$used_wares->quantity}}"> {{ $used_wares->wares_type }}
                                                </td>
                                            </tr>
                                            @php($used_wares_ids[]=$used_wares->wares_id)
                                        @endforeach

                                        @foreach($data['default_wares'] as $wares)
                                            @if(!in_array($wares->wares_id, $used_wares_ids))
                                                <tr>
                                                    <td class="lesspadding">{{ $wares->wares_code }}
                                                        - {{ $wares->wares_name }}</td>
                                                    <td style="width: 140px" class="lesspadding"><input
                                                                style="width: 40px" type="number" min="1" step="1"
                                                                name="wares_{{ $wares->wares_id }}"> {{ $wares->wares_type }}
                                                    </td>
                                                </tr>
                                            @endif
                                        @endforeach
                                        </tbody>
                                        {{--{{ /$default wares }}--}}
                                    </table>

                                </div>

                                <div class="col-6">

                                    <table class="table datatable-basic table-bordered table-striped">
                                        <thead>
                                        <tr>
                                            <th>Šifra i naziv materijala</th>
                                            <th>Količina</th>
                                        </tr>
                                        </thead>
                                        {{--@dd($default_wares)--}}
                                        {{--{{ default wares }}--}}
                                        <tbody>
                                        @for($i = 1; $i <= 12; $i++)
                                            {{--@foreach($data['default_wares'] as $wares)--}}
                                            <tr>
                                                <td class="lesspadding">{{ Form::selectGroupSearch('not_default_select_'.$i, false, null, null,
                                                             $data['wares'], null,
                                                                 ['data-placeholder' => 'Odaberite materijal',
                                                                 'id' => 'not_default_select_'.$i,
                                                                 'data-id'=> $i,
                                                                 'class'=> 'form-control select-search',                                                                                          'onchange'=> 'notDefaultSelect('.$i.')',
                                                                  ], 'col-12') }}</td>
                                                <td style="width: 140px" class="lesspadding">
                                                    <input id="not_default_input_{{ $i }}" style="width: 40px"
                                                           type="number" min="1" step="1"
                                                           name="not_default_input_{{ $i }}">
                                                </td>
                                            </tr>
                                        @endfor
                                        </tbody>
                                        {{--{{ /$default wares }}--}}
                                    </table>


                                </div>

                            </div>
                            {{-- /odabir materijala --}}


                            {{-- TEHNICARI --}}
                            <legend class="font-weight-semibold"><i class="{{-- NASE IKONICE --}}"></i> Izbor
                                tehnčara
                            </legend>
                            <div class="row mb-3">
                                @if(isset($data['order_technician']['0']))
                                    @php($technician_1 = 'technician_'.$data['order_technician']['0']->technician_id)
                                @else
                                    @php($technician_1 = null)
                                @endif

                                {{--Prvi tehnicar--}}
                                {{ Form::selectGroupSearch('technician_1',
                                            true,
                                            'Prvi tehničar:',
                                            null,
                                            $data['technicians'], $technician_1,
                                            ['data-placeholder' => 'Izaberite prvog tehničara',
                                             'class'=> 'form-control select-search technician_1',
                                             'data-focus'], 'col-6 form-row mb-2', true) }}

                                {{--Drugi tehnicar--}}
                                @if(isset($data['order_technician']['1']))
                                    @php($technician_2 = 'technician_'.$data['order_technician']['1']->technician_id)
                                @else
                                    @php($technician_2 = null)
                                @endif

                                {{ Form::selectGroupSearch('technician_2',
                                            true,
                                            'Drugi tehničar:',
                                            null,
                                            $data['technicians'], $technician_2,
                                            ['data-placeholder' => 'Izaberite drugog tehničara',
                                             'class'=> 'form-control select-search technician_2',
                                             'data-focus'], 'col-6 form-row mb-2') }}
                            </div>

                            {{-- /TEHNICARI --}}



                            {{--VRSTA OBJEKTA--}}
                            <legend class="font-weight-semibold"><i class="{{-- NASE IKONICE --}}"></i> Vrsta
                                objekta i privatno vozilo
                            </legend>
                            <div class="row">
                                <div class=" col-6">
                                    <div class="form-group">

                                        <label class="col-form-label mr-2" for="service_home">Vrsta objekta<span
                                                    class="text-danger">*</span></label>
                                        <div class="form-check form-check-inline">
                                            <label class="form-check-label">
                                                <input type="radio" class="form-check-input" value="apartment"
                                                       name="service_home"
                                                       @if($data['order']->service_home == 'apartment')                                                         checked="checked"
                                                        @endif
                                                >Stan</label>
                                        </div>

                                        <div class="form-check form-check-inline">
                                            <label class="form-check-label">
                                                <input type="radio" class="form-check-input" value="house"
                                                       name="service_home"
                                                       @if($data['order']->service_home == 'house')                                                         checked="checked"
                                                        @endif>Kuca</label>
                                        </div>

                                    </div>
                                </div>

                                {{--Privatno vozilo--}}
                                <div class=" col-6">
                                    <div class="form-check form-check-inline form-check-right">
                                        <label class="form-check-label">
                                            Tehničar koristio privatno vozilo
                                            <input type="checkbox" class="form-check-input" name="private_vehicle"
                                                   value="1" @if($data['order']->private_vehicle == 1)
                                                   checked="checked"
                                                    @endif
                                            >
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>


                        {{-- vreme zatvaranja --}}

                        <div class="row mb-3">

                            <legend class="font-weight-semibold"><i class="{{-- NASE IKONICE --}}"></i> Vreme
                                zavrsavanja naloga
                            </legend>


                            {{--vreme zatvaranja--}}
                            {{ Form::datetime_picker('completed_at', 'Datum i vreme zatvaranja naloga:', null, datetimeForPicker($data['order']->completed_at), 'form-control', ['placeholder' => 'Izaberi datum'], 'col-4', true) }}

                            <div class="col-6"></div>
                        </div>

                        {{-- /vreme zatvaranja --}}


                        {{-- KOMENTAR --}}


                        <div class="form-group">
                            <legend class="font-weight-semibold"><i class="{{-- NASE IKONICE --}}"></i> Komentar
                            </legend>

                            <textarea rows="5" cols="5" id="order_comment" name="order_comment"
                                      class="form-control"
                                      placeholder="Upisite dodatne komentare">{{$data['order']->note}}</textarea>
                        </div>

                        <div class="text-right">
                            <a class="btn bg-teal-300 back" href="{{ URL::previous() }}">Odustani</a>

                            <button type="submit" class="btn bg-teal-300 save_changes">Sačuvaj izmene<i
                                        class="icon-checkmark4 ml-2"></i></button>
                        </div>
                    </fieldset>
                </div>
            </div>


            </form>

        </div>
    </div>




@endsection