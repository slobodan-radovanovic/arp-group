@extends('layouts.app')



@push('scripts')

    <script type="text/javascript" src="{{ asset('global_assets/js/plugins/ui/moment/moment.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('global_assets/js/plugins/pickers/anytime.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/custom_datepicker.js') }}"></script>
    <!-- Load select2 -->
    <script type="text/javascript" src="{{ asset('global_assets/js/plugins/forms/selects/select2.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('global_assets/js/plugins/extensions/jquery_ui/interactions.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/custom_select2.js') }}"></script>

    <script>
        $('form').on('focus', 'input[type=number]', function (e) {
            $(this).on('wheel.disableScroll', function (e) {
                e.preventDefault()
            });
        });

        $('form').on('blur', 'input[type=number]', function (e) {
            $(this).off('wheel.disableScroll');
        });
    </script>


    <script>

        var start_status = $("#order_status option:selected").val();
        var start_scheduled_datetime = $("#scheduled_at_datetime").val();
        var start_uncomplete = $("#uncomplete").val();
        var start_order_comment = $("#order_comment").val();


        function onStatusChange() {
            // hide all form-duration-divs
            /*$('.order_details').hide();*/
            var status = $("#order_status option:selected").val();

            if (status == 'Otvoren' || status == 'Zakazan' || status == 'Odložen' || status == 'Otkazan' || status == 'Nekompletan') {
                $(".order_details").hide();
                $(".save_changes").show();
                console.log(status);
            } else {
                $(".order_details").show();
                $(".save_changes").show();
                console.log(status);
            }

            if (start_status == status) {
                $(".save_changes").hide();
            }

            if (status == 'Zakazan') {
                $(".div_status").show();
                if (start_scheduled_datetime != $("#scheduled_at_datetime").val()) {
                    $(".save_changes").show();
                }
            } else {
                $(".div_status").hide();
            }

            if (status == 'Završen') {
                $(".uncomplete").show();
                if (start_uncomplete != $("#uncomplete").val()) {
                    $(".save_changes").show();
                }
            } else {
                $(".uncomplete").hide();
            }

            if (status == 'Nerealizovan') {
                $(".free").show();
            } else {
                $(".free").hide();
            }

            /* Otvoren, Zakazan, Odložen, Otkazan, Nerealizovan, Nekompletan, Završen*/

        }


        $("#order_status").change(function () {
            setTimeout(function () {
                onStatusChange();
                checkCommentChange();
            }, 600);
        });

        $("#scheduled_at_datetime").change(function () {
            $(".save_changes").show();
        });

        function checkCommentChange() {
            if (start_order_comment != $("#order_comment").val()) {
                $(".save_changes").show();
            }
        }

        $("#order_comment").keyup(function () {
            if (start_order_comment != $("#order_comment").val()) {
                $(".save_changes").show();
            } else {
                $(".save_changes").hide();
            }
        });

        onStatusChange();

        if (start_status == "Otkazan") {
           /* $(".status_row").hide();*/
            $(".canceled").show();
        } else if(start_status == "Nerealizovan" || start_status == "Završen"){
            $(".status_row").hide();
            $(".order_details").hide();
            $(".unrealized").show();
        }


        //

        var json_equipment_select = {!! $data['equipment_for_select'] !!};
        var json_dismantled_eq_select = {!! $data['dismantled_eq_for_select'] !!};
        var json_all_serials = {!! json_encode($data['all_serials']) !!};

        function filterEquipment(key, value) {
            return json_equipment_select.filter(function (equipment) {
                return equipment[key] == value;
            });
        } // filtriranje opreme na osnovu tipa (subtype)

        function filterDismantledEquipment(key, value) {
            return json_dismantled_eq_select.filter(function (equipment) {
                return equipment[key] == value;
            });
        } // filtriranje demontirane opreme na osnovu tipa (subtype)

        function selectSerial(serial, subtype) {
            var selected_subtype = $('#' + subtype + ' option:selected').val();
            var filtered_equipment = filterEquipment('equipment_subtype_id', selected_subtype);
            //equipment_serial select
            var filtered_equipment_serial = {};
            $.each(filtered_equipment, function (index) {
                filtered_equipment_serial[index] = this['equipment_id'];
            });
            var serial_html = '<option value="" selected="selected"></option>';
            $.each(json_all_serials, function (serial_key, serial_value) {
                $.each(filtered_equipment_serial, function (key, equipment_id) {
                    var split_key = serial_key.split("_");
                    if (split_key['1'] == equipment_id) {
                        serial_html += '<option value="' + serial_key + '">' + serial_value + '</option>';
                    }
                });
            });
            $('#' + serial).html(serial_html);
        }

        $('#equipment_subtype_1').change(function () {
            selectSerial('equipment_serial_1', 'equipment_subtype_1');
        });

        $('#equipment_subtype_2').change(function () {
            selectSerial('equipment_serial_2', 'equipment_subtype_2');
        });

        $('#equipment_subtype_3').change(function () {
            selectSerial('equipment_serial_3', 'equipment_subtype_3');
        });

        $('#equipment_subtype_4').change(function () {
            selectSerial('equipment_serial_4', 'equipment_subtype_4');
        });

        $('#equipment_subtype_5').change(function () {
            selectSerial('equipment_serial_5', 'equipment_subtype_5');
        });

        function selectSubtype(serial, subtype) {
            var equipment_serial = $('#' + serial + ' option:selected').val();
            var slit_equipment_serial = equipment_serial.split("_");
            var equipment_id = slit_equipment_serial['1'];
            var filtered_equipment = filterEquipment('equipment_id', equipment_id);
            var equipment_subtype = filtered_equipment["0"].equipment_subtype_id;
            //equipment_subtype_1 select
            $('#' + subtype + ' option:selected').removeAttr("selected");
            $('#' + subtype + ' option[value="' + equipment_subtype + '"]').attr('selected', 'selected');
            var selected_subtype_text = $('#' + subtype + ' option:selected').text();
            $('#select2-' + subtype + '-container').prop('title', selected_subtype_text);
            $('#select2-' + subtype + '-container').prop('innerText', selected_subtype_text);
        } // selektovanje tipa na osnovu serijskog

        $('#equipment_serial_1').change(function () {
            selectSubtype('equipment_serial_1', 'equipment_subtype_1');
        });

        $('#equipment_serial_2').change(function () {
            selectSubtype('equipment_serial_2', 'equipment_subtype_2');
        });

        $('#equipment_serial_3').change(function () {
            selectSubtype('equipment_serial_3', 'equipment_subtype_3');
        });

        $('#equipment_serial_4').change(function () {
            selectSubtype('equipment_serial_4', 'equipment_subtype_4');
        });

        $('#equipment_serial_5').change(function () {
            selectSubtype('equipment_serial_5', 'equipment_subtype_5');
        });

        function notDefaultSelect(id) {
            var wares_id = $('#not_default_select_' + id + ' option:selected').val();
           // var slit_wares_value = wares_value.split("_");
            //var wares_id = slit_wares_value['1'];
            $('#not_default_input_' + id).attr('name', wares_id);
            console.log('notDefaultSelect is changed ' + id);
            console.log('wares id is ' + wares_id);
            console.log('not_default_input_' + id + 'name is ' + $('#not_default_input_' + id).attr('name'));
        }


        function selectDismantledModel(model, subtype) {

            var selected_subtype = $('#' + subtype + ' option:selected').val();
            console.log(selected_subtype);
            var filtered_equipment = filterDismantledEquipment('dismantled_eq_subtype_id', selected_subtype);

            //equipment_model_id select
            var filtered_equipment_model = {};
            $.each(filtered_equipment, function () {
                filtered_equipment_model[this['dismantled_eq_model_id']] = this['dismantled_eq_model_name'];
            });
            var model_html = '<option value="" selected="selected"> ';
            $.each(filtered_equipment_model, function (key, value) {
                if (key != 'null') {
                    model_html += '<option value="' + key + '">' + value + ' ';
                }
            });
            console.log(model_html);
            console.log('#'+model);
            $('#'+model).html(model_html);

        } // filter modela na osnovu subtype


        $('#dismantled_eq_subtype_id_1').change(function () {
            selectDismantledModel('dismantled_eq_model_id_1', 'dismantled_eq_subtype_id_1');
        });

        $('#dismantled_eq_subtype_id_2').change(function () {
            selectDismantledModel('dismantled_eq_model_id_2', 'dismantled_eq_subtype_id_2');
        });

        $('#dismantled_eq_subtype_id_3').change(function () {
            selectDismantledModel('dismantled_eq_model_id_3', 'dismantled_eq_subtype_id_3');
        });

        $('#dismantled_eq_subtype_id_4').change(function () {
            selectDismantledModel('dismantled_eq_model_id_4', 'dismantled_eq_subtype_id_4');
        });

        $('#dismantled_eq_subtype_id_5').change(function () {
            selectDismantledModel('dismantled_eq_model_id_5', 'dismantled_eq_subtype_id_5');
        });

      /*  $('#dismantled_eq_subtype_id_1').change(function () {
            var selected_subtype = $("#dismantled_eq_subtype_id_1 option:selected").val();
            var filtered_equipment = filterDismantledEquipment('dismantled_eq_subtype_id', selected_subtype);

            //equipment_model_id select
            var filtered_equipment_model = {};
            $.each(filtered_equipment, function () {
                filtered_equipment_model[this['dismantled_eq_model_id']] = this['dismantled_eq_model_name'];
            });
            var model_html = '<option value="" selected="selected"> ';
            $.each(filtered_equipment_model, function (key, value) {
                if (key != 'null') {
                    model_html += '<option value="' + key + '">' + value + ' ';
                }
            });

            $('#dismantled_eq_model_id_1').html(model_html);

        });*/
        function selectDismantledSubtype(model, subtype) {

            var selected_model = $('#'+model+' option:selected').val();

            var filtered_equipment = filterDismantledEquipment('dismantled_eq_model_id', selected_model);

            var equipment_subtype = filtered_equipment["0"].dismantled_eq_subtype_id;

            //equipment_subtype_id select
            $('#'+subtype+' option:selected').removeAttr("selected");
            $('#'+subtype+' option[value="' + equipment_subtype + '"]').attr('selected', 'selected');
            var selected_subtype_text = $('#'+subtype+' option:selected').text();
            $('#select2-'+subtype+'-container').prop('title', selected_subtype_text);
            $('#select2-'+subtype+'-container').prop('innerText', selected_subtype_text);

        } // select subtype na osnovu modela


        $('#dismantled_eq_model_id_1').change(function () {
            selectDismantledSubtype('dismantled_eq_model_id_1', 'dismantled_eq_subtype_id_1');
        });

        $('#dismantled_eq_model_id_2').change(function () {
            selectDismantledSubtype('dismantled_eq_model_id_2', 'dismantled_eq_subtype_id_2');
        });

        $('#dismantled_eq_model_id_3').change(function () {
            selectDismantledSubtype('dismantled_eq_model_id_3', 'dismantled_eq_subtype_id_3');
        });

        $('#dismantled_eq_model_id_4').change(function () {
            selectDismantledSubtype('dismantled_eq_model_id_4', 'dismantled_eq_subtype_id_4');
        });

        $('#dismantled_eq_model_id_5').change(function () {
            selectDismantledSubtype('dismantled_eq_model_id_5', 'dismantled_eq_subtype_id_5');
        });
        //equipment_model_id select
        /*$('#dismantled_eq_model_id_1').change(function () {
            var selected_model = $("#dismantled_eq_model_id_1 option:selected").val();
            console.log(selected_model);
            var filtered_equipment = filterDismantledEquipment('dismantled_eq_model_id', selected_model);
            //console.log(filtered_equipment);
            var equipment_subtype = filtered_equipment["0"].dismantled_eq_subtype_id;
            console.log(equipment_subtype);
            //equipment_subtype_id select
            $('#dismantled_eq_subtype_id_1 option:selected').removeAttr("selected");
            $('#dismantled_eq_subtype_id_1 option[value="' + equipment_subtype + '"]').attr('selected', 'selected');
            var selected_subtype_text = $("#dismantled_eq_subtype_id_1 option:selected").text();
            $('#select2-dismantled_eq_subtype_id_1-container').prop('title', selected_subtype_text);
            $('#select2-dismantled_eq_subtype_id_1-container').prop('innerText', selected_subtype_text);

        });*/


    </script>

@endpush

@push('style')
    <style>
        .lesspadding {
            padding: .3rem 1.0rem !important;
        }
    </style>
@endpush

@section('content')
    {{--{{ $id }}--}}

    @if ($errors->any())
        @push('scripts')
            <script>
                $( document ).ready(function() {
                    $(".status_row").show();
                    onStatusChange();
                    $(".save_changes").show();
                });
            </script>
        @endpush
        <div class="alert alert-danger">
            @foreach ($errors->all() as $error)
                <div>- {{ $error }}</div>
            @endforeach
        </div>
    @endif

    <!-- Edit modal -->
    <div id="modal_edit" class="modal fade" tabindex="-1">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Izmena podataka o nalogu</h5>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>

                <div class="modal-body">

                    <p>Redni broj naloga : {{ $data['order']->ordinal_number }}</p>


                    {!! Form::open(['route' => ['order.update_info', $data['order']->order_id], 'method' => 'POST']) !!}
                    {{ csrf_field() }}
                    {{ method_field('PUT')}}
                    {{--{{Form::hidden ('order_id', $data['order']->order_id, ['id' => 'order_id'])}}--}}

                    {{--{{ Form::textGroup('buyer_id', 'Broj kupca', null, ['placeholder' => 'Upisite broj kupca'], $data['order']->order_id, 'col-12', 'col-3', 'col-9') }}--}}

                    <div class="row">
                        {{--@dd(datetimeForPicker($data['order']->opened_at))--}}
                        {{ Form::textGroup2('order_number', 'Broj naloga:', null, $data['order']->order_number, 'form-control', ['placeholder' => 'Broj naloga', 'data-parsly-trigger'=> 'keyup'], 'col-3', true) }}

                        {{-- 'name', 'labelvalue', 'classlabel', 'value' => null, 'classtext', 'attributes' => ['required' => 'required'], 'col'--}}
                        {{ Form::datetime_picker('opened_at_datetime', 'Datum i vreme prijave:', null, datetimeForPicker($data['order']->opened_at), 'form-control', ['placeholder' => 'Izaberi datum'], 'col-4', true) }}

                   {{--  {{ Form::datetime_picker('started_at_datetime', 'Predvidjeni pocetak rada:', null, datetimeForPicker($data['order']->started_at), 'form-control', ['placeholder' => 'Izaberi datum'], 'col-4', true) }}--}}


                    </div>


                    <div class="row">

                        {{ Form::textGroup2('buyer_id', 'Broj kupca:', null, $data['order']->buyer_id, 'form-control', ['placeholder' => 'Broj kupca'], 'col-4') }}

                        {{ Form::textGroup2('treaty_id', 'Broj ugovora:', null, $data['order']->treaty_id, 'form-control', ['placeholder' => 'Broj ugovora'], 'col-4') }}


                        {{ Form::textGroup2('buyer', 'Ime i prezime kupca:', null, $data['order']->buyer, 'form-control', ['placeholder' => 'Ime i prezime kupca'], 'col-4', true) }}

                    </div>

                    <div class="row">

                        {{ Form::textGroup2('area_code', 'Poštanski broj:', null, $data['order']->area_code, 'form-control', ['placeholder' => 'Poštanski broj'], 'col-3') }}


                        {{ Form::textGroup2('city', 'Grad:', null, 'Beograd', 'form-control', ['readonly'], 'col-3', true) }}



                        {{ Form::selectGroupSearch('region', true, 'Lokacija:', null,
                        $select['region'], $data['order']->region,
                           ['data-placeholder' => 'Odaberite lokaciju',
                           'class'=> 'form-control select-search township'], 'col-3') }}



                        {{ Form::selectGroupSearch('township', true, 'Odaberite opštinu:', null,
                        $select['township'], $data['order']->township,
                         ['data-placeholder' => 'Odaberite opštinu',
                         'class'=> 'form-control select-search township'], 'col-3', true) }}

                    </div>


                    <div class="row">

                        {{ Form::textGroup2('address', 'Adresa:', null, $data['order']->address, 'form-control', ['placeholder' => 'Naziv ulice'], 'col-6', true) }}

                        {{ Form::textGroup2('address_number', 'broj:', null, $data['order']->address_number, 'form-control', ['placeholder' => 'broj'], 'col-2', true) }}

                        {{ Form::textGroup2('floor', 'sprat:', null, $data['order']->floor, 'form-control', ['placeholder' => 'sprat'], 'col-2') }}

                        {{ Form::textGroup2('apartment', 'stan:', null, $data['order']->apartment, 'form-control', ['placeholder' => 'stan'], 'col-2') }}

                    </div>


                    <div class="row">

                        {{ Form::textGroup2('phone_number', 'Broj telefona:', null, $data['order']->phone_number, 'form-control', ['placeholder' => 'Broj telefona'], 'col-6', true) }}

                        {{ Form::textGroup2('mobile_number', 'Mobilni:', null, $data['order']->mobile_number, 'form-control', ['placeholder' => 'Mobilni'], 'col-6') }}

                    </div>


                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-light" data-dismiss="modal">Odustani</button>
                    {!! Form::button('Sačuvaj', ['id' =>'button-create-technician', 'type' =>'submit', 'class' => 'btn bg-teal-300 button-submit', 'data-loading-text' => 'Sačuvaj izmene']) !!}
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
    <!-- /edit modal -->


    <!-- Delete modal -->
    <div id="modal_delete" class="modal fade" tabindex="-1">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Brisanje naloga</h5>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>

                <div class="modal-body">
                    {!!Form::open(['action' => ['OrdersController@destroy', $data['order']->order_id],'method'=> 'POST'])!!}
                    {{Form::hidden('_method', 'DELETE')}}


                    <p>Da li ste sigurni da želite da izbrišete ovaj nalog?<br>
                        Nalog ce biti trajno izbrisan.
                        <br>Ako je status naloga "Završen" ili "Nerealizovan" biće izvršene izmene:
                        <br>&nbsp&nbsp - Ugradjena oprema će se vratiti u status "Kod tehničara"
                        <br>&nbsp&nbsp - Upotrebljeni materijal se vraća na stanje
                        <br>&nbsp&nbsp - Briše stavka za ovaj nalog u izveštaju za tehničara
                        <br>&nbsp&nbsp - Briše se finansijski izveštaj za ovaj nalog


                    </p>

                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-light" data-dismiss="modal">Odustani</button>
                    {{Form::submit('Izbriši', ['class' => 'btn btn-danger'])}}
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
    <!-- /delete modal -->

    <!-- Delete file modal -->
    <div class="modal fade" id="delete_file" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title"> Da li ste sigurni da želite obrisati fajl za ovaj nalog? </h4>
                </div>
                <div class="modal-body">
                    <br/>
                </div>
                <div class="modal-footer">
                    {!! Form::open(['route' => ['order.delete_file', $data['order']->order_id], 'method' => 'POST']) !!}
                    {{ csrf_field() }}
                    {{ method_field('PUT')}}

                    {{ Form::submit('izbriši fajl', ['class' => 'btn btn-danger']) }}
                    {{ Form::button('Odustajem', ['class' => 'btn btn-default', 'data-dismiss' => 'modal']) }}
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
    <!-- /Delete file modal -->


    <div class="card mt-1">

        <div class="card-header header-elements-inline">
            <h4 class="card-title">Osnovni podaci naloga</h4>

            <div class="header-elements">
                <div class="list-icons">
                    <a class="list-icons-item" data-action="collapse"></a>
                </div>
            </div>
        </div>

        <div class="card-body">
            <div class="row">

                {{-- LEVA STRANA --}}
                <div class="col-6">
                    <fieldset>
                        <legend class="font-weight-semibold"><i class="icon-reading mr-2"></i> Podaci o kupcu</legend>

                        {{-- KUPAC --}}
                        {{-- U redu tri inputa --}}

                        <p>Redni broj naloga : {{ $data['order']->ordinal_number }}</p>
                        <p>Broj kupca: {{ ($data['order']->buyer_id != null ) ? $data['order']->buyer_id : "/" }} </p>
                        <p>Broj
                            ugovora: {{ ($data['order']->treaty_id != null ) ? $data['order']->treaty_id : "/" }}</p>
                        <p>Ime i prezime kupca: {{ ($data['order']->buyer != null ) ? $data['order']->buyer : "/" }}</p>
                        <p>
                            Adresa: {{ ($data['order']->area_code != null ) ? $data['order']->area_code : "/" }} {{$data['order']->city}}
                            , {{ ($data['order']->township != null ) ? $data['order']->township : "/" }}
                            , {{ ($data['order']->address != null ) ? $data['order']->address : "/" }}
                            {{ ($data['order']->address_number != null ) ? $data['order']->address_number : "/" }},
                            sprat: {{ ($data['order']->floor != null ) ? $data['order']->floor : "/" }},
                            stan: {{ ($data['order']->apartment != null ) ? $data['order']->apartment : "/" }}</p>
                        <p>
                            Region: {{ ($data['order']->region != null ) ? $data['order']->region : "/" }}</p>
                        <p>
                            Telefon: {{ ($data['order']->phone_number != null ) ? $data['order']->phone_number : "/" }}</p>
                        <p>Mobilni
                            telefon: {{ ($data['order']->mobile_number != null ) ? $data['order']->mobile_number : "/" }}</p>


                        @if($data['order']->file_name == null)
                            <p>Fajl:</p>
                            <p>
                                {{--{!! Form::open(['action' => 'OrdersController@store_file', 'method' => 'POST', 'enctype' => 'multipart/form-data']) !!}--}}
                                {!! Form::open(['route' => ['order.store_file', $data['order']->order_id], 'method' => 'POST', 'enctype' => 'multipart/form-data']) !!}
                                {{ csrf_field() }}
                                {{ method_field('PUT')}}

                                {{Form::file('file')}}

                                {{Form::submit('Sačuvaj fajl', ['class' => 'btn bg-teal-300 mt-1',])}}

                                {!! Form::close() !!}

                            </p>
                        @else
                            <p>Fajl: {{$data['order']->file_name}}</p>
                            <p><a class="btn bg-teal-300" target="_blank"
                                  href="/storage/orders_files/{{$data['order']->file_name}}"> Preuzmi fajl </a> <a
                                        class="btn btn-danger" href="#delete_file" data-toggle="modal"
                                        data-target="#delete_file" {{--style="color:red;"--}} title="Obriši">Obriši
                                    fajl</a></p>
                        @endif



                    </fieldset>
                </div>

                {{-- DESNA STRANA --}}
                <div class="col-6">
                    <fieldset>
                        <legend class="font-weight-semibold"><i class="icon-truck mr-2"></i> Podaci o nalogu</legend>
                        <p>Datum i vreme prijave: {{datetimeForView($data['order']->opened_at)}}</p>
                        <p>Broj naloga: {{$data['order']->order_number}}</p>
                       {{-- <p>Predvidjeni pocetak rada: {{datetimeForView($data['order']->started_at)}}</p>--}}
                        <p>Tip posla: {{$data['service_name']}}</p>
                        <p>&nbsp</p>
                        <p>&nbsp</p>

                        @if (Auth::user()->role == 'Kordinator')
                            <p class="text-right">
                                <button type="button" class="btn btn-danger" data-toggle="modal"
                                        data-target="#modal_delete">
                                    Izbriši <i class="icon-trash
 ml-1"></i></button>
                            </p>
                        @endif

                        @if (Auth::user()->role == 'Kordinator')

                        <p class="text-right">
                            <button type="button" class="btn bg-teal-300 " data-toggle="modal"
                                    data-target="#modal_edit">
                                Izmeni <i class="icon-pencil7 ml-1"></i></button>
                        </p>

                        @endif
                        <p class="text-right">
                            <a class="btn bg-teal-300 " href="print" target="_blank">
                                Štampanje <i class="icon-printer ml-1"></i></a>
                        </p>
                        {{--<p class="text-right">
                            <button type="button" class="btn bg-teal-300 " data-toggle="modal" data-target="#modal_add">
                                Izmeni <i class="icon-pencil7 ml-1"></i></button>
                        </p>--}}
                    </fieldset>
                </div>
            </div>
        </div>
    </div>

    {{--drugi deo--}}

    <div class="card">

        <div class="card-header header-elements-inline">
            <h4 class="card-title">Opis radova i podaci o opremi - planirano</h4>
            <div class="header-elements">
                <div class="list-icons">
                    <a class="list-icons-item" data-action="collapse"></a>
                    {{-- <a class="list-icons-item" data-action="reload"></a>--}}
                    {{--  NE FUNKCIONISE RELOAD DUGME --}}
                </div>
            </div>
        </div>

        <div class="card-body">
            <form action="#">
                <div class="row">

                    {{-- CELA STRANA --}}
                    <div class="col-md-12">
                        <fieldset>
                            <legend class="font-weight-semibold"><i class="{{-- NASE IKONICE --}}"></i> Opis radova
                            </legend>
                            {{--<p>Tip posla: {{$data['service'][$data['order']->service_type]}}</p>--}}
                            {{--<p>Tip posla: {{$data['service'][1]->service_name}}</p>--}}
                            <div class="row">
                                <div class="col-6">
                                    <legend class="font-weight-semibold  mb-2 pb-1 mt-3"><i
                                                class="{{-- NASE IKONICE --}}"></i> Aktivnosti na novoj opremi
                                    </legend>


                                    <!-- Basic table Aktivnosti na novoj opremi-->
                                    <table class="table datatable-basic table-bordered table-striped">
                                        <thead>
                                        <tr>
                                            <th class="lesspadding" style="width: 10%;">#</th>
                                            <th class="lesspadding">Tip opreme</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @if($data['planned_mounted']->count() == 0)
                                            <tr>
                                                <td></td>
                                                <td>/</td>
                                            </tr>
                                        @else
                                            @foreach($data['planned_mounted'] as $indexKey => $planed_mounted)
                                                <tr>
                                                    <td>{{ $indexKey+1 }}</td>
                                                    <td>{{$planed_mounted->equipment_subtype_name}}</td>
                                                </tr>
                                            @endforeach
                                        @endif

                                        </tbody>
                                    </table>
                                    <!-- /basic table Aktivnosti na novoj oprem-->
                                </div>
                                <div class="col-6">
                                    <legend class="font-weight-semibold mb-2 pb-1 mt-3" style=""><i
                                                class="{{-- NASE IKONICE --}}"></i> Aktivnosti na postojecoj opremi
                                    </legend>

                                    <!-- Basic table Aktivnosti na postojecoj opremi-->
                                    <table class="table datatable-basic table-bordered table-striped">
                                        <thead>
                                        <tr>
                                            <th class="lesspadding" style="width: 10%;">#</th>
                                            <th class="lesspadding">Tip opreme</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @if($data['planned_dismantled']->count() == 0)
                                            <tr>
                                                <td></td>
                                                <td>/</td>
                                            </tr>
                                        @else
                                            @foreach($data['planned_dismantled'] as $indexKey => $planned_dismantled)
                                                <tr>
                                                    <td>{{ $indexKey+1 }}</td>
                                                    <td>{{$planned_dismantled->dismantled_eq_subtype_name}}</td>
                                                </tr>
                                            @endforeach
                                        @endif
                                        </tbody>
                                    </table>
                                    <!-- /basic table Aktivnosti na postojecoj opremi-->
                                </div>
                            </div>
                            {{-- <p class="text-right"><button type="button" class="btn bg-teal-300 " data-toggle="modal" data-target="#modal_add">Izmeni <i class="icon-pencil7 ml-1" ></i></button></p>--}}


                        </fieldset>
                    </div>
                </div>
            </form>
        </div>
    </div>


    {{--ZATVARANJE NALOGA--}}

    <div class="card">


            @if(($data['order']->order_status == 'Završen' or $data['order']->order_status == 'Nerealizovan') and Auth::user()->role == 'Kordinator')
                <div class="text-left ml-2 mt-2">
                    <a class="btn bg-teal-300" href="/orders/{{$data['order']->order_id}}/edit">Izmeni nalog
                        <i class="icon-pencil ml-2"></i></a>
                </div>
            @endif


        {{--  <div class="card-header header-elements-inline">
            <h4 class="card-title">Izmene naloga</h4
        </div>>--}}

        <div class="card-body">
            {!! Form::open(['action' => ['OrdersController@update', $data['order']->order_id], 'method' => 'POST']) !!}
            {{ csrf_field() }}
            {{ method_field('PUT')}}
            <div class="row">

                {{-- CELA STRANA --}}
                <div class="col-12">
                    <fieldset>
                        <legend class="font-weight-semibold"><i class="{{-- NASE IKONICE --}}"></i>Status naloga
                        </legend>
                        <div class="row col-12 mb-3 status_row">
                            {{-- <div class="row ">--}}


                            {{ Form::selectGroupSearch('order_status', true ,'Status:', null,
                                                ['Otvoren' => 'Otvoren',
                                                'Zakazan' => 'Zakazan',
                                                'Odložen' => 'Odložen',
                                                'Otkazan' => 'Otkazan',
                                                /*'Nekompletan' => 'Nekompletan',*/
                                                'Nerealizovan' => 'Nerealizovan',
                                                'Završen' => 'Završen'
                                                ], $data['order']->order_status,
                                                ['data-placeholder'=> 'Izaberite status',
                                                'class'=> 'form-control select-search status',
                                                 ], 'col-4') }}
                            <div class="col-4">

                                @if($data['order']->order_status == 'Zakazan')
                                    {{ Form::datetime_picker('scheduled_at_datetime', 'Zazazano za:', null, datetimeForView($data['order']->scheduled_at), 'form-control', ['placeholder' => 'Izaberi datum'], 'div_status') }}
                                @else
                                    {{ Form::datetime_picker('scheduled_at_datetime', 'Zazazano za:', null,null, 'form-control', ['placeholder' => 'Izaberi datum'], 'div_status') }}
                                @endif

                                    {{--<div class="input-group uncomplete mt-4 ml-5">
                                        <label class="form-check-label" for="uncomplete">Nekompletan</label>
                                        <input id="uncomplete" type="checkbox" class="form-check-input " name="uncomplete" value="1">
                                    </div>--}}

                                    <div class="uncomplete form-check form-check-inline form-check-right mt-4 ml-2">
                                        <label class="form-check-label">
                                            Nekompletan
                                            <input type="checkbox" class="form-check-input" id="uncomplete" name="uncomplete" value="1">
                                        </label>
                                    </div>

                                    <div class="free form-check form-check-inline form-check-right mt-4 ml-2">
                                        <label class="form-check-label">
                                            Bez naplate
                                            <input type="checkbox" class="form-check-input" id="free" name="free" value="1">
                                        </label>
                                    </div>

                                {{--@if($data['order']->order_status == 'Završen' and $data['order']->uncomplete == 1)

                                        <div class="input-group uncomplete">
                                            <label class="form-check-label">Nekompletan</label>
                                            <input type="checkbox" checked="checked" class="form-check-input " name="uncomplete" value="1">
                                        </div>
                                 @else
                                        <div class="input-group uncomplete mt-4 ml-5">
                                            <label class="form-check-label">Nekompletan</label>
                                            <input type="checkbox" class="form-check-input " name="uncomplete" value="1">
                                        </div>
                                 @endif--}}






                            </div>

                            <div class="col-4 text-right mt-4">


                            </div>


                        </div>


                            {{--@if($data['order']->order_status == 'Završen' and $data['order']->uncomplete == 1)

                                <div class="col-12 text-left mt-4" id="uncomplete">

                                </div>
                            @else
                                <div class="col-4 text-right mt-4" id="uncomplete">
                                    <label class="form-check-label">Nekompletan:</label><br>
                                    <div class="input-group">
                                        <input type="checkbox" checked="checked" class="form-check-input uncomplete" name="uncomplete" value="1">
                                    </div>
                                </div>
                            @endif--}}


                        @if($data['order']->order_status == 'Otkazan')
                            <div class="row col-12 mb-1 canceled">
                                <p>{{--Status: Otkazan <br>--}}
                                    Vreme zatvaranja naloga: {{ datetimeForView($data['order']->updated_at)}}</p>
                            </div>
                        @elseif($data['order']->order_status == 'Nerealizovan' or $data['order']->order_status == 'Završen' )
                            <div class="col-12 mb-3 unrealized">
                                @if($data['order']->uncomplete == 1)
                                <p>Status: {{$data['order']->order_status}} (Nekompletan)</p>
                                @else
                                <p>Status: {{$data['order']->order_status}}</p>
                                @endif
                                <p>Vreme zatvaranja naloga: {{ datetimeForView($data['order']->completed_at)}} </p>


                                <legend class="font-weight-semibold mt-3"><i class=" NASE IKONICE "></i> Naziv opreme izdate na korišćenje
                                </legend>
                                <table class="table datatable-basic table-bordered table-striped">
                                    <thead>
                                    <tr>
                                        <th>Kôd</th>
                                        <th>Tip opreme</th>
                                        <th>Model</th>
                                        <th>S/N 1</th>
                                        <th>S/N 2</th>
                                        <th>S/N 3</th>

                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($data['used_equipment'] as $equipment)

                                        <tr>
                                            <td>{{ $equipment->equipment_code }}</td>
                                            <td> {{ $equipment->equipment_subtype_name }}</td>
                                            @if($equipment->equipment_model_id != null)
                                                <td> {{ $equipment->equipment_model_name }}</td>
                                            @else
                                                <td></td>
                                            @endif
                                            <td>{{ $equipment->equipment_serial1 }}</td>
                                            <td>{{ $equipment->equipment_serial2 }}</td>
                                            <td>{{ $equipment->equipment_serial3 }}</td>

                                        </tr>
                                    @endforeach

                                    </tbody>
                                </table>

                                <legend class="font-weight-semibold mt-3"><i class="{{-- NASE IKONICE --}}"></i> Demontirana oprema
                                </legend>

                                <table class="table datatable-basic table-bordered table-striped">
                                    <thead>
                                    <tr>
                                        <th>Kod</th>
                                        <th>Tip opreme</th>
                                        <th>Model opreme</th>
                                        <th>S/N 1</th>
                                        <th>S/N 2</th>

                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($data['removed_equipment'] as $removed_equipment)

                                        <tr>
                                            <td>{{ $removed_equipment->equipment_code }}</td>
                                            <td>{{ $removed_equipment->dismantled_eq_subtype_name }}</td>
                                            <td>{{ $removed_equipment->dismantled_eq_model_name }}</td>
                                            <td>{{ $removed_equipment->removed_equipment_serial1 }}</td>
                                            <td>{{ $removed_equipment->removed_equipment_serial2 }}</td>

                                        </tr>
                                    @endforeach

                                    </tbody>
                                </table>


                                <legend class="font-weight-semibold mt-3"><i class="{{-- NASE IKONICE --}}"></i> Upotrebljeni materijal
                                </legend>

                                <table class="table datatable-basic table-bordered table-striped">
                                    <thead>
                                    <tr>
                                        <th>Kod materijala</th>
                                        <th>Ime materijala</th>
                                        <th>Kolicina</th>

                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($data['used_wares'] as $used_wares)

                                        <tr>
                                            <td>{{ $used_wares->wares_code }}</td>
                                            <td>{{ $used_wares->wares_name }}</td>
                                            <td>{{ $used_wares->quantity }}</td>

                                        </tr>
                                    @endforeach

                                    </tbody>
                                </table>


                                <legend class="font-weight-semibold mt-3"><i class="{{-- NASE IKONICE --}}"></i> Tehničari koji su radili na nalogu
                                </legend>

                                    @foreach($data['order_technician'] as $key => $order_technician)
                                    <p>{{++$key}}. {{$order_technician->technician_name}}</p>

                                    @endforeach

                                <legend class="font-weight-semibold mt-3"><i class="{{-- NASE IKONICE --}}"></i> Vrsta
                                    objekta i privatno vozilo
                                </legend>

                                <p>Vrsta objekta:
                                    @if($data['order']->service_home == 'house')
                                Kuća
                                    @elseif($data['order']->service_home == 'apartment')
                                Stan
                                    @endif
                                </p>

                                <p>Tehničar koristio privatno vozilo:
                                    @if($data['order']->private_vehicle == 0)
                                        Ne
                                    @elseif($data['order']->private_vehicle == 1)
                                        Da
                                    @endif
                                </p>


                            </div>
                        {{--@elseif($data['order']->order_status == 'Završen')
                            <div class="row col-12 mb-3 completed">
                                <p>Status: Završen</p>
                                <p>Vreme zatvaranja naloga: {{ datetimeForView($data['order']->completed_at)}}</p>
                            </div>--}}
                        @endif

                        <div class="order_details">

                            <legend class="font-weight-semibold"><i class="{{-- NASE IKONICE --}}"></i> Tip posla
                            </legend>

                            {{ Form::selectGroupSearch('service_type', true ,'Tip posla:', null,
               $data['services'], $data['service']->service_select,
                ['data-placeholder' => 'Odaberite tip posla',
                'class'=> 'form-control select-search'], 'form-group col-5', true) }}


                            <legend class="font-weight-semibold"><i class="{{-- NASE IKONICE --}}"></i> Naziv opreme izdate na korišćenje
                            </legend>

                            {{-- AKTIVNOSTI NA NOVOJ OPREMI --}}
                            {{-- U redu tri SELECTA sa pet redova --}}

                            <div class="row">

                                {{ Form::selectGroupSearch('equipment_subtype_1',
                                                true,
                                                'Tip opreme:',
                                                null,
                                                $data['subtype'], null,
                                                ['data-placeholder' => 'Izaberite tip opreme',
                                                 'class'=> 'form-control select-search equipment_subtype',
                                                  ], 'col-6 form-row mb-2 mr-1') }}




                                {{ Form::selectGroupSearch('equipment_serial_1',
                                                                true,
                                                                'Serijski broj opreme:',
                                                                null,
                                                                $data['all_serials'], null,
                                                                ['data-placeholder' => 'Izaberite serijski broj',
                                                                 'class'=> 'form-control select-search equipment_model',
                                                                  ], 'col-6 form-row mb-2') }}

                            </div>

                            <div class="row">

                                {{ Form::selectGroupSearch('equipment_subtype_2',
                                                false,
                                                null,
                                                null,
                                                $data['subtype'], null,
                                                ['data-placeholder' => 'Izaberite tip opreme',
                                                'id' => 'equipment_subtype_2',
                                                 'class'=> 'form-control select-search equipment_subtype',
                                                  ], 'col-6 form-row mb-2 mr-1') }}




                                {{ Form::selectGroupSearch('equipment_serial_2',
                                                                false,
                                                                null,
                                                                null,
                                                                $data['all_serials'], null,
                                                                ['data-placeholder' => 'Izaberite serijski broj',
                                                                'id' => 'equipment_serial_2',
                                                                 'class'=> 'form-control select-search',
                                                                  ], 'col-6 form-row mb-2') }}

                            </div>

                            <div class="row">

                                {{ Form::selectGroupSearch('equipment_subtype_3',
                                                false,
                                                null,
                                                null,
                                                $data['subtype'], null,
                                                ['data-placeholder' => 'Izaberite tip opreme',
                                                'id' => 'equipment_subtype_3',
                                                 'class'=> 'form-control select-search equipment_subtype',
                                                  ], 'col-6 form-row mb-2 mr-1') }}




                                {{ Form::selectGroupSearch('equipment_serial_3',
                                                                false,
                                                                null,
                                                                null,
                                                                $data['all_serials'], null,
                                                                ['data-placeholder' => 'Izaberite serijski broj',
                                                                'id' => 'equipment_serial_3',
                                                                 'class'=> 'form-control select-search',
                                                                  ], 'col-6 form-row mb-2') }}

                            </div>

                            <div class="row">

                                {{ Form::selectGroupSearch('equipment_subtype_4',
                                                false,
                                                null,
                                                null,
                                                $data['subtype'], null,
                                                ['data-placeholder' => 'Izaberite tip opreme',
                                                'id' => 'equipment_subtype_4',
                                                 'class'=> 'form-control select-search equipment_subtype',
                                                  ], 'col-6 form-row mb-2 mr-1') }}




                                {{ Form::selectGroupSearch('equipment_serial_4',
                                                                false,
                                                                null,
                                                                null,
                                                                $data['all_serials'], null,
                                                                ['data-placeholder' => 'Izaberite serijski broj',
                                                                'id' => 'equipment_serial_4',
                                                                 'class'=> 'form-control select-search',
                                                                  ], 'col-6 form-row mb-2') }}

                            </div>

                            <div class="row">

                                {{ Form::selectGroupSearch('equipment_subtype_5',
                                                false,
                                                null,
                                                null,
                                                $data['subtype'], null,
                                                ['data-placeholder' => 'Izaberite tip opreme',
                                                'id' => 'equipment_subtype_5',
                                                 'class'=> 'form-control select-search equipment_subtype',
                                                  ], 'col-6 form-row mb-2 mr-1') }}




                                {{ Form::selectGroupSearch('equipment_serial_5',
                                                                false,
                                                                null,
                                                                null,
                                                                $data['all_serials'], null,
                                                                ['data-placeholder' => 'Izaberite serijski broj',
                                                                'id' => 'equipment_serial_5',
                                                                 'class'=> 'form-control select-search',
                                                                  ], 'col-6 form-row mb-2') }}

                            </div>


                            {{-- AKTIVNOSTI NA NOVOJ OPREMI --}}

                            <legend class="font-weight-semibold"><i class="{{-- NASE IKONICE --}}"></i> Naziv opreme koja se demontira i vraća
                            </legend>

                            {{-- AKTIVNOSTI NA POSTOJECOJ OPREMI --}}
                            {{-- U redu tri SELECTA sa pet redova --}}

                            <div class="row">

                                {{ Form::selectGroupSearch('dismantled_eq_subtype_id_1',
                                                true,
                                                'Tip opreme:',
                                                null,
                                                $data['dismantled_subtype'], null,
                                                ['data-placeholder' => 'Izaberite tip opreme',
                                                 'class'=> 'form-control select-search equipment_subtype',
                                                  ], 'col-3 form-row mb-2 mr-1') }}

                                {{ Form::selectGroupSearch('dismantled_eq_model_id_1',
                                                true,
                                                'Model opreme:',
                                                null,
                                                $data['dismantled_model'], null,
                                                ['data-placeholder' => 'Izaberite tip opreme',
                                                 'class'=> 'form-control select-search equipment_subtype',
                                                 'id'=> 'dismantled_eq_model_id_1',
                                                  ], 'col-3 form-row mb-2 mr-1') }}


                                {{--{{ Form::textGroup2('order_number', 'Broj naloga:', null, null, 'form-control', ['placeholder' => 'Broj naloga'], 'col-6') }}--}}
                                <div class="form-group col-3 mb-2">
                                    <label for="dismantled_eq1_serial1">Serijski broj 1:</label>
                                    <input class="form-control" placeholder="Upisite serijski broj"
                                           name="dismantled_eq1_serial1" type="text" id="dismantled_eq1_serial1">
                                </div>

                                <div class="form-group col-3 mb-2">
                                    <label for="dismantled_eq1_serial2">Serijski broj 2:</label>
                                    <input class="form-control" placeholder="Upisite serijski broj"
                                           name="dismantled_eq1_serial2" type="text" id="dismantled_eq1_serial2">
                                </div>


                            </div>

                            <div class="row">

                                {{ Form::selectGroupSearch('dismantled_eq_subtype_id_2',
                                                false,
                                                null,
                                                null,
                                                $data['dismantled_subtype'], null,
                                                ['data-placeholder' => 'Izaberite tip opreme',
                                                 'class'=> 'form-control select-search equipment_subtype',
                                                 'id'=> 'dismantled_eq_subtype_id_2',
                                                  ], 'col-3 form-row mb-2 mr-1') }}

                                {{ Form::selectGroupSearch('dismantled_eq_model_id_2',
                                                false,
                                                null,
                                                null,
                                                $data['dismantled_model'], null,
                                                ['data-placeholder' => 'Izaberite tip opreme',
                                                 'class'=> 'form-control select-search equipment_subtype',
                                                 'id'=> 'dismantled_eq_model_id_2',
                                                  ], 'col-3 form-row mb-2 mr-1') }}

                                <div class="form-group col-3 mb-2">
                                    <input class="form-control" placeholder="Upisite serijski broj"
                                           name="dismantled_eq2_serial1" type="text" id="dismantled_eq2_serial1">
                                </div>
                                <div class="form-group col-3 mb-2">
                                    <input class="form-control" placeholder="Upisite serijski broj"
                                           name="dismantled_eq2_serial2" type="text" id="dismantled_eq2_serial2">
                                </div>
                            </div>

                            <div class="row">

                                {{ Form::selectGroupSearch('dismantled_eq_subtype_id_3',
                                                false,
                                                null,
                                                null,
                                                $data['dismantled_subtype'], null,
                                                ['data-placeholder' => 'Izaberite tip opreme',
                                                 'class'=> 'form-control select-search equipment_subtype',
                                                 'id'=> 'dismantled_eq_subtype_id_3',
                                                  ], 'col-3 form-row mb-2 mr-1') }}

                                {{ Form::selectGroupSearch('dismantled_eq_model_id_3',
                                                false,
                                                null,
                                                null,
                                                $data['dismantled_model'], null,
                                                ['data-placeholder' => 'Izaberite tip opreme',
                                                 'class'=> 'form-control select-search equipment_subtype',
                                                 'id'=> 'dismantled_eq_model_id_3',
                                                  ], 'col-3 form-row mb-2 mr-1') }}

                                <div class="form-group col-3 mb-2">
                                    <input class="form-control" placeholder="Upisite serijski broj"
                                           name="dismantled_eq3_serial1" type="text" id="dismantled_eq3_serial1">
                                </div>
                                <div class="form-group col-3 mb-2">
                                    <input class="form-control" placeholder="Upisite serijski broj"
                                           name="dismantled_eq3_serial2" type="text" id="dismantled_eq3_serial2">
                                </div>
                            </div>

                            <div class="row">

                                {{ Form::selectGroupSearch('dismantled_eq_subtype_id_4',
                                                false,
                                                null,
                                                null,
                                                $data['dismantled_subtype'], null,
                                                ['data-placeholder' => 'Izaberite tip opreme',
                                                 'class'=> 'form-control select-search equipment_subtype',
                                                 'id'=> 'dismantled_eq_subtype_id_4',
                                                  ], 'col-3 form-row mb-2 mr-1') }}

                                {{ Form::selectGroupSearch('dismantled_eq_model_id_4',
                                                false,
                                                null,
                                                null,
                                                $data['dismantled_model'], null,
                                                ['data-placeholder' => 'Izaberite tip opreme',
                                                 'class'=> 'form-control select-search equipment_subtype',
                                                 'id'=> 'dismantled_eq_model_id_4',
                                                  ], 'col-3 form-row mb-2 mr-1') }}

                                <div class="form-group col-3 mb-2">
                                    <input class="form-control" placeholder="Upisite serijski broj"
                                           name="dismantled_eq4_serial1" type="text" id="dismantled_eq4_serial1">
                                </div>
                                <div class="form-group col-3 mb-2">
                                    <input class="form-control" placeholder="Upisite serijski broj"
                                           name="dismantled_eq4_serial2" type="text" id="dismantled_eq4_serial2">
                                </div>
                            </div>

                            <div class="row">

                                {{ Form::selectGroupSearch('dismantled_eq_subtype_id_5',
                                                false,
                                                null,
                                                null,
                                                $data['dismantled_subtype'], null,
                                                ['data-placeholder' => 'Izaberite tip opreme',
                                                 'class'=> 'form-control select-search equipment_subtype',
                                                 'id'=> 'dismantled_eq_subtype_id_5',
                                                  ], 'col-3 form-row mb-2 mr-1') }}

                                {{ Form::selectGroupSearch('dismantled_eq_model_id_5',
                                                false,
                                                null,
                                                null,
                                                $data['dismantled_model'], null,
                                                ['data-placeholder' => 'Izaberite tip opreme',
                                                 'class'=> 'form-control select-search equipment_subtype',
                                                 'id'=> 'dismantled_eq_model_id_5',
                                                  ], 'col-3 form-row mb-2 mr-1') }}

                                <div class="form-group col-3 mb-2">
                                    <input class="form-control" placeholder="Upisite serijski broj"
                                           name="dismantled_eq5_serial1" type="text" id="dismantled_eq5_serial1">
                                </div>
                                <div class="form-group col-3 mb-2">
                                    <input class="form-control" placeholder="Upisite serijski broj"
                                           name="dismantled_eq5_serial2" type="text" id="dismantled_eq5_serial2">
                                </div>
                            </div>

                            {{-- Odabir materijala --}}

                            <legend class="font-weight-semibold"><i class="{{-- NASE IKONICE --}}"></i> Utrošeni
                                materijal
                            </legend>
                            <div class="row">

                                <div class="col-6">
                                    <table class="table datatable-basic table-bordered table-striped">
                                        <thead>
                                        <tr>
                                            <th>Šifra i naziv materijala</th>
                                            <th>Količina</th>
                                        </tr>
                                        </thead>
                                        {{--@dd($default_wares)--}}
                                        {{--{{ default wares }}--}}
                                        <tbody>
                                        @foreach($data['default_wares'] as $wares)
                                            <tr>
                                                <td class="lesspadding">{{ $wares->wares_code }}
                                                    - {{ $wares->wares_name }}</td>
                                                <td style="width: 140px" class="lesspadding"><input
                                                            style="width: 40px" type="number" min="1" step="1"
                                                            name="wares_{{ $wares->wares_id }}"> {{ $wares->wares_type }}
                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                        {{--{{ /$default wares }}--}}
                                    </table>

                                </div>

                                <div class="col-6">

                                    <table class="table datatable-basic table-bordered table-striped">
                                        <thead>
                                        <tr>
                                            <th>Šifra i naziv materijala</th>
                                            <th>Količina</th>
                                        </tr>
                                        </thead>
                                        {{--@dd($default_wares)--}}
                                        {{--{{ default wares }}--}}
                                        <tbody>
                                        @for($i = 1; $i <= 12; $i++)
                                            {{--@foreach($data['default_wares'] as $wares)--}}
                                            <tr>
                                                <td class="lesspadding">{{ Form::selectGroupSearch('not_default_select_'.$i, false, null, null,
                                                             $data['wares'], null,
                                                                 ['data-placeholder' => 'Odaberite materijal',
                                                                 'id' => 'not_default_select_'.$i,
                                                                 'data-id'=> $i,
                                                                 'class'=> 'form-control select-search',                                                                                          'onchange'=> 'notDefaultSelect('.$i.')',
                                                                  ], 'col-12') }}</td>
                                                <td style="width: 140px" class="lesspadding">
                                                    <input id="not_default_input_{{ $i }}" style="width: 40px"
                                                           type="number" min="1" step="1" name="not_default_input_{{ $i }}">
                                                </td>
                                            </tr>
                                        @endfor
                                        </tbody>
                                        {{--{{ /$default wares }}--}}
                                    </table>


                                </div>

                            </div>
                            {{-- /odabir materijala --}}


                            {{-- TEHNICARI --}}
                            <legend class="font-weight-semibold"><i class="{{-- NASE IKONICE --}}"></i> Izbor
                                tehnčara
                            </legend>
                            <div class="row mb-3">
                                {{--Prvi tehnicar--}}
                                {{ Form::selectGroupSearch('technician_1',
                                            true,
                                            'Prvi tehničar:',
                                            null,
                                            $data['technicians'], null,
                                            ['data-placeholder' => 'Izaberite prvog tehničara',
                                             'class'=> 'form-control select-search technician_1',
                                             'data-focus'], 'col-6 form-row mb-2', true) }}

                                {{--Drugi tehnicar--}}
                                {{ Form::selectGroupSearch('technician_2',
                                            true,
                                            'Drugi tehničar:',
                                            null,
                                            $data['technicians'], null,
                                            ['data-placeholder' => 'Izaberite drugog tehničara',
                                             'class'=> 'form-control select-search technician_2',
                                             'data-focus'], 'col-6 form-row mb-2') }}
                            </div>

                            {{-- /TEHNICARI --}}



                            {{--VRSTA OBJEKTA--}}
                            <legend class="font-weight-semibold"><i class="{{-- NASE IKONICE --}}"></i> Vrsta
                                objekta i privatno vozilo
                            </legend>
                            <div class="row">
                                <div class=" col-6">
                                    <div class="form-group">

                                        <label class="col-form-label mr-2" for="service_home">Vrsta objekta<span class="text-danger">*</span></label>
                                        <div class="form-check form-check-inline">
                                            <label class="form-check-label">
                                                <input type="radio" class="form-check-input" value="apartment"
                                                       name="service_home">Stan</label>
                                        </div>

                                        <div class="form-check form-check-inline">
                                            <label class="form-check-label">
                                                <input type="radio" class="form-check-input" value="house"
                                                       name="service_home" >Kuca</label>
                                        </div>

                                    </div>
                                </div>

                                {{--Privatno vozilo--}}
                                <div class=" col-6">
                                    <div class="form-check form-check-inline form-check-right">
                                        <label class="form-check-label">
                                            Tehničar koristio privatno vozilo
                                            <input type="checkbox" class="form-check-input" name="private_vehicle"
                                                   value="1">
                                        </label>
                                    </div>
                                </div>

                            </div>



                            {{-- vreme zatvaranja --}}
                            <legend class="font-weight-semibold"><i class="{{-- NASE IKONICE --}}"></i> Vreme zavrsavanja naloga</legend>
                            <div class="row mb-3">
                                {{--vreme zatvaranja--}}
                                {{ Form::datetime_picker('completed_at', 'Datum i vreme zatvaranja naloga:', null, null, 'form-control', ['placeholder' => 'Izaberi datum'], 'col-4', true) }}

                                <div class="col-8"></div>
                            </div>

                            {{-- /vreme zatvaranja --}}


                        </div>

                        {{-- KOMENTAR --}}
                        <legend class="font-weight-semibold"><i class="{{-- NASE IKONICE --}}"></i> Komentar
                        </legend>
                        <div class="form-group">
                                    <textarea rows="5" cols="5" id="order_comment" name="order_comment"
                                              class="form-control"
                                              placeholder="Upisite dodatne komentare">{{$data['order']->note}}</textarea>
                        </div>

                        <div class="text-right">
                            <button type="submit" class="btn bg-teal-300 save_changes">Sačuvaj izmene<i
                                        class="icon-checkmark4 ml-2"></i></button>
                        </div>
                    </fieldset>
                </div>
            </div>


            </form>

        </div>
    </div>




@endsection