@extends('layouts.app')

@push('style')
    <style>
        .nav-pills-bordered .nav-link.active {
            border-color: #4db6ac !important;
        }

        .nav-pills .nav-link.active, .nav-pills .show > .nav-link {
            color: #fff;
            background-color: #4db6ac !important;
        }


    </style>

    <!-- Global stylesheets -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet"
          type="text/css">
    <link href="../../../../global_assets/css/icons/icomoon/styles.css" rel="stylesheet" type="text/css">
    <link href="../assets/css/bootstrap.min.css" rel="stylesheet" type="text/css">
    <link href="../assets/css/bootstrap_limitless.min.css" rel="stylesheet" type="text/css">
    <link href="../assets/css/layout.min.css" rel="stylesheet" type="text/css">
    <link href="../assets/css/components.min.css" rel="stylesheet" type="text/css">
    <link href="../assets/css/colors.min.css" rel="stylesheet" type="text/css">
    <!-- /global stylesheets -->
@endpush
@push('scripts')
    {{--<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
            integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
            crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
            integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
            crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
            integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
            crossorigin="anonymous"></script>--}}

    <script src="../global_assets/js/plugins/tables/datatables/datatables.min.js"></script>
    <script src="../global_assets/js/demo_pages/datatables_basic.js"></script>
    <!-- Load select2 -->
    <script type="text/javascript" src="../global_assets/js/plugins/forms/selects/select2.min.js"></script>
    <script type="text/javascript" src="../global_assets/js/plugins/extensions/jquery_ui/interactions.min.js"></script>
    <script src="../assets/js/custom_select2.js"></script>
    <script src="../global_assets/js/plugins/forms/selects/select2.min.js"></script>
    {{--anytime--}}
    <script src="../global_assets/js/plugins/ui/moment/moment.min.js"></script>
    <script src="../global_assets/js/plugins/pickers/anytime.min.js"></script>
    <script src="../../../../global_assets/js/plugins/pickers/daterangepicker.js"></script>
    <script src="../assets/js/custom_datepicker.js"></script>

    <script src="../global_assets/js/main/jquery.min.js"></script>
    <script src="../global_assets/js/main/bootstrap.bundle.min.js"></script>
    <script src="../global_assets/js/plugins/loaders/blockui.min.js"></script>
    <script src="../global_assets/js/plugins/tables/datatables/datatables.min.js"></script>
    <script src="../global_assets/js/plugins/forms/selects/select2.min.js"></script>
    <script src="../global_assets/js/plugins/tables/datatables/extensions/jszip/jszip.min.js"></script>
    <script src="../global_assets/js/plugins/tables/datatables/extensions/buttons.min.js"></script>

    {{--export datatable--}}

    {{--anytime--}}
    <script src="../global_assets/js/plugins/ui/moment/moment.min.js"></script>
    <script src="../global_assets/js/plugins/pickers/anytime.min.js"></script>
    <script src="../../../../global_assets/js/plugins/pickers/daterangepicker.js"></script>
    <script src="../assets/js/custom_datepicker.js"></script>



    <script>

        var today = new Date();
        var dd = String(today.getDate()).padStart(2, '0');
        var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
        var yyyy = today.getFullYear();

        today = dd + '-' + mm + '-' + yyyy;
        console.log(today);


        $('.datatable-basic1').DataTable({
            autoWidth: false,
            columnDefs: [{
                /*orderable: false,*/
                width: 10,
                targets: [ 7 ]
            }],
            dom: '<"datatable-header"fBl><"datatable-scroll-wrap"t><"datatable-footer"ip>',
            language: {
                search: '<span>Pretraga:</span> _INPUT_',
                lengthMenu: '<span>Prikaz:</span> _MENU_',
                info: '_START_ do _END_ od ukupno _TOTAL_',
                paginate: {'first': 'First', 'last': 'Last', 'next': '→', 'previous': '←'}
            },
            buttons: {
                buttons: [
                    {
                        extend: 'excelHtml5',
                        className: 'btn btn-light',
                        title: 'SBB nalozi (Aktivni nalozi) ' + today,
                        sheetName: 'SBB nalozi',
                        exportOptions: {
                            columns: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
                        }
                    }
                ]
            }
        });

        $('.datatable-basic2').DataTable({
            autoWidth: false,
            dom: '<"datatable-header"fBl><"datatable-scroll-wrap"t><"datatable-footer"ip>',
            language: {
                search: '<span>Pretraga:</span> _INPUT_',
                lengthMenu: '<span>Prikaz:</span> _MENU_',
                info: '_START_ do _END_ od ukupno _TOTAL_',
                paginate: {'first': 'First', 'last': 'Last', 'next': '→', 'previous': '←'}
            },
            buttons: {
                buttons: [
                    {
                        extend: 'excelHtml5',
                        className: 'btn btn-light',
                        title: 'SBB nalozi (Otvoreni nalozi) ' + today,
                        sheetName: 'SBB nalozi',
                        exportOptions: {
                            columns: [0, 1, 2, 3, 4, 5, 6, 7, 8]
                        }
                    }
                ]
            }
        });

        $('.datatable-basic3').DataTable({
            autoWidth: false,
            dom: '<"datatable-header"fBl><"datatable-scroll-wrap"t><"datatable-footer"ip>',
            language: {
                search: '<span>Pretraga:</span> _INPUT_',
                lengthMenu: '<span>Prikaz:</span> _MENU_',
                info: '_START_ do _END_ od ukupno _TOTAL_',
                paginate: {'first': 'First', 'last': 'Last', 'next': '→', 'previous': '←'}
            },
            buttons: {
                buttons: [
                    {
                        extend: 'excelHtml5',
                        className: 'btn btn-light',
                        title: 'SBB nalozi (Zakazani nalozi) ' + today,
                        sheetName: 'SBB nalozi',
                        exportOptions: {
                            columns: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
                        }
                    }
                ]
            }
        });

        $('.datatable-basic4').DataTable({
            autoWidth: false,
            dom: '<"datatable-header"fBl><"datatable-scroll-wrap"t><"datatable-footer"ip>',
            language: {
                search: '<span>Pretraga:</span> _INPUT_',
                lengthMenu: '<span>Prikaz:</span> _MENU_',
                info: '_START_ do _END_ od ukupno _TOTAL_',
                paginate: {'first': 'First', 'last': 'Last', 'next': '→', 'previous': '←'}
            },
            buttons: {
                buttons: [
                    {
                        extend: 'excelHtml5',
                        className: 'btn btn-light',
                        title: 'SBB nalozi (Odloženi nalozi) ' + today,
                        sheetName: 'SBB nalozi',
                        exportOptions: {
                            columns: [0, 1, 2, 3, 4, 5, 6, 7, 8]
                        }
                    }
                ]
            }
        });

        $('.datatable-basic5').DataTable({
            autoWidth: false,
            dom: '<"datatable-header"fBl><"datatable-scroll-wrap"t><"datatable-footer"ip>',
            language: {
                search: '<span>Pretraga:</span> _INPUT_',
                lengthMenu: '<span>Prikaz:</span> _MENU_',
                info: '_START_ do _END_ od ukupno _TOTAL_',
                paginate: {'first': 'First', 'last': 'Last', 'next': '→', 'previous': '←'}
            },
            buttons: {
                buttons: [
                    {
                        extend: 'excelHtml5',
                        className: 'btn btn-light',
                        title: 'SBB nalozi (Otkazani nalozi) ' + today,
                        sheetName: 'SBB nalozi',
                        exportOptions: {
                            columns: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
                        }
                    }
                ]
            }
        });

        $('.datatable-basic6').DataTable({
            autoWidth: false,
            dom: '<"datatable-header"fBl><"datatable-scroll-wrap"t><"datatable-footer"ip>',
            language: {
                search: '<span>Pretraga:</span> _INPUT_',
                lengthMenu: '<span>Prikaz:</span> _MENU_',
                info: '_START_ do _END_ od ukupno _TOTAL_',
                paginate: {'first': 'First', 'last': 'Last', 'next': '→', 'previous': '←'}
            },
            buttons: {
                buttons: [
                    {
                        extend: 'excelHtml5',
                        className: 'btn btn-light',
                        title: 'SBB nalozi (Nerealizovani nalozi) ' + today,
                        sheetName: 'SBB nalozi',
                        exportOptions: {
                            columns: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
                        }
                    }
                ]
            }
        });

        $('.datatable-basic7').DataTable({
            autoWidth: false,
            dom: '<"datatable-header"fBl><"datatable-scroll-wrap"t><"datatable-footer"ip>',
            language: {
                search: '<span>Pretraga:</span> _INPUT_',
                lengthMenu: '<span>Prikaz:</span> _MENU_',
                info: '_START_ do _END_ od ukupno _TOTAL_',
                paginate: {'first': 'First', 'last': 'Last', 'next': '→', 'previous': '←'}
            },
            buttons: {
                buttons: [
                    {
                        extend: 'excelHtml5',
                        className: 'btn btn-light',
                        title: 'SBB nalozi (Nekomplatni nalozi) ' + today,
                        sheetName: 'SBB nalozi',
                        exportOptions: {
                            columns: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
                        }
                    }
                ]
            }
        });

        $('.datatable-basic8').DataTable({
            autoWidth: false,
            dom: '<"datatable-header"fBl><"datatable-scroll-wrap"t><"datatable-footer"ip>',
            language: {
                search: '<span>Pretraga:</span> _INPUT_',
                lengthMenu: '<span>Prikaz:</span> _MENU_',
                info: '_START_ do _END_ od ukupno _TOTAL_',
                paginate: {'first': 'First', 'last': 'Last', 'next': '→', 'previous': '←'}
            },
            buttons: {
                buttons: [
                    {
                        extend: 'excelHtml5',
                        className: 'btn btn-light',
                        title: 'SBB nalozi (Završeni nalozi) ' + today,
                        sheetName: 'SBB nalozi',
                        exportOptions: {
                            columns: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
                        }
                    }
                ]
            }
        });


        $('.daterange-basic').daterangepicker({
            @if( empty($date_filter))
            startDate: moment().subtract(1, 'month').startOf('month'),
            endDate: moment().subtract(1, 'month').endOf('month'),
            @endif
            maxDate: moment(),
            locale: {
                format: 'DD/MM/YYYY'
            },
            applyClass: 'bg-teal-300',
            cancelClass: 'btn-light',

        });

        // hide all form-duration-divs
        /*$('.order_details').hide();*/
        @if( !empty($date_filter))
        $(".daterangecheckbox").hide();
        $(".daterange").show();
        @endif


        $(".daterangecheckbox").change(function () {
            $(".daterangecheckbox").hide(500);

            $(".daterange").show();
            console.log('datarange');
        });

    </script>
@endpush

@section('content')
   {{-- @if(isset($orders['region_filter']))
        @dd($orders['region_filter'])
    @endif--}}
    <div>
        <div class="content">
            <div class="title m-b-md">
                <h1 style="text-align: center;">Pregled SBB naloga</h1>
            </div>

            <div class="card mb-1">
                <div class="form-group card-body  mb-0">
                    {!! Form::open(['action' => 'OrdersController@index', 'method' => 'POST']) !!}
                    <div class="daterangecheckbox">
                        <label>Filteri: </label>
                        <input type="checkbox">
                    </div>

                    <div class="row daterange" style="display: none">

                        <div class="input-group col-4" {{--style="display: block !important;"--}}>

                            <span class="input-group-text"><i class="icon-calendar3"></i></span>
                            <input name="date_filter" style="width:200px" type="text"
                                   class="form-control daterange-basic" data-placeholder="Izaberite period"
                                   {{--value="20/03/2019 - 22/03/2019"--}}
                                   @if( ! empty($date_filter))
                                   value="{{$date_filter}}"
                                    @endif
                            >
                        </div>

                        @if(isset($orders['region_filter']))

                            {{ Form::selectGroupSearch('region', false, 'Region:', null,
                  $orders['region'], $orders['region_filter'],
                     ['data-placeholder' => 'Filter za region',
                     'class'=> 'form-control select-search region'], 'col-2') }}

                        @else

                            {{ Form::selectGroupSearch('region', false, 'Region:', null,
                  $orders['region'], null,
                     ['data-placeholder' => 'Filter za region',
                     'class'=> 'form-control select-search region'], 'col-2') }}

                        @endif


                        <button type="submit" class="btn bg-teal-300 ml-3 col-2">Primeni filtere</button>
                        <div class="col-1"></div>
                        <a class="btn bg-teal-300 ml-3 col-2" href="/orders">Poništi filtere</a>
                    </div>

                </div>
            </div>

            {!! Form::close() !!}
            <div>
            {{--<button type="button" class="btn bg-teal-300" data-toggle="modal" data-target="#modal_add">Dodavanje nove opreme <i class="icon-plus3 ml-1"></i></button>--}}

            <!-- Bordered pills -->
                <div class="card">

                    <div class="card-body" style="border-bottom: 1px solid #bbbfc3;">
                        <ul class="nav nav-pills nav-pills-bordered" style="margin-bottom:0 !important;">
                            <li class="nav-item"><a href="#bordered-pill1" class="nav-link active" data-toggle="tab">Aktivni
                                    nalozi ({{$orders['aktivni_count']}})</a></li>
                            <li class="nav-item"><a href="#bordered-pill2" class="nav-link"
                                                    data-toggle="tab">Otvoreni ({{$orders['otvoreni_count']}})</a></li>
                            <li class="nav-item"><a href="#bordered-pill3" class="nav-link"
                                                    data-toggle="tab">Zakazani ({{$orders['zakazani_count']}})</a></li>
                            <li class="nav-item"><a href="#bordered-pill4" class="nav-link"
                                                    data-toggle="tab">Odložen ({{$orders['odlozeni_count']}})</a></li>
                            <li class="nav-item"><a href="#bordered-pill5" class="nav-link"
                                                    data-toggle="tab">Otkazan ({{$orders['otkazani_count']}})</a></li>
                            <li class="nav-item"><a href="#bordered-pill6" class="nav-link" data-toggle="tab">Nerealizovan
                                    ({{$orders['nerealizovani_count']}})</a>
                            </li>
                            <li class="nav-item"><a href="#bordered-pill7" class="nav-link" data-toggle="tab">Nekompletan
                                    ({{$orders['nekompletni_count']}})</a>
                            </li>
                            <li class="nav-item"><a href="#bordered-pill8" class="nav-link"
                                                    data-toggle="tab">Završen ({{$orders['zavrseni_count']}})</a></li>
                        </ul>
                    </div>


                    <div class="tab-content">
                        <div class="tab-pane fade show active" id="bordered-pill1">

                            <!-- Prikaz aktivnih naloga datatable -->

                            <table class="table datatable-basic1 table-bordered table-striped">
                                <thead>
                                <tr>
                                    <th>Redni broj naloga</th>
                                    <th>Broj naloga</th>
                                    <th>Ime i preizme</th>
                                    <th>Opština</th>
                                    <th>Region</th>
                                    <th>Adresa</th>
                                    <th>Tip posla</th>
                                    <th>Napomena</th>
                                    <th>Datum i vreme prijave</th>
                                    <th>Status naloga</th>
                                    <th></th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($orders['aktivni'] as $order)
                                    <tr>
                                        <td>{{ $order->ordinal_number }}</td>
                                        <td>{{ $order->order_number }}</td>
                                        <td>{{ $order->buyer }}</td>
                                        <td>{{ $order->township }}</td>
                                        <td>{{ $order->region }}</td>
                                        <td>{{ $order->address }} {{ $order->address_number }}</td>
                                        <td>{{ $order->service->service_name }}</td>
                                        <td>{{ $order->note }}</td>
                                        <td>{{ datetimeForSort($order->opened_at) }}</td>
                                        <td class="text-center">
                                            @if($order->order_status=='Otvoren')
                                                <span class="badge badge-success p-2"
                                                      style="font-size: 12px;">Otvoren</span></td>
                                        @elseif($order->order_status=='Zakazan')
                                            <span class="badge badge-primary p-2"
                                                  style="font-size: 12px;">Zakazan</span></td>
                                        @elseif(($order->order_status=='Završen' and $order->uncomplete == 1))
                                            <span class="badge badge-danger p-2"
                                                  style="font-size: 12px;">Nekompletan</span></td>
                                        @elseif($order->order_status=='Odložen')
                                            <span class="badge badge-warning p-2"
                                                  style="font-size: 12px;">Odlozen</span></td>
                                        @endif
                                        <td><a class="btn btn-outline-dark" href="orders/{{ $order->order_id }}/show"
                                               role="button">Prikaži nalog</a>
                                        </td>
                                    </tr>
                                @endforeach


                                </tbody>
                            </table>

                            <!-- /Prikaz aktivnih naloga datatable -->

                        </div>

                        <div class="tab-pane fade" id="bordered-pill2">
                            <!-- Otovoreni datatable -->

                            <table class="table datatable-basic2 table-bordered table-striped">
                                <thead>
                                <tr>
                                    <th>Redni broj naloga</th>
                                    <th>Broj naloga</th>
                                    <th>Ime i preizme</th>
                                    <th>Opština</th>
                                    <th>Region</th>
                                    <th>Adresa</th>
                                    <th>Tip posla</th>
                                    <th>Napomena</th>
                                    <th>Datum i vreme prijave</th>
                                    <th></th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($orders['otvoreni'] as $order)
                                    <tr>
                                        <td>{{ $order->ordinal_number }}</td>
                                        <td>{{ $order->order_number }}</td>
                                        <td>{{ $order->buyer }}</td>
                                        <td>{{ $order->township }}</td>
                                        <td>{{ $order->region }}</td>
                                        <td>{{ $order->address }} {{ $order->address_number }}</td>
                                        <td>{{ $order->service->service_name }}</td>
                                        <td>{{ $order->note }}</td>
                                        <td>{{ datetimeForSort($order->opened_at) }}</td>
                                        <td><a class="btn btn-outline-dark" href="orders/{{ $order->order_id }}/show"
                                               role="button">Prikaži nalog</a>
                                        </td>
                                    </tr>
                                @endforeach


                                </tbody>
                            </table>

                            <!-- /Otovoreni datatable -->
                        </div>


                        <div class="tab-pane fade" id="bordered-pill3">
                            <!-- zakazani datatable -->


                            <table class="table datatable-basic3 table-bordered table-striped">
                                <thead>
                                <tr>
                                    <th>Redni broj naloga</th>
                                    <th>Broj naloga</th>
                                    <th>Ime i preizme</th>
                                    <th>Opština</th>
                                    <th>Region</th>
                                    <th>Adresa</th>
                                    <th>Tip posla</th>
                                    <th>Napomena</th>
                                    <th>Datum i vreme prijave</th>
                                    <th>Zakazano vreme</th>
                                    <th></th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($orders['zakazani'] as $order)
                                    <tr>
                                        <td>{{ $order->ordinal_number }}</td>
                                        <td>{{ $order->order_number }}</td>
                                        <td>{{ $order->buyer }}</td>
                                        <td>{{ $order->township }}</td>
                                        <td>{{ $order->region }}</td>
                                        <td>{{ $order->address }} {{ $order->address_number }}</td>
                                        <td>{{ $order->service->service_name }}</td>
                                        <td>{{ $order->note }}</td>
                                        <td>{{ datetimeForSort($order->opened_at) }}</td>
                                        <td>{{ datetimeForSort($order->scheduled_at) }}</td>
                                        <td><a class="btn btn-outline-dark" href="orders/{{ $order->order_id }}/show"
                                               role="button">Prikaži nalog</a>
                                        </td>
                                    </tr>
                                @endforeach


                                </tbody>
                            </table>

                            <!-- /zakazani datatable -->
                        </div>

                        <div class="tab-pane fade" id="bordered-pill4">
                            <!-- odlozeni  -->


                            <table class="table datatable-basic4 table-bordered table-striped">
                                <thead>
                                <tr>
                                    <th>Redni broj naloga</th>
                                    <th>Broj naloga</th>
                                    <th>Ime i preizme</th>
                                    <th>Opština</th>
                                    <th>Region</th>
                                    <th>Adresa</th>
                                    <th>Tip posla</th>
                                    <th>Napomena</th>
                                    <th>Datum i vreme prijave</th>
                                    <th></th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($orders['odlozeni'] as $order)
                                    <tr>
                                        <td>{{ $order->ordinal_number }}</td>
                                        <td>{{ $order->order_number }}</td>
                                        <td>{{ $order->buyer }}</td>
                                        <td>{{ $order->township }}</td>
                                        <td>{{ $order->region }}</td>
                                        <td>{{ $order->address }} {{ $order->address_number }}</td>
                                        <td>{{ $order->service->service_name }}</td>
                                        <td>{{ $order->note }}</td>
                                        <td>{{ datetimeForSort($order->opened_at) }}</td>
                                        <td><a class="btn btn-outline-dark" href="orders/{{ $order->order_id }}/show"
                                               role="button">Prikaži nalog</a>
                                        </td>
                                    </tr>
                                @endforeach


                                </tbody>
                            </table>

                            <!-- /odlozeni -->
                        </div>

                        <!-- otkazan  -->
                        <div class="tab-pane fade" id="bordered-pill5">


                            <table class="table datatable-basic5 table-bordered table-striped">
                                <thead>
                                <tr>
                                    <th>Redni broj naloga</th>
                                    <th>Broj naloga</th>
                                    <th>Ime i preizme</th>
                                    <th>Opština</th>
                                    <th>Region</th>
                                    <th>Adresa</th>
                                    <th>Tip posla</th>
                                    <th>Napomena</th>
                                    <th>Datum i vreme prijave</th>
                                    <th>Vreme zatvaranja naloga</th>
                                    <th></th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($orders['otkazani'] as $order)
                                    <tr>
                                        <td>{{ $order->ordinal_number }}</td>
                                        <td>{{ $order->order_number }}</td>
                                        <td>{{ $order->buyer }}</td>
                                        <td>{{ $order->township }}</td>
                                        <td>{{ $order->region }}</td>
                                        <td>{{ $order->address }} {{ $order->address_number }}</td>
                                        <td>{{ $order->service->service_name }}</td>
                                        <td>{{ $order->note }}</td>
                                        <td>{{ datetimeForSort($order->opened_at) }}</td>
                                        <td>{{ datetimeForSort($order->updated_at) }}</td>
                                        <td><a class="btn btn-outline-dark" href="orders/{{ $order->order_id }}/show"
                                               role="button">Prikaži nalog</a>
                                        </td>
                                    </tr>
                                @endforeach


                                </tbody>
                            </table>


                        </div>
                        <!-- /otkazan -->

                        <!-- nerealizovan  -->
                        <div class="tab-pane fade" id="bordered-pill6">


                            <table class="table datatable-basic6 table-bordered table-striped">
                                <thead>
                                <tr>
                                    <th>Redni broj naloga</th>
                                    <th>Broj naloga</th>
                                    <th>Ime i preizme</th>
                                    <th>Opština</th>
                                    <th>Region</th>
                                    <th>Adresa</th>
                                    <th>Tip posla</th>
                                    <th>Napomena</th>
                                    <th>Datum i vreme prijave</th>
                                    <th>Vreme zatvaranja naloga</th>
                                    <th></th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($orders['nerealizovani'] as $order)
                                    <tr>
                                        <td>{{ $order->ordinal_number }}</td>
                                        <td>{{ $order->order_number }}</td>
                                        <td>{{ $order->buyer }}</td>
                                        <td>{{ $order->township }}</td>
                                        <td>{{ $order->region }}</td>
                                        <td>{{ $order->address }} {{ $order->address_number }}</td>
                                        <td>{{ $order->service->service_name }}</td>
                                        <td>{{ $order->note }}</td>
                                        <td>{{ datetimeForSort($order->opened_at) }}</td>
                                        <td>{{ datetimeForSort($order->completed_at) }}</td>
                                        <td><a class="btn btn-outline-dark" href="orders/{{ $order->order_id }}/show"
                                               role="button">Prikaži nalog</a>
                                        </td>
                                    </tr>
                                @endforeach


                                </tbody>
                            </table>


                        </div>
                        <!-- /nerealizovan -->

                        <!-- Nekompletan  -->
                        <div class="tab-pane fade" id="bordered-pill7">


                            <table class="table datatable-basic7 table-bordered table-striped">
                                <thead>
                                <tr>
                                    <th>Redni broj naloga</th>
                                    <th>Broj naloga</th>
                                    <th>Ime i preizme</th>
                                    <th>Opština</th>
                                    <th>Region</th>
                                    <th>Adresa</th>
                                    <th>Tip posla</th>
                                    <th>Napomena</th>
                                    <th>Datum i vreme prijave</th>
                                    <th>Vreme izmene naloga</th>
                                    <th></th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($orders['nekompletni'] as $order)
                                    <tr>
                                        <td>{{ $order->ordinal_number }}</td>
                                        <td>{{ $order->order_number }}</td>
                                        <td>{{ $order->buyer }}</td>
                                        <td>{{ $order->township }}</td>
                                        <td>{{ $order->region }}</td>
                                        <td>{{ $order->address }} {{ $order->address_number }}</td>
                                        <td>{{ $order->service->service_name }}</td>
                                        <td>{{ $order->note }}</td>
                                        <td>{{ datetimeForSort($order->opened_at) }}</td>
                                        <td>{{ datetimeForSort($order->updated_at) }}</td>
                                        <td><a class="btn btn-outline-dark" href="orders/{{ $order->order_id }}/show"
                                               role="button">Prikaži nalog</a>
                                        </td>
                                    </tr>
                                @endforeach


                                </tbody>
                            </table>


                        </div>
                        <!-- /Nekompletan -->

                        <!-- Završen  -->
                        <div class="tab-pane fade" id="bordered-pill8">


                            <table class="table datatable-basic8 table-bordered table-striped">
                                <thead>
                                <tr>
                                    <th>Redni broj naloga</th>
                                    <th>Broj naloga</th>
                                    <th>Ime i preizme</th>
                                    <th>Opština</th>
                                    <th>Region</th>
                                    <th>Adresa</th>
                                    <th>Tip posla</th>
                                    <th>Napomena</th>
                                    <th>Datum i vreme prijave</th>
                                    <th>Vreme zatvaranja naloga</th>
                                    <th></th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($orders['zavrseni'] as $order)
                                    <tr>
                                        <td>{{ $order->ordinal_number }}</td>
                                        <td>{{ $order->order_number }}</td>
                                        <td>{{ $order->buyer }}</td>
                                        <td>{{ $order->township }}</td>
                                        <td>{{ $order->region }}</td>
                                        <td>{{ $order->address }} {{ $order->address_number }}</td>
                                        <td>{{ $order->service->service_name }}</td>
                                        <td>{{ $order->note }}</td>
                                        <td>{{ datetimeForSort($order->opened_at) }}</td>
                                        <td>{{ $order->completed_at }}</td>
                                        <td><a class="btn btn-outline-dark" href="orders/{{ $order->order_id }}/show"
                                               role="button">Prikaži nalog</a>
                                        </td>
                                    </tr>
                                @endforeach


                                </tbody>
                            </table>


                        </div>
                        <!-- /Završen -->

                    </div>
                </div>
                <!-- /Bordered pills -->


            </div>
        </div>
    </div>

@endsection



