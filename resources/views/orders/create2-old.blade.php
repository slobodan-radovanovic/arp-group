@extends('layouts.app')

@push('scripts')


  <script src="../global_assets/js/plugins/ui/moment/moment.min.js"></script>
  <script src="../global_assets/js/plugins/pickers/anytime.min.js"></script>



  <script src="../assets/js/custom_datepicker.js"></script>


  <!-- Load select2 -->
  <script type="text/javascript" src="../global_assets/js/plugins/forms/selects/select2.min.js"></script>
  <script type="text/javascript" src="../global_assets/js/plugins/extensions/jquery_ui/interactions.min.js"></script>
  <script src="../assets/js/custom_select2.js"></script>


@endpush

@section('content')
  <div class="title m-b-md mt-3">
    <h1 style="text-align: center;">Kreiranje SBB naloga</h1>
  </div>

  <div class="card mt-1">

    <div class="card-header header-elements-inline">
      <h4 class="card-title">Osnovni podaci naloga</h4>

      <div class="header-elements">
        <div class="list-icons">
          <a class="list-icons-item" data-action="collapse"></a>
        </div>
      </div>
    </div>

    <div class="card-body">
      {!! Form::open(['id' => 'form-create-order', 'role' => 'form', 'class' => 'form-loading-button']) !!}
      <div class="row">

        {{-- LEVA STRANA --}}
        <div class="col-8">
          <fieldset>
            <legend class="font-weight-semibold"><i class="icon-reading mr-2"></i> Podaci o kupcu</legend>

            {{-- KUPAC --}}
            {{-- U redu tri inputa --}}
            <div class="row">

              {{--'name', 'labelvalue', 'classlabel', 'value' => null, 'classtext', 'attributes' => ['required' => 'required'], 'col'--}}


              {{--<div class="col-md-3">
                 <div class="form-group">
                  <label for="buyer_id">Broj kupca:</label>
                  <input type="text" placeholder="Broj kupca" id="buyer_id" class="form-control">
                </div>
              </div>--}}
              {{ Form::textGroup2('buyer_id', 'Broj kupca:', null, null, 'form-control', ['placeholder' => 'Broj kupca'], 'col-3') }}


              {{--<div class="col-md-3">
                <div class="form-group">
                  <label for="treaty_id">Broj ugovora:</label>
                  <input type="text" placeholder="Broj ugovora" id="treaty_id" class="form-control">
                </div>
                </div>--}}
              {{ Form::textGroup2('treaty_id', 'Broj ugovora:', null, null, 'form-control', ['placeholder' => 'Broj ugovora'], 'col-3') }}

              {{--<div class="col-md-6">
                <div class="form-group">
                  <label for="buyer">Ime i prezime kupca:</label>
                  <input type="text" placeholder="Ime i prezime kupca" id="buyer" class="form-control">
                </div>
              </div>--}}

              {{ Form::textGroup2('buyer', 'Ime i prezime kupca:', null, null, 'form-control', ['placeholder' => 'Ime i prezime kupca'], 'col-6') }}

            </div>


            {{-- KUPAC --}}

            {{-- POST GRAD Opština --}}
            {{-- U redu tri inputa sa selectom--}}
            <div class="row">
              <div class="col-md-3">
                <div class="form-group">
                  <label for="area_code">Poštanski broj:</label>
                  <input type="text" placeholder="Poštanski broj" id="area_code" class="form-control">
                </div>
              </div>
              <div class="col-md-3">
                <div class="form-group">
                  <label for="area_code">Grad:</label>
                  <input type="text" value="Beograd" readonly id="city" class="form-control">
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label for="township">Odaberite opštinu:</label>
                  <select data-placeholder="Odaberite opštinu" id="township" class="form-control select-search township" data-fouc>
                    <option>
                    <option value="opt1">Novi Beograd
                    <option value="opt2">Čukarica
                    <option value="opt3">Palilula
                    <option value="opt4">Rakovica
                    <option value="opt5">Savski venac
                    <option value="opt6">Stari grad
                    <option value="opt7">Voždovac
                    <option value="opt8">Vračar
                    <option value="opt9">Zemun
                    <option value="opt10">Zvezdara
                    <option value="opt11">Barajevo
                    <option value="opt12">Grocka
                    <option value="opt13">Lazarevac
                    <option value="opt14">Mladenovac
                    <option value="opt15">Obrenovac
                    <option value="opt16">Sopot
                    <option value="opt17">Surčin
                  </select>
                </div>


                {{--<div class="form-group">
                  <label for="township">Odaberite opstinu:</label>
                    <select class="form-control">
                      <option value="opt1">Novi Beograd
                      <option value="opt2">Čukarica
                      <option value="opt3">Palilula
                      <option value="opt4">Rakovica
                      <option value="opt5">Savski venac
                      <option value="opt6">Stari grad
                      <option value="opt7">Voždovac
                      <option value="opt8">Vračar
                      <option value="opt9">Zemun
                      <option value="opt10">Zvezdara
                      <option value="opt11">Barajevo
                      <option value="opt12">Grocka
                      <option value="opt13">Lazarevac
                      <option value="opt14">Mladenovac
                      <option value="opt15">Obrenovac
                      <option value="opt16">Sopot
                      <option value="opt17">Surčin
                    </select>
                </div>--}}
              </div>
            </div>
            {{-- POST GRAD Opština --}}

            {{-- ADRESA --}}
            {{-- U redu cetiri inputa --}}
            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <label for="address">Adresa:</label>
                  <input type="text" placeholder="Naziv ulice" id="address" class="form-control">
                </div>
              </div>
              <div class="col-md-2">
                <div class="form-group">
                  <label for="address_number">broj:</label>
                  <input type="text" placeholder="broj" id="address_number" class="form-control">
                </div>
              </div>
              <div class="col-md-2">
                <div class="form-group">
                  <label for="floor">sprat:</label>
                  <input type="text" placeholder="sprat" id="floor" class="form-control">
                </div>
              </div>
              <div class="col-md-2">
                <div class="form-group">
                  <label for="apartment">stan:</label>
                  <input type="text" placeholder="stan" id="apartment" class="form-control">
                </div>
              </div>
            </div>
            {{-- ADRESA --}}

            {{-- TELEFONI --}}
            {{-- U redu dva inputa --}}
            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <label for="phone_number">Broj telefona:</label>
                  <input type="text" placeholder="Broj telefona" id="phone_number" class="form-control">
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label for="mobile_number">Mobilni:</label>
                  <input type="text" placeholder="Mobilni" id="mobile_number" class="form-control">
                </div>
              </div>
            </div>
            {{-- TELEFONI --}}

          </fieldset>
        </div>

        {{-- DESNA STRANA --}}
        <div class="col-4">
          <fieldset>
            <legend class="font-weight-semibold"><i class="icon-truck mr-2"></i> Podaci o nalogu</legend>

            {{-- NALOG START DATUM --}}
            {{-- U redu dva inputa --}}
            <div class="row">
              <div class="col-10">
                {{--<div class="form-group">
                  <label for="order_start_datetime">Datum i vreme prijave:</label>
                  <input type="text" placeholder="" id="order_start_datetime" class="form-control">
                </div>--}}
                <label for="order_start_datetime">Datum i vreme prijave:</label>

                {{--<br>
                <input type="text" class="form-control pickadate" placeholder="Try me">
                <br>--}}
                <div class="input-group">
                  <span class="input-group-prepend">
                    <span class="input-group-text"><i class="icon-calendar3"></i></span>
                  </span>
                  <input placeholder="Izaberi datum" type="text" class="form-control" id="opened_at_datetime" value="">
                </div>

                {{--<div class="input-group">
                  <span class="input-group-prepend">
                    <button type="button" class="btn btn-light btn-icon" id="ButtonCreationDemoButton"> <i class="icon-calendar3"></i></button>
                  </span>
                  <input type="text" class="form-control opened_at" id="ButtonCreationDemoInput" placeholder="Izaberi datum" readonly="">
                </div>--}}



              </div>

            </div>
            {{-- NALOG START DATUM --}}

            {{-- NALOG PREDVIDJEN RADA --}}
            {{-- U redu dva inputa --}}
            <div class="row mt-3">
              <div class="col-10">
                <div class="form-group">
                  <label for="order_number">Broj naloga:</label>
                  <input type="text" placeholder="Broj naloga" id="order_number" class="form-control">
                </div>
              </div>

            </div>

            <div class="row">
              <div class="col-10">
                <label for="order_start_datetime">Predvidjeni pocetak rada:</label>
                <div class="input-group">
                  <span class="input-group-prepend">
                    <span class="input-group-text"><i class="icon-calendar3"></i></span>
                  </span>
                  <input placeholder="Izaberi datum" type="text" class="form-control" id="started_at_datetime" value="">
                </div>
              </div>
            </div>
            {{-- NALOG PREDVIDJEN RADA --}}

            {{-- TEHNICARI --}}
            {{-- U redu dva SELECTA --}}
            {{--<div class="row mt-3">
              <div class="col-md-6">
                <div class="form-group">
                  <label for="technician1">Odaberite prvog tehnicara:</label>
                  <select class="form-control">
                    <option value="Pera Peri">Pera Peric
                    <option value="Nikola Nikolic">Nikola Nikolic
                    <option value="opt3">Marko Markovic
                    <option value="opt4">Zarko Zarkovic
                    <option value="opt5">Nenad Nenadovic
                    <option value="opt6">Mirko Mirkovic
                    <option value="opt7">Predrag Predragovic
                  </select>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label for="technician2">Odaberite drugog tehnicara:</label>
                  <select class="form-control">
                    <option value="empty">
                    <option value="Pera Peric">Pera Peric
                    <option value="Nikola Nikolic">Nikola Nikolic
                    <option value="opt3">Marko Markovic
                    <option value="opt4">Zarko Zarkovic
                    <option value="opt5">Nenad Nenadovic
                    <option value="opt6">Mirko Mirkovic
                    <option value="opt7">Predrag Predragovic
                  </select>
                </div>
              </div>
            </div>--}}
            {{-- TEHNICARI --}}


            {{-- Ima li tehnicar auto --}}
            {{--<div class="row">
              <label class="col-form-label col-lg-3" for="technician_vehicle">Tehnicar koristi vozilo</label>
              <div class="form-check form-check-inline">
                <label class="form-check-label">
                  <input type="radio" class="form-check-input" name="technician_vehicle" checked="" >DA</label>
              </div>
              <div class="form-check form-check-inline">
                <label class="form-check-label">
                  <input type="radio" class="form-check-input" name="technician_vehicle" >NE</label>
              </div>
            </div>--}}
            {{-- Ima li tehnicar auto --}}

          </fieldset>
        </div>
      </div>

      </form>
    </div>
  </div>

  {{--   ************************************************************************************************************************************** --}}
  <div class="card">

    <div class="card-header header-elements-inline">
      <h4 class="card-title">Opis radova i podaci o opremi - planirano</h4>
      <div class="header-elements">
        <div class="list-icons">
          <a class="list-icons-item" data-action="collapse"></a>
          {{-- <a class="list-icons-item" data-action="reload"></a>--}}
          {{--  NE FUNKCIONISE RELOAD DUGME --}}
        </div>
      </div>
    </div>

    <div class="card-body">
      <form action="#">
        <div class="row">

          {{-- CELA STRANA --}}
          <div class="col-md-12">
            <fieldset>
              <legend class="font-weight-semibold"><i class="{{-- NASE IKONICE --}}"></i> Opis radova</legend>

              {{-- OPIS RADOVA --}}
              {{-- U redu tri SELECTA sa pet redova --}}
              <div class="row">

                {{--<div class="col-md-4">
                  <div class="form-group row">
                    <label for="service_number1">Stavka:</label>
                    <select class="js-example-basic-single" name="state">
                      <option value="AL">Alabama
                      <option value="WY">Wyoming
                    </select>
                  </div>
                  <div class="form-group row">
                    <label for="service_number2"></label>
                    <select class="form-control">
                      <option value="empty">
                      <option value="10">Stavka 10
                      <option value="11">Stavka 11
                      <option value="12">Stavka 12
                    </select>
                  </div>
                  <div class="form-group row">
                    <label for="service_number3"></label>
                    <select class="form-control">
                      <option value="empty">
                      <option value="10">Stavka 10
                      <option value="11">Stavka 11
                      <option value="12">Stavka 12
                    </select>
                  </div>
                  <div class="form-group row">
                    <label for="service_number4"></label>
                    <select class="form-control">
                      <option value="empty">
                      <option value="10">Stavka 10
                      <option value="11">Stavka 11
                      <option value="12">Stavka 12
                    </select>
                  </div>
                  <div class="form-group row">
                    <label for="service_number5"></label>
                    <select class="form-control">
                      <option value="empty">
                      <option value="10">Stavka 10
                      <option value="11">Stavka 11
                      <option value="12">Stavka 12
                    </select>
                  </div>
                </div>

                <div class="col-md-4">
                  <div class="form-group row">
                    <label for="service1">Usluga:</label>
                    <select class="form-control">
                      <option value="PLAN007">PLAN007
                      <option value="PLAN009">PLAN009
                      <option value="PLAN0014">PLAN0014
                    </select>
                  </div>
                  <div class="form-group row">
                    <label for="service2"></label>
                    <select class="form-control">
                      <option value="empty">
                      <option value="PLAN007">PLAN007
                      <option value="PLAN009">PLAN009
                      <option value="PLAN0014">PLAN0014
                    </select>
                  </div>
                  <div class="form-group row">
                    <label for="service3"></label>
                    <select class="form-control">
                      <option value="empty">
                      <option value="PLAN007">PLAN007
                      <option value="PLAN009">PLAN009
                      <option value="PLAN0014">PLAN0014
                    </select>
                  </div>
                  <div class="form-group row">
                    <label for="service4"></label>
                    <select class="form-control">
                      <option value="empty">
                      <option value="PLAN007">PLAN007
                      <option value="PLAN009">PLAN009
                      <option value="PLAN0014">PLAN0014
                    </select>
                  </div>
                  <div class="form-group row">
                    <label for="service5"></label>
                    <select class="form-control">
                      <option value="empty">
                      <option value="PLAN007">PLAN007
                      <option value="PLAN009">PLAN009
                      <option value="PLAN0014">PLAN0014
                    </select>
                  </div>
                </div>--}}

                <div class="col-md-5">
                  <div class="form-row mb-2">
                    <label>Tip posla:</label><select name="service1" class="form-control select service_type1" data-fouc data-placeholder="Izaberite tip posla">
                      <option>
                      <option value="AZ">Prvo Priključenje
                      <option value="CO">Dodatno prikljukenje
                      <option value="ID">Preseljenje
                      <option value="WY">Zamena opreme
                      <option value="AL">Dorada instalacije
                      <option value="AR">Priključenje 2/3 TV
                    </select>
                  </div>

                  <div class="form-row mb-2">
                    <select name="service2" class="form-control select" data-fouc data-placeholder="Izaberite tip posla">
                      <option>
                      <option value="AZ">Prvo Priključenje
                      <option value="CO">Dodatno prikljukenje
                      <option value="ID">Preseljenje
                      <option value="WY">Zamena opreme
                      <option value="AL">Dorada instalacije
                      <option value="AR">Priključenje 2/3 TV
                    </select>
                  </div>

                  <div class="form-row mb-2">
                    <select name="service3" class="form-control select" data-fouc data-placeholder="Izaberite tip posla">
                      <option>
                      <option value="AZ">Prvo Priključenje
                      <option value="CO">Dodatno prikljukenje
                      <option value="ID">Preseljenje
                      <option value="WY">Zamena opreme
                      <option value="AL">Dorada instalacije
                      <option value="AR">Priključenje 2/3 TV
                    </select>
                  </div>

                  <div class="form-row mb-2">
                    <select name="service4" class="form-control select" data-fouc data-placeholder="Izaberite tip posla">
                      <option>
                      <option value="AZ">Prvo Priključenje
                      <option value="CO">Dodatno prikljukenje
                      <option value="ID">Preseljenje
                      <option value="WY">Zamena opreme
                      <option value="AL">Dorada instalacije
                      <option value="AR">Priključenje 2/3 TV
                    </select>
                  </div>

                  <div class="form-row mb-4">
                    <select name="service5" class="form-control select" data-fouc data-placeholder="Izaberite tip posla">
                      <option>
                      <option value="AZ">Prvo Priključenje
                      <option value="CO">Dodatno prikljukenje
                      <option value="ID">Preseljenje
                      <option value="WY">Zamena opreme
                      <option value="AL">Dorada instalacije
                      <option value="AR">Priključenje 2/3 TV
                    </select>
                  </div>


                  {{--<div class="form-group">
                      <label for="service_type2"></label>
                      <select class="form-control">
                        <option value="empty">
                        <option value="samo dod. prik.">samo dod. prik.
                        <option value="dod. prik. sa potpisom ugovora">dod. prik. sa potpisom ugovora
                        <option value="dod. prik. sa aneksom ugovora">dod. prik. sa aneksom ugovora
                      </select>
                    </div>
                  <div class="form-group">
                      <label for="service_type3"></label>
                      <select class="form-control">
                        <option value="empty">
                        <option value="samo dod. prik.">samo dod. prik.
                        <option value="dod. prik. sa potpisom ugovora">dod. prik. sa potpisom ugovora
                        <option value="dod. prik. sa aneksom ugovora">dod. prik. sa aneksom ugovora
                      </select>
                    </div>
                  <div class="form-group">
                      <label for="service_type4"></label>
                      <select class="form-control">
                        <option value="empty">
                        <option value="samo dod. prik.">samo dod. prik.
                        <option value="dod. prik. sa potpisom ugovora">dod. prik. sa potpisom ugovora
                        <option value="dod. prik. sa aneksom ugovora">dod. prik. sa aneksom ugovora
                      </select>
                    </div>
                  <div class="form-group">
                      <label for="service_type5"></label>
                      <select class="form-control">
                        <option value="empty">
                        <option value="samo dod. prik.">samo dod. prik.
                        <option value="dod. prik. sa potpisom ugovora">dod. prik. sa potpisom ugovora
                        <option value="dod. prik. sa aneksom ugovora">dod. prik. sa aneksom ugovora
                      </select>
                    </div>--}}
                </div>

              </div>
              {{-- OPIS RADOVA --}}

              <legend class="font-weight-semibold"><i class="{{-- NASE IKONICE --}}"></i> Aktivnosti na novoj opremi</legend>

              {{-- AKTIVNOSTI NA NOVOJ OPREMI --}}
              {{-- U redu tri SELECTA sa pet redova --}}
              <div class="row">

                <div class="col-md-4">
                  <div class="form-row mb-2">
                    <label for="eq_type1">Vrsta opreme:</label>
                    <select name="eq_type1" class="form-control select" data-fouc data-placeholder="Izaberite vrstu opreme">
                      <option>
                      <option value="AZ">Video basic STB
                      <option value="CO">Internet basic eq
                      <option value="ID">Digital receiver
                    </select>
                  </div>

                  <div class="form-row mb-2">
                    <select name="eq_type2" class="form-control select" data-fouc data-placeholder="Izaberite vrstu opreme">
                      <option>
                      <option value="AZ">Video basic STB
                      <option value="CO">Internet basic eq
                      <option value="ID">Digital receiver
                    </select>
                  </div>

                  <div class="form-row mb-2">
                    <select name="eq_type3" class="form-control select" data-fouc data-placeholder="Izaberite vrstu opreme">
                      <option>
                      <option value="AZ">Video basic STB
                      <option value="CO">Internet basic eq
                      <option value="ID">Digital receiver
                    </select>
                  </div>

                  <div class="form-row mb-2">
                    <select name="eq_type4" class="form-control select" data-fouc data-placeholder="Izaberite vrstu opreme">
                      <option>
                      <option value="AZ">Video basic STB
                      <option value="CO">Internet basic eq
                      <option value="ID">Digital receiver
                    </select>
                  </div>

                  <div class="form-row mb-2">
                    <select name="eq_type5" class="form-control select" data-fouc data-placeholder="Izaberite vrstu opreme">
                      <option>
                      <option value="AZ">Video basic STB
                      <option value="CO">Internet basic eq
                      <option value="ID">Digital receiver
                    </select>
                  </div>
                  {{--<div class="form-row">
                    <label for="eq_type2"></label>
                    <select class="form-control">
                      <option value="empty">
                      <option value="10">Video basic STB
                      <option value="11">Internet basic eq
                      <option value="12">Digital receiver
                    </select>
                  </div>
                  <div class="form-row ">
                    <label for="eq_type3"></label>
                    <select class="form-control">
                      <option value="empty">
                      <option value="10">Video basic STB
                      <option value="11">Internet basic eq
                      <option value="12">Digital receiver
                    </select>
                  </div>
                  <div class="form-row">
                    <label for="eq_type4"></label>
                    <select class="form-control">
                      <option value="empty">
                      <option value="10">Video basic STB
                      <option value="11">Internet basic eq
                      <option value="12">Digital receiver
                    </select>
                  </div>
                  <div class="form-group row">
                    <label for="eq_type5"></label>
                    <select class="form-control">
                        <option value="empty">
                        <option value="10">Video basic STB
                        <option value="11">Internet basic eq
                        <option value="12">Digital receiver
                    </select>
                  </div>--}}
                </div>

                <div class="col-md-4">
                  <div class="form-row mb-2">
                    <label for="eq_type1">Tip opreme:</label>
                    <select name="eq_model1" class="form-control select" data-fouc data-placeholder="Izaberite tip opreme">
                      <option>
                      <option value="PLAN007">D3 1 way receiver
                      <option value="PLAN009">Cable home gateway
                      <option value="PLAN0014">Proffesional gateway
                    </select>
                  </div>

                  <div class="form-row mb-2">
                    <select name="eq_model2" class="form-control select" data-fouc data-placeholder="Izaberite tip opreme">
                      <option>
                      <option value="PLAN007">D3 1 way receiver
                      <option value="PLAN009">Cable home gateway
                      <option value="PLAN0014">Proffesional gateway
                    </select>
                  </div>

                  <div class="form-row mb-2">
                    <select name="eq_model3" class="form-control select" data-fouc data-placeholder="Izaberite tip opreme">
                      <option>
                      <option value="PLAN007">D3 1 way receiver
                      <option value="PLAN009">Cable home gateway
                      <option value="PLAN0014">Proffesional gateway
                    </select>
                  </div>

                  <div class="form-row mb-2">
                    <select name="eq_model4" class="form-control select" data-fouc data-placeholder="Izaberite tip opreme">
                      <option>
                      <option value="PLAN007">D3 1 way receiver
                      <option value="PLAN009">Cable home gateway
                      <option value="PLAN0014">Proffesional gateway
                    </select>
                  </div>

                  <div class="form-row mb-2">
                    <select name="eq_model5" class="form-control select" data-fouc data-placeholder="Izaberite tip opreme">
                      <option>
                      <option value="PLAN007">D3 1 way receiver
                      <option value="PLAN009">Cable home gateway
                      <option value="PLAN0014">Proffesional gateway
                    </select>
                  </div>

                  {{--<div class="form-row">
                    <label for="eq_model1">Tip opreme:</label>
                    <select class="form-control">
                      <option value="PLAN007">D3 1 way receiver
                      <option value="PLAN009">Cable home gateway
                      <option value="PLAN0014">Proffesional gateway
                    </select>
                  </div>
                  <div class="form-group row">
                    <label for="eq_model2"></label>
                    <select class="form-control">
                      <option value="empty">
                      <option value="PLAN007">D3 1 way receiver
                      <option value="PLAN009">Cable home gateway
                      <option value="PLAN0014">Proffesional gateway
                    </select>
                  </div>
                  <div class="form-group row">
                    <label for="eq_model3"></label>
                    <select class="form-control">
                      <option value="empty">
                      <option value="PLAN007">D3 1 way receiver
                      <option value="PLAN009">Cable home gateway
                      <option value="PLAN0014">Proffesional gateway
                    </select>
                  </div>
                  <div class="form-group row">
                    <label for="eq_model4"></label>
                    <select class="form-control">
                      <option value="empty">
                      <option value="PLAN007">D3 1 way receiver
                      <option value="PLAN009">Cable home gateway
                      <option value="PLAN0014">Proffesional gateway
                    </select>
                  </div>
                  <div class="form-group row">
                    <label for="eq_model5"></label>
                    <select class="form-control">
                      <option value="empty">
                      <option value="PLAN007">D3 1 way receiver
                      <option value="PLAN009">Cable home gateway
                      <option value="PLAN0014">Proffesional gateway
                    </select>
                  </div>--}}
                </div>

                <div class="col-md-4">
                  <div class="form-row mb-2">
                    <label>Opis aktivnosti:</label>
                    <select name="eq_activity1" class="form-control select" data-fouc data-placeholder="Opis aktivnosti">
                      <option>
                      <option value="PLAN007">Instalacija na terenu
                    </select>
                  </div>

                  <div class="form-row mb-2">
                    <select name="eq_activity2" class="form-control select" data-fouc data-placeholder="Opis aktivnosti">
                      <option>
                      <option value="PLAN007">Instalacija na terenu
                    </select>
                  </div>

                  <div class="form-row mb-2">
                    <select name="eq_activity3" class="form-control select" data-fouc data-placeholder="Opis aktivnosti">
                      <option>
                      <option value="PLAN007">Instalacija na terenu
                    </select>
                  </div>

                  <div class="form-row mb-2">
                    <select name="eq_activity4" class="form-control select" data-fouc data-placeholder="Opis aktivnosti">
                      <option>
                      <option value="PLAN007">Instalacija na terenu
                    </select>
                  </div>

                  <div class="form-row mb-4">
                    <select name="eq_activity5" class="form-control select" data-fouc data-placeholder="Opis aktivnosti">
                      <option>
                      <option value="PLAN007">Instalacija na terenu
                    </select>
                  </div>
                  {{--<div class="form-group row">
                    <label for="eq_activity1">Tip Opis aktivnosti:</label>
                    <select class="form-control">
                      <option value="1">instalacija na terenu1
                      <option value="2">instalacija na terenu2
                      <option value="3">instalacija na terenu3
                    </select>
                  </div>
                <div class="form-group row">
                    <label for="eq_activity2"></label>
                    <select class="form-control">
                      <option value="empty">
                      <option value="1">instalacija na terenu1
                      <option value="2">instalacija na terenu2
                      <option value="3">instalacija na terenu3
                    </select>
                  </div>
                <div class="form-group row">
                    <label for="eq_activity3"></label>
                    <select class="form-control">
                      <option value="empty">
                      <option value="1">instalacija na terenu1
                      <option value="2">instalacija na terenu2
                      <option value="3">instalacija na terenu3
                    </select>
                  </div>
                <div class="form-group row">
                    <label for="eq_activity4"></label>
                    <select class="form-control">
                      <option value="empty">
                      <option value="1">instalacija na terenu1
                      <option value="2">instalacija na terenu2
                      <option value="3">instalacija na terenu3
                    </select>
                  </div>
                <div class="form-group row">
                    <label for="eq_activity5"></label>
                    <select class="form-control">
                      <option value="empty">
                      <option value="1">instalacija na terenu1
                      <option value="2">instalacija na terenu2
                      <option value="3">instalacija na terenu3
                    </select>
                  </div>--}}
                </div>

              </div>
              {{-- AKTIVNOSTI NA NOVOJ OPREMI --}}

              <legend class="font-weight-semibold"><i class="{{-- NASE IKONICE --}}"></i> Aktivnosti na postojecoj opremi</legend>

              {{-- AKTIVNOSTI NA POSTOJECOJ OPREMI --}}
              {{-- U redu tri SELECTA sa pet redova --}}

              <div class="row">

                <div class="col-md-4">
                  <div class="form-row mb-2">
                    <label for="eq_type1">Vrsta opreme:</label>
                    <select name="eq_type1" class="form-control select" data-fouc data-placeholder="Izaberite vrstu opreme">
                      <option>
                      <option value="AZ">Video basic STB
                      <option value="CO">Internet basic eq
                      <option value="ID">Digital receiver
                    </select>
                  </div>

                  <div class="form-row mb-2">
                    <select name="eq_type2" class="form-control select" data-fouc data-placeholder="Izaberite vrstu opreme">
                      <option>
                      <option value="AZ">Video basic STB
                      <option value="CO">Internet basic eq
                      <option value="ID">Digital receiver
                    </select>
                  </div>

                  <div class="form-row mb-2">
                    <select name="eq_type3" class="form-control select" data-fouc data-placeholder="Izaberite vrstu opreme">
                      <option>
                      <option value="AZ">Video basic STB
                      <option value="CO">Internet basic eq
                      <option value="ID">Digital receiver
                    </select>
                  </div>

                  <div class="form-row mb-2">
                    <select name="eq_type4" class="form-control select" data-fouc data-placeholder="Izaberite vrstu opreme">
                      <option>
                      <option value="AZ">Video basic STB
                      <option value="CO">Internet basic eq
                      <option value="ID">Digital receiver
                    </select>
                  </div>

                  <div class="form-row mb-2">
                    <select name="eq_type5" class="form-control select" data-fouc data-placeholder="Izaberite vrstu opreme">
                      <option>
                      <option value="AZ">Video basic STB
                      <option value="CO">Internet basic eq
                      <option value="ID">Digital receiver
                    </select>
                  </div>

                </div>

                <div class="col-md-4">
                  <div class="form-row mb-2">
                    <label for="eq_type1">Tip opreme:</label>
                    <select name="eq_model1" class="form-control select" data-fouc data-placeholder="Izaberite tip opreme">
                      <option>
                      <option value="PLAN007">D3 1 way receiver
                      <option value="PLAN009">Cable home gateway
                      <option value="PLAN0014">Proffesional gateway
                    </select>
                  </div>

                  <div class="form-row mb-2">
                    <select name="eq_model2" class="form-control select" data-fouc data-placeholder="Izaberite tip opreme">
                      <option>
                      <option value="PLAN007">D3 1 way receiver
                      <option value="PLAN009">Cable home gateway
                      <option value="PLAN0014">Proffesional gateway
                    </select>
                  </div>

                  <div class="form-row mb-2">
                    <select name="eq_model3" class="form-control select" data-fouc data-placeholder="Izaberite tip opreme">
                      <option>
                      <option value="PLAN007">D3 1 way receiver
                      <option value="PLAN009">Cable home gateway
                      <option value="PLAN0014">Proffesional gateway
                    </select>
                  </div>

                  <div class="form-row mb-2">
                    <select name="eq_model4" class="form-control select" data-fouc data-placeholder="Izaberite tip opreme">
                      <option>
                      <option value="PLAN007">D3 1 way receiver
                      <option value="PLAN009">Cable home gateway
                      <option value="PLAN0014">Proffesional gateway
                    </select>
                  </div>

                  <div class="form-row mb-2">
                    <select name="eq_model5" class="form-control select" data-fouc data-placeholder="Izaberite tip opreme">
                      <option>
                      <option value="PLAN007">D3 1 way receiver
                      <option value="PLAN009">Cable home gateway
                      <option value="PLAN0014">Proffesional gateway
                    </select>
                  </div>


                </div>

                <div class="col-md-4">
                  <div class="form-row mb-2">
                    <label>Opis aktivnosti:</label>
                    <select name="eq_activity1" class="form-control select" data-fouc data-placeholder="Opis aktivnosti">
                      <option>
                      <option value="PLAN007">Deinstalacija na terenu
                    </select>
                  </div>

                  <div class="form-row mb-2">
                    <select name="eq_activity2" class="form-control select" data-fouc data-placeholder="Opis aktivnosti">
                      <option>
                      <option value="PLAN007">Deinstalacija na terenu
                    </select>
                  </div>

                  <div class="form-row mb-2">
                    <select name="eq_activity3" class="form-control select" data-fouc data-placeholder="Opis aktivnosti">
                      <option>
                      <option value="PLAN007">Deinstalacija na terenu
                    </select>
                  </div>

                  <div class="form-row mb-2">
                    <select name="eq_activity4" class="form-control select" data-fouc data-placeholder="Opis aktivnosti">
                      <option>
                      <option value="PLAN007">Deinstalacija na terenu
                    </select>
                  </div>

                  <div class="form-row mb-4">
                    <select name="eq_activity5" class="form-control select" data-fouc data-placeholder="Opis aktivnosti">
                      <option>
                      <option value="PLAN007">Deinstalacija na terenu
                    </select>
                  </div>

                </div>

              </div>
              {{-- AKTIVNOSTI NA POSTOJECOJ OPREMI --}}


              {{-- KOMENTAR --}}
              <div class="form-group">
                <label>Komentar:</label>
                <textarea rows="5" cols="5" id="order_comment" class="form-control" placeholder="Upisite dodatne komentare"></textarea>
              </div>
              {{-- /KOMENTAR --}}

            </fieldset>
          </div>
        </div>

        <div class="text-right">
          <button type="submit" class="btn bg-teal-300">Kreiraj nalog <i class="icon-paperplane ml-2"></i></button>
        </div>

      </form>
    </div>
  </div>
  {{--   ************************************************************************************************************************************** --}}

  <br><br><br><br>
  {{--
  <p>FORMA KOJA SE PRIKAZUJE PRILIKOM ZATVARANJA NALOGA</p>



  <div class="card">

      <div class="card-header header-elements-inline">
          <h4 class="card-title">Podaci o opremi - realizovano</h4>
          <div class="header-elements">
            <div class="list-icons">
              <a class="list-icons-item" data-action="collapse"></a>
              <a class="list-icons-item" data-action="reload"></a>
              --}}{{--  NE FUNKCIONISE RELOAD DUGME --}}{{--
            </div>
          </div>
        </div>

      <div class="card-body">
          <form action="#">
            <div class="row">

          --}}{{-- LEVA STRANA --}}{{--
          <div class="col-md-6">
              <fieldset>
                <legend class="font-weight-semibold"><i class="--}}{{-- NASE IKONICE --}}{{--"></i> Naziv opreme izdate na koriscenje</legend>

                  --}}{{-- Naziv opreme izdate na koriscenje  --}}{{--
                  --}}{{-- U redu tri SELECTA sa pet redova --}}{{--
                  <div class="row">

                      <div class="col-md-4">
                        <div class="form-group row">
                          <label for="eq_type1">Vrsta opreme:</label>
                          <select class="form-control">
                            <option value="10">Video basic STB
                            <option value="11">Internet basic eq
                            <option value="12">Digital receiver
                          </select>
                        </div>
                        <div class="form-group row">
                          <label for="eq_typew"></label>
                          <select class="form-control">
                            <option value="empty">
                            <option value="10">Video basic STB
                            <option value="11">Internet basic eq
                            <option value="12">Digital receiver
                          </select>
                        </div>
                        <div class="form-group row">
                          <label for="eq_type3"></label>
                          <select class="form-control">
                            <option value="empty">
                            <option value="10">Video basic STB
                            <option value="11">Internet basic eq
                            <option value="12">Digital receiver
                          </select>
                        </div>
                        <div class="form-group row">
                          <label for="eq_type4"></label>
                          <select class="form-control">
                            <option value="empty">
                            <option value="10">Video basic STB
                            <option value="11">Internet basic eq
                            <option value="12">Digital receiver
                          </select>
                        </div>
                        <div class="form-group row">
                          <label for="eq_type5"></label>
                          <select class="form-control">
                              <option value="empty">
                              <option value="10">Video basic STB
                              <option value="11">Internet basic eq
                              <option value="12">Digital receiver
                          </select>
                        </div>
                      </div>

                      <div class="col-md-4">
                        <div class="form-group row">
                          <label for="eq_model1">Tip opreme:</label>
                          <select class="form-control">
                            <option value="PLAN007">D3 1 way receiver
                            <option value="PLAN009">Cable home gateway
                            <option value="PLAN0014">Proffesional gateway
                          </select>
                        </div>
                        <div class="form-group row">
                          <label for="eq_model2"></label>
                          <select class="form-control">
                            <option value="empty">
                            <option value="PLAN007">D3 1 way receiver
                            <option value="PLAN009">Cable home gateway
                            <option value="PLAN0014">Proffesional gateway
                          </select>
                        </div>
                        <div class="form-group row">
                          <label for="eq_model3"></label>
                          <select class="form-control">
                            <option value="empty">
                            <option value="PLAN007">D3 1 way receiver
                            <option value="PLAN009">Cable home gateway
                            <option value="PLAN0014">Proffesional gateway
                          </select>
                        </div>
                        <div class="form-group row">
                          <label for="eq_model4"></label>
                          <select class="form-control">
                            <option value="empty">
                            <option value="PLAN007">D3 1 way receiver
                            <option value="PLAN009">Cable home gateway
                            <option value="PLAN0014">Proffesional gateway
                          </select>
                        </div>
                        <div class="form-group row">
                          <label for="eq_model5"></label>
                          <select class="form-control">
                            <option value="empty">
                            <option value="PLAN007">D3 1 way receiver
                            <option value="PLAN009">Cable home gateway
                            <option value="PLAN0014">Proffesional gateway
                          </select>
                        </div>
                      </div>

                      <div class="col-md-4">
                        <div class="form-group row">
                          <label for="eq_serial">S/N prikljucenog:</label>
                          <input type="text" placeholder="" id="eq_serial" class="form-control">
                        </div>
                        <div class="form-group row">
                          <label for="eq_serial"></label>
                          <input type="text" placeholder="" id="eq_serial" class="form-control">
                        </div>
                          <div class="form-group row">
                            <label for="eq_serial"></label>
                            <input type="text" placeholder="" id="eq_serial" class="form-control">
                          </div>
                        <div class="form-group row">
                          <label for="eq_serial"></label>
                          <input type="text" placeholder="" id="eq_serial" class="form-control">
                        </div>
                        <div class="form-group row">
                          <label for="eq_serial"></label>
                          <input type="text" placeholder="" id="eq_serial" class="form-control">
                        </div>
                      </div>
                    </div>
                 --}}{{-- Naziv opreme izdate na koriscenje  --}}{{--
                </fieldset>
                </div>

          --}}{{-- DESNA STRANA --}}{{--
          <div class="col-md-6">
              <fieldset>


                <legend class="font-weight-semibold"><i class="--}}{{-- NASE IKONICE --}}{{--"></i> Naziv opreme koja se demontira i vraca</legend>

                  --}}{{-- Naziv opreme koja se demontira i vraca --}}{{--
                  --}}{{-- U redu tri SELECTA sa pet redova --}}{{--
                  <div class="row">

                      <div class="col-md-4">
                        <div class="form-group row">
                          <label for="eq_type1">Vrsta opreme:</label>
                          <select class="form-control">
                            <option value="10">Video basic STB
                            <option value="11">Internet basic eq
                            <option value="12">Digital receiver
                          </select>
                        </div>
                        <div class="form-group row">
                          <label for="eq_typew"></label>
                          <select class="form-control">
                            <option value="empty">
                            <option value="10">Video basic STB
                            <option value="11">Internet basic eq
                            <option value="12">Digital receiver
                          </select>
                        </div>
                        <div class="form-group row">
                          <label for="eq_type3"></label>
                          <select class="form-control">
                            <option value="empty">
                            <option value="10">Video basic STB
                            <option value="11">Internet basic eq
                            <option value="12">Digital receiver
                          </select>
                        </div>
                        <div class="form-group row">
                          <label for="eq_type4"></label>
                          <select class="form-control">
                            <option value="empty">
                            <option value="10">Video basic STB
                            <option value="11">Internet basic eq
                            <option value="12">Digital receiver
                          </select>
                        </div>
                        <div class="form-group row">
                          <label for="eq_type5"></label>
                          <select class="form-control">
                              <option value="empty">
                              <option value="10">Video basic STB
                              <option value="11">Internet basic eq
                              <option value="12">Digital receiver
                          </select>
                        </div>
                      </div>

                      <div class="col-md-4">
                        <div class="form-group row">
                          <label for="eq_model1">Tip opreme:</label>
                          <select class="form-control">
                            <option value="PLAN007">D3 1 way receiver
                            <option value="PLAN009">Cable home gateway
                            <option value="PLAN0014">Proffesional gateway
                          </select>
                        </div>
                        <div class="form-group row">
                          <label for="eq_model2"></label>
                          <select class="form-control">
                            <option value="empty">
                            <option value="PLAN007">D3 1 way receiver
                            <option value="PLAN009">Cable home gateway
                            <option value="PLAN0014">Proffesional gateway
                          </select>
                        </div>
                        <div class="form-group row">
                          <label for="eq_model3"></label>
                          <select class="form-control">
                            <option value="empty">
                            <option value="PLAN007">D3 1 way receiver
                            <option value="PLAN009">Cable home gateway
                            <option value="PLAN0014">Proffesional gateway
                          </select>
                        </div>
                        <div class="form-group row">
                          <label for="eq_model4"></label>
                          <select class="form-control">
                            <option value="empty">
                            <option value="PLAN007">D3 1 way receiver
                            <option value="PLAN009">Cable home gateway
                            <option value="PLAN0014">Proffesional gateway
                          </select>
                        </div>
                        <div class="form-group row">
                          <label for="eq_model5"></label>
                          <select class="form-control">
                            <option value="empty">
                            <option value="PLAN007">D3 1 way receiver
                            <option value="PLAN009">Cable home gateway
                            <option value="PLAN0014">Proffesional gateway
                          </select>
                        </div>
                      </div>

                      <div class="col-md-4">
                        <div class="form-group row">
                          <label for="eq_serial">S/N prikljucenog:</label>
                          <input type="text" placeholder="" id="eq_serial" class="form-control">
                        </div>
                        <div class="form-group row">
                          <label for="eq_serial"></label>
                          <input type="text" placeholder="" id="eq_serial" class="form-control">
                        </div>
                          <div class="form-group row">
                            <label for="eq_serial"></label>
                            <input type="text" placeholder="" id="eq_serial" class="form-control">
                          </div>
                        <div class="form-group row">
                          <label for="eq_serial"></label>
                          <input type="text" placeholder="" id="eq_serial" class="form-control">
                        </div>
                        <div class="form-group row">
                          <label for="eq_serial"></label>
                          <input type="text" placeholder="" id="eq_serial" class="form-control">
                        </div>
                      </div>
                    </div>
                  --}}{{-- Naziv opreme koja se demontira i vraca --}}{{--

                </fieldset>
              </div>
            </div>

              --}}{{-- Kuca/Stan --}}{{--
              <div class="row">
                <label class="col-form-label col-lg-1 " for="service_home">Tehnicar koristi vozilo</label>

                <div class="form-check form-check-inline">
                  <label class="form-check-label" >
                    <input type="radio" class="form-check-input" name="service_home" checked="" >Kuca</label>
                </div>

                <div class="form-check form-check-inline">
                  <label class="form-check-label">
                    <input type="radio" class="form-check-input" name="service_home" >Stan</label>
                </div>
              </div>
              --}}{{-- Kuca/Stan --}}{{--

              --}}{{-- KOMENTAR --}}{{--
              <div class="form-group">
                <label>Komentar:</label>
                <textarea rows="5" cols="5" id="order_comment" class="form-control" placeholder="Upisite dodatne komentare"></textarea>
              </div>
              --}}{{-- KOMENTAR --}}{{--

          </form>
        </div>
      </div>--}}
  {{--   ************************************************************************************************************************************** --}}
  {{--<div class="card">

      <div class="card-header header-elements-inline">
        <h4 class="card-title">Utroseni materijal</h4>
        <div class="header-elements">
          <div class="list-icons">
            <a class="list-icons-item" data-action="collapse"></a>
            <a class="list-icons-item" data-action="reload"></a>
            --}}{{--  NE FUNKCIONISE RELOAD DUGME --}}{{--
          </div>
        </div>
      </div>

      <div class="card-body">
        <form action="#">
          <div class="row">

            --}}{{-- LEVA STRANA --}}{{--
            <div class="col-md-6">
              <div class="card card-table ">
                  <table class="table table-xs">
                    <thead>
                      <tr class="bg-grey">
                        <th>br</th>
                        <th>sif.mat.</th>
                        <th>naziv ugradjenog materijala</th>
                        <th>j.m.</th>
                        <th>kol.</th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <td>1</td>
                        <td>01003</td>
                        <td>Kabl koaksijalni rg-6</td>
                        <td>m</td>
                        <td>13</td>
                      </tr>
                      <tr>
                        <td>2</td>
                        <td>01003</td>
                        <td>Kabl koaksijalni rg-6</td>
                        <td>m</td>
                        <td>13</td>
                      </tr>
                      <tr>
                        <td>3</td>
                        <td>01003</td>
                        <td>Kabl koaksijalni rg-6</td>
                        <td>m</td>
                        <td>13</td>
                      </tr>
                      <tr>
                        <td>4</td>
                        <td>01003</td>
                        <td>Kabl koaksijalni rg-6</td>
                        <td>m</td>
                        <td>13</td>
                      </tr>
                      <tr>
                        <td>5</td>
                        <td>01003</td>
                        <td>Kabl koaksijalni rg-6</td>
                        <td>m</td>
                        <td>13</td>
                      </tr>
                      <tr>
                        <td>6</td>
                        <td>01003</td>
                        <td>Kabl koaksijalni rg-6</td>
                        <td>m</td>
                        <td>13</td>
                      </tr>
                      <tr>
                        <td>7</td>
                        <td>01003</td>
                        <td>Kabl koaksijalni rg-6</td>
                        <td>m</td>
                        <td>13</td>
                      </tr>
                      <tr>
                        <td>8</td>
                        <td>01003</td>
                        <td>Kabl koaksijalni rg-6</td>
                        <td>m</td>
                        <td>13</td>
                      </tr>
                      <tr>
                        <td>9</td>
                        <td>01003</td>
                        <td>Kabl koaksijalni rg-6</td>
                        <td>m</td>
                        <td>13</td>
                      </tr>
                      <tr>
                        <td>10</td>
                        <td>01003</td>
                        <td>Kabl koaksijalni rg-6</td>
                        <td>m</td>
                        <td>13</td>
                      </tr>
                      <tr>
                        <td>11</td>
                        <td>01003</td>
                        <td>Kabl koaksijalni rg-6</td>
                        <td>m</td>
                        <td>13</td>
                      </tr>
                      <tr>
                        <td>12</td>
                        <td>01003</td>
                        <td>Kabl koaksijalni rg-6</td>
                        <td>m</td>
                        <td>13</td>
                      </tr>
                      <tr>
                        <td>13</td>
                        <td>01003</td>
                        <td>Kabl koaksijalni rg-6</td>
                        <td>m</td>
                        <td>13</td>
                      </tr>
                      <tr>
                        <td>14</td>
                        <td>01003</td>
                        <td>Kabl koaksijalni rg-6</td>
                        <td>m</td>
                        <td>13</td>
                      </tr>
                      <tr>
                        <td>15</td>
                        <td>01003</td>
                        <td>Kabl koaksijalni rg-6</td>
                        <td>m</td>
                        <td>13</td>
                      </tr>
                    </tbody>
                  </table>
                </div>
              </div>

          --}}{{-- DESNA STRANA --}}{{--
          <div class="col-md-6">
                <div class="table-responsive">
                  <table class="table table-xs">
                    <thead>
                      <tr class="bg-grey">
                        <th>br</th>
                        <th>sif.mat.</th>
                        <th>naziv ugradjenog materijala</th>
                        <th>j.m.</th>
                        <th>kol.</th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <td>16</td>
                        <td>01003</td>
                        <td>Kabl koaksijalni rg-6</td>
                        <td>m</td>
                        <td>13</td>
                      </tr>
                      <tr>
                        <td>17</td>
                        <td>01003</td>
                        <td>Kabl koaksijalni rg-6</td>
                        <td>m</td>
                        <td>13</td>
                      </tr>
                      <tr>
                        <td>18</td>
                        <td>01003</td>
                        <td>Kabl koaksijalni rg-6</td>
                        <td>m</td>
                        <td>13</td>
                      </tr>
                      <tr>
                        <td>19</td>
                        <td>01003</td>
                        <td>Kabl koaksijalni rg-6</td>
                        <td>m</td>
                        <td>13</td>
                      </tr>
                      <tr>
                        <td>20</td>
                        <td>01003</td>
                        <td>Kabl koaksijalni rg-6</td>
                        <td>m</td>
                        <td>13</td>
                      </tr>
                      <tr>
                        <td>21</td>
                        <td>01003</td>
                        <td>Kabl koaksijalni rg-6</td>
                        <td>m</td>
                        <td>13</td>
                      </tr>
                      <tr>
                        <td>22</td>
                        <td>01003</td>
                        <td>Kabl koaksijalni rg-6</td>
                        <td>m</td>
                        <td>13</td>
                      </tr>
                      <tr>
                        <td>23</td>
                        <td>01003</td>
                        <td>Kabl koaksijalni rg-6</td>
                        <td>m</td>
                        <td>13</td>
                      </tr>
                      <tr>
                        <td>24</td>
                        <td>01003</td>
                        <td>Kabl koaksijalni rg-6</td>
                        <td>m</td>
                        <td>13</td>
                      </tr>
                      <tr>
                        <td>25</td>
                        <td>01003</td>
                        <td>Kabl koaksijalni rg-6</td>
                        <td>m</td>
                        <td>13</td>
                      </tr>
                      <tr>
                        <td>26</td>
                        <td>01003</td>
                        <td>Kabl koaksijalni rg-6</td>
                        <td>m</td>
                        <td>13</td>
                      </tr>
                      <tr>
                        <td>27</td>
                        <td>01003</td>
                        <td>Kabl koaksijalni rg-6</td>
                        <td>m</td>
                        <td>13</td>
                      </tr>
                      <tr>
                        <td>28</td>
                        <td>01003</td>
                        <td>Kabl koaksijalni rg-6</td>
                        <td>m</td>
                        <td>13</td>
                      </tr>
                      <tr>
                        <td>29</td>
                        <td>01003</td>
                        <td>Kabl koaksijalni rg-6</td>
                        <td>m</td>
                        <td>13</td>
                      </tr>
                      <tr>
                        <td>30</td>
                        <td>01003</td>
                        <td>Kabl koaksijalni rg-6</td>
                        <td>m</td>
                        <td>13</td>
                      </tr>
                    </tbody>
                  </table>
                </div>
              </div>

          </div>
        </form>
      </div>
  </div>--}}

@endsection