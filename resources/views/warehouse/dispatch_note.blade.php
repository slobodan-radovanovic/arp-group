@extends('layouts.app')

@section('content')

    @push('style')
        <!-- Global stylesheets -->
        <link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet"
              type="text/css">
        <link href="../../../../global_assets/css/icons/icomoon/styles.css" rel="stylesheet" type="text/css">
        <link href="../assets/css/bootstrap.min.css" rel="stylesheet" type="text/css">
        <link href="../assets/css/bootstrap_limitless.min.css" rel="stylesheet" type="text/css">
        <link href="../assets/css/layout.min.css" rel="stylesheet" type="text/css">
        <link href="../assets/css/components.min.css" rel="stylesheet" type="text/css">
        <link href="../assets/css/colors.min.css" rel="stylesheet" type="text/css">
        <!-- /global stylesheets -->
        <style>
            h1 + div {
                font-size: large;
            }
        </style>
    @endpush


    @push('scripts')
        <script src="../global_assets/js/plugins/tables/datatables/datatables.min.js"></script>
        <script src="../global_assets/js/demo_pages/datatables_basic.js"></script>
        <!-- Load select2 -->
        <script type="text/javascript" src="../global_assets/js/plugins/forms/selects/select2.min.js"></script>
        <script type="text/javascript"
                src="../global_assets/js/plugins/extensions/jquery_ui/interactions.min.js"></script>
        <script src="../assets/js/custom_select2.js"></script>
        <script src="../global_assets/js/plugins/forms/selects/select2.min.js"></script>
        {{--anytime--}}
        <script src="../global_assets/js/plugins/ui/moment/moment.min.js"></script>
        <script src="../global_assets/js/plugins/pickers/anytime.min.js"></script>
        <script src="../../../../global_assets/js/plugins/pickers/daterangepicker.js"></script>
        <script src="../assets/js/custom_datepicker.js"></script>

        <script src="../global_assets/js/main/jquery.min.js"></script>
        <script src="../global_assets/js/main/bootstrap.bundle.min.js"></script>
        <script src="../global_assets/js/plugins/loaders/blockui.min.js"></script>
        <script src="../global_assets/js/plugins/tables/datatables/datatables.min.js"></script>
        <script src="../global_assets/js/plugins/forms/selects/select2.min.js"></script>
        <script src="../global_assets/js/plugins/tables/datatables/extensions/jszip/jszip.min.js"></script>
        <script src="../global_assets/js/plugins/tables/datatables/extensions/buttons.min.js"></script>

        {{--export datatable--}}



        <script>

            /*var today = new Date();
            var date = today.getFullYear() + '-' + (today.getMonth() + 1) + '-' + today.getDate();
            var time = today.getHours() + ":" + today.getMinutes();
            var dateTime = date + ' ' + time;*/

            var MyDate = new Date();
            var print_date;

            MyDate.setDate(MyDate.getDate());

            print_date = ('0' + MyDate.getDate()).slice(-2) + '/' + ('0' + (MyDate.getMonth()+1)).slice(-2) + '/' + MyDate.getFullYear();

            @if($dispatch_note != null)

            var dispatch_note = '{!! $dispatch_note !!}';
            console.log(dispatch_note);

            @if($print_date != null)
                print_date = '{!! $print_date !!}';
                console.log(print_date);

            @endif

            console.log(print_date);

            $('#datum_otpremnice').text(print_date);
            $('#datum_otpremnice2').text(print_date);

            $('.datatable-basic').DataTable({
                autoWidth: false,
                dom: '<"datatable-header"fBl><"datatable-scroll-wrap"t><"datatable-footer"ip>',
                language: {
                    search: '<span>Pretraga:</span> _INPUT_',
                    lengthMenu: '<span>Prikaz:</span> _MENU_',
                    info: '_START_ do _END_ od ukupno _TOTAL_',
                    paginate: {'first': 'First', 'last': 'Last', 'next': '→', 'previous': '←'}
                },
                buttons: {
                    buttons: [
                        {
                            extend: 'excelHtml5',
                            footer: true,
                            className: 'btn btn-light',
                            title: 'Otpremnica ' + dispatch_note,
                            sheetName: 'Otpremnica' + dispatch_note,
                            exportOptions: {
                                columns: [0, 1, 2, 3, 4, 5]
                            }
                        },
                        {
                            extend: 'print',
                            text: '<i class="icon-printer mr-2"></i> Print table',
                            className: 'btn btn-light',
                            messageTop: 'Broj otpremnice: '+ dispatch_note +' / Datum otpremnice: ' + print_date
                        }
                    ]
                }
            });

            @endif



        </script>


    @endpush

    @push('style')
        <style>
            .bold {
                font-weight: 700;
            }

            .large {
                font-size: large;
            }
        </style>
    @endpush

    <div>
        <div class="content">
            <div class="title m-b-md">
                <h1 style="text-align: center;">Pregled materijala po broju otpremnice</h1>
            </div>

            @if ($errors->any())
                <div class="alert alert-danger">
                    @foreach ($errors->all() as $error)
                        <div>- {{ $error }}</div>
                    @endforeach
                </div>
            @endif

            <div>

                <br>

                <!-- Basic datatable -->
                <div class="card">
                    {!! Form::open(['action' => 'WarehouseController@dispatch_note', 'method' => 'POST']) !!}
                    <div class="card-header header-elements-inline">
                        {{-- {{dd($dismantled_date_filter)}}--}}

                        <div class="row mb-3 col-12">


                            <div class="col-4 form-row mb-2">
                                    <label>Broj otpremnice:</label>
                                    <input type="text" name='dispatch_note' id='dispatch_note'
                                           placeholder="Upišite broj otpremnice"
                                           class="form-control" autocomplete="off" value="{{old('dispatch_note')}}">
                            </div>

                            @if(session('session_data'))
                                <div class="col-4">
                                    <div class="form-row mb-2">
                                        {{--datetimeForPicker($data['order']->completed_at)-}}
                                        {{--vreme zatvaranja--}}
                                        {{--{{ Form::datetime_picker('created_at', 'Datum za stampanje:', null, session('session_data')['4'], 'form-control', ['placeholder' => 'Izaberi datum'], 'col-12') }}--}}
                                        <div class="input-group">
										<span class="input-group-prepend">
											<span class="input-group-text"><i class="icon-calendar3"></i></span>
										</span>
                                            <input type="text" class="form-control" id="anytime-month-numeric" placeholder="izaberite datum" name="print_date">
                                        </div>

                                    </div>

                                </div>

                            @else
                                <div class="col-4">
                                    <div class="form-row mb-2">
                                        {{--datetimeForPicker($data['order']->completed_at)-}}
                                        {{--vreme zatvaranja--}}
                                        {{--{{ Form::datetime_picker('print_date', 'Datum za stampanje:', null, null, 'form-control', ['placeholder' => 'Izaberi datum'], 'col-12') }}--}}
                                        <label>Datum za stampanje:</label>
                                        <div class="input-group">

										<span class="input-group-prepend">
											<span class="input-group-text"><i class="icon-calendar3"></i></span>
										</span>
                                            <input type="text" class="form-control" id="anytime-month-numeric" name="print_date" placeholder="izaberite datum">
                                        </div>

                                    </div>

                                </div>

                            @endif


                            <div class="col-4 form-row mb-2">
                                <button type="submit" class="btn bg-teal-300 ml-3 mt-3" style="height: 70%">Prikaži
                                </button>
                            </div>


                        </div>

                        {!! Form::close() !!}


                    </div>

                    @if($dispatch_note != null)
                        <h2 style="text-align: center;">Broj otpremnice: {{$dispatch_note}}, datum otpremnice: <span id="datum_otpremnice"></span></h2>
        {{--@dd($added_wares)--}}
                        <table class="table datatable-basic table-bordered table-striped">
                            <thead>
                            <tr>
                                <th>Kôd</th>
                                <th>Materijal</th>
                                <th>Količina</th>
                                <th>Dodao</th>
                                <th>Broj optremnice</th>
                                <th>Vreme unosa</th>

                            </tr>
                            </thead>
                            <tbody>
                            @foreach($added_wares as $added_ware)
                                {{--@dd('technician_'.$equipment->equipment_technician)--}}
                                <tr>
                                    <td>{{ $added_ware->wares_code }}</td>
                                    <td>{{ $added_ware->wares_name }}</td>
                                    <td>{{ $added_ware->quantity }} {{ $added_ware->wares_type }} </td>
                                    <td>{{ $added_ware->name }}</td>
                                    <td>{{ $added_ware->wares_dispatch_note }}</td>
                                    <td>{{ datetimeForView($added_ware->created_at) }}</td>

                                </tr>
                            @endforeach

                            </tbody>
                            <tfoot>
                            <tr>

                                <th>Datum otpremnice: <span id="datum_otpremnice2"></span></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                            </tr>
                            </tfoot>
                        </table>
                        {{--<div class="bold large p-3">Ukupno za izabrani period: {{ $sum }} din.</div>--}}


                </div>
                <!-- /basic datatable -->
                @endif
            </div>
        </div>
    </div>

@endsection



