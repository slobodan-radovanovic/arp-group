@extends('layouts.app')
@push('scripts')


    <script src="../global_assets/js/plugins/ui/moment/moment.min.js"></script>
    <script src="../global_assets/js/plugins/pickers/anytime.min.js"></script>



    <script src="../assets/js/custom_datepicker.js"></script>


    <!-- Load select2 -->
    <script type="text/javascript" src="../global_assets/js/plugins/forms/selects/select2.min.js"></script>
    <script type="text/javascript" src="../global_assets/js/plugins/extensions/jquery_ui/interactions.min.js"></script>
    <script src="../assets/js/custom_select2.js"></script>

    <script src="../../global_assets/js/plugins/tables/datatables/datatables.min.js"></script>
    <script src="../../global_assets/js/demo_pages/datatables_basic.js"></script>
    <script src="../../global_assets/js/plugins/forms/selects/select2.min.js"></script>


    <script>

        $('.datatable-basic').DataTable({
            autoWidth: false,
            dom: '<"datatable-header"fl><"datatable-scroll"t><"datatable-footer"ip>',
            bLengthChange : false, //thought this line could hide the LengthMenu
            bInfo: false,
            bPaginate: false,
            order: [],
            bSort : false,
            language: {
                search: '<span>Pretraga:</span> _INPUT_',
            }
        });
        /*
        $('.datatable-basic').DataTable({
            autoWidth: false,
            dom: '<"datatable-header"fl><"datatable-scroll"t><"datatable-footer"ip>',
            order: [2, "desc"],
            language: {
                search: '<span>Pretraga:</span> _INPUT_',
                lengthMenu: '<span>Prikaz:</span> _MENU_',
                info: '_START_ do _END_ od ukupno _TOTAL_',
                emptyTable: "Nema podataka",
                paginate: { 'first': 'First', 'last': 'Last', 'next': '→', 'previous': '←' }
            }
        });

        $('.datatable-sorting').DataTable({
            order: [3, "desc"]
        });*/
    </script>



@endpush
@section('content')
    <div>
        <div class="content">
            <div class="title m-b-md">
                <h1 style="text-align: center;">Zaduzivanje materijala</h1>
            </div>

            @if ($errors->any())
                <div class="alert alert-danger">
                    @foreach ($errors->all() as $error)
                        <div>- {{ $error }}</div>
                    @endforeach
                </div>
            @endif

            <div class="card">
                {!! Form::open(['action' => 'WarehouseController@assign', 'method' => 'POST']) !!}
                <div class="card-body">
                    {{-- AKTIVNOSTI NA NOVOJ OPREMI --}}
                    {{-- U redu tri SELECTA sa pet redova --}}
                    <div class="row">

                        {{ Form::selectGroupSearch('wares_id', true, 'Materijal:', null,
             $assignment['wares'], null,
                 ['data-placeholder' => 'Odaberite materijal',
                 'class'=> 'form-control select-search',
                 'data-focus'], 'col-6') }}


                        <div class="col-md-2">
                            <div class="form-row mb-2">
                                <label for="quantity">Količina za zaduživanje</label>
                                <input name="quantity" type="text" placeholder="Kolicina za zaduzivanje" class="form-control">
                            </div>

                        </div>

                        {{ Form::selectGroupSearch('technician_id',
                                                    true,
                                                    'Zaduzije se:',
                                                    null,
                                                    $assignment['technicians'], null,
                                                    ['data-placeholder' => 'Izaberite tehničara',
                                                     'class'=> 'form-control select-search equipment_model',
                                                     'data-fouc'], 'col-3 form-row mb-2') }}
                        {{--<div class="col-md-3">
                            <div class="form-row mb-2">
                                <label for="eq_type1">Zaduzije se:</label>
                                <select name="eq_model1" class="form-control select" data-fouc data-placeholder="Izaberite tehnicara">
                                    <option>
                                    <option value="PLAN007">Marko Markovic
                                    <option value="PLAN009">Darko jovanovic
                                    <option value="PLAN0014">filip Filipovic
                                </select>
                            </div>

                        </div>--}}

                        <div class="col-md-1 right">
                            <div class="form-row " style="margin-top: 28px">
                                <button type="submit" class="btn bg-teal-300 " data-toggle="modal" data-target="#modal_add">Zaduzi <i class="icon-plus3 ml-1" ></i></button>
                            </div>

                        </div>

                    </div>


                    {{-- AKTIVNOSTI NA NOVOJ OPREMI --}}
                </div>
                {!! Form::close() !!}


            <!-- Zadužen materijal -->
           <legend class="m-0 p-0"></legend>
                <div class="card-header header-elements-inline">
                    <h5 class="card-title">Poslednje zaduženi materijal</h5>

                </div>

                <table class="table datatable-basic table-bordered table-striped">
                    <thead>
                    <tr>
                        <th>Naziv materijala</th>
                        <th>Količina</th>
                        <th>Vreme zaduzvanja</th>
                        <th>Zadužen</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($assigned_wares as $wares)
                        <tr>
                            <td>{{ $wares->wares_name }}</td>
                            <td>{{ $wares->quantity }}</td>
                            <td>{{ datetimeForView($wares->created_at) }}</td>
                            <td>{{ $wares->technician_name}}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
            <!-- /Zadužena oprema -->


        </div>
    </div>

@endsection



