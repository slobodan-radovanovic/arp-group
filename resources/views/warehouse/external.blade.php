@extends('layouts.app')

@section('content')

    @push('style')

        <!-- Global stylesheets -->
        <link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
        <link href="../../../../global_assets/css/icons/icomoon/styles.css" rel="stylesheet" type="text/css">
        <link href="../assets/css/bootstrap.min.css" rel="stylesheet" type="text/css">
        <link href="../assets/css/bootstrap_limitless.min.css" rel="stylesheet" type="text/css">
        <link href="../assets/css/layout.min.css" rel="stylesheet" type="text/css">
        <link href="../assets/css/components.min.css" rel="stylesheet" type="text/css">
        <link href="../assets/css/colors.min.css" rel="stylesheet" type="text/css">
        <!-- /global stylesheets -->
    @endpush

    @push('scripts')
        <script src="../global_assets/js/plugins/tables/datatables/datatables.min.js"></script>
        <script src="../global_assets/js/demo_pages/datatables_basic.js"></script>
        <!-- Load select2 -->
        <script type="text/javascript" src="../global_assets/js/plugins/forms/selects/select2.min.js"></script>
        <script type="text/javascript" src="../global_assets/js/plugins/extensions/jquery_ui/interactions.min.js"></script>
        <script src="../assets/js/custom_select2.js"></script>
        <script src="../global_assets/js/plugins/forms/selects/select2.min.js"></script>
        {{--anytime--}}
        <script src="../global_assets/js/plugins/ui/moment/moment.min.js"></script>
        <script src="../global_assets/js/plugins/pickers/anytime.min.js"></script>
        <script src="../../../../global_assets/js/plugins/pickers/daterangepicker.js"></script>
        <script src="../assets/js/custom_datepicker.js"></script>

        <script src="../global_assets/js/main/jquery.min.js"></script>
        <script src="../global_assets/js/main/bootstrap.bundle.min.js"></script>
        <script src="../global_assets/js/plugins/loaders/blockui.min.js"></script>
        <script src="../global_assets/js/plugins/tables/datatables/datatables.min.js"></script>
        <script src="../global_assets/js/plugins/forms/selects/select2.min.js"></script>
        <script src="../global_assets/js/plugins/tables/datatables/extensions/jszip/jszip.min.js"></script>
        <script src="../global_assets/js/plugins/tables/datatables/extensions/buttons.min.js"></script>

        {{--export datatable--}}

        {{--anytime--}}
        <script src="../global_assets/js/plugins/ui/moment/moment.min.js"></script>
        <script src="../global_assets/js/plugins/pickers/anytime.min.js"></script>
        <script src="../../../../global_assets/js/plugins/pickers/daterangepicker.js"></script>
        <script src="../assets/js/custom_datepicker.js"></script>



        <script>

            var today = new Date();
            var dd = String(today.getDate()).padStart(2, '0');
            var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
            var yyyy = today.getFullYear();

            today = dd + '-' + mm + '-' + yyyy;
            console.log(today);


            $('.datatable-basic').DataTable({
                autoWidth: false,
                dom: '<"datatable-header"fBl><"datatable-scroll-wrap"t><"datatable-footer"ip>',
                language: {
                    search: '<span>Pretraga:</span> _INPUT_',
                    lengthMenu: '<span>Prikaz:</span> _MENU_',
                    info: '_START_ do _END_ od ukupno _TOTAL_',
                    paginate: {'first': 'First', 'last': 'Last', 'next': '→', 'previous': '←'}
                },
                buttons: {
                    buttons: [
                        {
                            extend: 'excelHtml5',
                            className: 'btn btn-light',
                            title: 'Spoljni magacin '+today,
                            sheetName:'Spoljni magacin',
                            exportOptions: {
                                columns: [ 0, 1, 2, 3]
                            }
                        }
                    ]
                }
            });
        </script>


        {{-- dodatno ubaceno--}}
        <script src="../global_assets/js/plugins/ui/moment/moment.min.js"></script>
        <script src="../global_assets/js/plugins/pickers/anytime.min.js"></script>



        <script src="../assets/js/custom_datepicker.js"></script>


        <!-- Load select2 -->
        <script type="text/javascript" src="../global_assets/js/plugins/forms/selects/select2.min.js"></script>
        <script type="text/javascript" src="../global_assets/js/plugins/extensions/jquery_ui/interactions.min.js"></script>
        <script src="../assets/js/custom_select2.js"></script>






    @endpush

    <div>
        <div class="content">
            <div class="title m-b-md">
                <h1 style="text-align: center;">Magacin materijala koji je kod tehničara</h1>
            </div>

            <div>


                <!-- Basic datatable -->
                <div class="card mt-2">

                    <table class="table datatable-basic table-bordered table-striped">
                        {{--table-bordered dataTable table-striped--}}
                        <thead>
                        <tr>
                            <th>Kod materijala</th>
                            <th>Ime materijala</th>
                            <th>Količina</th>
                            <th>Jedinica mere</th>
                        </tr>

                        </thead>
                        <tbody>

                        @foreach($externalwarehouse as $wares)
                            <tr>
                                <td>{{$wares->wares_code}}</td>
                                <td>{{$wares->wares_name}}</td>
                                <td>{{$wares->quantity}}</td>
                                <td>{{$wares->wares_type}}</td>
                            </tr>
                        @endforeach




                        </tbody>
                    </table>
                </div>
                <!-- /basic datatable -->

            </div>
        </div>
    </div>

@endsection



