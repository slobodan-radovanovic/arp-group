@extends('layouts.app')
@push('scripts')


    <script src="../global_assets/js/plugins/ui/moment/moment.min.js"></script>
    <script src="../global_assets/js/plugins/pickers/anytime.min.js"></script>



    <script src="../assets/js/custom_datepicker.js"></script>


    <!-- Load select2 -->
    <script type="text/javascript" src="../global_assets/js/plugins/forms/selects/select2.min.js"></script>
    <script type="text/javascript" src="../global_assets/js/plugins/extensions/jquery_ui/interactions.min.js"></script>
    <script src="../assets/js/custom_select2.js"></script>

    <script src="../global_assets/js/plugins/tables/datatables/datatables.min.js"></script>
    <script src="../global_assets/js/demo_pages/datatables_basic.js"></script>
    <script src="../global_assets/js/plugins/forms/selects/select2.min.js"></script>


    <script>
        $('.datatable-basic').DataTable({
            autoWidth: false,
            dom: '<"datatable-header"fl><"datatable-scroll"t><"datatable-footer"ip>',
            bLengthChange : false, //thought this line could hide the LengthMenu
            bInfo: false,
            bPaginate: false,
            order: [],
            bSort : false,
            language: {
                search: '<span>Pretraga:</span> _INPUT_',
            }
        });
     </script>

@endpush
@section('content')
    <div>
        <div class="content">
            <div class="title m-b-md">
                <h1 style="text-align: center;">Unos materijala u magacin</h1>
            </div>

            @if ($errors->any())
                <div class="alert alert-danger">
                    @foreach ($errors->all() as $error)
                        <div>- {{ $error }}</div>
                    @endforeach
                </div>
            @endif

            <div class="card">

                {!! Form::open(['action' => 'WarehouseController@store', 'method' => 'POST']) !!}

                <div class="card-body">

                    <div class="row">
                    {{-- AKTIVNOSTI NA NOVOJ OPREMI --}}
                    {{-- U redu tri SELECTA sa pet redova --}}

                    {{ Form::selectGroupSearch('wares_id', true, 'Materijal:', null,
             $wares, null,
                 ['data-placeholder' => 'Odaberite materijal',
                 'class'=> 'form-control select-search',
                 'data-fouc'], 'col-6', true) }}


                    @if(session('session_data'))
                    <div class="col-6">
                        <div class="form-row mb-2">
                            <label>Broj otpremnice:</label>
                            <input type="text" name='wares_dispatch_note' id='wares_dispatch_note'
                                   placeholder="Upišite broj otpremnice" value="{{session('session_data')['0']}}"
                                   class="form-control" autocomplete="off">
                        </div>

                    </div>
                    @else
                        <div class="col-6">
                            <div class="form-row mb-2">
                                <label>Broj otpremnice:</label>
                                <input type="text" name='wares_dispatch_note' id='wares_dispatch_note'
                                       placeholder="Upišite broj otpremnice"
                                       class="form-control" autocomplete="off" value="{{old('equipment_dispatch_note')}}">
                            </div>

                        </div>
                    @endif
                    </div>

                    <div class="row">

                    {{ Form::textGroup2('quantity', 'Količina:', null, null, 'form-control', ['placeholder' => 'Kolicina'], 'col-6', true) }}

                        @if(session('session_data'))
                                <div class="form-row mb-2 col-6">
                                    {{ Form::datetime_picker('created_at', 'Datum i vreme unosa opreme:', null, session('session_data')['1'], 'form-control', ['placeholder' => 'Izaberi datum'], 'col-12', true) }}
                                </div>
                        @else
                                <div class="form-row mb-2 col-6">
                                    {{ Form::datetime_picker('created_at', 'Datum i vreme unosa opreme:', null, null, 'form-control', ['placeholder' => 'Izaberi datum'], 'col-12', true) }}

                                </div>
                        @endif

                    </div>

                    <p class="text-right">
                        <button type="submit" class="btn bg-teal-300 " data-toggle="modal" data-target="#modal_add">Unesi materijal u magacin <i class="icon-plus3 ml-1" ></i></button>
                    </p>

                    {{--<div class="col- text-right" style="margin-top: 27px">
                    <button type="submit" class="btn bg-teal-300 " data-toggle="modal" data-target="#modal_add">Unesi materijal u magacin <i class="icon-plus3 ml-1" ></i></button></div>--}}

                    {{-- AKTIVNOSTI NA NOVOJ OPREMI --}}
                </div>
                 {!! Form::close() !!}

            <!-- dodat materijal -->
                <legend class="m-0 p-0"></legend>
                <div class="card-header header-elements-inline">
                    <h5 class="card-title">Poslednje dodat materijal</h5>

                </div>

                <table class="table datatable-basic table-bordered table-striped">
                    <thead>
                    <tr>
                        <th>Kod materijala</th>
                        <th>Naziv materijala</th>
                        <th>Količina</th>
                        <th>Broj otpremnice</th>
                        <th>Vreme dodavanja</th>
                        <th>Dodao</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($last_added_wares as $wares)
                        <tr>
                            <td>{{ $wares->wares_code }}</td>
                            <td>{{ $wares->wares_name }}</td>
                            <td>{{ $wares->quantity }} {{ $wares->wares_type }}</td>
                            @if($wares->wares_dispatch_note != null)
                                <td>{{ $wares->wares_dispatch_note }}</td>
                            @else
                                <td></td>
                            @endif
                            <td>{{ datetimeForView($wares->created_at) }}</td>
                            <td>{{ $wares->name }}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
            <!-- /dodat oprema -->


            </div>





        </div>
    </div>

@endsection



