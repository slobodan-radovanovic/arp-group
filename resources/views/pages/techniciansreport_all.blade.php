@extends('layouts.app')

@section('content')

    @push('style')
        <!-- Global stylesheets -->
        <link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet"
              type="text/css">
        <link href="../../../../global_assets/css/icons/icomoon/styles.css" rel="stylesheet" type="text/css">
        <link href="../assets/css/bootstrap.min.css" rel="stylesheet" type="text/css">
        <link href="../assets/css/bootstrap_limitless.min.css" rel="stylesheet" type="text/css">
        <link href="../assets/css/layout.min.css" rel="stylesheet" type="text/css">
        <link href="../assets/css/components.min.css" rel="stylesheet" type="text/css">
        <link href="../assets/css/colors.min.css" rel="stylesheet" type="text/css">
        <!-- /global stylesheets -->
    @endpush

    @push('scripts')
        <script src="../global_assets/js/plugins/tables/datatables/datatables.min.js"></script>
        <script src="../global_assets/js/demo_pages/datatables_basic.js"></script>
        <!-- Load select2 -->
        <script type="text/javascript" src="../global_assets/js/plugins/forms/selects/select2.min.js"></script>
        <script type="text/javascript"
                src="../global_assets/js/plugins/extensions/jquery_ui/interactions.min.js"></script>
        <script src="../assets/js/custom_select2.js"></script>
        <script src="../global_assets/js/plugins/forms/selects/select2.min.js"></script>
        {{--anytime--}}
        <script src="../global_assets/js/plugins/ui/moment/moment.min.js"></script>
        <script src="../global_assets/js/plugins/pickers/anytime.min.js"></script>
        <script src="../../../../global_assets/js/plugins/pickers/daterangepicker.js"></script>
        <script src="../assets/js/custom_datepicker.js"></script>

        <script src="../global_assets/js/main/jquery.min.js"></script>
        <script src="../global_assets/js/main/bootstrap.bundle.min.js"></script>
        <script src="../global_assets/js/plugins/loaders/blockui.min.js"></script>
        <script src="../global_assets/js/plugins/tables/datatables/datatables.min.js"></script>
        <script src="../global_assets/js/plugins/forms/selects/select2.min.js"></script>
        <script src="../global_assets/js/plugins/tables/datatables/extensions/jszip/jszip.min.js"></script>
        <script src="../global_assets/js/plugins/tables/datatables/extensions/buttons.min.js"></script>

        {{--export datatable--}}

        {{--anytime--}}
        <script src="../global_assets/js/plugins/ui/moment/moment.min.js"></script>
        <script src="../global_assets/js/plugins/pickers/anytime.min.js"></script>
        <script src="../../../../global_assets/js/plugins/pickers/daterangepicker.js"></script>
        <script src="../assets/js/custom_datepicker.js"></script>



        <script>
                    @if(isset($date_period))
            var date_peiod = '{!! $date_period !!}';
            console.log(date_peiod);

            $('.datatable-basic').DataTable({
                autoWidth: false,
                dom: '<"datatable-header"fBl><"datatable-scroll-wrap"t><"datatable-footer"ip>',
                language: {
                    search: '<span>Pretraga:</span> _INPUT_',
                    lengthMenu: '<span>Prikaz:</span> _MENU_',
                    info: '_START_ do _END_ od ukupno _TOTAL_',
                    paginate: {'first': 'First', 'last': 'Last', 'next': '→', 'previous': '←'}
                },
                buttons: {
                    buttons: [
                        {
                            extend: 'excelHtml5',
                            footer: true,
                            className: 'btn btn-light',
                            title: 'Izveštaj za sve tehničare ' + date_peiod,
                            sheetName: 'Izveštaj za sve tehničare',
                            exportOptions: {
                                columns: [0, 1, 2, 3, 4]
                            }
                        }
                    ]
                }
            });

            @endif




            $('.daterange-basic').daterangepicker({
                @if( empty($dismantled_date_filter))
                startDate: moment().subtract(1, 'month').startOf('month'),
                endDate: moment().subtract(1, 'month').endOf('month'),
                @endif
                maxDate: moment(),
                locale: {
                    format: 'DD/MM/YYYY'
                },
                applyClass: 'bg-teal-300',
                cancelClass: 'btn-light',

            });

        </script>


    @endpush

    @push('style')
        <style>
            .bold {
                font-weight: 700;
            }

            .large {
                font-size: large;
            }
        </style>
    @endpush

    <div>
        <div class="content">
            <div class="title m-b-md">
                <h1 style="text-align: center;">Izveštaj za sve tehničare</h1>
            </div>

            @if ($errors->any())
                <div class="alert alert-danger">
                    @foreach ($errors->all() as $error)
                        <div>- {{ $error }}</div>
                    @endforeach
                </div>
            @endif

            <div>

                <br>

                <!-- Basic datatable -->
                <div class="card">
                    {!! Form::open(['action' => 'PagesController@techniciansreport_all_post', 'method' => 'POST']) !!}
                    <div class="card-header {{--header-elements-inline--}}">

                        <div class="row mb-3 col-12">

                            <div class="col-4 form-row mb-2">
                                <label>Filtriranje za period: </label>
                                <div class="input-group daterange">
                                    <span class="input-group-text"><i class="icon-calendar3"></i></span>
                                    <input name="dismantled_date_filter" style="width:200px" type="text"
                                           class="form-control daterange-basic" data-placeholder="Izaberite period"
                                           @if( ! empty($dismantled_date_filter))
                                           value="{{$dismantled_date_filter}}"
                                            @endif
                                    >
                                </div>
                            </div>

                            {{--  {{ Form::selectGroupSearch('technician',
                                             true,
                                             'Tehničar:',
                                             null,
                                             $technicians, $technician,
                                             ['data-placeholder' => 'Izaberite tehničara',
                                              'class'=> 'form-control select-search technician',
                                              'data-focus'], 'col-4 form-row mb-2') }}--}}

                            <div class="col-4 form-row mb-2">
                                <button type="submit" class="btn bg-teal-300 ml-3 mt-3" style="height: 70%">Primeni
                                    filter
                                </button>
                            </div>
                            <div class="col-4 form-row mb-2">

                            </div>
                        </div>


                        {!! Form::close() !!}
                    </div>


                        @if(isset($date_period))
                            <table class="table datatable-basic table-bordered table-striped">
                                <thead>
                                <tr>
                                    <th>Redni broj naloga</th>
                                    <th>Tip posla</th>
                                    <th>Za tehničara</th>
                                    <th>Vreme zatvaranja naloga</th>
                                    <th>Ukupno za nalog</th>
                                    <th></th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($techniciansreport as $report)
                                    <tr>
                                        <td>{{ $report->ordinal_number }}</td>
                                        <td>{{ $services[$report->service_name] }}</td>
                                        <td>{{ $technicians[$report->technician_id] }}</td>
                                        <td>{{ datetimeForView($report->completed_at) }}</td>
                                        <td>{{ $report->sum }} din.</td>

                                        <td><a class="btn btn-outline-dark"
                                               href="techniciansreport/{{ $report->technicians_report_id }}"
                                               role="button">Detalji</a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                            <div class="bold large p-3">Ukupno za izabrani period: {{ $sum }} din.</div>

                        @endif


                </div>
                <!-- /basic datatable -->

            </div>
        </div>
    </div>

@endsection



