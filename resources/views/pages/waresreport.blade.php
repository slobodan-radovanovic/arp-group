@extends('layouts.app')

@section('content')

    @push('style')
        <!-- Global stylesheets -->
        <link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
        <link href="../../../../global_assets/css/icons/icomoon/styles.css" rel="stylesheet" type="text/css">
        <link href="../assets/css/bootstrap.min.css" rel="stylesheet" type="text/css">
        <link href="../assets/css/bootstrap_limitless.min.css" rel="stylesheet" type="text/css">
        <link href="../assets/css/layout.min.css" rel="stylesheet" type="text/css">
        <link href="../assets/css/components.min.css" rel="stylesheet" type="text/css">
        <link href="../assets/css/colors.min.css" rel="stylesheet" type="text/css">
        <!-- /global stylesheets -->
    @endpush



    @push('scripts')
        <script src="../global_assets/js/plugins/tables/datatables/datatables.min.js"></script>
        <script src="../global_assets/js/demo_pages/datatables_basic.js"></script>
        <!-- Load select2 -->
        <script type="text/javascript" src="../global_assets/js/plugins/forms/selects/select2.min.js"></script>
        <script type="text/javascript" src="../global_assets/js/plugins/extensions/jquery_ui/interactions.min.js"></script>
        <script src="../assets/js/custom_select2.js"></script>
        <script src="../global_assets/js/plugins/forms/selects/select2.min.js"></script>
        {{--anytime--}}
        <script src="../global_assets/js/plugins/ui/moment/moment.min.js"></script>
        <script src="../global_assets/js/plugins/pickers/anytime.min.js"></script>
        <script src="../../../../global_assets/js/plugins/pickers/daterangepicker.js"></script>
        <script src="../assets/js/custom_datepicker.js"></script>

        <script src="../global_assets/js/main/jquery.min.js"></script>
        <script src="../global_assets/js/main/bootstrap.bundle.min.js"></script>
        <script src="../global_assets/js/plugins/loaders/blockui.min.js"></script>
        <script src="../global_assets/js/plugins/tables/datatables/datatables.min.js"></script>
        <script src="../global_assets/js/plugins/forms/selects/select2.min.js"></script>
        <script src="../global_assets/js/plugins/tables/datatables/extensions/jszip/jszip.min.js"></script>
        <script src="../global_assets/js/plugins/tables/datatables/extensions/buttons.min.js"></script>

        {{--export datatable--}}

        {{--anytime--}}
        <script src="../global_assets/js/plugins/ui/moment/moment.min.js"></script>
        <script src="../global_assets/js/plugins/pickers/anytime.min.js"></script>
        <script src="../../../../global_assets/js/plugins/pickers/daterangepicker.js"></script>
        <script src="../assets/js/custom_datepicker.js"></script>



        <script>
          @if( ! empty($date_period))

            var date_period = '{!! $date_period !!}';


            $('.datatable-basic1').DataTable({
                autoWidth: false,
                dom: '<"datatable-header"fBl><"datatable-scroll-wrap"t><"datatable-footer"ip>',
                language: {
                    search: '<span>Pretraga:</span> _INPUT_',
                    lengthMenu: '<span>Prikaz:</span> _MENU_',
                    info: '_START_ do _END_ od ukupno _TOTAL_',
                    paginate: {'first': 'First', 'last': 'Last', 'next': '→', 'previous': '←'}
                },
                buttons: {
                    buttons: [
                        {
                            extend: 'excelHtml5',
                            footer: true,
                            className: 'btn btn-light',
                            title: 'Utrošeni materijal '+date_period,
                            sheetName:'Utrošeni materijal'
                            /*exportOptions: {
                                columns: [ 0, 1, 2, 3]
                            }*/
                        }
                    ]
                }
            });

            $('.datatable-basic2').DataTable({
                autoWidth: false,
                dom: '<"datatable-header"fBl><"datatable-scroll-wrap"t><"datatable-footer"ip>',
                language: {
                    search: '<span>Pretraga:</span> _INPUT_',
                    lengthMenu: '<span>Prikaz:</span> _MENU_',
                    info: '_START_ do _END_ od ukupno _TOTAL_',
                    paginate: {'first': 'First', 'last': 'Last', 'next': '→', 'previous': '←'}
                },
                buttons: {
                    buttons: [
                        {
                            extend: 'excelHtml5',
                            footer: true,
                            className: 'btn btn-light',
                            title: 'Utrošeni materijal (po nalogu) '+date_period,
                            sheetName:'Utrošeni materijal'
                            /*exportOptions: {
                                columns: [ 0, 1, 2 ]
                            }*/
                        }
                    ]
                }
            });

            @endif


            $('.daterange-basic').daterangepicker({
                @if( empty($date_filter))
                startDate: moment().subtract(1, 'month').startOf('month'),
                endDate: moment().subtract(1, 'month').endOf('month'),
                @endif
                maxDate: moment(),
                locale: {
                    format: 'DD/MM/YYYY'
                },
                applyClass: 'bg-teal-300',
                cancelClass: 'btn-light',

            });

        </script>


    @endpush

    @push('style')
        <style>
            .bold {
                font-weight: 700;
            }

            .large {
                font-size: large;
            }
        </style>
    @endpush
    <div class="content">
        <div class="title m-b-md">
            <h1 style="text-align: center;">Izveštaj za utrošeni materijal</h1>
        </div>
        <br>
        <div class="card mb-1">
            {!! Form::open(['action' => 'PagesController@waresreport', 'method' => 'POST']) !!}
            <div class="card-header header-elements-inline">
                <div class="row col-12">

                    <div class="col-4 form-row mb-2">
                        <label>Filtriranje za period: </label>
                        <div class="input-group daterange">
                            <span class="input-group-text"><i class="icon-calendar3"></i></span>
                            <input name="date_filter" style="width:200px" type="text"
                                   class="form-control daterange-basic" data-placeholder="Izaberite period"
                                   @if( ! empty($date_filter))
                                   value="{{$date_filter}}"
                                    @endif
                            >
                        </div>
                    </div>

                    <div class="col-4 form-row mb-2">
                        <button type="submit" class="btn bg-teal-300 ml-3 mt-3" style="height: 70%">Primeni
                            filter
                        </button>
                    </div>
                </div>

                {!! Form::close() !!}


            </div>
        </div>
            @if(isset($date_filter))

                <div class="card">
                    <ul class="nav nav-pills nav-pills-bordered mt-2 ml-2">
                        <li class="nav-item"><a href="#bordered-pill1" class="nav-link active" data-toggle="tab">Izvestaj
                                po materijalu</a></li>
                        <li class="nav-item"><a href="#bordered-pill2" class="nav-link"
                                                data-toggle="tab">Izvestaj po nalogu</a></li>
                    </ul>


                    <div class="tab-content">
                        <div class="tab-pane fade show active" id="bordered-pill1">
                            <table class="table datatable-basic1 table-bordered table-striped">
                                <thead>
                                <tr>
                                    <th>Materijal</th>
                                    <th>Ukupno za period</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($waresreport1 as $report)
                                    <tr>
                                        <td>{{ $report->wares_name }} ({{ $report->wares_type }})</td>
                                        <td>{{ $report->total_quantity }}</td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>

                        <div class="tab-pane fade" id="bordered-pill2">


                            <table class="table datatable-basic2 table-bordered table-striped">
                                <thead>
                                <tr>
                                    <th>Redni broj naloga</th>
                                    <th>Materijal</th>
                                    <th>Količina</th>
                                    <th>Vreme zatvaranja naloga</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($waresreport2 as $report)
                                    <tr>
                                        <td>{{ $report->ordinal_number }}</td>
                                        <td>{{ $report->wares_name }} ({{ $report->wares_type }})</td>
                                        <td>{{ $report->quantity }} </td>
                                        <td>{{ datetimeForView($report->created_at) }}</td>

                                    </tr>
                                @endforeach
                                </tbody>
                            </table>

                        </div>
                    </div>
                </div>

            @endif

    </div>

@endsection



