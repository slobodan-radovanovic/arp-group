@extends('layouts.app')

@section('content')

    @push('style')
        <style>
            .nav-pills-bordered .nav-link.active {
                border-color: #4db6ac !important;
            }

            .nav-pills .nav-link.active, .nav-pills .show > .nav-link {
                color: #fff;
                background-color: #4db6ac !important;
            }


        </style>

        <!-- Global stylesheets -->
        <link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
        <link href="../../../../global_assets/css/icons/icomoon/styles.css" rel="stylesheet" type="text/css">
        <link href="../assets/css/bootstrap.min.css" rel="stylesheet" type="text/css">
        <link href="../assets/css/bootstrap_limitless.min.css" rel="stylesheet" type="text/css">
        <link href="../assets/css/layout.min.css" rel="stylesheet" type="text/css">
        <link href="../assets/css/components.min.css" rel="stylesheet" type="text/css">
        <link href="../assets/css/colors.min.css" rel="stylesheet" type="text/css">
        <!-- /global stylesheets -->
    @endpush
    @push('scripts')
        {{--<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
                integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
                crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
                integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
                crossorigin="anonymous"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
                integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
                crossorigin="anonymous"></script>--}}

        <script src="../global_assets/js/plugins/tables/datatables/datatables.min.js"></script>
        <script src="../global_assets/js/demo_pages/datatables_basic.js"></script>
        <!-- Load select2 -->
        <script type="text/javascript" src="../global_assets/js/plugins/forms/selects/select2.min.js"></script>
        <script type="text/javascript" src="../global_assets/js/plugins/extensions/jquery_ui/interactions.min.js"></script>
        <script src="../assets/js/custom_select2.js"></script>
        <script src="../global_assets/js/plugins/forms/selects/select2.min.js"></script>
        {{--anytime--}}
        <script src="../global_assets/js/plugins/ui/moment/moment.min.js"></script>
        <script src="../global_assets/js/plugins/pickers/anytime.min.js"></script>
        <script src="../../../../global_assets/js/plugins/pickers/daterangepicker.js"></script>
        <script src="../assets/js/custom_datepicker.js"></script>

        <script src="../global_assets/js/main/jquery.min.js"></script>
        <script src="../global_assets/js/main/bootstrap.bundle.min.js"></script>
        <script src="../global_assets/js/plugins/loaders/blockui.min.js"></script>
        <script src="../global_assets/js/plugins/tables/datatables/datatables.min.js"></script>
        <script src="../global_assets/js/plugins/forms/selects/select2.min.js"></script>
        <script src="../global_assets/js/plugins/tables/datatables/extensions/jszip/jszip.min.js"></script>
        <script src="../global_assets/js/plugins/tables/datatables/extensions/buttons.min.js"></script>

        {{--export datatable--}}

        {{--anytime--}}
        <script src="../global_assets/js/plugins/ui/moment/moment.min.js"></script>
        <script src="../global_assets/js/plugins/pickers/anytime.min.js"></script>
        <script src="../../../../global_assets/js/plugins/pickers/daterangepicker.js"></script>
        <script src="../assets/js/custom_datepicker.js"></script>



        <script>
            @if( ! empty($date_period))

            var date_period = '{!! $date_period !!}';


            $('.datatable-basic1').DataTable({
                autoWidth: false,
                dom: '<"datatable-header"fBl><"datatable-scroll-wrap"t><"datatable-footer"ip>',
                language: {
                    search: '<span>Pretraga:</span> _INPUT_',
                    lengthMenu: '<span>Prikaz:</span> _MENU_',
                    info: '_START_ do _END_ od ukupno _TOTAL_',
                    paginate: {'first': 'First', 'last': 'Last', 'next': '→', 'previous': '←'}
                },
                buttons: {
                    buttons: [
                        {
                            extend: 'excelHtml5',
                            footer: true,
                            className: 'btn btn-light',
                            title: 'Finansijski izveštaj '+date_period,
                            sheetName:'Finansijski izveštaj',
                            exportOptions: {
                                columns: [ 0, 1, 2, 3]
                            }
                        }
                    ]
                }
            });

            $('.datatable-basic2').DataTable({
                autoWidth: false,
                dom: '<"datatable-header"fBl><"datatable-scroll-wrap"t><"datatable-footer"ip>',
                language: {
                    search: '<span>Pretraga:</span> _INPUT_',
                    lengthMenu: '<span>Prikaz:</span> _MENU_',
                    info: '_START_ do _END_ od ukupno _TOTAL_',
                    paginate: {'first': 'First', 'last': 'Last', 'next': '→', 'previous': '←'}
                },
                buttons: {
                    buttons: [
                        {
                            extend: 'excelHtml5',
                            footer: true,
                            className: 'btn btn-light',
                            title: 'Finansijski izveštaj (SBB nalozi) '+date_period,
                            sheetName:'Finansijski izveštaj',
                            exportOptions: {
                                columns: [ 0, 1, 2 ]
                            }
                        }
                    ]
                }
            });

            $('.datatable-basic3').DataTable({
                autoWidth: false,
                dom: '<"datatable-header"fBl><"datatable-scroll-wrap"t><"datatable-footer"ip>',
                language: {
                    search: '<span>Pretraga:</span> _INPUT_',
                    lengthMenu: '<span>Prikaz:</span> _MENU_',
                    info: '_START_ do _END_ od ukupno _TOTAL_',
                    paginate: {'first': 'First', 'last': 'Last', 'next': '→', 'previous': '←'}
                },
                buttons: {
                    buttons: [
                        {
                            extend: 'excelHtml5',
                            footer: true,
                            className: 'btn btn-light',
                            title: 'Finansijski izveštaj (Digitalizacija) '+date_period,
                            sheetName:'Finansijski izveštaj',
                            exportOptions: {
                                columns: [ 0, 1, 2 ]
                            }
                        }
                    ]
                }
            });

            $('.datatable-basic4').DataTable({
                autoWidth: false,
                dom: '<"datatable-header"fBl><"datatable-scroll-wrap"t><"datatable-footer"ip>',
                language: {
                    search: '<span>Pretraga:</span> _INPUT_',
                    lengthMenu: '<span>Prikaz:</span> _MENU_',
                    info: '_START_ do _END_ od ukupno _TOTAL_',
                    paginate: {'first': 'First', 'last': 'Last', 'next': '→', 'previous': '←'}
                },
                buttons: {
                    buttons: [
                        {
                            extend: 'excelHtml5',
                            footer: true,
                            className: 'btn btn-light',
                            title: 'Finansijski izveštaj (Reklamacije) '+date_period,
                            sheetName:'Finansijski izveštaj',
                            exportOptions: {
                                columns: [ 0, 1, 2 ]
                            }
                        }
                    ]
                }
            });

            @endif


            $('.daterange-basic').daterangepicker({
                @if( empty($date_filter))
                startDate: moment().subtract(1, 'month').startOf('month'),
                endDate: moment().subtract(1, 'month').endOf('month'),
                @endif
                maxDate: moment(),
                locale: {
                    format: 'DD/MM/YYYY'
                },
                applyClass: 'bg-teal-300',
                cancelClass: 'btn-light',

            });

        </script>


    @endpush

    @push('style')
        <style>
            .bold {
                font-weight: 700;
            }

            .large {
                font-size: large;
            }
        </style>
    @endpush

    <div>
        <div class="content">
            <div class="title m-b-md">
                <h1 style="text-align: center;">Finansijski izveštaj</h1>
            </div>



            <div>

                <br>

                <!-- Basic datatable -->
                <div class="card">
                    {!! Form::open(['action' => 'PagesController@financereport', 'method' => 'POST']) !!}
                    <div class="card-header header-elements-inline">
                        {{-- {{dd($dismantled_date_filter)}} --}}

                        <div class="row mb-3 col-12">

                            <div class="col-4 form-row mb-2">
                                <label>Filtriranje za period: </label>
                                <div class="input-group daterange">
                                    <span class="input-group-text"><i class="icon-calendar3"></i></span>
                                    <input name="date_filter" style="width:200px" type="text"
                                           class="form-control daterange-basic" data-placeholder="Izaberite period"
                                           @if( ! empty($date_filter))
                                           value="{{$date_filter}}"
                                            @endif
                                    >
                                </div>
                            </div>

                            <div class="col-4 form-row mb-2">
                                <button type="submit" class="btn bg-teal-300 ml-3 mt-3" style="height: 70%">Primeni
                                    filter
                                </button>
                            </div>
                        </div>

                        {!! Form::close() !!}


                    </div>

                    @if(isset($date_filter))
                        <!-- Bordered pills -->
                            <div class="card">

                                <div class="card-body" style="border-bottom: 1px solid #bbbfc3;">
                                    <ul class="nav nav-pills nav-pills-bordered" style="margin-bottom:0 !important;">
                                        <li class="nav-item"><a href="#bordered-pill1" class="nav-link active" data-toggle="tab">Svi nalozi</a></li>
                                        <li class="nav-item"><a href="#bordered-pill2" class="nav-link"
                                                                data-toggle="tab">SBB nalozi</a></li>
                                        <li class="nav-item"><a href="#bordered-pill3" class="nav-link"
                                                                data-toggle="tab">Digitalizacija</a></li>
                                        <li class="nav-item"><a href="#bordered-pill4" class="nav-link"
                                                                data-toggle="tab">Reklamacije</a></li>

                                    </ul>
                                </div>


                                <div class="tab-content">
                                    <div class="tab-pane fade show active" id="bordered-pill1">

                                        <!-- Prikaz svih naloga datatable -->

                                        <table class="table datatable-basic1 table-bordered table-striped">
                                            <thead>
                                            <tr>
                                                <th>Redni broj naloga</th>
                                                <th>Tip naloga</th>
                                                <th>Vreme zatvaranja naloga</th>
                                                <th>Za fakturisanje</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @foreach($financereport as $report)
                                                <tr>
                                                    <td>{{ $report->ordinal_number }}</td>
                                                    <td>{{ $order_type[$report->order_type] }}</td>
                                                    <td>{{ datetimeForView($report->created_at) }}</td>
                                                    <td>{{ $report->sum }} din.</td>
                                                </tr>
                                            @endforeach
                                            {{--<tr>
                                                <td style="font-weight: bold"></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                            </tr>--}}
                                            </tbody>
                                            <tfoot>
                                            <tr>
                                                <th></th>
                                                <th></th>
                                                <th></th>
                                                <th style="text-align:right; font-size: large">Ukupno: {{ $sum }} din.</th>
                                            </tr>
                                            </tfoot>
                                        </table>
                                       {{-- <div class="bold large p-3">Ukupno za izabrani period: {{ $sum }} din.</div>--}}

                                        <!-- /Prikaz svih naloga datatable -->

                                    </div>

                                    <div class="tab-pane fade" id="bordered-pill2">
                                        <!-- Otovoreni datatable -->

                                        <table class="table datatable-basic2 table-bordered table-striped">
                                            <thead>
                                            <tr>
                                                <th>Redni broj naloga</th>

                                                <th>Vreme zatvaranja naloga</th>
                                                <th>Za fakturisanje</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @foreach($sbb_order as $report)
                                                <tr>
                                                    <td>{{ $report->ordinal_number }}</td>

                                                    <td>{{ datetimeForView($report->created_at) }}</td>
                                                    <td>{{ $report->sum }} din.</td>
                                                </tr>
                                            @endforeach
                                            </tbody>
                                            <tfoot>
                                            <tr>
                                                <th></th>
                                                <th></th>
                                                <th style="text-align:right; font-size: large">Ukupno: {{ $sbb_order_sum }} din.</th>
                                            </tr>
                                            </tfoot>
                                        </table>
                                        {{--<div class="bold large p-3">Ukupno za izabrani period: {{ $sbb_order_sum }} din.</div>--}}

                                        <!-- /Otovoreni datatable -->
                                    </div>


                                    <div class="tab-pane fade" id="bordered-pill3">
                                        <!-- zakazani datatable -->


                                        <table class="table datatable-basic3 table-bordered table-striped">
                                            <thead>
                                            <tr>
                                                <th>Redni broj naloga</th>

                                                <th>Vreme zatvaranja naloga</th>
                                                <th>Za fakturisanje</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @foreach($digitization as $report)
                                                <tr>
                                                    <td>{{ $report->ordinal_number }}</td>

                                                    <td>{{ datetimeForView($report->created_at) }}</td>
                                                    <td>{{ $report->sum }} din.</td>
                                                </tr>
                                            @endforeach
                                            </tbody>
                                            <tfoot>
                                            <tr>
                                                <th></th>
                                                <th></th>
                                                <th style="text-align:right; font-size: large">Ukupno: {{ $digitization_sum }} din.</th>
                                            </tr>
                                            </tfoot>
                                        </table>
                                        {{--<div class="bold large p-3">Ukupno za izabrani period: {{ $digitization_sum }} din.</div>--}}

                                        <!-- /zakazani datatable -->
                                    </div>

                                    <div class="tab-pane fade" id="bordered-pill4">
                                        <!-- odlozeni  -->


                                        <table class="table datatable-basic4 table-bordered table-striped">
                                            <thead>
                                            <tr>
                                                <th>Redni broj naloga</th>

                                                <th>Vreme zatvaranja naloga</th>
                                                <th>Za fakturisanje</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @foreach($reclamation as $report)
                                                <tr>
                                                    <td>{{ $report->ordinal_number }}</td>

                                                    <td>{{ datetimeForView($report->created_at) }}</td>
                                                    <td>{{ $report->sum }} din.</td>
                                                </tr>
                                            @endforeach
                                            </tbody>
                                            <tfoot>
                                            <tr>
                                                <th></th>
                                                <th></th>
                                                <th style="text-align:right; font-size: large">Ukupno: {{ $reclamation_sum }} din.</th>
                                            </tr>
                                            </tfoot>
                                        </table>
                                        {{--<div class="bold large p-3">Ukupno za izabrani period: {{ $reclamation_sum }} din.</div>--}}

                                    </div>

                                </div>
                            </div>
                            <!-- /Bordered pills -->

                </div>
                <!-- /basic datatable -->
                @endif
            </div>
        </div>
    </div>

@endsection



