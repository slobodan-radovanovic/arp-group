
@extends('layouts.app')

{{--@push('style')
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />

@endpush--}}

@push('scripts')
    <!-- Load select2 -->
    <script type="text/javascript" src="../global_assets/js/plugins/forms/selects/select2.min.js"></script>
    <script type="text/javascript" src="../global_assets/js/plugins/extensions/jquery_ui/interactions.min.js"></script>
    <script src="../assets/js/custom_select2.js"></script>

@endpush

@section('content')

    <!-- Main content -->
    <div class="content-wrapper">


        <div class="content pt-0">

            <div class="content">
                <div class="flex-center position-ref full-height">

                    <div class="content">
                        <div class="title m-b-md">
                            Testiranje<br>

                            <div class="form-group">
                                <label>Select with search</label>
                                <select class="form-control select-search sbb_Opština" data-fouc>
                                        <option value="AZ">Arizona
                                        <option value="CO">Colorado
                                        <option value="ID">Idaho
                                        <option value="WY">Wyoming
                                        <option value="AL">Alabama
                                        <option value="IA">Iowa
                                        <option value="KS">Kansas
                                        <option value="KY">Kentucky
                                </select>
                            </div>
                            <br><br>
                            <div class="form-group">
                                <label>Basic select</label>
                                <select class="form-control select" data-fouc>
                                    <optgroup label="Mountain Time Zone">
                                        <option value="AZ">Arizona
                                        <option value="CO">Colorado
                                        <option value="ID">Idaho
                                        <option value="WY">Wyoming
                                    </optgroup>
                                    <optgroup label="Central Time Zone">
                                        <option value="AL">Alabama
                                        <option value="AR">Arkansas
                                        <option value="KS">Kansas
                                        <option value="KY">Kentucky
                                    </optgroup>
                                </select>
                            </div>


                        </div>


                    </div>
                </div>

            </div>




        </div>
    </div>
    <!-- /main content -->




@endsection



