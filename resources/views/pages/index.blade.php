
@extends('layouts.app')

@section('content')

    {{--<!-- Main sidebar -->
    <div class="sidebar sidebar-dark sidebar-main sidebar-expand-md">

        <!-- Sidebar mobile toggler -->
        <div class="sidebar-mobile-toggler text-center">
            <a href="#" class="sidebar-mobile-main-toggle">
                <i class="icon-arrow-left8"></i>
            </a>
            Navigation
            <a href="#" class="sidebar-mobile-expand">
                <i class="icon-screen-full"></i>
                <i class="icon-screen-normal"></i>
            </a>
        </div>
        <!-- /sidebar mobile toggler -->


        <!-- Sidebar content -->
        <div class="sidebar-content">

            <!-- Main navigation -->
            <div class="card card-sidebar-mobile">
                <ul class="nav nav-sidebar" data-nav-type="accordion">

                    <!-- Main -->

                    <li class="nav-item">
                        <a href="/" class="nav-link active">
                            <i class="icon-home4"></i>
                            <span>
									Pocetna
                                <!--<span class="d-block font-weight-normal opacity-50">No active orders</span>-->
								</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="/orders" class="nav-link"><i class="icon-copy"></i> <span>Nalozi</span></a>

                    </li>
                    <li class="nav-item">
                        <a href="/scheduling" class="nav-link"><i class="icon-color-sampler"></i> <span>Zakazivanje</span></a>


                    </li>
                    <li class="nav-item">
                        <a href="/equipment" class="nav-link"><i class="icon-width"></i> <span>Oprema</span></a>


                    </li>
                    <li class="nav-item nav-item-submenu">
                        <a href="#" class="nav-link"><i class="icon-stack2"></i> <span>Magacinski materijal</span></a>

                        <ul class="nav nav-group-sub" data-submenu-title="Starter kit">
                            <li class="nav-item"><a href="/warehouse1" class="nav-link">Magacin 1</a></li>
                            <li class="nav-item"><a href="/warehouse2" class="nav-link">Magacin 2</a></li>

                        </ul>

                    </li>
                    <li class="nav-item nav-item-submenu">
                        <a href="#" class="nav-link"><i class="icon-stack"></i> <span>Izveštaji</span></a>

                        <ul class="nav nav-group-sub" data-submenu-title="Starter kit">
                            <li class="nav-item"><a href="/report1" class="nav-link">Finansijski izveštaj</a></li>
                            <li class="nav-item"><a href="/report2" class="nav-link">Izveštaj o zaposlenima</a></li>

                        </ul></li>
                    <!-- /main -->

                    <!-- Layout -->

                    <!-- /layout -->

                </ul>
            </div>
            <!-- /main navigation -->

        </div>
        <!-- /sidebar content -->

    </div>
    <!-- /main sidebar -->--}}

    <!-- Main content -->
    <div class="content-wrapper">


        <div class="content pt-0">

            <div class="content">
                <div class="flex-center position-ref full-height">

                    <div class="content">
                        <div class="title m-b-md">
                            Početna

                        </div>


                    </div>
                </div>

            </div>




        </div>
    </div>
    <!-- /main content -->




@endsection



