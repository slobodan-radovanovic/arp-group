@extends('layouts.app')

@push('style')
    <style>
        .nav-pills-bordered .nav-link.active {
            border-color: #4db6ac !important;
        }

        .nav-pills .nav-link.active, .nav-pills .show > .nav-link {
            color: #fff;
            background-color: #4db6ac !important;
        }


    </style>

    <!-- Global stylesheets -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet"
          type="text/css">
    <link href="../../../../global_assets/css/icons/icomoon/styles.css" rel="stylesheet" type="text/css">
    <link href="../assets/css/bootstrap.min.css" rel="stylesheet" type="text/css">
    <link href="../assets/css/bootstrap_limitless.min.css" rel="stylesheet" type="text/css">
    <link href="../assets/css/layout.min.css" rel="stylesheet" type="text/css">
    <link href="../assets/css/components.min.css" rel="stylesheet" type="text/css">
    <link href="../assets/css/colors.min.css" rel="stylesheet" type="text/css">
    <!-- /global stylesheets -->
@endpush
@push('scripts')
    {{--<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
            integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
            crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
            integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
            crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
            integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
            crossorigin="anonymous"></script>--}}

    <script src="../global_assets/js/plugins/tables/datatables/datatables.min.js"></script>
    <script src="../global_assets/js/demo_pages/datatables_basic.js"></script>
    <!-- Load select2 -->
    <script type="text/javascript" src="../global_assets/js/plugins/forms/selects/select2.min.js"></script>
    <script type="text/javascript" src="../global_assets/js/plugins/extensions/jquery_ui/interactions.min.js"></script>
    <script src="../assets/js/custom_select2.js"></script>
    <script src="../global_assets/js/plugins/forms/selects/select2.min.js"></script>
    {{--anytime--}}
    <script src="../global_assets/js/plugins/ui/moment/moment.min.js"></script>
    <script src="../global_assets/js/plugins/pickers/anytime.min.js"></script>
    <script src="../../../../global_assets/js/plugins/pickers/daterangepicker.js"></script>
    <script src="../assets/js/custom_datepicker.js"></script>

    <script src="../global_assets/js/main/jquery.min.js"></script>
    <script src="../global_assets/js/main/bootstrap.bundle.min.js"></script>
    <script src="../global_assets/js/plugins/loaders/blockui.min.js"></script>
    <script src="../global_assets/js/plugins/tables/datatables/datatables.min.js"></script>
    <script src="../global_assets/js/plugins/forms/selects/select2.min.js"></script>
    <script src="../global_assets/js/plugins/tables/datatables/extensions/jszip/jszip.min.js"></script>
    <script src="../global_assets/js/plugins/tables/datatables/extensions/buttons.min.js"></script>

    {{--export datatable--}}

    {{--anytime--}}
    <script src="../global_assets/js/plugins/ui/moment/moment.min.js"></script>
    <script src="../global_assets/js/plugins/pickers/anytime.min.js"></script>
    <script src="../../../../global_assets/js/plugins/pickers/daterangepicker.js"></script>
    <script src="../assets/js/custom_datepicker.js"></script>



    <script>

        var today = new Date();
        var dd = String(today.getDate()).padStart(2, '0');
        var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
        var yyyy = today.getFullYear();

        today = dd + '-' + mm + '-' + yyyy;
        console.log(today);


        $('.datatable-basic1').DataTable({
            autoWidth: false,
            columnDefs: [{
                width: 10,
                targets: [ 7 ]
            }],
            dom: '<"datatable-header"fBl><"datatable-scroll-wrap"t><"datatable-footer"ip>',
            language: {
                search: '<span>Pretraga:</span> _INPUT_',
                lengthMenu: '<span>Prikaz:</span> _MENU_',
                info: '_START_ do _END_ od ukupno _TOTAL_',
                paginate: {'first': 'First', 'last': 'Last', 'next': '→', 'previous': '←'}
            },
            buttons: {
                buttons: [
                    {
                        extend: 'excelHtml5',
                        className: 'btn btn-light',
                        title: 'Izvestaj za SBB (Nalozi u radu) ' + today,
                        sheetName: 'Nalozi u radu ' + today,
                        exportOptions: {
                            columns: [0, 1, 2, 3, 4, 5, 6, 7]
                        }
                    }
                ]
            }
        });


        $('.daterange-basic').daterangepicker({
            @if( empty($date_filter))
            startDate: moment().subtract(1, 'month').startOf('month'),
            endDate: moment().subtract(1, 'month').endOf('month'),
            @endif
            maxDate: moment(),
            locale: {
                format: 'DD/MM/YYYY'
            },
            applyClass: 'bg-teal-300',
            cancelClass: 'btn-light',

        });

        // hide all form-duration-divs
        /*$('.order_details').hide();*/
        @if( !empty($date_filter))
        $(".daterangecheckbox").hide();
        $(".daterange").show();
        @endif


        $(".daterangecheckbox").change(function () {
            $(".daterangecheckbox").hide(500);

            $(".daterange").show();
            console.log('datarange');
        });

    </script>
@endpush

@section('content')
    {{-- @if(isset($orders['region_filter']))
         @dd($orders['region_filter'])
     @endif--}}
    <div>
        <div class="content">
            <div class="title m-b-md">
                <h1 style="text-align: center;">Izveštaj za SBB - Nalozi u radu</h1>
            </div>

            <div class="card mb-1">
                <div class="form-group card-body  mb-0">
                    {!! Form::open(['action' => 'PagesController@sbbreport_in_progress', 'method' => 'POST']) !!}
                    <div class="daterangecheckbox">
                        <label>Filteri: </label>
                        <input type="checkbox">
                    </div>

                    <div class="row daterange" style="display: none">

                        <div class="input-group col-4" {{--style="display: block !important;"--}}>

                            <span class="input-group-text"><i class="icon-calendar3"></i></span>
                            <input name="date_filter" style="width:200px" type="text"
                                   class="form-control daterange-basic" data-placeholder="Izaberite period"
                                   {{--value="20/03/2019 - 22/03/2019"--}}
                                   @if( ! empty($date_filter))
                                   value="{{$date_filter}}"
                                    @endif
                            >
                        </div>


                        <button type="submit" class="btn bg-teal-300 ml-3 col-2">Primeni filter</button>
                        <div class="col-1"></div>
                        <a class="btn bg-teal-300 ml-3 col-2" href="/sbbreport_in_progress">Poništi filter</a>
                    </div>

                </div>
            </div>

            {!! Form::close() !!}
            <div>
            {{--<button type="button" class="btn bg-teal-300" data-toggle="modal" data-target="#modal_add">Dodavanje nove opreme <i class="icon-plus3 ml-1"></i></button>--}}

            <!-- Bordered pills -->
                <div class="card">

                    <div class="card-body" style="border-bottom: 1px solid #bbbfc3;">


                        <table class="table datatable-basic1 table-bordered table-striped">
                            <thead>
                            <tr>
                                <th>Redni broj naloga</th>
                                <th>Broj naloga</th>
                                {{--<th>Ime i preizme</th>
                                <th>Opština</th>--}}
                                <th>Region</th>
                                <th>Tip posla</th>
                                <th>Status naloga</th>
                                <th>Datum i vreme kreiranja</th>
                                <th>Zakazan za </th>
                                <th>Odložen </th>


                                <th></th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($orders['sbbreport_in_progress'] as $order)
                                <tr>
                                    <td><a class="text-secondary" href="orders/{{ $order->order_id }}/show">{{ $order->ordinal_number }}</a></td>
                                    <td>{{ $order->order_number }}</td>
                                    <td>{{ $order->region }}</td>
                                    <td>{{ $order->service->service_name }}</td>
                                    <td> {{ $order->order_status }} </td>
                                    <td>{{ datetimeForView($order->opened_at) }}</td>

                                    <td>
                                        @if($order->order_status=='Zakazan')
                                            {{ datetimeForView($order->scheduled_at) }}
                                        @else
                                            /
                                        @endif
                                    </td>
                                    <td>
                                        @if($order->order_status=='Odložen')
                                            {{ datetimeForView($order->updated_at) }}
                                        @else
                                            /
                                        @endif

                                    </td>

                                    <td><a class="btn btn-outline-dark" href="orders/{{ $order->order_id }}/show"
                                           role="button">Prikaži nalog</a>
                                    </td>
                                </tr>
                            @endforeach


                            </tbody>
                        </table>

                        <!-- /Prikaz svih naloga datatable -->

                    </div>



                </div>
            </div>
        </div>

@endsection



