
@extends('layouts.app')

@section('content')

    <style>
        .btn-success1{
            color: #fff;
            background-color: #50d339;
        }

        .btn-success2{
            color: #fff;
            background-color: #45b731;
        }

        .btn-success3{
            color: #fff;
            background-color: #3fa02e;
        }
    </style>
    {{--<!-- Main sidebar -->
    <div class="sidebar sidebar-dark sidebar-main sidebar-expand-md">

        <!-- Sidebar mobile toggler -->
        <div class="sidebar-mobile-toggler text-center">
            <a href="#" class="sidebar-mobile-main-toggle">
                <i class="icon-arrow-left8"></i>
            </a>
            Navigation
            <a href="#" class="sidebar-mobile-expand">
                <i class="icon-screen-full"></i>
                <i class="icon-screen-normal"></i>
            </a>
        </div>
        <!-- /sidebar mobile toggler -->


        <!-- Sidebar content -->
        <div class="sidebar-content">

            <!-- Main navigation -->
            <div class="card card-sidebar-mobile">
                <ul class="nav nav-sidebar" data-nav-type="accordion">

                    <!-- Main -->

                    <li class="nav-item">
                        <a href="/" class="nav-link">
                            <i class="icon-home4"></i>
                            <span>
									Pocetna
                                <!--<span class="d-block font-weight-normal opacity-50">No active orders</span>-->
								</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="/orders" class="nav-link"><i class="icon-copy"></i> <span>Nalozi</span></a>

                    </li>
                    <li class="nav-item">
                        <a href="/scheduling" class="nav-link active"><i class="icon-color-sampler"></i> <span>Zakazivanje</span></a>


                    </li>
                    <li class="nav-item">
                        <a href="/equipment" class="nav-link"><i class="icon-width"></i> <span>Oprema</span></a>


                    </li>
                    <li class="nav-item nav-item-submenu">
                        <a href="#" class="nav-link"><i class="icon-stack2"></i> <span>Magacinski materijal</span></a>

                        <ul class="nav nav-group-sub" data-submenu-title="Starter kit">
                            <li class="nav-item"><a href="/warehouse1" class="nav-link">Magacin 1</a></li>
                            <li class="nav-item"><a href="/warehouse2" class="nav-link">Magacin 2</a></li>

                        </ul>

                    </li>
                    <li class="nav-item nav-item-submenu">
                        <a href="#" class="nav-link"><i class="icon-stack"></i> <span>Izveštaji</span></a>

                        <ul class="nav nav-group-sub" data-submenu-title="Starter kit">
                            <li class="nav-item"><a href="/report1" class="nav-link">Finansijski izveštaj</a></li>
                            <li class="nav-item"><a href="/report2" class="nav-link">Izveštaj o zaposlenima</a></li>

                        </ul></li>
                    <!-- /main -->

                    <!-- Layout -->

                    <!-- /layout -->

                </ul>
            </div>
            <!-- /main navigation -->

        </div>
        <!-- /sidebar content -->

    </div>
    <!-- /main sidebar -->--}}

    <!-- Main content -->


    <div class="flex-center position-ref full-height">

        <div class="content">
            <div class="title m-b-md">
                <button type="button" {{--class="btn"--}} data-toggle="modal" data-target="#exampleModal">
                <img src="../global_assets/images/scheduling.jpg" style="width: 100%">
                </button>
                <!-- Button trigger modal -->
                {{--<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">
                    Launch demo modal
                </button>--}}

                <!-- Modal -->
                <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" >
                    <div class="modal-dialog modal-lg" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel">Otvoreni nalozi</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <img src="../global_assets/images/modal.jpg" style="width: 100%;">
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Odustani</button>
                                <button type="button" class="btn btn-primary">Zakaži</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


        </div>
    </div>



    {{--<div class="content-wrapper">
        <div class="content pt-0">
            <div class="flex-center position-ref full-height">
            <div class="content">--}}


{{--                <div class="float-left">


                <table class="table table-bordered float-left" style="width: 100%;">
                    <thead class="thead-light">
                    <tr>
                        <th scope="col">Filtriranje</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td> <p>Deo grada</p><br>


                        <div class="form-check text-left">
                            <input class="form-check-input" type="checkbox" id="gridCheck">
                            <label class="form-check-label" for="gridCheck">
                                Savski Venac
                            </label>
                        </div>
                        <div class="form-check text-left">
                            <input class="form-check-input" type="checkbox" id="gridCheck">
                            <label class="form-check-label" for="gridCheck">
                                Stari grad
                            </label>
                        </div>
                        <div class="form-check text-left">
                            <input class="form-check-input" type="checkbox" id="gridCheck">
                            <label class="form-check-label" for="gridCheck">
                                Rakovica
                            </label>
                        </div>
                            <div class="form-check text-left">
                                <input class="form-check-input" type="checkbox" id="gridCheck">
                                <label class="form-check-label" for="gridCheck">
                                    Palilula
                                </label>
                            </div>
                            <div class="form-check text-left">
                                <input class="form-check-input" type="checkbox" id="gridCheck">
                                <label class="form-check-label" for="gridCheck">
                                    Zemun
                                </label>
                            </div>
                            <div class="form-check text-left">
                                <input class="form-check-input" type="checkbox" id="gridCheck">
                                <label class="form-check-label" for="gridCheck">
                                    Novi Beograd
                                </label>
                            </div>
                        </td>


                    </tr>
                    <tr>
                        <td><p>Timovi</p><br>


                            <div class="form-check text-left">
                                <input class="form-check-input" type="checkbox" id="gridCheck">
                                <label class="form-check-label" for="gridCheck">
                                    Marko Petrovic<br>
                                    Nikola Pavlovic
                                </label>
                            </div>
                            <div class="form-check text-left">
                                <input class="form-check-input" type="checkbox" id="gridCheck">
                                <label class="form-check-label" for="gridCheck">
                                    Pavle Jović<br>
                                    Filip Todorović
                                </label>
                            </div>
                            <div class="form-check text-left">
                                <input class="form-check-input" type="checkbox" id="gridCheck">
                                <label class="form-check-label" for="gridCheck">
                                    Petar Matić<br>
                                    Bojan Marković
                                </label>
                            </div>


                        </td>


                    </tr>

                    </tbody>
                </table>
                </div>


                <div class="float-right">
                <table class="table table-bordered" style="width: 100%;">
                    <thead class="thead-light">
                    <tr>
                        <th scope="col"></th>
                        <th scope="col">Broj naloga<i class="fa fa-sort float-right" aria-hidden="true"></i></th>
                        <th scope="col">Ulica i deo grada<i class="fa fa-sort float-right" aria-hidden="true"></i></th>
                        <th scope="col">Tim<i class="fa fa-sort float-right" aria-hidden="true"></i></th>
                        <th scope="col">Datum i vreme <br>otvaranja naloga<i class="fa fa-sort float-right" aria-hidden="true"></i></th>
                        <th scope="col"></th>

                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td><input type="radio"></td>
                        <td>7000913314</td>
                        <td>Savska 21, Savski Venac</td>
                        <td>Marko Petrovic<br>
                            Nikola Pavlovic
                        </td>
                        <td>29.10.2018.<br>
                            09:18
                        </td>
                        <td><button type="button" class="btn btn-outline-secondary">Detaljnije</button></td>

                    </tr>
                    <tr>
                        <td><input type="radio"></td>
                        <td>7000913358</td>
                        <td>Prvomajska 14, Zemun</td>
                        <td>Pavle Jović<br>
                            Filip Todorović</td>
                        <td>29.10.2018.<br>
                            09:25
                        </td>
                        <td><button type="button" class="btn btn-outline-secondary">Detaljnije</button></td>

                    </tr>
                    <tr>
                        <td><input type="radio"></td>
                        <td>7000913355</td>
                        <td>Gandijeva 67, Novi Beograd</td>
                        <td>Petar Matić<br>
                            Bojan Marković</td>
                        <td>29.10.2018.<br>
                            09:30
                        </td>
                        <td><button type="button" class="btn btn-outline-secondary">Detaljnije</button></td>
                    </tr>
                    <tr>
                        <td><input type="radio"></td>
                        <td>7000913342</td>
                        <td>Kneza Višelava 16, Rakovica</td>
                        <td>Pavle Jović<br>
                            Filip Todorović</td>
                        <td>29.10.2018.<br>
                            09:35
                        </td>
                        <td><button type="button" class="btn btn-outline-secondary">Detaljnije</button></td>

                    </tr>
                    <tr>
                        <td><input type="radio"></td>
                        <td>7000913340</td>
                        <td>Cara Dušana 14, Stari Grad</td>
                        <td>Marko Petrovic<br>
                            Nikola Pavlovic</td>
                        <td>29.10.2018.<br>
                            09:40
                        </td>
                        <td><button type="button" class="btn btn-outline-secondary">Detaljnije</button></td>

                    </tr>
                    <tr>
                        <td><input type="radio"></td>
                        <td>7000913341</td>
                        <td>Bulevar despota Stefana 95, Palulula</td>
                        <td>Petar Matić<br>
                            Bojan Marković</td>
                        <td>29.10.2018.<br>
                            09:48
                        </td>
                        <td><button type="button" class="btn btn-outline-secondary">Detaljnije</button></td>
                    </tr>
                    </tbody>
                </table>

            </div>--}}

                {{--<table class="table table-bordered">
                    <thead class="thead-light">
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">1</th>
                        <th scope="col">2</th>
                        <th scope="col">3</th>
                        <th scope="col">4</th>
                        <th scope="col">5</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <th scope="row">09:00</th>
                        <td><button type="button" class="btn btn-success1">7000913314</button></td>
                        <td><button type="button" class="btn btn-primary">Zakaži</button></td>
                        <td><button type="button" class="btn btn-primary">Zakaži</button></td>
                        <td><button type="button" class="btn btn-primary">Zakaži</button></td>
                        <td><button type="button" class="btn btn-primary">Zakaži</button></td>
                    </tr>
                    <tr>
                        <th scope="row">09:30</th>
                        <td><button type="button" class="btn btn-success2">7000913355</button></td>
                        <td><button type="button" class="btn btn-primary">Zakaži</button></td>
                        <td><button type="button" class="btn btn-primary">Zakaži</button></td>
                        <td><button type="button" class="btn btn-primary">Zakaži</button></td>
                        <td><button type="button" class="btn btn-primary">Zakaži</button></td>
                    </tr>
                    <tr>
                        <th scope="row">10:00</th>
                        <td><button type="button" class="btn btn-success2">7000913355</button></td>
                        <td><button type="button" class="btn btn-primary">Zakaži</button></td>
                        <td><button type="button" class="btn btn-primary">Zakaži</button></td>
                        <td><button type="button" class="btn btn-primary">Zakaži</button></td>
                        <td><button type="button" class="btn btn-primary">Zakaži</button></td>
                    </tr>
                    <tr>
                        <th scope="row">10:30</th>
                        <td><button type="button" class="btn btn-success3">7000913342</button></td>
                        <td><button type="button" class="btn btn-primary">Zakaži</button></td>
                        <td><button type="button" class="btn btn-primary">Zakaži</button></td>
                        <td><button type="button" class="btn btn-primary">Zakaži</button></td>
                        <td><button type="button" class="btn btn-primary">Zakaži</button></td>
                    </tr>
                    <tr>
                        <th scope="row">11:00</th>
                        <td><button type="button" class="btn btn-success3">7000913342</button></td>
                        <td><button type="button" class="btn btn-primary">Zakaži</button></td>
                        <td><button type="button" class="btn btn-primary">Zakaži</button></td>
                        <td><button type="button" class="btn btn-primary">Zakaži</button></td>
                        <td><button type="button" class="btn btn-primary">Zakaži</button></td>
                    </tr>
                    <tr>
                        <th scope="row">11:30</th>
                        <td><button type="button" class="btn btn-success3">7000913342</button></td>
                        <td><button type="button" class="btn btn-primary">Zakaži</button></td>
                        <td><button type="button" class="btn btn-primary">Zakaži</button></td>
                        <td><button type="button" class="btn btn-primary">Zakaži</button></td>
                        <td><button type="button" class="btn btn-primary">Zakaži</button></td>
                    </tr>
                    <tr>
                        <th scope="row">12:00</th>
                        <td><button type="button" class="btn btn-primary">Zakaži</button></td>
                        <td><button type="button" class="btn btn-primary">Zakaži</button></td>
                        <td><button type="button" class="btn btn-primary">Zakaži</button></td>
                        <td><button type="button" class="btn btn-primary">Zakaži</button></td>
                        <td><button type="button" class="btn btn-primary">Zakaži</button></td>
                    </tr>
                    <tr>
                        <th scope="row">12:30</th>
                        <td><button type="button" class="btn btn-primary">Zakaži</button></td>
                        <td><button type="button" class="btn btn-primary">Zakaži</button></td>
                        <td><button type="button" class="btn btn-primary">Zakaži</button></td>
                        <td><button type="button" class="btn btn-primary">Zakaži</button></td>
                        <td><button type="button" class="btn btn-primary">Zakaži</button></td>
                    </tr>
                    <tr>
                        <th scope="row">13:00</th>
                        <td><button type="button" class="btn btn-primary">Zakaži</button></td>
                        <td><button type="button" class="btn btn-primary">Zakaži</button></td>
                        <td><button type="button" class="btn btn-primary">Zakaži</button></td>
                        <td><button type="button" class="btn btn-primary">Zakaži</button></td>
                        <td><button type="button" class="btn btn-primary">Zakaži</button></td>
                    </tr>
                    <tr>
                        <th scope="row">13:30</th>
                        <td><button type="button" class="btn btn-primary">Zakaži</button></td>
                        <td><button type="button" class="btn btn-primary">Zakaži</button></td>
                        <td><button type="button" class="btn btn-primary">Zakaži</button></td>
                        <td><button type="button" class="btn btn-primary">Zakaži</button></td>
                        <td><button type="button" class="btn btn-primary">Zakaži</button></td>
                    </tr>
                    <tr>
                        <th scope="row">14:00</th>
                        <td><button type="button" class="btn btn-primary">Zakaži</button></td>
                        <td><button type="button" class="btn btn-primary">Zakaži</button></td>
                        <td><button type="button" class="btn btn-primary">Zakaži</button></td>
                        <td><button type="button" class="btn btn-primary">Zakaži</button></td>
                        <td><button type="button" class="btn btn-primary">Zakaži</button></td>
                    </tr>
                    <tr>
                        <th scope="row">14:30</th>
                        <td><button type="button" class="btn btn-primary">Zakaži</button></td>
                        <td><button type="button" class="btn btn-primary">Zakaži</button></td>
                        <td><button type="button" class="btn btn-primary">Zakaži</button></td>
                        <td><button type="button" class="btn btn-primary">Zakaži</button></td>
                        <td><button type="button" class="btn btn-primary">Zakaži</button></td>
                    </tr>
                    <tr>
                        <th scope="row">15:00</th>
                        <td><button type="button" class="btn btn-primary">Zakaži</button></td>
                        <td><button type="button" class="btn btn-primary">Zakaži</button></td>
                        <td><button type="button" class="btn btn-primary">Zakaži</button></td>
                        <td><button type="button" class="btn btn-primary">Zakaži</button></td>
                        <td><button type="button" class="btn btn-primary">Zakaži</button></td>
                    </tr>
                    <tr>
                        <th scope="row">16:30</th>
                        <td><button type="button" class="btn btn-primary">Zakaži</button></td>
                        <td><button type="button" class="btn btn-primary">Zakaži</button></td>
                        <td><button type="button" class="btn btn-primary">Zakaži</button></td>
                        <td><button type="button" class="btn btn-primary">Zakaži</button></td>
                        <td><button type="button" class="btn btn-primary">Zakaži</button></td>
                    </tr>
                    <tr>
                        <th scope="row">17:00</th>
                        <td><button type="button" class="btn btn-primary">Zakaži</button></td>
                        <td><button type="button" class="btn btn-primary">Zakaži</button></td>
                        <td><button type="button" class="btn btn-primary">Zakaži</button></td>
                        <td><button type="button" class="btn btn-primary">Zakaži</button></td>
                        <td><button type="button" class="btn btn-primary">Zakaži</button></td>
                    </tr>
                    <tr>
                        <th scope="row">17:30</th>
                        <td><button type="button" class="btn btn-primary">Zakaži</button></td>
                        <td><button type="button" class="btn btn-primary">Zakaži</button></td>
                        <td><button type="button" class="btn btn-primary">Zakaži</button></td>
                        <td><button type="button" class="btn btn-primary">Zakaži</button></td>
                        <td><button type="button" class="btn btn-primary">Zakaži</button></td>
                    </tr>
                    <tr>
                        <th scope="row">18:00</th>
                        <td><button type="button" class="btn btn-primary">Zakaži</button></td>
                        <td><button type="button" class="btn btn-primary">Zakaži</button></td>
                        <td><button type="button" class="btn btn-primary">Zakaži</button></td>
                        <td><button type="button" class="btn btn-primary">Zakaži</button></td>
                        <td><button type="button" class="btn btn-primary">Zakaži</button></td>
                    </tr>
                    <tr>
                        <th scope="row">18:30</th>
                        <td><button type="button" class="btn btn-primary">Zakaži</button></td>
                        <td><button type="button" class="btn btn-primary">Zakaži</button></td>
                        <td><button type="button" class="btn btn-primary">Zakaži</button></td>
                        <td><button type="button" class="btn btn-primary">Zakaži</button></td>
                        <td><button type="button" class="btn btn-primary">Zakaži</button></td>
                    </tr>
                    <tr>
                        <th scope="row">19:00</th>
                        <td><button type="button" class="btn btn-primary">Zakaži</button></td>
                        <td><button type="button" class="btn btn-primary">Zakaži</button></td>
                        <td><button type="button" class="btn btn-primary">Zakaži</button></td>
                        <td><button type="button" class="btn btn-primary">Zakaži</button></td>
                        <td><button type="button" class="btn btn-primary">Zakaži</button></td>
                    </tr>



                    </tbody>
                </table>--}}


            {{--</div>
            </div>




        </div>
    </div>--}}
    <!-- /main content -->







@endsection



