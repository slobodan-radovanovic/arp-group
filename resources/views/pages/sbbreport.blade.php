@extends('layouts.app')

@section('content')

    @push('style')

               <!-- Global stylesheets -->
        <link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
        <link href="../../../../global_assets/css/icons/icomoon/styles.css" rel="stylesheet" type="text/css">
        <link href="../assets/css/bootstrap.min.css" rel="stylesheet" type="text/css">
        <link href="../assets/css/bootstrap_limitless.min.css" rel="stylesheet" type="text/css">
        <link href="../assets/css/layout.min.css" rel="stylesheet" type="text/css">
        <link href="../assets/css/components.min.css" rel="stylesheet" type="text/css">
        <link href="../assets/css/colors.min.css" rel="stylesheet" type="text/css">
        <!-- /global stylesheets -->
    @endpush



    @push('scripts')
        <script src="../global_assets/js/plugins/tables/datatables/datatables.min.js"></script>
        <script src="../global_assets/js/demo_pages/datatables_basic.js"></script>
        <!-- Load select2 -->
        <script type="text/javascript" src="../global_assets/js/plugins/forms/selects/select2.min.js"></script>
        <script type="text/javascript" src="../global_assets/js/plugins/extensions/jquery_ui/interactions.min.js"></script>
        <script src="../assets/js/custom_select2.js"></script>
        <script src="../global_assets/js/plugins/forms/selects/select2.min.js"></script>
        {{--anytime--}}
        <script src="../global_assets/js/plugins/ui/moment/moment.min.js"></script>
        <script src="../global_assets/js/plugins/pickers/anytime.min.js"></script>
        <script src="../../../../global_assets/js/plugins/pickers/daterangepicker.js"></script>
        <script src="../assets/js/custom_datepicker.js"></script>

        <script src="../global_assets/js/main/jquery.min.js"></script>
        <script src="../global_assets/js/main/bootstrap.bundle.min.js"></script>
        <script src="../global_assets/js/plugins/loaders/blockui.min.js"></script>
        <script src="../global_assets/js/plugins/tables/datatables/datatables.min.js"></script>
        <script src="../global_assets/js/plugins/forms/selects/select2.min.js"></script>
        <script src="../global_assets/js/plugins/tables/datatables/extensions/jszip/jszip.min.js"></script>
        <script src="../global_assets/js/plugins/tables/datatables/extensions/buttons.min.js"></script>

        {{--export datatable--}}

        {{--anytime--}}
        <script src="../global_assets/js/plugins/ui/moment/moment.min.js"></script>
        <script src="../global_assets/js/plugins/pickers/anytime.min.js"></script>
        <script src="../../../../global_assets/js/plugins/pickers/daterangepicker.js"></script>
        <script src="../assets/js/custom_datepicker.js"></script>






        <script>
            @if( ! empty($date_period))

            var date_period = '{!! $date_period !!}';
            $('.datatable-basic3').DataTable({
                autoWidth: false,
                dom: '<"datatable-header"fBl><"datatable-scroll-wrap"t><"datatable-footer"ip>',
                language: {
                    search: '<span>Pretraga:</span> _INPUT_',
                    lengthMenu: '<span>Prikaz:</span> _MENU_',
                    info: '_START_ do _END_ od ukupno _TOTAL_',
                    paginate: {'first': 'First', 'last': 'Last', 'next': '→', 'previous': '←'}
                },
                buttons: {
                    buttons: [
                        {
                            extend: 'excelHtml5',
                            footer: true,
                            className: 'btn btn-light',
                            title: 'Završeni nalozi (Digitalizacija) '+date_period,
                            sheetName:'Završeni nalozi',
                            /*exportOptions: {
                                columns: [  { "visible": true }]
                            }*/
                        }
                    ]
                }
            });

            $('.datatable-basic1').DataTable({
                autoWidth: false,
                dom: '<"datatable-header"fBl><"datatable-scroll-wrap"t><"datatable-footer"ip>',
                language: {
                    search: '<span>Pretraga:</span> _INPUT_',
                    lengthMenu: '<span>Prikaz:</span> _MENU_',
                    info: '_START_ do _END_ od ukupno _TOTAL_',
                    paginate: {'first': 'First', 'last': 'Last', 'next': '→', 'previous': '←'}
                },
                buttons: {
                    buttons: [
                        {
                            extend: 'excelHtml5',
                            footer: true,
                            className: 'btn btn-light',
                            title: 'Završeni nalozi (SBB nalozi) '+date_period,
                            sheetName:'Završeni nalozi',
                            /*exportOptions: {
                                columns: [ 0, 1, 2, 3 ]
                            }*/
                        }
                    ]
                }
            });

            $('.datatable-basic2').DataTable({
                autoWidth: false,
                dom: '<"datatable-header"fBl><"datatable-scroll-wrap"t><"datatable-footer"ip>',
                language: {
                    search: '<span>Pretraga:</span> _INPUT_',
                    lengthMenu: '<span>Prikaz:</span> _MENU_',
                    info: '_START_ do _END_ od ukupno _TOTAL_',
                    paginate: {'first': 'First', 'last': 'Last', 'next': '→', 'previous': '←'}
                },
                buttons: {
                    buttons: [
                        {
                            extend: 'excelHtml5',
                            footer: true,
                            className: 'btn btn-light',
                            title: 'Završeni nalozi (Reklamacije) '+date_period,
                            sheetName:'Završeni nalozi',
                            /*exportOptions: {
                                columns: [ 0, 1, 2, 3 ]
                            }*/
                        }
                    ]
                }
            });


            @endif


            $('.daterange-basic').daterangepicker({
                @if( empty($date_filter))
                startDate: moment().subtract(1, 'month').startOf('month'),
                endDate: moment().subtract(1, 'month').endOf('month'),
                @endif
                maxDate: moment(),
                locale: {
                    format: 'DD/MM/YYYY'
                },
                applyClass: 'bg-teal-300',
                cancelClass: 'btn-light',

            });

        </script>


    @endpush

    @push('style')
        <style>
            .bold {
                font-weight: 700;
            }

            .large {
                font-size: large;
            }
        </style>
    @endpush



    <div>
        <div class="content">
            <div class="title m-b-md">
                <h1 style="text-align: center;">Izveštaj za SBB</h1>
            </div>

            <div>

                <br>

                <!-- Basic datatable -->
                <div class="card">
                    <div class="form-group card-body  mb-0">
                        {!! Form::open(['action' => 'PagesController@sbbreport', 'method' => 'POST']) !!}
                        {{--<div class="daterangecheckbox">
                            <label>Filteri:  </label>
                            <input type="checkbox">
                        </div>
--}}
                        <div class="row daterange" {{--style="display: none"--}}>

                            <div class="input-group col-4" {{--style="display: block !important;"--}}>

                                <span class="input-group-text"><i class="icon-calendar3"></i></span>
                                <input name="date_filter" style="width:200px" type="text" class="form-control daterange-basic" data-placeholder="Izaberite period" {{--value="20/03/2019 - 22/03/2019"--}}
                                @if(isset($date_filter))
                               {{-- @if( ! empty($date_filter))--}}
                                value="{{$date_filter}}"
                                        @endif
                                >
                            </div>


                            @if(isset($region_filter))

                                {{ Form::selectGroupSearch('region', false, 'Region:', null,
                      $region, $region_filter,
                         ['data-placeholder' => 'Filter za region',
                         'class'=> 'form-control select-search region'], 'col-2') }}

                            @else

                                {{ Form::selectGroupSearch('region', false, 'Region:', null,
                      $region, null,
                         ['data-placeholder' => 'Filter za region',
                         'class'=> 'form-control select-search region'], 'col-2') }}

                            @endif

                            <button type="submit" class="btn bg-teal-300 ml-3 col-2">Primeni filtere</button>
                            <div class="col-1"></div>
                            <a class="btn bg-teal-300 ml-3 col-2" href="/sbbreport">Poništi filtere</a>
                        </div>
                        {!! Form::close() !!}
                    </div>

                    @if(isset($date_filter))
                        <br>
                        <h4 style="text-align: center;">SBB nalozi</h4>
                        <table class="table datatable-basic1 table-bordered table-striped">
                            <thead>
                            <tr>
                                <th>Broj naloga</th>
                                <th>Region</th>
                                <th>Tip posla</th>
                                <th>Datum kreiranja</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($orders['sbb_order'] as $order)
                                <tr>
                                    <td>{{ $order->order_number }}</td>
                                    <td>{{ $order->region }}</td>
                                    <td>{{ $orders['services'][$order->service_type] }}</td>
                                    <td>{{ datetimeForView($order->created_at) }}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        <hr>
                        <h4 style="text-align: center;">Reklamacioni nalozi</h4>
                        <table class="table datatable-basic2 table-bordered table-striped">
                            <thead>
                            <tr>
                                <th>Broj naloga</th>
                                <th>Region</th>
                                <th>Tip posla</th>
                                <th>Datum kreiranja</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($orders['reclamation'] as $order)
                                <tr>
                                    <td>{{ $order->order_number }}</td>
                                    <td>{{ $order->region }}</td>
                                    <td>{{ $orders['services'][$order->service_type] }}</td>
                                    <td>{{ datetimeForView($order->created_at) }}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>

                        @if(!isset($region_filter))
                        <hr>
                        <h4 style="text-align: center;">Nalozi za digializaciju</h4>
                        <table class="table datatable-basic3 table-bordered table-striped">
                            <thead>
                            <tr>
                                <th>Broj naloga</th>

                                <th>Datum kreiranja</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($orders['digitization'] as $order)
                                <tr>
                                    <td>{{ $order->ordinal_number }}</td>

                                    <td>{{ datetimeForView($order->created_at) }}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        @endif
                </div>
                <!-- /basic datatable -->
                @endif
            </div>
        </div>
    </div>

@endsection



