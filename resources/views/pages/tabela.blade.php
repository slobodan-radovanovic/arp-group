@extends('layouts.app')

@section('content')

    @push('style')

        <!-- Global stylesheets -->
        <link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
        <link href="../../../../global_assets/css/icons/icomoon/styles.css" rel="stylesheet" type="text/css">
        <link href="../assets/css/bootstrap.min.css" rel="stylesheet" type="text/css">
        <link href="../assets/css/bootstrap_limitless.min.css" rel="stylesheet" type="text/css">
        <link href="../assets/css/layout.min.css" rel="stylesheet" type="text/css">
        <link href="../assets/css/components.min.css" rel="stylesheet" type="text/css">
        <link href="../assets/css/colors.min.css" rel="stylesheet" type="text/css">
        <!-- /global stylesheets -->
    @endpush

    @push('scripts')
        <script src="../global_assets/js/plugins/tables/datatables/datatables.min.js"></script>
        <script src="../global_assets/js/demo_pages/datatables_basic.js"></script>
        <!-- Load select2 -->
        <script type="text/javascript" src="../global_assets/js/plugins/forms/selects/select2.min.js"></script>
        <script type="text/javascript" src="../global_assets/js/plugins/extensions/jquery_ui/interactions.min.js"></script>
        <script src="../assets/js/custom_select2.js"></script>
        <script src="../global_assets/js/plugins/forms/selects/select2.min.js"></script>
        {{--anytime--}}
        <script src="../global_assets/js/plugins/ui/moment/moment.min.js"></script>
        <script src="../global_assets/js/plugins/pickers/anytime.min.js"></script>
        <script src="../../../../global_assets/js/plugins/pickers/daterangepicker.js"></script>
        <script src="../assets/js/custom_datepicker.js"></script>

        <script src="../global_assets/js/main/jquery.min.js"></script>
        <script src="../global_assets/js/main/bootstrap.bundle.min.js"></script>
        <script src="../global_assets/js/plugins/loaders/blockui.min.js"></script>
        <script src="../global_assets/js/plugins/tables/datatables/datatables.min.js"></script>
        <script src="../global_assets/js/plugins/forms/selects/select2.min.js"></script>
        <script src="../global_assets/js/plugins/tables/datatables/extensions/jszip/jszip.min.js"></script>
        <script src="../global_assets/js/plugins/tables/datatables/extensions/buttons.min.js"></script>

        {{--export datatable--}}

        {{--anytime--}}
        <script src="../global_assets/js/plugins/ui/moment/moment.min.js"></script>
        <script src="../global_assets/js/plugins/pickers/anytime.min.js"></script>
        <script src="../../../../global_assets/js/plugins/pickers/daterangepicker.js"></script>
        <script src="../assets/js/custom_datepicker.js"></script>



        <script>

            var today = new Date();
            var dd = String(today.getDate()).padStart(2, '0');
            var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
            var yyyy = today.getFullYear();

            today = dd + '-' + mm + '-' + yyyy;
            console.log(today);


            $('.datatable-basic').DataTable({
                autoWidth: false,
                dom: '<"datatable-header"fBl><"datatable-scroll-wrap"t><"datatable-footer"ip>',
                language: {
                    search: '<span>Pretraga:</span> _INPUT_',
                    lengthMenu: '<span>Prikaz:</span> _MENU_',
                    info: '_START_ do _END_ od ukupno _TOTAL_',
                    paginate: {'first': 'First', 'last': 'Last', 'next': '→', 'previous': '←'}
                },
                buttons: {
                    buttons: [
                        {
                            extend: 'excelHtml5',
                            className: 'btn btn-light',
                            title: 'Magacin materijala '+today,
                            sheetName:'Magacin materijala',
                            exportOptions: {
                                columns: [ 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
                            }
                        }
                    ]
                }
            });

        </script>



        {{-- dodatno ubaceno--}}
        <script src="../global_assets/js/plugins/ui/moment/moment.min.js"></script>
        <script src="../global_assets/js/plugins/pickers/anytime.min.js"></script>



        <script src="../assets/js/custom_datepicker.js"></script>


        <!-- Load select2 -->
        <script type="text/javascript" src="../global_assets/js/plugins/forms/selects/select2.min.js"></script>
        <script type="text/javascript" src="../global_assets/js/plugins/extensions/jquery_ui/interactions.min.js"></script>
        <script src="../assets/js/custom_select2.js"></script>






    @endpush

    <div>
        <div class="content">
            <div class="title m-b-md">
                <h1 style="text-align: center;">Magacin materijala</h1>
            </div>

            <div>


                <!-- Basic datatable -->
                <div class="card mt-2">

                    <table class="table datatable-basic table-bordered table-striped">
                        {{--table-bordered dataTable table-striped--}}
                        <thead>
                        <tr>
                            <th>Kategorija</th>
                            <th>Ime i prezime</th>
                            <th>Naziv</th>
                            {{-- <th>Vreme</th>
                             <th>Mesto</th>--}}
                            <th>Trajanje</th>
                            <th>Troskovi</th>
                            <th>Čovek dan</th>
                            {{--<th>Napomena</th>--}}
                            <th>Status</th>
                            <th>Komenatar direktora</th>
                            <th>Detalji</th>
                        </tr>

                        </thead>
                        <tbody>


                        <tr>
                            <td>Stručno usavršavanje - Konferencija</td>
                            <td>Slobodan Radovanović<br><hr style="margin: 2px;height: 1px;
        background-color: #b8c2cc;
        border: none;">
                                Milena Branković
                            </td>
                            <td>Enter Konferencija</td>
                            {{--<td>16-17 maj 2020</td>
                            <td>Beograd</td>--}}
                            <td>2</td>
                            <td>80€</td>
                            <td>4</td>
                            {{--<td>Navedena cena je sa uracunatim popustom od 20% ukoliko se uplati do 20 februara</td>--}}
                            <td><span class="badge badge-danger">&nbsp&nbsp Odbijen &nbsp&nbsp</span></td>
                            <td>
                                Komenatar direktora
                            </td>
                            <td>
                                <button type="button" class="btn btn-outline bg-slate-600 text-slate-600 border-slate-600">Detalji</button>
                            </td>
                        </tr>

                        <tr>
                            <td>Stručno usavršavanje - Kurs</td>
                            <td>Milena Branković</td>
                            <td>ScrumMaster course – Agilno programiranje</td>
                            {{--  <td>Nepoznato</td>
                              <td>Beograd</td>--}}
                            <td>2</td>
                            <td>500€</td>
                            <td>2</td>
                            {{--<td></td>--}}
                            <td>
                                <span class="badge badge-success">Prihvaćeno</span>
                            </td>
                            <td>
                            </td>
                            <td>
                                <button type="button" class="btn btn-outline bg-slate-600 text-slate-600 border-slate-600">Detalji</button>
                            </td>
                        </tr>

                        <tr>
                            <td>Stručna literatura</td>
                            <td></td>
                            <td>Stručne knjige</td>
                            {{--<td></td>
                            <td></td>--}}
                            <td></td>
                            <td>200€</td>
                            {{--<td></td>--}}
                            <td></td>
                            <td>
                                <span class="badge badge-warning">Na čekanju</span>
                            </td>
                            <td>
                            </td>
                            <td>
                                <button type="button" class="btn btn-outline bg-slate-600 text-slate-600 border-slate-600">Detalji</button>
                            </td>
                        </tr>





                        </tbody>
                    </table>


                </div>

                <div class="card mt-2">

                    <table class="table datatable-basic table-bordered table-striped">
                        {{--table-bordered dataTable table-striped--}}
                        <thead>
                        <tr>
                            <th>Kategorija</th>
                            <th>Ime i prezime</th>
                            <th>Naziv</th>
                            {{-- <th>Vreme</th>
                             <th>Mesto</th>--}}
                            <th>Trajanje</th>
                            <th>Troskovi</th>
                            <th>Čovek dan</th>
                            {{--<th>Napomena</th>--}}
                            <th>Status</th>
                            <th>Komenatar direktora</th>
                            <th>Detalji</th>
                            <th>Grupna akcija</th>
                        </tr>

                        </thead>
                        <tbody>


                        <tr>
                            <td>Stručno usavršavanje - Konferencija</td>
                            <td>Slobodan Radovanović<br><hr style="margin: 2px;height: 1px;
        background-color: #b8c2cc;
        border: none;">
                                Milena Branković
                            </td>
                            <td>Enter Konferencija</td>
                            {{--<td>16-17 maj 2020</td>
                            <td>Beograd</td>--}}
                            <td>2</td>
                            <td>80€</td>
                            <td>4</td>
                            {{--<td>Navedena cena je sa uracunatim popustom od 20% ukoliko se uplati do 20 februara</td>--}}
                            <td><span class="badge badge-danger">&nbsp&nbsp Odbijen &nbsp&nbsp</span></td>
                            <td>
                                Komenatar direktora
                            </td>
                            <td>
                                <button type="button" class="btn btn-outline bg-slate-600 text-slate-600 border-slate-600">Detalji</button>
                            </td>

                            {{--<td>

                                <button type="button" class="btn btn-outline-success"><i class="icon-checkmark2"></i> Prihvati</button>

                                <br>
                                <br>
                                <button type="button" class="btn btn-outline-danger"><i class="icon-cross3"></i> Odbij </button>
                            </td>--}}

                            <td>
                                <div class="uniform-checker"><span><input type="checkbox" class="form-check-input-styled" data-fouc=""></span></div>

                            </td>
                        </tr>

                        <tr>
                            <td>Stručno usavršavanje - Kurs</td>
                            <td>Milena Branković</td>
                            <td>ScrumMaster course – Agilno programiranje</td>
                            {{--  <td>Nepoznato</td>
                              <td>Beograd</td>--}}
                            <td>2</td>
                            <td>500€</td>
                            <td>2</td>
                            {{--<td></td>--}}
                            <td>
                                <span class="badge badge-success">Prihvaćeno</span>
                            </td>
                            <td>
                            </td>
                            <td>
                                <button type="button" class="btn btn-outline bg-slate-600 text-slate-600 border-slate-600">Detalji</button>
                            </td>

                            <td>
                                <div class="uniform-checker"><span><input type="checkbox" class="form-check-input-styled" data-fouc=""></span></div>

                            </td>


                        </tr>

                        <tr>
                            <td>Stručna literatura</td>
                            <td></td>
                            <td>Stručne knjige</td>
                            {{--<td></td>
                            <td></td>--}}
                            <td></td>
                            <td>200€</td>
                            {{--<td></td>--}}
                            <td></td>
                            <td>
                                <span class="badge badge-warning">Na čekanju</span>
                            </td>
                            <td>
                            </td>
                            <td>
                                <button type="button" class="btn btn-outline bg-slate-600 text-slate-600 border-slate-600">Detalji</button>
                            </td>

                            <td>
                                <div class="uniform-checker"><span class="checked"><input type="checkbox" class="form-check-input-styled" checked="" data-fouc=""></span></div>

                            </td>
                        </tr>





                        </tbody>
                        <tfoot>
                        <td colspan="9"></td>
                        <td>
                            <button type="button" class="btn btn-outline-success mb-1"><i class="icon-checkmark2"></i> Prihvati&nbsp</button>

                            <br>
                            <button type="button" class="btn btn-outline-danger">&nbsp <i class="icon-cross3"></i> Odbij &nbsp&nbsp</button></td>
                        </tfoot>
                    </table>


                </div>
                <!-- /basic datatable -->

            </div>
        </div>
    </div>

@endsection



