@extends('layouts.app')
@push('scripts')


    <script src="../global_assets/js/plugins/ui/moment/moment.min.js"></script>
    <script src="../global_assets/js/plugins/pickers/anytime.min.js"></script>



    <script src="../assets/js/custom_datepicker.js"></script>


    <!-- Load select2 -->
    <script type="text/javascript" src="../global_assets/js/plugins/forms/selects/select2.min.js"></script>
    <script type="text/javascript" src="../global_assets/js/plugins/extensions/jquery_ui/interactions.min.js"></script>
    <script src="../assets/js/custom_select2.js"></script>

    <script>
        $('#region').keypress(function( e ) {
            if(e.which === 32)
                return false;
        });

    </script>


@endpush
@section('content')
    <div>
        <div class="content">
            <div class="title m-b-md">
                <h1 style="text-align: center;">Dodavanje opštine i regiona</h1>
            </div>

            @if ($errors->any())
                <div class="alert alert-danger">
                    @foreach ($errors->all() as $error)
                        <div>- {{ $error }}</div>
                    @endforeach
                </div>
            @endif

            <div class="card">

                <div class="card-body">
                    <legend class="font-weight-semibold"><i class="icon-plus-circle2 mr-2"></i> Unes novog grada</legend>

                    {!! Form::open(['action' => 'PagesController@add_city', 'method' => 'POST']) !!}
                    <div class="row">


                        {{ Form::textGroup2('city', 'Ime grada:', null, null, 'form-control', ['placeholder' => 'Upišite ime grada', 'autocomplete' => 'off'], 'col-4', true) }}




                        <div class="col-4" style="margin-top: 28px; text-align: center;">
                            <button type="submit" class="btn bg-teal-300 ">Unesi novi grad<i class="icon-plus3 ml-1" ></i></button>
                        </div>
                        {!! Form::close() !!}

                        <div class="col-4" style="margin-top: 28px; text-align: center;">

                        </div>

                    </div>
                    <br><br>

                    <legend class="font-weight-semibold"><i class="icon-plus-circle2 mr-2"></i> Unes nove opštine</legend>

                    {!! Form::open(['action' => 'PagesController@add_township', 'method' => 'POST']) !!}
                    <div class="row">


                        {{ Form::textGroup2('township', 'Ime opštine:', null, null, 'form-control', ['placeholder' => 'Upišite ime opštine', 'autocomplete' => 'off'], 'col-4', true) }}




                        <div class="col-4" style="margin-top: 28px; text-align: center;">
                            <button type="submit" class="btn bg-teal-300 ">Unesi novu opštinu<i class="icon-plus3 ml-1" ></i></button>
                        </div>
                        {!! Form::close() !!}

                        <div class="col-4" style="margin-top: 28px; text-align: center;">

                        </div>

                    </div>
                    <br><br>
                    <legend class="font-weight-semibold"><i class="icon-plus-circle2 mr-2"></i> Unes novog regiona</legend>

                    {!! Form::open(['action' => 'PagesController@add_region', 'method' => 'POST']) !!}
                    <div class="row">

                        {{ Form::textGroup2('region', 'Ime regiona:', null, null, 'form-control', ['placeholder' => 'Upišite ime regiona', 'autocomplete' => 'off', 'id' => 'region'], 'col-4', true) }}




                        <div class="col-4" style="margin-top: 28px; text-align: center;">
                            <button type="submit" class="btn bg-teal-300 ">Unesi novi region<i class="icon-plus3 ml-1" ></i></button>
                        </div>
                        {!! Form::close() !!}

                        <div class="col-4" style="margin-top: 28px; text-align: center;">

                        </div>
                    </div>







                    <p class="text-right"></p>


                    {{-- AKTIVNOSTI NA NOVOJ OPREMI --}}
                </div>
            </div>


        </div>
    </div>

@endsection



