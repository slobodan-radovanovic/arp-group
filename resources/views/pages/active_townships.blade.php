@extends('layouts.app')

@section('content')

    @push('style')
        <style>
            .nav-pills-bordered .nav-link.active {
                border-color: #4db6ac !important;
            }

            .nav-pills .nav-link.active, .nav-pills .show > .nav-link {
                color: #fff;
                background-color: #4db6ac !important;
            }


        </style>

        <!-- Global stylesheets -->
        <link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
        <link href="../../../../global_assets/css/icons/icomoon/styles.css" rel="stylesheet" type="text/css">
        <link href="../assets/css/bootstrap.min.css" rel="stylesheet" type="text/css">
        <link href="../assets/css/bootstrap_limitless.min.css" rel="stylesheet" type="text/css">
        <link href="../assets/css/layout.min.css" rel="stylesheet" type="text/css">
        <link href="../assets/css/components.min.css" rel="stylesheet" type="text/css">
        <link href="../assets/css/colors.min.css" rel="stylesheet" type="text/css">
        <!-- /global stylesheets -->
    @endpush

    @push('scripts')
        <script src="../global_assets/js/plugins/tables/datatables/datatables.min.js"></script>
        <script src="../global_assets/js/demo_pages/datatables_basic.js"></script>
        <!-- Load select2 -->
        <script type="text/javascript" src="../global_assets/js/plugins/forms/selects/select2.min.js"></script>
        <script type="text/javascript" src="../global_assets/js/plugins/extensions/jquery_ui/interactions.min.js"></script>
        <script src="../assets/js/custom_select2.js"></script>
        <script src="../global_assets/js/plugins/forms/selects/select2.min.js"></script>
        {{--anytime--}}
        <script src="../global_assets/js/plugins/ui/moment/moment.min.js"></script>
        <script src="../global_assets/js/plugins/pickers/anytime.min.js"></script>
        <script src="../../../../global_assets/js/plugins/pickers/daterangepicker.js"></script>
        <script src="../assets/js/custom_datepicker.js"></script>

        <script src="../global_assets/js/main/jquery.min.js"></script>
        <script src="../global_assets/js/main/bootstrap.bundle.min.js"></script>
        <script src="../global_assets/js/plugins/loaders/blockui.min.js"></script>
        <script src="../global_assets/js/plugins/tables/datatables/datatables.min.js"></script>
        <script src="../global_assets/js/plugins/forms/selects/select2.min.js"></script>
        <script src="../global_assets/js/plugins/tables/datatables/extensions/jszip/jszip.min.js"></script>
        <script src="../global_assets/js/plugins/tables/datatables/extensions/buttons.min.js"></script>

        {{--export datatable--}}

        {{--anytime--}}
        <script src="../global_assets/js/plugins/ui/moment/moment.min.js"></script>
        <script src="../global_assets/js/plugins/pickers/anytime.min.js"></script>
        <script src="../../../../global_assets/js/plugins/pickers/daterangepicker.js"></script>
        <script src="../assets/js/custom_datepicker.js"></script>



        <script>

            var today = new Date();
            var dd = String(today.getDate()).padStart(2, '0');
            var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
            var yyyy = today.getFullYear();

            today = dd + '-' + mm + '-' + yyyy;
            console.log(today);


            $('.datatable-basic').DataTable({
                autoWidth: false,
                dom: '<"datatable-header"fBl><"datatable-scroll-wrap"t><"datatable-footer"ip>',
                language: {
                    search: '<span>Pretraga:</span> _INPUT_',
                    lengthMenu: '<span>Prikaz:</span> _MENU_',
                    info: '_START_ do _END_ od ukupno _TOTAL_',
                    paginate: {'first': 'First', 'last': 'Last', 'next': '→', 'previous': '←'}
                },
                buttons: {
                    buttons: [
                        {
                            extend: 'excelHtml5',
                            className: 'btn btn-light',
                            title: 'Vrste materijala '+today,
                            sheetName:'Vrste materijala',
                            exportOptions: {
                                columns: [ 0, 1, 2]
                            }
                        }
                    ]
                }
            });


            $('#modal_edit_city').on('show.bs.modal', function (event) {
                var button = $(event.relatedTarget);

                var select = button.data('alldata');
                var modal = $(this);
                console.log(select);
                modal.find('.modal-body #select_id_city').val(select.select_id);
                modal.find('.modal-body #city').text(select.select_value);



                if(select.flag == 1){
                    modal.find('#active_city').prop('checked',true);
                }else{
                    modal.find('#active_city').prop('checked',false);
                };


            });

            $('#modal_edit_township').on('show.bs.modal', function (event) {
                var button = $(event.relatedTarget);

                var select = button.data('alldata');
                var modal = $(this);
                console.log(select);
                modal.find('.modal-body #select_id_township').val(select.select_id);
                modal.find('.modal-body #township').text(select.select_value);



                if(select.flag == 1){
                    modal.find('#active_township').prop('checked',true);
                }else{
                    modal.find('#active_township').prop('checked',false);
                };


            });

            $('#modal_edit_region').on('show.bs.modal', function (event) {
                var button = $(event.relatedTarget);

                var select = button.data('alldata');
                var modal = $(this);
                console.log(select);
                modal.find('.modal-body #select_id_region').val(select.select_id);
                modal.find('.modal-body #region').text(select.select_value);



                if(select.flag == 1){
                    modal.find('#active_region').prop('checked',true);
                }else{
                    modal.find('#active_region').prop('checked',false);
                };


            });
        </script>
    @endpush

    <div>
        <div class="content">
            <div class="title m-b-md">
                <h2 style="text-align: center;">Gradovi, opštine i regioni</h2>
            </div>

            @if ($errors->any())
                <div class="alert alert-danger">
                    @foreach ($errors->all() as $error)
                        <div>- {{ $error }}</div>
                    @endforeach
                </div>
            @endif

            <div>

                <div class="title m-b-md">
                    <h3 style="text-align: center;">Pregled gradova</h3>
                </div>

                <!-- Edit modal -->
                <div id="modal_edit_city" class="modal fade" tabindex="-1">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title">Izmena grada</h5>
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                            </div>

                            <div class="modal-body">
                                {!! Form::open(['action' => ['PagesController@update_city', 'update'], 'method' => 'POST']) !!}
                                {{ csrf_field() }}
                                {{ method_field('PUT')}}
                                {{Form::hidden ('select_id_city', 'update', ['id' => 'select_id_city'])}}
                                <p>
                                    Ime grada : <span id ="city"></span></p>

                                <div class="row mt-2">
                                    <div class="col-2">{!! Form::label("active_city", "Aktivan:", ['class' => 'control-label']) !!}</div>
                                    <div class="col-10">{{ Form::checkbox("active_city", 1, null)}}</div>

                                </div>

                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-light" data-dismiss="modal">Odustani</button>
                                {!! Form::button('Sačuvaj', ['type' => 'submit', 'id' =>'button-edit-city', 'class' => 'btn bg-teal-300 button-submit', 'data-loading-text' => 'Sačuvaj']) !!}
                            </div>
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
                <!-- /edit modal -->



                <br>

                <!-- Basic datatable -->
                <div class="card">

                    <table class="table datatable-basic table-bordered table-striped">
                        <thead>
                        <tr>
                            <th>Ime grada</th>
                            <th>Aktivan</th>
                            <th{{-- class="text-center"--}}>Izmena</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($cities as $city)

                            {{--@dd($ware)--}}
                            <tr>

                                <td>{{$city->select_value}}</td>
                                <td>
                                    @if($city->flag)
                                        <i class="icon-checkmark2"></i>
                                        {{--@else--}}
                                    @else
                                        <i class="icon-cross3"></i>
                                    @endif

                                </td>
                                <td class="text-center">
                                    <a href="#" class="dropdown-item" data-toggle="modal" data-target="#modal_edit_city" data-alldata="{{$city}}"> <i class="icon-pencil7"></i> {{--Izmeni--}}</a>

                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
                <!-- /basic datatable -->

            </div>





            <div>

                <div class="title m-b-md">
                    <h3 style="text-align: center;">Pregled opština</h3>
                </div>

                <!-- Edit modal -->

                <div id="modal_edit_township" class="modal fade" tabindex="-1">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title">Izmena opštine</h5>
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                            </div>

                            <div class="modal-body">
                                {!! Form::open(['action' => ['PagesController@update_township', 'update'], 'method' => 'POST']) !!}
                                {{ csrf_field() }}
                                {{ method_field('PUT')}}
                                {{Form::hidden ('select_id_township', 'update', ['id' => 'select_id_township'])}}
                                <p>
                                    Ime opštine : <span id ="township"></span></p>

                                <div class="row mt-2">
                                    <div class="col-2">{!! Form::label("active_township", "Aktivan:", ['class' => 'control-label']) !!}</div>
                                    <div class="col-10">{{ Form::checkbox("active_township", 1, null)}}</div>

                                </div>

                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-light" data-dismiss="modal">Odustani</button>
                                {!! Form::button('Sačuvaj', ['type' => 'submit', 'id' =>'button-edit-township', 'class' => 'btn bg-teal-300 button-submit', 'data-loading-text' => 'Sačuvaj']) !!}
                            </div>
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
                <!-- /edit modal -->



                <br>

                <!-- Basic datatable -->
                <div class="card">

                    <table class="table datatable-basic table-bordered table-striped">
                        <thead>
                        <tr>
                            <th>Ime opštine</th>
                            <th>Aktivan</th>
                            <th>Izmena</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($townships as $township)


                            <tr>

                                <td>{{$township->select_value}}</td>
                                <td>
                                    @if($township->flag)
                                        <i class="icon-checkmark2"></i>
                                    @else
                                        <i class="icon-cross3"></i>
                                    @endif

                                </td>
                                <td class="text-center">
                                    <a href="#" class="dropdown-item" data-toggle="modal" data-target="#modal_edit_township" data-alldata="{{$township}}"> <i class="icon-pencil7"></i> </a>

                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
                <!-- /basic datatable -->

            </div>


            <div>

                <div class="title m-b-md">
                    <h3 style="text-align: center;">Pregled regiona</h3>
                </div>

                <!-- Edit modal -->

                <div id="modal_edit_region" class="modal fade" tabindex="-1">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title">Izmena regiona</h5>
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                            </div>

                            <div class="modal-body">
                                {!! Form::open(['action' => ['PagesController@update_region', 'update'], 'method' => 'POST']) !!}
                                {{ csrf_field() }}
                                {{ method_field('PUT')}}
                                {{Form::hidden ('select_id_region', 'update', ['id' => 'select_id_region'])}}
                                <p>
                                    Ime regiona : <span id ="region"></span></p>

                                <div class="row mt-2">
                                    <div class="col-2">{!! Form::label("active_region", "Aktivan:", ['class' => 'control-label']) !!}</div>
                                    <div class="col-10">{{ Form::checkbox("active_region", 1, null)}}</div>

                                </div>

                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-light" data-dismiss="modal">Odustani</button>
                                {!! Form::button('Sačuvaj', ['type' => 'submit', 'id' =>'button-edit-region', 'class' => 'btn bg-teal-300 button-submit', 'data-loading-text' => 'Sačuvaj']) !!}
                            </div>
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
                <!-- /edit modal -->



                <br>

                <!-- Basic datatable -->
                <div class="card">

                    <table class="table datatable-basic table-bordered table-striped">
                        <thead>
                        <tr>
                            <th>Ime opštine</th>
                            <th>Aktivan</th>
                            <th>Izmena</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($regions as $region)


                            <tr>

                                <td>{{$region->select_value}}</td>
                                <td>
                                    @if($region->flag)
                                        <i class="icon-checkmark2"></i>
                                    @else
                                        <i class="icon-cross3"></i>
                                    @endif

                                </td>
                                <td class="text-center">
                                    <a href="#" class="dropdown-item" data-toggle="modal" data-target="#modal_edit_region" data-alldata="{{$region}}"> <i class="icon-pencil7"></i> </a>

                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
                <!-- /basic datatable -->

            </div>

        </div>
    </div>

@endsection



