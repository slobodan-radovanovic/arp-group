@extends('layouts.app')

@section('content')

    @push('scripts')
        <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
                integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
                crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
                integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
                crossorigin="anonymous"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
                integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
                crossorigin="anonymous"></script>

        <script src="../global_assets/js/plugins/tables/datatables/datatables.min.js"></script>
        <script src="../global_assets/js/demo_pages/datatables_basic.js"></script>
        <script src="../global_assets/js/plugins/forms/selects/select2.min.js"></script>

        {{--anytime--}}
        <script src="../global_assets/js/plugins/ui/moment/moment.min.js"></script>
        <script src="../global_assets/js/plugins/pickers/anytime.min.js"></script>
        <script src="../../../../global_assets/js/plugins/pickers/daterangepicker.js"></script>

        <script src="../assets/js/custom_datepicker.js"></script>

        <!-- Load select2 -->
        <script type="text/javascript" src="../global_assets/js/plugins/forms/selects/select2.min.js"></script>
        <script type="text/javascript"
                src="../global_assets/js/plugins/extensions/jquery_ui/interactions.min.js"></script>
        <script src="../assets/js/custom_select2.js"></script>


        <script>
            $('.datatable-basic').DataTable({
                autoWidth: false,
                dom: '<"datatable-header"fl><"datatable-scroll"t><"datatable-footer"ip>',
                /*order: [4, "desc"],*/
                language: {
                    search: '<span>Pretraga:</span> _INPUT_',
                    lengthMenu: '<span>Prikaz:</span> _MENU_',
                    info: '_START_ do _END_ od ukupno _TOTAL_',
                    paginate: {'first': 'First', 'last': 'Last', 'next': '→', 'previous': '←'}
                }
            });

            $('.daterange-basic').daterangepicker({
                @if( empty($dismantled_date_filter))
                startDate: moment().subtract(1, 'month').startOf('month'),
                endDate: moment().subtract(1, 'month').endOf('month'),
                @endif
                maxDate: moment(),
                locale: {
                    format: 'DD/MM/YYYY'
                },
                applyClass: 'bg-teal-300',
                cancelClass: 'btn-light',

            });

        </script>


    @endpush

    @push('style')
        <style>
            .bold {
                font-weight: 700;
            }

            .large {
                font-size: large;
            }
        </style>
    @endpush

    <div>
        <div class="content">
            <div class="title m-b-md">

                <h1 style="text-align: center;">Detalji za nalog {{ $report->ordinal_number }}</h1>
            </div>

            <p>Redni broj naloga: {{ $report->ordinal_number }}<br>
                Broj tehnicara koji su radili nalog: {{ $count }}<br>
                Vrsta naloga: {{ $order_types[$report->order_type] }}<br>
                Izveštaj za tehničara: {{ $report->technician_name }}<br>
                Vreme zatvaranja naloga: {{ datetimeForView($report->created_at) }}<br>
                @foreach($report as $key => $value)
                    {{-- Kljuc: {{$key}}  Vredost: {{$value}}--}}
                    @if($value != null and $value !== '0')
                        {{--Kljuc: {{$key}}  Vredost: {{$value}}--}}
                        @if($key == 'service_name')
                            Tip posla: {{ $services[$report->service_name] }}<br>
                        @elseif($key == 'order_status' and $value == 'Nerealizovan')
                            Status naloga: Nerealizovan<br>
                        @elseif($key == 'price_apartment')
                            Vrsta objekta: Stan<br>
                            Cena za ovu vrstu objekta: {{ $report->price_apartment }} din.<br>
                        @elseif($key == 'price_house' )
                            Vrsta objekta: Kuća<br>
                            Cena za ovu vrstu objekta: {{ $report->price_house }} din.<br>
                        @elseif($key == 'price_first_equipment' )
                            Cena za prvu ugrađenu opremu: {{ $report->price_first_equipment }} din.<br>
                        @elseif($key == 'price_additional_equipment' )
                            Ukupna cena za ostalu ugrađenu opremu: {{ $report->price_additional_equipment }} din.<br>
                        @elseif($key == 'price_additional_equipment' )
                            Za ovaj tip posla cena je fiksna: {{ $report->price_fixed  }} din.<br>
                        @elseif($key == 'price_private_vehicle')
                            Tehnicar je koristio privatno vozilo: {{ $report->price_private_vehicle }} din.<br>
                        @endif
                    @endif
                @endforeach


                @if($report->order_type == 'reclamation' or $count == 1)
                    <br>
                    <span style="font-weight: bold">Ukupno za ovaj nalog: {{ $report->sum }} din.</span>

                @elseif($count == 2 and $report->price_private_vehicle === null)
                    <br>
                    <span style="font-weight: bold">Ukupno za ovaj nalog: {{ $report->sum * 2 }}/2 = {{ $report->sum }}
                        din.</span>
                @elseif($count == 2 and $report->price_private_vehicle !== null)
                    <br>
                    <span style="font-weight: bold">Ukupno za ovaj nalog: {{ ($report->sum - $report->price_private_vehicle) * 2 }}
                        /2 + {{ $report->price_private_vehicle }} = {{ $report->sum }} din.</span>
                @endif
            </p>
        </div>
    </div>
                @if($report->order_type == 'reclamation')
                    <p class="ml-3"> Komntar za nalog {{ $report->ordinal_number }}: <br> {{ $order_note['0']->note}}</p>
                    <br>
                    <button type="button" class="btn bg-teal-300 ml-3" data-toggle="modal" data-target="#modal_edit">Izmena
                        finansijskih detalja<i class="icon-pencil7 ml-1"></i></button>

                <!-- Edit modal -->
            <div id="modal_edit" class="modal fade" tabindex="-1">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title">Izmena finansijskih detalja za nalog {{ $report->ordinal_number }}</h5>
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                        </div>

                        <div class="modal-body">

                            {!! Form::open(['action' => ['PagesController@update', 'update_reports'], 'method' => 'POST']) !!}
                            {{ csrf_field() }}
                            {{ method_field('PUT')}}
                            {{--{{Form::hidden ('technician_id', 'update', ['id' => 'technician_id'])}}--}}
                        @foreach($order_reports as $order_report)
                            <hr>

            <p>Za tehničara "{{$order_report->technician_name}}" za nalog {{ $report->ordinal_number }} </p>
                            {{ Form::textGroup('edit_report_'.$order_report->technicians_report_id, 'Ukupno:', null, ['placeholder' => 'Nova cena'], $order_report->sum, 'col-12', 'col-3', 'col-9') }}

                        @endforeach

            <hr>
            <p>Finansijski izvestaj za nalog {{ $report->ordinal_number }}</p>
            {{ Form::textGroup('finance_report_'.$finance_report['0']->finance_report_id, 'Ukupno:', null, ['placeholder' => 'Nova cena'], $finance_report['0']->sum, 'col-12', 'col-3', 'col-9') }}

            <hr>
            <p>Komentar za nalog {{ $report->ordinal_number }}</p>
            <div class="form-group">


                                    <textarea rows="3" cols="5" id="order_comment"
                                              name="<?= 'order_note_'.$order_note['0']->order_id ?>"
                                              class="form-control"
                                              placeholder="Upisite dodatne komentare">{{$order_note['0']->note}}</textarea>
            </div>


                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-light" data-dismiss="modal">Odustani</button>
                            {!! Form::button('Sačuvaj', ['id' =>'button-create-technician', 'type' =>'submit', 'class' => 'btn bg-teal-300 button-submit', 'data-loading-text' => 'Sačuvaj novog korisnika']) !!}
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
            <!-- /edit modal -->

            @endif



@endsection



