@extends('layouts.app')

@section('content')

    @push('scripts')
        <script src="../global_assets/js/plugins/tables/datatables/datatables.min.js"></script>
        <script src="../global_assets/js/demo_pages/datatables_basic.js"></script>
        <!-- Load select2 -->
        <script type="text/javascript" src="../global_assets/js/plugins/forms/selects/select2.min.js"></script>
        <script type="text/javascript"
                src="../global_assets/js/plugins/extensions/jquery_ui/interactions.min.js"></script>
        <script src="../assets/js/custom_select2.js"></script>
        <script src="../global_assets/js/plugins/forms/selects/select2.min.js"></script>
        {{--anytime--}}
        <script src="../global_assets/js/plugins/ui/moment/moment.min.js"></script>
        <script src="../global_assets/js/plugins/pickers/anytime.min.js"></script>
        <script src="../../../../global_assets/js/plugins/pickers/daterangepicker.js"></script>
        <script src="../assets/js/custom_datepicker.js"></script>

        <script src="../global_assets/js/main/jquery.min.js"></script>
        <script src="../global_assets/js/main/bootstrap.bundle.min.js"></script>
        <script src="../global_assets/js/plugins/loaders/blockui.min.js"></script>
        <script src="../global_assets/js/plugins/tables/datatables/datatables.min.js"></script>
        <script src="../global_assets/js/plugins/forms/selects/select2.min.js"></script>
        <script src="../global_assets/js/plugins/tables/datatables/extensions/jszip/jszip.min.js"></script>
        <script src="../global_assets/js/plugins/tables/datatables/extensions/buttons.min.js"></script>

        {{--export datatable--}}

        {{--anytime--}}
        <script src="../global_assets/js/plugins/ui/moment/moment.min.js"></script>
        <script src="../global_assets/js/plugins/pickers/anytime.min.js"></script>
        <script src="../../../../global_assets/js/plugins/pickers/daterangepicker.js"></script>
        <script src="../assets/js/custom_datepicker.js"></script>


        <script>

                $('.datatable-basic').DataTable({
                autoWidth: false,
                dom: '<"datatable-header"fBl><"datatable-scroll-wrap"t><"datatable-footer"ip>',
                language: {
                    search: '<span>Pretraga:</span> _INPUT_',
                    lengthMenu: '<span>Prikaz:</span> _MENU_',
                    info: '_START_ do _END_ od ukupno _TOTAL_',
                    paginate: {'first': 'First', 'last': 'Last', 'next': '→', 'previous': '←'}
                },
                buttons: {
                    buttons: [
                        {
                            extend: 'excelHtml5',
                            footer: true,
                            className: 'btn btn-light',
                            title: 'Oprema vraćena u SBB magacin',
                            /*title: 'Zaduzena oprema za tehničara ' + technician + ' ' + date_period,*/
                            sheetName: 'Oprema vraćena u SBB magacin',
                            exportOptions: {
                                columns: [0, 1, 2, 3, 4, 5, 6]
                            }
                        }
                    ]
                }
            });

        </script>
    @endpush

    <div>
        <div class="content">
            <div class="title m-b-md">
                <h1 style="text-align: center;">Pregled opreme koja je vraćena u SBB magacin</h1>
            </div>

            <div class="card">
                <!-- Izbrisana oprema datatable -->

                <table class="table datatable-basic table-bordered table-striped">
                    <thead>
                    <tr>

                        <th>Tip opreme</th>
                        <th>Model</th>
                        <th>S/N 1</th>
                        <th>S/N 2</th>
                        <th>S/N 3</th>
                        <th>Izvršio</th>
                        <th>Vreme</th>

                    </tr>
                    </thead>
                    <tbody>
                    @foreach($sbb_warehouse as $equipment)

                        <tr>
                            <td> {{ $equipment->equipment_subtype_name }}</td>
                            <td> {{ $equipment->equipment_model_name }}</td>
                            <td>{{ $equipment->equipment_serial1 }}</td>
                            <td>{{ $equipment->equipment_serial2 }}</td>
                            <td>{{ $equipment->equipment_serial3 }}</td>
                            <td>{{ $equipment->equipment_technician }}</td>
                            <td>{{ datetimeForView($equipment->updated_at) }}</td>

                        </tr>
                    @endforeach

                    </tbody>
                </table>

                <!-- /izbrisana oprema datatable -->

            </div>
        </div>
    </div>

@endsection



