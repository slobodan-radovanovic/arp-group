@extends('layouts.app')

@section('content')

    @push('style')
        <!-- Global stylesheets -->
        <link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet"
              type="text/css">
        <link href="../../../../global_assets/css/icons/icomoon/styles.css" rel="stylesheet" type="text/css">
        <link href="../assets/css/bootstrap.min.css" rel="stylesheet" type="text/css">
        <link href="../assets/css/bootstrap_limitless.min.css" rel="stylesheet" type="text/css">
        <link href="../assets/css/layout.min.css" rel="stylesheet" type="text/css">
        <link href="../assets/css/components.min.css" rel="stylesheet" type="text/css">
        <link href="../assets/css/colors.min.css" rel="stylesheet" type="text/css">
        <!-- /global stylesheets -->
        <style>
            h1 + div {
                font-size: large;
            }
        </style>
    @endpush


    @push('scripts')
        <script src="../global_assets/js/plugins/tables/datatables/datatables.min.js"></script>
        <script src="../global_assets/js/demo_pages/datatables_basic.js"></script>
        <!-- Load select2 -->
        <script type="text/javascript" src="../global_assets/js/plugins/forms/selects/select2.min.js"></script>
        <script type="text/javascript"
                src="../global_assets/js/plugins/extensions/jquery_ui/interactions.min.js"></script>
        <script src="../assets/js/custom_select2.js"></script>
        <script src="../global_assets/js/plugins/forms/selects/select2.min.js"></script>
        {{--anytime--}}
        <script src="../global_assets/js/plugins/ui/moment/moment.min.js"></script>
        <script src="../global_assets/js/plugins/pickers/anytime.min.js"></script>
        <script src="../../../../global_assets/js/plugins/pickers/daterangepicker.js"></script>
        <script src="../assets/js/custom_datepicker.js"></script>

        <script src="../global_assets/js/main/jquery.min.js"></script>
        <script src="../global_assets/js/main/bootstrap.bundle.min.js"></script>
        <script src="../global_assets/js/plugins/loaders/blockui.min.js"></script>
        <script src="../global_assets/js/plugins/tables/datatables/datatables.min.js"></script>
        <script src="../global_assets/js/plugins/forms/selects/select2.min.js"></script>
        <script src="../global_assets/js/plugins/tables/datatables/extensions/jszip/jszip.min.js"></script>
        <script src="../global_assets/js/plugins/tables/datatables/extensions/buttons.min.js"></script>

        {{--export datatable--}}

        {{--anytime--}}
        <script src="../global_assets/js/plugins/ui/moment/moment.min.js"></script>
        <script src="../global_assets/js/plugins/pickers/anytime.min.js"></script>
        <script src="../../../../global_assets/js/plugins/pickers/daterangepicker.js"></script>
        <script src="../assets/js/custom_datepicker.js"></script>



        <script>

            var today = new Date();
            var date = today.getFullYear() + '-' + (today.getMonth() + 1) + '-' + today.getDate();
            var time = today.getHours() + ":" + today.getMinutes();
            var dateTime = date + ' ' + time;

        @if($technician != null)

            var technician = '{!! $technicians[$technician] !!}';
            var date_period = '{!! $date_period !!}';
            console.log(technician);
            console.log(date_period);

            $('.datatable-basic').DataTable({
                autoWidth: false,
                dom: '<"datatable-header"fBl><"datatable-scroll-wrap"t><"datatable-footer"ip>',
                language: {
                    search: '<span>Pretraga:</span> _INPUT_',
                    lengthMenu: '<span>Prikaz:</span> _MENU_',
                    info: '_START_ do _END_ od ukupno _TOTAL_',
                    paginate: {'first': 'First', 'last': 'Last', 'next': '→', 'previous': '←'}
                },
                buttons: {
                    buttons: [
                        {
                            extend: 'excelHtml5',
                            footer: true,
                            className: 'btn btn-light',
                            title: 'Zaduzena oprema za tehničara ' + technician + ' ' + date_period,
                            sheetName: 'Zaduzena oprema',
                            exportOptions: {
                                columns: [0, 1, 2, 3, 4, 5, 6, 7]
                            }
                        },
                        {
                            extend: 'print',
                            text: '<i class="icon-printer mr-2"></i> Print table',
                            className: 'btn btn-light',
                            messageTop: 'Vreme: ' + dateTime + '  /  ' + 'Opremu zadužio tehničar: _________________'
                        }
                    ]
                }
            });

        @elseif($date_period != null)


            var date_period = '{!! $date_period !!}';

            console.log('date_period');

            $('.datatable-basic').DataTable({
                autoWidth: false,
                dom: '<"datatable-header"fBl><"datatable-scroll-wrap"t><"datatable-footer"ip>',
                language: {
                    search: '<span>Pretraga:</span> _INPUT_',
                    lengthMenu: '<span>Prikaz:</span> _MENU_',
                    info: '_START_ do _END_ od ukupno _TOTAL_',
                    paginate: {'first': 'First', 'last': 'Last', 'next': '→', 'previous': '←'}
                },
                buttons: {
                    buttons: [
                        {
                            extend: 'excelHtml5',
                            footer: true,
                            className: 'btn btn-light',
                            title: 'Zaduzena oprema za period ' + date_period,
                            sheetName: 'Zaduzena oprema',
                            exportOptions: {
                                columns: [0, 1, 2, 3, 4, 5, 6, 7]
                            }
                        },
                        {
                            extend: 'print',
                            text: '<i class="icon-printer mr-2"></i> Print table',
                            className: 'btn btn-light',
                            messageTop: 'Vreme: ' + dateTime + '  /  ' + 'Opremu zadužio tehničar: _________________'
                        }
                    ]
                }
            });

            @endif





            $('.daterange-basic').daterangepicker({
                @if( empty($dismantled_date_filter))
                startDate: moment().subtract(1, 'month').startOf('month'),
                endDate: moment().subtract(1, 'month').endOf('month'),
                @endif
                maxDate: moment(),
                locale: {
                    format: 'DD/MM/YYYY'
                },
                applyClass: 'bg-teal-300',
                cancelClass: 'btn-light'

            });

        </script>


    @endpush

    @push('style')
        <style>
            .bold {
                font-weight: 700;
            }

            .large {
                font-size: large;
            }
        </style>
    @endpush

    <div>
        <div class="content">
            <div class="title m-b-md">
                <h1 style="text-align: center;">Pregled zadužene opreme</h1>
            </div>

            @if ($errors->any())
                <div class="alert alert-danger">
                    @foreach ($errors->all() as $error)
                        <div>- {{ $error }}</div>
                    @endforeach
                </div>
            @endif

            <div>

                <br>

                <!-- Basic datatable -->
                <div class="card">
                    {!! Form::open(['action' => 'EquipmentController@assigned_post', 'method' => 'POST']) !!}
                    <div class="card-header header-elements-inline">
                        {{-- {{dd($dismantled_date_filter)}}--}}

                        <div class="row mb-3 col-12">

                            <div class="col-4 form-row mb-2">
                                <label>Filtriranje za period: </label>
                                <div class="input-group daterange">
                                    <span class="input-group-text"><i class="icon-calendar3"></i></span>
                                    <input name="dismantled_date_filter" style="width:200px" type="text"
                                           class="form-control daterange-basic" data-placeholder="Izaberite period"
                                           @if( ! empty($dismantled_date_filter))
                                           value="{{$dismantled_date_filter}}"
                                            @endif
                                    >
                                </div>
                            </div>

                            {{ Form::selectGroupSearch('technician',
                                            true,
                                            'Tehničar:',
                                            null,
                                            $technicians, $technician,
                                            ['data-placeholder' => 'Izaberite tehničara',
                                             'class'=> 'form-control select-search technician',
                                             'data-focus'], 'col-4 form-row mb-2') }}

                            <div class="col-4 form-row mb-2">
                                <button type="submit" class="btn bg-teal-300 ml-3 mt-3" style="height: 70%">Primeni
                                    filter
                                </button>
                            </div>
                        </div>

                        {!! Form::close() !!}


                    </div>

                    @if( ! empty($dismantled_date_filter))
                        <h2 style="text-align: center;">Zadužena oprema </h2>

                        <table class="table datatable-basic table-bordered table-striped">
                            <thead>
                            <tr>
                                <th>Kôd</th>
                                <th>Tip opreme</th>
                                <th>Model</th>
                                <th>S/N 1</th>
                                <th>S/N 2</th>
                                <th>S/N 3</th>
                                <th>Zadužen tehničar</th>
                                <th>Vreme zaduživanja</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($equipments as $equipment)
                                {{--@dd('technician_'.$equipment->equipment_technician)--}}
                                <tr>
                                    <td>{{ $equipment->equipment_code }}</td>
                                    <td> {{ $data['subtype'][$equipment->equipment_subtype_id] }}</td>
                                    @if($equipment->equipment_model_id != null)
                                        <td> {{ $data['model'][$equipment->equipment_model_id] }}</td>
                                    @else
                                        <td></td>
                                    @endif
                                    <td>{{ $equipment->equipment_serial1 }}</td>
                                    <td>{{ $equipment->equipment_serial2 }}</td>
                                    <td>{{ $equipment->equipment_serial3 }}</td>
                                    <td> {{ $technicians['technician_'.$equipment->equipment_technician] }}</td>
                                    <td>{{ datetimeForView($equipment->equipment_assign_date) }}</td>

                                </tr>
                            @endforeach

                            </tbody>
                            {{--<tfoot>
                            <tr>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th colspan="3">Opremu zaduzio: ___________</th>
                            </tr>
                            </tfoot>--}}
                        </table>
                        {{--<div class="bold large p-3">Ukupno za izabrani period: {{ $sum }} din.</div>--}}


                </div>
                <!-- /basic datatable -->
                @endif
            </div>
        </div>
    </div>

@endsection



