@extends('layouts.app')
@push('scripts')


    <script src="../global_assets/js/plugins/ui/moment/moment.min.js"></script>
    <script src="../global_assets/js/plugins/pickers/anytime.min.js"></script>



    <script src="../assets/js/custom_datepicker.js"></script>


    <!-- Load select2 -->
    <script type="text/javascript" src="../global_assets/js/plugins/forms/selects/select2.min.js"></script>
    <script type="text/javascript" src="../global_assets/js/plugins/extensions/jquery_ui/interactions.min.js"></script>
    <script src="../assets/js/custom_select2.js"></script>

    <script src="../global_assets/js/plugins/tables/datatables/datatables.min.js"></script>
    <script src="../global_assets/js/demo_pages/datatables_basic.js"></script>
    <script src="../global_assets/js/plugins/forms/selects/select2.min.js"></script>

    <script>
        $('.datatable-basic').DataTable({
            autoWidth: false,
            dom: '<"datatable-header"fl><"datatable-scroll"t><"datatable-footer"ip>',
            bLengthChange : false, //thought this line could hide the LengthMenu
            bInfo: false,
            bPaginate: false,
            order: [],
            bSort : false,
            language: {
                search: '<span>Pretraga:</span> _INPUT_',
            }
        });

        var json_equipment_select = {!! $data['equipment_for_select'] !!};
        var json_all_serials = {!! json_encode($data['all_serials']) !!};
        var json_model_for_select = {!! json_encode($data['model_for_select']) !!};

        console.log(json_all_serials);

        function filterEquipment(key, value) {
            return json_equipment_select.filter(function (equipment) {
                return equipment[key] == value;
            });
        } // filtriranje opreme na osnovu tipa (subtype)

        function filterModelAndSerial() {
            var selected_subtype = $('#equipment_subtype_id option:selected').val();

            var filtered_equipment = filterEquipment('equipment_subtype_id', selected_subtype);
            console.log(filtered_equipment);
            var filtered_equipment_ids = {};
            $.each(filtered_equipment, function (index) {
                filtered_equipment_ids[index] = this['equipment_id'];
            });
            var serial_html = '<option value="" selected="selected"></option>';
            $.each(json_all_serials, function (serial_key, serial_value) {
                $.each(filtered_equipment_ids, function (key, equipment_id) {
                    var split_key = serial_key.split("_");
                    if (split_key['1'] == equipment_id) {
                        serial_html += '<option value="' + serial_key + '">' + serial_value + '</option>';
                    }
                });
            });

            var model_html = '<option value="" selected="selected"></option>';
            $.each(filtered_equipment, function (equipment_key, equipment_value) {
                        model_html += '<option value="' + equipment_value.equipment_model_id + '">' + equipment_value.equipment_model_name + '</option>';
                });

            $('#equipment_model_id').html(model_html);
            $('#equipment_serial').html(serial_html);
        }

        function setSubtypeAndFilterSerial() {
            var selected_model = $('#equipment_model_id option:selected').val();
            console.log(selected_model);
            var filtered_equipment = filterEquipment('equipment_model_id', selected_model);
            console.log(filtered_equipment);
            var filtered_equipment_ids = {};
            $.each(filtered_equipment, function (index) {
                filtered_equipment_ids[index] = this['equipment_id'];
            });
            var serial_html = '<option value="" selected="selected"></option>';
            $.each(json_all_serials, function (serial_key, serial_value) {
                $.each(filtered_equipment_ids, function (key, equipment_id) {
                    var split_key = serial_key.split("_");
                    if (split_key['1'] == equipment_id) {
                        serial_html += '<option value="' + serial_key + '">' + serial_value + '</option>';
                    }
                });
            });

            $('#equipment_serial').html(serial_html);


            var equipment_subtype = filtered_equipment["0"].equipment_subtype_id;
            console.log(equipment_subtype);
            //equipment_subtype_1 select
            $('#equipment_subtype_id option:selected').removeAttr("selected");
            $('#equipment_subtype_id option[value="' + equipment_subtype + '"]').attr('selected', 'selected');
            var selected_subtype_text = $('#equipment_subtype_id option:selected').text();
            $('#select2-equipment_subtype_id-container').prop('title', selected_subtype_text);
            $('#select2-equipment_subtype_id-container').prop('innerText', selected_subtype_text);

        }

        function setSubtypeAndSetModel() {
            var equipment_serial = $('#equipment_serial option:selected').val();
            var slit_equipment_serial = equipment_serial.split("_");
            var equipment_id = slit_equipment_serial['1'];
            var filtered_equipment = filterEquipment('equipment_id', equipment_id);
            console.log(filtered_equipment);
            var equipment_subtype = filtered_equipment["0"].equipment_subtype_id;
            var equipment_model = filtered_equipment["0"].equipment_model_id;
            console.log(equipment_subtype);
            console.log(equipment_model);
            //equipment_subtype_id set
            $('#equipment_subtype_id option:selected').removeAttr("selected");
            $('#equipment_subtype_id option[value="' + equipment_subtype + '"]').attr('selected', 'selected');
            var selected_subtype_text = $('#equipment_subtype_id option:selected').text();
            $('#select2-equipment_subtype_id-container').prop('title', selected_subtype_text);
            $('#select2-equipment_subtype_id-container').prop('innerText', selected_subtype_text);

            //equipment_model_id set
            $('#equipment_model_id option:selected').removeAttr("selected");
            $('#equipment_model_id option[value="' + equipment_model + '"]').attr('selected', 'selected');
            var selected_model_text = $('#equipment_model_id option:selected').text();
            $('#select2-equipment_model_id-container').prop('title', selected_model_text);
            $('#select2-equipment_model_id-container').prop('innerText', selected_model_text);
        } // selektovanje tipa na osnovu serijskog


        $('#equipment_subtype_id').change(function () {
            filterModelAndSerial();
        });

        $('#equipment_model_id').change(function () {
            setSubtypeAndFilterSerial();
        });

        $('#equipment_serial').change(function () {
            setSubtypeAndSetModel();
        });

        $('#equipment_serial').select2('open');





    </script>


@endpush
@section('content')
    <div>
        <div class="content">
            <div class="title m-b-md">
                <h1 style="text-align: center;">Zaduzivanje opreme</h1>
            </div>

            @if ($errors->any())
                <div class="alert alert-danger">
                    @foreach ($errors->all() as $error)
                        <div>- {{ $error }}</div>
                    @endforeach
                </div>
            @endif

            <div class="card">

                {{--<div class="card-header header-elements-inline">
                    <h4 class="card-title">Unos opreme</h4>
                    <div class="header-elements">
                        <div class="list-icons">
                            <a class="list-icons-item" data-action="collapse"></a>
                            --}}{{-- <a class="list-icons-item" data-action="reload"></a>--}}{{--
                            --}}{{--  NE FUNKCIONISE RELOAD DUGME --}}{{--
                        </div>
                    </div>
                </div>--}}

                <div class="card-body">
                    {{-- AKTIVNOSTI NA NOVOJ OPREMI --}}
                    {{-- U redu tri SELECTA sa pet redova --}}

                    {!! Form::open(['action' => ['EquipmentController@update', 'assign'], 'method' => 'POST']) !!}
                        {{ csrf_field() }}
                        {{ method_field('PUT')}}


                    <div class="row">
{{--@php
    dd($equipment['technicians']);
@endphp--}}


                        {{ Form::selectGroupSearch('equipment_subtype_id',
                                    true,
                                    'Tip opreme:',
                                    null,
                                    $data['subtype_for_select'], null,
                                    ['data-placeholder' => 'Izaberite tip opreme',
                                     'class'=> 'form-control select-search equipment_subtype',
                                     'data-fouc'], 'col-3 form-row mb-2 mr-1', true) }}


                        {{ Form::selectGroupSearch('equipment_model_id',
                                                    true,
                                                    'Model:',
                                                    null,
                                                    $data['model_for_select'], null,
                                                    ['data-placeholder' => 'Izaberite model',
                                                     'class'=> 'form-control select-search equipment_model',
                                                     'data-fouc'], 'col-3 form-row mb-2', true) }}
                        {{ Form::selectGroupSearch('equipment_serial',
                                                    true,
                                                    'Serijski broj opreme:',
                                                    null,
                                                    $data['all_serials'], null,
                                                    ['data-placeholder' => 'Izaberite serijski broj',
                                                     'class'=> 'form-control select-search equipment_model',
                                                     'data-fouc'], 'col-3 form-row mb-2', true) }}

                        @if (session('equipment_technician'))
                            {{ Form::selectGroupSearch('equipment_technician',
                                                    true,
                                                    'Zaduzije se:',
                                                    null,
                                                    $data['active_technicians'], session('equipment_technician'),
                                                    ['data-placeholder' => 'Izaberite tehničara',
                                                     'class'=> 'form-control select-search equipment_model',
                                                     'data-fouc'], 'col-3 form-row mb-2', true) }}
                        @else

                            {{ Form::selectGroupSearch('equipment_technician',
                                                    true,
                                                    'Zaduzije se:',
                                                    null,
                                                    $data['active_technicians'], null,
                                                    ['data-placeholder' => 'Izaberite tehničara',
                                                     'class'=> 'form-control select-search equipment_model',
                                                     'data-fouc'], 'col-3 form-row mb-2', true) }}
                         @endif

                    </div>

                    <p class="text-right"><button type="submit" class="btn bg-teal-300 " data-toggle="modal" data-target="#modal_add">Zaduzi <i class="icon-plus3 ml-1" ></i></button></p>
                    {{-- AKTIVNOSTI NA NOVOJ OPREMI --}}
                    {!! Form::close() !!}


                    <br>
                    <!-- Prikaz poslednje zaduzene opreme -->

                    <legend></legend>
                    <h3 class="text-center m-0 p-0 ">Poslednje zaduzena oprema</h3>
                    <table class="table datatable-basic table-bordered table-striped">
                        <thead>
                        <tr>
                            <th>Kôd</th>
                            <th>Tip opreme</th>
                            <th>Model</th>
                            <th>S/N 1</th>
                            <th>S/N 2</th>
                            <th>S/N 3</th>
                            <th>Zaduzen tehničar</th>
                            <th>Vreme zaduživanja</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($data['equipment'] as $equipment)
                        {{--@dd('technician_'.$equipment->equipment_technician)--}}
                            <tr>
                                <td>{{ $equipment->equipment_code }}</td>
                                <td> {{ $data['subtype'][$equipment->equipment_subtype_id] }}</td>
                                @if($equipment->equipment_model_id != null)
                                    <td> {{ $data['model'][$equipment->equipment_model_id] }}</td>
                                @else
                                    <td></td>
                                @endif
                                <td>{{ $equipment->equipment_serial1 }}</td>
                                <td>{{ $equipment->equipment_serial2 }}</td>
                                <td>{{ $equipment->equipment_serial3 }}</td>
                                <td> {{ $data['technicians']['technician_'.$equipment->equipment_technician] }}</td>
                                <td>{{ datetimeForView($equipment->updated_at) }}</td>

                            </tr>
                        @endforeach

                        </tbody>
                    </table>

                    <!-- /Prikaz poslednje zaduzene opreme -->
                </div>
            </div>


        </div>
    </div>

@endsection



