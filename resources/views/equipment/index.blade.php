@extends('layouts.app')

@section('content')
    @push ('style')
        <!-- Global stylesheets -->
        <link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
        <link href="../../../../global_assets/css/icons/icomoon/styles.css" rel="stylesheet" type="text/css">
        <link href="../assets/css/bootstrap.min.css" rel="stylesheet" type="text/css">
        <link href="../assets/css/bootstrap_limitless.min.css" rel="stylesheet" type="text/css">
        <link href="../assets/css/layout.min.css" rel="stylesheet" type="text/css">
        <link href="../assets/css/components.min.css" rel="stylesheet" type="text/css">
        <link href="../assets/css/colors.min.css" rel="stylesheet" type="text/css">
        <!-- /global stylesheets -->

        <!-- Busy Load CSS -->
        <link href="https://cdn.jsdelivr.net/npm/busy-load/dist/app.min.css" rel="stylesheet">

    @endpush

    @push('scripts')

<script src="../global_assets/js/plugins/ui/moment/moment.min.js"></script>
<script src="../global_assets/js/plugins/pickers/anytime.min.js"></script>

<script src="../assets/js/custom_datepicker.js"></script>
        <!-- Load select2 -->
        <script type="text/javascript" src="../global_assets/js/plugins/forms/selects/select2.min.js"></script>
        <script type="text/javascript" src="../global_assets/js/plugins/extensions/jquery_ui/interactions.min.js"></script>
        <script src="../assets/js/custom_select2.js"></script>



        <script src="../global_assets/js/plugins/tables/datatables/datatables.min.js"></script>
        <script src="../global_assets/js/demo_pages/datatables_basic.js"></script>
        {{--<script src="../global_assets/js/plugins/forms/selects/select2.min.js"></script>--}}
        <script src="../global_assets/js/plugins/loaders/blockui.min.js"></script>



        <script src="../global_assets/js/plugins/tables/datatables/datatables.min.js"></script>
        {{--<script src="../global_assets/js/plugins/forms/selects/select2.min.js"></script>--}}
        <script src="../global_assets/js/plugins/tables/datatables/extensions/jszip/jszip.min.js"></script>
        <script src="../global_assets/js/plugins/tables/datatables/extensions/buttons.min.js"></script>
        {{--  <script src="../assets/js/app.js"></script>
          <script src="../global_assets/js/demo_pages/datatables_extension_buttons_html5.js"></script>--}}


        {{--export--}}

        {{--<script src="../global_assets/js/plugins/forms/selects/select2.min.js"></script>
         <script src="../global_assets/js/plugins/tables/datatables/extensions/jszip/jszip.min.js"></script>
         <script src="../global_assets/js/plugins/tables/datatables/extensions/pdfmake/pdfmake.min.js"></script>
         <script src="../global_assets/js/plugins/tables/datatables/extensions/pdfmake/vfs_fonts.min.js"></script>
         <script src="../global_assets/js/plugins/tables/datatables/extensions/buttons.min.js"></script>--}}
        {{-- <script src="../global_assets/js/demo_pages/datatables_extension_buttons_html5.js"></script>--}}

        <!-- Theme JS files -->
        {{--<script src="../../../../global_assets/js/plugins/tables/datatables/datatables.min.js"></script>
        <script src="../../../../global_assets/js/plugins/forms/selects/select2.min.js"></script>
        <script src="../../../../global_assets/js/plugins/tables/datatables/extensions/jszip/jszip.min.js"></script>
        <script src="../../../../global_assets/js/plugins/tables/datatables/extensions/pdfmake/pdfmake.min.js"></script>
        <script src="../../../../global_assets/js/plugins/tables/datatables/extensions/pdfmake/vfs_fonts.min.js"></script>
        <script src="../../../../global_assets/js/plugins/tables/datatables/extensions/buttons.min.js"></script>--}}

        {{--<script src="assets/js/app.js"></script>--}}
        {{--<script src="../../../../global_assets/js/demo_pages/datatables_extension_buttons_html5.js"></script>--}}
        <!-- /theme JS files -->


        <!-- Busy Load JS -->
        {{--<script src="https://cdn.jsdelivr.net/npm/busy-load/dist/app.min.js"></script>
--}}

        <script>

            /*$('.datatable-basic').DataTable( {
                dom: 'Bfrtip',
                buttons: [
                    'excelHtml5'
                ]
            } );*/
            /*var active_tab = $('.nav-link.active')['1'].innerText;


            function set_actitve_tab(){
                $( document ).ready(function() {
                    active_tab = $('.nav-link.active')['1'].innerText;
                    console.log(active_tab);
                    console.log( "ready!" );
                });

            }*/

            var today = new Date();
            var dd = String(today.getDate()).padStart(2, '0');
            var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
            var yyyy = today.getFullYear();

            today = dd + '-' + mm + '-' + yyyy;
            console.log(today);

            /*$.ajax({
                type: 'GET', //THIS NEEDS TO BE GET
                url: "{{--{{ route('equipment.index') }}--}}",
                dataType: 'json',
                success: function (data) {
                    console.log(data);
                },error:function(data){
                    console.log(data);
                }
            });*/
            /* var equipment1 = {{--{!! $data['equipment1'] !!}--}};

            console.log(equipment1);*/

            var table =  $('.datatable-basic1').DataTable({
                autoWidth: false,
                processing: true,
                /* serverSide: true,*/
                dom: '<"datatable-header"fBl><"datatable-scroll-wrap"t><"datatable-footer"ip>',

                ajax: {
                    url:"{{ route('neupotrebljena_oprema') }}"
                },
                columns:[
                    {
                        data: 'equipment_code',
                        name: 'equipment_code'
                    },
                    {
                        data: 'equipment_subtype_name',
                        name: 'equipment_subtype_name'
                    },
                    {
                        data: 'equipment_model_name',
                        name: 'equipment_model_name'
                    },
                    {
                        data: 'equipment_serial1',
                        name: 'equipment_serial1'
                    },
                    {
                        data: 'equipment_serial2',
                        name: 'equipment_serial2'
                    },
                    {
                        data: 'equipment_serial3',
                        name: 'equipment_serial3'
                    },
                    {
                        data: 'equipment_status',
                        name: 'equipment_status'
                    },
                    {
                        data: 'technician_name',
                        name: 'technician_name'
                    },
                    {
                        data: 'equipment_dispatch_note',
                        name: 'equipment_dispatch_note'
                    },
                    {
                        data: 'equipment_added_by',
                        name: 'equipment_added_by'
                    },
                    {
                        data: 'created_at',
                        name: 'created_at'
                    },
                    {
                        data: 'action',
                        name: 'action',
                        orderable: false
                    }
                ],
                columnDefs: [
                    {
                        targets: [ 0, 1, 2, 3, 4, 5, 6, 7, 8, 9,10 ],
                        searchable: true },
                    {
                        targets: -1,
                        data: null,
                        defaultContent: '<div class="list-icons"><div class="dropdown">' +
                        '<a href="#" class="list-icons-item" data-toggle="dropdown">' +
                        '<i class="icon-menu9"></i></a>' +
                        '<div class="dropdown-menu dropdown-menu-left">' +
                        '<button class="dropdown-item" data-toggle="modal" data-target="#modal_edit"><i class="icon-pencil7"></i>Izmeni</button>' +
                        '<button class="dropdown-item" data-toggle="modal" data-target="#modal_sbb_warehouse"><i class="icon-cancel-square"></i>Vraćena SSB-u</button>' +
                        '<button class="dropdown-item" data-toggle="modal" data-target="#modal_delete"><i class="icon-cancel-square"></i>Izbriši</button></div></div></div>'
                    } ],
                language: {
                    search: '<span>Pretraga:</span> _INPUT_',
                    lengthMenu: '<span>Prikaz:</span> _MENU_',
                    info: '_START_ do _END_ od ukupno _TOTAL_',
                    paginate: {'first': 'First', 'last': 'Last', 'next': '→', 'previous': '←'}
                },
                lengthMenu: [
                    [ 10, 25, 50, 100, -1 ],
                    [ '10', '25', '50', '100', 'Show all' ]
                ],
                buttons: {
                    buttons: [
                        {
                            extend: 'excelHtml5',
                            className: 'btn btn-light',
                            title: 'Oprema (Neupotrebljena oprema) '+today,
                            sheetName:'Neupotrebljena oprema',
                            exportOptions: {
                                columns: [ 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 ],
                            }
                        }
                    ]
                }
            });


            $('.datatable-basic1 tbody').on( 'click', 'button', function () {

                data = table.row( $(this).parents('tr') ).data();
                console.log(table.row);
                /*alert( data['equipment_id'] );*/
            } );

            $('.datatable-basic2').DataTable({
                autoWidth: false,
                processing: true,
                dom: '<"datatable-header"fBl><"datatable-scroll-wrap"t><"datatable-footer"ip>',
                ajax: {
                    url:"{{ route('oprema_u_magacinu') }}"
                },
                columns:[
                    {
                        data: 'equipment_code',
                        name: 'equipment_code'
                    },
                    {
                        data: 'equipment_subtype_name',
                        name: 'equipment_subtype_name'
                    },
                    {
                        data: 'equipment_model_name',
                        name: 'equipment_model_name'
                    },
                    {
                        data: 'equipment_serial1',
                        name: 'equipment_serial1'
                    },
                    {
                        data: 'equipment_serial2',
                        name: 'equipment_serial2'
                    },
                    {
                        data: 'equipment_serial3',
                        name: 'equipment_serial3'
                    }
                ],
                columnDefs: [
                    {
                        targets: [ 0, 1, 2, 3, 4, 5 ],
                        searchable: true
                    }
                ],
                lengthMenu: [
                    [ 10, 25, 50, 100, -1 ],
                    [ '10', '25', '50', '100', 'Show all' ]
                ],
                language: {
                    search: '<span>Pretraga:</span> _INPUT_',
                    lengthMenu: '<span>Prikaz:</span> _MENU_',
                    info: '_START_ do _END_ od ukupno _TOTAL_',
                    paginate: {'first': 'First', 'last': 'Last', 'next': '→', 'previous': '←'}
                },
                buttons: {
                    buttons: [
                        {
                            extend: 'excelHtml5',
                            className: 'btn btn-light',
                            title: 'Oprema (Oprema u magacinu) '+today,
                            sheetName:'Oprema u magacinu',
                            exportOptions: {
                                columns: [ 0, 1, 2, 3, 4, 5 ]
                            }
                        }
                    ]
                }
            });


            $('.datatable-basic3').DataTable({
                autoWidth: false,
                processing: true,
                dom: '<"datatable-header"fBl><"datatable-scroll-wrap"t><"datatable-footer"ip>',
                ajax: {
                    url:"{{ route('oprema_kod_tehnicara') }}"
                },
                columns:[
                    {
                        data: 'equipment_code',
                        name: 'equipment_code'
                    },
                    {
                        data: 'equipment_subtype_name',
                        name: 'equipment_subtype_name'
                    },
                    {
                        data: 'equipment_model_name',
                        name: 'equipment_model_name'
                    },
                    {
                        data: 'equipment_serial1',
                        name: 'equipment_serial1'
                    },
                    {
                        data: 'equipment_serial2',
                        name: 'equipment_serial2'
                    },
                    {
                        data: 'equipment_serial3',
                        name: 'equipment_serial3'
                    },
                    {
                        data: 'technician_name',
                        name: 'technician_name'
                    }
                ],
                columnDefs: [
                    {
                        targets: [ 0, 1, 2, 3, 4, 5, 6 ],
                        searchable: true
                    }
                ],
                language: {
                    search: '<span>Pretraga:</span> _INPUT_',
                    lengthMenu: '<span>Prikaz:</span> _MENU_',
                    info: '_START_ do _END_ od ukupno _TOTAL_',
                    paginate: {'first': 'First', 'last': 'Last', 'next': '→', 'previous': '←'}
                },
                buttons: {
                    buttons: [
                        {
                            extend: 'excelHtml5',
                            className: 'btn btn-light',
                            title: 'Oprema (Oprema kod tehničara) '+today,
                            sheetName:'Oprema kod tehničara',
                            exportOptions: {
                                columns: [ 0, 1, 2, 3, 4, 5, 6 ]
                            }
                        }
                    ]
                }
            });


            var json_equipment_select = {!! $data['equipment_for_select'] !!};

            console.log(json_equipment_select);

            function filterEquipment(key, value) {

                return json_equipment_select.filter(function (equipment) {
                    return equipment[key] == value;
                });
            } // filtriranje opreme na osnovu vrste, tipa ili modela


            $('#modal_edit').on('show.bs.modal', function (event) {

                var button = $(event.relatedTarget);

                var equipment = data;

                var created_at = equipment.created_at;
                var exploded_created_at = created_at.split(" ");
                //.log(exploded_created_at);
                var exploded_date = exploded_created_at[0].split("-");
               // console.log(exploded_date);
                var exploded_time = exploded_created_at[1].split(":");
               // console.log(exploded_time);
                var datetimeForView = exploded_date[2] + '/' + exploded_date[1] + '/' + exploded_date[0] + '  ' + exploded_time[0] + ':' + exploded_time[1];
               // console.log(datetimeForView);

                /*button.data('alldata');*/

                var modal = $(this);

                modal.find('.modal-body #update_equipment_id').val(equipment.equipment_id);
                modal.find('.modal-body #equipment_code').val(equipment.equipment_code);


                //dodato 8.2.2021
                var filtered_equipment = filterEquipment('equipment_type_id', equipment.equipment_type_id); // filtriranje na osnovu vrste (type)
                var filtered_equipment_subtype = {};
                $.each(filtered_equipment, function () {
                    filtered_equipment_subtype[this['equipment_subtype_id']] = this['equipment_subtype_name'];
                }); //lista tipova opreme koje pripadaju toj vrsti (subtype)
                console.log(filtered_equipment_subtype);
                var subtype_html = '<option value="" selected="selected"> ';
                $.each(filtered_equipment_subtype, function (key, value) {

                    subtype_html += '<option value="' + key + '">' + value + ' ';
                }); //kreiranje option tagova

                $('#equipment_subtype_id').html(subtype_html); // ubacivanje filtriranih option tagova


                //ista procedura za model
                var filtered_equipment_subtype = filterEquipment('equipment_subtype_id', equipment.equipment_subtype_id);
                var filtered_equipment_model = {};
                $.each(filtered_equipment_subtype, function () {
                    filtered_equipment_model[this['equipment_model_id']] = this['equipment_model_name'];
                });
                var model_html = '<option value="" selected="selected"> ';
                $.each(filtered_equipment_model, function (key, value) {
                    if (key != 'null') {
                        model_html += '<option value="' + key + '">' + value + ' ';
                    }
                });

                $('#equipment_model_id').html(model_html);

                // /dodato 8.2.2021


                modal.find('#equipment_type_id option:selected').removeAttr("selected");
                modal.find('#equipment_type_id option[value="'+equipment.equipment_type_id+'"]').attr('selected', 'selected');
                var selected_type_text = $( "#equipment_type_id option:selected" ).text();
                modal.find('#select2-equipment_type_id-container').prop('title',selected_type_text);
                modal.find('#select2-equipment_type_id-container').prop('innerText',selected_type_text);

                modal.find('#equipment_subtype_id option:selected').removeAttr("selected");
                modal.find('#equipment_subtype_id option[value="'+equipment.equipment_subtype_id+'"]').attr('selected', 'selected');
                var selected_subtype_text = $( "#equipment_subtype_id option:selected" ).text();
                modal.find('#select2-equipment_subtype_id-container').prop('title',selected_subtype_text);
                modal.find('#select2-equipment_subtype_id-container').prop('innerText',selected_subtype_text);


                modal.find('#equipment_model_id option:selected').removeAttr("selected");
                modal.find('#equipment_model_id option[value="'+equipment.equipment_model_id+'"]').attr('selected', 'selected');
                var selected_model_text = $( "#equipment_model_id option:selected" ).text();
                modal.find('#select2-equipment_model_id-container').prop('title',selected_model_text);
                modal.find('#select2-equipment_model_id-container').prop('innerText',selected_model_text);

                modal.find('.modal-body #equipment_serial1').val(equipment.equipment_serial1);
                modal.find('.modal-body #equipment_serial2').val(equipment.equipment_serial2);
                modal.find('.modal-body #equipment_serial3').val(equipment.equipment_serial3);
                modal.find('.modal-body #equipment_dispatch_note').val(equipment.equipment_dispatch_note);
                modal.find('.modal-body #created_at').val(datetimeForView);

            });

            $('#modal_delete').on('show.bs.modal', function (event) {
                var button = $(event.relatedTarget);
                var equipment = data;
                console.log(equipment.equipment_id);
                var modal = $(this);
                modal.find('.modal-body #delete_equipment_id').val(equipment.equipment_id);
                /*modal.find('.modal-body #technician_name').text(technician.technician_name);*/
            });

            $('#modal_sbb_warehouse').on('show.bs.modal', function (event) {
                var button = $(event.relatedTarget);
                var equipment = data;
                console.log(equipment.equipment_id);
                var modal = $(this);
                modal.find('.modal-body #sbb_warehouse_equipment_id').val(equipment.equipment_id);
                /*modal.find('.modal-body #technician_name').text(technician.technician_name);*/
            });

            /* $('.dataTables_processing', $('.datatable-basic1').closest('.dataTables_wrapper')).show();*/



            $('#equipment_type_id').change(function () {
                var selected_type = $("#equipment_type_id option:selected").val(); // izabrana vrsta opreme (type)
                console.log(selected_type);
                var filtered_equipment = filterEquipment('equipment_type_id', selected_type); // filtriranje na osnovu izabrane vrste (type)
                var filtered_equipment_subtype = {};
                $.each(filtered_equipment, function () {
                    filtered_equipment_subtype[this['equipment_subtype_id']] = this['equipment_subtype_name'];
                }); //lista tipova opreme koje pripadaju toj vrsti (subtype)
                console.log(filtered_equipment_subtype);
                var subtype_html = '<option value="" selected="selected"> ';
                $.each(filtered_equipment_subtype, function (key, value) {

                    subtype_html += '<option value="' + key + '">' + value + ' ';
                }); //kreiranje option tagova

                $('#equipment_subtype_id').html(subtype_html); // ubacivanje filtriranih option tagova


                //ista procedura za model
                var filtered_equipment_model = {};
                $.each(filtered_equipment, function () {
                    filtered_equipment_model[this['equipment_model_id']] = this['equipment_model_name'];
                });
                var model_html = '<option value="" selected="selected"> ';
                $.each(filtered_equipment_model, function (key, value) {
                    if (key != 'null') {
                        model_html += '<option value="' + key + '">' + value + ' ';
                    }
                });

                $('#equipment_model_id').html(model_html);
            });


            $('#equipment_subtype_id').change(function () {
                var selected_subtype = $("#equipment_subtype_id option:selected").val();
                var filtered_equipment = filterEquipment('equipment_subtype_id', selected_subtype);

                var equipment_type = filtered_equipment["0"].equipment_type_id;

                //equipment_type_id select
                $('#equipment_type_id option:selected').removeAttr("selected");
                $('#equipment_type_id option[value="' + equipment_type + '"]').attr('selected', 'selected');
                var selected_type_text = $("#equipment_type_id option:selected").text();
                $('#select2-equipment_type_id-container').prop('title', selected_type_text);
                $('#select2-equipment_type_id-container').prop('innerText', selected_type_text);


                //equipment_model_id select
                var filtered_equipment_model = {};
                $.each(filtered_equipment, function () {
                    filtered_equipment_model[this['equipment_model_id']] = this['equipment_model_name'];
                });
                var model_html = '<option value="" selected="selected"> ';
                $.each(filtered_equipment_model, function (key, value) {
                    if (key != 'null') {
                        model_html += '<option value="' + key + '">' + value + ' ';
                    }
                });

                $('#equipment_model_id').html(model_html);

            });

            //equipment_model_id select
            $('#equipment_model_id').change(function () {
                var selected_model = $("#equipment_model_id option:selected").val();
                var filtered_equipment = filterEquipment('equipment_model_id', selected_model);
                var equipment_type = filtered_equipment["0"].equipment_type_id;
                var equipment_subtype = filtered_equipment["0"].equipment_subtype_id;

                //equipment_type_id select
                $('#equipment_type_id option:selected').removeAttr("selected");
                $('#equipment_type_id option[value="' + equipment_type + '"]').attr('selected', 'selected');
                var selected_type_text = $("#equipment_type_id option:selected").text();
                $('#select2-equipment_type_id-container').prop('title', selected_type_text);
                $('#select2-equipment_type_id-container').prop('innerText', selected_type_text);


                //equipment_subtype_id select
                $('#equipment_subtype_id option:selected').removeAttr("selected");
                $('#equipment_subtype_id option[value="' + equipment_subtype + '"]').attr('selected', 'selected');
                var selected_subtype_text = $("#equipment_subtype_id option:selected").text();
                $('#select2-equipment_subtype_id-container').prop('title', selected_subtype_text);
                $('#select2-equipment_subtype_id-container').prop('innerText', selected_subtype_text);

            });



        </script>

<script src="../global_assets/js/main/jquery.min.js"></script>
<script src="../global_assets/js/main/bootstrap.bundle.min.js"></script>
    @endpush

    <div>
        <div class="content">
            <div class="title m-b-md">
                <h1 style="text-align: center;">Pregled opreme</h1>
            </div>

            <div>
            {{--<button type="button" class="btn bg-teal-300" data-toggle="modal" data-target="#modal_add">Dodavanje nove opreme <i class="icon-plus3 ml-1"></i></button>--}}

            <!-- Edit modal -->
                <div id="modal_edit" class="modal fade" style="overflow:hidden;"> {{--tabindex="-1"--}}
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title">Izmena </h5>
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                            </div>

                            <div class="modal-body">

                                {!! Form::open(['action' => ['EquipmentController@update', 'update'], 'method' => 'POST']) !!}
                                {{ csrf_field() }}
                                {{ method_field('PUT')}}
                                {{Form::hidden ('equipment_id', 'update', ['id' => 'update_equipment_id'])}}

                                {{ Form::datetime_picker('created_at', 'Datum i vreme unosa opreme:', null, null, 'form-control', ['placeholder' => 'Izaberi datum'], 'col-12 mb-2 pl-0', true) }}

                                {{ Form::selectGroupSearch2('equipment_type_id', 'Vrsta opreme:','col-3',
                              $data['type'], null,
                               ['data-placeholder' => 'Izaberite vrstu' /*'disabled' => 'disabled'*/,
                               'class'=> 'form-control select-search equipment_type',
                               'data-fouc'], 'col-12 mb-2 ') }}

                                {{ Form::selectGroupSearch2('equipment_subtype_id',
                                                            'Tip opreme:',
                                                            'col-3',
                                                            $data['subtype'], null,
                                                            ['data-placeholder' => 'Izaberite tip opreme' /*'disabled' => 'disabled'*/,
                                                             'class'=> 'form-control select-search equipment_subtype',
                                                             'data-fouc'], 'col-12 mb-2') }}

{{--@dd($data['model'])--}}
                                {{ Form::selectGroupSearch2('equipment_model_id',
                                                            'Model:',
                                                            'col-3',
                                                            $data['model'], null,
                                                            ['data-placeholder' => 'Izaberite model',
                                                             'class'=> 'form-control select-search equipment_model_id',
                                                             'data-fouc'], 'col-12 mb-2') }}

                                {{ Form::textGroup('equipment_serial1', 'S/N 1:', null, ['placeholder' => 'Upišite S/N 1'], null, 'col-12', 'col-3', 'col-9') }}

                                {{ Form::textGroup('equipment_serial2', 'S/N 2:', null, ['placeholder' => 'Upišite S/N 2'], null, 'col-12', 'col-3', 'col-9') }}

                                {{ Form::textGroup('equipment_serial3', 'S/N 3:', null, ['placeholder' => 'Upišite S/N 3'], null, 'col-12', 'col-3', 'col-9') }}

                                {{ Form::textGroup('equipment_dispatch_note', 'Broj otpremnice:', null, ['placeholder' => 'Upišite broj otpremnice'], null, 'col-12', 'col-3', 'col-9') }}



                                {{--<div class="form-row mb-2">
                                    <label>Broj otpremnice:</label>
                                    <input type="text" name='equipment_dispatch_note' id='equipment_dispatch_note'
                                           placeholder="Upišite broj otpremnice" value="{{session('session_data')['5']}}"
                                           class="form-control" autocomplete="off">
                                </div>--}}


                            </div>



                            <div class="modal-footer">
                                <button type="button" class="btn btn-light" data-dismiss="modal">Odustani</button>
                                {!! Form::button('Sačuvaj', ['id' =>'button-create-technician', 'type' =>'submit', 'class' => 'btn bg-teal-300 button-submit', 'data-loading-text' => 'Sačuvaj novog korisnika']) !!}
                            </div>
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
                <!-- /edit modal -->

                <!-- Delete modal -->
                <div id="modal_delete" class="modal fade" tabindex="-1">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title">Brisanje opreme</h5>
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                            </div>

                            <div class="modal-body">
                                {!! Form::open(['action' => ['EquipmentController@update', 'delete'], 'method' => 'POST']) !!}
                                {{ csrf_field() }}
                                {{ method_field('PUT')}}
                                {{Form::hidden ('equipment_id', 'delete', ['id' => 'delete_equipment_id'])}}
                                <p>Da li ste sigurni da želite da izbrišete ovu opremu {{--"<span id="technician_name"></span>"--}}?</p>
                            </div>

                            <div class="modal-footer">
                                <button type="button" class="btn btn-light" data-dismiss="modal">Odustani</button>
                                {!! Form::button('Izbriši', ['type' => 'submit', 'id' =>'button-delete-equipment', 'class' => 'btn btn-danger button-submit', 'data-loading-text' => 'Izbriši opremu']) !!}
                            </div>
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
                <!-- /delete modal -->

                <!-- sbb warehouse modal -->
                <div id="modal_sbb_warehouse" class="modal fade" tabindex="-1">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title">Vraćanje opreme u SBB magacin</h5>
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                            </div>

                            <div class="modal-body">
                                {!! Form::open(['action' => ['EquipmentController@update', 'sbb_warehouse'], 'method' => 'POST']) !!}
                                {{ csrf_field() }}
                                {{ method_field('PUT')}}
                                {{Form::hidden ('equipment_id', 'sbb_warehouse', ['id' => 'sbb_warehouse_equipment_id'])}}
                                <p>Da li ste sigurni da želite da ovu opremu vratite u SBB magacin {{--"<span id="technician_name"></span>"--}}?</p>
                            </div>

                            <div class="modal-footer">
                                <button type="button" class="btn btn-light" data-dismiss="modal">Odustani</button>
                                {!! Form::button('Vrati u SBB magacin', ['type' => 'submit', 'id' =>'button-sbb-equipment', 'class' => 'btn btn-danger button-submit', 'data-loading-text' => 'Izbriši opremu']) !!}
                            </div>
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
                <!-- /sbb warehouse modal -->


                @if ($errors->any())
                    <div class="alert alert-danger">
                        @foreach ($errors->all() as $error)
                            <div>- {{ $error }}</div>
                        @endforeach
                    </div>
            @endif


            <!-- Bordered pills -->
                <div class="card">

                    <div class="card-body" style="border-bottom: 1px solid #bbbfc3;">
                        <ul class="nav nav-pills nav-pills-bordered" style="margin-bottom:0 !important;">
                            <li class="nav-item"><a href="#bordered-pill1" class="nav-link active" data-toggle="tab" onclick="set_actitve_tab()">Neupotrebljena
                                    oprema</a></li>
                            <li class="nav-item"><a href="#bordered-pill2" class="nav-link" data-toggle="tab" onclick="set_actitve_tab()">Oprema u
                                    magacinu</a></li>
                            <li class="nav-item"><a href="#bordered-pill3" class="nav-link" data-toggle="tab" onclick="set_actitve_tab()">Oprema kod
                                    tehničara</a></li>

                            <li class="nav-item"><a href="#bordered-pill4" class="nav-link" data-toggle="tab" onclick="set_actitve_tab()">Prikaz opreme po količini</a></li>
                        </ul>
                    </div>


                    <div class="tab-content">
                        <div class="tab-pane fade show active" id="bordered-pill1">

                            <!-- Prikaz neiskoriscene opreme datatable -->
                            <table class="table datatable-basic1 table-bordered table-striped">
                                <thead>
                                <tr>
                                    <th>Kôd</th>
                                    <th>Tip opreme</th>
                                    <th>Model</th>
                                    <th>S/N 1</th>
                                    <th>S/N 2</th>
                                    <th>S/N 3</th>
                                    <th>Status</th>
                                    <th>Zadužen</th>
                                    <th>Broj otpremnice</th>
                                    <th>Dodao</th>
                                    <th>Datum i vreme unosa</th>
                                    <th>Akcija</th>
                                </tr>
                                </thead>
                            </table>
                        </div>
                        <!-- /Prikaz neiskoriscene opreme datatable -->

                        <!-- Oprema u magacinu -->
                        <div class="tab-pane fade" id="bordered-pill2">

                            <table class="table datatable-basic2 table-bordered table-striped">

                                <thead>
                                <tr>
                                    <th>Kôd</th>
                                    <th>Tip opreme</th>
                                    <th>Model</th>
                                    <th>S/N 1</th>
                                    <th>S/N 2</th>
                                    <th>S/N 3</th>
                                </tr>
                                </thead>
                            </table>
                        </div>
                        <!-- /oprema u magacinu -->

                        <!-- Oprema kod tehnicara -->
                        <div class="tab-pane fade" id="bordered-pill3">

                            <table class="table datatable-basic3 table-bordered table-striped">

                                <thead>
                                <tr>
                                    <th>Kôd</th>
                                    <th>Tip opreme</th>
                                    <th>Model</th>
                                    <th>S/N 1</th>
                                    <th>S/N 2</th>
                                    <th>S/N 3</th>
                                    <th>Zadužen</th>
                                </tr>
                                </thead>
                            </table>
                        </div>
                        <!-- /oprema kod tehnicara -->


                        <div class="tab-pane fade" id="bordered-pill4">

                            <!-- Prikaz tipa opreme i kolicine datatable -->

                            <table class="table datatable-basic4 table-bordered table-striped">
                                <thead>
                                <tr>
                                    <th>Tip opreme</th>
                                    <th>Količina u magacinu</th>
                                    <th>Količina kod tehničara</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($data['unused_equimpment'] as $subtype_id => $subtype_name)
                                    <tr>
                                        <td>{{ $subtype_name }}</td>
                                        <td> @if(array_key_exists($subtype_id, $data['equipment_count_warehouse']))                                                           {{ $data['equipment_count_warehouse'][$subtype_id] }}
                                            @else
                                                0
                                            @endif</td>
                                        <td> @if(array_key_exists($subtype_id, $data['equipment_count_tehnician']))                                                           {{ $data['equipment_count_tehnician'][$subtype_id] }}
                                            @else
                                                0
                                            @endif</td>
                                    </tr>
                                @endforeach

                                </tbody>
                            </table>

                            <!-- /Prikaz neiskoriscene opreme datatable -->
                        </div>





                    </div>
                </div>


            </div>
        </div>
    </div>

@endsection



