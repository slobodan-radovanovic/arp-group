@extends('layouts.app')

@section('content')
    @push ('style')
        <!-- Global stylesheets -->
        <link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
        <link href="../../../../global_assets/css/icons/icomoon/styles.css" rel="stylesheet" type="text/css">
        <link href="../assets/css/bootstrap.min.css" rel="stylesheet" type="text/css">
        <link href="../assets/css/bootstrap_limitless.min.css" rel="stylesheet" type="text/css">
        <link href="../assets/css/layout.min.css" rel="stylesheet" type="text/css">
        <link href="../assets/css/components.min.css" rel="stylesheet" type="text/css">
        <link href="../assets/css/colors.min.css" rel="stylesheet" type="text/css">
        <!-- /global stylesheets -->

    @endpush

    @push('scripts')

        {{--<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
                integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
                crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
                integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
                crossorigin="anonymous"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
                integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
                crossorigin="anonymous"></script>--}}

        <!-- Load select2 -->
        <script type="text/javascript" src="../global_assets/js/plugins/forms/selects/select2.min.js"></script>
        <script type="text/javascript" src="../global_assets/js/plugins/extensions/jquery_ui/interactions.min.js"></script>
        <script src="../assets/js/custom_select2.js"></script>

        <script src="../global_assets/js/plugins/tables/datatables/datatables.min.js"></script>
        <script src="../global_assets/js/demo_pages/datatables_basic.js"></script>
        <script src="../global_assets/js/plugins/forms/selects/select2.min.js"></script>


        <script src="../global_assets/js/main/jquery.min.js"></script>
        <script src="../global_assets/js/main/bootstrap.bundle.min.js"></script>
        <script src="../global_assets/js/plugins/loaders/blockui.min.js"></script>
        <script src="../global_assets/js/plugins/tables/datatables/datatables.min.js"></script>
        <script src="../global_assets/js/plugins/forms/selects/select2.min.js"></script>
        <script src="../global_assets/js/plugins/tables/datatables/extensions/jszip/jszip.min.js"></script>
        <script src="../global_assets/js/plugins/tables/datatables/extensions/buttons.min.js"></script>
      {{--  <script src="../assets/js/app.js"></script>
        <script src="../global_assets/js/demo_pages/datatables_extension_buttons_html5.js"></script>--}}


        {{--export--}}

       {{--<script src="../global_assets/js/plugins/forms/selects/select2.min.js"></script>
        <script src="../global_assets/js/plugins/tables/datatables/extensions/jszip/jszip.min.js"></script>
        <script src="../global_assets/js/plugins/tables/datatables/extensions/pdfmake/pdfmake.min.js"></script>
        <script src="../global_assets/js/plugins/tables/datatables/extensions/pdfmake/vfs_fonts.min.js"></script>
        <script src="../global_assets/js/plugins/tables/datatables/extensions/buttons.min.js"></script>--}}
       {{-- <script src="../global_assets/js/demo_pages/datatables_extension_buttons_html5.js"></script>--}}

        <!-- Theme JS files -->
        {{--<script src="../../../../global_assets/js/plugins/tables/datatables/datatables.min.js"></script>
        <script src="../../../../global_assets/js/plugins/forms/selects/select2.min.js"></script>
        <script src="../../../../global_assets/js/plugins/tables/datatables/extensions/jszip/jszip.min.js"></script>
        <script src="../../../../global_assets/js/plugins/tables/datatables/extensions/pdfmake/pdfmake.min.js"></script>
        <script src="../../../../global_assets/js/plugins/tables/datatables/extensions/pdfmake/vfs_fonts.min.js"></script>
        <script src="../../../../global_assets/js/plugins/tables/datatables/extensions/buttons.min.js"></script>--}}

        {{--<script src="assets/js/app.js"></script>--}}
        {{--<script src="../../../../global_assets/js/demo_pages/datatables_extension_buttons_html5.js"></script>--}}
        <!-- /theme JS files -->



        <script>
            /*$('.datatable-basic').DataTable( {
                dom: 'Bfrtip',
                buttons: [
                    'excelHtml5'
                ]
            } );*/
            /*var active_tab = $('.nav-link.active')['1'].innerText;


            function set_actitve_tab(){
                $( document ).ready(function() {
                    active_tab = $('.nav-link.active')['1'].innerText;
                    console.log(active_tab);
                    console.log( "ready!" );
                });

            }*/

            var today = new Date();
            var dd = String(today.getDate()).padStart(2, '0');
            var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
            var yyyy = today.getFullYear();

            today = dd + '-' + mm + '-' + yyyy;
            console.log(today);

            $('.datatable-basic1').DataTable({
                autoWidth: false,
                dom: '<"datatable-header"fBl><"datatable-scroll-wrap"t><"datatable-footer"ip>',
                language: {
                    search: '<span>Pretraga:</span> _INPUT_',
                    lengthMenu: '<span>Prikaz:</span> _MENU_',
                    info: '_START_ do _END_ od ukupno _TOTAL_',
                    paginate: {'first': 'First', 'last': 'Last', 'next': '→', 'previous': '←'}
                },
                buttons: {
                    buttons: [
                        {
                            extend: 'excelHtml5',
                            className: 'btn btn-light',
                            title: 'Oprema (Neupotrebljena oprema) '+today,
                            sheetName:'Neupotrebljena oprema',
                            exportOptions: {
                                    columns: [ 0, 1, 2, 3, 4, 5, 6 ]
                            }
                        }/*,
                        {
                            extend: 'colvis',
                            text: '<i class="icon-three-bars"></i>',
                            className: 'btn bg-blue btn-icon dropdown-toggle'
                        }*/
                    ]
                }
            });

            $('.datatable-basic2').DataTable({
                autoWidth: false,
                dom: '<"datatable-header"fBl><"datatable-scroll-wrap"t><"datatable-footer"ip>',
                language: {
                    search: '<span>Pretraga:</span> _INPUT_',
                    lengthMenu: '<span>Prikaz:</span> _MENU_',
                    info: '_START_ do _END_ od ukupno _TOTAL_',
                    paginate: {'first': 'First', 'last': 'Last', 'next': '→', 'previous': '←'}
                },
                buttons: {
                    buttons: [
                        {
                            extend: 'excelHtml5',
                            className: 'btn btn-light',
                            title: 'Oprema (Oprema u magacinu) '+today,
                            sheetName:'Oprema u magacinu',
                            exportOptions: {
                                columns: [ 0, 1, 2, 3, 4, 5 ]
                            }
                        }
                    ]
                }
            });

            $('.datatable-basic3').DataTable({
                autoWidth: false,
                dom: '<"datatable-header"fBl><"datatable-scroll-wrap"t><"datatable-footer"ip>',
                language: {
                    search: '<span>Pretraga:</span> _INPUT_',
                    lengthMenu: '<span>Prikaz:</span> _MENU_',
                    info: '_START_ do _END_ od ukupno _TOTAL_',
                    paginate: {'first': 'First', 'last': 'Last', 'next': '→', 'previous': '←'}
                },
                buttons: {
                    buttons: [
                        {
                            extend: 'excelHtml5',
                            className: 'btn btn-light',
                            title: 'Oprema (Oprema kod tehničara) '+today,
                            sheetName:'Oprema kod tehničara',
                            exportOptions: {
                                columns: [ 0, 1, 2, 3, 4, 5 ]
                            }
                        }
                    ]
                }
            });

            $('.datatable-basic4').DataTable({
                autoWidth: false,
                dom: '<"datatable-header"fBl><"datatable-scroll-wrap"t><"datatable-footer"ip>',
                language: {
                    search: '<span>Pretraga:</span> _INPUT_',
                    lengthMenu: '<span>Prikaz:</span> _MENU_',
                    info: '_START_ do _END_ od ukupno _TOTAL_',
                    paginate: {'first': 'First', 'last': 'Last', 'next': '→', 'previous': '←'}
                },
                buttons: {
                    buttons: [
                        {
                            extend: 'excelHtml5',
                            className: 'btn btn-light',
                            title: 'Oprema (Prikaz opreme po količini) '+today,
                            sheetName:'Prikaz opreme po količini',
                            exportOptions: {
                                columns: [ 0, 1, 2 ]
                            }
                        }
                    ]
                }
            });


            $('#modal_edit').on('show.bs.modal', function (event) {
                var button = $(event.relatedTarget);

                var equipment = button.data('alldata');
                var modal = $(this);

                modal.find('.modal-body #equipment_id').val(equipment.equipment_id);
                modal.find('.modal-body #equipment_code').val(equipment.equipment_code);

                //equipment_type select
                modal.find('#equipment_type option:selected').removeAttr("selected");
                modal.find('#equipment_type option[value="'+equipment.equipment_type_id+'"]').attr('selected', 'selected');
                var selected_type_text = $( "#equipment_type option:selected" ).text();
                modal.find('#select2-equipment_type-container').prop('title',selected_type_text);
                modal.find('#select2-equipment_type-container').prop('innerText',selected_type_text);

                //equipment_subtype select
                modal.find('#equipment_subtype option:selected').removeAttr("selected");
                modal.find('#equipment_subtype option[value="'+equipment.equipment_subtype_id+'"]').attr('selected', 'selected');
                var selected_subtype_text = $( "#equipment_subtype option:selected" ).text();
                modal.find('#select2-equipment_subtype-container').prop('title',selected_subtype_text);
                modal.find('#select2-equipment_subtype-container').prop('innerText',selected_subtype_text);

                //equipment_model select
                if(equipment.equipment_model_id==''){
                    modal.find('#equipment_model option:selected').removeAttr("selected");
                    modal.find('#equipment_model option[value=""]').attr('selected', 'selected');
                    /*var selected_model_text = $( "#equipment_model option:selected" ).text();*/

                    modal.find('#select2-equipment_model-container').prop('innerHTML','<span class="select2-selection__placeholder">Izaberite model</span>');
                    /*modal.find('#select2-equipment_model-container').prop('innerText',selected_model_text);*/
                }else{
                    modal.find('#equipment_model option:selected').removeAttr("selected");
                    modal.find('#equipment_model option[value="'+equipment.equipment_model_id+'"]').attr('selected', 'selected');
                    var selected_model_text = $( "#equipment_model option:selected" ).text();
                    modal.find('#select2-equipment_model-container').prop('title',selected_model_text);
                    modal.find('#select2-equipment_model-container').prop('innerText',selected_model_text);
                }

                modal.find('.modal-body #equipment_serial1').val(equipment.equipment_serial1);
                modal.find('.modal-body #equipment_serial2').val(equipment.equipment_serial2);
                modal.find('.modal-body #equipment_serial3').val(equipment.equipment_serial3);
                /*console.log(modal.find('#equipment_model option:selected'));*/
            });


            $('#modal_delete').on('show.bs.modal', function (event) {
                var button = $(event.relatedTarget);
                var equipment = button.data('alldata');
                console.log(equipment.equipment_id);
                var modal = $(this);
                modal.find('.modal-body #equipment_id').val(equipment.equipment_id);
                /*modal.find('.modal-body #technician_name').text(technician.technician_name);*/
            });

        </script>
    @endpush

    <div>
        <div class="content">
            <div class="title m-b-md">
                <h1 style="text-align: center;">Pregled opreme</h1>
            </div>

            <div>
            {{--<button type="button" class="btn bg-teal-300" data-toggle="modal" data-target="#modal_add">Dodavanje nove opreme <i class="icon-plus3 ml-1"></i></button>--}}

            <!-- Add modal -->
                <div id="modal_" class="modal fade" tabindex="-1">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title">Dodavanje nove opreme</h5>
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                            </div>

                            <div class="modal-body">
                                {!! Form::open(['id' => 'form-create-customer', 'role' => 'form', 'class' => 'form-loading-button']) !!}

                                {{ Form::textGroup('name', 'Ime tehničara', null, ['placeholder' => 'Upisite ime novog tehničara'], null, 'col-12', 'col-3', 'col-9') }}
                            </div>

                            <div class="modal-footer">
                                <button type="button" class="btn btn-light" data-dismiss="modal">Odustani</button>
                                {!! Form::button('Dodaj', ['type' => 'button', 'id' =>'button-create-technician', 'class' => 'btn bg-teal-300 button-submit', 'data-loading-text' => 'Sačuvaj novog korisnika']) !!}
                            </div>
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
                <!-- /basic modal -->

                <!-- Edit modal -->
                <div id="modal_edit" class="modal fade" tabindex="-1">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title">Izmena </h5>
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                            </div>

                            <div class="modal-body">

                                {!! Form::open(['action' => ['EquipmentController@update', 'update'], 'method' => 'POST']) !!}
                                {{ csrf_field() }}
                                {{ method_field('PUT')}}
                                {{Form::hidden ('equipment_id', 'update', ['id' => 'equipment_id'])}}

                                {{ Form::textGroup('equipment_code', 'Kôd:', null, ['placeholder' => 'Upisite kôd opreme'], null, 'col-12', 'col-3', 'col-9') }}

                                {{ Form::selectGroupSearch2('equipment_type', 'Vrsta opreme:','col-3',
                              $data['type'], null,
                               ['data-placeholder' => 'Izaberite vrstu', 'disabled',
                               'class'=> 'form-control select-search equipment_type',
                               'data-fouc'], 'col-12 mb-2 ') }}

                                {{ Form::selectGroupSearch2('equipment_subtype',
                                                            'Tip opreme:',
                                                            'col-3',
                                                            $data['subtype'], null,
                                                            ['data-placeholder' => 'Izaberite tip opreme', 'disabled',
                                                             'class'=> 'form-control select-search equipment_subtype',
                                                             'data-fouc'], 'col-12 mb-2') }}


                                {{ Form::selectGroupSearch2('equipment_model',
                                                            'Model:',
                                                            'col-3',
                                                            $data['model'], null,
                                                            ['data-placeholder' => 'Izaberite model', 'disabled',
                                                             'class'=> 'form-control select-search equipment_model',
                                                             'data-fouc'], 'col-12 mb-2') }}

                                {{ Form::textGroup('equipment_serial1', 'S/N 1:', null, ['placeholder' => 'Upišite S/N 1'], null, 'col-12', 'col-3', 'col-9') }}

                                {{ Form::textGroup('equipment_serial2', 'S/N 2:', null, ['placeholder' => 'Upišite S/N 2'], null, 'col-12', 'col-3', 'col-9') }}

                                {{ Form::textGroup('equipment_serial3', 'S/N 3:', null, ['placeholder' => 'Upišite S/N 3'], null, 'col-12', 'col-3', 'col-9') }}
                            </div>



                            <div class="modal-footer">
                                <button type="button" class="btn btn-light" data-dismiss="modal">Odustani</button>
                                {!! Form::button('Sačuvaj', ['id' =>'button-create-technician', 'type' =>'submit', 'class' => 'btn bg-teal-300 button-submit', 'data-loading-text' => 'Sačuvaj novog korisnika']) !!}
                            </div>
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
                <!-- /edit modal -->

                <!-- Delete modal -->
                <div id="modal_delete" class="modal fade" tabindex="-1">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title">Brisanje opreme</h5>
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                            </div>

                            <div class="modal-body">
                                {!! Form::open(['action' => ['EquipmentController@update', 'delete'], 'method' => 'POST']) !!}
                                {{ csrf_field() }}
                                {{ method_field('PUT')}}
                                {{Form::hidden ('equipment_id', 'delete', ['id' => 'equipment_id'])}}
                                <p>Da li ste sigurni da želite da izbrišete ovu opremu {{--"<span id="technician_name"></span>"--}}?</p>
                            </div>

                            <div class="modal-footer">
                                <button type="button" class="btn btn-light" data-dismiss="modal">Odustani</button>
                                {!! Form::button('Izbriši', ['type' => 'submit', 'id' =>'button-delete-equipment', 'class' => 'btn btn-danger button-submit', 'data-loading-text' => 'Izbriši opremu']) !!}
                            </div>
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
                <!-- /delete modal -->

                <!-- Bordered pills -->
                <div class="card">

                    <div class="card-body" style="border-bottom: 1px solid #bbbfc3;">
                        <ul class="nav nav-pills nav-pills-bordered" style="margin-bottom:0 !important;">
                            <li class="nav-item"><a href="#bordered-pill1" class="nav-link active" data-toggle="tab" onclick="set_actitve_tab()">Neupotrebljena
                                    oprema</a></li>
                            <li class="nav-item"><a href="#bordered-pill2" class="nav-link" data-toggle="tab" onclick="set_actitve_tab()">Oprema u
                                    magacinu</a></li>
                            <li class="nav-item"><a href="#bordered-pill3" class="nav-link" data-toggle="tab" onclick="set_actitve_tab()">Oprema kod
                                    tehničara</a></li>
                            {{--<li class="nav-item nav-link"><a href="#--}}{{--bordered-pill4--}}{{--" class="nav-link" data-toggle="tab"> Ugradjena oprema (4)</a>
                                Ugradjena oprema
                            </li>--}}
                           {{-- <li class="nav-item"><a href="equipment/installed_equipment" class="nav-link">Ugradjena oprema</a></li>--}}
                            <li class="nav-item"><a href="#bordered-pill5" class="nav-link" data-toggle="tab" onclick="set_actitve_tab()">Prikaz opreme po količini</a></li>
                        </ul>
                    </div>


                    <div class="tab-content">
                        <div class="tab-pane fade show active" id="bordered-pill1">

                            <!-- Prikaz neiskoriscene opreme datatable -->

                            <table class="table datatable-basic1 table-bordered table-striped">
                                <thead>
                                <tr>
                                    <th>Kôd</th>
                                    <th>Tip opreme</th>
                                    <th>Model</th>
                                    <th>S/N 1</th>
                                    <th>S/N 2</th>
                                    <th>S/N 3</th>
                                    <th>Status</th>
                                    <th class="text-center"></th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($data['equipment1'] as $equipment)

                                    <tr>
                                        <td>{{ $equipment->equipment_code }}</td>
                                        <td> {{ $data['subtype'][$equipment->equipment_subtype_id] }}</td>
                                        @if($equipment->equipment_model_id != null)
                                            <td> {{ $data['model'][$equipment->equipment_model_id] }}</td>
                                        @else
                                            <td></td>
                                        @endif
                                        <td>{{ $equipment->equipment_serial1 }}</td>
                                        <td>{{ $equipment->equipment_serial2 }}</td>
                                        <td>{{ $equipment->equipment_serial3 }}</td>
                                        <td>{{ $equipment->equipment_status }}</td>

                                        <td class="text-center">
                                            <div class="list-icons">
                                                <div class="dropdown">
                                                    <a href="#" class="list-icons-item" data-toggle="dropdown">
                                                        <i class="icon-menu9"></i>
                                                    </a>

                                                    <div class="dropdown-menu dropdown-menu-left">
                                                        <button class="dropdown-item" data-alldata="{{$equipment}}" data-toggle="modal" data-target="#modal_edit"><i class="icon-pencil7"></i>Izmeni</button>

                                                        <button class="dropdown-item" data-alldata="{{$equipment}}" data-toggle="modal" data-target="#modal_delete"><i class="icon-cancel-square"></i>Izbriši</button>

                                                    </div>
                                                </div>
                                            </div>

                                        </td>
                                    </tr>
                                @endforeach

                                </tbody>
                            </table>

                            <!-- /Prikaz neiskoriscene opreme datatable -->

                        </div>

                        <div class="tab-pane fade" id="bordered-pill2">
                            <!-- Zakazani datatable -->


                            <table class="table datatable-basic2 table-bordered table-striped">
                                <thead>
                                <tr>
                                    <th>Kôd</th>
                                    <th>Tip opreme</th>
                                    <th>Model</th>
                                    <th>S/N 1</th>
                                    <th>S/N 2</th>
                                    <th>S/N 3</th>
                                    {{--<th>Status</th>--}}
                                    {{--<th class="text-center"></th>--}}
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($data['equipment2'] as $equipment)

                                    <tr>
                                        <td>{{ $equipment->equipment_code }}</td>
                                        <td> {{ $data['subtype'][$equipment->equipment_subtype_id] }}</td>
                                        @if($equipment->equipment_model_id != null)
                                            <td> {{ $data['model'][$equipment->equipment_model_id] }}</td>
                                        @else
                                            <td></td>
                                        @endif
                                        <td>{{ $equipment->equipment_serial1 }}</td>
                                        <td>{{ $equipment->equipment_serial2 }}</td>
                                        <td>{{ $equipment->equipment_serial3 }}</td>
                                        {{--<td>{{ $equipment->equipment_status }}</td>--}}
                                    </tr>
                                @endforeach

                                </tbody>
                            </table>

                            <!-- /zakazani datatable -->
                        </div>


                        <div class="tab-pane fade" id="bordered-pill3">
                            <!-- Nekompletni datatable -->


                            <table class="table datatable-basic3 table-bordered table-striped">
                                <thead>
                                <tr>
                                    <th>Kôd</th>
                                    <th>Tip opreme</th>
                                    <th>Model</th>
                                    <th>S/N 1</th>
                                    <th>S/N 2</th>
                                    <th>S/N 3</th>{{--
                                        <th>Status</th>--}}
                                   {{-- <th class="text-center"></th>--}}
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($data['equipment3'] as $equipment)

                                    <tr>
                                        <td>{{ $equipment->equipment_code }}</td>
                                        <td> {{ $data['subtype'][$equipment->equipment_subtype_id] }}</td>
                                        @if($equipment->equipment_model_id != null)
                                            <td> {{ $data['model'][$equipment->equipment_model_id] }}</td>
                                        @else
                                            <td></td>
                                        @endif
                                        <td>{{ $equipment->equipment_serial1 }}</td>
                                        <td>{{ $equipment->equipment_serial2 }}</td>
                                        <td>{{ $equipment->equipment_serial3 }}</td>
                                        {{-- <td>{{ $equipment->equipment_status }}</td>--}}
                                    </tr>
                                @endforeach

                                </tbody>
                            </table>

                            <!-- /zakazani datatable -->
                        </div>

                        <div class="tab-pane fade" id="bordered-pill4">
                            <!-- Zatvoreni datatable -->

                            <!-- /Zatvoreni datatable -->
                        </div>


                        <div class="tab-pane fade" id="bordered-pill5">

                            <!-- Prikaz tipa opreme i kolicine datatable -->

                            <table class="table datatable-basic4 table-bordered table-striped">
                                <thead>
                                <tr>
                                    <th>Tip opreme</th>
                                    <th>Količina u magacinu</th>
                                    <th>Količina kod tehnicara</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($data['unused_equimpment'] as $subtype_id => $subtype_name)
                                    <tr>
                                        <td>{{ $subtype_name }}</td>
                                        <td> @if(array_key_exists($subtype_id, $data['equipment_count_warehouse']))                                                           {{ $data['equipment_count_warehouse'][$subtype_id] }}
                                            @else
                                                0
                                            @endif</td>
                                        <td> @if(array_key_exists($subtype_id, $data['equipment_count_tehnician']))                                                           {{ $data['equipment_count_tehnician'][$subtype_id] }}
                                            @else
                                                0
                                            @endif</td>
                                    </tr>
                                @endforeach

                                </tbody>
                            </table>

                            <!-- /Prikaz neiskoriscene opreme datatable -->

                        </div>


                    </div>
                </div>
                <!-- /Bordered pills -->

                <!-- Basic datatable -->
            {{-- <div class="card">

                 <table class="table datatable-basic">
                     <thead>
                     <tr>
                         <th>Tip opreme</th>
                         <th>Model</th>
                         <th>S/N 1</th>
                         <th>S/N 2</th>
                         <th>S/N 3</th>
                         <th>Status</th>
                         <th class="text-center"></th>
                     </tr>
                     </thead>
                     <tbody>
                     @foreach($data['equipment'] as $equipment)

                         <tr>
                             <td> {{ $data['subtype'][$equipment->equipment_subtype_id] }}</td>
                             <td> {{ $data['model'][$equipment->equipment_model_id] }}</td>
                             <td>{{ $equipment->equipment_serial1 }}</td>
                             <td>{{ $equipment->equipment_serial2 }}</td>
                             <td>{{ $equipment->equipment_serial3 }}</td>
                             <td>{{ $equipment->equipment_status }}</td>

                             <td class="text-center">
                                 <div class="list-icons">
                                     <div class="dropdown">
                                         <a href="#" class="list-icons-item" data-toggle="dropdown">
                                             <i class="icon-menu9"></i>
                                         </a>

                                         <div class="dropdown-menu dropdown-menu-left">
                                             <a href="#" class="dropdown-item" data-toggle="modal" data-target="#modal_edit"><i class="icon-pencil7" ></i> Izmeni</a>
                                             <a href="#" class="dropdown-item" data-toggle="modal" data-target="#modal_delete"><i class="icon-cancel-square"></i> Izbriši</a>
                                         </div>
                                     </div>
                                 </div>

                             </td>
                         </tr>
                     @endforeach

                     </tbody>
                 </table>
             </div>--}}
            <!-- /basic datatable -->

            </div>
        </div>
    </div>

@endsection



