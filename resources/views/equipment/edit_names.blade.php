@extends('layouts.app')

@section('content')

    @push('style')
        <style>
            .nav-pills-bordered .nav-link.active {
                border-color: #4db6ac !important;
            }

            .nav-pills .nav-link.active, .nav-pills .show > .nav-link {
                color: #fff;
                background-color: #4db6ac !important;
            }


        </style>

        <!-- Global stylesheets -->
        <link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
        <link href="../../../../global_assets/css/icons/icomoon/styles.css" rel="stylesheet" type="text/css">
        <link href="../assets/css/bootstrap.min.css" rel="stylesheet" type="text/css">
        <link href="../assets/css/bootstrap_limitless.min.css" rel="stylesheet" type="text/css">
        <link href="../assets/css/layout.min.css" rel="stylesheet" type="text/css">
        <link href="../assets/css/components.min.css" rel="stylesheet" type="text/css">
        <link href="../assets/css/colors.min.css" rel="stylesheet" type="text/css">
        <!-- /global stylesheets -->
    @endpush

    @push('scripts')
        <script src="../global_assets/js/plugins/tables/datatables/datatables.min.js"></script>
        <script src="../global_assets/js/demo_pages/datatables_basic.js"></script>
        <!-- Load select2 -->
        <script type="text/javascript" src="../global_assets/js/plugins/forms/selects/select2.min.js"></script>
        <script type="text/javascript" src="../global_assets/js/plugins/extensions/jquery_ui/interactions.min.js"></script>
        <script src="../assets/js/custom_select2.js"></script>
        <script src="../global_assets/js/plugins/forms/selects/select2.min.js"></script>
        {{--anytime--}}
        <script src="../global_assets/js/plugins/ui/moment/moment.min.js"></script>
        <script src="../global_assets/js/plugins/pickers/anytime.min.js"></script>
        <script src="../../../../global_assets/js/plugins/pickers/daterangepicker.js"></script>
        <script src="../assets/js/custom_datepicker.js"></script>

        <script src="../global_assets/js/main/jquery.min.js"></script>
        <script src="../global_assets/js/main/bootstrap.bundle.min.js"></script>
        <script src="../global_assets/js/plugins/loaders/blockui.min.js"></script>
        <script src="../global_assets/js/plugins/tables/datatables/datatables.min.js"></script>
        <script src="../global_assets/js/plugins/forms/selects/select2.min.js"></script>
        <script src="../global_assets/js/plugins/tables/datatables/extensions/jszip/jszip.min.js"></script>
        <script src="../global_assets/js/plugins/tables/datatables/extensions/buttons.min.js"></script>

        {{--export datatable--}}

        {{--anytime--}}
        <script src="../global_assets/js/plugins/ui/moment/moment.min.js"></script>
        <script src="../global_assets/js/plugins/pickers/anytime.min.js"></script>
        <script src="../../../../global_assets/js/plugins/pickers/daterangepicker.js"></script>
        <script src="../assets/js/custom_datepicker.js"></script>



        <script>

            var today = new Date();
            var dd = String(today.getDate()).padStart(2, '0');
            var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
            var yyyy = today.getFullYear();

            today = dd + '-' + mm + '-' + yyyy;
            console.log(today);


            $('.datatable-basic').DataTable({
                autoWidth: false,
                dom: '<"datatable-header"fBl><"datatable-scroll-wrap"t><"datatable-footer"ip>',
                language: {
                    search: '<span>Pretraga:</span> _INPUT_',
                    lengthMenu: '<span>Prikaz:</span> _MENU_',
                    info: '_START_ do _END_ od ukupno _TOTAL_',
                    paginate: {'first': 'First', 'last': 'Last', 'next': '→', 'previous': '←'}
                },
                buttons: {
                    buttons: [
                        {
                            extend: 'excelHtml5',
                            className: 'btn btn-light',
                            title: 'Vrste materijala '+today,
                            sheetName:'Vrste materijala',
                            exportOptions: {
                                columns: [ 0, 1, 2]
                            }
                        }
                    ]
                }
            });


            $('#modal_edit_subtype').on('show.bs.modal', function (event) {
                var button = $(event.relatedTarget);

                var subtype = button.data('alldata');
                var modal = $(this);
                console.log(subtype);
                modal.find('.modal-body #equipment_subtype_id').val(subtype.equipment_subtype_id);
                modal.find('.modal-body #equipment_subtype_name').val(subtype.equipment_subtype_name);

            });

            $('#modal_edit_model').on('show.bs.modal', function (event) {
                var button = $(event.relatedTarget);
                var model = button.data('alldata');
                var modal = $(this);
                console.log(model);
                modal.find('.modal-body #equipment_model_id').val(model.equipment_model_id);
                modal.find('.modal-body #equipment_model_name').val(model.equipment_model_name);
                modal.find('.modal-body #equipment_code').val(model.equipment_code);
            });

            $('#modal_edit_dismantled_subtype').on('show.bs.modal', function (event) {
                var button = $(event.relatedTarget);
                var dismantled_eq_subtype = button.data('alldata');
                var modal = $(this);
                console.log(dismantled_eq_subtype);
                modal.find('.modal-body #dismantled_eq_subtype_id').val(dismantled_eq_subtype.dismantled_eq_subtype_id);
                modal.find('.modal-body #dismantled_eq_subtype_name').val(dismantled_eq_subtype.dismantled_eq_subtype_name);

            });

            $('#modal_edit_dismantled_model').on('show.bs.modal', function (event) {
                var button = $(event.relatedTarget);
                var dismantled_eq_model = button.data('alldata');
                var modal = $(this);
                console.log(dismantled_eq_model);
                modal.find('.modal-body #dismantled_eq_model_id').val(dismantled_eq_model.dismantled_eq_model_id);
                modal.find('.modal-body #dismantled_eq_model_name').val(dismantled_eq_model.dismantled_eq_model_name);
                modal.find('.modal-body #equipment_code').val(dismantled_eq_model.equipment_code);
            });


        </script>
    @endpush

    <div>
        <div class="content">
            <div class="title m-b-md">
                <h1 style="text-align: center;">Izmena ipova i modela opreme</h1>
            </div>

            @if ($errors->any())
                <div class="alert alert-danger">
                    @foreach ($errors->all() as $error)
                        <div>- {{ $error }}</div>
                    @endforeach
                </div>
            @endif

            <div>

                <!-- Edit modal -->
                <div id="modal_edit_subtype" class="modal fade" tabindex="-1">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title">Izmena tipa opreme</h5>
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                            </div>

                            <div class="modal-body">
                                {!! Form::open(['action' => ['EquipmentController@update', 'update_subtype'], 'method' => 'POST']) !!}
                                {{ csrf_field() }}
                                {{ method_field('PUT')}}

                                {{Form::hidden ('equipment_subtype_id', 'update', ['id' => 'equipment_subtype_id'])}}

                                {{ Form::textGroup('equipment_subtype_name', 'Ime tipa opreme', null, ['placeholder' => 'Upisite ime tipa opreme'], null, 'col-12', 'col-3', 'col-9') }}

                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-light" data-dismiss="modal">Odustani</button>
                                {!! Form::button('Izmeni', ['type' => 'submit', 'id' =>'button-edit-subtype', 'class' => 'btn bg-teal-300 button-submit', 'data-loading-text' => 'Izmeni materijal']) !!}
                            </div>
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
                <!-- /edit modal -->

                <!-- Edit modal -->
                <div id="modal_edit_model" class="modal fade" tabindex="-1">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title">Izmena modela opreme</h5>
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                            </div>

                            <div class="modal-body">
                                {!! Form::open(['action' => ['EquipmentController@update', 'update_model'], 'method' => 'POST']) !!}
                                {{ csrf_field() }}
                                {{ method_field('PUT')}}

                                {{Form::hidden ('equipment_model_id', 'update', ['id' => 'equipment_model_id'])}}

                                {{ Form::textGroup('equipment_model_name', 'Ime modela opreme', null, ['placeholder' => 'Upisite ime modela opreme'], null, 'col-12', 'col-3', 'col-9') }}


                                {{ Form::textGroup('equipment_code', 'Kôd opreme', null, ['placeholder' => 'Upisite kôd opreme'], null, 'col-12', 'col-3', 'col-9') }}



                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-light" data-dismiss="modal">Odustani</button>
                                {!! Form::button('Izmeni', ['type' => 'submit', 'id' =>'button-edit-subtype', 'class' => 'btn bg-teal-300 button-submit', 'data-loading-text' => 'Izmeni']) !!}
                            </div>
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
                <!-- /edit modal -->


                <!-- Edit modal -->
                <div id="modal_edit_dismantled_subtype" class="modal fade" tabindex="-1">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title">Izmena tipa demontirane opreme</h5>
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                            </div>

                            <div class="modal-body">
                                {!! Form::open(['action' => ['EquipmentController@update', 'update_dismantled_eq_subtype'], 'method' => 'POST']) !!}
                                {{ csrf_field() }}
                                {{ method_field('PUT')}}

                                {{Form::hidden ('dismantled_eq_subtype_id', 'update', ['id' => 'dismantled_eq_subtype_id'])}}

                                {{ Form::textGroup('dismantled_eq_subtype_name', 'Ime tipa demontirane opreme', null, ['placeholder' => 'Upisite ime tipa demontirane opreme'], null, 'col-12', 'col-3', 'col-9') }}

                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-light" data-dismiss="modal">Odustani</button>
                                {!! Form::button('Izmeni', ['type' => 'submit', 'id' =>'button-edit-subtype', 'class' => 'btn bg-teal-300 button-submit', 'data-loading-text' => 'Izmeni materijal']) !!}
                            </div>
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
                <!-- /edit modal -->

                <!-- Edit modal -->
                <div id="modal_edit_dismantled_model" class="modal fade" tabindex="-1">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title">Izmena modela demontirane opreme</h5>
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                            </div>

                            <div class="modal-body">
                                {!! Form::open(['action' => ['EquipmentController@update', 'update_dismantled_eq_model'], 'method' => 'POST']) !!}
                                {{ csrf_field() }}
                                {{ method_field('PUT')}}

                                {{Form::hidden ('dismantled_eq_model_id', 'update', ['id' => 'dismantled_eq_model_id'])}}

                                {{ Form::textGroup('dismantled_eq_model_name', 'Ime modela demontirane opreme', null, ['placeholder' => 'Upisite ime modela demontirane opreme'], null, 'col-12', 'col-3', 'col-9') }}


                                {{ Form::textGroup('equipment_code', 'Kôd opreme', null, ['placeholder' => 'Upisite kôd opreme'], null, 'col-12', 'col-3', 'col-9') }}



                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-light" data-dismiss="modal">Odustani</button>
                                {!! Form::button('Izmeni', ['type' => 'submit', 'id' =>'button-edit-subtype', 'class' => 'btn bg-teal-300 button-submit', 'data-loading-text' => 'Izmeni']) !!}
                            </div>
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
                <!-- /edit modal -->



                <br>

                <!-- Basic datatable -->
                <div class="card mt-2">

                    <div class="card-body" style="border-bottom: 1px solid #bbbfc3;">
                        <ul class="nav nav-pills nav-pills-bordered" style="margin-bottom:0 !important;">
                            <li class="nav-item"><a href="#bordered-pill1" class="nav-link active" data-toggle="tab" onclick="set_actitve_tab()">Tipovi opreme</a></li>
                            <li class="nav-item"><a href="#bordered-pill2" class="nav-link" data-toggle="tab" onclick="set_actitve_tab()">Modeli opreme</a></li>
                            <li class="nav-item"><a href="#bordered-pill3" class="nav-link" data-toggle="tab" onclick="set_actitve_tab()">Tipovi demontirane opreme</a></li>

                            <li class="nav-item"><a href="#bordered-pill4" class="nav-link" data-toggle="tab" onclick="set_actitve_tab()">Modeli demontirane opreme</a></li>
                        </ul>
                    </div>

                    <div class="tab-content">
                        <div class="tab-pane fade show active" id="bordered-pill1">

                    <table class="table datatable-basic table-bordered table-striped">
                        <thead>
                        <tr>
                            <th>Ime tipa opreme</th>
                            <th>Izmena</th>
                        </tr>
                        </thead>

                        <tbody>
                        @foreach($data['subtype'] as $subtype)
{{--@dd($subtype)--}}
                            <tr>
                                <td>{{$subtype['equipment_subtype_name']}}</td>
                                <td class="text-center">
                                    <a href="#" class="dropdown-item" data-toggle="modal" data-target="#modal_edit_subtype" data-alldata="{{$subtype}}" data-id="{{$subtype->equipment_subtype_id}}"> <i class="icon-pencil7"></i> {{--Izmeni--}}</a>

                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>

                        </div>

                        <div class="tab-pane fade" id="bordered-pill2">

                    <table class="table datatable-basic table-bordered table-striped">
                        <thead>
                        <tr>
                            <th>Ime odela opreme</th>
                            <th>Kôd opreme</th>
                            <th>Izmena</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($data['model'] as $model)

                            <tr>
                                <td>{{$model['equipment_model_name']}}</td>
                                <td>{{$model['equipment_code']}}</td>
                                <td class="text-center">
                                    <a href="#" class="dropdown-item" data-toggle="modal" data-target="#modal_edit_model" data-alldata="{{$model}}"> <i class="icon-pencil7"></i> {{--Izmeni--}}</a>

                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                        </div>

                        <div class="tab-pane fade" id="bordered-pill3">


                    <table class="table datatable-basic table-bordered table-striped">
                        <thead>
                        <tr>
                            <th>Ime tipa demontirane opreme</th>
                            <th>Izmena</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($data['dismantled_subtype'] as $dismantled_subtype)

                            <tr>
                                <td>{{$dismantled_subtype['dismantled_eq_subtype_name']}}</td>
                                <td class="text-center">
                                    <a href="#" class="dropdown-item" data-toggle="modal" data-target="#modal_edit_dismantled_subtype" data-alldata="{{$dismantled_subtype}}"> <i class="icon-pencil7"></i> {{--Izmeni--}}</a>

                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>

                        </div>

                        <div class="tab-pane fade" id="bordered-pill4">

                    <table class="table datatable-basic table-bordered table-striped">
                        <thead>
                        <tr>
                            <th>Ime modela demonitrane opreme</th>
                            <th>Kôd demonitrane opreme</th>
                            <th>Izmena</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($data['dismantled_model'] as $dismantled_model)

                            <tr>
                                <td>{{$dismantled_model['dismantled_eq_model_name']}}</td>
                                <td>{{$dismantled_model['equipment_code']}}</td>
                                <td class="text-center">
                                    <a href="#" class="dropdown-item" data-toggle="modal" data-target="#modal_edit_dismantled_model" data-alldata="{{$dismantled_model}}"> <i class="icon-pencil7"></i> {{--Izmeni--}}</a>

                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>

                        </div>

                    </div>
                </div>
                <!-- /basic datatable -->

            </div>
        </div>
    </div>

@endsection



