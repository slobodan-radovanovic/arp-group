@extends('layouts.app')

@section('content')

    @push('style')

        <!-- Global stylesheets -->
        <link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet"
              type="text/css">
        <link href="../../../../global_assets/css/icons/icomoon/styles.css" rel="stylesheet" type="text/css">
        <link href="../assets/css/bootstrap.min.css" rel="stylesheet" type="text/css">
        <link href="../assets/css/bootstrap_limitless.min.css" rel="stylesheet" type="text/css">
        <link href="../assets/css/layout.min.css" rel="stylesheet" type="text/css">
        <link href="../assets/css/components.min.css" rel="stylesheet" type="text/css">
        <link href="../assets/css/colors.min.css" rel="stylesheet" type="text/css">
        <!-- /global stylesheets -->
    @endpush

    @push('scripts')
        <script src="../global_assets/js/plugins/tables/datatables/datatables.min.js"></script>
        <script src="../global_assets/js/demo_pages/datatables_basic.js"></script>
        <!-- Load select2 -->
        <script type="text/javascript" src="../global_assets/js/plugins/forms/selects/select2.min.js"></script>
        <script type="text/javascript" src="../global_assets/js/plugins/extensions/jquery_ui/interactions.min.js"></script>
        <script src="../assets/js/custom_select2.js"></script>
        <script src="../global_assets/js/plugins/forms/selects/select2.min.js"></script>
        {{--anytime--}}
        <script src="../global_assets/js/plugins/ui/moment/moment.min.js"></script>
        <script src="../global_assets/js/plugins/pickers/anytime.min.js"></script>
        <script src="../../../../global_assets/js/plugins/pickers/daterangepicker.js"></script>
        <script src="../assets/js/custom_datepicker.js"></script>

        <script src="../global_assets/js/main/jquery.min.js"></script>
        <script src="../global_assets/js/main/bootstrap.bundle.min.js"></script>
        <script src="../global_assets/js/plugins/loaders/blockui.min.js"></script>
        <script src="../global_assets/js/plugins/tables/datatables/datatables.min.js"></script>
        <script src="../global_assets/js/plugins/forms/selects/select2.min.js"></script>
        <script src="../global_assets/js/plugins/tables/datatables/extensions/jszip/jszip.min.js"></script>
        <script src="../global_assets/js/plugins/tables/datatables/extensions/buttons.min.js"></script>

        {{--export datatable--}}

        {{--anytime--}}
        <script src="../global_assets/js/plugins/ui/moment/moment.min.js"></script>
        <script src="../global_assets/js/plugins/pickers/anytime.min.js"></script>
        <script src="../../../../global_assets/js/plugins/pickers/daterangepicker.js"></script>
        <script src="../assets/js/custom_datepicker.js"></script>



        <script>

           /* $('.datatable-basic').DataTable({
                autoWidth: false,
                dom: '<"datatable-header"fBl><"datatable-scroll-wrap"t><"datatable-footer"ip>',
                language: {
                    search: '<span>Pretraga:</span> _INPUT_',
                    lengthMenu: '<span>Prikaz:</span> _MENU_',
                    info: '_START_ do _END_ od ukupno _TOTAL_',
                    paginate: {'first': 'First', 'last': 'Last', 'next': '→', 'previous': '←'}
                },
                buttons: {
                    buttons: [
                        {
                            extend: 'excelHtml5',
                            footer: true,
                            className: 'btn btn-light',
                            title: 'Razdužena oprema',
                            /!*title: 'Zaduzena oprema za tehničara ' + technician + ' ' + date_period,*!/
                            sheetName: 'Razdužena oprema',
                            exportOptions: {
                                columns: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
                            }
                        }
                    ]
                }
            });*/

           var today = new Date();
           var dd = String(today.getDate()).padStart(2, '0');
           var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
           var yyyy = today.getFullYear();

           today = dd + '-' + mm + '-' + yyyy;



           var table =  $('.datatable-basic').DataTable({
               autoWidth: false,
               processing: true,
               dom: '<"datatable-header"fBl><"datatable-scroll-wrap"t><"datatable-footer"ip>',
               ajax: {
                   url:"{{ route('razduzena_oprema') }}"
               },
               columns:[

                   {
                       data: 'equipment_code',
                       name: 'equipment_code'
                   },
                   {
                       data: 'equipment_subtype_name',
                       name: 'equipment_subtype_name'
                   },
                   {
                       data: 'equipment_model_name',
                       name: 'equipment_model_name'
                   },
                   {
                       data: 'equipment_serial1',
                       name: 'equipment_serial1'
                   },
                   {
                       data: 'equipment_serial2',
                       name: 'equipment_serial2'
                   },
                   {
                       data: 'equipment_serial3',
                       name: 'equipment_serial3'
                   },
                   {
                       data: 'technician_name',
                       name: 'technician_name'
                   },
                   {
                       data: 'discharged_by',
                       name: 'discharged_by'
                   },
                   {
                       data: 'equipment_assign_date',
                       name: 'equipment_assign_date'
                   },
                   {
                       data: 'created_at',
                       name: 'created_at'
                   }
               ],
               lengthMenu: [
                   [ 10, 25, 50, 100, -1 ],
                   [ '10', '25', '50', '100', 'Show all' ]
               ],
               language: {
                   search: '<span>Pretraga:</span> _INPUT_',
                   lengthMenu: '<span>Prikaz:</span> _MENU_',
                   info: '_START_ do _END_ od ukupno _TOTAL_',
                   paginate: {'first': 'First', 'last': 'Last', 'next': '→', 'previous': '←'}
               },
               buttons: {
                   buttons: [
                       {
                           extend: 'excelHtml5',
                           className: 'btn btn-light',
                           title: 'Pregled svih naloga '+today,
                           sheetName:'Pregled svih naloga',
                           exportOptions: {
                               columns: [ 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 ]
                           }
                       }
                   ]
               }
           });


        </script>
    @endpush

    <div>
        <div class="content">
            <div class="title m-b-md">
                <h1 style="text-align: center;">Razdužena oprema</h1>
            </div>

            <div class="card">
                <!-- Izbrisana oprema datatable -->

                <table class="table datatable-basic table-bordered table-striped">
                    <thead>
                    <tr>

                        <th>Kôd</th>
                        <th>Tip opreme</th>
                        <th>Model</th>
                        <th>S/N 1</th>
                        <th>S/N 2</th>
                        <th>S/N 3</th>
                        <th>Rezdužen</th>
                        <th>Razdužio</th>
                        <th>Vreme zaduživanja</th>
                        <th>Vreme razduživanja</th>

                    </tr>
                    </thead>
                    {{-- <tbody>
                     @foreach($discharged_equipment as $equipment)

                         <tr>
                             <td> {{ $equipment->equipment->models->equipment_code }}</td>
                             <td> {{ $equipment->equipment->subtype->equipment_subtype_name }}</td>
                             <td> {{ $equipment->equipment->models->equipment_model_name }}</td>
                             <td>{{ $equipment->equipment->equipment_serial1 }}</td>
                             <td>{{ $equipment->equipment->equipment_serial2 }}</td>
                             <td>{{ $equipment->equipment->equipment_serial3 }}</td>
                             <td>{{ $equipment->technician->technician_name }}</td>
                             <td>{{ $equipment->discharged_by }}</td>
                             <td>{{ datetimeForView($equipment->equipment_assign_date) }}</td>
                             <td>{{ datetimeForView($equipment->created_at) }}</td>

                         </tr>
                     @endforeach-

                    </tbody>--}}
                </table>


            </div>
        </div>
    </div>

@endsection



