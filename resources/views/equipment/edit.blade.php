@extends('layouts.app')
@push('scripts')


    <script src="../global_assets/js/plugins/ui/moment/moment.min.js"></script>
    <script src="../global_assets/js/plugins/pickers/anytime.min.js"></script>



    <script src="../assets/js/custom_datepicker.js"></script>


    <!-- Load select2 -->
    <script type="text/javascript" src="../global_assets/js/plugins/forms/selects/select2.min.js"></script>
    <script type="text/javascript" src="../global_assets/js/plugins/extensions/jquery_ui/interactions.min.js"></script>
    <script src="../assets/js/custom_select2.js"></script>

    <script src="../global_assets/js/plugins/tables/datatables/datatables.min.js"></script>
    <script src="../global_assets/js/demo_pages/datatables_basic.js"></script>
    <script src="../global_assets/js/plugins/forms/selects/select2.min.js"></script>


    <script>
        $('.datatable-basic').DataTable({
            autoWidth: false,
            dom: '<"datatable-header"fl><"datatable-scroll"t><"datatable-footer"ip>',
            bLengthChange: false, //thought this line could hide the LengthMenu
            bInfo: false,
            bPaginate: false,
            order: [],
            bSort: false,
            language: {
                search: '<span>Pretraga:</span> _INPUT_',
            }
        });


        var json_equipment_select = {!! $data['equipment_for_select'] !!};

        function filterEquipment(key, value) {

            return json_equipment_select.filter(function (equipment) {
                return equipment[key] == value;
            });
        } // filtriranje opreme na osnovu vrste, tipa ili modela


        $('#equipment_type_id').change(function () {
            var selected_type = $("#equipment_type_id option:selected").val(); // izabrana vrsta opreme (type)
            console.log(selected_type);
            var filtered_equipment = filterEquipment('equipment_type_id', selected_type); // filtriranje na osnovu izabrane vrste (type)
            var filtered_equipment_subtype = {};
            $.each(filtered_equipment, function () {
                filtered_equipment_subtype[this['equipment_subtype_id']] = this['equipment_subtype_name'];
            }); //lista tipova opreme koje pripadaju toj vrsti (subtype)
            console.log(filtered_equipment_subtype);
            var subtype_html = '<option value="" selected="selected"> ';
            $.each(filtered_equipment_subtype, function (key, value) {

                subtype_html += '<option value="' + key + '">' + value + ' ';
            }); //kreiranje option tagova

            $('#equipment_subtype_id').html(subtype_html); // ubacivanje filtriranih option tagova


            //ista procedura za model
            var filtered_equipment_model = {};
            $.each(filtered_equipment, function () {
                filtered_equipment_model[this['equipment_model_id']] = this['equipment_model_name'];
            });
            var model_html = '<option value="" selected="selected"> ';
            $.each(filtered_equipment_model, function (key, value) {
                if (key != 'null') {
                    model_html += '<option value="' + key + '">' + value + ' ';
                }
            });

            $('#equipment_model_id').html(model_html);
        });


        $('#equipment_subtype_id').change(function () {
            var selected_subtype = $("#equipment_subtype_id option:selected").val();
            var filtered_equipment = filterEquipment('equipment_subtype_id', selected_subtype);

            var equipment_type = filtered_equipment["0"].equipment_type_id;

            //equipment_type_id select
            $('#equipment_type_id option:selected').removeAttr("selected");
            $('#equipment_type_id option[value="' + equipment_type + '"]').attr('selected', 'selected');
            var selected_type_text = $("#equipment_type_id option:selected").text();
            $('#select2-equipment_type_id-container').prop('title', selected_type_text);
            $('#select2-equipment_type_id-container').prop('innerText', selected_type_text);


            //equipment_model_id select
            var filtered_equipment_model = {};
            $.each(filtered_equipment, function () {
                filtered_equipment_model[this['equipment_model_id']] = this['equipment_model_name'];
            });
            var model_html = '<option value="" selected="selected"> ';
            $.each(filtered_equipment_model, function (key, value) {
                if (key != 'null') {
                    model_html += '<option value="' + key + '">' + value + ' ';
                }
            });

            $('#equipment_model_id').html(model_html);

        });

        //equipment_model_id select
        $('#equipment_model_id').change(function () {
            var selected_model = $("#equipment_model_id option:selected").val();
            var filtered_equipment = filterEquipment('equipment_model_id', selected_model);
            var equipment_type = filtered_equipment["0"].equipment_type_id;
            var equipment_subtype = filtered_equipment["0"].equipment_subtype_id;

            //equipment_type_id select
            $('#equipment_type_id option:selected').removeAttr("selected");
            $('#equipment_type_id option[value="' + equipment_type + '"]').attr('selected', 'selected');
            var selected_type_text = $("#equipment_type_id option:selected").text();
            $('#select2-equipment_type_id-container').prop('title', selected_type_text);
            $('#select2-equipment_type_id-container').prop('innerText', selected_type_text);


            //equipment_subtype_id select
            $('#equipment_subtype_id option:selected').removeAttr("selected");
            $('#equipment_subtype_id option[value="' + equipment_subtype + '"]').attr('selected', 'selected');
            var selected_subtype_text = $("#equipment_subtype_id option:selected").text();
            $('#select2-equipment_subtype_id-container').prop('title', selected_subtype_text);
            $('#select2-equipment_subtype_id-container').prop('innerText', selected_subtype_text);

        });

        $("#equipment_serial1").focus();

    </script>




@endpush
@section('content')
    <div>
        <div class="content">
            <div class="title m-b-md">
                <h1 style="text-align: center;">Izmena opreme</h1>
            </div>

            @if ($errors->any())
                <div class="alert alert-danger">
                    @foreach ($errors->all() as $error)
                        <div>- {{ $error }}</div>
                    @endforeach
                </div>
            @endif

            <div class="card">

                <div class="card-body">


                    {!! Form::open(['action' => 'EquipmentController@store', 'method' => 'POST']) !!}
                    <div class="row">

                        {{--@dd(session('session_data'))--}}
                        @if(session('session_data'))

                            {{--<div class="col-2">
                                <div class="form-row mb-2">
                                    <label>Kôd opreme:</label><span class="text-danger">*</span>
                                    <input type="text" name='equipment_code' id='equipment_code'
                                           placeholder="Upišite kôd opreme" value="{{session('session_data')['5']}}"
                                           class="form-control" autocomplete="off">
                                </div>

                            </div>--}}


                            {{ Form::selectGroupSearch('equipment_type_id', true, 'Vrsta opreme:', null,
                                                          $data['type'], session('session_data')['0'],
                                                           ['data-placeholder' => 'Izaberite vrstu',
                                                           'class'=> 'form-control select-search equipment_type',
                                                           'data-fouc'], 'col-3 form-row mb-2 mr-1', true) }}

                            {{ Form::selectGroupSearch('equipment_subtype_id',
                                                        true,
                                                        'Tip opreme:',
                                                        null,
                                                        $data['subtype'], session('session_data')['1'],
                                                        ['data-placeholder' => 'Izaberite tip opreme',
                                                         'class'=> 'form-control select-search equipment_subtype',
                                                         'data-fouc'], 'col-3 form-row mb-2 mr-1', true) }}


                            {{ Form::selectGroupSearch('equipment_model_id',
                                                        true,
                                                        'Model:',
                                                        null,
                                                        $data['model'], session('session_data')['2'],
                                                        ['data-placeholder' => 'Izaberite model',
                                                         'class'=> 'form-control select-search equipment_model',
                                                         'data-fouc'], 'col-4 form-row mb-2') }}

                            <div class="col-2">
                                <div class="form-row mb-2">
                                    <label>Broj otpremnice:</label>
                                    <input type="text" name='equipment_dispatch_note' id='equipment_dispatch_note'
                                           placeholder="Upišite broj otpremnice" value="{{session('session_data')['4']}}"
                                           class="form-control" autocomplete="off">
                                </div>

                            </div>
                        @else
                            {{--<div class="col-2">
                                <div class="form-row mb-2">
                                    <label>Kôd opreme:</label><span class="text-danger">*</span>
                                    <input type="text" name='equipment_code' id='equipment_code'
                                           placeholder="Upišite kôd opreme"
                                           class="form-control" autocomplete="off" value="{{old('equipment_code')}}">
                                </div>

                            </div>--}}

                            {{ Form::selectGroupSearch('equipment_type_id', true, 'Vrsta opreme:', null,
                                                          $data['type'], null,
                                                           ['data-placeholder' => 'Izaberite vrstu',
                                                           'class'=> 'form-control select-search equipment_type',
                                                           'data-fouc'], 'col-3 form-row mb-2 mr-1', true) }}

                            {{ Form::selectGroupSearch('equipment_subtype_id',
                                                        true,
                                                        'Tip opreme:',
                                                        null,
                                                        $data['subtype'], null,
                                                        ['data-placeholder' => 'Izaberite tip opreme',
                                                         'class'=> 'form-control select-search equipment_subtype',
                                                         'data-fouc'], 'col-3 form-row mb-2 mr-1', true) }}


                            {{ Form::selectGroupSearch('equipment_model_id',
                                                        true,
                                                        'Model:',
                                                        null,
                                                        $data['model'], null,
                                                        ['data-placeholder' => 'Izaberite model',
                                                         'class'=> 'form-control select-search equipment_model',
                                                         'data-fouc'], 'col-4 form-row mb-2', true) }}

                            <div class="col-2">
                                <div class="form-row mb-2">
                                    <label>Broj otpremnice:</label>
                                    <input type="text" name='equipment_dispatch_note' id='equipment_dispatch_note'
                                           placeholder="Upišite broj otpremnice"
                                           class="form-control" autocomplete="off" value="{{old('equipment_dispatch_note')}}">
                                </div>

                            </div>
                        @endif

                    </div>
                    <div class="row">

                        @if(session('session_data'))
                            <div class="col-3">
                                <div class="form-row mb-2">
                                    {{--datetimeForPicker($data['order']->completed_at)-}}
                                    {{--vreme zatvaranja--}}
                                    {{ Form::datetime_picker('created_at', 'Datum i vreme unosa opreme:', null, session('session_data')['3'], 'form-control', ['placeholder' => 'Izaberi datum'], 'col-12', true) }}

                                </div>

                            </div>

                        @else
                            <div class="col-3">
                                <div class="form-row mb-2">
                                    {{--datetimeForPicker($data['order']->completed_at)-}}
                                    {{--vreme zatvaranja--}}
                                    {{ Form::datetime_picker('created_at', 'Datum i vreme unosa opreme:', null, null, 'form-control', ['placeholder' => 'Izaberi datum'], 'col-12', true) }}

                                </div>

                            </div>

                        @endif


                        <div class="col-3">
                            <div class="form-row mb-2">
                                <label>Serijski broj 1:</label><span class="text-danger">*</span>
                                <input type="text" name='equipment_serial1' id='equipment_serial1'
                                       placeholder="Upisite serijski broj"
                                       class="form-control" autocomplete="off" value="{{old('equipment_serial1')}}">
                            </div>

                        </div>



                        <div class="col-3">
                            <div class="form-row mb-2">
                                <label>Serijski broj 2:</label>
                                <input type="text" name='equipment_serial2' placeholder="Upisite serijski broj"
                                       class="form-control" autocomplete="off" value="{{old('equipment_serial2')}}">
                            </div>
                        </div>

                        <div class="col-3">
                            <div class="form-row mb-2">
                                <label>Serijski broj 3:</label>
                                <input type="text" name='equipment_serial3' placeholder="Upisite serijski broj"
                                       class="form-control" autocomplete="off" value="{{old('equipment_serial3')}}">
                            </div>

                        </div>

                    </div>

                    <p class="text-right">
                        <button type="submit" class="btn bg-teal-300 ">Unesi opremu <i class="icon-plus3 ml-1"></i>
                        </button>
                    </p>

                    {!! Form::close() !!}

                    <br>
                    <!-- Prikaz poslednje dodate opreme -->

                    <legend></legend>
                    <h3 class="text-center m-0 p-0 ">Poslednje unešena oprema</h3>
                    <table class="table datatable-basic table-bordered table-striped">
                        <thead>
                        <tr>
                            <th>Kôd</th>
                            <th>Vrsta opreme</th>
                            <th>Tip opreme</th>
                            <th>Model</th>
                            <th>S/N 1</th>
                            <th>S/N 2</th>
                            <th>S/N 3</th>
                            <th>Broj otpremnice</th>
                            <th>Izabrano vreme unosa</th>
                            <th>Vreme dodavanja</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($data['equipment'] as $equipment)

                            <tr>
                                <td>{{ $data['equipment_code'][$equipment->equipment_model_id] }}</td>
                                <td> {{ $data['type'][$equipment->equipment_type_id] }}</td>
                                <td> {{ $data['subtype'][$equipment->equipment_subtype_id] }}</td>
                                @if($equipment->equipment_model_id != null)
                                    <td> {{ $data['model'][$equipment->equipment_model_id] }}</td>
                                @else
                                    <td></td>
                                @endif
                                <td>{{ $equipment->equipment_serial1 }}</td>
                                <td>{{ $equipment->equipment_serial2 }}</td>
                                <td>{{ $equipment->equipment_serial3 }}</td>
                                <td>{{ $equipment->equipment_dispatch_note }}</td>
                                <td>{{ datetimeForView($equipment->created_at) }}</td>
                                <td>{{ datetimeForView($equipment->updated_at) }}</td>

                            </tr>
                        @endforeach

                        </tbody>
                    </table>

                    <!-- /Prikaz poslednje dodate opreme -->


                </div>
            </div>


        </div>
    </div>

@endsection



