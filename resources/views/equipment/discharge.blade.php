@extends('layouts.app')

@section('content')
    @push ('style')
        <!-- Global stylesheets -->
        <link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
        <link href="../../../../global_assets/css/icons/icomoon/styles.css" rel="stylesheet" type="text/css">
        <link href="../assets/css/bootstrap.min.css" rel="stylesheet" type="text/css">
        <link href="../assets/css/bootstrap_limitless.min.css" rel="stylesheet" type="text/css">
        <link href="../assets/css/layout.min.css" rel="stylesheet" type="text/css">
        <link href="../assets/css/components.min.css" rel="stylesheet" type="text/css">
        <link href="../assets/css/colors.min.css" rel="stylesheet" type="text/css">
        <!-- /global stylesheets -->

        <!-- Busy Load CSS -->
        <link href="https://cdn.jsdelivr.net/npm/busy-load/dist/app.min.css" rel="stylesheet">

    @endpush

    @push('scripts')
        <script type="text/javascript" src="../global_assets/js/plugins/forms/selects/select2.min.js"></script>
        <script type="text/javascript" src="../global_assets/js/plugins/extensions/jquery_ui/interactions.min.js"></script>
        <script src="../assets/js/custom_select2.js"></script>

        <script src="../global_assets/js/plugins/tables/datatables/datatables.min.js"></script>
        <script src="../global_assets/js/demo_pages/datatables_basic.js"></script>
        <script src="../global_assets/js/plugins/forms/selects/select2.min.js"></script>


        <script src="../global_assets/js/main/jquery.min.js"></script>
        <script src="../global_assets/js/main/bootstrap.bundle.min.js"></script>
        <script src="../global_assets/js/plugins/loaders/blockui.min.js"></script>
        <script src="../global_assets/js/plugins/tables/datatables/datatables.min.js"></script>
        <script src="../global_assets/js/plugins/forms/selects/select2.min.js"></script>
        <script src="../global_assets/js/plugins/tables/datatables/extensions/jszip/jszip.min.js"></script>
        <script src="../global_assets/js/plugins/tables/datatables/extensions/buttons.min.js"></script>

        <!-- Busy Load JS -->
        <script src="https://cdn.jsdelivr.net/npm/busy-load/dist/app.min.js"></script>


        <script>


            var today = new Date();
            var dd = String(today.getDate()).padStart(2, '0');
            var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
            var yyyy = today.getFullYear();

            today = dd + '-' + mm + '-' + yyyy;
            console.log(today);

            var table =  $('.datatable-basic1').DataTable({
                autoWidth: false,
                processing: true,
                /* serverSide: true,*/
                dom: '<"datatable-header"fBl><"datatable-scroll-wrap"t><"datatable-footer"ip>',

                ajax: {
                    url:"{{ route('oprema_kod_tehnicara') }}"
                },
                columns:[
                    {
                        data: 'equipment_code',
                        name: 'equipment_code'
                    },
                    {
                        data: 'equipment_subtype_name',
                        name: 'equipment_subtype_name'
                    },
                    {
                        data: 'equipment_model_name',
                        name: 'equipment_model_name'
                    },
                    {
                        data: 'equipment_serial1',
                        name: 'equipment_serial1'
                    },
                    {
                        data: 'equipment_serial2',
                        name: 'equipment_serial2'
                    },
                    {
                        data: 'equipment_serial3',
                        name: 'equipment_serial3'
                    },
                    {
                        data: 'technician_name',
                        name: 'technician_name'
                    },
                    {
                        data: 'action',
                        name: 'action',
                        orderable: false
                    }
                ],
                columnDefs: [
                    {
                        targets: [ 0, 1, 2, 3, 4, 5, 6 ],
                        searchable: true },
                    {
                        targets: -1,
                        data: null,
                        defaultContent:
                        '<button class="btn btn-outline-dark" data-toggle="modal" data-target="#modal_discharge"><i class="icon-minus-circle2"></i> Razduži</button>'
                    } ],
                language: {
                    search: '<span>Pretraga:</span> _INPUT_',
                    lengthMenu: '<span>Prikaz:</span> _MENU_',
                    info: '_START_ do _END_ od ukupno _TOTAL_',
                    paginate: {'first': 'First', 'last': 'Last', 'next': '→', 'previous': '←'}
                },
                lengthMenu: [
                    [ 10, 25, 50, 100, -1 ],
                    [ '10', '25', '50', '100', 'Show all' ]
                ],
                buttons: {
                    buttons: [
                        {
                            extend: 'excelHtml5',
                            className: 'btn btn-light',
                            title: 'Oprema (Oprema kod tehničara) '+today,
                            sheetName:'Oprema kod tehničara',
                            exportOptions: {
                                columns: [ 0, 1, 2, 3, 4, 5, 6 ],
                            }
                        }
                    ]
                }
            });


            $('.datatable-basic1 tbody').on( 'click', 'button', function () {

                data = table.row( $(this).parents('tr') ).data();
                /*console.log(data);
                alert( data['equipment_id'] );*/
            } );


            $('#modal_discharge').on('show.bs.modal', function (event) {
                var button = $(event.relatedTarget);
                var equipment = data;
                console.log(equipment);
                console.log(equipment.equipment_id);
                var modal = $(this);
                modal.find('.modal-body #equipment_id').val(equipment.equipment_id);
                /*modal.find('.modal-body #technician_name').text(technician.technician_name);*/
            });

            /* $('.dataTables_processing', $('.datatable-basic1').closest('.dataTables_wrapper')).show();*/



        </script>
    @endpush

    <div>
        <div class="content">
            <div class="title m-b-md">
                <h1 style="text-align: center;">Razduživanje opreme</h1>
            </div>

            @if ($errors->any())
                <div class="alert alert-danger">
                    @foreach ($errors->all() as $error)
                        <div>- {{ $error }}</div>
                    @endforeach
                </div>
            @endif

            <div>

                <!-- Delete modal -->
                <div id="modal_discharge" class="modal fade" tabindex="-1">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title">Razduživanje opreme</h5>
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                            </div>

                            <div class="modal-body">
                                {!! Form::open(['action' => ['EquipmentController@update', 'discharge'], 'method' => 'POST']) !!}
                                {{ csrf_field() }}
                                {{ method_field('PUT')}}
                                {{Form::hidden ('equipment_id', 'discharge', ['id' => 'equipment_id'])}}
                                <p>Da li ste sigurni da želite da razdužite ovu opremu {{--"<span id="technician_name"></span>"--}}?</p>

                               {{-- <table class="table table-bordered table-striped">
                                    <thead>
                                    <tr>
                                        <th>Kôd</th>
                                        <th>Tip opreme</th>
                                        <th>Model</th>
                                        <th>S/N 1</th>
                                        <th>S/N 2</th>
                                        <th>S/N 3</th>
                                        <th>Zadužen</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <th id="modal_equipment_code"></th>
                                        <th id="modal_equipment_subtype_name"></th>
                                        <th id="modal_equipment_model_name"></th>
                                        <th id="modal_equipment_serial1"></th>
                                        <th id="modal_equipment_serial2"></th>
                                        <th id="modal_equipment_serial3"></th>
                                        <th id="modal_technician_name"></th>
                                    </tr>
                                    </tbody>
                                </table>--}}
                            </div>

                            <div class="modal-footer">
                                <button type="button" class="btn btn-light" data-dismiss="modal">Odustani</button>
                                {!! Form::button('Razduži opremu', ['type' => 'submit', 'id' =>'button-delete-equipment', 'class' => 'btn btn-danger button-submit', 'data-loading-text' => 'Razduži opremu']) !!}
                            </div>
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
                <!-- /delete modal -->



                <!-- Bordered pills -->
                <div class="card">


                            <table class="table datatable-basic1 table-bordered table-striped">
                                <thead>
                                <tr>
                                    <th>Kôd</th>
                                    <th>Tip opreme</th>
                                    <th>Model</th>
                                    <th>S/N 1</th>
                                    <th>S/N 2</th>
                                    <th>S/N 3</th>
                                    <th>Zadužen</th>
                                    <th>Akcija</th>
                                </tr>
                                </thead>
                            </table>

                </div>


            </div>

            <div class="title m-b-md">
                <h1 style="text-align: center;">Grupno razduživanje opreme</h1>
            </div>

            {!! Form::open(['action' => ['EquipmentController@update', 'group_discharge'], 'method' => 'POST']) !!}
            {{ csrf_field() }}
            {{ method_field('PUT')}}
<div class="card p-3">
            <div class="row">

            {{ Form::selectGroupSearch('equipment_technician',
                                                   false,
                                                   null,
                                                   null,
                                                   $active_technicians, null,
                                                   ['data-placeholder' => 'Izaberite tehničara',
                                                    'class'=> 'form-control select-search equipment_model',
                                                    'data-fouc'], 'col-4 form-row mb-2', true) }}

                <p class="{{--text-right--}} col-4 form-row mb-2"><button type="submit" class="btn bg-teal-300"><i class="icon-minus-circle2 ml-1" ></i> Razaduži tehničara</button></p>


            </div>

            {!! Form::close() !!}
</div>


        </div>
    </div>

@endsection



