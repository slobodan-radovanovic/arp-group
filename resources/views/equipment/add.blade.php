@extends('layouts.app')
@push('scripts')


    <script src="../global_assets/js/plugins/ui/moment/moment.min.js"></script>
    <script src="../global_assets/js/plugins/pickers/anytime.min.js"></script>



    <script src="../assets/js/custom_datepicker.js"></script>


    <!-- Load select2 -->
    <script type="text/javascript" src="../global_assets/js/plugins/forms/selects/select2.min.js"></script>
    <script type="text/javascript" src="../global_assets/js/plugins/extensions/jquery_ui/interactions.min.js"></script>
    <script src="../assets/js/custom_select2.js"></script>


@endpush
@section('content')
    <div>
        <div class="content">
            <div class="title m-b-md">
                <h1 style="text-align: center;">Dodavanje tipa i modela opreme</h1>
            </div>

            @if ($errors->any())
                <div class="alert alert-danger">
                    @foreach ($errors->all() as $error)
                        <div>- {{ $error }}</div>
                    @endforeach
                </div>
            @endif

            <div class="card">

                <div class="card-body">
                    <legend class="font-weight-semibold"><i class="icon-plus-circle2 mr-2"></i> Unes novog tipa opreme</legend>

                    {!! Form::open(['action' => 'EquipmentController@store_subtype', 'method' => 'POST']) !!}
                    <div class="row">


                        {{ Form::selectGroupSearch('equipment_type_id', true, 'Vrsta opreme:', null,
                                      $equipment['type'], null,
                                       ['data-placeholder' => 'Izaberite vrstu',
                                       'class'=> 'form-control select-search equipment_type',
                                       'data-fouc'], 'col-4 form-row mb-2', true) }}

                        {{ Form::textGroup2('equipment_subtype_name', 'Tip opreme:', null, null, 'form-control', ['placeholder' => 'Upišite novi tip opreme', 'autocomplete' => 'off'], 'col-4', true) }}




                        <div class="col-4" style="margin-top: 28px; text-align: center;">
                            <button type="submit" class="btn bg-teal-300 ">Unesi novi tip opreme<i class="icon-plus3 ml-1" ></i></button>
                        </div>
                        {!! Form::close() !!}

                    </div>
                    <br><br>
                    <legend class="font-weight-semibold"><i class="icon-plus-circle2 mr-2"></i> Unes novog modela opreme</legend>

                    {!! Form::open(['action' => 'EquipmentController@store_model', 'method' => 'POST']) !!}
                    <div class="row">
                        {{ Form::selectGroupSearch('equipment_subtype_id', true, 'Tip opreme:',
                        null,$equipment['subtype'], null,
                                       ['data-placeholder' => 'Izaberite tip opreme',
                                       'class'=> 'form-control select-search equipment_type',
                                       'data-fouc'], 'col-3 form-row mb-2', true) }}

                        {{ Form::textGroup2('equipment_model_name', 'Model opreme:', null, null, 'form-control', ['placeholder' => 'Upišite novi model opreme', 'autocomplete' => 'off'], 'col-3', true) }}

                        {{ Form::textGroup2('equipment_code', 'Kôd opreme:', null, null, 'form-control', ['placeholder' => 'Upišite kôd opreme', 'autocomplete' => 'off'], 'col-2', true) }}


                        <div class="col-4" style="margin-top: 28px; text-align: center;">
                            <button type="submit" class="btn bg-teal-300 ">Unesi model opreme<i class="icon-plus3 ml-1" ></i></button>
                        </div>
                        {!! Form::close() !!}
                    </div>

                    <br><br>
                    <legend class="font-weight-semibold"><i class="icon-plus-circle2 mr-2"></i> Unes novog tipa opreme za  demontiranu opremu</legend>

                    {!! Form::open(['action' => 'EquipmentController@store_dismantled_subtype', 'method' => 'POST']) !!}
                    <div class="row">

                        {{ Form::textGroup2('dismantled_eq_subtype_name', 'Tip demontirane opreme:
:', null, null, 'form-control', ['placeholder' => 'Upišite novi tip opreme', 'autocomplete' => 'off'], 'col-4', true) }}

                        <div class="col-4" style="margin-top: 28px; text-align: center;">

                        </div>


                        <div class="col-4" style="margin-top: 28px; text-align: center;">
                            <button type="submit" class="btn bg-teal-300 ">Unesi novi tip opreme<i class="icon-plus3 ml-1" ></i></button>
                        </div>
                        {!! Form::close() !!}
                    </div>

                    <br><br>
                    <legend class="font-weight-semibold"><i class="icon-plus-circle2 mr-2"></i> Unes novog modela opreme za  demontiranu opremu</legend>

                    {!! Form::open(['action' => 'EquipmentController@store_dismantled_model', 'method' => 'POST']) !!}
                    <div class="row">

                        {{ Form::selectGroupSearch('dismantled_eq_subtype_id', true, 'Tip demontirane opreme:',
                        null,$equipment['dismantled_subtype'], null,
                                       ['data-placeholder' => 'Izaberite tip opreme',
                                       'class'=> 'form-control select-search equipment_type',
                                       'data-fouc'], 'col-3 form-row mb-2', true) }}

                        {{ Form::textGroup2('dismantled_eq_model_name', 'Model demontirane opreme:', null, null, 'form-control', ['placeholder' => 'Upišite novi model opreme', 'autocomplete' => 'off'], 'col-3', true) }}

                        {{ Form::textGroup2('equipment_code', 'Kôd demontirane opreme:', null, null, 'form-control', ['placeholder' => 'Upišite kôd opreme', 'autocomplete' => 'off'], 'col-2', true) }}


                        <div class="col-4" style="margin-top: 28px; text-align: center;">
                            <button type="submit" class="btn bg-teal-300 ">Unesi novi model opreme<i class="icon-plus3 ml-1" ></i></button>
                        </div>
                        {!! Form::close() !!}
                    </div>



                    <p class="text-right"></p>


                    {{-- AKTIVNOSTI NA NOVOJ OPREMI --}}
                </div>
            </div>


        </div>
    </div>

@endsection



