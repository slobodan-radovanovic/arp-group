@extends('layouts.app')

@section('content')

    @push('style')
        <style>
            .big-checkbox {
                width: 15px;
                height: 15px;
            }
        </style>
    @endpush
    @push('scripts')
        {{--<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
                integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
                crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
                integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
                crossorigin="anonymous"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
                integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
                crossorigin="anonymous"></script>

        <script src="../global_assets/js/plugins/tables/datatables/datatables.min.js"></script>
        <script src="../global_assets/js/demo_pages/datatables_basic.js"></script>
        <script src="../global_assets/js/plugins/forms/selects/select2.min.js"></script>

        --}}{{--anytime--}}{{--
        <script src="../global_assets/js/plugins/ui/moment/moment.min.js"></script>
        <script src="../global_assets/js/plugins/pickers/anytime.min.js"></script>
        <script src="../global_assets/js/plugins/pickers/daterangepicker.js"></script>

        <script src="../global_assets/js/plugins/forms/styling/uniform.min.js"></script>
        <script src="../global_assets/js/plugins/forms/styling/switchery.min.js"></script>
        <script src="../global_assets/js/plugins/forms/styling/switch.min.js"></script>


        <script src="../global_assets/js/demo_pages/form_checkboxes_radios.js"></script>

        <script src="../assets/js/custom_datepicker.js"></script>--}}


        <script src="../global_assets/js/plugins/tables/datatables/datatables.min.js"></script>
        <script src="../global_assets/js/demo_pages/datatables_basic.js"></script>
        <!-- Load select2 -->
        <script type="text/javascript" src="../global_assets/js/plugins/forms/selects/select2.min.js"></script>
        <script type="text/javascript" src="../global_assets/js/plugins/extensions/jquery_ui/interactions.min.js"></script>
        <script src="../assets/js/custom_select2.js"></script>
        <script src="../global_assets/js/plugins/forms/selects/select2.min.js"></script>
        {{--anytime--}}
        <script src="../global_assets/js/plugins/ui/moment/moment.min.js"></script>
        <script src="../global_assets/js/plugins/pickers/anytime.min.js"></script>
        <script src="../../../../global_assets/js/plugins/pickers/daterangepicker.js"></script>
        <script src="../assets/js/custom_datepicker.js"></script>

        <script src="../global_assets/js/main/jquery.min.js"></script>
        <script src="../global_assets/js/main/bootstrap.bundle.min.js"></script>
        <script src="../global_assets/js/plugins/loaders/blockui.min.js"></script>
        <script src="../global_assets/js/plugins/tables/datatables/datatables.min.js"></script>
        <script src="../global_assets/js/plugins/forms/selects/select2.min.js"></script>
        <script src="../global_assets/js/plugins/tables/datatables/extensions/jszip/jszip.min.js"></script>
        <script src="../global_assets/js/plugins/tables/datatables/extensions/buttons.min.js"></script>

        {{--export datatable--}}

        {{--anytime--}}
        <script src="../global_assets/js/plugins/ui/moment/moment.min.js"></script>
        <script src="../global_assets/js/plugins/pickers/anytime.min.js"></script>
        <script src="../../../../global_assets/js/plugins/pickers/daterangepicker.js"></script>
        <script src="../assets/js/custom_datepicker.js"></script>


        <script>

            var today = new Date();
            var dd = String(today.getDate()).padStart(2, '0');
            var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
            var yyyy = today.getFullYear();

            today = dd + '-' + mm + '-' + yyyy;


            $('.datatable-basic1').DataTable({
                autoWidth: false,
                dom: '<"datatable-header"fBl><"datatable-scroll-wrap"t><"datatable-footer"ip>',
                order: [6, "desc"],
                language: {
                    search: '<span>Pretraga:</span> _INPUT_',
                    lengthMenu: '<span>Prikaz:</span> _MENU_',
                    info: '_START_ do _END_ od ukupno _TOTAL_',
                    paginate: {'first': 'First', 'last': 'Last', 'next': '→', 'previous': '←'}
                },
                buttons: {
                    buttons: [
                        {
                            extend: 'excelHtml5',
                            className: 'btn btn-light',
                            title: 'Demontirana oprema u magacinu ' + today,
                            sheetName: 'Demontirana oprema u magacinu',
                            exportOptions: {
                                columns: [0, 1, 2, 3, 4, 5, 6]
                            }
                        }
                    ]
                }
            });

            $('.datatable-basic2').DataTable({
                autoWidth: false,
                dom: '<"datatable-header"fBl><"datatable-scroll-wrap"t><"datatable-footer"ip>',
                order: [6, "desc"],
                language: {
                    search: '<span>Pretraga:</span> _INPUT_',
                    lengthMenu: '<span>Prikaz:</span> _MENU_',
                    info: '_START_ do _END_ od ukupno _TOTAL_',
                    paginate: {'first': 'First', 'last': 'Last', 'next': '→', 'previous': '←'}
                },
                buttons: {
                    buttons: [
                        {
                            extend: 'excelHtml5',
                            className: 'btn btn-light',
                            title: 'Demontirana oprema vraćena SBB-u - ' + today,
                            sheetName: 'Demontirana oprema vraćena SBB-u',
                            exportOptions: {
                                columns: [0, 1, 2, 3, 4, 5, 6, 7]
                            }
                        }
                    ]
                }
            });

            $('.daterange-basic').daterangepicker({
                @if( empty($dismantled_date_filter))
                startDate: moment().subtract(1, 'month').startOf('month'),
                endDate: moment().subtract(1, 'month').endOf('month'),
                @endif
                maxDate: moment(),
                locale: {
                    format: 'DD/MM/YYYY'
                },
                applyClass: 'bg-teal-300',
                cancelClass: 'btn-light',

            });

            $('.daterange-basic2').daterangepicker({
                @if( empty($dismantled_discharged_date_filter))
                startDate: moment().subtract(1, 'month').startOf('month'),
                endDate: moment().subtract(1, 'month').endOf('month'),
                @endif
                maxDate: moment(),
                locale: {
                    format: 'DD/MM/YYYY'
                },
                applyClass: 'bg-teal-300',
                cancelClass: 'btn-light',

            });

            // hide all form-duration-divs
            /*$('.order_details').hide();*/
            @if( !empty($dismantled_date_filter))
            console.log('nije ovde');
            $(".daterangecheckbox2").show();
            $(".daterange2").hide();
            $(".daterangecheckbox").hide();
            $(".daterange").show();
            @endif

            @if( !empty($dismantled_discharged_date_filter))
            console.log('ovde je');
            $(".daterangecheckbox2").hide();
            $(".daterange2").show();
            $(".daterangecheckbox").show();
            $(".daterange").hide();
            @endif


            {{--@if( !empty($dismantled_discharge_date_filter))
                @dd($dismantled_discharge_date_filter)
            @endif--}}

            $(".daterangecheckbox").change(function () {
                $(".daterangecheckbox").hide(1000);
                $(".daterange").show();
            });

            $(".daterangecheckbox2").change(function () {
                $(".daterangecheckbox2").hide(1000);
                $(".daterange2").show();
            });
        </script>


    @endpush

    <div>
        <div class="content">
            <div class="title m-b-md">
                <h1 style="text-align: center;">Pregled demontirane opreme</h1>
            </div>

            <div>
                {{--<button type="button" class="btn bg-teal-300" data-toggle="modal" data-target="#modal_add">Dodavanje nove opreme <i class="icon-plus3 ml-1"></i></button>--}}


                <br>

                <!-- Basic datatable -->
                <div class="card">

                    <div class="card-body" style="border-bottom: 1px solid #bbbfc3;">
                        <ul class="nav nav-pills nav-pills-bordered" style="margin-bottom:0 !important;">
                            <li class="nav-item"><a href="#bordered-pill1" class="nav-link {{ isset($dismantled_discharged_date_filter) ? '' : 'active' }}" data-toggle="tab"
                                                    onclick="set_actitve_tab()">Demontirana oprema u magacinu</a></li>
                            <li class="nav-item"><a href="#bordered-pill2" class="nav-link {{ isset($dismantled_discharged_date_filter) ? 'active' : '' }}" data-toggle="tab"
                                                    onclick="set_actitve_tab()">Demontirana oprema vraćena SBB-u</a>
                            </li>
                        </ul>
                    </div>

                    <div class="tab-content">
                        <div class="tab-pane fade {{ isset($dismantled_discharged_date_filter) ? '' : 'show active' }}" id="bordered-pill1">

                            {{--@dd($dismantled_discharged_date_filter)--}}


                            <div class="card-header header-elements-inline">
                                {{-- {{dd($dismantled_date_filter)}}--}}
                                {!! Form::open(['action' => 'EquipmentController@dismantled', 'method' => 'POST']) !!}
                                <div class="form-group">
                                    <label>Filtriranje po datumu zatvaranja naloga za period: </label>
                                    <input type="checkbox" class="daterangecheckbox">
                                    <div class="input-group daterange" style="display: none">
                                        <span class="input-group-text"><i class="icon-calendar3"></i></span>
                                        <input name="dismantled_date_filter" style="width:200px" type="text"
                                               class="form-control daterange-basic" data-placeholder="Izaberite period"
                                               {{--value="20/03/2019 - 22/03/2019"--}}
                                               @if( ! empty($dismantled_date_filter))
                                               value="{{$dismantled_date_filter}}"
                                                @endif
                                        >
                                        <button type="submit" class="btn bg-teal-300 ml-3">Primeni filter</button>
                                    </div>
                                </div>

                                {!! Form::close() !!}
                                <br>
                                <a class="btn bg-teal-300" href="/equipment/dismantled">Prikaz sve demontirane
                                    opreme u magacinu</a>


                                {{-- <div class="row">
                                     <div class="col-md-6">
                                         <p><input type="text" class="form-control" id="rangeDemoStart" placeholder="Start date"></p>
                                     </div>

                                     <div class="col-md-6">
                                         <p><input type="text" class="form-control" id="rangeDemoFinish" placeholder="Finish date" disabled="disabled"></p>
                                     </div>
                                 </div>--}}

                            </div>







                            {!! Form::open(['action' => 'EquipmentController@dismantled_discharge', 'method' => 'POST']) !!}
                            <table class="table datatable-basic1 table-bordered table-striped">
                                <thead>
                                <tr>
                                    <th>Broj naloga</th>
                                    <th>Kod</th>
                                    <th>Tip opreme</th>
                                    <th>Model opreme</th>
                                    <th>S/N 1</th>
                                    <th>S/N 2</th>
                                    <th>Vreme zatvaranja naloga</th>
                                    <th>Razduživanje</th>

                                </tr>
                                </thead>
                                <tbody>

                                @foreach($removed_equipment as $equipment)
{{--@dd($equipment)--}}
                                    <tr>
                                        <td>{{ $equipment->ordinal_number }}</td>
                                       {{-- <td>{{ $equipment->removed_equipment_subtype }}</td>--}}
                                        <td>{{ $equipment->equipment_code }}</td>
                                        <td>{{ $equipment->dismantled_eq_subtype_name }}</td>
                                        <td>{{ $equipment->dismantled_eq_model_name }}</td>
                                        <td>{{ $equipment->removed_equipment_serial1 }}</td>
                                        <td>{{ $equipment->removed_equipment_serial2 }}</td>
                                        <td>{{ datetimeForView($equipment->created_at) }}</td>
                                        <td>
                                            <input class="big-checkbox" style="margin:0 auto;display: block;"
                                                   type='checkbox' name='razduzivanje[]'
                                                   value='{{ $equipment->removed_equipment_id }}'/>
                                        </td>

                                    </tr>
                                @endforeach

                                </tbody>
                                <tfoot>
                                <tr>


                                    <td colspan="7"></td>
                                    <td><input class="btn bg-teal-300" style="margin:0 auto;display: block;"
                                               type="submit" value="Razduži"></td>

                                </tr>
                                </tfoot>
                            </table>
                            {!! Form::close() !!}
                        </div>

                        <div class="tab-pane fade {{ isset($dismantled_discharged_date_filter) ? 'show active' : '' }}" id="bordered-pill2">

                            <div class="card-header header-elements-inline">
                                {{-- {{dd($dismantled_date_filter)}}--}}
                                {!! Form::open(['action' => 'EquipmentController@dismantled', 'method' => 'POST']) !!}
                                <div class="form-group">
                                    <label>Filtriranje po datumu razduživanja za period: </label>
                                    <input type="checkbox" class="daterangecheckbox2">
                                    <div class="input-group daterange2" style="display: none">
                                        <span class="input-group-text"><i class="icon-calendar3"></i></span>
                                        <input name="dismantled_discharged_date_filter" style="width:200px" type="text"
                                               class="form-control daterange-basic2" data-placeholder="Izaberite period"
                                               {{--value="20/03/2019 - 22/03/2019"--}}
                                               @if( ! empty($dismantled_discharged_date_filter))
                                               value="{{$dismantled_discharged_date_filter}}"
                                                @endif
                                        >
                                        <button type="submit" class="btn bg-teal-300 ml-3">Primeni filter</button>
                                    </div>
                                </div>

                                {!! Form::close() !!}
                                <br>
                                <a class="btn bg-teal-300" href="/equipment/dismantled">Prikaz sve vraćene
                                    opreme</a>


                            </div>
                            {!! Form::open(['action' => 'EquipmentController@dismantled_discharge', 'method' => 'POST']) !!}
                            <table class="table datatable-basic2 table-bordered table-striped">
                                <thead>
                                <tr>
                                    <th>Broj naloga</th>
                                    <th>Kod</th>
                                    <th>Tip opreme</th>
                                    <th>Model opreme</th>
                                    <th>S/N 1</th>
                                    <th>S/N 2</th>
                                    <th>Vreme zatvaranja naloga</th>
                                    <th>Vreme razduživanja</th>

                                </tr>
                                </thead>
                                <tbody>

                                @foreach($removed_equipment_discharged as $equipment)

                                    <tr>
                                        <td>{{ $equipment->ordinal_number }}</td>
                                        <td>{{ $equipment->equipment_code }}</td>
                                        <td>{{ $equipment->dismantled_eq_subtype_name }}</td>
                                        <td>{{ $equipment->dismantled_eq_model_name }}</td>
                                        <td>{{ $equipment->removed_equipment_serial1 }}</td>
                                        <td>{{ $equipment->removed_equipment_serial2 }}</td>
                                        <td>{{ datetimeForView($equipment->created_at) }}</td>
                                        <td>{{ datetimeForView($equipment->updated_at) }}</td>
                                        {{--<td>{{ $equipment->created_at }}</td>
                                        <td>{{ $equipment->updated_at }}</td>--}}


                                    </tr>
                                @endforeach

                                </tbody>
                            </table>
                            {!! Form::close() !!}
                        </div>


                    </div>
                </div>
                <!-- /basic datatable -->

            </div>
        </div>
    </div>

@endsection



