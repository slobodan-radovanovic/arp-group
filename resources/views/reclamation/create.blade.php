@extends('layouts.app')
@push('scripts')
11

    <script src="../global_assets/js/plugins/ui/moment/moment.min.js"></script>
    <script src="../global_assets/js/plugins/pickers/anytime.min.js"></script>
    <script src="../assets/js/custom_datepicker.js"></script>

    <!-- Load select2 -->
    <script type="text/javascript" src="../global_assets/js/plugins/forms/selects/select2.min.js"></script>
    <script type="text/javascript" src="../global_assets/js/plugins/extensions/jquery_ui/interactions.min.js"></script>
    <script src="../assets/js/custom_select2.js"></script>

    <script>
        var json_equipment_select = {!! $data['equipment_for_select'] !!};
        var json_all_serials = {!! json_encode($data['all_serials']) !!};

        function filterEquipment(key, value) {
            return json_equipment_select.filter(function (equipment) {
                return equipment[key] == value;
            });
        } // filtriranje opreme na osnovu tipa (subtype)

        function selectSerial(serial, subtype) {
            var selected_subtype = $('#' + subtype + ' option:selected').val();
            var filtered_equipment = filterEquipment('equipment_subtype_id', selected_subtype);
            //equipment_serial select
            var filtered_equipment_serial = {};
            $.each(filtered_equipment, function (index) {
                filtered_equipment_serial[index] = this['equipment_id'];
            });
            var serial_html = '<option value="" selected="selected"></option>';
            $.each(json_all_serials, function (serial_key, serial_value) {
                $.each(filtered_equipment_serial, function (key, equipment_id) {
                    var slit_key = serial_key.split("_");
                    if (slit_key['1'] == equipment_id) {
                        serial_html += '<option value="' + serial_key + '">' + serial_value + '</option>';
                    }
                });
            });
            $('#' + serial).html(serial_html);
        }

        $('#equipment_subtype_1').change(function () {
            selectSerial('equipment_serial_1', 'equipment_subtype_1');
        });

        $('#equipment_subtype_2').change(function () {
            selectSerial('equipment_serial_2', 'equipment_subtype_2');
        });

        $('#equipment_subtype_3').change(function () {
            selectSerial('equipment_serial_3', 'equipment_subtype_3');
        });

        $('#equipment_subtype_4').change(function () {
            selectSerial('equipment_serial_4', 'equipment_subtype_4');
        });

        $('#equipment_subtype_5').change(function () {
            selectSerial('equipment_serial_5', 'equipment_subtype_5');
        });

        function selectSubtype(serial, subtype) {
            var equipment_serial = $('#' + serial + ' option:selected').val();
            var slit_equipment_serial = equipment_serial.split("_");
            var equipment_id = slit_equipment_serial['1'];
            var filtered_equipment = filterEquipment('equipment_id', equipment_id);
            var equipment_subtype = filtered_equipment["0"].equipment_subtype_id;
            //equipment_subtype_1 select
            $('#' + subtype + ' option:selected').removeAttr("selected");
            $('#' + subtype + ' option[value="' + equipment_subtype + '"]').attr('selected', 'selected');
            var selected_subtype_text = $('#' + subtype + ' option:selected').text();
            $('#select2-' + subtype + '-container').prop('title', selected_subtype_text);
            $('#select2-' + subtype + '-container').prop('innerText', selected_subtype_text);
        } // selektovanje tipa na osnovu serijskog

        $('#equipment_serial_1').change(function () {
            selectSubtype('equipment_serial_1', 'equipment_subtype_1');
        });

        $('#equipment_serial_2').change(function () {
            selectSubtype('equipment_serial_2', 'equipment_subtype_2');
        });

        $('#equipment_serial_3').change(function () {
            selectSubtype('equipment_serial_3', 'equipment_subtype_3');
        });

        $('#equipment_serial_4').change(function () {
            selectSubtype('equipment_serial_4', 'equipment_subtype_4');
        });

        $('#equipment_serial_5').change(function () {
            selectSubtype('equipment_serial_5', 'equipment_subtype_5');
        });

        function notDefaultSelect(id) {
            var wares_id = $('#not_default_select_' + id + ' option:selected').val();
            // var slit_wares_value = wares_value.split("_");
            //var wares_id = slit_wares_value['1'];
            $('#not_default_input_' + id).attr('name', wares_id);
            console.log('notDefaultSelect is changed ' + id);
            console.log('wares id is ' + wares_id);
            console.log('not_default_input_' + id + 'name is ' + $('#not_default_input_' + id).attr('name'));
        }


        $('body').on('focus', 'input[type=number]', function (e) {
            $(this).on('wheel.disableScroll', function (e) {
                e.preventDefault()
            });
        });

        $('body').on('blur', 'input[type=number]', function (e) {
            $(this).off('wheel.disableScroll');
        });

        $("#service_type").change(function () {

            var reclamation_type = $("#service_type option:selected").val();

            switch(reclamation_type) {
                case 'reklamacija':
                    $('.reklamacija_po_stavkama').addClass('d-none');
                    $('.multiplication_number').addClass('d-none');
                    break;
                case 'reklamacija_po_stavkama':
                    $('.multiplication_number').addClass('d-none');
                    $('.reklamacija_po_stavkama').removeClass('d-none');
                    break;
                case 'izgradnja_po_etazi':
                    $('.reklamacija_po_stavkama').addClass('d-none');
                    $('.multiplication_number').removeClass('d-none');
                    $('label[for=multiplication_number]').text('Broj etaža');
                    $('#multiplication_number').attr('placeholder', 'Broj etaža');
                    break;
                case 'rekonstrukcija_korisnika':
                    $('.reklamacija_po_stavkama').addClass('d-none');
                    $('.multiplication_number').removeClass('d-none');
                    $('label[for=multiplication_number]').text('Broj korisnika');
                    $('#multiplication_number').attr('placeholder', 'Broj korisnika');
                    break;
                case 'izgradnja_po_metru':
                    $('.reklamacija_po_stavkama').addClass('d-none');
                    $('.multiplication_number').removeClass('d-none');
                    $('label[for=multiplication_number]').text('Broj metara');
                    $('#multiplication_number').attr('placeholder', 'Broj metara');

                    break;
                default:
                // code block
            }



            /*$('.'+reclamation_type).removeClass('d-none');
            $('.izgradnja_po_etazi').removeAttr('disabled');*/
            console.log(reclamation_type);
        });

    </script>




@endpush
@section('content')
    <div class="title m-b-md mt-3">
        <h1 style="text-align: center;">Kreiranje reklamacionog naloga</h1>
    </div>

    @if ($errors->any())

        <div class="alert alert-danger">
            @foreach ($errors->all() as $error)
                <div>- {{ $error }}</div>
            @endforeach
        </div>
    @endif

    <div class="card mt-1">

        <div class="card-header header-elements-inline">
            <h4 class="card-title">Osnovni podaci naloga</h4>

            <div class="header-elements">
                <div class="list-icons">
                    <a class="list-icons-item" data-action="collapse"></a>
                </div>
            </div>
        </div>

        <div class="card-body">
            {!! Form::open(['action' => 'ReclamationController@store', 'method' => 'POST']) !!}
            <div class="row">

                {{-- LEVA STRANA --}}
                <div class="col-12">
                    <fieldset>
                        <legend class="font-weight-semibold"><i class="icon-reading mr-2"></i>Podaci o kupcu</legend>

                        {{-- KUPAC --}}
                        {{-- U redu tri inputa --}}

                       {{-- @dd(newReclamationOrdinalNumber())--}}
                        <div class="row">
                            <p style="font-size: 14px" class="ml-2 mb-3">Broj naloga: <span
                                        style="font-weight: bold;">{{newReclamationOrdinalNumber()}}</span></p>
                            <br>
                        </div>

                        <div class="row">

                            {{ Form::selectGroupSearch('service_type', true ,'Tip reklamacije:', null,
                           $data['services'], null,
                            ['data-placeholder' => 'Odaberite tip posla',
                            'class'=> 'form-control select-search'], 'form-group col-4', true) }}

                            {{ Form::numberGroup('price_fixed', 'Ukupna cena za tehnicara/tehnicare:', null, null, 'form-control', ['placeholder' => 'Cena za tehnicara:', 'min' => '1',  'step' => '1'], 'col-4 reklamacija_po_stavkama d-none', true) }}

                            {{ Form::numberGroup('for_invoicing', 'Ukupna cena za fakturaisanje:', null, null, 'form-control', ['placeholder' => 'Cena za fakturaisanje:', 'min' => '1',  'step' => '1'], 'col-4 reklamacija_po_stavkama d-none', true) }}

                            {{ Form::numberGroup('multiplication_number', 'Broj etaža:', null, null, 'form-control', ['placeholder' => 'Broj etaža:', 'min' => '1',  'step' => '1'], 'col-4 d-none multiplication_number', true) }}

                        </div>


                        <div class="row">
                            {{ Form::textGroup2('order_number', 'SR broj naloga:', null, null, 'form-control', ['placeholder' => 'SR broj naloga:'], 'col-4', true) }}
                        </div>

                        <div class="row">

                            {{ Form::textGroup2('buyer_id', 'Broj kupca:', null, null, 'form-control', ['placeholder' => 'Broj kupca'], 'col-3') }}

                            {{ Form::textGroup2('treaty_id', 'Broj ugovora:', null, null, 'form-control', ['placeholder' => 'Broj ugovora'], 'col-3') }}


                            {{ Form::textGroup2('buyer', 'Ime i prezime kupca:', null, null, 'form-control', ['placeholder' => 'Ime i prezime kupca'], 'col-6') }}

                        </div>


                        {{-- KUPAC --}}

                        {{-- POST GRAD Opština --}}
                        {{-- U redu tri inputa sa selectom--}}
                        <div class="row">

                            {{ Form::textGroup2('area_code', 'Poštanski broj:', null, null, 'form-control', ['placeholder' => 'Poštanski broj'], 'col-3') }}


                            {{--{{ Form::textGroup2('city', 'Grad:', null, 'Beograd', 'form-control', ['readonly'], 'col-3') }}--}}


                            {{ Form::selectGroupSearch('city', true, 'Grad:', null,
              $data['city'], 'Beograd',
                 ['data-placeholder' => 'Odaberite grad',
                 'class'=> 'form-control select-search city'], 'col-3', true) }}


                            {{ Form::selectGroupSearch('region', true, 'Region:', null,
              $data['region'], null,
                 ['data-placeholder' => 'Odaberite region',
                 'class'=> 'form-control select-search region'], 'col-3') }}


                            {{ Form::selectGroupSearch('township', true, 'Odaberite opštinu:', null,
                            $data['township'], null,
                             ['data-placeholder' => 'Odaberite opštinu',
                             'class'=> 'form-control select-search township'], 'col-3') }}

                        </div>
                        {{-- POST GRAD Opština --}}

                        {{-- ADRESA --}}
                        {{-- U redu cetiri inputa --}}
                        <div class="row">

                            {{ Form::textGroup2('address', 'Adresa:', null, null, 'form-control', ['placeholder' => 'Naziv ulice'], 'col-6', true) }}

                            {{ Form::textGroup2('address_number', 'broj:', null, null, 'form-control', ['placeholder' => 'broj'], 'col-2', true) }}

                            {{ Form::textGroup2('floor', 'sprat:', null, null, 'form-control', ['placeholder' => 'sprat'], 'col-2') }}

                            {{ Form::textGroup2('apartment', 'stan:', null, null, 'form-control', ['placeholder' => 'stan'], 'col-2') }}

                        </div>
                        {{-- /ADRESA --}}

                        {{-- TELEFONI --}}
                        {{-- U redu dva inputa --}}
                        <div class="row">

                            {{ Form::textGroup2('phone_number', 'Broj telefona:', null, null, 'form-control', ['placeholder' => 'Broj telefona'], 'col-6') }}

                            {{ Form::textGroup2('mobile_number', 'Mobilni:', null, null, 'form-control', ['placeholder' => 'Mobilni'], 'col-6') }}

                        </div>
                        {{-- /TELEFONI --}}


                    </fieldset>
                </div>

                {{-- BEZ DESNE STRANA --}}

            </div>

        </div>
    </div>

    <div class="card">

        <div class="card-header header-elements-inline">
            <h4 class="card-title">Ugradjena oprema i materijal</h4>
            <div class="header-elements">
                <div class="list-icons">
                    <a class="list-icons-item" data-action="collapse"></a>
                </div>
            </div>
        </div>

        <div class="card-body">
            <div class="row">

                {{-- CELA STRANA --}}
                <div class="col-md-12">
                    <fieldset>
                        <legend class="font-weight-semibold"><i class="{{-- NASE IKONICE --}}"></i> Naziv opreme
                            izdate na korišćenje
                        </legend>

                        {{-- AKTIVNOSTI NA NOVOJ OPREMI --}}
                        {{-- U redu tri SELECTA sa pet redova --}}
                        <div class="mb-1">


                            <div class="row">

                                {{ Form::selectGroupSearch('equipment_subtype_1',
                                                true,
                                                'Tip opreme:',
                                                null,
                                                $data['subtype'], null,
                                                ['data-placeholder' => 'Izaberite tip opreme',
                                                 'class'=> 'form-control select-search equipment_subtype',
                                                  ], 'col-6 form-row mb-2 mr-1') }}




                                {{ Form::selectGroupSearch('equipment_serial_1',
                                                                true,
                                                                'Serijski broj opreme:',
                                                                null,
                                                                $data['all_serials'], null,
                                                                ['data-placeholder' => 'Izaberite serijski broj',
                                                                 'class'=> 'form-control select-search equipment_model',
                                                                  ], 'col-6 form-row mb-2') }}

                            </div>

                            <div class="row">

                                {{ Form::selectGroupSearch('equipment_subtype_2',
                                                false,
                                                null,
                                                null,
                                                $data['subtype'], null,
                                                ['data-placeholder' => 'Izaberite tip opreme',
                                                'id' => 'equipment_subtype_2',
                                                 'class'=> 'form-control select-search equipment_subtype',
                                                  ], 'col-6 form-row mb-2 mr-1') }}




                                {{ Form::selectGroupSearch('equipment_serial_2',
                                                                false,
                                                                null,
                                                                null,
                                                                $data['all_serials'], null,
                                                                ['data-placeholder' => 'Izaberite serijski broj',
                                                                'id' => 'equipment_serial_2',
                                                                 'class'=> 'form-control select-search',
                                                                  ], 'col-6 form-row mb-2') }}

                            </div>

                            <div class="row">

                                {{ Form::selectGroupSearch('equipment_subtype_3',
                                                false,
                                                null,
                                                null,
                                                $data['subtype'], null,
                                                ['data-placeholder' => 'Izaberite tip opreme',
                                                'id' => 'equipment_subtype_3',
                                                 'class'=> 'form-control select-search equipment_subtype',
                                                  ], 'col-6 form-row mb-2 mr-1') }}




                                {{ Form::selectGroupSearch('equipment_serial_3',
                                                                false,
                                                                null,
                                                                null,
                                                                $data['all_serials'], null,
                                                                ['data-placeholder' => 'Izaberite serijski broj',
                                                                'id' => 'equipment_serial_3',
                                                                 'class'=> 'form-control select-search',
                                                                  ], 'col-6 form-row mb-2') }}

                            </div>

                        </div>
                        {{--/ AKTIVNOSTI NA NOVOJ OPREMI --}}


                        {{-- UTROSENI MATERIJAL --}}
                        <legend class="font-weight-semibold"><i class="{{-- NASE IKONICE --}}"></i> Utroseni
                            materijal
                        </legend>
                        <div class="row">

                            <div class="col-6">
                                <table class="table datatable-basic table-bordered table-striped">
                                    <thead>
                                    <tr>
                                        <th>Šifra i naziv materijala</th>
                                        <th>Količina</th>
                                    </tr>
                                    </thead>
                                    {{--@dd($default_wares)--}}
                                    {{--{{ default wares }}--}}
                                    <tbody>
                                    @foreach($data['default_wares'] as $wares)
                                        <tr>
                                            <td class="lesspadding">{{ $wares->wares_code }}
                                                - {{ $wares->wares_name }}</td>
                                            <td style="width: 140px" class="lesspadding"><input
                                                        style="width: 40px" type="number" min="1" step="1"
                                                        name="wares_{{ $wares->wares_id }}"> {{ $wares->wares_type }}
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                    {{--{{ /$default wares }}--}}
                                </table>

                            </div>

                            <div class="col-6">

                                <table class="table datatable-basic table-bordered table-striped">
                                    <thead>
                                    <tr>
                                        <th>Šifra i naziv materijala</th>
                                        <th>Količina</th>
                                    </tr>
                                    </thead>
                                    {{--@dd($default_wares)--}}
                                    {{--{{ default wares }}--}}
                                    <tbody>
                                    @for($i = 1; $i <= 20; $i++)
                                        {{--@foreach($data['default_wares'] as $wares)--}}
                                        <tr>
                                            <td class="lesspadding">{{ Form::selectGroupSearch('not_default_select_'.$i, false, null, null,
                                                             $data['wares'], null,
                                                                 ['data-placeholder' => 'Odaberite materijal',
                                                                 'id' => 'not_default_select_'.$i,
                                                                 'data-id'=> $i,
                                                                 'class'=> 'form-control select-search',                                                                                          'onchange'=> 'notDefaultSelect('.$i.')',
                                                                  ], 'col-12') }}</td>
                                            <td style="width: 140px" class="lesspadding">
                                                <input id="not_default_input_{{ $i }}" style="width: 40px"
                                                       type="number" min="1" step="1" name="not_default_input_{{ $i }}">
                                            </td>
                                        </tr>
                                    @endfor
                                    </tbody>
                                    {{--{{ /$default wares }}--}}
                                </table>


                            </div>

                        </div>
                        {{-- /UTROSENI MATERIJAL --}}

                        {{-- TEHNICARI --}}
                        <legend class="font-weight-semibold"><i class="{{-- NASE IKONICE --}}"></i> Izbor tehnčara
                        </legend>
                        <div class="row mb-3">
                            {{--Prvi tehnicar--}}
                            {{ Form::selectGroupSearch('technician_1',
                                        true,
                                        'Prvi tehničar:',
                                        null,
                                        $data['technicians'], null,
                                        ['data-placeholder' => 'Izaberite prvog tehničara',
                                         'class'=> 'form-control select-search technician_1',
                                         'data-focus'], 'col-6 form-row mb-2', true) }}

                            {{--Drugi tehnicar--}}
                            {{ Form::selectGroupSearch('technician_2',
                                        true,
                                        'Drugi tehničar:',
                                        null,
                                        $data['technicians'], null,
                                        ['data-placeholder' => 'Izaberite drugog tehničara',
                                         'class'=> 'form-control select-search technician_2',
                                         'data-focus'], 'col-6 form-row mb-2') }}
                        </div>

                        {{-- /TEHNICARI --}}

                        {{--Privatno vozilo--}}
                        <legend class="font-weight-semibold"><i class="{{-- NASE IKONICE --}}"></i> Privatno vozilo
                        </legend>
                        <div class="row mb-3">

                            <div class=" col-6">
                                <div class="form-check form-check-inline form-check-right">
                                    <label class="form-check-label">
                                        Tehničar koristio privatno vozilo
                                        <input type="checkbox" class="form-check-input" name="private_vehicle"
                                               value="1">
                                    </label>
                                </div>
                            </div>

                        </div>

                        {{-- vreme zatvaranja --}}
                        <legend class="font-weight-semibold"><i class="{{-- NASE IKONICE --}}"></i> Vreme zavrsavanja naloga</legend>
                        <div class="row mb-3">
                            {{--vreme zatvaranja--}}
                            {{ Form::datetime_picker('completed_at', 'Datum i vreme zatvaranja naloga:', null, null, 'form-control', ['placeholder' => 'Izaberi datum'], 'col-4', true) }}

                            <div class="col-8"></div>
                        </div>

                        {{-- /vreme zatvaranja --}}

                        {{-- KOMENTAR --}}
                        <legend class="font-weight-semibold"><i class="{{-- NASE IKONICE --}}"></i> Komentar
                        </legend>
                        <div class="form-group">
                                    <textarea rows="5" cols="5" id="order_comment" name="order_comment"
                                              class="form-control"
                                              placeholder="Upisite dodatne komentare"></textarea>
                        </div>
                        {{-- KOMENTAR --}}

                    </fieldset>
                </div>
            </div>

            <div class="text-right">
                <button type="submit" class="btn bg-teal-300">Kreiraj nalog <i class="icon-paperplane ml-2"></i>
                </button>
            </div>

            </form>
        </div>
    </div>

@endsection



