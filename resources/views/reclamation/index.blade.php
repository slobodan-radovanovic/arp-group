@extends('layouts.app')
@push('style')
    <style>
        .nav-pills-bordered .nav-link.active {
            border-color: #4db6ac !important;
        }

        .nav-pills .nav-link.active, .nav-pills .show > .nav-link {
            color: #fff;
            background-color: #4db6ac !important;
        }


    </style>

    <!-- Global stylesheets -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
    <link href="../../../../global_assets/css/icons/icomoon/styles.css" rel="stylesheet" type="text/css">
    <link href="../assets/css/bootstrap.min.css" rel="stylesheet" type="text/css">
    <link href="../assets/css/bootstrap_limitless.min.css" rel="stylesheet" type="text/css">
    <link href="../assets/css/layout.min.css" rel="stylesheet" type="text/css">
    <link href="../assets/css/components.min.css" rel="stylesheet" type="text/css">
    <link href="../assets/css/colors.min.css" rel="stylesheet" type="text/css">
    <!-- /global stylesheets -->
@endpush
@push('scripts')
    <script src="../global_assets/js/plugins/tables/datatables/datatables.min.js"></script>
    <script src="../global_assets/js/demo_pages/datatables_basic.js"></script>
    <!-- Load select2 -->
    <script type="text/javascript" src="../global_assets/js/plugins/forms/selects/select2.min.js"></script>
    <script type="text/javascript" src="../global_assets/js/plugins/extensions/jquery_ui/interactions.min.js"></script>
    <script src="../assets/js/custom_select2.js"></script>
    <script src="../global_assets/js/plugins/forms/selects/select2.min.js"></script>
    {{--anytime--}}
    <script src="../global_assets/js/plugins/ui/moment/moment.min.js"></script>
    <script src="../global_assets/js/plugins/pickers/anytime.min.js"></script>
    <script src="../../../../global_assets/js/plugins/pickers/daterangepicker.js"></script>
    <script src="../assets/js/custom_datepicker.js"></script>

    <script src="../global_assets/js/main/jquery.min.js"></script>
    <script src="../global_assets/js/main/bootstrap.bundle.min.js"></script>
    <script src="../global_assets/js/plugins/loaders/blockui.min.js"></script>
    <script src="../global_assets/js/plugins/tables/datatables/datatables.min.js"></script>
    <script src="../global_assets/js/plugins/forms/selects/select2.min.js"></script>
    <script src="../global_assets/js/plugins/tables/datatables/extensions/jszip/jszip.min.js"></script>
    <script src="../global_assets/js/plugins/tables/datatables/extensions/buttons.min.js"></script>

    {{--export datatable--}}

    {{--anytime--}}
    <script src="../global_assets/js/plugins/ui/moment/moment.min.js"></script>
    <script src="../global_assets/js/plugins/pickers/anytime.min.js"></script>
    <script src="../../../../global_assets/js/plugins/pickers/daterangepicker.js"></script>
    <script src="../assets/js/custom_datepicker.js"></script>



    <script>

        var today = new Date();
        var dd = String(today.getDate()).padStart(2, '0');
        var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
        var yyyy = today.getFullYear();

        today = dd + '-' + mm + '-' + yyyy;
        console.log(today);


        $('.datatable-basic').DataTable({
            autoWidth: false,
            dom: '<"datatable-header"fBl><"datatable-scroll-wrap"t><"datatable-footer"ip>',
            language: {
                search: '<span>Pretraga:</span> _INPUT_',
                lengthMenu: '<span>Prikaz:</span> _MENU_',
                info: '_START_ do _END_ od ukupno _TOTAL_',
                paginate: {'first': 'First', 'last': 'Last', 'next': '→', 'previous': '←'}
            },
            buttons: {
                buttons: [
                    {
                        extend: 'excelHtml5',
                        className: 'btn btn-light',
                        title: 'Nalozi za Digitalizaciju '+today,
                        sheetName:'Digitalizacija',
                        exportOptions: {
                            columns: [ 0, 1, 2, 3, 4]
                        }
                    }
                ]
            }
        });

        $('.daterange-basic').daterangepicker({
            @if( empty($date_filter))
            startDate: moment().subtract(1, 'month').startOf('month'),
            endDate: moment().subtract(1, 'month').endOf('month'),
            @endif
            maxDate: moment(),
            locale: {
                format: 'DD/MM/YYYY'
            },
            applyClass: 'bg-teal-300',
            cancelClass: 'btn-light',

        });

        // hide all form-duration-divs
        /*$('.order_details').hide();*/
        @if( !empty($date_filter))
        $(".daterangecheckbox").hide();
        $(".daterange").show();
        @endif


        $(".daterangecheckbox").change(function(){
            $(".daterangecheckbox").hide(500);
            $(".daterange").show();
        });

    </script>
@endpush

@section('content')

    <div>
        <div class="content">
            <div class="title m-b-md">
                <h1 style="text-align: center;">Pregled naloga za Digitalizaciju</h1>
            </div>

            <div class="card mb-1">
                <div class="form-group card-body  mb-0">
                    {!! Form::open(['action' => 'DigitizationController@index', 'method' => 'POST']) !!}
                    <label>Filtriranje za period:  </label>
                    <div class="daterangecheckbox">
                        <input type="checkbox">
                    </div>

                    <div class="row daterange" style="display: none">

                        <div class="input-group col-4" {{--style="display: block !important;"--}}>

                            <span class="input-group-text"><i class="icon-calendar3"></i></span>
                            <input name="date_filter" style="width:200px" type="text" class="form-control daterange-basic" data-placeholder="Izaberite period" {{--value="20/03/2019 - 22/03/2019"--}}
                            @if( ! empty($date_filter))
                            value="{{$date_filter}}"
                                    @endif
                            >
                        </div>

                        <button type="submit" class="btn bg-teal-300 ml-3 col-2">Primeni filter</button>
                        <div class="col-3"></div>
                        <a class="btn bg-teal-300 ml-3 col-2" href="/reclamation">Prikaz svih naloga</a>
                    </div>

                </div>
            </div>

            {!! Form::close() !!}
            <div>
                <!-- Basic datatable -->
                <div class="card">

                    <table class="table datatable-basic table-bordered table-striped">
                        <thead>
                        <tr>
                            <th>Redni broj naloga</th>
                            <th>Ime i preizme</th>
                            <th>Opština</th>
                            <th>Adresa</th>
                            <th>Datum i vreme kreiranja</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($reclamation_orders as $order)
                            <tr>
                                <td>{{ $order->ordinal_number }}</td>
                                <td>{{ $order->buyer }}</td>
                                <td>{{ $order->township }}</td>
                                <td>{{ $order->address }} {{ $order->address_number }}</td>
                                <td>{{ datetimeForView($order->created_at) }}</td>
                                <td><a class="btn btn-outline-dark" href="reclamation/{{ $order->order_id }}"
                                       role="button">Prikaži nalog</a>
                                </td>
                            </tr>

                        @endforeach
                        </tbody>
                    </table>
                </div>
                <!-- /basic datatable -->

            </div>
        </div>
    </div>

@endsection



