@extends('layouts.app')



@push('scripts')


    <script src="../global_assets/js/plugins/ui/moment/moment.min.js"></script>
    <script src="../global_assets/js/plugins/pickers/anytime.min.js"></script>

    <script src="../assets/js/custom_datepicker.js"></script>

    <!-- Load select2 -->
    <script type="text/javascript" src="../global_assets/js/plugins/forms/selects/select2.min.js"></script>
    <script type="text/javascript" src="../global_assets/js/plugins/extensions/jquery_ui/interactions.min.js"></script>
    <script src="../assets/js/custom_select2.js"></script>
    <script>

        var start_status = $("#order_status option:selected").val();
        var start_scheduled_datetime = $("#scheduled_at_datetime").val();
        var start_order_comment = $("#order_comment").val();

        function onStatusChange() {
            // hide all form-duration-divs
            /*$('.order_details').hide();*/
            var status = $("#order_status option:selected").val();

            if (status == 'Otvoren' || status == 'Zakazan' || status == 'Odložen' || status == 'Otkazan' || status == 'Nekompletan') {
                $(".order_details").hide();
                $(".save_changes").show();
                console.log(status);
            } else {
                $(".order_details").show();
                $(".save_changes").show();
                console.log(status);
            }

            if (start_status == status) {
                $(".save_changes").hide();
            }

            if (status == 'Zakazan') {
                $(".div_status").show();
                if (start_scheduled_datetime != $("#scheduled_at_datetime").val()) {
                    $(".save_changes").show();
                }
            } else {
                $(".div_status").hide();
            }

            /* Otvoren, Zakazan, Odložen, Otkazan, Nerealizovan, Nekompletan, Završen*/

        }


        $("#order_status").change(function () {
            setTimeout(function () {
                onStatusChange();
                checkCommentChange();
            }, 600);
        });

        $("#scheduled_at_datetime").change(function () {
            $(".save_changes").show();
        });

        function checkCommentChange() {
            if (start_order_comment != $("#order_comment").val()) {
                $(".save_changes").show();
            }
        }

        $("#order_comment").keyup(function () {
            if (start_order_comment != $("#order_comment").val()) {
                $(".save_changes").show();
            } else {
                $(".save_changes").hide();
            }
        });

        onStatusChange();

        if (start_status == "Otkazan") {
            $(".status_row").hide();
            $(".canceled").show();
        } else if (start_status == "Nerealizovan") {
            $(".status_row").hide();
            $(".order_details").hide();
            $(".unrealized").show();
        }


    </script>

@endpush

@push('style')
    <style>
        .lesspadding {
            padding: .3rem 1.0rem !important;
        }
    </style>
@endpush

@section('content')
    {{--{{ $id }}--}}

    <div class="card mt-1">

        <div class="card-header header-elements-inline">
            <h4 class="card-title">Osnovni podaci naloga</h4>

            <div class="header-elements">
                <div class="list-icons">
                    <a class="list-icons-item" data-action="collapse"></a>
                </div>
            </div>
        </div>

        <div class="card-body">
            <div class="row">

                {{-- LEVA STRANA --}}
                <div class="col-6">
                    <fieldset>

                        {{-- KUPAC --}}
                        {{-- U redu tri inputa --}}

                        <p>Redni broj naloga : {{ $data['order']->ordinal_number }}</p>
                        <p>Broj kupca: {{ ($data['order']->buyer_id != null ) ? $data['order']->buyer_id : "/" }} </p>
                        <p>Broj
                            ugovora: {{ ($data['order']->treaty_id != null ) ? $data['order']->treaty_id : "/" }}</p>
                        <p>Ime i prezime kupca: {{ ($data['order']->buyer != null ) ? $data['order']->buyer : "/" }}</p>
                        <p>
                            Adresa: {{ ($data['order']->area_code != null ) ? $data['order']->area_code : "/" }} {{$data['order']->city}}
                            , {{ ($data['order']->township != null ) ? $data['order']->township : "/" }}
                            , {{ ($data['order']->address != null ) ? $data['order']->address : "/" }}
                            {{ ($data['order']->address_number != null ) ? $data['order']->address_number : "/" }},
                            sprat: {{ ($data['order']->floor != null ) ? $data['order']->floor : "/" }},
                            stan: {{ ($data['order']->apartment != null ) ? $data['order']->apartment : "/" }}</p>
                        <p>
                            Telefon: {{ ($data['order']->phone_number != null ) ? $data['order']->phone_number : "/" }}</p>
                        <p>Mobilni
                            telefon: {{ ($data['order']->mobile_number != null ) ? $data['order']->mobile_number : "/" }}</p>
                        <p>Datum i vreme kreiranja: {{datetimeForView($data['order']->created_at)}}</p>
                    </fieldset>
                </div>

                {{-- DESNA STRANA --}}
                <div class="col-6">
                    <fieldset>

                        <p>&nbsp</p>
                        <p>&nbsp</p>
                        {{--<p class="text-right">
                            <button type="button" class="btn bg-teal-300 " data-toggle="modal" data-target="#modal_add">
                                Izmeni <i class="icon-pencil7 ml-1"></i></button>
                        </p>--}}
                    </fieldset>
                </div>
            </div>
        </div>
    </div>

    {{--drugi deo--}}

    {{--ZATVARANJE NALOGA--}}

    <div class="card p-3">
        {{-- <div class="row">--}}

        {{-- CELA STRANA --}}
        {{--<div class="col-12">--}}
        {{-- <fieldset>--}}


        <legend class="font-weight-semibold mt-3"><i class=" NASE IKONICE "></i> Naziv opreme izdate na
            korišćenje
        </legend>
        <table class="table datatable-basic table-bordered table-striped">
            <thead>
            <tr>
                <th>Kôd</th>
                <th>Tip opreme</th>
                <th>Model</th>
                <th>S/N 1</th>
                <th>S/N 2</th>
                <th>S/N 3</th>

            </tr>
            </thead>
            <tbody>
            @foreach($data['used_equipment'] as $equipment)

                <tr>
                    <td>{{ $equipment->equipment_code }}</td>
                    <td> {{ $equipment->equipment_subtype_name }}</td>
                    @if($equipment->equipment_model_id != null)
                        <td> {{ $equipment->equipment_model_name }}</td>
                    @else
                        <td></td>
                    @endif
                    <td>{{ $equipment->equipment_serial1 }}</td>
                    <td>{{ $equipment->equipment_serial2 }}</td>
                    <td>{{ $equipment->equipment_serial3 }}</td>

                </tr>
            @endforeach

            </tbody>
        </table>

        <legend class="font-weight-semibold mt-3"><i class="{{-- NASE IKONICE --}}"></i> Upotrebljeni
            materijal
        </legend>

        <table class="table datatable-basic table-bordered table-striped">
            <thead>
            <tr>
                <th>Kod materijala</th>
                <th>Ime materijala</th>
                <th>Kolicina</th>

            </tr>
            </thead>
            <tbody>
            @foreach($data['used_wares'] as $used_wares)

                <tr>
                    <td>{{ $used_wares->wares_code }}</td>
                    <td>{{ $used_wares->wares_name }}</td>
                    <td>{{ $used_wares->quantity }}</td>

                </tr>
            @endforeach

            </tbody>
        </table>


        <legend class="font-weight-semibold mt-3"><i class="{{-- NASE IKONICE --}}"></i> Tehničari koji su
            radili na nalogu
        </legend>

        @foreach($data['order_technician'] as $key => $order_technician)
            <p>{{++$key}}. {{$order_technician->technician_name}}</p>

        @endforeach

        <legend class="font-weight-semibold mt-3"><i class="{{-- NASE IKONICE --}}"></i> Privatno vozilo
        </legend>

        <p>Tehničar koristio privatno vozilo:
            @if($data['order']->private_vehicle == 0)
                Ne
            @elseif($data['order']->private_vehicle == 1)
                Da
            @endif
        </p>

        {{-- KOMENTAR --}}
        <legend class="font-weight-semibold"><i class="{{-- NASE IKONICE --}}"></i> Komentar
        </legend>
        <div class="form-group">
                                    <textarea rows="5" cols="5" id="order_comment" name="order_comment"
                                              class="form-control"
                                              placeholder="Upisite dodatne komentare">{{$data['order']->note}}</textarea>
        </div>

        <div class="text-right">
            <button type="submit" class="btn bg-teal-300 save_changes">Sačuvaj izmene<i
                        class="icon-checkmark4 ml-2"></i></button>
        </div>
        {{-- </fieldset>--}}
        {{-- </div>--}}
        {{--</div>--}}
    </div>


@endsection