@extends('layouts.app')

@section('content')

    @push('style')

        <!-- Global stylesheets -->
        <link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
        <link href="../../../../global_assets/css/icons/icomoon/styles.css" rel="stylesheet" type="text/css">
        <link href="../assets/css/bootstrap.min.css" rel="stylesheet" type="text/css">
        <link href="../assets/css/bootstrap_limitless.min.css" rel="stylesheet" type="text/css">
        <link href="../assets/css/layout.min.css" rel="stylesheet" type="text/css">
        <link href="../assets/css/components.min.css" rel="stylesheet" type="text/css">
        <link href="../assets/css/colors.min.css" rel="stylesheet" type="text/css">
        <!-- /global stylesheets -->
    @endpush

    @push('scripts')
        <script src="../global_assets/js/plugins/tables/datatables/datatables.min.js"></script>
        <script src="../global_assets/js/demo_pages/datatables_basic.js"></script>
        <!-- Load select2 -->
        <script type="text/javascript" src="../global_assets/js/plugins/forms/selects/select2.min.js"></script>
        <script type="text/javascript" src="../global_assets/js/plugins/extensions/jquery_ui/interactions.min.js"></script>
        <script src="../assets/js/custom_select2.js"></script>
        <script src="../global_assets/js/plugins/forms/selects/select2.min.js"></script>
        {{--anytime--}}
        <script src="../global_assets/js/plugins/ui/moment/moment.min.js"></script>
        <script src="../global_assets/js/plugins/pickers/anytime.min.js"></script>
        <script src="../../../../global_assets/js/plugins/pickers/daterangepicker.js"></script>
        <script src="../assets/js/custom_datepicker.js"></script>

        <script src="../global_assets/js/main/jquery.min.js"></script>
        <script src="../global_assets/js/main/bootstrap.bundle.min.js"></script>
        <script src="../global_assets/js/plugins/loaders/blockui.min.js"></script>
        <script src="../global_assets/js/plugins/tables/datatables/datatables.min.js"></script>
        <script src="../global_assets/js/plugins/forms/selects/select2.min.js"></script>
        <script src="../global_assets/js/plugins/tables/datatables/extensions/jszip/jszip.min.js"></script>
        <script src="../global_assets/js/plugins/tables/datatables/extensions/buttons.min.js"></script>

        {{--export datatable--}}

        {{--anytime--}}
        <script src="../global_assets/js/plugins/ui/moment/moment.min.js"></script>
        <script src="../global_assets/js/plugins/pickers/anytime.min.js"></script>
        <script src="../../../../global_assets/js/plugins/pickers/daterangepicker.js"></script>
        <script src="../assets/js/custom_datepicker.js"></script>



        <script>

            var today = new Date();
            var dd = String(today.getDate()).padStart(2, '0');
            var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
            var yyyy = today.getFullYear();

            today = dd + '-' + mm + '-' + yyyy;
            console.log(today);


            $('.datatable-basic').DataTable({
                autoWidth: false,
                dom: '<"datatable-header"fBl><"datatable-scroll-wrap"t><"datatable-footer"ip>',
                language: {
                    search: '<span>Pretraga:</span> _INPUT_',
                    lengthMenu: '<span>Prikaz:</span> _MENU_',
                    info: '_START_ do _END_ od ukupno _TOTAL_',
                    paginate: {'first': 'First', 'last': 'Last', 'next': '→', 'previous': '←'}
                },
                buttons: {
                    buttons: [
                        {
                            extend: 'excelHtml5',
                            className: 'btn btn-light',
                            title: 'Korisnički nalozi '+today,
                            sheetName:'Korisnički nalozi',
                            exportOptions: {
                                columns: [ 0, 1, 2]
                            }
                        }
                    ]
                }
            });


            $('#modal_edit').on('show.bs.modal', function (event) {
                var button = $(event.relatedTarget);

                var user = button.data('alldata');
                var modal = $(this);
                modal.find('.modal-body #user_id').val(user.id);
                modal.find('.modal-body #name').val(user.name);
                modal.find('.modal-body #email').val(user.email);

                //user role select
                modal.find('#role option:selected').removeAttr("selected");
                modal.find('#role option[value="'+user.role+'"]').attr('selected', 'selected');
                var selected_role_text = $( "#role option:selected" ).text();
                modal.find('#select2-role-container').prop('title',selected_role_text);
                modal.find('#select2-role-container').prop('innerText',selected_role_text);
            });

            $('#modal_update_pass').on('show.bs.modal', function (event) {
                var button = $(event.relatedTarget);

                var user = button.data('alldata');
                var modal = $(this);
                modal.find('.modal-body #user_id').val(user.id);
            });


            $('#modal_delete').on('show.bs.modal', function (event) {
                var button = $(event.relatedTarget);
                var user = button.data('alldata');
                console.log(user.id);
                var modal = $(this);
                modal.find('.modal-body #user_id').val(user.id);
                modal.find('.modal-body #name').text(user.name);
            });

        </script>

    @endpush

    <div>
        <div class="content">
            <div class="title m-b-md">
                <h1 style="text-align: center;">Pregled korisničkih naloga</h1>
            </div>

            <div>

                @if ($errors->any())
                    <div class="alert alert-danger">
                        @foreach ($errors->all() as $error)
                            <div>- {{ $error }}</div>
                        @endforeach
                    </div>
                @endif



                <button type="button" class="btn bg-teal-300" data-toggle="modal" data-target="#modal_add">Otvaranje
                    korisničkog naloga <i class="icon-plus3 ml-1"></i></button>

                <!-- Add modal -->
                <div id="modal_add" class="modal fade" tabindex="-1">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title">Otvaranje korisničkog naloga</h5>
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                            </div>

                            <div class="modal-body">
                                <form method="POST" action="{{ route('register') }}">
                                    @csrf

                                    <div class="form-group row">
                                        <label for="name" class="col-md-3 col-form-label">Ime i
                                            prezime</label>

                                        <div class="col-md-8">
                                            <input id="name" type="text"
                                                   class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}"
                                                   name="name" value="{{ old('name') }}" required autofocus
                                                   placeholder="Upišite ime i prezime">

                                            @if ($errors->has('name'))
                                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label for="email" class="col-md-3 col-form-label">E-Mail
                                            adresa</label>

                                        <div class="col-md-8">
                                            <input id="email" type="email"
                                                   class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}"
                                                   name="email" value="{{ old('email') }}" required
                                                   placeholder="Upišite е-mail">

                                            @if ($errors->has('email'))
                                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label for="role" class="col-md-3 col-form-label">Uloga</label>

                                        <div class="col-md-8">

                                            <select name="role" class="form-control select" data-fouc
                                                    data-placeholder="Izaberite ulogu">
                                                <option></option>
                                                <option value="Administrator">Administrator</option>
                                                <option value="Kordinator">Kordinator</option>

                                            </select>

                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label for="password"
                                               class="col-md-3 col-form-label">Lozinka</label>

                                        <div class="col-md-8">
                                            <input id="password" type="password"
                                                   class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}"
                                                   name="password" required placeholder="Upišite lozinku">

                                            @if ($errors->has('password'))
                                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label for="password-confirm" class="col-md-3 col-form-label">Ponovite
                                            lozinku</label>

                                        <div class="col-md-8">
                                            <input id="password-confirm" type="password" class="form-control"
                                                   name="password_confirmation" required placeholder="Ponovite lozinku">
                                        </div>
                                    </div>

                                    <div class="form-group row mb-0">
                                        <div class="col-md-3 offset-md-3">
                                            <button type="submit" class="btn bg-teal-300">
                                                Otvori korisnički nalog
                                            </button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /basic modal -->

                <!-- Edit modal -->
                <div id="modal_edit" class="modal fade" tabindex="-1">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title">Izmena korisničkog naloga</h5>
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                            </div>

                            <div class="modal-body">

                                {!! Form::open(['action' => ['UserController@update', 'update_user'], 'method' => 'POST']) !!}
                                {{ csrf_field() }}
                                {{ method_field('PUT')}}
                                {{Form::hidden ('user_id', 'update', ['id' => 'user_id'])}}

                                <div class="form-group row">
                                    <label for="name" class="col-md-3 col-form-label">Ime i
                                        prezime</label>

                                    <div class="col-md-8">
                                        <input id="name" type="text"
                                               class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}"
                                               name="name" value="{{ old('name') }}" required autofocus
                                               placeholder="Upišite ime i prezime">

                                        @if ($errors->has('name'))
                                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="email" class="col-md-3 col-form-label">E-Mail
                                        adresa</label>

                                    <div class="col-md-8">
                                        <input id="email" type="email"
                                               class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}"
                                               name="email" value="{{ old('email') }}" required
                                               placeholder="Upišite е-mail">

                                        @if ($errors->has('email'))
                                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="role" class="col-md-3 col-form-label">Uloga</label>

                                    <div class="col-md-8">

                                        <select name="role" class="form-control select" data-fouc
                                                data-placeholder="Izaberite ulogu" id="role">
                                            <option></option>
                                            <option value="Administrator">Administrator</option>
                                            <option value="Kordinator">Kordinator</option>

                                        </select>

                                    </div>
                                </div>

                                <div class="form-group row mb-0">
                                    <div class="col-md-3 offset-md-3">
                                        <button type="submit" class="btn bg-teal-300">
                                            Izmeni korisnički nalog
                                        </button>
                                    </div>
                                </div>
                                </div>
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
                <!-- /edit modal -->

                    <!-- update pass modal -->
                    <div id="modal_update_pass" class="modal fade" tabindex="-1">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title">Izmena lozike</h5>
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                </div>

                                <div class="modal-body">

                                    {!! Form::open(['action' => ['UserController@update', 'update_pass'], 'method' => 'POST']) !!}
                                    {{ csrf_field() }}
                                    {{ method_field('PUT')}}
                                    {{Form::hidden ('user_id', 'update', ['id' => 'user_id'])}}


                                    <div class="form-group row">
                                        <label for="password"
                                               class="col-md-3 col-form-label">Nova lozinka</label>

                                        <div class="col-md-8">
                                            <input id="password" type="password"
                                                   class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}"
                                                   name="password" required placeholder="Upišite novu lozinku">

                                            @if ($errors->has('password'))
                                                <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('password') }}</strong>
                                        </span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label for="password-confirm" class="col-md-3 col-form-label">Ponovite
                                            lozinku</label>

                                        <div class="col-md-8">
                                            <input id="password-confirm" type="password" class="form-control"
                                                   name="password_confirmation" required placeholder="Ponovite lozinku">
                                        </div>
                                    </div>

                                    <div class="form-group row mb-0">
                                        <div class="col-md-3 offset-md-3">
                                            <button type="submit" class="btn bg-teal-300">
                                                Postavi novu lozinku
                                            </button>
                                        </div>
                                    </div>
                                </div>
                                {!! Form::close() !!}
                            </div>
                        </div>
                    </div>
                    <!-- /update pass modal -->

                <!-- Delete modal -->
                <div id="modal_delete" class="modal fade" tabindex="-1">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title">Brisanje korisničkog naloga</h5>
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                            </div>

                            <div class="modal-body">
                                {!! Form::open(['action' => ['UserController@update', 'delete'], 'method' => 'POST']) !!}
                                {{ csrf_field() }}
                                {{ method_field('PUT')}}
                                {{Form::hidden ('user_id', 'delete', ['id' => 'user_id'])}}
                                <p>Da li ste sigurni da želite da izbrišete korisnički nalog za "<span
                                            id="name"></span>"?</p>
                            </div>

                            <div class="modal-footer">
                                <button type="button" class="btn btn-light" data-dismiss="modal">Odustani</button>
                                {!! Form::button('Izbriši', ['type' => 'submit', 'id' =>'button-create-technician', 'class' => 'btn btn-danger button-submit', 'data-loading-text' => 'Sačuvaj novog korisnika']) !!}
                            </div>
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
                <!-- /delete modal -->

                <br>

                <!-- Basic datatable -->
                <div class="card mt-2">

                    <table class="table datatable-basic">
                        <thead>
                        <tr>
                            <th>Ime i prezime</th>
                            <th>E-mail</th>
                            <th>Uloga</th>
                            <th class="text-center">Izmena / Brisanje</th>
                        </tr>
                        </thead>

                        {{--{{ $technicians }}--}}
                        <tbody>
                        @foreach($users as $user)
                            <tr>
                                <td>{{ $user->name }}</td>
                                <td>{{ $user->email }}</td>
                                <td>{{ $user->role }}</td>
                                <td class="text-center">
                                    <div class="list-icons">
                                        <div class="dropdown">
                                            <a href="#" class="list-icons-item" data-toggle="dropdown">
                                                <i class="icon-menu9"></i>
                                            </a>
                                            <div class="dropdown-menu dropdown-menu-left">
                                                <button class="dropdown-item" data-alldata="{{$user}}"
                                                        data-toggle="modal" data-target="#modal_edit"><i
                                                            class="icon-pencil7"></i>Izmeni
                                                </button>

                                                <button class="dropdown-item" data-alldata="{{$user}}"
                                                        data-toggle="modal" data-target="#modal_update_pass"><i
                                                            class="icon-pencil7"></i>Promeni lazinku
                                                </button>





                                                <button class="dropdown-item" data-alldata="{{$user}}"
                                                        data-toggle="modal" data-target="#modal_delete"><i
                                                            class="icon-cancel-square"></i>Izbriši
                                                </button>

                                            </div>
                                        </div>
                                    </div>

                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                        {{--{{ /$technicians }}--}}
                    </table>
                </div>
                <!-- /basic datatable -->

            </div>
        </div>
    </div>

@endsection



