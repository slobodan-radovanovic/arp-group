@extends('layouts.app')

@section('content')

    @push('style')
        <style>
            .nav-pills-bordered .nav-link.active {
                border-color: #4db6ac !important;
            }

            .nav-pills .nav-link.active, .nav-pills .show > .nav-link {
                color: #fff;
                background-color: #4db6ac !important;
            }


        </style>

        <!-- Global stylesheets -->
        <link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
        <link href="../../../../global_assets/css/icons/icomoon/styles.css" rel="stylesheet" type="text/css">
        <link href="../assets/css/bootstrap.min.css" rel="stylesheet" type="text/css">
        <link href="../assets/css/bootstrap_limitless.min.css" rel="stylesheet" type="text/css">
        <link href="../assets/css/layout.min.css" rel="stylesheet" type="text/css">
        <link href="../assets/css/components.min.css" rel="stylesheet" type="text/css">
        <link href="../assets/css/colors.min.css" rel="stylesheet" type="text/css">
        <!-- /global stylesheets -->
    @endpush

    @push('scripts')
        <script src="../global_assets/js/plugins/tables/datatables/datatables.min.js"></script>
        <script src="../global_assets/js/demo_pages/datatables_basic.js"></script>
        <!-- Load select2 -->
        <script type="text/javascript" src="../global_assets/js/plugins/forms/selects/select2.min.js"></script>
        <script type="text/javascript" src="../global_assets/js/plugins/extensions/jquery_ui/interactions.min.js"></script>
        <script src="../assets/js/custom_select2.js"></script>
        <script src="../global_assets/js/plugins/forms/selects/select2.min.js"></script>
        {{--anytime--}}
        <script src="../global_assets/js/plugins/ui/moment/moment.min.js"></script>
        <script src="../global_assets/js/plugins/pickers/anytime.min.js"></script>
        <script src="../../../../global_assets/js/plugins/pickers/daterangepicker.js"></script>
        <script src="../assets/js/custom_datepicker.js"></script>

        <script src="../global_assets/js/main/jquery.min.js"></script>
        <script src="../global_assets/js/main/bootstrap.bundle.min.js"></script>
        <script src="../global_assets/js/plugins/loaders/blockui.min.js"></script>
        <script src="../global_assets/js/plugins/tables/datatables/datatables.min.js"></script>
        <script src="../global_assets/js/plugins/forms/selects/select2.min.js"></script>
        <script src="../global_assets/js/plugins/tables/datatables/extensions/jszip/jszip.min.js"></script>
        <script src="../global_assets/js/plugins/tables/datatables/extensions/buttons.min.js"></script>

        {{--export datatable--}}

        {{--anytime--}}
        <script src="../global_assets/js/plugins/ui/moment/moment.min.js"></script>
        <script src="../global_assets/js/plugins/pickers/anytime.min.js"></script>
        <script src="../../../../global_assets/js/plugins/pickers/daterangepicker.js"></script>
        <script src="../assets/js/custom_datepicker.js"></script>



        <script>

            var today = new Date();
            var dd = String(today.getDate()).padStart(2, '0');
            var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
            var yyyy = today.getFullYear();

            today = dd + '-' + mm + '-' + yyyy;
            console.log(today);


            $('.datatable-basic').DataTable({
                autoWidth: false,
                dom: '<"datatable-header"fBl><"datatable-scroll-wrap"t><"datatable-footer"ip>',
                language: {
                    search: '<span>Pretraga:</span> _INPUT_',
                    lengthMenu: '<span>Prikaz:</span> _MENU_',
                    info: '_START_ do _END_ od ukupno _TOTAL_',
                    paginate: {'first': 'First', 'last': 'Last', 'next': '→', 'previous': '←'}
                },
                buttons: {
                    buttons: [
                        {
                            extend: 'excelHtml5',
                            className: 'btn btn-light',
                            title: 'Vrste materijala '+today,
                            sheetName:'Vrste materijala',
                            exportOptions: {
                                columns: [ 0, 1, 2]
                            }
                        }
                    ]
                }
            });


            $('#modal_edit').on('show.bs.modal', function (event) {
                var button = $(event.relatedTarget);

                var ware = button.data('alldata');
                var modal = $(this);
                console.log(ware);
                modal.find('.modal-body #wares_id').val(ware.wares_id);
                modal.find('.modal-body #wares_code').val(ware.wares_code);
                modal.find('.modal-body #wares_name').val(ware.wares_name);

                modal.find('#wares_type_edit option:selected').removeAttr("selected");
                modal.find('#wares_type_edit option[value="'+ware.wares_type+'"]').attr('selected', 'selected');
                var selected_type_text = $( "#wares_type_edit option:selected" ).text();
                modal.find('span[id^="select2-wares_type_edit"]').prop('title',selected_type_text);
                modal.find('span[id^="select2-wares_type_edit"]').prop('innerText',selected_type_text);

                if(ware.default == 1){
                    modal.find('#default_edit').prop('checked',true);
                }else{
                    modal.find('#default_edit').prop('checked',false);
                };


            });


        </script>
    @endpush

    <div>
        <div class="content">
            <div class="title m-b-md">
                <h1 style="text-align: center;">Pregled vrsta materijala</h1>
            </div>

            @if ($errors->any())
                <div class="alert alert-danger">
                    @foreach ($errors->all() as $error)
                        <div>- {{ $error }}</div>
                    @endforeach
                </div>
            @endif

            <div>
                <button type="button" class="btn bg-teal-300" data-toggle="modal" data-target="#modal_add">Dodavanje nove vrste materijala <i class="icon-plus3 ml-1"></i></button>

                <!-- Add modal -->
                <div id="modal_add" class="modal fade" tabindex="-1">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title">Dodavanje nove vrste materijala</h5>
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                            </div>

                            <div class="modal-body">
                                {!! Form::open(['action' => 'WaresController@store', 'method' => 'POST']) !!}

                                {{ Form::textGroup('wares_code', 'Kod vrste materijala', null, ['placeholder' => 'Upisite kod vrste materijala'], null, 'col-12', 'col-3', 'col-9') }}

                                {{ Form::textGroup('wares_name', 'Ime vrste materijala', null, ['placeholder' => 'Upisite ime vrste materijala'], null, 'col-12', 'col-3', 'col-9') }}

         <div class="row">
         <div class="col-3"><label>Jedinica mere:</label></div>
         <div class="col-9">
             <select name="wares_type" class="form-control select" data-fouc data-placeholder="Izaberite jedinica mere">
                    <option></option>
                 <option value="kom">kom</option>
                    <option value="m">m</option>
             </select>
         </div>
         </div>

             <div class="row mt-2">
                 <div class="col-3">{!! Form::label("default", "Najčešće korišćeni", ['class' => 'control-label']) !!}</div>
                 <div class="col-9">{{ Form::checkbox("default", 1, null)}}</div>

             </div>

         </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-light" data-dismiss="modal">Odustani</button>
                                {!! Form::button('Dodaj', ['type' => 'submit', 'id' =>'button-create-technician', 'class' => 'btn bg-teal-300 button-submit', 'data-loading-text' => 'Sačuvaj novog korisnika']) !!}
                            </div>
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
                <!-- /add modal -->

                <!-- Edit modal -->
                <div id="modal_edit" class="modal fade" tabindex="-1">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title">Izmena vrste materijala</h5>
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                            </div>

                            <div class="modal-body">
                                {!! Form::open(['action' => ['WaresController@update', 'update'], 'method' => 'POST']) !!}
                                {{ csrf_field() }}
                                {{ method_field('PUT')}}
                                {{Form::hidden ('wares_id', 'update', ['id' => 'wares_id'])}}
                                {{ Form::textGroup('wares_code', 'Kod vrste materijala', null, ['placeholder' => 'Upisite kod vrste materijala'], null, 'col-12', 'col-3', 'col-9') }}

                                {{ Form::textGroup('wares_name', 'Ime vrste materijala', null, ['placeholder' => 'Upisite ime vrste materijala'], null, 'col-12', 'col-3', 'col-9') }}

                                <div class="row">
                                    <div class="col-3"><label>Jedinica mere:</label></div>
                                    <div class="col-9">
                                        <select name="wares_type_edit" id="wares_type_edit" class="form-control select service_type1" data-fouc data-placeholder="Izaberite jedinica mere">
                                            <option>
                                            <option value="kom">kom
                                            <option value="m">m
                                        </select>
                                    </div>
                                </div>

                                <div class="row mt-2">
                                    <div class="col-3">{!! Form::label("default_edit", "Najčešće korišćeni", ['class' => 'control-label']) !!}</div>
                                    <div class="col-9">{{ Form::checkbox("default_edit", 1, null)}}</div>

                                </div>

                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-light" data-dismiss="modal">Odustani</button>
                                {!! Form::button('Izmeni', ['type' => 'submit', 'id' =>'button-edit-wares', 'class' => 'btn bg-teal-300 button-submit', 'data-loading-text' => 'Izmeni materijal']) !!}
                            </div>
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
                <!-- /edit modal -->

                <!-- Delete modal -->
                <div id="modal_d" class="modal fade" tabindex="-1">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title">Brisanje tehničara</h5>
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                            </div>

                            <div class="modal-body">
                                {!! Form::open(['id' => 'form-create-customer', 'role' => 'form', 'class' => 'form-loading-button']) !!}

                                {{--{{ Form::textGroup('name', 'Da li ste sigurni da želite da izbrišete tehničara "Aleksandar Jovanovic"', null, ['placeholder' => 'Upisite ime novog tehničara'], null, 'col-12', 'col-12', 'col-0') }}--}}
                                <p>Da li ste sigurni da želite da izbrišete tehničara "Aleksandar Jovanovic"</p>
                            </div>

                            <div class="modal-footer">
                                <button type="button" class="btn btn-light" data-dismiss="modal">Odustani</button>
                                {!! Form::button('Izbriši', ['type' => 'button', 'id' =>'button-create-technician', 'class' => 'btn btn-danger button-submit', 'data-loading-text' => 'Sačuvaj novog korisnika']) !!}
                            </div>
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
                <!-- /delete modal -->

                <br>

                <!-- Basic datatable -->
                <div class="card mt-2">

                    <table class="table datatable-basic table-bordered table-striped">
                        <thead>
                        <tr>
                            <th>Kod vrste materijala</th>
                            <th>Ime vrste materijala</th>
                            <th>Jedinica mere</th>
                            <th>Najčešće korišćeni</th>
                            <th{{-- class="text-center"--}}>Izmena</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($wares as $ware)

                            {{--@dd($ware)--}}
                        <tr>
                            <td>{{$ware->wares_code}}</td>
                            <td>{{$ware->wares_name}}</td>
                            <td>{{$ware->wares_type}}</td>
                            <td>
                                @if($ware->default)
                                <i class="icon-checkmark2"></i>
                                {{--@else--}}
                                @endif

                            </td>
                            <td class="text-center">
                                <a href="#" class="dropdown-item" data-toggle="modal" data-target="#modal_edit" data-alldata="{{$ware}}"> <i class="icon-pencil7"></i> {{--Izmeni--}}</a>

                                {{--<div class="list-icons">
                                    <div class="dropdown">
                                        <a href="#" class="list-icons-item" data-toggle="dropdown">
                                            <i class="icon-menu9"></i>
                                        </a>

                                        <div class="dropdown-menu dropdown-menu-left">

                                            <a href="#" class="dropdown-item" data-toggle="modal" data-target="#modal_delete"><i class="icon-cancel-square"></i> Izbriši</a>
                                        </div>
                                    </div>
                                </div>--}}

                            </td>
                        </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
                <!-- /basic datatable -->

            </div>
        </div>
    </div>

@endsection



