@extends('layouts.app')

@section('content')

    @push('style')
        <style>
            .nav-pills-bordered .nav-link.active {
                border-color: #4db6ac !important;
            }

            .nav-pills .nav-link.active, .nav-pills .show > .nav-link {
                color: #fff;
                background-color: #4db6ac !important;
            }

            .nav-pills .nav-link {
                display: block;
                padding: .625rem .8rem !important;
            }


        </style>

        <!-- Global stylesheets -->
        <link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet"
              type="text/css">
        <link href="../../../../global_assets/css/icons/icomoon/styles.css" rel="stylesheet" type="text/css">
        <link href="../assets/css/bootstrap.min.css" rel="stylesheet" type="text/css">
        <link href="../assets/css/bootstrap_limitless.min.css" rel="stylesheet" type="text/css">
        <link href="../assets/css/layout.min.css" rel="stylesheet" type="text/css">
        <link href="../assets/css/components.min.css" rel="stylesheet" type="text/css">
        <link href="../assets/css/colors.min.css" rel="stylesheet" type="text/css">
        <!-- /global stylesheets -->
    @endpush



    @push('scripts')
        {{--<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>

        <script src="../../global_assets/js/plugins/tables/datatables/datatables.min.js"></script>
        <script src="../../global_assets/js/demo_pages/datatables_basic.js"></script>
        <script src="../../global_assets/js/plugins/forms/selects/select2.min.js"></script>
        <script src="../global_assets/js/plugins/tables/datatables/extensions/jszip/jszip.min.js"></script>
        <script src="../global_assets/js/plugins/tables/datatables/extensions/buttons.min.js"></script>--}}

        <script src="../global_assets/js/plugins/tables/datatables/datatables.min.js"></script>
        <script src="../global_assets/js/demo_pages/datatables_basic.js"></script>
        <!-- Load select2 -->
        <script type="text/javascript" src="../global_assets/js/plugins/forms/selects/select2.min.js"></script>
        <script type="text/javascript" src="../global_assets/js/plugins/extensions/jquery_ui/interactions.min.js"></script>
        <script src="../assets/js/custom_select2.js"></script>
        <script src="../global_assets/js/plugins/forms/selects/select2.min.js"></script>
        {{--anytime--}}
        <script src="../global_assets/js/plugins/ui/moment/moment.min.js"></script>
        <script src="../global_assets/js/plugins/pickers/anytime.min.js"></script>
        <script src="../../../../global_assets/js/plugins/pickers/daterangepicker.js"></script>
        <script src="../assets/js/custom_datepicker.js"></script>

        <script src="../global_assets/js/main/jquery.min.js"></script>
        <script src="../global_assets/js/main/bootstrap.bundle.min.js"></script>
        <script src="../global_assets/js/plugins/loaders/blockui.min.js"></script>
        <script src="../global_assets/js/plugins/tables/datatables/datatables.min.js"></script>
        <script src="../global_assets/js/plugins/forms/selects/select2.min.js"></script>
        <script src="../global_assets/js/plugins/tables/datatables/extensions/jszip/jszip.min.js"></script>
        <script src="../global_assets/js/plugins/tables/datatables/extensions/buttons.min.js"></script>

        {{--export datatable--}}

        {{--anytime--}}
        <script src="../global_assets/js/plugins/ui/moment/moment.min.js"></script>
        <script src="../global_assets/js/plugins/pickers/anytime.min.js"></script>
        <script src="../../../../global_assets/js/plugins/pickers/daterangepicker.js"></script>
        <script src="../assets/js/custom_datepicker.js"></script>


        <script>

            var technician_name = '{!! (string)$technician_name !!}';


            /*$('.datatable-basic1').DataTable({
                autoWidth: false,
                dom: '<"datatable-header"fl><"datatable-scroll"t><"datatable-footer"ip>',
                order: [5, "desc"],
                language: {
                    search: '<span>Pretraga:</span> _INPUT_',
                    lengthMenu: '<span>Prikaz:</span> _MENU_',
                    info: '_START_ do _END_ od ukupno _TOTAL_',
                    emptyTable: "Nema podataka",
                    paginate: { 'first': 'First', 'last': 'Last', 'next': '→', 'previous': '←' }
                },
                buttons: {
                    buttons: [
                        {
                            extend: 'excelHtml5',
                            className: 'btn btn-light',
                            title: technician_name + ' - Zadužena oprema',
                            sheetName: 'Zadužena oprema',
                            exportOptions: {
                                columns: [0, 1, 2, 3, 4]
                            }
                        }
                    ]
                }
            });*/



            /*$('.datatable-basic2').DataTable({
                autoWidth: false,
                dom: '<"datatable-header"fl><"datatable-scroll"t><"datatable-footer"ip>',
                order: [2, "desc"],
                language: {
                    search: '<span>Pretraga:</span> _INPUT_',
                    lengthMenu: '<span>Prikaz:</span> _MENU_',
                    info: '_START_ do _END_ od ukupno _TOTAL_',
                    emptyTable: "Nema podataka",
                    paginate: { 'first': 'First', 'last': 'Last', 'next': '→', 'previous': '←' }
                }
            });*/

            var today = new Date();
            var dd = String(today.getDate()).padStart(2, '0');
            var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
            var yyyy = today.getFullYear();

            today = dd + '-' + mm + '-' + yyyy;



            $('.datatable-basic1').DataTable({
                autoWidth: false,
                dom: '<"datatable-header"fBl><"datatable-scroll-wrap"t><"datatable-footer"ip>',
                order: [5, "desc"],
                language: {
                    search: '<span>Pretraga:</span> _INPUT_',
                    lengthMenu: '<span>Prikaz:</span> _MENU_',
                    info: '_START_ do _END_ od ukupno _TOTAL_',
                    paginate: {'first': 'First', 'last': 'Last', 'next': '→', 'previous': '←'}
                },
                buttons: {
                    buttons: [
                        {
                            extend: 'excelHtml5',
                            className: 'btn btn-light',
                            title: technician_name + ' - Zadužena oprema '+ today,
                            sheetName: 'Zadužena oprema',
                            exportOptions: {
                                columns: [0, 1, 2, 3, 4, 5]
                            }
                        }
                    ]
                }
            });

            $('.datatable-basic2').DataTable({
                autoWidth: false,
                dom: '<"datatable-header"fBl><"datatable-scroll-wrap"t><"datatable-footer"ip>',
                order: [2, "desc"],
                language: {
                    search: '<span>Pretraga:</span> _INPUT_',
                    lengthMenu: '<span>Prikaz:</span> _MENU_',
                    info: '_START_ do _END_ od ukupno _TOTAL_',
                    paginate: {'first': 'First', 'last': 'Last', 'next': '→', 'previous': '←'}
                },
                buttons: {
                    buttons: [
                        {
                            extend: 'excelHtml5',
                            className: 'btn btn-light',
                            title: technician_name + ' - Zadužen materijal ' + today,
                            sheetName: 'Zadužen materijal',
                            exportOptions: {
                                columns: [0, 1, 2]
                            }
                        }
                    ]
                }
            });

            $('.datatable-basic3').DataTable({
                autoWidth: false,
                dom: '<"datatable-header"fBl><"datatable-scroll-wrap"t><"datatable-footer"ip>',
                language: {
                    search: '<span>Pretraga:</span> _INPUT_',
                    lengthMenu: '<span>Prikaz:</span> _MENU_',
                    info: '_START_ do _END_ od ukupno _TOTAL_',
                    paginate: {'first': 'First', 'last': 'Last', 'next': '→', 'previous': '←'}
                },
                buttons: {
                    buttons: [
                        {
                            extend: 'excelHtml5',
                            className: 'btn btn-light',
                            title: technician_name + ' - Zadužen materijal ukupno',
                            sheetName: 'Zadužen materijal ukupno',
                            exportOptions: {
                                columns: [0, 1]
                            }
                        }
                    ]
                }
            });


            $('.datatable-basic4').DataTable({
                autoWidth: false,
                dom: '<"datatable-header"fBl><"datatable-scroll-wrap"t><"datatable-footer"ip>',
                language: {
                    search: '<span>Pretraga:</span> _INPUT_',
                    lengthMenu: '<span>Prikaz:</span> _MENU_',
                    info: '_START_ do _END_ od ukupno _TOTAL_',
                    paginate: {'first': 'First', 'last': 'Last', 'next': '→', 'previous': '←'}
                },
                buttons: {
                    buttons: [
                        {
                            extend: 'excelHtml5',
                            className: 'btn btn-light',
                            title: technician_name + ' - Utrošen materijal po nalogu',
                            sheetName: 'Utrošen materijal po nalogu',
                            exportOptions: {
                                columns: [0, 1, 2]
                            }
                        }
                    ]
                }
            });


            $('.datatable-basic5').DataTable({
                autoWidth: false,
                dom: '<"datatable-header"fBl><"datatable-scroll-wrap"t><"datatable-footer"ip>',
                language: {
                    search: '<span>Pretraga:</span> _INPUT_',
                    lengthMenu: '<span>Prikaz:</span> _MENU_',
                    info: '_START_ do _END_ od ukupno _TOTAL_',
                    paginate: {'first': 'First', 'last': 'Last', 'next': '→', 'previous': '←'}
                },
                buttons: {
                    buttons: [
                        {
                            extend: 'excelHtml5',
                            className: 'btn btn-light',
                            title: technician_name + ' - Utrošen materijal ukupno',
                            sheetName: 'Utrošen materijal ukupno',
                            exportOptions: {
                                columns: [0, 1]
                            }
                        }
                    ]
                }
            });


            $('.datatable-basic6').DataTable({
                autoWidth: false,
                dom: '<"datatable-header"fBl><"datatable-scroll-wrap"t><"datatable-footer"ip>',
                language: {
                    search: '<span>Pretraga:</span> _INPUT_',
                    lengthMenu: '<span>Prikaz:</span> _MENU_',
                    info: '_START_ do _END_ od ukupno _TOTAL_',
                    paginate: {'first': 'First', 'last': 'Last', 'next': '→', 'previous': '←'}
                },
                buttons: {
                    buttons: [
                        {
                            extend: 'excelHtml5',
                            className: 'btn btn-light',
                            title: technician_name + ' - Izveštaj za materijal (Zaduženo - Utrošeno)',
                            sheetName: 'Izveštaj za materijal (Zaduženo - Utrošeno)',
                            exportOptions: {
                                columns: [0, 1, 2]
                            }
                        }
                    ]
                }
            });

        </script>
    @endpush

    <div>
        <div class="content">
            <div class="title m-b-md">
                <h1 style="text-align: center;">Pregled zadužene opreme i materijala ze tehničara {{$technician_name}}</h1>
            </div>

            <div>

                <br>

                <!-- Zadužena oprema -->
                <div class="card mt-2">



                    <div class="card-body" style="border-bottom: 1px solid #bbbfc3;">
                        <ul class="nav nav-pills nav-pills-bordered" style="margin-bottom:0 !important;">
                            <li class="nav-item"><a href="#bordered-pill1" class="nav-link active" data-toggle="tab" onclick="set_actitve_tab()">Zadužena oprema</a></li>
                            <li class="nav-item"><a href="#bordered-pill2" class="nav-link" data-toggle="tab" onclick="set_actitve_tab()">Zadužen materijal</a></li>
                            <li class="nav-item"><a href="#bordered-pill3" class="nav-link" data-toggle="tab" onclick="set_actitve_tab()">Zadužen materijal ukupno</a></li>

                            <li class="nav-item"><a href="#bordered-pill4" class="nav-link" data-toggle="tab" onclick="set_actitve_tab()">Utrošen materijal po nalogu</a></li>

                            <li class="nav-item"><a href="#bordered-pill5" class="nav-link" data-toggle="tab" onclick="set_actitve_tab()">Utrošen materijal ukupno</a></li>

                            <li class="nav-item"><a href="#bordered-pill6" class="nav-link" data-toggle="tab" onclick="set_actitve_tab()">Razlika (zaduženo - utrošeno)</a></li>
                        </ul>
                    </div>

                    <div class="tab-content">

                        <div class="tab-pane fade show active" id="bordered-pill1">

                    <table class="table datatable-basic1 table-bordered table-striped">
                        <thead>
                        <tr>
                            <th>Tip opreme</th>
                            <th>Model</th>
                            <th>S/N 1</th>
                            <th>S/N 2</th>
                            <th>S/N 3</th>
                            <th>Vreme zaduzvanja</th>
                        </tr>
                        </thead>

                        {{--{{ $technicians }}--}}
                        <tbody>
                        @foreach($assigned_equipments as $equipment)
                            <tr>
                                <td>{{ $equipment->equipment_subtype_name }}</td>
                                <td>{{ $equipment->equipment_model_name }}</td>
                                <td>{{ $equipment->equipment_serial1 }}</td>
                                <td>{{ $equipment->equipment_serial2 }}</td>
                                <td>{{ $equipment->equipment_serial3 }}</td>
                                <td>{{ datetimeForView($equipment->equipment_assign_date) }}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                        </div>
                <!-- /Zadužena oprema -->

                <!-- Zadužen materijal -->
                   <div class="tab-pane fade" id="bordered-pill2">

                    <table class="table datatable-basic2 table-bordered table-striped">
                        <thead>
                        <tr>
                            <th>Naziv materijala</th>
                            <th>Količina</th>
                            <th>Vreme zaduzvanja</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($assigned_wares as $wares)
                            <tr>
                                <td>{{ $wares->wares_name }}</td>
                                <td>{{ $wares->quantity }}</td>
                                <td>{{ datetimeForView($wares->created_at) }}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
                <!-- /Zadužena materijal -->

                <!-- Zadužen materijal sum-->
                  <div class="tab-pane fade" id="bordered-pill3">

                    <table class="table datatable-basic3 table-bordered table-striped">
                        <thead>
                        <tr>
                            <th>Naziv materijala</th>
                            <th>Količina</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($assigned_wares_sum as $wares)
                            <tr>
                                <td>{{ $wares->wares_name }}</td>
                                <td>{{ $wares->assigned_wares_sum }}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
                <!-- /Zadužen materijal sum -->

                <!-- Upotrebljen materijal po nalozima-->
                <div class="tab-pane fade" id="bordered-pill4">


                    <table class="table datatable-basic4 table-bordered table-striped">
                        <thead>
                        <tr>
                            <th>Reni broj naloga</th>
                            <th>Ime materijala</th>
                            <th>Količina</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($wares_by_order as $ordinal_number => $order_wares)
                            @foreach($order_wares as $wares_name => $wares_quantity)
                            <tr>
                                <td> {{ $ordinal_number }} </td>
                                <td>{{ $wares_name }}</td>
                                <td>{{ $wares_quantity }}</td>

                            </tr>
                            @endforeach
                        @endforeach
                        </tbody>
                    </table>
                </div>
                <!-- /Upotrebljen materijal po nalozima -->

                <!-- Upotrebljen materijal ukupno-->
                <div class="tab-pane fade" id="bordered-pill5">

                    <table class="table datatable-basic5 table-bordered table-striped">
                        <thead>
                        <tr>
                            <th>Ime materijala</th>
                            <th>Količina</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($wares_by_technician as $wares_name => $wares_quantity)

                                <tr>
                                    <td>{{ $wares_name }}</td>
                                    <td>{{ $wares_quantity }}</td>

                                </tr>

                        @endforeach
                        </tbody>
                    </table>
                </div>
                <!-- /Upotrebljen materijal ukupno -->


                <!-- Raylika-->
                <div class="tab-pane fade" id="bordered-pill6">

                    <table class="table datatable-basic6 table-bordered table-striped">
                        <thead>
                        <tr>
                            <th>Naziv materijala</th>
                            <th>Zaduženo - Utrošeno</th>
                            <th>Razlika</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($assigned_wares_sum as $wares)
                            @if(array_key_exists ( $wares->wares_name , $wares_by_technician))
                            <tr>
                                <td>{{ $wares->wares_name }}</td>
                                <td>{{ $wares->assigned_wares_sum }} - {{$wares_by_technician[$wares->wares_name]}}</td>
                                <td>{{ $wares->assigned_wares_sum - $wares_by_technician[$wares->wares_name]}}</td>
                            </tr>
                            @endif
                        @endforeach
                        </tbody>
                    </table>
                </div>
                <!-- Razlika -->
                    </div>



            </div>
        </div>
    </div>

@endsection



