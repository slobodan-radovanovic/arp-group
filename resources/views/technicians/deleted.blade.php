@extends('layouts.app')

@section('content')

    @push('scripts')
        <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>

        <script src="../global_assets/js/plugins/tables/datatables/datatables.min.js"></script>
        <script src="../global_assets/js/demo_pages/datatables_basic.js"></script>
        <script src="../global_assets/js/plugins/forms/selects/select2.min.js"></script>


        <script>
            $('.datatable-basic').DataTable({
                autoWidth: false,
                dom: '<"datatable-header"fl><"datatable-scroll"t><"datatable-footer"ip>',
                language: {
                    search: '<span>Pretraga:</span> _INPUT_',
                    lengthMenu: '<span>Prikaz:</span> _MENU_',
                    info: '_START_ do _END_ od ukupno _TOTAL_',
                    paginate: { 'first': 'First', 'last': 'Last', 'next': '→', 'previous': '←' }
                }
            });

            $('#modal_edit').on('show.bs.modal', function (event) {
                var button = $(event.relatedTarget);
                var technician = button.data('alldata');
                var modal = $(this);
                modal.find('.modal-body #technician_id').val(technician.technician_id);
                modal.find('.modal-body #technician_name').val(technician.technician_name);
                modal.find('.modal-body #mobile_number').val(technician.mobile_number);
            });


            $('#modal_activate').on('show.bs.modal', function (event) {
                var button = $(event.relatedTarget);
                var technician = button.data('alldata');
                var modal = $(this);
                modal.find('.modal-body #technician_id').val(technician.technician_id);
                modal.find('.modal-body #technician_name').text(technician.technician_name);
            })

        </script>
    @endpush

    <div>
        <div class="content">
            <div class="title m-b-md">
                <h1 style="text-align: center;">Pregled izbrisanih tehničara</h1>
            </div>

            <div>

                <!-- Edit modal -->
                <div id="modal_edit" class="modal fade" tabindex="-1">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title">Izmena </h5>
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                            </div>

                            <div class="modal-body">

                                {!! Form::open(['action' => ['TechniciansController@update', 'update_inactive'], 'method' => 'POST']) !!}
                                {{ csrf_field() }}
                                {{ method_field('PUT')}}
                                {{Form::hidden ('technician_id', 'update', ['id' => 'technician_id'])}}

                                {{ Form::textGroup('technician_name', 'Ime tehničara', null, ['placeholder' => 'Upisite ime novog tehničara'], null, 'col-12', 'col-3', 'col-9') }}

                                {{ Form::textGroup('mobile_number', 'Broje telefona', null, ['placeholder' => 'Upisite broj telefona'], null, 'col-12', 'col-3', 'col-9') }}
                            </div>



                            <div class="modal-footer">
                                <button type="button" class="btn btn-light" data-dismiss="modal">Odustani</button>
                                {!! Form::button('Sačuvaj', ['id' =>'button-create-technician', 'type' =>'submit', 'class' => 'btn bg-teal-300 button-submit', 'data-loading-text' => 'Sačuvaj novog korisnika']) !!}
                            </div>
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
                <!-- /edit modal -->

                <!-- Active modal -->
                <div id="modal_activate" class="modal fade" tabindex="-1">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title">Vraćanje u aktivne tehničare</h5>
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                            </div>

                            <div class="modal-body">
                                {!! Form::open(['action' => ['TechniciansController@update', 'activate'], 'method' => 'POST']) !!}
                                {{ csrf_field() }}
                                {{ method_field('PUT')}}
                                {{Form::hidden ('technician_id', 'activate', ['id' => 'technician_id'])}}
                                {{--{{ Form::textGroup('name', 'Da li ste sigurni da želite da izbrišete tehničara "Aleksandar Jovanovic"', null, ['placeholder' => 'Upisite ime novog tehničara'], null, 'col-12', 'col-12', 'col-0') }}--}}
                                <p>Da li ste sigurni da želite da tehničara "<span id="technician_name"></span>" vratite u aktivne?</p>
                            </div>

                            <div class="modal-footer">
                                <button type="button" class="btn btn-light" data-dismiss="modal">Odustani</button>
                                {!! Form::button('Vrati u aktivne', ['type' => 'submit', 'id' =>'button-create-technician', 'class' => 'btn bg-teal-300 button-submit', 'data-loading-text' => 'Sačuvaj novog korisnika']) !!}
                            </div>
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>

                <!-- /active modal -->

                <br>

                <!-- Basic datatable -->
                <div class="card">

                    <table class="table datatable-basic">
                        <thead>
                        <tr>
                            <th>Ime i prezime tehničara</th>
                            <th>Broj telefona</th>
                            <th class="text-center">Izmena / Vraćanje u aktivne</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($technicians as $technician)
                            <tr>
                                <td>{{ $technician->technician_name }}</td>
                                <td>{{ $technician->mobile_number }}</td>
                                <td class="text-center">
                                    <div class="list-icons">
                                        <div class="dropdown">
                                            <a href="#" class="list-icons-item" data-toggle="dropdown">
                                                <i class="icon-menu9"></i>
                                            </a>
                                            <div class="dropdown-menu dropdown-menu-left">
                                                <button class="dropdown-item" data-alldata="{{$technician}}" data-toggle="modal" data-target="#modal_edit"><i class="icon-pencil7"></i>Izmeni</button>

                                                <button class="dropdown-item" data-alldata="{{$technician}}" data-toggle="modal" data-target="#modal_activate"><i class="icon-checkmark4"></i>Vraćanje u aktivne</button>


                                            </div>
                                        </div>
                                    </div>

                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
                <!-- /basic datatable -->

            </div>
        </div>
    </div>

@endsection



