@extends('layouts.app')

@section('content')

    @push('scripts')
        <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>

        <script src="../../global_assets/js/plugins/tables/datatables/datatables.min.js"></script>
        <script src="../../global_assets/js/demo_pages/datatables_basic.js"></script>
        <script src="../../global_assets/js/plugins/forms/selects/select2.min.js"></script>


        <script>
            $('.datatable-basic').DataTable({
                autoWidth: false,
                dom: '<"datatable-header"fl><"datatable-scroll"t><"datatable-footer"ip>',
                language: {
                    search: '<span>Pretraga:</span> _INPUT_',
                    lengthMenu: '<span>Prikaz:</span> _MENU_',
                    info: '_START_ do _END_ od ukupno _TOTAL_',
                    emptyTable: "Nema podataka",
                    paginate: { 'first': 'First', 'last': 'Last', 'next': '→', 'previous': '←' }
                }
            });

            $('.datatable-basic2').DataTable({
                autoWidth: false,
                dom: '<"datatable-header"fl><"datatable-scroll"t><"datatable-footer"ip>',
                order: [2, "desc"],
                language: {
                    search: '<span>Pretraga:</span> _INPUT_',
                    lengthMenu: '<span>Prikaz:</span> _MENU_',
                    info: '_START_ do _END_ od ukupno _TOTAL_',
                    emptyTable: "Nema podataka",
                    paginate: { 'first': 'First', 'last': 'Last', 'next': '→', 'previous': '←' }
                }
            });
        </script>
    @endpush

    <div>
        <div class="content">
            <div class="title m-b-md">
                <h1 style="text-align: center;">Pregled zaduzene opreme i materijala ze tehničara {{$technician_name}}</h1>
            </div>

            <div>

                <br>

                <!-- Zadužena oprema -->
                <div class="card mt-2">
                    <div class="card-header header-elements-inline">
                        <h5 class="card-title">Zadužena oprema</h5>

                    </div>

                    <table class="table datatable-basic table-bordered table-striped">
                        <thead>
                        <tr>
                            <th>Tip opreme</th>
                            <th>Model</th>
                            <th>S/N 1</th>
                            <th>S/N 2</th>
                            <th>S/N 3</th>
                            <th>Vreme zaduzvanja</th>
                        </tr>
                        </thead>

                        {{--{{ $technicians }}--}}
                        <tbody>
                        @foreach($assigned_equipments as $equipment)
                            <tr>
                                <td>{{ $equipment->equipment_subtype_name }}</td>
                                <td>{{ $equipment->equipment_model_name }}</td>
                                <td>{{ $equipment->equipment_serial1 }}</td>
                                <td>{{ $equipment->equipment_serial2 }}</td>
                                <td>{{ $equipment->equipment_serial3 }}</td>
                                <td>{{ datetimeForView($equipment->equipment_assign_date) }}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
                <!-- /Zadužena oprema -->

                <!-- Zadužen materijal -->
                <div class="card mt-2">
                    <div class="card-header header-elements-inline">
                        <h5 class="card-title">Zadužen materijal</h5>

                    </div>

                    <table class="table datatable-basic2 table-bordered table-striped">
                        <thead>
                        <tr>
                            <th>Naziv materijala</th>
                            <th>Količina</th>
                            <th>Vreme zaduzvanja</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($assigned_wares as $wares)
                            <tr>
                                <td>{{ $wares->wares_name }}</td>
                                <td>{{ $wares->quantity }}</td>
                                <td>{{ datetimeForView($wares->created_at) }}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
                <!-- /Zadužena oprema -->

            </div>
        </div>
    </div>

@endsection



