@extends('layouts.app')

@section('content')

    @push('scripts')
        <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>

        <script src="../global_assets/js/plugins/tables/datatables/datatables.min.js"></script>
        <script src="../global_assets/js/demo_pages/datatables_basic.js"></script>
        <script src="../global_assets/js/plugins/forms/selects/select2.min.js"></script>


        <script>
            $('.datatable-basic').DataTable({
                autoWidth: false,
                dom: '<"datatable-header"fl><"datatable-scroll"t><"datatable-footer"ip>',
                language: {
                    search: '<span>Pretraga:</span> _INPUT_',
                    lengthMenu: '<span>Prikaz:</span> _MENU_',
                    info: '_START_ do _END_ od ukupno _TOTAL_',
                    paginate: { 'first': 'First', 'last': 'Last', 'next': '→', 'previous': '←' }
                }
            });
        </script>
    @endpush

    <div>
        <div class="content">
            <div class="title m-b-md">
                <h1 style="text-align: center;">Pregled zaduzenja</h1>
            </div>

            <div>
                {{--<button type="button" class="btn bg-teal-300" data-toggle="modal" data-target="#modal_add">Dodavanje novog tehničara <i class="icon-plus3 ml-1"></i></button>--}}

                <br>

                <!-- Basic datatable -->
                <div class="card mt-2">

                    <table class="table datatable-basic table-bordered table-striped">
                        <thead>
                        <tr>
                            <th>Ime i prezime tehničara</th>
                            <th></th>
                        </tr>
                        </thead>

                        {{--{{ $technicians }}--}}
                        <tbody>
                        @foreach($technicians as $technician)
                            <tr>
                                <td>{{ $technician->technician_name }}</td>
                                <td class="text-center">
                                    <a class="btn btn-outline-dark" href="./{{$technician->technician_id}}" role="button">Prikaži zaduženja</a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                        {{--{{ /$technicians }}--}}
                    </table>
                </div>
                <!-- /basic datatable -->

            </div>
        </div>
    </div>

@endsection



