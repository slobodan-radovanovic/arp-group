@extends('layouts.app')

@section('content')

    @push('scripts')
        <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
                integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
                crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
                integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
                crossorigin="anonymous"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
                integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
                crossorigin="anonymous"></script>

        <script src="../global_assets/js/plugins/tables/datatables/datatables.min.js"></script>
        <script src="../global_assets/js/demo_pages/datatables_basic.js"></script>
        <script src="../global_assets/js/plugins/forms/selects/select2.min.js"></script>


        <script>
            $('.datatable-basic1').DataTable({
                autoWidth: false,
                dom: '<"datatable-header"fl><"datatable-scroll"t><"datatable-footer"ip>',
                language: {
                    search: '<span>Pretraga:</span> _INPUT_',
                    lengthMenu: '<span>Prikaz:</span> _MENU_',
                    info: '_START_ do _END_ od ukupno _TOTAL_',
                    paginate: {'first': 'First', 'last': 'Last', 'next': '→', 'previous': '←'}
                }
            });

            $('.datatable-basic2').DataTable({
                autoWidth: false,
                dom: '<"datatable-header"fl><"datatable-scroll"t><"datatable-footer"ip>',
                language: {
                    search: '<span>Pretraga:</span> _INPUT_',
                    lengthMenu: '<span>Prikaz:</span> _MENU_',
                    info: '_START_ do _END_ od ukupno _TOTAL_',
                    paginate: {'first': 'First', 'last': 'Last', 'next': '→', 'previous': '←'}
                }
            });

            $('.datatable-basic3').DataTable({
                autoWidth: false,
                dom: '<"datatable-header"fl><"datatable-scroll"t><"datatable-footer"ip>',
                language: {
                    search: '<span>Pretraga:</span> _INPUT_',
                    lengthMenu: '<span>Prikaz:</span> _MENU_',
                    info: '_START_ do _END_ od ukupno _TOTAL_',
                    paginate: {'first': 'First', 'last': 'Last', 'next': '→', 'previous': '←'}
                }
            });


            $('#modal_edit').on('show.bs.modal', function (event) {
                var button = $(event.relatedTarget);

                var service = button.data('alldata');
                var modal = $(this);
                console.log(service);

                modal.find('.modal-body #service_id').val(service.service_id);
                modal.find('.modal-body #service_name').val(service.service_name);
                modal.find('.modal-body #price_private_vehicle').val(service.price_private_vehicle);
                modal.find('.modal-body #for_invoicing_apartment').val(service.for_invoicing_apartment);
                modal.find('.modal-body #for_invoicing_house').val(service.for_invoicing_house);

                if (service.price_fixed == null) {
                    modal.find('.modal-body #fixed_price_edit').hide();
                    modal.find('.modal-body #no_fixed_price_edit').show();
                    modal.find('.modal-body #price_apartment').val(service.price_apartment);
                    modal.find('.modal-body #price_house').val(service.price_house);
                    modal.find('.modal-body #price_first_equipment').val(service.price_first_equipment);
                    modal.find('.modal-body #price_additional_equipment').val(service.price_additional_equipment);
                } else {
                    modal.find('.modal-body #fixed_price_edit').show();
                    modal.find('.modal-body #no_fixed_price_edit').hide();
                    modal.find('.modal-body #price_fixed').val(service.price_fixed);
                }


                /*modal.find('.modal-body #wares_id').val(ware.wares_id);
                modal.find('.modal-body #wares_code').val(ware.wares_code);
                modal.find('.modal-body #wares_name').val(ware.wares_name);

                modal.find('#wares_type_edit option:selected').removeAttr("selected");
                modal.find('#wares_type_edit option[value="'+ware.wares_type+'"]').attr('selected', 'selected');
                var selected_type_text = $( "#wares_type_edit option:selected" ).text();
                modal.find('span[id^="select2-wares_type_edit"]').prop('title',selected_type_text);
                modal.find('span[id^="select2-wares_type_edit"]').prop('innerText',selected_type_text);*/


            });

            $('#modal_edit_reklamacija').on('show.bs.modal', function (event) {
                var button = $(event.relatedTarget);

                var service = button.data('alldata');
                var modal = $(this);
                console.log(service);

                modal.find('.modal-body #price_fixed').val(service.price_fixed);
                modal.find('.modal-body #service_id').val(service.service_id);
                modal.find('.modal-body #service_name').val(service.service_name);
                modal.find('.modal-body #price_private_vehicle').val(service.price_private_vehicle);
                modal.find('.modal-body #for_invoicing').val(service.for_invoicing);

            });

            $('#modal_edit_reklamacija_po_stavkama').on('show.bs.modal', function (event) {
                var button = $(event.relatedTarget);

                var service = button.data('alldata');
                var modal = $(this);
                console.log(service);

                modal.find('.modal-body #service_id').val(service.service_id);
                modal.find('.modal-body #service_name').val(service.service_name);
                modal.find('.modal-body #price_private_vehicle').val(service.price_private_vehicle);

            });

            $('#modal_edit_izgradnja_po_etazi').on('show.bs.modal', function (event) {
                var button = $(event.relatedTarget);
                var service = button.data('alldata');
                var modal = $(this);
                console.log(service);

                modal.find('.modal-body #price_fixed').val(service.price_fixed);
                modal.find('.modal-body #service_id').val(service.service_id);
                modal.find('.modal-body #service_name').val(service.service_name);
                modal.find('.modal-body #price_private_vehicle').val(service.price_private_vehicle);
                modal.find('.modal-body #for_invoicing').val(service.for_invoicing);
            });

            $('#modal_edit_rekonstrukcija_korisnika').on('show.bs.modal', function (event) {
                var button = $(event.relatedTarget);
                var service = button.data('alldata');
                var modal = $(this);
                console.log(service);

                modal.find('.modal-body #price_fixed').val(service.price_fixed);
                modal.find('.modal-body #service_id').val(service.service_id);
                modal.find('.modal-body #service_name').val(service.service_name);
                modal.find('.modal-body #price_private_vehicle').val(service.price_private_vehicle);
                modal.find('.modal-body #for_invoicing').val(service.for_invoicing);
            });

            $('#modal_edit_izgradnja_po_metru').on('show.bs.modal', function (event) {
                var button = $(event.relatedTarget);
                var service = button.data('alldata');
                var modal = $(this);
                console.log(service);

                modal.find('.modal-body #price_fixed').val(service.price_fixed);
                modal.find('.modal-body #service_id').val(service.service_id);
                modal.find('.modal-body #service_name').val(service.service_name);
                modal.find('.modal-body #price_private_vehicle').val(service.price_private_vehicle);
                modal.find('.modal-body #for_invoicing').val(service.for_invoicing);
            });
            //visak
            /*$('#modal_edit_reclamation_type').on('show.bs.modal', function (event) {
                var button = $(event.relatedTarget);

                var service = button.data('alldata');
                var modal = $(this);
                console.log(service);

                modal.find('.modal-body #service_id').val(service.service_id);
                modal.find('.modal-body #service_name').val(service.service_name);
                modal.find('.modal-body #price_private_vehicle').val(service.price_private_vehicle);
                modal.find('.modal-body #for_invoicing').val(service.for_invoicing);
                modal.find('.modal-body #price_apartment').val(service.price_apartment);
                modal.find('.modal-body #price_house').val(service.price_house);
                modal.find('.modal-body #price_fixed').val(service.price_fixed);

            });*/

            $('#modal_edit_digitization_type').on('show.bs.modal', function (event) {
                var button = $(event.relatedTarget);

                var service = button.data('alldata');
                var modal = $(this);
                console.log(service);

                modal.find('.modal-body #service_id').val(service.service_id);
                modal.find('.modal-body #service_name').val(service.service_name);
                modal.find('.modal-body #price_private_vehicle').val(service.price_private_vehicle);
                modal.find('.modal-body #for_invoicing').val(service.for_invoicing);
                modal.find('.modal-body #price_fixed').val(service.price_fixed);

            });


            /*\service_name'price_apartment'\price_house'\price_fixed'\price_private_vehicle'\for_invoicing'*/


            function is_fixed_price() {
                if ($("#price_fixed_checkbox").is(':checked')) {
                    $('#fixed_price').show();
                    $("#no_fixed_price").hide();
                } else {
                    $('#fixed_price').hide();
                    $("#no_fixed_price").show();
                }
            }

            is_fixed_price();

            $('#price_fixed_checkbox').change(function () {
                is_fixed_price();
            });

        </script>

        {{-- dodatno ubaceno--}}
        <script src="../global_assets/js/plugins/ui/moment/moment.min.js"></script>
        <script src="../global_assets/js/plugins/pickers/anytime.min.js"></script>

        <script src="../assets/js/custom_datepicker.js"></script>

        <!-- Load select2 -->
        <script type="text/javascript" src="../global_assets/js/plugins/forms/selects/select2.min.js"></script>
        <script type="text/javascript"
                src="../global_assets/js/plugins/extensions/jquery_ui/interactions.min.js"></script>
        <script src="../assets/js/custom_select2.js"></script>
    @endpush

    <div>
        <div class="content">
            <div class="title m-b-md">
                <h1 style="text-align: center;">Pregled vrsta posla</h1>
            </div>

            @if ($errors->any())
                <div class="alert alert-danger">
                    @foreach ($errors->all() as $error)
                        <div>- {{ $error }}</div>
                    @endforeach
                </div>
            @endif

            <div>
                <button type="button" class="btn bg-teal-300" data-toggle="modal" data-target="#modal_add">Dodavanje
                    nove vrste posla za SBB nalog<i class="icon-plus3 ml-1"></i></button>

               {{-- <button type="button" class="btn bg-teal-300 ml-2" data-toggle="modal"
                        data-target="#modal_add_reclamation">Dodavanje nove vrste posla za reklamaciju<i
                            class="icon-plus3 ml-1"></i></button>--}}

                <!-- Add modal -->
                <div id="modal_add" class="modal fade" tabindex="-1">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title">Dodavanje nove vrste posla za SBB nalog</h5>
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                            </div>

                            <div class="modal-body">
                                {!! Form::open(['action' => 'ServicesController@store', 'method' => 'POST']) !!}
                                <div class="row mt-2 mb-2">
                                    <div class="col-3">{!! Form::label("price_fixed_checkbox", "Fiksna cena", ['class' => 'control-label']) !!}</div>
                                    <div class="col-9">{{ Form::checkbox("price_fixed_checkbox", 1, null)}}</div>
                                </div>

                                {{ Form::textGroup('service_name', 'Ime tipa posla', null, ['placeholder' => 'Upisite ime vrste posla'], null, 'col-12', 'col-5', 'col-7') }}

                                <div id="no_fixed_price">
                                    {{ Form::textGroup('price_apartment', 'Cena ako je stan', null, ['placeholder' => 'Upisite cenu'], null, 'col-12', 'col-5', 'col-7') }}

                                    {{ Form::textGroup('price_house', 'Cena ako je kuća', null, ['placeholder' => 'Upisite cenu'], null, 'col-12', 'col-5', 'col-7') }}
                                    {{ Form::textGroup('price_first_equipment', 'Cena za ugradjenu prvu opremu', null, ['placeholder' => 'Upisite cenu'], null, 'col-12', 'col-5', 'col-7') }}
                                    {{ Form::textGroup('price_additional_equipment', 'Cena za svaku sledecu opremu', null, ['placeholder' => 'Upisite cenu'], null, 'col-12', 'col-5', 'col-7') }}

                                </div>
                                <div id="fixed_price">
                                    {{ Form::textGroup('price_fixed', 'Cena', null, ['placeholder' => 'Upisite cenu'], null, 'col-12', 'col-5', 'col-7') }}
                                </div>
                                {{ Form::textGroup('price_private_vehicle', 'Kada se koristi privatno vozilo', null, ['placeholder' => 'Upisite cenu'], null, 'col-12', 'col-5', 'col-7') }}

                                {{ Form::textGroup('for_invoicing_apartment', 'Za fakturisanje ako je stan', null, ['placeholder' => 'Upisite cenu'], null, 'col-12', 'col-5', 'col-7') }}

                                {{ Form::textGroup('for_invoicing_house', 'Za fakturisanje ako je kuća', null, ['placeholder' => 'Upisite cenu'], null, 'col-12', 'col-5', 'col-7') }}


                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-light" data-dismiss="modal">Odustani</button>
                                {!! Form::button('Dodaj', ['type' => 'submit', 'id' =>'button-create-technician', 'class' => 'btn bg-teal-300 button-submit', 'data-loading-text' => 'Sačuvaj novog korisnika']) !!}
                            </div>
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
                <!-- /add modal -->

                <!-- Add reclamation modal -->
               {{-- <div id="modal_add_reclamation" class="modal fade" tabindex="-1">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title">Dodavanje nove vrste posla za reklamacije</h5>
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                            </div>

                            <div class="modal-body">
                                {!! Form::open(['action' => 'ServicesController@store_reclamation', 'method' => 'POST']) !!}
                                {{ Form::textGroup('service_name', 'Ime tipa posla', null, ['placeholder' => 'Upisite ime vrste posla'], null, 'col-12', 'col-5', 'col-7') }}


                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-light" data-dismiss="modal">Odustani</button>
                                {!! Form::button('Dodaj', ['type' => 'submit', 'id' =>'button-create-technician', 'class' => 'btn bg-teal-300 button-submit', 'data-loading-text' => 'Sačuvaj novog korisnika']) !!}
                            </div>
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>--}}
                <!-- /add reclamation modal -->

                <!-- Edit modal -->
                <div id="modal_edit" class="modal fade" tabindex="-1">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title">Izmena vrste posla</h5>
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                            </div>

                            <div class="modal-body">
                                {!! Form::open(['action' => ['ServicesController@update', 'update_sbb'], 'method' => 'POST']) !!}
                                {{ csrf_field() }}
                                {{ method_field('PUT')}}
                                {{Form::hidden ('service_id', 'update_sbb', ['id' => 'service_id'])}}


                                {{ Form::textGroup('service_name', 'Ime tipa posla', null, ['placeholder' => 'Upisite ime vrste posla'], null, 'col-12', 'col-5', 'col-7') }}

                                <div id="no_fixed_price_edit">
                                    {{ Form::textGroup('price_apartment', 'Cena ako je stan', null, ['placeholder' => 'Upisite cenu'], null, 'col-12', 'col-5', 'col-7') }}

                                    {{ Form::textGroup('price_house', 'Cena ako je kuća', null, ['placeholder' => 'Upisite cenu'], null, 'col-12', 'col-5', 'col-7') }}
                                    {{ Form::textGroup('price_first_equipment', 'Cena za ugradjenu prvu opremu', null, ['placeholder' => 'Upisite cenu'], null, 'col-12', 'col-5', 'col-7') }}
                                    {{ Form::textGroup('price_additional_equipment', 'Cena za svaku sledecu opremu', null, ['placeholder' => 'Upisite cenu'], null, 'col-12', 'col-5', 'col-7') }}

                                </div>
                                <div id="fixed_price_edit">
                                    {{ Form::textGroup('price_fixed', 'Cena', null, ['placeholder' => 'Upisite cenu'], null, 'col-12', 'col-5', 'col-7') }}
                                </div>
                                {{ Form::textGroup('price_private_vehicle', 'Kada se koristi privatno vozilo', null, ['placeholder' => 'Upisite cenu'], null, 'col-12', 'col-5', 'col-7') }}

                                {{ Form::textGroup('for_invoicing_apartment', 'Za fakturisanje ako je stan', null, ['placeholder' => 'Upisite cenu'], null, 'col-12', 'col-5', 'col-7') }}

                                {{ Form::textGroup('for_invoicing_house', 'Za fakturisanje ako je kuća', null, ['placeholder' => 'Upisite cenu'], null, 'col-12', 'col-5', 'col-7') }}


                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-light" data-dismiss="modal">Odustani</button>
                                {!! Form::button('Izmeni tip posla', ['type' => 'submit', 'id' =>'button-create-technician', 'class' => 'btn bg-teal-300 button-submit', 'data-loading-text' => 'Izmeni tip posla']) !!}
                            </div>
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
                <!-- /edit modal -->


                <!-- Edit  reklamacija_po_stavkama modal -->

                <div id="modal_edit_reklamacija_po_stavkama" class="modal fade" tabindex="-1">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title">Izmena vrste posla</h5>
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                            </div>

                            <div class="modal-body">
                                {!! Form::open(['action' => ['ServicesController@update', 'update_reklamacija_po_stavkama'], 'method' => 'POST']) !!}
                                {{ csrf_field() }}
                                {{ method_field('PUT')}}
                                {{Form::hidden ('service_id', 'update_reklamacija_po_stavkama', ['id' => 'service_id'])}}


                                {{ Form::textGroup('service_name', 'Ime tipa posla', null, ['placeholder' => '', 'readonly' => ''], null, 'col-12', 'col-5', 'col-7') }}
                                {{ Form::textGroup('price_private_vehicle', 'Kada se koristi privatno vozilo', null, ['placeholder' => ''], null, 'col-12', 'col-5', 'col-7') }}
                            </div>


                            <div class="modal-footer">
                                <button type="button" class="btn btn-light" data-dismiss="modal">Odustani</button>
                                {!! Form::button('Izmeni tip posla', ['type' => 'submit', 'id' =>'button-create-technician', 'class' => 'btn bg-teal-300 button-submit', 'data-loading-text' => 'Izmeni tip posla']) !!}
                            </div>
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>

                <!-- /edit reklamacija_po_stavkama modal -->

                <!-- Edit  reklamacija modal -->

                <div id="modal_edit_reklamacija" class="modal fade" tabindex="-1">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title">Izmena vrste posla</h5>
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                            </div>

                            <div class="modal-body">
                                {!! Form::open(['action' => ['ServicesController@update', 'update_reclamation'], 'method' => 'POST']) !!}
                                {{ csrf_field() }}
                                {{ method_field('PUT')}}
                                {{Form::hidden ('service_id', 'update_reklamacija', ['id' => 'service_id'])}}


                                {{ Form::textGroup('service_name', 'Ime tipa posla', null, ['placeholder' => '', 'readonly' => ''], null, 'col-12', 'col-5', 'col-7') }}

                                {{ Form::textGroup('price_fixed', 'Cena za tehničara', null, ['placeholder' => ''], null, 'col-12', 'col-5', 'col-7') }}

                                {{ Form::textGroup('price_private_vehicle', 'Kada se koristi privatno vozilo', null, ['placeholder' => ''], null, 'col-12', 'col-5', 'col-7') }}

                                {{ Form::textGroup('for_invoicing', 'Za fakturisanje', null, ['placeholder' => ''], null, 'col-12', 'col-5', 'col-7') }}
                            </div>


                            <div class="modal-footer">
                                <button type="button" class="btn btn-light" data-dismiss="modal">Odustani</button>
                                {!! Form::button('Izmeni tip posla', ['type' => 'submit', 'id' =>'button-create-technician', 'class' => 'btn bg-teal-300 button-submit', 'data-loading-text' => 'Izmeni tip posla']) !!}
                            </div>
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>

                <!-- /edit reklamacija modal -->

                <!-- Edit  izgradnja_po_etazi modal -->

                <div id="modal_edit_izgradnja_po_etazi" class="modal fade" tabindex="-1">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title">Izmena vrste posla</h5>
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                            </div>

                            <div class="modal-body">
                                {!! Form::open(['action' => ['ServicesController@update', 'update_reclamation'], 'method' => 'POST']) !!}
                                {{ csrf_field() }}
                                {{ method_field('PUT')}}
                                {{Form::hidden ('service_id', 'update_izgradnja_po_etazi', ['id' => 'service_id'])}}


                                {{ Form::textGroup('service_name', 'Ime tipa posla', null, ['placeholder' => '', 'readonly' => ''], null, 'col-12', 'col-5', 'col-7') }}

                                {{ Form::textGroup('price_fixed', 'Cena za tehničara po etaži', null, ['placeholder' => ''], null, 'col-12', 'col-5', 'col-7') }}

                                {{ Form::textGroup('price_private_vehicle', 'Kada se koristi privatno vozilo', null, ['placeholder' => ''], null, 'col-12', 'col-5', 'col-7') }}

                                {{ Form::textGroup('for_invoicing', 'Za fakturisanje po etaži', null, ['placeholder' => ''], null, 'col-12', 'col-5', 'col-7') }}
                            </div>


                            <div class="modal-footer">
                                <button type="button" class="btn btn-light" data-dismiss="modal">Odustani</button>
                                {!! Form::button('Izmeni tip posla', ['type' => 'submit', 'id' =>'button-create-technician', 'class' => 'btn bg-teal-300 button-submit', 'data-loading-text' => 'Izmeni tip posla']) !!}
                            </div>
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>

                <!-- /edit izgradnja_po_etazi modal -->

                <!-- Edit  rekonstrukcija_korisnika modal -->

                <div id="modal_edit_rekonstrukcija_korisnika" class="modal fade" tabindex="-1">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title">Izmena vrste posla</h5>
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                            </div>

                            <div class="modal-body">
                                {!! Form::open(['action' => ['ServicesController@update', 'update_reclamation'], 'method' => 'POST']) !!}
                                {{ csrf_field() }}
                                {{ method_field('PUT')}}
                                {{Form::hidden ('service_id', 'update_rekonstrukcija_korisnika', ['id' => 'service_id'])}}


                                {{ Form::textGroup('service_name', 'Ime tipa posla', null, ['placeholder' => '', 'readonly' => ''], null, 'col-12', 'col-5', 'col-7') }}

                                {{ Form::textGroup('price_fixed', 'Cena za tehničara po korisniku', null, ['placeholder' => ''], null, 'col-12', 'col-5', 'col-7') }}

                                {{ Form::textGroup('price_private_vehicle', 'Kada se koristi privatno vozilo', null, ['placeholder' => ''], null, 'col-12', 'col-5', 'col-7') }}

                                {{ Form::textGroup('for_invoicing', 'Za fakturisanje po korisniku', null, ['placeholder' => ''], null, 'col-12', 'col-5', 'col-7') }}
                            </div>


                            <div class="modal-footer">
                                <button type="button" class="btn btn-light" data-dismiss="modal">Odustani</button>
                                {!! Form::button('Izmeni tip posla', ['type' => 'submit', 'id' =>'button-create-technician', 'class' => 'btn bg-teal-300 button-submit', 'data-loading-text' => 'Izmeni tip posla']) !!}
                            </div>
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>

                <!-- /edit rekonstrukcija_korisnika modal -->

                <!-- Edit  izgradnja_po_metru modal -->

                <div id="modal_edit_izgradnja_po_metru" class="modal fade" tabindex="-1">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title">Izmena vrste posla</h5>
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                            </div>

                            <div class="modal-body">
                                {!! Form::open(['action' => ['ServicesController@update', 'update_reclamation'], 'method' => 'POST']) !!}
                                {{ csrf_field() }}
                                {{ method_field('PUT')}}
                                {{Form::hidden ('service_id', 'update_izgradnja_po_metru', ['id' => 'service_id'])}}


                                {{ Form::textGroup('service_name', 'Ime tipa posla', null, ['placeholder' => '', 'readonly' => ''], null, 'col-12', 'col-5', 'col-7') }}

                                {{ Form::textGroup('price_fixed', 'Cena za tehničara po metru', null, ['placeholder' => ''], null, 'col-12', 'col-5', 'col-7') }}

                                {{ Form::textGroup('price_private_vehicle', 'Kada se koristi privatno vozilo', null, ['placeholder' => ''], null, 'col-12', 'col-5', 'col-7') }}

                                {{ Form::textGroup('for_invoicing', 'Za fakturisanje po metru', null, ['placeholder' => ''], null, 'col-12', 'col-5', 'col-7') }}
                            </div>


                            <div class="modal-footer">
                                <button type="button" class="btn btn-light" data-dismiss="modal">Odustani</button>
                                {!! Form::button('Izmeni tip posla', ['type' => 'submit', 'id' =>'button-create-technician', 'class' => 'btn bg-teal-300 button-submit', 'data-loading-text' => 'Izmeni tip posla']) !!}
                            </div>
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>

                <!-- /edit izgradnja_po_metru modal -->


                <!-- Edit digitization type modal -->
                <div id="modal_edit_digitization_type" class="modal fade" tabindex="-1">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title">Izmena vrste posla</h5>
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                            </div>

                            <div class="modal-body">
                                {!! Form::open(['action' => ['ServicesController@update', 'update_digitization_type'], 'method' => 'POST']) !!}
                                {{ csrf_field() }}
                                {{ method_field('PUT')}}
                                {{Form::hidden ('service_id', 'update_digitization_type', ['id' => 'service_id'])}}


                                {{ Form::textGroup('service_name', 'Ime tipa posla', null, ['placeholder' => '', 'readonly' => ''], null, 'col-12', 'col-5', 'col-7') }}

                                {{ Form::textGroup('price_fixed', 'Cena', null, ['placeholder' => ''], null, 'col-12', 'col-5', 'col-7') }}

                                {{ Form::textGroup('price_private_vehicle', 'Kada se koristi privatno vozilo', null, ['placeholder' => ''], null, 'col-12', 'col-5', 'col-7') }}

                                {{ Form::textGroup('for_invoicing', 'Za fakturisanje', null, ['placeholder' => ''], null, 'col-12', 'col-5', 'col-7') }}
                            </div>


                            <div class="modal-footer">
                                <button type="button" class="btn btn-light" data-dismiss="modal">Odustani</button>
                                {!! Form::button('Izmeni tip posla', ['type' => 'submit', 'id' =>'button-create-technician', 'class' => 'btn bg-teal-300 button-submit', 'data-loading-text' => 'Izmeni tip posla']) !!}
                            </div>
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
                <!-- /edit modal -->

                <br>

                <!-- sbb order datatable -->
                <div class="card mt-2">
                    <h5 class="ml-2 mt-2 mb-0">Vrste posla za SBB naloge</h5>
                    <table class="table datatable-basic1 table-bordered table-striped">
                        <thead>
                        <tr>
                            <th>Ime vrste posla</th>
                            <th>Izmena</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($data['sbb_order'] as $service)

                            {{--@dd($ware)--}}
                            <tr>

                                <td>{{$service->service_name}}</td>

                                <td class="text-center">
                                    <a href="#" class="dropdown-item" data-toggle="modal" data-target="#modal_edit"
                                       data-alldata="{{$service}}"> <i class="icon-pencil7"></i> {{--Izmeni--}}</a>


                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
                <!-- /sbb order datatable -->

                <!-- reklamacija datatable -->
                <div class="card mt-2">
                    <h5 class="ml-2 mt-2 mb-0">Reklamacije</h5>
                    <table class="table datatable-basic2 table-bordered table-striped">
                        <thead>
                        <tr>

                            <th>Ime vrste posla</th>
                            <th>Izmena</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($data['reclamation'] as $service)

                            {{--@dd($ware)--}}
                            <tr>

                                <td>{{$service->service_name}}</td>


                                <td class="text-center">
                                    <a href="#" class="dropdown-item" data-toggle="modal"
                                       data-target="#modal_edit_{{$service->service_select}}" data-alldata="{{$service}}"> <i
                                                class="icon-pencil7"></i> {{--Izmeni--}}</a>
                                </td>

                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
                <!-- /reklamacija datatable -->

                <!-- reklamacija type datatable -->
               {{-- <div class="card mt-2">
                    <h5 class="ml-2 mt-2 mb-0">Reklamacije</h5>
                    <table class="table datatable-basic table-bordered table-striped">
                        <thead>
                        <tr>

                            <th>Ime vrste posla</th>
                            <th>Izmena</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($data['reclamation_type'] as $service)

                            --}}{{--@dd($ware)--}}{{--
                            <tr>

                                <td>{{$service->service_name}}</td>

                                <td class="text-center">
                                    <a href="#" class="dropdown-item" data-toggle="modal"
                                       data-target="#modal_edit_reclamation_type" data-alldata="{{$service}}"> <i
                                                class="icon-pencil7"></i> --}}{{--Izmeni--}}{{--</a>


                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>--}}
                <!-- /reklamacija type datatable -->

                <!-- digitalizacija datatable -->
                <div class="card mt-2">
                    <h5 class="ml-2 mt-2 mb-0">Digitalzacija</h5>
                    <table class="table datatable-basic table-bordered table-striped">
                        <thead>
                        <tr>

                            <th>Ime vrste posla</th>
                            <th>Izmena</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($data['digitization_type'] as $service)

                            {{--@dd($ware)--}}
                            <tr>

                                <td>{{$service->service_name}}</td>

                                <td class="text-center">
                                    <a href="#" class="dropdown-item" data-toggle="modal"
                                       data-target="#modal_edit_digitization_type" data-alldata="{{$service}}"> <i
                                                class="icon-pencil7"></i> {{--Izmeni--}}</a>


                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
                <!-- /digitalizacija datatable -->


            </div>
        </div>
    </div>

@endsection



