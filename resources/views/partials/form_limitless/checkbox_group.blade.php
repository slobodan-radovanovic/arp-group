@stack($name . '_input_start')

<div class="form-group {{ $col }} {{ isset($attributes['required']) ? 'required' : '' }} {{ $errors->has($name) ? 'has-error' : '' }}">
    {!! Form::label($name, $text, ['class' => 'control-label']) !!}
    <br/>
    <div class="row">
        @foreach($items as $item)
            <div class="col-md-3">
                <div class="form-check">
                    <label class="/*form-check-label*/ input-checkbox">
                        {{ Form::checkbox($name . '[]', $item->$id, null, ['class' => 'form-check-input-styled', 'data-fouc']) }} &nbsp; {{ $item->$value }}
                    </label>
                </div>
            </div>
        @endforeach
    </div>
    {!! $errors->first($name, '<span class="form-text text-danger"><i class="icon-cancel-circle2 mr-2"></i><strong>:message</strong></span>') !!}
</div>

@stack($name . '_input_end')