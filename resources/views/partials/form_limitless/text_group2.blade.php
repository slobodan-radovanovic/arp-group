@stack($name . '_input_start')
<div class="{{ $col }}">
    <div class="form-group">
        {{ Form::label($name, $labelvalue, ['class' => $classlabel]) }}
        {{ Form::text($name, $value, array_merge(['class' => $classtext], $attributes)) }}
    </div>
</div>
@stack($name . '_input_end')