@stack($name . '_input_start')
{{--<div class="">--}}
                <div class="{{ $col }}">
                    @if($show_label)
                    {{ Form::label($name, $labelvalue, ['class' => $classlabel]) }}
                    @endif
                    {{ Form::select($name, array_merge(['' => ''], $values), $selected, $attributes) }}

                </div>
              {{--</div>--}}

{{--<div class="form-row mb-2">
    <select name="eq_model2" class="form-control select" data-fouc data-placeholder="Izaberite tip opreme">
        <option>
        <option value="PLAN007">D3 1 way receiver
        <option value="PLAN009">Cable home gateway
        <option value="PLAN0014">Proffesional gateway
    </select>
</div>--}}

@stack($name . '_input_end')