<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<!-- CSRF Token -->
<meta name="csrf-token" content="{{ csrf_token() }}">
<title>@yield('title')</title>

<!-- Global stylesheets -->
<link href="{{ asset('css/theme.css') }}" rel="stylesheet" media="screen">
<link href="{{ asset('css/app.css') }}" rel="stylesheet">
<!-- /global stylesheets -->

{{--<script src="/js/app.js"></script>--}}
<!-- Core JS files -->
{{--<script src="/vendor/limitless/global_assets/js/main/jquery.min.js"></script>--}}
{{--<script src="/vendor/limitless/global_assets/js/main/bootstrap.bundle.min.js"></script>--}}
{{--<script src="/vendor/limitless/global_assets/js/plugins/loaders/blockui.min.js"></script>--}}
{{--<!-- /core JS files -->--}}

{{--<!-- Theme JS files -->--}}
{{--<script src="/vendor/limitless/global_assets/js/plugins/visualization/d3/d3.min.js"></script>--}}
{{--<script src="/vendor/limitless/global_assets/js/plugins/visualization/d3/d3_tooltip.js"></script>--}}
{{--<script src="/vendor/limitless/global_assets/js/plugins/forms/styling/switchery.min.js"></script>--}}
{{--<script src="/vendor/limitless/global_assets/js/plugins/forms/selects/bootstrap_multiselect.js"></script>--}}
{{--<script src="/vendor/limitless/global_assets/js/plugins/ui/moment/moment.min.js"></script>--}}
{{--<script src="/vendor/limitless/global_assets/js/plugins/pickers/daterangepicker.js"></script>--}}

{{--<script src="https://cdn.jsdelivr.net/npm/js-cookie@2/src/js.cookie.min.js"></script>--}}
<script src="/vendor/limitless/assets/js/app.js"></script>
{{--<script src="/vendor/limitless/global_assets/js/demo_pages/dashboard.js"></script>--}}
<!-- /theme JS files -->
{{--<script src="/vendor/limitless/global_assets/js/plugins/tables/datatables/datatables.min.js"></script>--}}


{{--<script src="https://cdn.datatables.net/1.10.18/js/jquery.dataTables.min.js"></script>--}}
{{--<script src="https://cdn.datatables.net/1.10.18/js/dataTables.bootstrap4.min.js"></script>--}}

