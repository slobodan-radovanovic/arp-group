<?php
$rq = explode("/", Request::url());
$rq = array_splice($rq, 0, 6);
$rqq = implode($rq, "/");
?>
@if(array_key_exists('submenu', $menuitem))
    <li class="nav-item nav-item-submenu {{ (array_search($rqq, array_column($menuitem['submenu'], 'url', 'url'))) ? 'nav-item-open' : ''  }}">
        <a href="#" class="nav-link"><i class="{{ $menuitem['icon'] }}"></i> <span>{{ $menuitem['name'] }}</span></a>


        <ul class="nav nav-group-sub" data-submenu-title="{{ $menuitem['name'] }}" style="{{ (array_search($rqq, array_column($menuitem['submenu'], 'url', 'url'))) ? 'display: block;' : ''  }}">
            <li class="nav-item">
                @foreach($menuitem['submenu'] as $submenuitem)
                    @include('partials.limitless-sidebar-item', ['menuitem' => $submenuitem])
                @endforeach
            </li>
        </ul>

    </li>

@else
    <li class="nav-item">
        <a href="{{ $menuitem['url'] }}" class="nav-link {{ ($menuitem['url'] == $rqq) ? 'active' : ''}}">
            {{ $menuitem['name'] }}
        </a>
    </li>
@endif