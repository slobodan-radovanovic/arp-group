<!-- Main sidebar -->
<div class="sidebar sidebar-light sidebar-main sidebar-expand-md align-self-start no-print">

    <!-- Sidebar mobile toggler -->
    <div class="sidebar-mobile-toggler text-center">
        <a href="#" class="sidebar-mobile-main-toggle">
            <i class="icon-arrow-left8"></i>
        </a>
        <span class="font-weight-semibold">Main sidebar</span>
        <a href="#" class="sidebar-mobile-expand">
            <i class="icon-screen-full"></i>
            <i class="icon-screen-normal"></i>
        </a>
    </div>
    <!-- /sidebar mobile toggler -->


    <!-- Sidebar content -->
    <div class="sidebar-content">
        <div class="card card-sidebar-mobile">

            <!-- Header -->
            <!-- Main navigation -->
            <div class="card-body p-0">
                <ul class="nav nav-sidebar" data-nav-type="accordion">

              {{--  @foreach(mainMenuHelper() as $menuItem)
                    @include('partials.limitless-sidebar-item', ['menuitem' => $menuItem])
                @endforeach--}}

                <!-- RADNI MENU -->
                <li class="nav-item"><a {{--href="{{ route(('admin.language.index')) }}"--}} class="nav-link "><i class="icon-sphere"></i><span>@lang('menus.base.main_menu.languages')</span></a></li>
                <!-- /RADNI MENU -->

            </div>
            <!-- /main navigation -->

        </div>
    </div>
    <!-- /sidebar content -->

</div>
<!-- /main sidebar -->