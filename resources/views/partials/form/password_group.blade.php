@stack($name . '_input_start')

<div class="form-group form-group-feedback form-group-feedback-right {{ $col }} {{ isset($attributes['required']) ? 'required' : '' }} {{ $errors->has($name) ? 'has-error' : '' }}">
    {!! Form::password($name, array_merge(['class' => 'form-control '.($errors->has($name) ? 'border-danger' : ''), 'placeholder' => trans('labels.form.enter', ['field' => $text])], $attributes)) !!}
    @if ($errors->has($name))
        <div class="form-control-feedback text-danger"><i class="icon-cancel-circle2"></i></div>
    @else
        <div class="form-control-feedback"><i class="{{ $icon }} text-muted"></i></div>
    @endif
    {!! $errors->first($name, '<span class="form-text text-danger"><strong>:message</strong></span>') !!}
</div>

@stack($name . '_input_end')
