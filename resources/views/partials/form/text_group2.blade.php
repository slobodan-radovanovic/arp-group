@stack($name . '_input_start')
<div class="{{ $col }}">
    <div class="form-group">


        {{ Form::label($name, $labelvalue, ['class' => $classlabel]) }}
        @php
            echo ($required == true ? "<span class='text-danger'>*</span>" : "");
        @endphp
        {{ Form::text($name, $value, array_merge(['class' => $classtext], $attributes)) }}
    </div>
</div>
@stack($name . '_input_end')