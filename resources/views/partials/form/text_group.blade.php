@stack($name . '_input_start')

<div class="form-group row form-group-feedback form-group-feedback-right{{ $col }} {{ isset($attributes['required']) ? 'required' : '' }} {{ $errors->has($name) ? 'has-error' : '' }}">
    {!! Form::label($name, $text, ['class' => 'col-form-label '.$labelcol]) !!}
    @php
        echo ($required == true ? "<span class='text-danger'>*</span>" : "");
    @endphp

    {!! Form::text($name, $value, array_merge(['class' => 'form-control '.$inputcol/*.($errors->has($name) ? 'border-danger' : '')*/, 'placeholder' => trans('labels.form.enter', ['field' => $text])], $attributes)) !!}
    {{--@if ($errors->has($name))
        <div class="form-control-feedback text-danger"></div>
        @else
        <div class="form-control-feedback"><i class="{{ $icon }} text-muted"></i></div>
    @endif
    {!! $errors->first($name, '<span class="form-text text-danger"><strong>:message</strong></span>') !!}--}}
</div>

@stack($name . '_input_end')

