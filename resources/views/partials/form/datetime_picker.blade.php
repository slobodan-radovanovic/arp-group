@stack($name . '_input_start')

<div class="{{ $col }}">
    {{ Form::label($name, $labelvalue, ['class' => $classlabel]) }}
    @php
        echo ($required == true ? "<span class='text-danger'>*</span>" : "");
    @endphp
    <div class="input-group">
                  <span class="input-group-prepend">
                    <span class="input-group-text"><i class="icon-calendar3"></i></span>
                  </span>
        {{ Form::text($name, $value, array_merge(['class' => $classtext], $attributes)) }}

    </div>
</div>

@stack($name . '_input_end')