@stack($name . '_input_start')

{{--{{ Form::checkbox('enabled', 'value') }}--}}

<div class="form-check form-check-switch form-check-switch-left {{ $col }} {{ isset($attributes['required']) ? 'required' : '' }} {{ $errors->has($name) ? 'has-error' : '' }}">
    <label class="form-check-label d-flex align-items-center">
        {{ Form::checkbox($name, 1, null, [
            'class'=>'form-check-input-switch',
            'data-on-color' => 'success',
            'data-off-color' => 'danger',
            'data-on-text' => trans('labels.general.yes'),
            'data-off-text' => trans('labels.general.no'),
            ]
            )
        }}
        {!! Form::label($name, $text, ['class' => 'control-label']) !!}
    </label>
    {!! $errors->first($name, '<p class="help-block">:message</p>') !!}
</div>

@stack($name . '_input_end')

