@stack($name . '_input_start')

{{--<div class="{{ $col }}">--}}
    <div class="form-group {{ $col }}">
        {{ Form::label($name, $labelvalue, ['class' => $classlabel]) }}
        {{ Form::textarea($name, $value, array_merge(['class' => $classtextarea], $attributes)) }}
    </div>
{{--</div>--}}

{{--<div class="form-group">
    <label>Komentar:</label>
    <textarea rows="5" cols="5" id="order_comment" class="form-control" placeholder="Upisite komentar, napomenu"></textarea>
</div>--}}

@stack($name . '_input_end')
