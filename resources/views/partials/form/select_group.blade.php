@stack($name . '_input_start')

<div class="{{ $col }}">
    @if($show_label)
        {{ Form::label($name, $labelvalue, ['class' => $classlabel]) }}
    @endif
    {{ Form::select($name, array_merge(['' => ''], $values), $selected, $attributes) }}

</div>

{{--<div class="col-6">
    <label>Prvi tehnicar:</label><select name="service1" class="form-control select service_type1" data-fouc data-placeholder="Izaberite prvog tehnicara">
        <option>
        <option value="Pera Peri">Pera Peric
        <option value="Nikola Nikolic">Nikola Nikolic
        <option value="opt3">Marko Markovic
        <option value="opt4">Zarko Zarkovic
        <option value="opt5">Nenad Nenadovic
        <option value="opt6">Mirko Mirkovic
        <option value="opt7">Predrag Predragovic
    </select>
</div>--}}

@stack($name . '_input_end')
