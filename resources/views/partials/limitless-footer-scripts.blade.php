<script src="/js/app.js"></script>
<script src="{{ asset('vendor/limitless/global_assets/js/plugins/forms/styling/uniform.min.js') }}"></script>
<script src="{{ asset('vendor/limitless/global_assets/js/plugins/forms/selects/select2.min.js') }}"></script>
<script src="{{ asset('js/parsley.js') }}"></script>
<script src="{{ asset('vendor/limitless/global_assets/js/plugins/notifications/pnotify.min.js') }}"></script>
<script src="{{ asset('vendor/limitless/global_assets/js/plugins/notifications/sweet_alert.min.js') }}"></script>
<!--begin::Base Scripts -->
@stack('js')
<!--end::Base Scripts -->