<!-- Main navbar -->
<div class="navbar navbar-expand-md navbar-dark no-print">
    <div class="navbar-brand wmin-200">
        <a href="/" class="d-inline-block">
            <img src="/img/ENTEL-LOGO-AD-beli.png" alt="">
        </a>
    </div>

    <div class="d-md-none">
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar-mobile">
            <i class="icon-tree5"></i>
        </button>
        <button class="navbar-toggler sidebar-mobile-main-toggle" type="button">
            <i class="icon-paragraph-justify3"></i>
        </button>
    </div>

    <div class="collapse navbar-collapse" id="navbar-mobile">
        <ul class="navbar-nav">
            <li class="nav-item">
                <a href="#" class="navbar-nav-link sidebar-control sidebar-main-toggle d-none d-md-block">
                    <i class="icon-paragraph-justify3"></i>
                </a>
            </li>
        </ul>

        <span class="navbar-text ml-md-auto mr-md-3">
        </span>

        <ul class="navbar-nav">

            <li class="nav-item dropdown dropdown-user">
                <a href="#" class="navbar-nav-link dropdown-toggle" data-toggle="dropdown">
                    <img src="/img/flag-{{App::getLocale()}}.png">
                </a>
                <div class="dropdown-menu dropdown-menu-right">
                    <a href="{{ str_replace('/rs/', '/en/',Request::url()) }}" class="dropdown-item">
                        <span class="m-nav__link-icon"><img class="m-topbar__language-img" src="/img/flag-en.png"></span>
                        <span class="m-nav__link-title m-topbar__language-text m-nav__link-text">{{__('Engleski') }}</span>
                    </a>

                    <a href="{{ str_replace('/en/', '/rs/',Request::url()) }}" class="dropdown-item">
                        <span class="m-nav__link-icon"><img class="m-topbar__language-img" src="/img/flag-rs.png"></span>
                        <span class="m-nav__link-title m-topbar__language-text m-nav__link-text">{{__('Srpski') }}</span>
                    </a>
                </div>
            </li>
        </ul>

        <ul class="navbar-nav">

            <li class="nav-item dropdown dropdown-user">
                <a href="#" class="navbar-nav-link dropdown-toggle" data-toggle="dropdown">
                    <i class="icon-user"></i>
                  {{--  <span>{{  auth()->user()->name .' - '. auth()->user()->zemlja }}</span>--}}
                </a>

                <div class="dropdown-menu dropdown-menu-right">
                    <a href="#" onclick="event.preventDefault(); document.getElementById('logout-form').submit();" class="dropdown-item"><i class="icon-switch2"></i> Logout</a>

                    <form id="logout-form" action="{{ url('logout') }}" method="POST" style="display: none;">
                        {{ csrf_field() }}
                    </form>

                </div>
            </li>
        </ul>
    </div>
</div>
<!-- /main navbar -->