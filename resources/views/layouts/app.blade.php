<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>ARP Group</title>

   {{-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>
    <script src="http://parsleyjs.org/dist/parsley.js"></script>--}}

    <!-- Styles -->
    @include('inc.style')

    @stack('style')

</head>

<body>

@include('inc.preloader')

@include('inc.navbar-top')
    <div class="page-content">

        @include('inc.navbar-left')

        <!-- Main content -->
            <div class="content-wrapper">
                <div class="content pt-0">
                    <!-- Morao sam da spustim prikaz begao je gore -->
                    <!-- <br><br><br><br><br><br><br><br> -->
                    @yield('content')
                </div>
            </div>
        <!-- /main content -->


    </div>
<!-- Scripts -->
@include('inc.scripts')
@stack('scripts')

</body>
</html>
