<!DOCTYPE html>
<html lang="en">
<head>
    @include('partials.limitless-head')
</head>

<body {{--{{Cookie::get('sidebar_toggle_state') == 'on' ? 'class=sidebar-xs':'' }}--}}>

@include('partials.limitless-aside-menu')


@include('partials.limitless-header')

{{--<div id="app">--}}
<!-- Page content -->
<div class="page-content pt-0" id="app">

@include('partials.limitless-sidebar')


<!-- Main content -->
    <div class="content-wrapper">

        <!-- Content area -->
        <div class="content">

            @yield('content')

        </div>
        <!-- /content area -->

    </div>
    <!-- /main content -->
    <flash message="{{ session('flash_message') }}"></flash>
</div>
<!-- /page content -->
{{--</div>--}}

@include('partials.limitless-footer')
@include('partials.limitless-footer-scripts')

</body>
</html>
