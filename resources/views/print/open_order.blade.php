<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Štampanje naloga {{ $data['order']->ordinal_number }}</title>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>
    <script src="http://www.position-absolute.com/creation/print/jquery.printPage.js"></script>
    <script>
        window.onload = function() { window.print(); }
        /*$( document ).ready(function() {
            console.log( "ready!" );
            printPage();
        });*/
    </script>
    <style>
        table {
            font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
            width: 100%;
            margin: 0 auto;
            border: 1px solid black;
            border-collapse: collapse;
            font-size: 12px;
        }

        th, td{
            border: 1px solid black;
            padding: 3px;
        }

        .w16{
            width: 16%;
        }

        .w20{
            width: 20%;
        }

        .w25{
            width: 25%;
        }

        .w33{
            width: 33%;
        }

        .w50{
            width: 50%;
        }
    </style>
</head>
<body>
<h2 style="text-align: center ">Nalog: {{ $data['order']->ordinal_number }}</h2>
<table>
    <tbody>
    <tr>
        <td colspan="4" class="w50">Vreme stampanja naloga : {{ $data['date'] }}</td>

    </tr>
        <tr>
            <td colspan="2" class="w50">Redni broj naloga : {{ $data['order']->ordinal_number }}</td>
            <td colspan="2" class="w50">Status naloga: {{ $data['order']->order_status }}</td>
        </tr>
        <tr>
            <td colspan="2" class="w50">Broj kupca: {{ ($data['order']->buyer_id != null ) ? $data['order']->buyer_id : "/" }}</td>
            <td colspan="2" class="w50">Datum i vreme prijave: {{datetimeForView($data['order']->opened_at)}}</td>
        </tr>
        <tr>
            <td colspan="2" class="w50">Broj ugovora: {{ ($data['order']->treaty_id != null ) ? $data['order']->treaty_id : "/" }}</td>
            <td colspan="2" class="w50">Broj naloga: {{$data['order']->order_number}}</td>
        </tr>
        <tr>
            <td colspan="2" class="w50">Ime i prezime kupca: {{ ($data['order']->buyer != null ) ? $data['order']->buyer : "/" }}</td>
            {{--<td colspan="2" class="w50">Predvidjeni pocetak rada: {{datetimeForView($data['order']->started_at)}}</td>--}}
        </tr>
        <tr>
            <td colspan="2" class="w50">Adresa: {{ ($data['order']->area_code != null ) ? $data['order']->area_code : "/" }} {{$data['order']->city}}, {{ ($data['order']->township != null ) ? $data['order']->township : "/" }}
                , {{ ($data['order']->address != null ) ? $data['order']->address : "/" }}
                {{ ($data['order']->address_number != null ) ? $data['order']->address_number : "/" }},
                sprat: {{ ($data['order']->floor != null ) ? $data['order']->floor : "/" }},
                stan: {{ ($data['order']->apartment != null ) ? $data['order']->apartment : "/" }}</td>
            <td colspan="2" class="w50">Tip posla: {{$data['service_name']}}</td>
        </tr>
        <tr>
            <td colspan="2" class="w50">Region: {{ ($data['order']->region != null ) ? $data['order']->region : "/" }}</td>
        </tr>
        <tr>
            <td colspan="2" class="w50">Telefon: {{ ($data['order']->phone_number != null ) ? $data['order']->phone_number : "/" }}</td>
            <td colspan="2" class="w50"></td>
        </tr>
        <tr>
            <td colspan="2" class="w50">Mobilni telefon: {{ ($data['order']->mobile_number != null ) ? $data['order']->mobile_number : "/" }}</td>
            <td colspan="2" class="w50"></td>
        </tr>
        <tr>
            <td colspan="4">Komentar: <br>
                {{ ($data['order']->note != null ) ? $data['order']->note : "/" }}</td>
        </tr>
        @if($data['order']->order_status == 'Otkazan')
            <tr>
                <td colspan="2" class="w50">Status: Otkazan </td>
                <td colspan="2" class="w50">Vreme zatvaranja naloga: {{ datetimeForView($data['order']->completed_at)}}</td>
            </tr>
        @elseif($data['order']->order_status == 'Nerealizovan' or $data['order']->order_status == 'Završen' )
            <tr>
                <td colspan="2" class="w50">Status naloga: {{ $data['order']->order_status}}</td>
                <td colspan="2" class="w50">Vreme zatvaranja naloga: {{ datetimeForView($data['order']->completed_at)}}</td>
            </tr>
            <tr>
                <td colspan="4" style="padding: 5px">
                    Oprema izdata na korišćenje:
                    @if(count($data['used_equipment']) == 0)
                        Nije izdata oprema za ovaj nalog
                    @else
                        <br>
                        <table>
                            <thead>
                            <tr>
                                <th class="w16">Kôd</th>
                                <th class="w20">Tip opreme</th>
                                <th class="w16">Model</th>
                                <th class="w16">S/N 1</th>
                                <th class="w16">S/N 2</th>
                                <th class="w16">S/N 3</th>

                            </tr>
                            </thead>
                            <tbody>
                            @foreach($data['used_equipment'] as $equipment)

                                <tr>
                                    <td>{{ $equipment->equipment_code }}</td>
                                    <td> {{ $equipment->equipment_subtype_name }}</td>
                                    @if($equipment->equipment_model_id != null)
                                        <td> {{ $equipment->equipment_model_name }}</td>
                                    @else
                                        <td></td>
                                    @endif
                                    <td>{{ $equipment->equipment_serial1 }}</td>
                                    <td>{{ $equipment->equipment_serial2 }}</td>
                                    <td>{{ $equipment->equipment_serial3 }}</td>

                                </tr>
                            @endforeach

                            </tbody>
                        </table>
                    @endif
                </td>
            </tr>
            <tr>
                <td colspan="4" style="padding: 5px">
                    Demontirana oprema:
                    @if(count($data['removed_equipment']) == 0)
                        Nije uneta demontirana oprema za ovaj nalog
                    @else
                        <br>
                        <table>
                            <thead>
                            <tr>
                                <th>Tip opreme</th>
                                <th class="w33">S/N 1</th>
                                <th class="w33">S/N 2</th>

                            </tr>
                            </thead>
                            <tbody>
                            @foreach($data['removed_equipment'] as $removed_equipment)

                                <tr>
                                    <td>{{ $removed_equipment->dismantled_eq_subtype_name }}</td>
                                    <td>{{ $removed_equipment->removed_equipment_serial1 }}</td>
                                    <td>{{ $removed_equipment->removed_equipment_serial2 }}</td>

                                </tr>
                            @endforeach

                            </tbody>
                        </table>
                    @endif
                </td>
            </tr>






            <tr>
                <td colspan="4" style="padding: 5px">
                    Upotrebljeni materijal:
                    @if(count($data['used_wares']) == 0)
                        Nije unet materijal za ovaj nalog
                    @else
                    <br>
                    <table>
                        <thead>
                        <tr>
                            <th class="w33">Kod materijala</th>
                            <th>Ime materijala</th>
                            <th class="w33">Količina</th>

                        </tr>
                        </thead>
                        <tbody>
                        @foreach($data['used_wares'] as $used_wares)

                            <tr>
                                <td>{{ $used_wares->wares_code }}</td>
                                <td>{{ $used_wares->wares_name }}</td>
                                <td>{{ $used_wares->quantity }}</td>

                            </tr>
                        @endforeach

                        </tbody>
                    </table>
                    @endif
                </td>
            </tr>
            <tr>
                <td colspan="4" style="padding: 5px">
                    Tehničari koji su radili na nalogu:<br>
                    @foreach($data['order_technician'] as $key => $order_technician)
                        {{++$key}}. {{$order_technician->technician_name}}<br>
                    @endforeach
                </td>
            </tr>
            <tr>
                <td colspan="4" style="padding: 5px">
                   Vrsta objekta:
                        @if($data['order']->service_home == 'house')
                            Kuća
                        @elseif($data['order']->service_home == 'apartment')
                            Stan
                        @endif
                </td>
            </tr>
            <tr>
                <td colspan="4" style="padding: 5px">
                    Tehničar koristio privatno vozilo:
                    @if($data['order']->private_vehicle == 0)
                        Ne
                    @elseif($data['order']->private_vehicle == 1)
                        Da
                    @endif
                </td>
            </tr>
        @endif
    </tbody>

</table>

{{--<div class="col-12 mb-3 unrealized">

    <legend class="font-weight-semibold mt-3"><i class="--}}{{-- NASE IKONICE --}}{{--"></i> Tehničari koji su radili na nalogu
    </legend>

    @foreach($data['order_technician'] as $key => $order_technician)
        <p>{{++$key}}. {{$order_technician->technician_name}}</p>

    @endforeach

    <legend class="font-weight-semibold mt-3"><i class="--}}{{-- NASE IKONICE --}}{{--"></i> Vrsta
        objekta i privatno vozilo
    </legend>

    <p>Vrsta objekta:
        @if($data['order']->service_home == 'house')
            Kuća
        @elseif($data['order']->service_home == 'apartment')
            Stan
        @endif
    </p>

    <p>Tehničar koristio privatno vozilo:
        @if($data['order']->private_vehicle == 0)
            Ne
        @elseif($data['order']->private_vehicle == 1)
            Da
        @endif
    </p>


</div>--}}

{{--@dd($data)--}}


</body>
</html>