<?php

use Illuminate\Database\Seeder;

class RegionSelectTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('selects')->insert([
            [
                'select_name' => 'region',
                'select_value' => 'BG-1',
                'flag' => null
            ], [
                'select_name' => 'region',
                'select_value' => 'BG-2',
                'flag' => null
            ],  [
                'select_name' => 'region',
                'select_value' => 'BG-3',
                'flag' => null
            ], [
                'select_name' => 'region',
                'select_value' => 'BG-4',
                'flag' => null
            ], [
                'select_name' => 'region',
                'select_value' => 'BG-5',
                'flag' => null
            ], [
                'select_name' => 'region',
                'select_value' => 'BG-6',
                'flag' => null
            ], [
                'select_name' => 'region',
                'select_value' => 'BG-7',
                'flag' => null
            ], [
                'select_name' => 'region',
                'select_value' => 'BG-8',
                'flag' => null
            ], [
                'select_name' => 'region',
                'select_value' => 'BG-9',
                'flag' => null
            ], [
                'select_name' => 'region',
                'select_value' => 'BG-10',
                'flag' => null
            ], [
                'select_name' => 'region',
                'select_value' => 'BG-11',
                'flag' => null
            ], [
                'select_name' => 'region',
                'select_value' => 'BG-12',
                'flag' => null
            ], [
                'select_name' => 'region',
                'select_value' => 'BG-13',
                'flag' => null
            ], [
                'select_name' => 'region',
                'select_value' => 'BG-14',
                'flag' => null
            ], [
                'select_name' => 'region',
                'select_value' => 'BG-15',
                'flag' => null
            ], [
                'select_name' => 'region',
                'select_value' => 'BG-16',
                'flag' => null
            ], [
                'select_name' => 'region',
                'select_value' => 'BG-17',
                'flag' => null
            ], [
                'select_name' => 'region',
                'select_value' => 'BG-18',
                'flag' => null
            ], [
                'select_name' => 'region',
                'select_value' => 'BG-19',
                'flag' => null
            ], [
                'select_name' => 'region',
                'select_value' => 'BG-20',
                'flag' => null
            ]
        ]);
    }
}
