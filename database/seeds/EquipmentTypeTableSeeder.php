<?php

use Illuminate\Database\Seeder;

class EquipmentTypeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('equipment_type')->insert([
            [
                'equipment_type_id' => 'type_1',
                'equipment_type_name' => 'Pojačivač',
            ], [
                'equipment_type_id' => 'type_2',
                'equipment_type_name' => 'Telefon',
            ],  [
                'equipment_type_id' => 'type_3',
                'equipment_type_name' => 'WiFi router',
            ], [
                'equipment_type_id' => 'type_4',
                'equipment_type_name' => 'Dekoder',
            ], [
                'equipment_type_id' => 'type_5',
                'equipment_type_name' => 'Modem',
            ], [
                'equipment_type_id' => 'type_6',
                'equipment_type_name' => 'GPON modem',
            ], [
                'equipment_type_id' => 'type_7',
                'equipment_type_name' => 'CAM',
            ], [
                'equipment_type_id' => 'type_8',
                'equipment_type_name' => 'EON',
            ]
        ]);
    }
}
