<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();
        /*$this->call('WaresTableSeeder');
        $this->call('TownshipSelectTableSeeder');
        $this->call('RegionSelectTableSeeder');
        $this->call('EquipmentTypeTableSeeder');
        $this->call('EquipmentSubtypeTableSeeder');
        $this->call('EquipmentModelTableSeeder');
        $this->call('EquipmentTableSeeder');
        $this->call('TechnicianTableSeeder');
        $this->call('TechnicianReportTableSeeder');*/
        $this->call('WarehouseTableSeeder');
        /*$this->call('ExernalwarehouseTableSeeder');
        $this->call('RemovedEquipmentTableSeeder');
        $this->call('DismantledSubtypeTableSeeder');
        $this->call('ServiceTableSeeder');
        $this->call('OrdersTableSeeder');*/

    }
}
