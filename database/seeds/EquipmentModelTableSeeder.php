<?php

use Illuminate\Database\Seeder;

class EquipmentModelTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('equipment_model')->insert([
            [
                'equipment_model_id' => 'model_1',
                'equipment_subtype_id' => 'subtype_1',
                'equipment_model_name' => 'Compact EGC MINI A 93230.10 338 85',
            ], [
                'equipment_model_id' => 'model_2',
                'equipment_subtype_id' => 'subtype_2',
                'equipment_model_name' => 'Delta LHD 43-R GA',
            ], [
                'equipment_model_id' => 'model_3',
                'equipment_subtype_id' => 'subtype_3',
                'equipment_model_name' => 'OS 1300-B Dect telefon',
            ], [
                'equipment_model_id' => 'model_4',
                'equipment_subtype_id' => 'subtype_4',
                'equipment_model_name' => 'TL-WR740N WiFi router 2,4GHz',
            ], [
                'equipment_model_id' => 'model_5',
                'equipment_subtype_id' => 'subtype_6',
                'equipment_model_name' => 'UBEE EVW32C-ON',
            ], [
                'equipment_model_id' => 'model_6',
                'equipment_subtype_id' => 'subtype_7',
                'equipment_model_name' => 'PDS2100 DVB-C IP Digital STB',
            ], [
                'equipment_model_id' => 'model_7',
                'equipment_subtype_id' => 'subtype_8',
                'equipment_model_name' => 'STB PDS 3121 3K SET TOP BOX',
            ], [
                'equipment_model_id' => 'model_8',
                'equipment_subtype_id' => 'subtype_9',
                'equipment_model_name' => 'EPC 3928S  GaTeWaY',
            ], [
                'equipment_model_id' => 'model_9',
                'equipment_subtype_id' => 'subtype_9',
                'equipment_model_name' => 'EPC 3928S V2 GaTeWaY',
            ], [
                'equipment_model_id' => 'model_10',
                'equipment_subtype_id' => 'subtype_9',
                'equipment_model_name' => 'EPC 3928S V2.5 GaTeWaY',
            ], [
                'equipment_model_id' => 'model_11',
                'equipment_subtype_id' => 'subtype_10',
                'equipment_model_name' => 'MOCA854G-2 GigaCenter 2Pots4GE',
            ], [
                'equipment_model_id' => 'model_12',
                'equipment_subtype_id' => 'subtype_11',
                'equipment_model_name' => 'Huawei HG8247U GPON',
            ], [
                'equipment_model_id' => 'model_13',
                'equipment_subtype_id' => 'subtype_12',
                'equipment_model_name' => 'CAM  cardless SC 5.0 ci+1.3 CP',
            ], [
                'equipment_model_id' => 'model_14',
                'equipment_subtype_id' => 'subtype_14',
                'equipment_model_name' => 'EPC Docsis 3.0 EMTA',
            ]
        ]);
    }
}
