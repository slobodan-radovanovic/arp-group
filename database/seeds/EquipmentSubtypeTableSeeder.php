<?php

use Illuminate\Database\Seeder;

class EquipmentSubtypeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('equipment_subtype')->insert([
            [
                'equipment_subtype_id' => 'subtype_1',
                'equipment_type_id' => 'type_1',
                'equipment_subtype_name' => 'Compact EGC MINI',
            ], [
                'equipment_subtype_id' => 'subtype_2',
                'equipment_type_id' => 'type_1',
                'equipment_subtype_name' => 'Delta LHD',
            ],  [
                'equipment_subtype_id' => 'subtype_3',
                'equipment_type_id' => 'type_2',
                'equipment_subtype_name' => 'OS Dect telefon',
            ], [
                'equipment_subtype_id' => 'subtype_4',
                'equipment_type_id' => 'type_3',
                'equipment_subtype_name' => 'TL WiFi router',
            ], [
                'equipment_subtype_id' => 'subtype_5',
                'equipment_type_id' => 'type_4',
                'equipment_subtype_name' => 'Digital MPEG HD DVB-D3 mini',
            ], [
                'equipment_subtype_id' => 'subtype_6',
                'equipment_type_id' => 'type_5',
                'equipment_subtype_name' => 'UBEE',
            ], [
                'equipment_subtype_id' => 'subtype_7',
                'equipment_type_id' => 'type_4',
                'equipment_subtype_name' => 'DVB-C IP Digital STB',
            ], [
                'equipment_subtype_id' => 'subtype_8',
                'equipment_type_id' => 'type_4',
                'equipment_subtype_name' => 'STB SET TOP BOX',
            ], [
                'equipment_subtype_id' => 'subtype_9',
                'equipment_type_id' => 'type_5',
                'equipment_subtype_name' => 'EPC GaTeWaY',
            ], [
                'equipment_subtype_id' => 'subtype_10',
                'equipment_type_id' => 'type_6',
                'equipment_subtype_name' => 'GigaCenter',
            ], [
                'equipment_subtype_id' => 'subtype_11',
                'equipment_type_id' => 'type_5',
                'equipment_subtype_name' => 'Huawei GPON',
            ], [
                'equipment_subtype_id' => 'subtype_12',
                'equipment_type_id' => 'type_7',
                'equipment_subtype_name' => 'CAM cardless',
            ], [
                'equipment_subtype_id' => 'subtype_13',
                'equipment_type_id' => 'type_8',
                'equipment_subtype_name' => 'EON BOX',
            ], [
                'equipment_subtype_id' => 'subtype_14',
                'equipment_type_id' => 'type_8',
                'equipment_subtype_name' => 'EPC Docsis',
            ]
        ]);
    }
}
