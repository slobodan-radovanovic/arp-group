<?php

use Illuminate\Database\Seeder;

class RemovedEquipmentTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('removed_equipment')->insert([
            [
                'ordinal_number' => '18-03-00001',
                'removed_equipment_subtype' => 'EPC 3208G EU Docsis 3.0 EMTA',
                'removed_equipment_serial1' => '247023352',
                'removed_equipment_serial2' => 'BCC810F5843A'
            , 'created_at' => '2019-03-20 15:17:00' ],
            [
                'ordinal_number' => '18-03-00001',
                'removed_equipment_subtype' => 'Technicolor EPC 3928S',
                'removed_equipment_serial1' => '287278064',
                'removed_equipment_serial2' => 'FC9114AE770E'
            , 'created_at' => '2019-03-20 15:18:00' ],
            [
                'ordinal_number' => '18-03-00002',
                'removed_equipment_subtype' => 'EPC 3925',
                'removed_equipment_serial1' => '260329866',
                'removed_equipment_serial2' => '105F4978CA20'
            , 'created_at' =>  '2019-03-20 15:19:00' ],
            [
                'ordinal_number' => '18-03-00003',
                'removed_equipment_subtype' => 'THG 571 Docsis 3.0',
                'removed_equipment_serial1' => '00968142404999',
                'removed_equipment_serial2' => '80C6AB95658E'
            , 'created_at' =>  '2019-03-20 15:20:00' ],
            [
                'ordinal_number' => '18-03-00004',
                'removed_equipment_subtype' => 'CBN CV6181E Docsis 3.0',
                'removed_equipment_serial1' => '601081322820537458790000',
                'removed_equipment_serial2' => '5C353B93F6DD'
            , 'created_at' =>  '2019-03-20 15:21:00' ],
            [
                'ordinal_number' => '18-03-00005',
                'removed_equipment_subtype' => 'CBN CH6541E Docsis 3.0',
                'removed_equipment_serial1' => '600269115305938200000000',
                'removed_equipment_serial2' => '5C353B270B29'
            , 'created_at' =>  '2019-03-20 15:22:00' ],
            [
                'ordinal_number' => '18-03-00006',
                'removed_equipment_subtype' => 'Moto SB 5121E',
                'removed_equipment_serial1' => '192758812611487001012013',
                'removed_equipment_serial2' => '001BDDDAC0BE'
            , 'created_at' =>  '2019-03-20 15:23:00' ],
            [
                'ordinal_number' => '18-03-00007',
                'removed_equipment_subtype' => 'Moto SB 5101E',
                'removed_equipment_serial1' => '140255600605819001023001',
                'removed_equipment_serial2' => '001371E3D06E'
            , 'created_at' =>  '2019-03-20 15:24:00' ],
            [
                'ordinal_number' => '18-03-00008',
                'removed_equipment_subtype' => 'Moto SB 5100E',
                'removed_equipment_serial1' => '125055406000183002020000',
                'removed_equipment_serial2' => '000E5CCED630'
            , 'created_at' =>  '2019-03-20 15:25:00' ],
            [
                'ordinal_number' => '18-03-00009',
                'removed_equipment_subtype' => 'TCM 420 Docsis 2.0',
                'removed_equipment_serial1' => '00770037107048',
                'removed_equipment_serial2' => '002624A73D50'
            , 'created_at' =>  '2019-03-20 15:26:00' ],
            [
                'ordinal_number' => '18-03-00010',
                'removed_equipment_subtype' => 'Kablovski modem Cisco EPC2100R3',
                'removed_equipment_serial1' => '243735785',
                'removed_equipment_serial2' => 'E448C7B41C2A'
            , 'created_at' =>  '2019-03-20 15:27:00' ],
            [
                'ordinal_number' => '18-03-00011',
                'removed_equipment_subtype' => 'Cisco 4682DVB',
                'removed_equipment_serial1' => 'SACCRVQVP',
                'removed_equipment_serial2' => 'BCC8101DF044'
            , 'created_at' =>  '2019-03-20 15:28:00' ],
            [
                'ordinal_number' => '18-03-00012',
                'removed_equipment_subtype' => 'Ubee EVW32C',
                'removed_equipment_serial1' => 'EVW32C0N00013123',
                'removed_equipment_serial2' => '28565A722D21'
            , 'created_at' =>  '2019-03-20 15:29:00' ],
            [
                'ordinal_number' => '18-03-00013',
                'removed_equipment_subtype' => 'Ubee EVW3270',
                'removed_equipment_serial1' => 'EVW327N010873',
                'removed_equipment_serial2' => '40B89AA95BE5'
            , 'created_at' =>  '2019-03-20 15:30:00' ],
            [
                'ordinal_number' => '18-03-00014',
                'removed_equipment_subtype' => 'UG STB KSTB6020',
                'removed_equipment_serial1' => 'BS1005288C014487',
                'removed_equipment_serial2' => '00729403926'
            , 'created_at' =>  '2019-03-20 15:31:00' ],
            [
                'ordinal_number' => '18-03-00015',
                'removed_equipment_subtype' => 'Technicolor CGA2121',
                'removed_equipment_serial1' => '201912018333102786',
                'removed_equipment_serial2' => '54A65C5666C8'
            , 'created_at' =>  '2019-03-20 15:32:00' ],
            [
                'ordinal_number' => '18-03-00016',
                'removed_equipment_subtype' => 'SACFRDNBB',
                'removed_equipment_serial1' => 'PV6M7E3007874',
                'removed_equipment_serial2' => 'B0C554DB4FF0'
            , 'created_at' =>  '2019-03-20 15:33:00' ],
            [
                'ordinal_number' => '18-03-00017',
                'removed_equipment_subtype' => 'Telefon Vtech OS1300-B',
                'removed_equipment_serial1' => '1503005504',
                'removed_equipment_serial2' => ''
            , 'created_at' =>  '2019-03-20 15:34:00' ],
            [
                'ordinal_number' => '18-03-00018',
                'removed_equipment_subtype' => 'Ikom POWERBOX HD',
                'removed_equipment_serial1' => '010067DC',
                'removed_equipment_serial2' => '02017950543'
            , 'created_at' =>  '2019-03-20 15:35:00' ],
            [
                'ordinal_number' => '18-03-00019',
                'removed_equipment_subtype' => 'Ikom GOSPEL',
                'removed_equipment_serial1' => '211120161534103',
                'removed_equipment_serial2' => '02014760842'
            , 'created_at' =>  '2019-03-20 15:36:00' ],
            [
                'ordinal_number' => '18-03-00020',
                'removed_equipment_subtype' => 'Ikom ECHOLITE mali ',
                'removed_equipment_serial1' => '312000163115031402113',
                'removed_equipment_serial2' => '020179282395'
            , 'created_at' =>  '2019-03-20 15:37:00' ],
            [
                'ordinal_number' => '18-03-00021',
                'removed_equipment_subtype' => 'D3 mini',
                'removed_equipment_serial1' => '18212250016220009845',
                'removed_equipment_serial2' => '00677752243'
            , 'created_at' =>  '2019-03-10 15:38:00' ],
            [
                'ordinal_number' => '18-03-00022',
                'removed_equipment_subtype' => 'PDS 2100',
                'removed_equipment_serial1' => '168645452',
                'removed_equipment_serial2' => '3817E1D878CE',
                'created_at' =>  '2019-03-19 15:39:00' ],
            [
                'ordinal_number' => '18-03-00023',
                'removed_equipment_subtype' => 'PDS3121',
                'removed_equipment_serial1' => '125270203',
                'removed_equipment_serial2' => '2CABA4FF11C2',
                'created_at' =>  '2019-03-02 15:40:00' ],
            [
                'ordinal_number' => '18-03-00024',
                'removed_equipment_subtype' => 'Ikom KAON + kartica',
                'removed_equipment_serial1' => '20675700709F9A000471',
                'removed_equipment_serial2' => '02017904686',
                'created_at' =>  '2019-03-22 15:41:00'
            ]
        ]);
    }
}
