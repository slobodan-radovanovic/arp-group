<?php

use Illuminate\Database\Seeder;

class ServiceTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('services')->insert([
            [
                'service_select' => 'prvo_prikljucenje',
                'service_name' => 'Prvo priključenje',
                'order_type' => 'sbb_order',
                'price_apartment' => 600,
                'price_house' => 1200,
                'price_first_equipment' => 400,
                'price_additional_equipment' => 100,
                'price_fixed' => null,
                'price_private_vehicle' => 100
            ],[
                'service_select' => 'dodatno_prikljucenje',
                'service_name' => 'Dodatno prikljukenje',
                'order_type' => 'sbb_order',
                'price_apartment' => 0,
                'price_house' => 0,
                'price_first_equipment' => '400',
                'price_additional_equipment' => '100',
                'price_fixed' => null,
                'price_private_vehicle' => 50
            ],[
                'service_select' => 'preseljenje',
                'service_name' => 'Preseljenje',
                'order_type' => 'sbb_order',
                'price_apartment' => 1000,
                'price_house' => 1600,
                'price_first_equipment' => null,
                'price_additional_equipment' => null,
                'price_fixed' => null,
                'price_private_vehicle' => 100
            ],[
                'service_select' => 'zamena_opreme',
                'service_name' => 'Zamena opreme',
                'order_type' => 'sbb_order',
                'price_apartment' => null,
                'price_house' => null,
                'price_first_equipment' => null,
                'price_additional_equipment' => null,
                'price_fixed' => 350,
                'price_private_vehicle' => 50
            ],[
                'service_select' => 'dorada_instalacije',
                'service_name' => 'Dorada instalacije',
                'order_type' => 'sbb_order',
                'price_apartment' => null,
                'price_house' => null,
                'price_first_equipment' => null,
                'price_additional_equipment' => null,
                'price_fixed' => 400,
                'price_private_vehicle' => 50
            ],[
                'service_select' => 'prikljucenje_2_3_tv',
                'service_name' => 'Priključenje 2/3 TV',
                'order_type' => 'sbb_order',
                'price_apartment' => null,
                'price_house' => null,
                'price_first_equipment' => null,
                'price_additional_equipment' => null,
                'price_fixed' => 400,
                'price_private_vehicle' => 50
            ],[
                'service_select' => 'digitalizacija',
                'service_name' => 'Digitalizacija',
                'order_type' => 'digitization',
                'price_apartment' => null,
                'price_house' => null,
                'price_first_equipment' => null,
                'price_additional_equipment' => null,
                'price_fixed' => 250,
                'price_private_vehicle' => 50
            ]
        ]);
    }
}
