<?php

use Illuminate\Database\Seeder;

class TownshipSelectTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        DB::table('selects')->insert([
                [
                    'select_name' => 'township',
                    'select_value' => 'Novi Beograd',
                    'flag' => null
                ], [
                    'select_name' => 'township',
                    'select_value' => 'Čukarica',
                    'flag' => null
                ],  [
                'select_name' => 'township',
                'select_value' => 'Palilula',
                'flag' => null
                ], [
                'select_name' => 'township',
                'select_value' => 'Rakovica',
                'flag' => null
                ], [
                'select_name' => 'township',
                'select_value' => 'Savski venac',
                'flag' => null
                ], [
                'select_name' => 'township',
                'select_value' => 'Stari grad',
                'flag' => null
                ], [
                'select_name' => 'township',
                'select_value' => 'Voždovac',
                'flag' => null
                ], [
                'select_name' => 'township',
                'select_value' => 'Vračar',
                'flag' => null
                ], [
                'select_name' => 'township',
                'select_value' => 'Zemun',
                'flag' => null
                ], [
                'select_name' => 'township',
                'select_value' => 'Zvezdara',
                'flag' => null
                ], [
                'select_name' => 'township',
                'select_value' => 'Barajevo',
                'flag' => null
                ], [
                'select_name' => 'township',
                'select_value' => 'Grocka',
                'flag' => null
                ], [
                'select_name' => 'township',
                'select_value' => 'Lazarevac',
                'flag' => null
                ], [
                'select_name' => 'township',
                'select_value' => 'Mladenovac',
                'flag' => null
                ], [
                'select_name' => 'township',
                'select_value' => 'Obrenovac',
                'flag' => null
                ], [
                'select_name' => 'township',
                'select_value' => 'Sopot',
                'flag' => null
                ], [
                'select_name' => 'township',
                'select_value' => 'Surčin',
                'flag' => null
                ]
        ]);
    }
}
