<?php

use Illuminate\Database\Seeder;

class TechnicianTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('technicians')->insert([
            [
                'technician_name' => 'Slobodan Radovanovic',
                'mobile_number' => '0643339313',
                'active' => 1
            ], [
                'technician_name' => 'Nenad Damjanovic',
                'mobile_number' => '0691112223',
                'active' => 0
            ], [
                'technician_name' => 'Marko Petrovic',
                'mobile_number' => '0654445553',
                'active' => 1
            ], [
                'technician_name' => 'Stojan Markovic',
                'mobile_number' => '0610055432',
                'active' => 0
            ], [
                'technician_name' => 'Milos Davidovic',
                'mobile_number' => '0637292772',
                'active' => 1
            ], [
                'technician_name' => 'Uros Kovacevic',
                'mobile_number' => '0642136546',
                'active' => 1
            ]
        ]);
    }
}



