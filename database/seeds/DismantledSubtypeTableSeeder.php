<?php

use Illuminate\Database\Seeder;

class DismantledSubtypeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('dismantled_eq_subtype')->insert([
            [
                'dismantled_eq_subtype_id' => 'subtype_1',
                'dismantled_eq_subtype_name' => 'CBN CH6541E Docsis 3.0',
            ], [
                'dismantled_eq_subtype_id' => 'subtype_2',
                'dismantled_eq_subtype_name' => 'CBN CV6181E Docsis 3.0	',
            ],  [
                'dismantled_eq_subtype_id' => 'subtype_3',
                'dismantled_eq_subtype_name' => 'Cisco 4682DVB',
            ], [
                'dismantled_eq_subtype_id' => 'subtype_4',
                'dismantled_eq_subtype_name' => 'D3 mini',
            ], [
                'dismantled_eq_subtype_id' => 'subtype_5',
                'dismantled_eq_subtype_name' => 'EPC 3208G EU Docsis 3.0 EMTA',
            ], [
                'dismantled_eq_subtype_id' => 'subtype_6',
                'dismantled_eq_subtype_name' => 'EPC 3925',
            ], [
                'dismantled_eq_subtype_id' => 'subtype_7',
                'dismantled_eq_subtype_name' => 'Ikom ECHOLITE mali',
            ], [
                'dismantled_eq_subtype_id' => 'subtype_8',
                'dismantled_eq_subtype_name' => 'Ikom GOSPEL',
            ], [
                'dismantled_eq_subtype_id' => 'subtype_9',
                'dismantled_eq_subtype_name' => 'Ikom KAON + kartica',
            ], [
                'dismantled_eq_subtype_id' => 'subtype_10',
                'dismantled_eq_subtype_name' => 'Ikom POWERBOX HD',
            ], [
                'dismantled_eq_subtype_id' => 'subtype_11',
                'dismantled_eq_subtype_name' => 'Kablovski modem Cisco EPC2100R3',
            ], [
                'dismantled_eq_subtype_id' => 'subtype_12',
                'dismantled_eq_subtype_name' => 'Moto SB 5100E',
            ], [
                'dismantled_eq_subtype_id' => 'subtype_13',
                'dismantled_eq_subtype_name' => 'Moto SB 5101E',
            ], [
                'dismantled_eq_subtype_id' => 'subtype_14',
                'dismantled_eq_subtype_name' => 'PDS 2100',
            ], [
                'dismantled_eq_subtype_id' => 'subtype_15',
                'dismantled_eq_subtype_name' => 'PDS3121',
            ], [
                'dismantled_eq_subtype_id' => 'subtype_16',
                'dismantled_eq_subtype_name' => 'SACFRDNBB',
            ], [
                'dismantled_eq_subtype_id' => 'subtype_17',
                'dismantled_eq_subtype_name' => 'TCM 420 Docsis 2.0',
            ], [
                'dismantled_eq_subtype_id' => 'subtype_18',
                'dismantled_eq_subtype_name' => 'Technicolor CGA2121',
            ], [
                'dismantled_eq_subtype_id' => 'subtype_19',
                'dismantled_eq_subtype_name' => 'Technicolor EPC 3928S',
            ], [
                'dismantled_eq_subtype_id' => 'subtype_20',
                'dismantled_eq_subtype_name' => 'Telefon Vtech OS1300-B',
            ], [
                'dismantled_eq_subtype_id' => 'subtype_21',
                'dismantled_eq_subtype_name' => 'THG 571 Docsis 3.0',
            ], [
                'dismantled_eq_subtype_id' => 'subtype_22',
                'dismantled_eq_subtype_name' => 'Ubee EVW3270',
            ], [
                'dismantled_eq_subtype_id' => 'subtype_23',
                'dismantled_eq_subtype_name' => 'Ubee EVW32C',
            ], [
                'dismantled_eq_subtype_id' => 'subtype_24',
                'dismantled_eq_subtype_name' => 'UG STB KSTB6020',
            ]
        ]);
    }
}
