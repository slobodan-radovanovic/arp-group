<?php

use Illuminate\Database\Seeder;

class WaresTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('wares')->insert([
            [
                'wares_code' =>  101001,
                'wares_name' =>  'Kabl koaksijalni RG-11M',
                'wares_type' =>  'm',
                'default' =>  0
            ],
            [
                'wares_code' =>  101002,
                'wares_name' =>  'Kabl koaksijalni RG-11',
                'wares_type' =>  'm',
                'default' =>  0
            ],
            [
                'wares_code' =>  101003,
                'wares_name' =>  'Kabl koaksijalni RG-6',
                'wares_type' =>  'm',
                'default' =>  0
            ],
            [
                'wares_code' =>  101008,
                'wares_name' =>  'Kabl strujni 3X1,5mm',
                'wares_type' =>  'm',
                'default' =>  0
            ],
            [
                'wares_code' =>  101009,
                'wares_name' =>  'Kabl strujni 2X0,75mm',
                'wares_type' =>  'm',
                'default' =>  0
            ],
            [
                'wares_code' =>  101010,
                'wares_name' =>  'Kabl strujni PP/Y 3 X 2,5mm',
                'wares_type' =>  'm',
                'default' =>  0
            ],
            [
                'wares_code' =>  101014,
                'wares_name' =>  'Provodnik P/F 4mm',
                'wares_type' =>  'm',
                'default' =>  0
            ],
            [
                'wares_code' =>  101017,
                'wares_name' =>  'UTP patch kabl CAT.5E',
                'wares_type' =>  'm',
                'default' =>  0
            ],
            [
                'wares_code' =>  101073,
                'wares_name' =>  'Angle plug IEC male/female RG6cable 1,5m',
                'wares_type' =>  'kom',
                'default' =>  0
            ],
            [
                'wares_code' =>  102003,
                'wares_name' =>  'Konektor, F15-1 (T32) za RG-11',
                'wares_type' =>  'kom',
                'default' =>  0
            ],
            [
                'wares_code' =>  102007,
                'wares_name' =>  'Konektor, pin 5/8 za kabl RG-11',
                'wares_type' =>  'kom',
                'default' =>  0
            ],
            [
                'wares_code' =>  102011,
                'wares_name' =>  'Nastavak P28 - za RG11 ',
                'wares_type' =>  'kom',
                'default' =>  0
            ],
            [
                'wares_code' =>  102038,
                'wares_name' =>  'Self - insall konektor RG6',
                'wares_type' =>  'kom',
                'default' =>  0
            ],
            [
                'wares_code' =>  103002,
                'wares_name' =>  'Završna impendansa 75Ohm',
                'wares_type' =>  'kom',
                'default' =>  0
            ],
            [
                'wares_code' =>  103004,
                'wares_name' =>  'Adapter 5/8F-PG11M',
                'wares_type' =>  'kom',
                'default' =>  0
            ],
            [
                'wares_code' =>  103007,
                'wares_name' =>  'Adapter F na F ',
                'wares_type' =>  'kom',
                'default' =>  0
            ],
            [
                'wares_code' =>  103008,
                'wares_name' =>  'Adapter 5/8-FF',
                'wares_type' =>  'kom',
                'default' =>  0
            ],
            [
                'wares_code' =>  103015,
                'wares_name' =>  'Outdoor zavrsna impedansa 75 Ohm',
                'wares_type' =>  'kom',
                'default' =>  0
            ],
            [
                'wares_code' =>  103018,
                'wares_name' =>  'Adapter 90° F ženski na IEC muški',
                'wares_type' =>  'kom',
                'default' =>  0
            ],
            [
                'wares_code' =>  104092,
                'wares_name' =>  'Uvodnik napajanja LPI',
                'wares_type' =>  'kom',
                'default' =>  0
            ],
            [
                'wares_code' =>  104101,
                'wares_name' =>  'Razdelnik 2-grani,indoor',
                'wares_type' =>  'kom',
                'default' =>  0
            ],
            [
                'wares_code' =>  104102,
                'wares_name' =>  'Razdelnik 3-grani,indoor  ',
                'wares_type' =>  'kom',
                'default' =>  0
            ],
            [
                'wares_code' =>  104103,
                'wares_name' =>  'Razdelnik 3-grani balansirani (WB),indoor',
                'wares_type' =>  'kom',
                'default' =>  0
            ],
            [
                'wares_code' =>  104104,
                'wares_name' =>  'Razdelnik 4-grani,indoor',
                'wares_type' =>  'kom',
                'default' =>  0
            ],
            [
                'wares_code' =>  104105,
                'wares_name' =>  'Razdelnik 6-grani,indoor',
                'wares_type' =>  'kom',
                'default' =>  0
            ],
            [
                'wares_code' =>  104106,
                'wares_name' =>  'Razdelnik 8-grani,indoor',
                'wares_type' =>  'kom',
                'default' =>  0
            ],
            [
                'wares_code' =>  104107,
                'wares_name' =>  'Odvodnik, IT-1W-6,indoor',
                'wares_type' =>  'kom',
                'default' =>  0
            ],
            [
                'wares_code' =>  104108,
                'wares_name' =>  'Odvodnik, IT-1W-8,indoor',
                'wares_type' =>  'kom',
                'default' =>  0
            ],
            [
                'wares_code' =>  104110,
                'wares_name' =>  'Odvodnik, IT-1W-10,indoor',
                'wares_type' =>  'kom',
                'default' =>  0
            ],
            [
                'wares_code' =>  104112,
                'wares_name' =>  'Odvodnik, IT-1W-12,indoor',
                'wares_type' =>  'kom',
                'default' =>  0
            ],
            [
                'wares_code' =>  104114,
                'wares_name' =>  'Odvodnik, IT-1W-16,indoor',
                'wares_type' =>  'kom',
                'default' =>  0
            ],
            [
                'wares_code' =>  104116,
                'wares_name' =>  'Odvodnik, IT-1W-20,indoor',
                'wares_type' =>  'kom',
                'default' =>  0
            ],
            [
                'wares_code' =>  104119,
                'wares_name' =>  'Odvodnik, IT-2W-8,indoor',
                'wares_type' =>  'kom',
                'default' =>  0
            ],
            [
                'wares_code' =>  104134,
                'wares_name' =>  'Razdelnik 2-grani, SP-215, za spoljnu montažu',
                'wares_type' =>  'kom',
                'default' =>  0
            ],
            [
                'wares_code' =>  104135,
                'wares_name' =>  'Razdelnik 3-grani, SP-315, za spoljnu montažu',
                'wares_type' =>  'kom',
                'default' =>  0
            ],
            [
                'wares_code' =>  104136,
                'wares_name' =>  'Razdelnik 3-grani, SP-3U15, balansirani,za spoljnu montažu',
                'wares_type' =>  'kom',
                'default' =>  0
            ],
            [
                'wares_code' =>  104141,
                'wares_name' =>  'Odvodnik,outdoor, TP2W-8',
                'wares_type' =>  'kom',
                'default' =>  0
            ],
            [
                'wares_code' =>  104142,
                'wares_name' =>  'Odvodnik,outdoor, TP2W-11',
                'wares_type' =>  'kom',
                'default' =>  0
            ],
            [
                'wares_code' =>  104143,
                'wares_name' =>  'Odvodnik,outdoor, TP2W-14',
                'wares_type' =>  'kom',
                'default' =>  0
            ],
            [
                'wares_code' =>  104149,
                'wares_name' =>  'Odvodnik,outdoor, TP4W-8',
                'wares_type' =>  'kom',
                'default' =>  0
            ],
            [
                'wares_code' =>  104150,
                'wares_name' =>  'Odvodnik,outdoor,TP4W-11',
                'wares_type' =>  'kom',
                'default' =>  0
            ],
            [
                'wares_code' =>  104151,
                'wares_name' =>  'Odvodnik,outdoor,TP4W-14',
                'wares_type' =>  'kom',
                'default' =>  0
            ],
            [
                'wares_code' =>  104152,
                'wares_name' =>  'Odvodnik,outdoor,TP4W-17',
                'wares_type' =>  'kom',
                'default' =>  0
            ],
            [
                'wares_code' =>  104153,
                'wares_name' =>  'Odvodnik,outdoor, TP4W-20',
                'wares_type' =>  'kom',
                'default' =>  0
            ],
            [
                'wares_code' =>  104154,
                'wares_name' =>  'Odvodnik,outdoor, TP4W-23',
                'wares_type' =>  'kom',
                'default' =>  0
            ],
            [
                'wares_code' =>  104158,
                'wares_name' =>  'Odvodnik,outdoor,TP8W-11',
                'wares_type' =>  'kom',
                'default' =>  0
            ],
            [
                'wares_code' =>  104159,
                'wares_name' =>  'Odvodnik,outdoor,TP8W-14',
                'wares_type' =>  'kom',
                'default' =>  0
            ],
            [
                'wares_code' =>  104160,
                'wares_name' =>  'Odvodnik,outdoor,TP8W-17',
                'wares_type' =>  'kom',
                'default' =>  0
            ],
            [
                'wares_code' =>  104161,
                'wares_name' =>  'Odvodnik,outdoor,TP8W-20',
                'wares_type' =>  'kom',
                'default' =>  0
            ],
            [
                'wares_code' =>  106024,
                'wares_name' =>  'Transformator 500VA,220V/65V',
                'wares_type' =>  'kom',
                'default' =>  0
            ],
            [
                'wares_code' =>  106025,
                'wares_name' =>  'Transformator 1000VA,220V/65V',
                'wares_type' =>  'kom',
                'default' =>  0
            ],
            [
                'wares_code' =>  106054,
                'wares_name' =>  'AGC pojacavac',
                'wares_type' =>  'kom',
                'default' =>  0
            ],
            [
                'wares_code' =>  106055,
                'wares_name' =>  'Compact EGC A93240.10338 85MHz konfiguracija',
                'wares_type' =>  'kom',
                'default' =>  0
            ],
            [
                'wares_code' =>  106056,
                'wares_name' =>  'Compact EGC A93240.10238 85MHz konfiguracija',
                'wares_type' =>  'kom',
                'default' =>  0
            ],
            [
                'wares_code' =>  106057,
                'wares_name' =>  'Compact EGC A93230.10338 85MHz konfiguracija',
                'wares_type' =>  'kom',
                'default' =>  0
            ],
            [
                'wares_code' =>  106058,
                'wares_name' =>  'Compact EGC A93230.10238 85MHz konfiguracija',
                'wares_type' =>  'kom',
                'default' =>  0
            ],
            [
                'wares_code' =>  106075,
                'wares_name' =>  'Poja?ava? Delta LHD 43-R GA',
                'wares_type' =>  'kom',
                'default' =>  0
            ],
            [
                'wares_code' =>  106076,
                'wares_name' =>  'Poja?ava? Delta LHD 43 GA',
                'wares_type' =>  'kom',
                'default' =>  0
            ],
            [
                'wares_code' =>  107002,
                'wares_name' =>  'Plug-in atenuator 0db SA77140-00',
                'wares_type' =>  'kom',
                'default' =>  0
            ],
            [
                'wares_code' =>  107003,
                'wares_name' =>  'Plug-in atenuator 2db,SA77140-02',
                'wares_type' =>  'kom',
                'default' =>  0
            ],
            [
                'wares_code' =>  107004,
                'wares_name' =>  'Plug-in atenuator 4db,SA77140-04',
                'wares_type' =>  'kom',
                'default' =>  0
            ],
            [
                'wares_code' =>  107005,
                'wares_name' =>  'Plug-in atenuator 6db,SA77140-06',
                'wares_type' =>  'kom',
                'default' =>  0
            ],
            [
                'wares_code' =>  107006,
                'wares_name' =>  'Plug-in atenuator 8db,SA77140-08',
                'wares_type' =>  'kom',
                'default' =>  0
            ],
            [
                'wares_code' =>  107007,
                'wares_name' =>  'Plug-in atenuator 10db,SA77140-10',
                'wares_type' =>  'kom',
                'default' =>  0
            ],
            [
                'wares_code' =>  107008,
                'wares_name' =>  'Plug-in atenuator 12db,SA77140-12',
                'wares_type' =>  'kom',
                'default' =>  0
            ],
            [
                'wares_code' =>  107009,
                'wares_name' =>  'Plug-in equaliser, 6db 40-862Mhz SA74100-10806',
                'wares_type' =>  'kom',
                'default' =>  0
            ],
            [
                'wares_code' =>  107010,
                'wares_name' =>  'Plug-in splitter 3,5/3,5db, SA77041-10',
                'wares_type' =>  'kom',
                'default' =>  0
            ],
            [
                'wares_code' =>  107011,
                'wares_name' =>  'Plug-in splitter 2,0/6,0db,SA77042-10',
                'wares_type' =>  'kom',
                'default' =>  0
            ],
            [
                'wares_code' =>  107012,
                'wares_name' =>  'Plug-in splitter 1,5/10,0db,SA77043-10',
                'wares_type' =>  'kom',
                'default' =>  0
            ],
            [
                'wares_code' =>  107013,
                'wares_name' =>  'Plug-in splitter 0,6/14,0db,SA77044-10',
                'wares_type' =>  'kom',
                'default' =>  0
            ],
            [
                'wares_code' =>  107014,
                'wares_name' =>  'Plug-in Link,0db,SA74089-10',
                'wares_type' =>  'kom',
                'default' =>  0
            ],
            [
                'wares_code' =>  107015,
                'wares_name' =>  'Plug-in Link,0db IS AUX, SA74069-10',
                'wares_type' =>  'kom',
                'default' =>  0
            ],
            [
                'wares_code' =>  107018,
                'wares_name' =>  'Plug-in attenuator 6db, SA77150-06',
                'wares_type' =>  'kom',
                'default' =>  0
            ],
            [
                'wares_code' =>  107019,
                'wares_name' =>  'Plug-in Reverse Equalizer,Eq at 65Mhz,SA74140-1065',
                'wares_type' =>  'kom',
                'default' =>  0
            ],
            [
                'wares_code' =>  107020,
                'wares_name' =>  'Reverse amplifier module 65Mhz,21db gain,SA93144-10621',
                'wares_type' =>  'kom',
                'default' =>  0
            ],
            [
                'wares_code' =>  107021,
                'wares_name' =>  'Plug-in equaliser 9db 40-862Mhz,SA74100-10809',
                'wares_type' =>  'kom',
                'default' =>  0
            ],
            [
                'wares_code' =>  107137,
                'wares_name' =>  'Plug-in atenuator  14 db ',
                'wares_type' =>  'kom',
                'default' =>  0
            ],
            [
                'wares_code' =>  107138,
                'wares_name' =>  'Plug-in atenuator  16 db ',
                'wares_type' =>  'kom',
                'default' =>  0
            ],
            [
                'wares_code' =>  107142,
                'wares_name' =>  'Plug in diplex filter 85/105',
                'wares_type' =>  'kom',
                'default' =>  0
            ],
            [
                'wares_code' =>  107143,
                'wares_name' =>  'modul za AGC pojacavac',
                'wares_type' =>  'kom',
                'default' =>  0
            ],
            [
                'wares_code' =>  107144,
                'wares_name' =>  'Plug-in Rev Equalizer,85MHz,EGC Amps(Mult=10)',
                'wares_type' =>  'kom',
                'default' =>  0
            ],
            [
                'wares_code' =>  108005,
                'wares_name' =>  'Kutija metalna 350x300x150',
                'wares_type' =>  'kom',
                'default' =>  0
            ],
            [
                'wares_code' =>  108006,
                'wares_name' =>  'Kutija metalna 550x450x180',
                'wares_type' =>  'kom',
                'default' =>  0
            ],
            [
                'wares_code' =>  108009,
                'wares_name' =>  'Kutija metalna 550x450x180 za banderu',
                'wares_type' =>  'kom',
                'default' =>  0
            ],
            [
                'wares_code' =>  108012,
                'wares_name' =>  'Maska za Orman 250x250x70',
                'wares_type' =>  'kom',
                'default' =>  0
            ],
            [
                'wares_code' =>  108019,
                'wares_name' =>  'VVD kutija',
                'wares_type' =>  'kom',
                'default' =>  0
            ],
            [
                'wares_code' =>  108020,
                'wares_name' =>  'Metalna maska 150x150x50mm',
                'wares_type' =>  'kom',
                'default' =>  0
            ],
            [
                'wares_code' =>  108021,
                'wares_name' =>  'Kanalica metalna 30x50x350',
                'wares_type' =>  'kom',
                'default' =>  0
            ],
            [
                'wares_code' =>  108022,
                'wares_name' =>  'Kanalica metalna 30x50x2000',
                'wares_type' =>  'kom',
                'default' =>  0
            ],
            [
                'wares_code' =>  108023,
                'wares_name' =>  'PVC Kanalica 15x15',
                'wares_type' =>  'kom',
                'default' =>  0
            ],
            [
                'wares_code' =>  108024,
                'wares_name' =>  'PVC Kanalica 20x30',
                'wares_type' =>  'kom',
                'default' =>  0
            ],
            [
                'wares_code' =>  108025,
                'wares_name' =>  'PVC Kanalica 40x25',
                'wares_type' =>  'kom',
                'default' =>  0
            ],
            [
                'wares_code' =>  108027,
                'wares_name' =>  'PSK - 1/300',
                'wares_type' =>  'kom',
                'default' =>  0
            ],
            [
                'wares_code' =>  108033,
                'wares_name' =>  'PSK - 6 M12/230',
                'wares_type' =>  'kom',
                'default' =>  0
            ],
            [
                'wares_code' =>  108034,
                'wares_name' =>  'PSK - 6 M12/320',
                'wares_type' =>  'kom',
                'default' =>  0
            ],
            [
                'wares_code' =>  108035,
                'wares_name' =>  'PSK - 6 M12/380',
                'wares_type' =>  'kom',
                'default' =>  0
            ],
            [
                'wares_code' =>  108051,
                'wares_name' =>  'PSK - 10',
                'wares_type' =>  'kom',
                'default' =>  0
            ],
            [
                'wares_code' =>  108055,
                'wares_name' =>  'PSK - 13',
                'wares_type' =>  'kom',
                'default' =>  0
            ],
            [
                'wares_code' =>  108056,
                'wares_name' =>  'PSK - 16',
                'wares_type' =>  'kom',
                'default' =>  0
            ],
            [
                'wares_code' =>  108057,
                'wares_name' =>  'PSK - 17',
                'wares_type' =>  'kom',
                'default' =>  0
            ],
            [
                'wares_code' =>  108058,
                'wares_name' =>  'PSK - 18 M8',
                'wares_type' =>  'kom',
                'default' =>  0
            ],
            [
                'wares_code' =>  108059,
                'wares_name' =>  'PSK - 18 M10',
                'wares_type' =>  'kom',
                'default' =>  0
            ],
            [
                'wares_code' =>  108063,
                'wares_name' =>  'PSK - 26/76',
                'wares_type' =>  'kom',
                'default' =>  0
            ],
            [
                'wares_code' =>  108064,
                'wares_name' =>  'PSK - 26/170',
                'wares_type' =>  'kom',
                'default' =>  0
            ],
            [
                'wares_code' =>  108065,
                'wares_name' =>  'PSK - 26/170 - 4kuke',
                'wares_type' =>  'kom',
                'default' =>  0
            ],
            [
                'wares_code' =>  108066,
                'wares_name' =>  'PSK - 26/230',
                'wares_type' =>  'kom',
                'default' =>  0
            ],
            [
                'wares_code' =>  108067,
                'wares_name' =>  'PSK - 26/230 - 4kuke',
                'wares_type' =>  'kom',
                'default' =>  0
            ],
            [
                'wares_code' =>  108069,
                'wares_name' =>  'Stezaljka distributivna 17.07.09',
                'wares_type' =>  'kom',
                'default' =>  0
            ],
            [
                'wares_code' =>  108070,
                'wares_name' =>  'Stezaljka ugaona nosna',
                'wares_type' =>  'kom',
                'default' =>  0
            ],
            [
                'wares_code' =>  108071,
                'wares_name' =>  'Kanalica metalna 30X50 Levi nastavak',
                'wares_type' =>  'kom',
                'default' =>  0
            ],
            [
                'wares_code' =>  108077,
                'wares_name' =>  'Bravica univerzalna',
                'wares_type' =>  'kom',
                'default' =>  0
            ],
            [
                'wares_code' =>  108110,
                'wares_name' =>  'Rotor zateza?',
                'wares_type' =>  'kom',
                'default' =>  0
            ],
            [
                'wares_code' =>  108113,
                'wares_name' =>  'PSK-20 Sidro za beton',
                'wares_type' =>  'kom',
                'default' =>  0
            ],
            [
                'wares_code' =>  108114,
                'wares_name' =>  'PSK-26/170 sa T nosa?em',
                'wares_type' =>  'kom',
                'default' =>  0
            ],
            [
                'wares_code' =>  108115,
                'wares_name' =>  'PSK-26/230 sa T nosa?em',
                'wares_type' =>  'kom',
                'default' =>  0
            ],
            [
                'wares_code' =>  108133,
                'wares_name' =>  'Konzola za stub 40/40/4/1040',
                'wares_type' =>  'kom',
                'default' =>  0
            ],
            [
                'wares_code' =>  108134,
                'wares_name' =>  'Drzac za stub 40/40/40/250',
                'wares_type' =>  'kom',
                'default' =>  0
            ],
            [
                'wares_code' =>  108138,
                'wares_name' =>  'Jajasti izolator Fi30/40',
                'wares_type' =>  'kom',
                'default' =>  0
            ],
            [
                'wares_code' =>  108157,
                'wares_name' =>  'Kutija metalna  450x350x200',
                'wares_type' =>  'kom',
                'default' =>  0
            ],
            [
                'wares_code' =>  108162,
                'wares_name' =>  'Metalna kanalica za banderu ',
                'wares_type' =>  'kom',
                'default' =>  0
            ],
            [
                'wares_code' =>  108178,
                'wares_name' =>  'Brezon M10x250 2 matice i 2 podloške M10  ',
                'wares_type' =>  'kom',
                'default' =>  0
            ],
            [
                'wares_code' =>  108193,
                'wares_name' =>  'Ankeri ',
                'wares_type' =>  'kom',
                'default' =>  0
            ],
            [
                'wares_code' =>  108210,
                'wares_name' =>  'Ukrasna lajsna YS-2',
                'wares_type' =>  'kom',
                'default' =>  0
            ],
            [
                'wares_code' =>  110003,
                'wares_name' =>  'Crevo rebrasto fi16',
                'wares_type' =>  'kom',
                'default' =>  0
            ],
            [
                'wares_code' =>  110004,
                'wares_name' =>  'Crevo rebrasto fi23',
                'wares_type' =>  'kom',
                'default' =>  0
            ],
            [
                'wares_code' =>  110005,
                'wares_name' =>  'Crevo rebrasto fi29',
                'wares_type' =>  'kom',
                'default' =>  0
            ],
            [
                'wares_code' =>  110010,
                'wares_name' =>  'Luster grlo kerami?ko 133',
                'wares_type' =>  'kom',
                'default' =>  0
            ],
            [
                'wares_code' =>  110011,
                'wares_name' =>  'Luster klema 12 2,5mm',
                'wares_type' =>  'kom',
                'default' =>  0
            ],
            [
                'wares_code' =>  110014,
                'wares_name' =>  'Osigura? automatski 10A',
                'wares_type' =>  'kom',
                'default' =>  0
            ],
            [
                'wares_code' =>  110015,
                'wares_name' =>  'Osigura? automatski 16A',
                'wares_type' =>  'kom',
                'default' =>  0
            ],
            [
                'wares_code' =>  110034,
                'wares_name' =>  'Tabla za jedan osigura?',
                'wares_type' =>  'kom',
                'default' =>  0
            ],
            [
                'wares_code' =>  110040,
                'wares_name' =>  'Traka perforirana fi6',
                'wares_type' =>  'kom',
                'default' =>  0
            ],
            [
                'wares_code' =>  110051,
                'wares_name' =>  'Viljuška gumena šuko',
                'wares_type' =>  'kom',
                'default' =>  0
            ],
            [
                'wares_code' =>  110142,
                'wares_name' =>  'Utka? šuko',
                'wares_type' =>  'kom',
                'default' =>  0
            ],
            [
                'wares_code' =>  110143,
                'wares_name' =>  'Uti?nica OG',
                'wares_type' =>  'kom',
                'default' =>  0
            ],
            [
                'wares_code' =>  114039,
                'wares_name' =>  'okiten crevo fi 40',
                'wares_type' =>  'kom',
                'default' =>  0
            ],
            [
                'wares_code' =>  116054,
                'wares_name' =>  'Konektor kompresioni za RG59',
                'wares_type' =>  'kom',
                'default' =>  0
            ],
            [
                'wares_code' =>  116126,
                'wares_name' =>  'Adapter SCART / RCA 3 +SVHS',
                'wares_type' =>  'kom',
                'default' =>  0
            ],
            [
                'wares_code' =>  116363,
                'wares_name' =>  'Metalna le?a za razvodne ormane',
                'wares_type' =>  'kom',
                'default' =>  0
            ],
            [
                'wares_code' =>  116506,
                'wares_name' =>  'Adapter za kameru 500mW',
                'wares_type' =>  'kom',
                'default' =>  0
            ],
            [
                'wares_code' =>  116554,
                'wares_name' =>  'Kolor kamera',
                'wares_type' =>  'kom',
                'default' =>  0
            ],
            [
                'wares_code' =>  116635,
                'wares_name' =>  'Konektor kompresioni za RG6',
                'wares_type' =>  'kom',
                'default' =>  0
            ],
            [
                'wares_code' =>  116636,
                'wares_name' =>  'Konektor kompresioni za RG11',
                'wares_type' =>  'kom',
                'default' =>  0
            ],
            [
                'wares_code' =>  116882,
                'wares_name' =>  'Adapter IEC muški na F ženski PCT-IM-FF',
                'wares_type' =>  'kom',
                'default' =>  0
            ],
            [
                'wares_code' =>  116883,
                'wares_name' =>  'Adapter IEC ženski na F ženski PCT-IM-FF',
                'wares_type' =>  'kom',
                'default' =>  0
            ],
            [
                'wares_code' =>  116935,
                'wares_name' =>  'PSK-26/150x150 sa 4 kuke',
                'wares_type' =>  'kom',
                'default' =>  0
            ],
            [
                'wares_code' =>  116937,
                'wares_name' =>  'PSK-26/130x130 sa 4 kuke',
                'wares_type' =>  'kom',
                'default' =>  0
            ],
            [
                'wares_code' =>  116940,
                'wares_name' =>  'PSK-26/170x170 sa 4 kuke',
                'wares_type' =>  'kom',
                'default' =>  0
            ],
            [
                'wares_code' =>  116944,
                'wares_name' =>  'PSK-26/130X130 SA T NOSA?EM',
                'wares_type' =>  'kom',
                'default' =>  0
            ],
            [
                'wares_code' =>  116946,
                'wares_name' =>  'PSK-26/150X150 SA T NOSA?EM',
                'wares_type' =>  'kom',
                'default' =>  0
            ],
            [
                'wares_code' =>  116948,
                'wares_name' =>  'PSK-26/170X170 SA T NOSA?EM',
                'wares_type' =>  'kom',
                'default' =>  0
            ],
            [
                'wares_code' =>  117018,
                'wares_name' =>  'PSK-26/180x220',
                'wares_type' =>  'kom',
                'default' =>  0
            ],
            [
                'wares_code' =>  117020,
                'wares_name' =>  'PSK-26/150x170',
                'wares_type' =>  'kom',
                'default' =>  0
            ],
            [
                'wares_code' =>  117021,
                'wares_name' =>  'PSK-26/150x170 sa T nosa?em',
                'wares_type' =>  'kom',
                'default' =>  0
            ],
            [
                'wares_code' =>  117072,
                'wares_name' =>  'PCT-FAM-12 Oslabljiva? 12dB',
                'wares_type' =>  'kom',
                'default' =>  0
            ],
            [
                'wares_code' =>  117195,
                'wares_name' =>  'PCT-FAM-08 Oslabljiva? 8dB',
                'wares_type' =>  'kom',
                'default' =>  0
            ],
            [
                'wares_code' =>  117267,
                'wares_name' =>  'Plug-in diplex filter 65/85 SA75110-106587',
                'wares_type' =>  'kom',
                'default' =>  0
            ],
            [
                'wares_code' =>  117307,
                'wares_name' =>  'PSK 26/170 T',
                'wares_type' =>  'kom',
                'default' =>  0
            ],
            [
                'wares_code' =>  117308,
                'wares_name' =>  'PSK 26/230 T',
                'wares_type' =>  'kom',
                'default' =>  0
            ],
            [
                'wares_code' =>  117489,
                'wares_name' =>  'PCT-FAM-06 Oslabljiva? 06dB',
                'wares_type' =>  'kom',
                'default' =>  0
            ],
            [
                'wares_code' =>  117538,
                'wares_name' =>  'Plasti?ni obeleživa? za kabl',
                'wares_type' =>  'kom',
                'default' =>  0
            ],
            [
                'wares_code' =>  117998,
                'wares_name' =>  'Compact EGC A93240.10338 ',
                'wares_type' =>  'kom',
                'default' =>  0
            ],
            [
                'wares_code' =>  118558,
                'wares_name' =>  '90 F ugaoni f adapter',
                'wares_type' =>  'kom',
                'default' =>  0
            ],
            [
                'wares_code' =>  118561,
                'wares_name' =>  '71 F musko-muski f adapter',
                'wares_type' =>  'kom',
                'default' =>  0
            ],
            [
                'wares_code' =>  118726,
                'wares_name' =>  'RJ-45 konektor kat. 5E 8P8C 8-pinski',
                'wares_type' =>  'kom',
                'default' =>  0
            ],
            [
                'wares_code' =>  118920,
                'wares_name' =>  'Kutija metalna  200x200x130',
                'wares_type' =>  'kom',
                'default' =>  0
            ],
            [
                'wares_code' =>  119201,
                'wares_name' =>  'OG Razvodnik \'T\' sa 3 mesta',
                'wares_type' =>  'kom',
                'default' =>  0
            ],
            [
                'wares_code' =>  119344,
                'wares_name' =>  'Return Atenuator 6 dB',
                'wares_type' =>  'kom',
                'default' =>  0
            ],
            [
                'wares_code' =>  119345,
                'wares_name' =>  'Return Atenuator 8 dB',
                'wares_type' =>  'kom',
                'default' =>  0
            ],
            [
                'wares_code' =>  119346,
                'wares_name' =>  'Return Atenuator 12 dB',
                'wares_type' =>  'kom',
                'default' =>  0
            ],
            [
                'wares_code' =>  119376,
                'wares_name' =>  'Samoskupljaju?i bužir  za P28 nastavak ',
                'wares_type' =>  'kom',
                'default' =>  0
            ],
            [
                'wares_code' =>  119377,
                'wares_name' =>  'Samoskupljaju?i bužir  za RG11 konektor',
                'wares_type' =>  'kom',
                'default' =>  0
            ],
            [
                'wares_code' =>  119378,
                'wares_name' =>  'Samoskupljaju?i bužir  za T32 konektor ',
                'wares_type' =>  'kom',
                'default' =>  0
            ],
            [
                'wares_code' =>  119568,
                'wares_name' =>  'RJ-11 telefonski konektor',
                'wares_type' =>  'kom',
                'default' =>  0
            ],
            [
                'wares_code' =>  119681,
                'wares_name' =>  'Modulator za 66 kanal',
                'wares_type' =>  'kom',
                'default' =>  0
            ],
            [
                'wares_code' =>  119833,
                'wares_name' =>  'Izolator galvanski jednograni',
                'wares_type' =>  'kom',
                'default' =>  0
            ],
            [
                'wares_code' =>  120152,
                'wares_name' =>  'Teleste L Amp CX3L085',
                'wares_type' =>  'kom',
                'default' =>  0
            ],
            [
                'wares_code' =>  120201,
                'wares_name' =>  'Teleste D Amp CX3R085',
                'wares_type' =>  'kom',
                'default' =>  0
            ],
            [
                'wares_code' =>  120543,
                'wares_name' =>  'Filter povratnog smera HPF',
                'wares_type' =>  'kom',
                'default' =>  0
            ],
            [
                'wares_code' =>  330001,
                'wares_name' =>  'Gips (alabaster)',
                'wares_type' =>  'kom',
                'default' =>  0
            ],
            [
                'wares_code' =>  330003,
                'wares_name' =>  'Osiguru? cevasti 5A 6X32mm',
                'wares_type' =>  'kom',
                'default' =>  0
            ],
            [
                'wares_code' =>  330004,
                'wares_name' =>  'Osigura? cevasti 3.15A 6X32mm',
                'wares_type' =>  'kom',
                'default' =>  0
            ],
            [
                'wares_code' =>  330008,
                'wares_name' =>  'Rascepka fi6',
                'wares_type' =>  'kom',
                'default' =>  0
            ],
            [
                'wares_code' =>  330015,
                'wares_name' =>  'Tipl PVC fi10',
                'wares_type' =>  'kom',
                'default' =>  0
            ],
            [
                'wares_code' =>  330017,
                'wares_name' =>  'Tipl PVC fi6',
                'wares_type' =>  'kom',
                'default' =>  0
            ],
            [
                'wares_code' =>  330018,
                'wares_name' =>  'Tipl PVC fi8',
                'wares_type' =>  'kom',
                'default' =>  0
            ],
            [
                'wares_code' =>  330019,
                'wares_name' =>  'Traka izolir',
                'wares_type' =>  'kom',
                'default' =>  0
            ],
            [
                'wares_code' =>  330020,
                'wares_name' =>  'Traka termoskupljaju?a (PIB)',
                'wares_type' =>  'kom',
                'default' =>  0
            ],
            [
                'wares_code' =>  330021,
                'wares_name' =>  'Vezica PVC 2.5 X 100',
                'wares_type' =>  'kom',
                'default' =>  0
            ],
            [
                'wares_code' =>  330022,
                'wares_name' =>  'Vezica PVC 4.8 X 280',
                'wares_type' =>  'kom',
                'default' =>  0
            ],
            [
                'wares_code' =>  330023,
                'wares_name' =>  'Vijak krstasti 4x35',
                'wares_type' =>  'kom',
                'default' =>  0
            ],
            [
                'wares_code' =>  330025,
                'wares_name' =>  'Vijak krstasti 6X50',
                'wares_type' =>  'kom',
                'default' =>  0
            ],
            [
                'wares_code' =>  330033,
                'wares_name' =>  'Silikon tuba ',
                'wares_type' =>  'kom',
                'default' =>  0
            ],
            [
                'wares_code' =>  330039,
                'wares_name' =>  'Vijak krstasti 5x50',
                'wares_type' =>  'kom',
                'default' =>  0
            ],
            [
                'wares_code' =>  330182,
                'wares_name' =>  'Ekser obujmice',
                'wares_type' =>  'kom',
                'default' =>  0
            ],
            [
                'wares_code' =>  440060,
                'wares_name' =>  'Lockin terminatori',
                'wares_type' =>  'kom',
                'default' =>  0
            ]
        ]);
    }
}
