<?php

use Illuminate\Database\Seeder;

class TechnicianReportTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('technicians_report')->insert([
            [

                'technician_id' => '22',
                'order_type' => 'sbb_order',
                'ordinal_number' => '19-11-00746',
                'service_name' => 'dodatno_prikljucenje',
                'price_apartment' => null,
                'price_house' => '0',
                'price_first_equipment' => '400',
                'price_additional_equipment' => '200',
                'price_fixed' => null,
                'price_private_vehicle' => '50',
                'sum' => '650',
                'created_at' => '2019-12-02 17:48:03',
                'updated_at' => null
            ], [

                'technician_id' => '22',
                'order_type' => 'sbb_order',
                'ordinal_number' => '19-11-00728',
                'service_name' => 'dodatno_prikljucenje',
                'price_apartment' => null,
                'price_house' => '0',
                'price_first_equipment' => '400',
                'price_additional_equipment' => null,
                'price_fixed' => null,
                'price_private_vehicle' => '50',
                'sum' => '450',
                'created_at' => '2019-12-02 17:47:28',
                'updated_at' => null
            ], [

                'technician_id' => '22',
                'order_type' => 'sbb_order',
                'ordinal_number' => '19-11-00528',
                'service_name' => 'dodatno_prikljucenje',
                'price_apartment' => null,
                'price_house' => '0',
                'price_first_equipment' => '400',
                'price_additional_equipment' => '200',
                'price_fixed' => null,
                'price_private_vehicle' => '50',
                'sum' => '650',
                'created_at' => '2019-12-02 17:46:45',
                'updated_at' => null
            ], [

                'technician_id' => '22',
                'order_type' => 'sbb_order',
                'ordinal_number' => '19-11-00506',
                'service_name' => 'dodatno_prikljucenje',
                'price_apartment' => null,
                'price_house' => '0',
                'price_first_equipment' => '400',
                'price_additional_equipment' => null,
                'price_fixed' => null,
                'price_private_vehicle' => '50',
                'sum' => '450',
                'created_at' => '2019-12-02 17:46:00',
                'updated_at' => null
            ], [

                'technician_id' => '22',
                'order_type' => 'sbb_order',
                'ordinal_number' => '19-11-00047',
                'service_name' => 'dodatno_prikljucenje',
                'price_apartment' => null,
                'price_house' => '0',
                'price_first_equipment' => '400',
                'price_additional_equipment' => '100',
                'price_fixed' => null,
                'price_private_vehicle' => '50',
                'sum' => '550',
                'created_at' => '2019-12-02 17:45:08',
                'updated_at' => null
            ], [

                'technician_id' => '39',
                'order_type' => 'sbb_order',
                'ordinal_number' => '19-11-01217',
                'service_name' => 'dodatno_priklju\u010denje_etth',
                'price_apartment' => '0',
                'price_house' => null,
                'price_first_equipment' => '400',
                'price_additional_equipment' => '100',
                'price_fixed' => null,
                'price_private_vehicle' => null,
                'sum' => '500',
                'created_at' => '2019-12-02 15:30:19',
                'updated_at' => null
            ], [

                'technician_id' => '25',
                'order_type' => 'sbb_order',
                'ordinal_number' => '19-11-01254',
                'service_name' => 'dodatno_prikljucenje',
                'price_apartment' => null,
                'price_house' => '0',
                'price_first_equipment' => '400',
                'price_additional_equipment' => '100',
                'price_fixed' => null,
                'price_private_vehicle' => '50',
                'sum' => '550',
                'created_at' => '2019-12-02 15:26:36',
                'updated_at' => null
            ], [

                'technician_id' => '25',
                'order_type' => 'sbb_order',
                'ordinal_number' => '19-11-01256',
                'service_name' => 'dodatno_prikljucenje',
                'price_apartment' => null,
                'price_house' => '0',
                'price_first_equipment' => '400',
                'price_additional_equipment' => null,
                'price_fixed' => null,
                'price_private_vehicle' => '50',
                'sum' => '450',
                'created_at' => '2019-12-02 15:23:30',
                'updated_at' => null
            ], [

                'technician_id' => '25',
                'order_type' => 'sbb_order',
                'ordinal_number' => '19-11-01227',
                'service_name' => 'prvo_prikljucenje',
                'price_apartment' => '600',
                'price_house' => null,
                'price_first_equipment' => '400',
                'price_additional_equipment' => '200',
                'price_fixed' => null,
                'price_private_vehicle' => '100',
                'sum' => '1300',
                'created_at' => '2019-12-02 15:21:32',
                'updated_at' => null
            ], [

                'technician_id' => '25',
                'order_type' => 'sbb_order',
                'ordinal_number' => '19-11-01235',
                'service_name' => 'prvo_prikljucenje',
                'price_apartment' => '600',
                'price_house' => null,
                'price_first_equipment' => '400',
                'price_additional_equipment' => '100',
                'price_fixed' => null,
                'price_private_vehicle' => '100',
                'sum' => '1200',
                'created_at' => '2019-12-02 15:17:48',
                'updated_at' => null
            ], [

                'technician_id' => '39',
                'order_type' => 'sbb_order',
                'ordinal_number' => '19-11-01153',
                'service_name' => 'dodatno_priklju\u010denje_etth',
                'price_apartment' => '0',
                'price_house' => null,
                'price_first_equipment' => '400',
                'price_additional_equipment' => null,
                'price_fixed' => null,
                'price_private_vehicle' => null,
                'sum' => '400',
                'created_at' => '2019-12-02 15:13:38',
                'updated_at' => null
            ], [

                'technician_id' => '36',
                'order_type' => 'sbb_order',
                'ordinal_number' => '19-11-01179',
                'service_name' => 'preseljenje',
                'price_apartment' => null,
                'price_house' => '1600',
                'price_first_equipment' => null,
                'price_additional_equipment' => null,
                'price_fixed' => null,
                'price_private_vehicle' => null,
                'sum' => '1600',
                'created_at' => '2019-12-02 15:11:28',
                'updated_at' => null
            ], [

                'technician_id' => '10',
                'order_type' => 'sbb_order',
                'ordinal_number' => '19-11-01109',
                'service_name' => 'dodatno_priklju\u010denje_etth',
                'price_apartment' => null,
                'price_house' => '0',
                'price_first_equipment' => '400',
                'price_additional_equipment' => null,
                'price_fixed' => null,
                'price_private_vehicle' => null,
                'sum' => '400',
                'created_at' => '2019-12-02 15:06:09',
                'updated_at' => null
            ], [

                'technician_id' => '10',
                'order_type' => 'sbb_order',
                'ordinal_number' => '19-11-01027',
                'service_name' => 'dodatno_priklju\u010denje_etth',
                'price_apartment' => null,
                'price_house' => '0',
                'price_first_equipment' => '400',
                'price_additional_equipment' => null,
                'price_fixed' => null,
                'price_private_vehicle' => null,
                'sum' => '400',
                'created_at' => '2019-12-02 15:02:21',
                'updated_at' => null
            ], [

                'technician_id' => '39',
                'order_type' => 'sbb_order',
                'ordinal_number' => '19-11-01054',
                'service_name' => 'dodatno_priklju\u010denje_etth',
                'price_apartment' => '0',
                'price_house' => null,
                'price_first_equipment' => '400',
                'price_additional_equipment' => null,
                'price_fixed' => null,
                'price_private_vehicle' => null,
                'sum' => '400',
                'created_at' => '2019-12-02 15:01:28',
                'updated_at' => null
            ], [

                'technician_id' => '10',
                'order_type' => 'sbb_order',
                'ordinal_number' => '19-11-01214',
                'service_name' => 'dodatno_priklju\u010denje_etth',
                'price_apartment' => null,
                'price_house' => '0',
                'price_first_equipment' => '400',
                'price_additional_equipment' => null,
                'price_fixed' => null,
                'price_private_vehicle' => null,
                'sum' => '400',
                'created_at' => '2019-12-02 15:00:16',
                'updated_at' => null
            ], [

                'technician_id' => '10',
                'order_type' => 'sbb_order',
                'ordinal_number' => '19-11-01052',
                'service_name' => 'dodatno_priklju\u010denje_etth',
                'price_apartment' => null,
                'price_house' => '0',
                'price_first_equipment' => '400',
                'price_additional_equipment' => '100',
                'price_fixed' => null,
                'price_private_vehicle' => null,
                'sum' => '500',
                'created_at' => '2019-12-02 14:58:30',
                'updated_at' => null
            ], [

                'technician_id' => '39',
                'order_type' => 'sbb_order',
                'ordinal_number' => '19-11-01218',
                'service_name' => 'dodatno_priklju\u010denje_etth',
                'price_apartment' => null,
                'price_house' => null,
                'price_first_equipment' => null,
                'price_additional_equipment' => null,
                'price_fixed' => '400',
                'price_private_vehicle' => null,
                'sum' => '400',
                'created_at' => '2019-12-02 14:57:36',
                'updated_at' => null
            ], [

                'technician_id' => '39',
                'order_type' => 'sbb_order',
                'ordinal_number' => '19-11-01257',
                'service_name' => 'dodatno_priklju\u010denje_etth',
                'price_apartment' => '0',
                'price_house' => null,
                'price_first_equipment' => '400',
                'price_additional_equipment' => null,
                'price_fixed' => null,
                'price_private_vehicle' => null,
                'sum' => '400',
                'created_at' => '2019-12-02 14:55:47',
                'updated_at' => null
            ], [

                'technician_id' => '22',
                'order_type' => 'sbb_order',
                'ordinal_number' => '19-11-01122',
                'service_name' => 'zamena_opreme',
                'price_apartment' => null,
                'price_house' => null,
                'price_first_equipment' => null,
                'price_additional_equipment' => null,
                'price_fixed' => '400',
                'price_private_vehicle' => '50',
                'sum' => '50',
                'created_at' => '2019-12-02 14:52:59',
                'updated_at' => null
            ], [

                'technician_id' => '22',
                'order_type' => 'sbb_order',
                'ordinal_number' => '19-11-01222',
                'service_name' => 'dodatno_prikljucenje',
                'price_apartment' => null,
                'price_house' => '0',
                'price_first_equipment' => '400',
                'price_additional_equipment' => null,
                'price_fixed' => null,
                'price_private_vehicle' => '50',
                'sum' => '450',
                'created_at' => '2019-12-02 14:37:21',
                'updated_at' => null
            ], [

                'technician_id' => '22',
                'order_type' => 'sbb_order',
                'ordinal_number' => '19-11-00985',
                'service_name' => 'dodatno_prikljucenje',
                'price_apartment' => null,
                'price_house' => '0',
                'price_first_equipment' => '400',
                'price_additional_equipment' => null,
                'price_fixed' => null,
                'price_private_vehicle' => '50',
                'sum' => '450',
                'created_at' => '2019-12-02 14:24:04',
                'updated_at' => null
            ], [

                'technician_id' => '22',
                'order_type' => 'sbb_order',
                'ordinal_number' => '19-11-01190',
                'service_name' => 'dodatno_prikljucenje',
                'price_apartment' => '0',
                'price_house' => null,
                'price_first_equipment' => '400',
                'price_additional_equipment' => '100',
                'price_fixed' => null,
                'price_private_vehicle' => '50',
                'sum' => '550',
                'created_at' => '2019-12-02 14:21:19',
                'updated_at' => null
            ], [

                'technician_id' => '22',
                'order_type' => 'sbb_order',
                'ordinal_number' => '19-11-00907',
                'service_name' => 'dodatno_prikljucenje',
                'price_apartment' => '0',
                'price_house' => null,
                'price_first_equipment' => '400',
                'price_additional_equipment' => '100',
                'price_fixed' => null,
                'price_private_vehicle' => '50',
                'sum' => '550',
                'created_at' => '2019-12-02 14:18:40',
                'updated_at' => null
            ], [

                'technician_id' => '22',
                'order_type' => 'sbb_order',
                'ordinal_number' => '19-11-01207',
                'service_name' => 'dodatno_prikljucenje',
                'price_apartment' => '0',
                'price_house' => null,
                'price_first_equipment' => '400',
                'price_additional_equipment' => '100',
                'price_fixed' => null,
                'price_private_vehicle' => '50',
                'sum' => '550',
                'created_at' => '2019-12-02 14:14:37',
                'updated_at' => null
            ], [

                'technician_id' => '36',
                'order_type' => 'sbb_order',
                'ordinal_number' => '19-11-00760',
                'service_name' => 'prvo_prikljucenje',
                'price_apartment' => null,
                'price_house' => null,
                'price_first_equipment' => null,
                'price_additional_equipment' => null,
                'price_fixed' => '400',
                'price_private_vehicle' => null,
                'sum' => '400',
                'created_at' => '2019-12-02 14:10:42',
                'updated_at' => null
            ]
        ]);
    }
}



