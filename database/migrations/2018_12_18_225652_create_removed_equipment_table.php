<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRemovedEquipmentTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('removed_equipment', function (Blueprint $table) {
            $table->increments('removed_equipment_id');
            $table->string('ordinal_number');
            $table->string('removed_equipment_subtype');
            $table->string('removed_equipment_serial1');
            $table->string('removed_equipment_serial2')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('removed_equipment');
    }
}
