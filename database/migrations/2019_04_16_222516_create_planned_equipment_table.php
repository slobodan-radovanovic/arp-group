<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePlannedEquipmentTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('planned_equipment', function (Blueprint $table) {
            $table->increments('planned_equipment_id');
            $table->string('planned_equipment_ordinal_number');
            $table->string('planned_equipment_subtype');
            $table->string('planned_equipment_status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('planned_equipment');
    }
}
