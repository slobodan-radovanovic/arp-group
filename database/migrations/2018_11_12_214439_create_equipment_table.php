<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEquipmentTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('equipment', function (Blueprint $table) {
            $table->increments('equipment_id');
            $table->string('equipment_code');
            $table->string('equipment_type_id');
            $table->string('equipment_subtype_id');
            $table->string('equipment_model_id');
            $table->string('equipment_serial1');
            $table->string('equipment_serial2')->nullable();
            $table->string('equipment_serial3')->nullable();
            $table->string('equipment_status');
            $table->string('equipment_technician')->nullable();
            $table->dateTime('equipment_assign_date')->nullable();
            $table->string('equipment_ordinal_number')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('equipment');
    }
}
