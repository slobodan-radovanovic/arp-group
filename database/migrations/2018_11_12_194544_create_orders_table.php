<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->increments('order_id');
            $table->string('ordinal_number')->unique();  //redni broj naloga, jedinstven 18-03-00015
            $table->string('order_number')->unique()->nullable();  //broj naloga, jedinstven
            $table->string('order_type');               // sbb, digitalizacija, reklamacija
            $table->string('order_status');            // status 1,2,3,4
            $table->string('buyer_id')->nullable();                // broj kupca (291002564)
            $table->string('treaty_id')->nullable();               // broj ugovora
            $table->string('buyer')->nullable();                    // ime i prezime kupca
            $table->string('area_code')->nullable();               // postnski broj (11158)
            $table->string('city');                     // Beograd
            $table->string('region')->nullable();                   // BG-1
            $table->string('township')->nullable();                 // Stari grad
            $table->string('address');                  // Dunavski Kej
            $table->string('address_number')->nullable();           // 15a (string zbog mogucnosti da bude slovo ili "bb")

            $table->string('floor')->nullable();                   //sprat
            $table->string('apartment')->nullable();               //broj stana
            $table->string('phone_number')->nullable();            //broj telefona 1
            $table->string('mobile_number')->nullable();           //broj telefona 2
            /*$table->integer('technician_1');              //operater 1 (id)
            $table->integer('technician_2');              //operater 2 (id)
            */
            $table->string('service_type')->nullable();   // tip posla, dodatno prikljucenje ...
            $table->integer('private_vehicle')->nullable();         // da li se koristi privatno vozilo
            /*$table->string('equipment_class');
            $table->string('equipment_type');
            $table->string('equipment_activity');*/
            $table->string('service_home')->nullable();             // da li je stan ili kuca
            $table->string('note')->nullable();                     // napomena, komentar
            $table->dateTime('opened_at')->nullable();  // datum i vreme prijave (8.10.2018 11:43:26)
            $table->dateTime('started_at')->nullable(); // predvidjeni pocetak rada (8.10.2018 161:44:54)
            $table->dateTime('created_at');              // kada je nalog upisan u sistem
            $table->dateTime('completed_at')->nullable(); // kada je nalog zavrsen, kada je zatvoren u sistemu
            $table->dateTime('updated_at')->nullable();   // kada je izvrsena bilo kakva izmena na nalogu

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
