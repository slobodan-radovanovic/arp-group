<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateServicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('services', function (Blueprint $table) {
            $table->increments('service_id');
            $table->string('service_select');
            $table->string('service_name');
            $table->string('order_type');
            $table->string('price_apartment')->nullable();
            $table->string('price_house')->nullable();
            $table->string('price_first_equipment')->nullable();
            $table->string('price_additional_equipment')->nullable();
            $table->string('price_fixed')->nullable();
            $table->string('price_private_vehicle')->nullable();
            $table->timestamps();
        });
    }

    /**
     * echo str_replace(' ', '_', strtolower("Hello WORLD"));
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('services');
    }
}
