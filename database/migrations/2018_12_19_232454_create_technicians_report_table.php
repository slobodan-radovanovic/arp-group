<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTechniciansReportTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('technicians_report', function (Blueprint $table) {
            $table->increments('technicians_report_id');
            $table->string('technician_id');
            $table->string('order_type');
            $table->string('ordinal_number');
            $table->string('service_name');
            $table->string('price_apartment')->nullable();
            $table->string('price_house')->nullable();
            $table->string('price_first_equipment')->nullable();
            $table->string('price_additional_equipment')->nullable();
            $table->string('price_fixed')->nullable();
            $table->string('price_private_vehicle')->nullable();
            $table->string('sum')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('technicians_report');
    }
}
