<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/logout', 'Auth\LoginController@logout')->name('logout' );


Route::group(['middleware' => 'auth'], function () {

    Route::get('/', 'PagesController@index');
    Route::get('/home', 'PagesController@index');


    /*Route::resource('orders', 'OrdersController')->except([
        'edit', 'store']);


    Route::post('/orders', 'OrdersController@index');
    Route::post('/orders/store', 'OrdersController@store');*/
    Route::resource('orders', 'OrdersController')->except(['store', 'show']);
    Route::get('/orders/create_with_status', 'OrdersController@create_with_status');
    Route::get('/orders/{id}/{print}', 'OrdersController@show');
    Route::post('/orders', 'OrdersController@index');
    Route::get('/all_orders', 'OrdersController@index_all');
    Route::get('/group_search', 'OrdersController@group_search');
    Route::post('/all_orders', 'OrdersController@index_all');
    Route::post('/group_search', 'OrdersController@group_search');
    Route::post('/orders/store', 'OrdersController@store');
    Route::post('/orders/store_with_status', 'OrdersController@store_with_status');
    Route::put('/orders/update_info/{id}', 'OrdersController@update_info')->name('order.update_info');
    Route::put('/orders/store_file/{id}', 'OrdersController@store_file')->name('order.store_file');
    Route::put('/orders/delete_file/{id}', 'OrdersController@delete_file')->name('order.delete_file');

    Route::get('/technicians/deleted', 'TechniciansController@deleted');
    Route::get('/technicians/assigned', 'TechniciansController@assigned');
    Route::resource('technicians', 'TechniciansController')->except('edit');

    /*Route::get('/orders', 'OrdersController@index');
    Route::get('/orders/create', 'OrdersController@create');*/
    Route::get('/digitization', 'DigitizationController@index');
    Route::get('/digitization/create', 'DigitizationController@create');
    Route::post('/digitization/store', 'DigitizationController@store')->name('digitization.store');
    Route::get('/digitization/{id}', 'DigitizationController@show');
    Route::post('/digitization', 'DigitizationController@index');

    Route::resource('reclamation', 'ReclamationController')->except([
        'edit', 'store']);

    Route::post('/reclamation', 'ReclamationController@index');
    Route::post('/reclamation/store', 'ReclamationController@store');
   /* Route::get('/reclamation', 'ReclamationController@index');
    Route::get('/reclamation/create', 'ReclamationController@create');
    Route::post('/reclamation/', 'ReclamationController@store')->name('reclamation.store');
    Route::get('/reclamation/{id}', 'ReclamationController@show');
    Route::post('/reclamation/{id}', 'ReclamationController@update');*/
    /*Route::get('/technicians', 'TechniciansController@index');
    Route::get('/technicians/create', 'TechniciansController@create');*/

    Route::get('/scheduling', 'PagesController@scheduling');
    /*Route::get('/pages/assigned', 'PagesController@assigned');
    Route::get('/pages/assigned/{technican}', 'PagesController@showassigned');*/

    Route::get('/equipment/add', 'EquipmentController@add');
    Route::get('/equipment/test', 'EquipmentController@equipment_test');
    Route::get('/equipment/edit', 'EquipmentController@edit_names');
    Route::get('/equipment/dispatch_note', 'EquipmentController@dispatch_note');
    Route::post('/equipment/dispatch_note', 'EquipmentController@dispatch_note');
    Route::get('/equipment/assignment', 'EquipmentController@assignment');
    Route::get('/equipment/assigned', 'EquipmentController@assigned');
    Route::post('/equipment/assigned', 'EquipmentController@assigned_post');
    Route::get('/equipment/discharge', 'EquipmentController@discharge');
    Route::get('/equipment/discharged_equipment', 'EquipmentController@discharged_equipment');
    Route::get('/equipment/dismantled', 'EquipmentController@dismantled');
    Route::post('/equipment/dismantled', 'EquipmentController@dismantled');
    Route::post('/equipment/dismantled_discharge', 'EquipmentController@dismantled_discharge');
    Route::get('/equipment/installed_equipment', 'EquipmentController@installed_equipment');
    Route::get('/equipment/deleted_equipment', 'EquipmentController@deleted_equipment');
    Route::get('/equipment/sbb_warehouse', 'EquipmentController@sbb_warehouse');
    Route::resource('equipment', 'EquipmentController')/*->except('edit')*/;
    Route::post('/equipment/store_subtype', 'EquipmentController@store_subtype');
    Route::post('/equipment/store_model', 'EquipmentController@store_model');
    Route::post('/equipment/store_dismantled_subtype', 'EquipmentController@store_dismantled_subtype');
    Route::post('/equipment/store_dismantled_model', 'EquipmentController@store_dismantled_model');

    Route::resource('wares', 'WaresController')->except('edit');
    Route::get('/test', 'WarehouseController@test');
    /*Route::get('/warehouse', 'WarehouseController@index');*/
    Route::get('/warehouse/external', 'WarehouseController@external');
    Route::get('/warehouse/add', 'WarehouseController@add');
    Route::post('/warehouse/assign', 'WarehouseController@assign');
    Route::get('/warehouse/assignment', 'WarehouseController@assignment');
    Route::get('/warehouse/dispatch_note', 'WarehouseController@dispatch_note');
    Route::post('/warehouse/dispatch_note', 'WarehouseController@dispatch_note');
    Route::resource('warehouse', 'WarehouseController')->except('edit');

    Route::get('/financereport', 'PagesController@financereport');
    Route::post('/financereport', 'PagesController@financereport');
    Route::get('/waresreport', 'PagesController@waresreport');
    Route::post('/waresreport', 'PagesController@waresreport');
    Route::get('/sbbreport', 'PagesController@sbbreport');
    Route::post('/sbbreport', 'PagesController@sbbreport');

    Route::get('/sbbreport_completed', 'PagesController@sbbreport_completed');
    Route::get('/sbbreport_canceled', 'PagesController@sbbreport_canceled');
    Route::get('/sbbreport_in_progress', 'PagesController@sbbreport_in_progress');

    Route::post('/sbbreport_completed', 'PagesController@sbbreport_completed');
    Route::post('/sbbreport_canceled', 'PagesController@sbbreport_canceled');
    Route::post('/sbbreport_in_progress', 'PagesController@sbbreport_in_progress');

    Route::get('/techniciansreport', 'PagesController@techniciansreport');
    Route::get('/techniciansreport_all', 'PagesController@techniciansreport_all');
    Route::post('/techniciansreport', 'PagesController@techniciansreport_post');
    Route::post('/techniciansreport_all', 'PagesController@techniciansreport_all_post');
    Route::get('/techniciansreport/{id}', 'PagesController@techniciansreport_show');
    Route::put('/techniciansreport/{id}', 'PagesController@update');
    Route::resource('service', 'ServicesController')->except('edit');
    Route::post('/service/reclamatiion', 'ServicesController@store_reclamation');
    Route::resource('users', 'UserController')->except('edit');

    Route::get('/print_open_order_preview/{order}', 'PrintController@print_open_order_preview');
    Route::get('/print_open_order/{order}', 'PrintController@print_open_order');
    Route::get('/print_close_order_preview/{order}', 'PrintController@print_open_order_preview');
    Route::get('/print_close_order/{order}', 'PrintController@print_close_order');

    Route::get('/townships', 'PagesController@townships');
    Route::get('/active_townships', 'PagesController@active_townships');
    Route::post('/townships/add_city', 'PagesController@add_city');
    Route::post('/townships/add_township', 'PagesController@add_township');
    Route::post('/townships/add_region', 'PagesController@add_region');

    Route::put('/update_city/{id}', 'PagesController@update_city');
    Route::put('/update_township/{id}', 'PagesController@update_township');
    Route::put('/update_region/{id}', 'PagesController@update_region');

    Route::get('logs', '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index');


    Route::get  ('/tabela', 'PagesController@tabela');



});


