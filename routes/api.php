<?php

use Illuminate\Http\Request;
use Yajra\DataTables\Contracts\DataTable;
use Illuminate\Support\Facades\DB;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::group(['middlewere' => 'api'], function(){

    Route::get('neupotrebljena_oprema', function(){
        return datatables()->of(DB::table('equipment')
            ->join('equipment_subtype', 'equipment.equipment_subtype_id', '=', 'equipment_subtype.equipment_subtype_id')
            ->join('equipment_model', 'equipment.equipment_model_id', '=', 'equipment_model.equipment_model_id')
            ->leftJoin('technicians', 'equipment.equipment_technician', '=', 'technicians.technician_id')
            ->select('equipment.equipment_type_id','equipment.equipment_subtype_id','equipment.equipment_model_id','equipment.equipment_id','equipment.equipment_status','equipment.equipment_serial1','equipment.equipment_serial2','equipment.equipment_serial3','equipment.equipment_dispatch_note','equipment.equipment_added_by','equipment.created_at','equipment_model.equipment_model_name', 'equipment_model.equipment_code', 'equipment_subtype.equipment_subtype_name', 'technicians.technician_name')
            ->where('equipment_status', '!=', 'Kod kupca')
            ->where('equipment_status', '!=', 'Izbrisan')
            ->where('equipment_status', '!=', 'SBB magacin')
            ->get())->toJson();
    })->name('neupotrebljena_oprema');



    Route::get('oprema_u_magacinu', function(){
        return datatables()->of(DB::table('equipment')
            ->join('equipment_subtype', 'equipment.equipment_subtype_id', '=', 'equipment_subtype.equipment_subtype_id')
            ->join('equipment_model', 'equipment.equipment_model_id', '=', 'equipment_model.equipment_model_id')
            ->select('equipment.equipment_serial1','equipment.equipment_serial2','equipment.equipment_serial3','equipment_model.equipment_model_name', 'equipment_model.equipment_code', 'equipment_subtype.equipment_subtype_name')
            ->where('equipment_status', '=', 'U magacinu')
            ->get())->toJson();
    })->name('oprema_u_magacinu');


    Route::get('oprema_kod_tehnicara', function(){
        return datatables()->of(DB::table('equipment')
            ->join('equipment_subtype', 'equipment.equipment_subtype_id', '=', 'equipment_subtype.equipment_subtype_id')
            ->join('equipment_model', 'equipment.equipment_model_id', '=', 'equipment_model.equipment_model_id')
            ->join('technicians', 'equipment.equipment_technician', '=', 'technicians.technician_id')
            ->select('equipment.equipment_id','equipment.equipment_serial1','equipment.equipment_serial2','equipment.equipment_serial3','equipment_model.equipment_model_name', 'equipment_model.equipment_code', 'equipment_subtype.equipment_subtype_name', 'technicians.technician_name')
            ->where('equipment_status', '=', 'Kod tehničara')
            ->get())->toJson();
    })->name('oprema_kod_tehnicara');



    Route::get('svi_nalozi', function(){
        return datatables()->of(DB::table('orders')
            ->join('services', 'orders.service_type', '=', 'services.service_select')
            ->where('orders.order_type', '=','sbb_order')
            ->get())->toJson();
    })->name('svi_nalozi');

    Route::get('razduzena_oprema', function(){
        return datatables()->of(DB::table('discharged_equipment')
            ->join('equipment', 'discharged_equipment.equipment_id', '=', 'equipment.equipment_id')
            ->join('equipment_model', 'equipment.equipment_model_id', '=', 'equipment_model.equipment_model_id')
            ->join('equipment_subtype', 'equipment.equipment_subtype_id', '=', 'equipment_subtype.equipment_subtype_id')
            ->join('technicians', 'equipment.equipment_technician', '=', 'technicians.technician_id')
            ->select('equipment_model.equipment_code', 'equipment_subtype.equipment_subtype_name', 'equipment_model.equipment_model_name', 'equipment.equipment_serial1', 'equipment.equipment_serial2', 'equipment.equipment_serial3', 'technicians.technician_name', 'discharged_equipment.discharged_by', 'discharged_equipment.equipment_assign_date',  'discharged_equipment.created_at')
            ->get())->toJson();
    })->name('razduzena_oprema');



    /* Route::get('contacts', function(){
         return Contact::latest()->orderBy('created_at', 'desc')->get();
     });


     Route::get('contact/{id}', function($id){
         return Contact::findOrFail($id);
     });


     Route::post('contact/store', function(Request $request){
         return Contact::create(['name' => $request->input(['name'])],['email' => $request->input(['email'])],['phone' => $request->input(['phone'])]);
     });

     Route::patch('contact/{id}', function(Request $request. $id){
         return Contact::findOrFail($id)->update(['name' => $request->input(['name'])],['email' => $request->input(['email'])],['phone' => $request->input(['phone'])]);
     });

     Route::delete('contact/{id}', function($id){
         return Contact::destroy($id);
     });*/


});




Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
