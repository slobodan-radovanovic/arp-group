<?php

namespace App;


use Illuminate\Database\Eloquent\Model;
use App\Equipment;

class Models extends Model
{
    public $table = 'equipment_model';
    /*protected $primaryKey = 'equipment_model_id';*/


    public function getCodeAttribute()
    {
        return "{$this->equipment_model_name} - {$this->equipment_code}";
    }

    public function equipment()
    {
        return $this->hasOne(Equipment::class, 'equipment_model_id', 'equipment_model_id');
    }

}
