<?php

namespace App;


use Illuminate\Database\Eloquent\Model;

class Wares extends Model
{
    protected $primaryKey = 'wares_id';

    public function warehouse()
    {
        return $this->belongsTo(Warehouse::class, 'wares_id', 'wares_id');
    }

    public function externalwarehouse()
    {
        return $this->belongsTo(ExternalWarehouse::class, 'wares_id', 'wares_id');
    }
}
