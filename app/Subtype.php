<?php

namespace App;


use Illuminate\Database\Eloquent\Model;

class Subtype extends Model
{
    public $table = 'equipment_subtype';
    protected $primaryKey = 'equipment_subtype_id';

    public function equipment()
    {
        return $this->hasOne(Equipment::class, 'equipment_subtype_id', 'equipment_subtype_id');
    }

}
