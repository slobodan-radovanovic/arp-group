<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $primaryKey = 'order_id';

    public $table = 'orders';

    protected $fillable = ['order_id', 'ordinal_number', 'order_number', 'order_type', 'order_status', 'buyer_id', 'treaty_id', 'buyer', 'area_code', 'city', 'region', 'township', 'address', 'address_number', 'floor', 'apartment', 'phone_number', 'mobile_number', 'service_type', 'private_vehicle', 'service_home', 'note', 'opened_at', 'started_at', 'scheduled_at', 'created_at', 'completed_at', 'updated_at'];

    public function equipment()
    {
        return $this->hasMany(Equipment::class, 'order_number');
    }

    public function technicians()
    {
        return $this->belongsToMany(Technician::class, 'order_technician', 'ordinal_number', 'technician_id');
    }

    public function service()
    {
        return $this->belongsTo(Service::class, 'service_type', 'service_select');
    }


}
