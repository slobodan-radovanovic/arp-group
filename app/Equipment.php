<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Models;
use App\Subtype;
use App\Type;

class Equipment extends Model
{
    protected $primaryKey = 'equipment_id';
    protected $appends = ['equipment_technician_name'];

    public function getEquipmentTechnicianNameAttribute()
    {
        return "{$this->equipment_status} - {$this->technician()->technician_name}";
    }



    public function order()
    {
        return $this->belongsTo(Order::class, 'equipment_order_number', 'order_number');
    }

    public function dischargedequipment()
    {
        return $this->hasOne(DischargedEquipment::class, 'equipment_id', 'equipment_id');

    }

    public function type()
    {
        return $this->hasOne(Type::class, 'equipment_type_id', 'equipment_type_id');
    }

    public function subtype()
    {
        return $this->hasOne(Subtype::class, 'equipment_subtype_id', 'equipment_subtype_id');
    }

    public function models()
    {
        return $this->hasOne(Models::class, 'equipment_model_id', 'equipment_model_id');
    }

    public function technician()
    {
        return $this->hasOne(Technician::class, 'technician_id', 'equipment_technician');
    }

}
