<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Technician extends Model
{
    protected $primaryKey = 'technician_id';

    public function orders()
    {
        return $this->belongsToMany(Order::class, 'order_technician', 'technician_id', 'ordinal_number');
    }

    public function discharged_equipment()
    {
        return $this->hasMany(DischargedEquipment::class, 'equipment_technician');
    }

    public function equipment()
    {
        return $this->hasOne(Equipment::class, 'equipment_technician', 'technician_id');
    }
}
