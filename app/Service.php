<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Service extends Model
{
    protected $primaryKey = 'service_id';

    public function order()
    {
        return $this->hasMany(Order::class, 'service_select', 'service_type');
    }
}
