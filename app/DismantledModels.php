<?php

namespace App;


use Illuminate\Database\Eloquent\Model;

class DismantledModels extends Model
{
    public $table = 'dismantled_eq_model';
    protected $appends = ['code_model'];
    //   protected $primaryKey = 'dismantled_eq_model_id';

    public function getCodeModelAttribute()
    {
        return "{$this->equipment_code} - {$this->dismantled_eq_model_name}";
    }

}
