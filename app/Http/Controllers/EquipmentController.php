<?php

namespace App\Http\Controllers;

use App\Equipment;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use stdClass;
use App\Technician;
use App\Subtype;
use App\Models;
use App\DismantledModels;
use App\DismantledSubtype;
use Validator;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use App\DischargedEquipment;


class EquipmentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $query1 = DB::table('equipment')
            ->join('equipment_subtype', 'equipment.equipment_subtype_id', '=', 'equipment_subtype.equipment_subtype_id')
            ->join('equipment_model', 'equipment.equipment_model_id', '=', 'equipment_model.equipment_model_id')
            ->select('equipment.equipment_type_id', 'equipment.equipment_subtype_id', 'equipment.equipment_model_id', 'equipment.equipment_id', 'equipment.equipment_status', 'equipment_model.equipment_code', 'equipment.equipment_serial1', 'equipment.equipment_serial2', 'equipment.equipment_serial3', 'equipment_model.equipment_model_name', 'equipment_subtype.equipment_subtype_name')
            ->where('equipment_status', '!=', 'Kod kupca')
            ->where('equipment_status', '!=', 'Izbrisan')
            ->where('equipment_status', '!=', 'SBB magacin')
            ->get();/*$equipment1 = datatables()->of($query1)->toJson()*/;

        if (request()->ajax()) {
            return datatables()->of($query1)->toJson();
        }

        $unused_equimpment = DB::table('equipment')
            ->join('equipment_subtype', 'equipment.equipment_subtype_id', '=', 'equipment_subtype.equipment_subtype_id')
            ->select('equipment.equipment_subtype_id', 'equipment_subtype.equipment_subtype_name')
            ->distinct(/*'equipment.equipment_subtype_id'*/)
            ->pluck('equipment_subtype.equipment_subtype_name', 'equipment_subtype.equipment_subtype_id');

        $equipment_count_tehnician = createKeyArray(DB::table('equipment')
            ->select(DB::raw('count(*) as equipment_count, equipment_subtype_id'))
            ->where('equipment_status', '=', 'Kod tehničara')
            ->groupBy('equipment_subtype_id')
            ->pluck('count(*) as equipment_count', 'equipment_subtype_id'));

        $equipment_count_warehouse = createKeyArray(DB::table('equipment')
            ->select(DB::raw('count(*) as equipment_count, equipment_subtype_id'))
            ->where('equipment_status', '=', 'U magacinu')
            ->groupBy('equipment_subtype_id')
            ->pluck('count(*) as equipment_count', 'equipment_subtype_id'));

        $type = createKeyArray(DB::table('equipment_type')->pluck('equipment_type_name', 'equipment_type_id'));
        $subtype = createKeyArray(DB::table('equipment_subtype')->pluck('equipment_subtype_name', 'equipment_subtype_id'));

        //$model_all = DB::table('equipment_model')->groupBy('equipment_subtype_id')->get();

        //$model_by_subtypes = [];

        /*foreach($model_all as $model){
            dd($model->pluck('equipment_model_name', 'equipment_model_id'));
            $model_by_subtypes[$model->equipment_subtype_id] = [$model->equipment_model_id => $model->equipment_model_name ];
        };

        dd($model_by_subtypes);*/

        $model = createKeyArray(DB::table('equipment_model')->pluck('equipment_model_name', 'equipment_model_id'));

        // dd($model);

        $equipment_for_select = DB::table('equipment_subtype')
            ->join('equipment_type', 'equipment_subtype.equipment_type_id', '=', 'equipment_type.equipment_type_id')
            ->leftJoin('equipment_model', 'equipment_subtype.equipment_subtype_id', '=', 'equipment_model.equipment_subtype_id')
            ->select('equipment_subtype.equipment_type_id', 'equipment_type.equipment_type_name', 'equipment_subtype.equipment_subtype_id', 'equipment_subtype.equipment_subtype_name', 'equipment_model.equipment_model_id', 'equipment_model.equipment_model_name')->get()/*->toJson()*/
        ;

        $data = [
            'unused_equimpment' => $unused_equimpment,
            'equipment_count_tehnician' => $equipment_count_tehnician,
            'equipment_count_warehouse' => $equipment_count_warehouse,
            'type' => $type,
            'subtype' => $subtype,
            'model' => $model,
            'equipment_for_select' => $equipment_for_select
        ];

        return view('equipment.index', compact('data'));
    }

    public function index_old()
    {
        $equipment1 = Equipment::where('equipment_status', '!=', 'Kod kupca')
            ->where('equipment_status', '!=', 'Izbrisan')->get();  /*DB::table('equipment')->where('equipment_status', '!=', 'Kod kupca')->get();*/
        $equipment2 = Equipment::where('equipment_status', '=', 'U magacinu')->get();
        $equipment3 = Equipment::where('equipment_status', '=', 'Kod tehničara')->get();

        $unused_equimpment = DB::table('equipment')
            ->join('equipment_subtype', 'equipment.equipment_subtype_id', '=', 'equipment_subtype.equipment_subtype_id')
            ->select('equipment.equipment_subtype_id', 'equipment_subtype.equipment_subtype_name')
            ->distinct(/*'equipment.equipment_subtype_id'*/)
            ->pluck('equipment_subtype.equipment_subtype_name', 'equipment_subtype.equipment_subtype_id');
//        dd($unused_equimpment);
        $equipment_count_tehnician = createKeyArray(DB::table('equipment')
            ->select(DB::raw('count(*) as equipment_count, equipment_subtype_id'))
            ->where('equipment_status', '=', 'Kod tehničara')
            ->groupBy('equipment_subtype_id')
            ->pluck('count(*) as equipment_count', 'equipment_subtype_id'));
        /*dd($equipment_count_tehnician);*/

        $equipment_count_warehouse = createKeyArray(DB::table('equipment')
            ->select(DB::raw('count(*) as equipment_count, equipment_subtype_id'))
            ->where('equipment_status', '=', 'U magacinu')
            ->groupBy('equipment_subtype_id')
            ->pluck('count(*) as equipment_count', 'equipment_subtype_id'));

        /*dd($equipment_count_warehouse);*/

        $type = createKeyArray(DB::table('equipment_type')->pluck('equipment_type_name', 'equipment_type_id'));

        $subtype = createKeyArray(DB::table('equipment_subtype')->pluck('equipment_subtype_name', 'equipment_subtype_id'));

        $model = createKeyArray(DB::table('equipment_model')->pluck('equipment_model_name', 'equipment_model_id'));

        $data = [
            'equipment1' => $equipment1,
            'equipment2' => $equipment2,
            'equipment3' => $equipment3,
            'unused_equimpment' => $unused_equimpment,
            'equipment_count_tehnician' => $equipment_count_tehnician,
            'equipment_count_warehouse' => $equipment_count_warehouse,
            'type' => $type,
            'subtype' => $subtype,
            'model' => $model
        ];

        /*dd($data);*/

        return view('equipment.index', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function discharge()
    {

        $active_technicians = createKeyArray(Technician::where('active', 1)->pluck('technician_name', 'technician_id'), 'technician_');

        return view('equipment.discharge', compact('active_technicians'));
    }

    public function create()
    {
        $type = createKeyArray(DB::table('equipment_type')->pluck('equipment_type_name', 'equipment_type_id'));

        $subtype = createKeyArray(DB::table('equipment_subtype')->orderBy('created_at')->pluck('equipment_subtype_name', 'equipment_subtype_id'));

        $model = createKeyArray(Models::all()->pluck('code', 'equipment_model_id'));


        /* $model = createKeyArray(DB::table('equipment_model')->pluck('equipment_model_name' , 'equipment_model_id'));*/

        $equipment_code = createKeyArray(DB::table('equipment_model')->pluck('equipment_code', 'equipment_model_id'));

        // $equipment = DB::table('equipment')->orderByDesc('created_at')->limit(10)->get();
        $equipment = DB::table('equipment')->orderByDesc('equipment_id')->latest()->take(10)->get();

        $equipment_for_select = DB::table('equipment_subtype')
            ->join('equipment_type', 'equipment_subtype.equipment_type_id', '=', 'equipment_type.equipment_type_id')
            ->leftJoin('equipment_model', 'equipment_subtype.equipment_subtype_id', '=', 'equipment_model.equipment_subtype_id')
            ->select('equipment_subtype.equipment_type_id', 'equipment_type.equipment_type_name', 'equipment_subtype.equipment_subtype_id', 'equipment_subtype.equipment_subtype_name', 'equipment_model.equipment_model_id', 'equipment_model.equipment_model_name')->get()/*->toJson()*/
        ;

        //dd($equipment_for_select);
        $data = [
            'type' => $type,
            'subtype' => $subtype,
            'model' => $model,
            'equipment' => $equipment,
            'equipment_code' => $equipment_code,
            'equipment_for_select' => $equipment_for_select
        ];

        /*dd($equipment);*/
        return view('equipment.create', compact('data'));
    }

    public function assigned()
    {
        $technicians = createKeyArray(Technician::pluck('technician_name', 'technician_id'), 'technician_');

        $equipments = [];

        $technician = null;

        $date_period = null;


        return view('equipment.assigned', compact('equipments', 'technicians', 'technician', 'date_period'));
    }

    public function assigned_post(Request $request)
    {

        /* Validator::make($request->all(), [
             'technician' => 'required',
             'dismantled_date_filter' => 'required',
         ], [
             'technician.required' => 'Polje "Tehničar" je obavezno, izaberite tehničara',
             'dismantled_date_filter.required' => 'Niste izabrali dan - period',
         ])->validate();*/


        $subtype = createKeyArray(DB::table('equipment_subtype')->pluck('equipment_subtype_name', 'equipment_subtype_id'));

        $model = createKeyArray(DB::table('equipment_model')->pluck('equipment_model_name', 'equipment_model_id'));

        $all_serials = createAllSerialsArray(DB::table('equipment')->select('equipment_id', 'equipment_serial1', 'equipment_serial2', 'equipment_serial3')
            ->where('equipment_status', 'U magacinu')
            ->get());

        $technicians = createKeyArray(Technician::pluck('technician_name', 'technician_id'), 'technician_');


        $dismantled_date_filter = $request->input('dismantled_date_filter');
        $date_period = str_replace("/", "-", $dismantled_date_filter);
        $exploded_date_filter = explode(' - ', $dismantled_date_filter);
        $exploded_start_date = explode('/', $exploded_date_filter[0]);
        $exploded_end_date = explode('/', $exploded_date_filter[1]);
        $start_date = $exploded_start_date['2'] . '-'
            . $exploded_start_date['1'] . '-'
            . $exploded_start_date['0'] . ' 00:00:01';
        $end_date = $exploded_end_date['2'] . '-'
            . $exploded_end_date['1'] . '-'
            . $exploded_end_date['0'] . ' 23:59:59';


        $technician = $request->input('technician');

        if ($technician != null) {
            $technician_id = explode('_', $request->input('technician'))[1];
            $equipments = DB::table('equipment')
                ->where('equipment_status', '!=', 'Izbrisan')
                ->where('equipment_status', '!=', 'SBB magacin')
                ->where('equipment_assign_date', '!=', null)
                ->where([
                    //  ['equipment_status', '!=', 'Izbrisan'],
                    ['equipment_assign_date', '!=', null],
                    ['equipment_assign_date', '>', $start_date],
                    ['equipment_assign_date', '<', $end_date],
                    ['equipment_technician', '=', $technician_id]
                ])->get();
        } else {
            $equipments = DB::table('equipment')
                ->where('equipment_status', '!=', 'Izbrisan')
                ->where('equipment_status', '!=', 'SBB magacin')
                ->where('equipment_assign_date', '!=', null)
                ->where([
                    // ['equipment_status', '!=', 'Izbrisan'],
                    ['equipment_assign_date', '!=', null],
                    ['equipment_assign_date', '>', $start_date],
                    ['equipment_assign_date', '<', $end_date],
                ])->get();
        }

        $data = [
            'subtype' => $subtype,
            'model' => $model,
            'all_serials' => $all_serials,
        ];


        return view('equipment.assigned', compact('data', 'equipments', 'technician', 'technicians', 'dismantled_date_filter', 'date_period'));

        //return view('pages.techniciansreport', compact('techniciansreport', 'technicians', 'sum', 'dismantled_date_filter', 'technician', ' ', 'date_period'));
    }

    public function assignment()
    {

        $subtype = createKeyArray(DB::table('equipment_subtype')->pluck('equipment_subtype_name', 'equipment_subtype_id'));

        $model = createKeyArray(DB::table('equipment_model')->pluck('equipment_model_name', 'equipment_model_id'));


        $subtype_for_select = createKeyArray(DB::table('equipment_subtype')
            ->rightJoin('equipment', 'equipment_subtype.equipment_subtype_id', '=', 'equipment.equipment_subtype_id')
            ->where('equipment_status', 'U magacinu')
            ->pluck('equipment_subtype.equipment_subtype_name', 'equipment_subtype.equipment_subtype_id'));

        $model_for_select = createKeyArray(DB::table('equipment_model')
            ->rightJoin('equipment', 'equipment_model.equipment_model_id', '=', 'equipment.equipment_model_id')
            ->where('equipment_status', 'U magacinu')
            ->pluck('equipment_model.equipment_model_name', 'equipment_model.equipment_model_id'));


        $all_serials = createAllSerialsArray(DB::table('equipment')->select('equipment_id', 'equipment_serial1', 'equipment_serial2', 'equipment_serial3')
            ->where('equipment_status', 'U magacinu')
            ->get());

        //dd($all_serials);

        $active_technicians = createKeyArray(Technician::where('active', 1)->pluck('technician_name', 'technician_id'), 'technician_');

        //dd($active_technicians);

        $technicians = createKeyArray(Technician::pluck('technician_name', 'technician_id'), 'technician_');

        $equipment = DB::table('equipment')
            ->join('equipment_model', 'equipment.equipment_model_id', '=', 'equipment_model.equipment_model_id')
            ->where('equipment_status', '!=', 'Izbrisan')
            ->where('equipment_status', '!=', 'SBB magacin')
            ->where('equipment_assign_date', '!=', null)
            ->select('equipment.*', 'equipment_model.equipment_code')
            ->orderByDesc('equipment_assign_date')->take(5)->get();

        $equipment_for_select = DB::table('equipment')
            ->join('equipment_subtype', 'equipment.equipment_subtype_id', '=', 'equipment_subtype.equipment_subtype_id')
            ->join('equipment_model', 'equipment.equipment_model_id', '=', 'equipment_model.equipment_model_id')
            ->select('equipment.equipment_id', 'equipment_subtype.equipment_subtype_id', 'equipment_subtype.equipment_subtype_name', 'equipment_model.equipment_model_id', 'equipment_model.equipment_model_name', 'equipment.equipment_serial1', 'equipment.equipment_serial2', 'equipment.equipment_serial3')->where('equipment.equipment_status', 'U magacinu')->get();

        //dd($equipment_for_select);

//        dd($equipment);
        $data = [
            'equipment' => $equipment,
            'subtype' => $subtype,
            'model' => $model,
            'all_serials' => $all_serials,
            'technicians' => $technicians,
            'active_technicians' => $active_technicians,
            'equipment_for_select' => $equipment_for_select,
            'subtype_for_select' => $subtype_for_select,
            'model_for_select' => $model_for_select,
        ];

        return view('equipment.assignment', compact('data'));
    }

    public function add()
    {
        $type = createKeyArray(DB::table('equipment_type')->pluck('equipment_type_name', 'equipment_type_id'));

        $subtype = createKeyArray(DB::table('equipment_subtype')->pluck('equipment_subtype_name', 'equipment_subtype_id'));

        $dismantled_subtype = createKeyArray(DB::table('dismantled_eq_subtype')->pluck('dismantled_eq_subtype_name', 'dismantled_eq_subtype_id'));

        $equipment = [
            'type' => $type,
            'subtype' => $subtype,
            'dismantled_subtype' => $dismantled_subtype,
        ];

        return view('equipment.add', compact('equipment'));
    }

    public function store_subtype(Request $request)
    {

        Validator::make($request->all(), [
            'equipment_type_id' => 'required',
            'equipment_subtype_name' => 'required',

        ], [
            'equipment_type_id.required' => 'Polje "Vrsta opreme" je obavezno',
            'equipment_subtype_name.required' => 'Polje "Tip opreme" je obavezno',
        ])->validate();


        $last_subtype_id = DB::table('equipment_subtype')->orderBy('created_at', 'desc')->first();
        //ako ne postoji ni jedan unos, prethodni je psotavljen da bude 0, i onda bi prvi bio subtype_1
        if (gettype($last_subtype_id) != 'object') {
            $last_subtype_id = new stdClass(); //kreiranje praznog objekta
            $last_subtype_id->equipment_subtype_id = 'subtype_0';
        }

        $exploded_subtype_id = explode('_', $last_subtype_id->equipment_subtype_id);
        $new_subtype_id = $exploded_subtype_id['0'] . '_' . ($exploded_subtype_id['1'] + 1);
        DB::table('equipment_subtype')->insert(
            ['equipment_subtype_id' => $new_subtype_id,
                'equipment_type_id' => $request->input('equipment_type_id'),
                'equipment_subtype_name' => $request->input('equipment_subtype_name'),
                'created_at' => date('Y-m-d H:i:s')
            ]);
        return redirect('/equipment/add')->with('success', 'Uspešno dodat novi tip opreme "' . $request->input('equipment_subtype_name') . '"');
    }

    public function store_subtype_old(Request $request)
    {

        Validator::make($request->all(), [
            'equipment_type_id' => 'required',
            'equipment_subtype_name' => 'required',
        ], [
            'equipment_type_id.required' => 'Polje "Vrsta opreme" je obavezno',
            'equipment_subtype_name.required' => 'Polje "Tip opreme" je obavezno',
        ])->validate();


        $last_subtype_id = DB::table('equipment_subtype')->orderBy('created_at', 'desc')->first();
        //ako ne postoji ni jedan unos, prethodni je psotavljen da bude 0, i onda bi prvi bio subtype_1
        if (gettype($last_subtype_id) != 'object') {
            $last_subtype_id = new stdClass(); //kreiranje praznog objekta
            $last_subtype_id->equipment_subtype_id = 'subtype_0';
        }
        $exploded_subtype_id = explode('_', $last_subtype_id->equipment_subtype_id);
        $new_subtype_id = $exploded_subtype_id['0'] . '_' . ($exploded_subtype_id['1'] + 1);

        DB::table('equipment_subtype')->insert(
            ['equipment_subtype_id' => $new_subtype_id,
                'equipment_type_id' => $request->input('equipment_type_id'),
                'equipment_subtype_name' => $request->input('equipment_subtype_name'),
                'created_at' => date('Y-m-d H:i:s')
            ]);
        return redirect('/equipment/add');
    }

    public function store_model(Request $request)
    {

        Validator::make($request->all(), [
            'equipment_subtype_id' => 'required',
            'equipment_model_name' => 'required',
            'equipment_code' => 'required|unique:equipment_model,equipment_code',
        ], [
            'equipment_subtype_id.required' => 'Polje "Tip opreme" je obavezno',
            'equipment_model_name.required' => 'Polje "Model opreme" je obavezno',
            'equipment_code.required' => 'Polje "Kôd opreme" je obavezno',
            'equipment_code.unique' => 'Već postoji model opreme sa kôdom "' . $request->input('equipment_code') . '"',
        ])->validate();


        $last_model_id = DB::table('equipment_model')->orderBy('created_at', 'desc')->first();
        //ako ne postoji ni jedan unos, prethodni je psotavljen da bude 0, i onda bi prvi bio model_1
        if (gettype($last_model_id) != 'object') {
            $last_model_id = new stdClass(); //kreiranje praznog objekta
            $last_model_id->equipment_model_id = 'model_0';
        }
        $exploded_model_id = explode('_', $last_model_id->equipment_model_id);
        $new_model_id = $exploded_model_id['0'] . '_' . ($exploded_model_id['1'] + 1);
        DB::table('equipment_model')->insert(
            ['equipment_model_id' => $new_model_id,
                'equipment_subtype_id' => $request->input('equipment_subtype_id'),
                'equipment_model_name' => $request->input('equipment_model_name'),
                'equipment_code' => $request->input('equipment_code'),
                'created_at' => date('Y-m-d H:i:s')
            ]);
        return redirect('/equipment/add')->with('success', 'Uspešno dodat novi model opreme "' . $request->input('equipment_model_name') . '"');
    }

    public function store_dismantled_subtype(Request $request)
    {

        Validator::make($request->all(), [
            'dismantled_eq_subtype_name' => 'required',
        ], [
            'dismantled_eq_subtype_name.required' => 'Polje "Tip demontirane opreme" je obavezno',
        ])->validate();


        $last_dismantled_eq_subtype_id = DB::table('dismantled_eq_subtype')->orderBy('created_at', 'desc')->first();
        //ako ne postoji ni jedan unos, prethodni je psotavljen da bude 0, i onda bi prvi bio subtype_1
        if (gettype($last_dismantled_eq_subtype_id) != 'object') {
            $last_dismantled_eq_subtype_id = new stdClass(); //kreiranje praznog objekta
            $last_dismantled_eq_subtype_id->dismantled_eq_subtype_id = 'subtype_0';
        }

        $exploded_dismantled_eq_subtype_id = explode('_', $last_dismantled_eq_subtype_id->dismantled_eq_subtype_id);
        $new_dismantled_eq_subtype_id = $exploded_dismantled_eq_subtype_id['0'] . '_' . ($exploded_dismantled_eq_subtype_id['1'] + 1);
        DB::table('dismantled_eq_subtype')->insert(
            ['dismantled_eq_subtype_id' => $new_dismantled_eq_subtype_id,
                'dismantled_eq_subtype_name' => $request->input('dismantled_eq_subtype_name'),
                'created_at' => date('Y-m-d H:i:s')
            ]);
        return redirect('/equipment/add')->with('success', 'Uspešno dodat novi tip za demontiranu opremu: "' . $request->input('dismantled_eq_subtype_name') . '"');
    }

    public function store_dismantled_model(Request $request)
    {

        Validator::make($request->all(), [
            'dismantled_eq_model_name' => 'required',
            'dismantled_eq_subtype_id' => 'required',
            'equipment_code' => 'required|unique:dismantled_eq_model,equipment_code',
        ], [
            'dismantled_eq_model_name.required' => 'Polje "Model demontirane opreme" je obavezno',
            'dismantled_eq_subtype_id.required' => 'Polje "Tip demontirane opreme" je obavezno',
            'equipment_code.required' => 'Polje "Kôd demontirane opreme" je obavezno',
            'equipment_code.unique' => 'Već postoji model opreme sa kôdom "' . $request->input('equipment_code') . '"',
        ])->validate();


        $last_dismantled_eq_model_id = DB::table('dismantled_eq_model')->orderBy('created_at', 'desc')->first();
        //ako ne postoji ni jedan unos, prethodni je psotavljen da bude 0, i onda bi prvi bio model_1
        if (gettype($last_dismantled_eq_model_id) != 'object') {
            $last_dismantled_eq_model_id = new stdClass(); //kreiranje praznog objekta
            $last_dismantled_eq_model_id->dismantled_eq_model_id = 'model_0';
        }

        $exploded_dismantled_eq_model_id = explode('_', $last_dismantled_eq_model_id->dismantled_eq_model_id);
        $new_dismantled_eq_model_id = $exploded_dismantled_eq_model_id['0'] . '_' . ($exploded_dismantled_eq_model_id['1'] + 1);
        DB::table('dismantled_eq_model')->insert(
            ['dismantled_eq_model_id' => $new_dismantled_eq_model_id,
                'dismantled_eq_model_name' => $request->input('dismantled_eq_model_name'),
                'dismantled_eq_subtype_id' => $request->input('dismantled_eq_subtype_id'),
                'equipment_code' => $request->input('equipment_code'),
                'created_at' => date('Y-m-d H:i:s')
            ]);
        return redirect('/equipment/add')->with('success', 'Uspešno dodat novi model za demontiranu opremu: "' . $request->input('equipment_model_name') . '"');
    }

    public function dismantled(Request $request)
    {
        if (empty($request->input('dismantled_date_filter')) and empty($request->input('dismantled_discharged_date_filter'))) {

            $removed_equipment = DB::table('removed_equipment')
                ->join('dismantled_eq_subtype', 'removed_equipment.removed_equipment_subtype', '=', 'dismantled_eq_subtype.dismantled_eq_subtype_id')
                ->join('dismantled_eq_model', 'removed_equipment.removed_equipment_model', '=', 'dismantled_eq_model.dismantled_eq_model_id')
                ->select('removed_equipment.*', 'dismantled_eq_subtype.dismantled_eq_subtype_name', 'dismantled_eq_model.dismantled_eq_model_name', 'dismantled_eq_model.equipment_code')
                ->where('removed_equipment.removed_equipment_status', 'magacin')->get();

            $removed_equipment_discharged = DB::table('removed_equipment')
                ->join('dismantled_eq_subtype', 'removed_equipment.removed_equipment_subtype', '=', 'dismantled_eq_subtype.dismantled_eq_subtype_id')
                ->join('dismantled_eq_model', 'removed_equipment.removed_equipment_model', '=', 'dismantled_eq_model.dismantled_eq_model_id')
                ->select('removed_equipment.*', 'dismantled_eq_subtype.dismantled_eq_subtype_name', 'dismantled_eq_model.dismantled_eq_model_name', 'dismantled_eq_model.equipment_code')
                ->where('removed_equipment_status', 'razduzeno')->get();

            return view('equipment.dismantled', compact('removed_equipment', 'removed_equipment_discharged'));

        } elseif ($request->input('dismantled_date_filter')) {


            $dismantled_date_filter = $request->input('dismantled_date_filter');
            $exploded_date_filter = explode(' - ', $request->input('dismantled_date_filter'));
            $exploded_start_date = explode('/', $exploded_date_filter[0]);
            $exploded_end_date = explode('/', $exploded_date_filter[1]);
            $start_date = $exploded_start_date['2'] . '-'
                . $exploded_start_date['1'] . '-'
                . $exploded_start_date['0'] . ' 00:00:01';
            $end_date = $exploded_end_date['2'] . '-'
                . $exploded_end_date['1'] . '-'
                . $exploded_end_date['0'] . ' 23:59:59';

            $removed_equipment = DB::table('removed_equipment')
                ->join('dismantled_eq_subtype', 'removed_equipment.removed_equipment_subtype', '=', 'dismantled_eq_subtype.dismantled_eq_subtype_id')
                ->join('dismantled_eq_model', 'removed_equipment.removed_equipment_model', '=', 'dismantled_eq_model.dismantled_eq_model_id')
                ->select('removed_equipment.*', 'dismantled_eq_subtype.dismantled_eq_subtype_name', 'dismantled_eq_model.dismantled_eq_model_name', 'dismantled_eq_model.equipment_code')
                ->where([
                    ['removed_equipment.created_at', '>', $start_date],
                    ['removed_equipment.created_at', '<', $end_date],
                    ['removed_equipment.removed_equipment_status', '=', 'magacin']
                ])->get();

            $removed_equipment_discharged = DB::table('removed_equipment')
                ->join('dismantled_eq_subtype', 'removed_equipment.removed_equipment_subtype', '=', 'dismantled_eq_subtype.dismantled_eq_subtype_id')
                ->join('dismantled_eq_model', 'removed_equipment.removed_equipment_model', '=', 'dismantled_eq_model.dismantled_eq_model_id')
                ->select('removed_equipment.*', 'dismantled_eq_subtype.dismantled_eq_subtype_name', 'dismantled_eq_model.dismantled_eq_model_name', 'dismantled_eq_model.equipment_code')
                ->where('removed_equipment.removed_equipment_status', 'razduzeno')->get();

            //return redirect('/eqipment/dismantled')->with('dismantled_date_filter',);

            return view('equipment.dismantled', compact('removed_equipment', 'removed_equipment_discharged', 'dismantled_date_filter'));

        } elseif ($request->input('dismantled_discharged_date_filter')) {

            $dismantled_discharged_date_filter = $request->input('dismantled_discharged_date_filter');

            $exploded_date_filter = explode(' - ', $request->input('dismantled_discharged_date_filter'));
            $exploded_start_date = explode('/', $exploded_date_filter[0]);
            $exploded_end_date = explode('/', $exploded_date_filter[1]);
            $start_date = $exploded_start_date['2'] . '-'
                . $exploded_start_date['1'] . '-'
                . $exploded_start_date['0'] . ' 00:00:01';
            $end_date = $exploded_end_date['2'] . '-'
                . $exploded_end_date['1'] . '-'
                . $exploded_end_date['0'] . ' 23:59:59';

            $removed_equipment_discharged = DB::table('removed_equipment')
                ->join('dismantled_eq_subtype', 'removed_equipment.removed_equipment_subtype', '=', 'dismantled_eq_subtype.dismantled_eq_subtype_id')
                ->join('dismantled_eq_model', 'removed_equipment.removed_equipment_model', '=', 'dismantled_eq_model.dismantled_eq_model_id')
                ->select('removed_equipment.*', 'dismantled_eq_subtype.dismantled_eq_subtype_name', 'dismantled_eq_model.dismantled_eq_model_name', 'dismantled_eq_model.equipment_code')
                ->where([
                    ['removed_equipment.updated_at', '>', $start_date],
                    ['removed_equipment.updated_at', '<', $end_date],
                    ['removed_equipment.removed_equipment_status', '=', 'razduzeno']
                ])->get();

            $removed_equipment = DB::table('removed_equipment')
                ->join('dismantled_eq_subtype', 'removed_equipment.removed_equipment_subtype', '=', 'dismantled_eq_subtype.dismantled_eq_subtype_id')
                ->join('dismantled_eq_model', 'removed_equipment.removed_equipment_model', '=', 'dismantled_eq_model.dismantled_eq_model_id')
                ->select('removed_equipment.*', 'dismantled_eq_subtype.dismantled_eq_subtype_name', 'dismantled_eq_model.dismantled_eq_model_name', 'dismantled_eq_model.equipment_code')
                ->where('removed_equipment.removed_equipment_status', 'magacin')->get();

            // dd($dismantled_discharged_date_filter);

            return view('equipment.dismantled', compact('removed_equipment', 'removed_equipment_discharged', 'dismantled_discharged_date_filter'));
        }

    }

    public function dismantled_discharge(Request $request)
    {

        foreach ($request->input('razduzivanje') as $removed_equipment_id) {

            DB::table('removed_equipment')
                ->where('removed_equipment_id', $removed_equipment_id)
                ->update(['removed_equipment_status' => 'razduzeno', 'updated_at' => date('Y-m-d H:i:s')]);
        };

        return redirect('/equipment/dismantled')->with('razduzeno', true)->with('success', 'Uspešno razduzena oprema iz magacina');

        // return view('equipment.dismantled', compact('removed_equipment', 'dismantled_date_filter'))->with('razduzeno', true);

    }

    public function dispatch_note(Request $request)
    {


        if (empty($request->input('dispatch_note'))) {

            $equipments = null;
            $print_date = null;
            $dispatch_note = null;
            return view('equipment.dispatch_note', compact('equipments', 'print_date', 'dispatch_note'));

        } else {

            $subtype = createKeyArray(DB::table('equipment_subtype')->pluck('equipment_subtype_name', 'equipment_subtype_id'));

            $model = createKeyArray(DB::table('equipment_model')->pluck('equipment_model_name', 'equipment_model_id'));

            $equipment_code = createKeyArray(DB::table('equipment_model')->pluck('equipment_code', 'equipment_model_id'));

            $data = [
                'subtype' => $subtype,
                'model' => $model,
                'equipment_code' => $equipment_code
            ];


            $dispatch_note = $request->input('dispatch_note');

            $print_date = $request->input('print_date');

            $equipments = Equipment::where('equipment_dispatch_note', '=', $dispatch_note)->get();

            return view('equipment.dispatch_note', compact('equipments', 'dispatch_note', 'data', 'print_date'));
        }

    }

    public function installed_equipment(Request $request)
    {
        $installed_equipment = DB::table('equipment')
            ->join('equipment_subtype', 'equipment.equipment_subtype_id', '=', 'equipment_subtype.equipment_subtype_id')
            ->leftJoin('equipment_model', 'equipment.equipment_model_id', '=', 'equipment_model.equipment_model_id')
            ->join('orders', 'equipment.equipment_ordinal_number', '=', 'orders.ordinal_number')
            ->join('technicians', 'equipment.equipment_technician', '=', 'technicians.technician_id')
            ->select('equipment.*', 'equipment_subtype.equipment_subtype_name', 'equipment_model.equipment_model_name', 'orders.order_number', 'technicians.technician_name', 'orders.completed_at')
            ->where('equipment.equipment_status', '=', 'Kod kupca')->get();

//        dd($installed_equipment);
        return view('equipment.installed_equipment', compact('installed_equipment'));

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function equipment_test()
    {

        $all_serials = DB::table('equipment')
            ->where('equipment_subtype_id', '=', 'subtype_7')
            ->get();
        //->update(['votes' => 1]);

        dd($all_serials);

    }


    public function store(Request $request)
    {
        //dd($request->input('equipment_serial1'));
        $all_serials = createAllSerialsArray(DB::table('equipment')->select('equipment_id', 'equipment_serial1', 'equipment_serial2', 'equipment_serial3')->where('equipment_status', '!=', 'SBB magacin')
            ->get());


        $uppcasearray = array_map('strtoupper', $all_serials);
        $uppcaseserial1 = strtoupper($request->input('equipment_serial1'));
        $uppcaseserial2 = strtoupper($request->input('equipment_serial2'));
        $uppcaseserial3 = strtoupper($request->input('equipment_serial3'));

        Validator::make($request->all(), [
            'equipment_type_id' => 'required',
            'equipment_subtype_id' => 'required',
            'equipment_model_id' => 'required',
            'equipment_serial1' => 'required',
            'created_at' => 'required',
        ], [
            'equipment_type_id.required' => 'Polje "Vrsta opreme" je obavezno',
            'equipment_subtype_id.required' => 'Polje "Tip opreme" je obavezno',
            'equipment_model_id.required' => 'Polje "Model opreme" je obavezno',
            'equipment_serial1.required' => 'Polje "Serijski broj 1" je obavezno',
            'created_at.required' => 'Niste izabrali "Datum i vreme unosa orpeme"',
        ])->validate();

        if (in_array($uppcaseserial1, $uppcasearray)) {

            /*$equipment = Equipment::where('equipment_serial1', $uppcaseserial1)
                ->orWhere('active', 1)
                ->orWhere('active', 1)
                ->get();*/

            Validator::make($request->all(), [
                'serial1' => 'required',
            ], [
                'serial1.required' => 'U bazi već postoji oprema sa serijskim brojem "' . $uppcaseserial1 . '" (Serijski broj 1)',
            ])->validate();

        } elseif (in_array($uppcaseserial2, $uppcasearray)) {
            Validator::make($request->all(), [
                'serial2' => 'required',
            ], [
                'serial2.required' => 'U bazi već postoji oprema sa serijskim brojem "' . $uppcaseserial2 . '" (Serijski broj 2)',
            ])->validate();
        } elseif (in_array($uppcaseserial3, $uppcasearray)) {
            Validator::make($request->all(), [
                'serial3' => 'required',
            ], [
                'serial3.required' => 'U bazi već postoji oprema sa serijskim brojem "' . $uppcaseserial3 . '" (Serijski broj 3)',
            ])->validate();
        }

        /*dd($request->all());*/
        $equipment = new Equipment;
        $equipment->equipment_type_id = $request->input('equipment_type_id');
        $equipment->equipment_subtype_id = $request->input('equipment_subtype_id');
        $equipment->equipment_model_id = $request->input('equipment_model_id');
        $equipment->equipment_serial1 = $request->input('equipment_serial1');
        $equipment->equipment_serial2 = $request->input('equipment_serial2');
        $equipment->equipment_serial3 = $request->input('equipment_serial3');
        $equipment->created_at = datetimeForDatabase($request->input('created_at'));
        $equipment->equipment_status = "U magacinu";
        $equipment->equipment_dispatch_note = str_replace(' ', '', $request->input('equipment_dispatch_note'));
        $equipment->equipment_added_by = Auth::user()->name;
        //dd($equipment);
        $equipment->save();


        $session_data = [
            $equipment_type_id = $request->input('equipment_type_id'),
            $equipment_subtype_id = $request->input('equipment_subtype_id'),
            $equipment_model_id = $request->input('equipment_model_id'),
            $created_at = $request->input('created_at'),
            $equipment_dispatch_note = $request->input('equipment_dispatch_note'),
            $equipment_code = $request->input('equipment_code')
        ];

        return redirect('/equipment/create')->with('session_data', $session_data)->with('success', 'Uspešno dodata nova oprema');
    }

    public function store_old(Request $request)
    {
        Validator::make($request->all(), [
            'equipment_code' => 'required',
            'equipment_type_id' => 'required',
            'equipment_subtype_id' => 'required',
            /*'equipment_model_id' => 'required',*/
            'equipment_serial1' => 'required',
        ], [
            'equipment_code.required' => 'Polje "Kôd opreme" je obavezno',
            'equipment_type_id.required' => 'Polje "Vrsta opreme" je obavezno',
            'equipment_subtype_id.required' => 'Polje "Tip opreme" je obavezno',
            /* 'equipment_model_id.required' => 'Polje "Model" je obavezno',*/
            'equipment_serial1.required' => 'Polje "Serijski broj 1" je obavezno',
        ])->validate();

        /*dd($request->all());*/
        $equipment = new Equipment;
        $equipment->equipment_code = $request->input('equipment_code');
        $equipment->equipment_type_id = $request->input('equipment_type_id');
        $equipment->equipment_subtype_id = $request->input('equipment_subtype_id');
        $equipment->equipment_model_id = $request->input('equipment_model_id');
        $equipment->equipment_serial1 = $request->input('equipment_serial1');
        $equipment->equipment_serial2 = $request->input('equipment_serial2');
        $equipment->equipment_serial3 = $request->input('equipment_serial3');
        $equipment->equipment_status = "U magacinu";

        $equipment->save();

        $session_data = [
            $equipment_type_id = $request->input('equipment_type_id'),
            $equipment_subtype_id = $request->input('equipment_subtype_id'),
            $equipment_model_id = $request->input('equipment_model_id'),
            $equipment_code = $request->input('equipment_code'),
        ];

        return redirect('/equipment/create')->with('session_data', $session_data);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit()
    {


        $subtype = Subtype::all();
        $model = Models::all();
        $dismantled_subtype = DismantledSubtype::all();
        $dismantled_model = DismantledModels::all();


        /*$subtype = DB::table('equipment_subtype')->get();
        $model = DB::table('equipment_model')->get();
        $dismantled_subtype = DB::table('dismantled_eq_subtype')->get();
        $dismantled_model = DB::table('dismantled_eq_model')->get();*/

        //$subtype = DB::table('equipment_subtype')->get();

        $data = [
            'model' => $model,
            'subtype' => $subtype,
            'dismantled_subtype' => $dismantled_subtype,
            'dismantled_model' => $dismantled_model,
        ];

        //dd($data);

        return view('equipment.edit', compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //dd($request->all());

        if ($id == "assign") {

            Validator::make($request->all(), [
                'equipment_subtype_id' => 'required',
                'equipment_model_id' => 'required',
                'equipment_serial' => 'required',
                'equipment_technician' => 'required',
            ], [
                'equipment_subtype_id.required' => 'Polje "Tip opreme" je obavezno',
                'equipment_model_id.required' => 'Polje "Model" je obavezno',
                'equipment_serial.required' => 'Polje "Serijski broj opreme" je obavezno',
                'equipment_technician.required' => 'Polje "Zaduzije se" je obavezno',
            ])->validate();

            $equipment_technician = $request->input('equipment_technician');
            $exploded_technician = explode('_', $equipment_technician);
            $technician_id = $exploded_technician[1];
            $exploded_serial = explode('_', $request->input('equipment_serial'));
            $equipment_id = $exploded_serial[1];
            $assigned_time = date('Y-m-d H:i:s');
            $equipment = Equipment::find($equipment_id);
            $equipment->equipment_technician = $technician_id;
            $equipment->equipment_assign_date = $assigned_time;
            $equipment->equipment_status = 'Kod tehničara';


            $equipment->save();
            return redirect('/equipment/assignment')->with('equipment_technician', $equipment_technician)->with('success', 'Oprema uspešno zadužena');

        } elseif ($id == "update") {

            $all_serials = createAllSerialsArray(DB::table('equipment')->select('equipment_id', 'equipment_serial1', 'equipment_serial2', 'equipment_serial3')->whereDate('created_at', Carbon::today())
                ->get());

            $uppcasearray = array_map('strtoupper', $all_serials);
            $uppcaseserial1 = strtoupper($request->input('equipment_serial1'));
            $uppcaseserial2 = strtoupper($request->input('equipment_serial2'));
            $uppcaseserial3 = strtoupper($request->input('equipment_serial3'));

            Validator::make($request->all(), [
                'equipment_serial1' => 'required',
                'created_at' => 'required',
                'equipment_model_id' => 'required',
                'equipment_type_id' => 'required',
                'equipment_subtype_id' => 'required',
            ], [
                'equipment_serial1.required' => 'Polje "Serijski broj 1" je obavezno',
                'created_at.required' => 'Niste izabrali "Datum i vreme unosa orpeme"',
                'equipment_model_id.required' => 'Polje "Model" je obavezno',
                'equipment_type_id.required' => 'Polje "Vrsta opreme" je obavezno',
                'equipment_subtype_id.required' => 'Polje "Tip opreme" je obavezno',
            ])->validate();


            $equipment = Equipment::find($request->input('equipment_id'));

            if (in_array($uppcaseserial1, $uppcasearray)) {

                if (strtoupper($equipment->equipment_serial1) != $uppcaseserial1) {

                    Validator::make($request->all(), [
                        'serial1' => 'required',
                    ], [
                        'serial1.required' => 'Danas je već uneta oprema sa serijskim brojem "' . $uppcaseserial1 . '" (Serijski broj 1)',
                    ])->validate();

                }

            }

            if (in_array($uppcaseserial2, $uppcasearray)) {

                if (strtoupper($equipment->equipment_serial2) != $uppcaseserial2) {
                    Validator::make($request->all(), [
                        'serial2' => 'required',
                    ], [
                        'serial2.required' => 'Oprema sa serijskim brojem "' . $uppcaseserial2 . '" (Serijski broj 2) već postoji',
                    ])->validate();
                }
            }

            if (in_array($uppcaseserial3, $uppcasearray)) {

                if (strtoupper($equipment->equipment_serial3) != $uppcaseserial3) {
                    Validator::make($request->all(), [
                        'serial3' => 'required',
                    ], [
                        'serial3.required' => 'Oprema sa serijskim brojem "' . $uppcaseserial3 . '" (Serijski broj 3) već postoji',
                    ])->validate();
                }
            }

            $equipment->equipment_type_id = $request->input('equipment_type_id');
            $equipment->equipment_subtype_id = $request->input('equipment_subtype_id');
            $equipment->equipment_model_id = $request->input('equipment_model_id');
            $equipment->equipment_serial1 = $request->input('equipment_serial1');
            $equipment->equipment_serial1 = $request->input('equipment_serial1');
            $equipment->equipment_serial2 = $request->input('equipment_serial2');
            $equipment->equipment_serial3 = $request->input('equipment_serial3');
            $equipment->created_at = datetimeForDatabase($request->input('created_at'));
            $equipment->equipment_dispatch_note = str_replace(' ', '', $request->input('equipment_dispatch_note'));
            $equipment->save();
            return redirect('/equipment')->with('success', 'Uspešno izmeneni podaci o opremi');
        } elseif ($id == "delete") {
            //dd(Auth::user()->name);

            $equipment = Equipment::find($request->input('equipment_id'));

            $equipment->equipment_status = 'Izbrisan';

            $equipment->equipment_technician = Auth::user()->name;
            // dd($equipment);
            $equipment->save();
            return redirect('/equipment/deleted_equipment')->with('delete', 'Uspešno izbrisana oprema');
        } elseif ($id == "sbb_warehouse") {
            //dd(Auth::user()->name);

            $equipment = Equipment::find($request->input('equipment_id'));

            $equipment->equipment_status = 'SBB magacin';

            $equipment->equipment_technician = Auth::user()->name;
            // dd($equipment);
            $equipment->save();
            return redirect('/equipment/sbb_warehouse')->with('success', 'Oprema uspešno vraćena u SBB magacin');
        } elseif ($id == "discharge") {
            $equipment = Equipment::find($request->input('equipment_id'));

            DB::transaction(function () use ($equipment) {

                $dischargedEquipment = new DischargedEquipment();
                $dischargedEquipment->equipment_id = $equipment->equipment_id;
                $dischargedEquipment->equipment_technician = $equipment->equipment_technician;
                $dischargedEquipment->equipment_assign_date = $equipment->equipment_assign_date;
                $dischargedEquipment->discharged_by = Auth::user()->name;
                $dischargedEquipment->created_at = date('Y-m-d H:i:s');
                $dischargedEquipment->save();

                $equipment->equipment_technician = null;
                $equipment->equipment_assign_date = null;
                $equipment->equipment_status = 'U magacinu';
                $equipment->save();

            });

            return redirect('/equipment/discharge')->with('success', 'Uspešno razdužena oprema');

        } elseif ($id == "group_discharge") {

            Validator::make($request->all(), [
                'equipment_technician' => 'required',
            ], [
                'equipment_technician.required' => 'Niste izabrali tehničara kojeg želite da razdužite',
            ])->validate();

            $technician_id = explode('_', $request->input('equipment_technician'))[1];

            $technician_equipments = Equipment::where('equipment_technician', '=', $technician_id)->where('equipment_status', '=', 'Kod tehničara')->get();

            /*dd($technician_equipments);*/

            foreach ($technician_equipments as $technician_equipment) {

                $dischargedEquipment = new DischargedEquipment();
                $dischargedEquipment->equipment_id = $technician_equipment->equipment_id;
                $dischargedEquipment->equipment_technician = $technician_equipment->equipment_technician;
                $dischargedEquipment->equipment_assign_date = $technician_equipment->equipment_assign_date;
                $dischargedEquipment->discharged_by = Auth::user()->name;
                $dischargedEquipment->created_at = date('Y-m-d H:i:s');
                $dischargedEquipment->save();
            }

            Equipment::where('equipment_technician', '=', $technician_id)
                ->where('equipment_status', '=', 'Kod tehničara')
                ->update([
                    'equipment_technician' => null,
                    'equipment_assign_date' => null,
                    'equipment_status' => 'U magacinu',
                ]);

            $technician = Technician::find($technician_id);

            return redirect('/equipment/discharge')->with('success', 'Uspešno razdužena oprema za ' . $technician->technician_name);
        } elseif ($id == "update_subtype") {

            Validator::make($request->all(), [
                'equipment_subtype_id' => 'required',
                'equipment_subtype_name' => 'required',
            ], [
                'equipment_subtype_id.required' => 'Doslo je do greske',
                'equipment_subtype_name.required' => 'Doslo je do greske',
            ])->validate();


            $subtype = DB::table('equipment_subtype')
                ->where('equipment_subtype_id', '=', $request->input('equipment_subtype_id'))->update(['equipment_subtype_name' => $request->input('equipment_subtype_name'), 'updated_at' => date('Y-m-d H:i:s')]);


            return redirect('/equipment/edit')->with('success', 'Uspešno izmenjen tip opreme')->with('active', 'equipment_subtype');
        } elseif ($id == "update_model") {

            Validator::make($request->all(), [
                'equipment_model_id' => 'required',
                'equipment_model_name' => 'required',
                'equipment_code' => 'required|unique:equipment_model,equipment_code',
            ], [
                'equipment_model_id.required' => 'Doslo je do greske',
                'equipment_model_name.required' => 'Doslo je do greske',
                'equipment_code.required' => 'Doslo je do greske',
                'equipment_code.unique' => 'Vec postoji model opreme sa kôdom ' . $request->input('equipment_code'),
            ])->validate();


            $model = DB::table('equipment_model')
                ->where('equipment_model_id', '=', $request->input('equipment_model_id'))->update(['equipment_model_name' => $request->input('equipment_model_name'), 'equipment_code' => $request->input('equipment_code'), 'updated_at' => date('Y-m-d H:i:s')]);


            return redirect('/equipment/edit')->with('success', 'Uspešno izmenjen model opreme')->with('active', 'equipment_model');
        } elseif ($id == "update_dismantled_eq_subtype") {

            Validator::make($request->all(), [
                'dismantled_eq_subtype_id' => 'required',
                'dismantled_eq_subtype_name' => 'required',
            ], [
                'dismantled_eq_subtype_id.required' => 'Doslo je do greske',
                'dismantled_eq_subtype_name.required' => 'Doslo je do greske',
            ])->validate();

            $subtype = DB::table('dismantled_eq_subtype')
                ->where('dismantled_eq_subtype_id', '=', $request->input('dismantled_eq_subtype_id'))->update(['dismantled_eq_subtype_name' => $request->input('dismantled_eq_subtype_name'), 'updated_at' => date('Y-m-d H:i:s')]);


            return redirect('/equipment/edit')->with('success', 'Uspešno izmenjen tip demontirane opreme')->with('active', 'dismantled_eq_subtype');
        } elseif ($id == "update_dismantled_eq_model") {

            Validator::make($request->all(), [
                'dismantled_eq_model_id' => 'required',
                'dismantled_eq_model_name' => 'required',
                'equipment_code' => 'required|unique:equipment_model,equipment_code',
            ], [
                'dismantled_eq_model_id.required' => 'Doslo je do greske',
                'dismantled_eq_model_name.required' => 'Doslo je do greske',
                'equipment_code.required' => 'Doslo je do greske',
                'equipment_code.unique' => 'Vec postoji model eontirane opreme sa kôdom ' . $request->input('equipment_code'),
            ])->validate();


            $dismantled_eq_model = DB::table('dismantled_eq_model')
                ->where('dismantled_eq_model_id', '=', $request->input('dismantled_eq_model_id'))->update(['dismantled_eq_model_name' => $request->input('dismantled_eq_model_name'), 'equipment_code' => $request->input('equipment_code'), 'updated_at' => date('Y-m-d H:i:s')]);


            return redirect('/equipment/edit')->with('success', 'Uspešno izmenjen model demontirane opreme')->with('active', 'dismantled_eq_model');
        }


    }

    public function deleted_equipment()
    {
        $deleted_equipment = DB::table('equipment')
            ->join('equipment_subtype', 'equipment.equipment_subtype_id', '=', 'equipment_subtype.equipment_subtype_id')
            ->leftJoin('equipment_model', 'equipment.equipment_model_id', '=', 'equipment_model.equipment_model_id')
            ->select('equipment.*', 'equipment_subtype.equipment_subtype_name', 'equipment_model.equipment_model_name')
            ->where('equipment.equipment_status', '=', 'Izbrisan')->get();

        /*dd($deleted_equipment);*/
        return view('equipment.deleted_equipment', compact('deleted_equipment'));
    }

    public function sbb_warehouse()
    {
        $sbb_warehouse = DB::table('equipment')
            ->join('equipment_subtype', 'equipment.equipment_subtype_id', '=', 'equipment_subtype.equipment_subtype_id')
            ->leftJoin('equipment_model', 'equipment.equipment_model_id', '=', 'equipment_model.equipment_model_id')
            ->select('equipment.*', 'equipment_subtype.equipment_subtype_name', 'equipment_model.equipment_model_name')
            ->where('equipment.equipment_status', '=', 'SBB magacin')->get();

        /*dd($deleted_equipment);*/
        return view('equipment.sbb_warehouse', compact('sbb_warehouse'));
    }

    public function discharged_equipment()
    {

        return view('equipment.discharged_equipment');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }


}
