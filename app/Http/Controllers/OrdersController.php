<?php

namespace App\Http\Controllers;

use App\Service;
use Validator;
use App\Equipment;
use Illuminate\Http\Request;
use App\Order;
use App\Select;
use App\Externalwarehouse;
use Illuminate\Support\Facades\DB;
use App\Technician;
use App\Wares;
use App\DismantledModels;
use Carbon\Carbon;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Auth;

class OrdersController extends Controller
{

    public function index(Request $request)
    {

        if (empty($request->input('date_filter'))) {

            //dd('prezan filter');
            $region_filter = $request->input('region');
            $all_orders = Order::where('order_type', 'sbb_order')->orderBy('order_id', 'desc')->get();
            $active_orders = Order::where('order_type', 'sbb_order')->where(function ($query) {
                $query->where('order_status', 'Otvoren')
                    ->orWhere('order_status', 'Zakazan')
                    ->orWhere('order_status', 'Odložen');
            })->orderBy('order_id', 'desc')->get();

            /*$removed_equipment = DB::table('removed_equipment')->get();
            return view('equipment.dismantled', compact('removed_equipment'));*/
        } else {

            $date_filter = $request->input('date_filter');
            $region_filter = $request->input('region');
            $exploded_date_filter = explode(' - ', $request->input('date_filter'));
            $exploded_start_date = explode('/', $exploded_date_filter[0]);
            $exploded_end_date = explode('/', $exploded_date_filter[1]);
            $start_date = $exploded_start_date['2'] . '-'
                . $exploded_start_date['1'] . '-'
                . $exploded_start_date['0'] . ' 00:00:01';
            $end_date = $exploded_end_date['2'] . '-'
                . $exploded_end_date['1'] . '-'
                . $exploded_end_date['0'] . ' 23:59:59';
            /* dd($end_date);*/

            if ($region_filter == null) {
                $all_orders = Order::where([
                    ['created_at', '>', $start_date],
                    ['created_at', '<', $end_date],
                    ['order_type', '=', 'sbb_order']
                ])->orderBy('order_id', 'desc')->get();

                $active_orders = Order::where([
                    ['created_at', '>', $start_date],
                    ['created_at', '<', $end_date],
                    ['order_type', '=', 'sbb_order']
                ])->where(function ($query) {
                    $query->where('order_status', 'Otvoren')
                        ->orWhere('order_status', 'Zakazan')
                        ->orWhere('order_status', 'Odložen');
                })->orderBy('order_id', 'desc')->get();
            } else {
                $all_orders = Order::where([
                    ['created_at', '>', $start_date],
                    ['created_at', '<', $end_date],
                    ['order_type', '=', 'sbb_order'],
                    ['region', '=', $region_filter]
                ])->orderBy('order_id', 'desc')->get();

                $active_orders = Order::where([
                    ['created_at', '>', $start_date],
                    ['created_at', '<', $end_date],
                    ['order_type', '=', 'sbb_order'],
                    ['region', '=', $region_filter]
                ])->where(function ($query) {
                    $query->where('order_status', 'Otvoren')
                        ->orWhere('order_status', 'Zakazan')
                        ->orWhere('order_status', 'Odložen');
                })->orderBy('order_id', 'desc')->get();
            }

            //dd($active_orders);
            //return view('equipment.dismantled', compact('removed_equipment', 'dismantled_date_filter'));
        }


        $region = createValueArray(Select::where('select_name', 'region')->pluck('select_value'));

        $orders = [
            'aktivni' => $active_orders,
            'aktivni_count' => $active_orders->count(),
            'otvoreni' => $all_orders->where('order_status', 'Otvoren'),
            'otvoreni_count' => $all_orders->where('order_status', 'Otvoren')->count(),
            'zakazani' => $all_orders->where('order_status', 'Zakazan'),
            'zakazani_count' => $all_orders->where('order_status', 'Zakazan')->count(),
            'odlozeni' => $all_orders->where('order_status', 'Odložen'),
            'odlozeni_count' => $all_orders->where('order_status', 'Odložen')->count(),
            'otkazani' => $all_orders->where('order_status', 'Otkazan'),
            'otkazani_count' => $all_orders->where('order_status', 'Otkazan')->count(),
            'nekompletni' => $all_orders->where('order_status', 'Završen')->where('uncomplete', 1),
            'nekompletni_count' => $all_orders->where('order_status', 'Završen')->where('uncomplete', 1)->count(),
            'nerealizovani' => $all_orders->where('order_status', 'Nerealizovan'),
            'nerealizovani_count' => $all_orders->where('order_status', 'Nerealizovan')->count(),
            'zavrseni' => $all_orders->where('order_status', 'Završen'),
            'zavrseni_count' => $all_orders->where('order_status', 'Završen')->count(),
            'region' => $region,
            'region_filter' => $region_filter,
        ];
        // dd($orders['nekompletni_count']);
        if (empty($request->input('date_filter'))) {
            return view('orders.index', compact('orders'));
        } else {
            return view('orders.index', compact('orders', 'date_filter'));
        }


    }

    /*public function index_all(Request $request)
    {

        if (empty($request->input('date_filter'))) {

            //dd('prezan filter');
            //$region_filter = $request->input('region');
            $all_orders = Order::where('order_type', 'sbb_order')->get();

        } else {

            $date_filter = $request->input('date_filter');
            //$region_filter = $request->input('region');
            $exploded_date_filter = explode(' - ', $request->input('date_filter'));
            $exploded_start_date = explode('/', $exploded_date_filter[0]);
            $exploded_end_date = explode('/', $exploded_date_filter[1]);
            $start_date = $exploded_start_date['2'] . '-'
                . $exploded_start_date['1'] . '-'
                . $exploded_start_date['0'] . ' 00:00:01';
            $end_date = $exploded_end_date['2'] . '-'
                . $exploded_end_date['1'] . '-'
                . $exploded_end_date['0'] . ' 23:59:59';

            $all_orders = Order::where([
                ['created_at', '>', $start_date],
                ['created_at', '<', $end_date],
                ['order_type', '=', 'sbb_order']
            ])->orderBy('order_id', 'desc')->get();

            if ($region_filter == null) {


            } else {
                $all_orders = Order::where([
                    ['created_at', '>', $start_date],
                    ['created_at', '<', $end_date],
                    ['order_type', '=', 'sbb_order'],
                    ['region', '=', $region_filter]
                ])->orderBy('order_id', 'desc')->get();
            }
        }


     $region = createValueArray(Select::where('select_name', 'region')->pluck('select_value'));

        $orders = [
            'all_orders' => $all_orders,
            'all_orders_count' => $all_orders->count()
            'region' => $region,
            'region_filter' => $region_filter,
        ];

        if (empty($request->input('date_filter'))) {
            return view('orders.index_all', compact('orders'));
        } else {
            return view('orders.index_all', compact('orders', 'date_filter'));
        }


    }*/

    public function index_all()
    {
        return view('orders.index_all');
    }

    public function group_search(Request $request)
    {

        if (!empty($request->input('group_search'))) {
            //dd($request->input('group_search'));
            $group_search_array = explode("\r\n", $request->input('group_search'));

            //dd(count($group_search_array));
            $orders = [];
            $not_found = [];
            foreach ($group_search_array as $order_number) {
                $order = Order::where('order_number', $order_number)->get();
                if(!isset($order['0'])){
                    $order = Order::where('order_number', $order_number.'N')->get();
                }

                if(isset($order['0'])){
                    $orders[] = $order['0'];
                }else{
                    $not_found[] = $order_number;
                }


            }
            $order_count = count($orders);

            //dd($orders);
           // dd($not_found);
        }

        if (empty($request->input('group_search'))) {
            return view('orders.group_search');
        } else {
            return view('orders.group_search', compact('orders', 'order_count', 'not_found'));
        }


    }

    public function create()
    {
        $city = Select::where('select_name', 'city')->where('flag', '1')->pluck('select_value');
        $township = Select::where('select_name', 'township')->where('flag', '1')->pluck('select_value');
        $region = Select::where('select_name', 'region')->where('flag', '1')->pluck('select_value');
        $services = DB::table('services')->where('order_type', 'sbb_order')->pluck('service_name', 'service_select');
        $subtype = createKeyArray(DB::table('equipment_subtype')
            ->rightJoin('equipment', 'equipment_subtype.equipment_subtype_id', '=', 'equipment.equipment_subtype_id')
            ->where('equipment_status', 'Kod tehničara')
            ->pluck('equipment_subtype.equipment_subtype_name', 'equipment_subtype.equipment_subtype_id'));

        $all_serials = createAllSerialsArray(DB::table('equipment')->select('equipment_id', 'equipment_serial1', 'equipment_serial2', 'equipment_serial3')
            ->where('equipment_status', 'Kod tehničara')
            ->get());

        /*$subtype = DB::table('equipment_subtype')->pluck('equipment_subtype_name', 'equipment_subtype_id');*/
       /* $dismantled_subtype = DB::table('dismantled_eq_subtype')->pluck('dismantled_eq_subtype_name', 'dismantled_eq_subtype_id');*/

        $dismantled_subtype = DB::table('dismantled_eq_model')
            ->join('dismantled_eq_subtype', 'dismantled_eq_model.dismantled_eq_subtype_id', '=', 'dismantled_eq_subtype.dismantled_eq_subtype_id')->distinct()
            ->pluck('dismantled_eq_subtype.dismantled_eq_subtype_name', 'dismantled_eq_model.dismantled_eq_subtype_id');


        /*$dismantled_model = createKeyArray(DB::table('dismantled_eq_model')->pluck('code_model', 'dismantled_eq_model_id'));*/

        $dismantled_model = createKeyArray(DismantledModels::all()->pluck('code_model', 'dismantled_eq_model_id'));

       // dd($dismantled_model);

        $all_wares = Wares::all();
        $default_wares = $all_wares->where('default', 1);
        $wares = createWaresArray($all_wares->where('default', 0));

        $technicians = createKeyArray(Technician::where('active', 1)->pluck('technician_name', 'technician_id'), 'technician_');

        $equipment_for_select = DB::table('equipment_subtype')
            ->join('equipment', 'equipment_subtype.equipment_subtype_id', '=', 'equipment.equipment_subtype_id')
            ->select('equipment.equipment_id', 'equipment_subtype.equipment_subtype_id', 'equipment_subtype.equipment_subtype_name', 'equipment.equipment_serial1', 'equipment.equipment_serial2', 'equipment.equipment_serial3')->where('equipment.equipment_status', 'Kod tehničara')->get();

        $dismantled_eq_for_select = DB::table('dismantled_eq_subtype')
            ->join('dismantled_eq_model', 'dismantled_eq_subtype.dismantled_eq_subtype_id', '=', 'dismantled_eq_model.dismantled_eq_subtype_id')
            ->select('dismantled_eq_subtype.dismantled_eq_subtype_id', 'dismantled_eq_subtype.dismantled_eq_subtype_name', 'dismantled_eq_model.dismantled_eq_model_id', 'dismantled_eq_model.dismantled_eq_model_name')->get();

        $data = [
            'city' => createValueArray($city),
            'township' => createValueArray($township),
            'region' => createValueArray($region),
            'services' => createKeyArray($services),
            'subtype' => $subtype,
            'all_serials' => $all_serials,
            'dismantled_subtype' => createKeyArray($dismantled_subtype),
            'dismantled_model' => $dismantled_model,
            'default_wares' => $default_wares,
            'wares' => $wares,
            'technicians' => $technicians,
            'equipment_for_select' => $equipment_for_select,
            'dismantled_eq_for_select' => $dismantled_eq_for_select,
        ];

        return view('orders.create', compact('data'));
    }

    public function create_with_status()
    {
        $township = Select::where('select_name', 'township')->pluck('select_value');
        $region = Select::where('select_name', 'region')->pluck('select_value');
        $services = DB::table('services')->where('order_type', 'sbb_order')->pluck('service_name', 'service_select');
        $subtype = DB::table('equipment_subtype')->pluck('equipment_subtype_name', 'equipment_subtype_id');
        $dismantled_subtype = DB::table('dismantled_eq_subtype')->pluck('dismantled_eq_subtype_name', 'dismantled_eq_subtype_id');

        $select = [
            'township' => createValueArray($township),
            'region' => createValueArray($region),
            'services' => createKeyArray($services),
            'subtype' => createKeyArray($subtype),
            'dismantled_subtype' => createKeyArray($dismantled_subtype),
        ];

        return view('orders.create_with_status', compact('select'));

    }

    public function show($id, $print)
    {

        $township = Select::where('select_name', 'township')->pluck('select_value');
        $region = Select::where('select_name', 'region')->pluck('select_value');

        $select = [
            'township' => createValueArray($township),
            'region' => createValueArray($region),
        ];

        $order = Order::find($id);

        $service = $order->service;
        //dd($order);
        $services = createKeyArray(DB::table('services')->where('order_type', 'sbb_order')->pluck('service_name', 'service_select'));
        $service_name = $order->service->service_name;


        $subtype = createKeyArray(DB::table('equipment_subtype')
            ->rightJoin('equipment', 'equipment_subtype.equipment_subtype_id', '=', 'equipment.equipment_subtype_id')
            ->where('equipment_status', 'Kod tehničara')
            ->pluck('equipment_subtype.equipment_subtype_name', 'equipment_subtype.equipment_subtype_id'));


        $all_serials = createAllSerialsArray(DB::table('equipment')->select('equipment_id', 'equipment_serial1', 'equipment_serial2', 'equipment_serial3')
            ->where('equipment_status', 'Kod tehničara')
            ->get());

        $planned_mounted = DB::table('planned_equipment')
            ->join('equipment_subtype', 'planned_equipment.planned_equipment_subtype', '=', 'equipment_subtype.equipment_subtype_id')
            ->select('equipment_subtype.equipment_subtype_name')
            ->where('planned_equipment.planned_equipment_status', '=', 'mounted')
            ->where('planned_equipment.planned_equipment_ordinal_number', '=', $order->ordinal_number)
            ->get();

        $planned_dismantled = DB::table('planned_equipment')
            ->join('dismantled_eq_subtype', 'planned_equipment.planned_equipment_subtype', '=', 'dismantled_eq_subtype.dismantled_eq_subtype_id')
            ->select('dismantled_eq_subtype.dismantled_eq_subtype_name')
            ->where('planned_equipment.planned_equipment_status', '=', 'dismantled')
            ->where('planned_equipment.planned_equipment_ordinal_number', '=', $order->ordinal_number)
            ->get();

        //dd($planned_dismantled->count());

        $technicians = createKeyArray(Technician::where('active', 1)->pluck('technician_name', 'technician_id'), 'technician_');
        $all_wares = Wares::all();
        $default_wares = $all_wares->where('default', 1);
        $wares = createWaresArray($all_wares->where('default', 0));
        $dismantled_subtype = createKeyArray(DB::table('dismantled_eq_subtype')->pluck('dismantled_eq_subtype_name', 'dismantled_eq_subtype_id'));
        $dismantled_model = createKeyArray(DB::table('dismantled_eq_model')->pluck('dismantled_eq_model_name', 'dismantled_eq_model_id'));

        $equipment_for_select = DB::table('equipment_subtype')
            ->join('equipment', 'equipment_subtype.equipment_subtype_id', '=', 'equipment.equipment_subtype_id')
            ->select('equipment.equipment_id', 'equipment_subtype.equipment_subtype_id', 'equipment_subtype.equipment_subtype_name', 'equipment.equipment_serial1', 'equipment.equipment_serial2', 'equipment.equipment_serial3')->where('equipment.equipment_status', 'Kod tehničara')->get();

        $dismantled_eq_for_select = DB::table('dismantled_eq_subtype')
            ->join('dismantled_eq_model', 'dismantled_eq_subtype.dismantled_eq_subtype_id', '=', 'dismantled_eq_model.dismantled_eq_subtype_id')
            ->select('dismantled_eq_subtype.dismantled_eq_subtype_id', 'dismantled_eq_subtype.dismantled_eq_subtype_name', 'dismantled_eq_model.dismantled_eq_model_id', 'dismantled_eq_model.dismantled_eq_model_name')->get();

        if ($order->order_status == 'Nerealizovan' or $order->order_status == 'Završen') {

            $used_equipment = DB::table('equipment')
                ->join('equipment_subtype', 'equipment.equipment_subtype_id', '=', 'equipment_subtype.equipment_subtype_id')
                ->join('equipment_model', 'equipment.equipment_model_id', '=', 'equipment_model.equipment_model_id')
                ->select('equipment.*', 'equipment_subtype.equipment_subtype_name', 'equipment_model.equipment_model_name', 'equipment_model.equipment_code')
                ->where('equipment.equipment_ordinal_number', '=', $order->ordinal_number)
                ->get();


            $used_wares = DB::table('used_wares')
                ->join('wares', 'used_wares.wares_id', '=', 'wares.wares_id')
                ->select('wares.wares_code', 'wares.wares_name', 'used_wares.quantity')
                ->where('used_wares.ordinal_number', '=', $order->ordinal_number)
                ->get();

            $removed_equipment = DB::table('removed_equipment')
                ->join('dismantled_eq_subtype', 'removed_equipment.removed_equipment_subtype', '=', 'dismantled_eq_subtype.dismantled_eq_subtype_id')
                ->join('dismantled_eq_model', 'removed_equipment.removed_equipment_model', '=', 'dismantled_eq_model.dismantled_eq_model_id')
                ->select('dismantled_eq_subtype.dismantled_eq_subtype_name', 'dismantled_eq_model.dismantled_eq_model_name', 'dismantled_eq_model.equipment_code', 'removed_equipment.removed_equipment_serial1', 'removed_equipment.removed_equipment_serial2')
                ->where('removed_equipment.ordinal_number', '=', $order->ordinal_number)
                ->get();

            //treba resiti preko relacije!!!!
            $order_technician = DB::table('order_technician')
                ->join('technicians', 'order_technician.technician_id', '=', 'technicians.technician_id')
                ->join('orders', 'order_technician.ordinal_number', '=', 'orders.ordinal_number')
                ->select('technicians.technician_name')
                ->where('order_technician.ordinal_number', '=', $order->ordinal_number)
                ->get();

        } else {
            $used_equipment = '';
            $used_wares = '';
            $removed_equipment = '';
            $order_technician = '';
        }

        $date = date('Y-m-d H:i:s');
        //dd($order_technician);
        $data = [
            'order' => $order,
            'subtype' => $subtype,
            'default_wares' => $default_wares,
            'wares' => $wares,
            'dismantled_subtype' => $dismantled_subtype,
            'dismantled_model' => $dismantled_model,
            'all_serials' => $all_serials,
            'technicians' => $technicians,
            'planned_mounted' => $planned_mounted,
            'planned_dismantled' => $planned_dismantled,
            'equipment_for_select' => $equipment_for_select,
            'dismantled_eq_for_select' => $dismantled_eq_for_select,
            'used_equipment' => $used_equipment,
            'used_wares' => $used_wares,
            'removed_equipment' => $removed_equipment,
            'order_technician' => $order_technician,
            'service' => $service,
            'services' => $services,
            'service_name' => $service_name,
            'date' => $date,

        ];

        // dd($data);

        if ($print == 'print') {
            return view('print.open_order', compact('data', 'id'));
        }

        return view('orders.show', compact('data', 'select', 'id'));

    }

    public function show2()
    {
        //return view('orders.show2');
        $orders = Order::orderBy('order_id', 'desc')->paginate(20);
        return view('orders.show2')->with('orders', $orders);
    }

    public function edit($id)
    {
        $order = Order::find($id);

        // dd($order->order_status == 'Završen' and Auth::user()->role == 'Kordinator');

        if ($order->order_status == 'Završen' and Auth::user()->role != 'Kordinator') {
            return redirect('orders/' . $id . '/show');
        }

        //dd($order);

        $service = $order->service;


        $service_name = $order->service->service_name;

        $services = createKeyArray(DB::table('services')->where('order_type', 'sbb_order')->pluck('service_name', 'service_select'));

        $subtype = createKeyArray(DB::table('equipment_subtype')
            ->rightJoin('equipment', 'equipment_subtype.equipment_subtype_id', '=', 'equipment.equipment_subtype_id')
            ->where('equipment.equipment_status', 'Kod tehničara')
            ->orWhere('equipment.equipment_ordinal_number', $order->ordinal_number)
            ->pluck('equipment_subtype.equipment_subtype_name', 'equipment_subtype.equipment_subtype_id'));
        //dd($subtype);
        $all_serials = createAllSerialsArray(DB::table('equipment')->select('equipment_id', 'equipment_serial1', 'equipment_serial2', 'equipment_serial3')
            ->where('equipment_status', 'Kod tehničara')
            ->orWhere('equipment_ordinal_number', $order->ordinal_number)
            ->get());

        $dismantled_eq_for_select = DB::table('dismantled_eq_subtype')
            ->join('dismantled_eq_model', 'dismantled_eq_subtype.dismantled_eq_subtype_id', '=', 'dismantled_eq_model.dismantled_eq_subtype_id')
            ->select('dismantled_eq_subtype.dismantled_eq_subtype_id', 'dismantled_eq_subtype.dismantled_eq_subtype_name', 'dismantled_eq_model.dismantled_eq_model_id', 'dismantled_eq_model.dismantled_eq_model_name')->get();

        /*$used_equipment_serials = createAllSerialsArray(DB::table('equipment')->select('equipment_id', 'equipment_serial1', 'equipment_serial2', 'equipment_serial3')->where('equipment.equipment_ordinal_number', '=', $order->ordinal_number)
            ->get());*/

        // dd($all_serials);

        $planned_mounted = DB::table('planned_equipment')
            ->join('equipment_subtype', 'planned_equipment.planned_equipment_subtype', '=', 'equipment_subtype.equipment_subtype_id')
            ->select('equipment_subtype.equipment_subtype_name')
            ->where('planned_equipment.planned_equipment_status', '=', 'mounted')
            ->where('planned_equipment.planned_equipment_ordinal_number', '=', $order->ordinal_number)
            ->get();

        $planned_dismantled = DB::table('planned_equipment')
            ->join('dismantled_eq_subtype', 'planned_equipment.planned_equipment_subtype', '=', 'dismantled_eq_subtype.dismantled_eq_subtype_id')
            ->select('dismantled_eq_subtype.dismantled_eq_subtype_name')
            ->where('planned_equipment.planned_equipment_status', '=', 'dismantled')
            ->where('planned_equipment.planned_equipment_ordinal_number', '=', $order->ordinal_number)
            ->get();

        //dd($planned_dismantled->count());

        $technicians = createKeyArray(Technician::where('active', 1)->pluck('technician_name', 'technician_id'), 'technician_');
        $all_wares = Wares::all();
        $default_wares = $all_wares->where('default', 1);
        $wares = createWaresArray($all_wares->where('default', 0));
        $dismantled_subtype = createKeyArray(DB::table('dismantled_eq_subtype')->pluck('dismantled_eq_subtype_name', 'dismantled_eq_subtype_id'));
        $dismantled_model = createKeyArray(DB::table('dismantled_eq_model')->pluck('dismantled_eq_model_name', 'dismantled_eq_model_id'));

        $equipment_for_select = DB::table('equipment_subtype')
            ->join('equipment', 'equipment_subtype.equipment_subtype_id', '=', 'equipment.equipment_subtype_id')
            ->select('equipment.equipment_id', 'equipment_subtype.equipment_subtype_id', 'equipment_subtype.equipment_subtype_name', 'equipment.equipment_serial1', 'equipment.equipment_serial2', 'equipment.equipment_serial3')->where('equipment.equipment_status', 'Kod tehničara')
            ->orWhere('equipment.equipment_ordinal_number', $order->ordinal_number)
            ->get();

        // dd($service);
        if ($order->order_status == 'Nerealizovan' or $order->order_status == 'Završen') {

            $used_equipment = DB::table('equipment')
                ->join('equipment_subtype', 'equipment.equipment_subtype_id', '=', 'equipment_subtype.equipment_subtype_id')
                ->join('equipment_model', 'equipment.equipment_model_id', '=', 'equipment_model.equipment_model_id')
                ->select('equipment.*', 'equipment_subtype.equipment_subtype_name', 'equipment_model.equipment_model_name', 'equipment_model.equipment_code')
                ->where('equipment.equipment_ordinal_number', '=', $order->ordinal_number)
                ->get();

            $count_used_equipment = count($used_equipment);
            //dd(count($used_equipment));

            $used_wares = DB::table('used_wares')
                ->join('wares', 'used_wares.wares_id', '=', 'wares.wares_id')
                ->select('wares.wares_id', 'wares.wares_code', 'wares.wares_type', 'wares.wares_name', 'used_wares.quantity')
                ->where('used_wares.ordinal_number', '=', $order->ordinal_number)
                ->get();
            //dd($used_wares);
            $removed_equipment = DB::table('removed_equipment')
                ->join('dismantled_eq_subtype', 'removed_equipment.removed_equipment_subtype', '=', 'dismantled_eq_subtype.dismantled_eq_subtype_id')
                ->join('dismantled_eq_model', 'removed_equipment.removed_equipment_model', '=', 'dismantled_eq_model.dismantled_eq_model_id')
                ->select('dismantled_eq_subtype.dismantled_eq_subtype_name', 'removed_equipment.removed_equipment_subtype',
                    'dismantled_eq_model.dismantled_eq_model_name', 'removed_equipment.removed_equipment_model', 'removed_equipment.removed_equipment_status',
                    'removed_equipment.removed_equipment_serial1', 'removed_equipment.removed_equipment_serial2')
                ->where('removed_equipment.ordinal_number', '=', $order->ordinal_number)
                ->get();

            $count_removed_equipment = count($removed_equipment);

            $order_technician = DB::table('order_technician')
                ->select('technician_id')
                ->where('ordinal_number', '=', $order->ordinal_number)
                ->get();
            //dd($order_technician['0']->technician_id);
        } else {
            $used_equipment = '';
            $used_wares = '';
            $removed_equipment = '';
            $order_technician = '';
        }

        //dd($order_technician);
        $data = [
            'order' => $order,
            'subtype' => $subtype,
            'default_wares' => $default_wares,
            'wares' => $wares,
            'dismantled_subtype' => $dismantled_subtype,
            'dismantled_model' => $dismantled_model,
            'dismantled_eq_for_select' => $dismantled_eq_for_select,
            'all_serials' => $all_serials,
            'technicians' => $technicians,
            'planned_mounted' => $planned_mounted,
            'planned_dismantled' => $planned_dismantled,
            'equipment_for_select' => $equipment_for_select,
            'used_equipment' => $used_equipment,
            'used_wares' => $used_wares,
            'removed_equipment' => $removed_equipment,
            'order_technician' => $order_technician,
            'service' => $service,
            'services' => $services,
            'service_name' => $service_name,
            'count_used_equipment' => $count_used_equipment,
            'count_removed_equipment' => $count_removed_equipment

        ];

        return view('orders.edit', compact('data', 'id'));
    }

    /*public function datetimeForDatabase($datetime)
    {
        $exploded_datetime = explode('  ', $datetime);
        dd($exploded_datetime);
    }*/

    public function store(Request $request)
    {
        Validator::make($request->all(), [
            'order_number' => 'required | unique:orders',
            'opened_at_datetime' => 'required',
            'buyer' => 'required',
            'city' => 'required',
            'region' => 'required',
            'township' => 'required',
            'address' => 'required',
            'address_number' => 'required',
            'phone_number' => 'required',
            'service_type' => 'required',
            'technician_1' => 'required',
            'completed_at' => 'required',
            'service_home' => 'required',
        ], [
            'order_number.required' => 'Polje "Broj naloga" je obavezno',
            'order_number.unique' => 'Već postoji nalog sa brojem ' . $request->input('order_number'),
            'opened_at_datetime.required' => 'Polje "Datum i vreme prijave" je obavezno',
            'buyer.required' => 'Polje "Ime i prezime kupca" je obavezno',
            'city.required' => 'Polje "Grad" je obavezno, izaberite region',
            'region.required' => 'Polje "Region" je obavezno, izaberite region',
            'township.required' => 'Polje "Opština" je obavezno, izaberite opštinu',
            'address.required' => 'Polje "Adresa" je obavezno',
            'address_number.required' => 'Polje "broj" (Adresa) je obavezno',
            'phone_number.required' => 'Polje "Broj telefona" je obavezno',
            'service_type.required' => 'Polje "Tip posla" je obavezno, izaberite tip posla',
            'technician_1.required' => 'Polje "Prvi tehničar" je obavezno',
            'completed_at.required' => 'Polje "Datum i vreme zatvaranja naloga" je obavezno',
            'service_home.required' => 'Polje "Vrsta objekta" je obavezno'
        ])->validate();

        $new_ordinal_number = newSbbOrdinalNumber();

        //create order
        $sbb_order = new Order;
        $sbb_order->ordinal_number = $new_ordinal_number;
        $sbb_order->order_type = 'sbb_order';
        $sbb_order->order_status = 'Završen';
        $sbb_order->order_number = $request->input('order_number');
        $sbb_order->opened_at = datetimeForDatabase($request->input('opened_at_datetime'));
        $sbb_order->buyer_id = $request->input('buyer_id');
        $sbb_order->treaty_id = $request->input('treaty_id');
        $sbb_order->buyer = $request->input('buyer');
        $sbb_order->area_code = $request->input('area_code');
        $sbb_order->city = $request->input('city');
        $sbb_order->region = $request->input('region');
        $sbb_order->township = $request->input('township');
        $sbb_order->address = $request->input('address');
        $sbb_order->address_number = $request->input('address_number');
        $sbb_order->floor = $request->input('floor');
        $sbb_order->apartment = $request->input('apartment');
        $sbb_order->phone_number = $request->input('phone_number');
        $sbb_order->mobile_number = $request->input('mobile_number');
        $sbb_order->service_type = $request->input('service_type');
        $sbb_order->service_home = $request->input('service_home');
        $sbb_order->note = $request->input('order_note');
        $sbb_order->order_updated_by = Auth::user()->name;
        $sbb_order->created_at = date('Y-m-d H:i:s');
        $sbb_order->completed_at = datetimeForDatabase($request->input('completed_at'));

        if ($request->input('private_vehicle') == '1') {
            $sbb_order->private_vehicle = 1;
        } else {
            $sbb_order->private_vehicle = 0;
        }

            // dd($request->all());

            DB::transaction(function () use ($request, $sbb_order) {
                // funkcije pisane u app/Helpers/functions.php

                //promena statusa opreme

                updateEquipmentStatus($request, $sbb_order);

                //upisivanje demontirane opreme

                insertRemovedEquipment($request, $sbb_order);

                // upisivanje upotrebljenog materijala i skidanje sa stanja iz spoljnog magacina

                insertUsedWares($request, $sbb_order);
                insertNotDefaultUsedWares($request, $sbb_order);

                //upisivanje tehnicara koji su radili

                insertTechnicians($request, $sbb_order);

                // create finance_report

                $service = DB::table('services')
                    ->where('service_select', $request->input('service_type'))
                    ->get();

                financeReport($service, $sbb_order);
                // create technicina_report


                //izracunavanje za isvestaj

                //da li je korisceno privatno vizilo
                $technicians_report_private_vehicle = isUsedPrivateVehicle($service, $request);


                $technicians_report_sum = 0;
                //dd($equipment_count);
                if ($service['0']->price_fixed == null) {

                    $technicians_report_price_fixed = null;

                    if ($request->input('service_home') == 'apartment') {
                        $technicians_report_apartment = $service['0']->price_apartment;
                        $technicians_report_house = null;
                        $technicians_report_sum += $technicians_report_apartment;
                    } else if ($request->input('service_home') == 'house') {
                        $technicians_report_house = $service['0']->price_house;
                        $technicians_report_apartment = null;
                        $technicians_report_sum += $technicians_report_house;
                    }

                    $equipment_count = getEquipmentCount($request);

                    if ($equipment_count >= 1) {
                        $technicians_report_first_equipment = $service['0']->price_first_equipment;
                        $technicians_report_sum += $technicians_report_first_equipment;
                    } else {
                        $technicians_report_first_equipment = null;
                    }

                    if ($equipment_count > 1) {
                        $additional_equipment = $equipment_count - 1;
                        $technicians_report_additional_equipment = $service['0']->price_additional_equipment * $additional_equipment;
                        $technicians_report_sum += $technicians_report_additional_equipment;
                    } else {
                        $technicians_report_additional_equipment = null;
                    }

                    $service_for_report = new Service;
                    $service_for_report->price_apartment = $technicians_report_apartment;
                    $service_for_report->price_house = $technicians_report_house;
                    $service_for_report->price_first_equipment = $technicians_report_first_equipment;
                    $service_for_report->price_additional_equipment = $technicians_report_additional_equipment;
                    $service_for_report->price_private_vehicle = $technicians_report_private_vehicle;
                    $service_for_report->sum = $technicians_report_sum;


                } else {
                    $service_for_report = new Service;
                    $service_for_report->price_apartment = null;
                    $service_for_report->price_house = null;
                    $service_for_report->price_first_equipment = null;
                    $service_for_report->price_additional_equipment = null;
                    $service_for_report->price_fixed = $service['0']->price_fixed;
                    $service_for_report->price_private_vehicle = $technicians_report_private_vehicle;
                    $service_for_report->sum = $service['0']->price_fixed;
                }

                if ($request->input('technician_2') != null) {
                    //ako su dva tehnicara, upisuju se dva sloga u bazu i deli se suma na 2

                    //za drugog je private_vehicle null

                    techniciansReport($sbb_order, $request, $service_for_report, 2);

                    if ($technicians_report_private_vehicle != null) {
                        $service_for_report->sum = ($service_for_report->sum / 2) + $technicians_report_private_vehicle;
                    } else {
                        $service_for_report->sum = $service_for_report->sum / 2;
                    }

                    //dd($service_for_report);
                    techniciansReport($sbb_order, $request, $service_for_report, 1);


                } else {

                    if ($technicians_report_private_vehicle != null) {
                        $service_for_report->sum += $technicians_report_private_vehicle;
                    }

                    //$service_for_report->sum = $technicians_report_sum;

                    techniciansReport($sbb_order, $request, $service_for_report, 1);
                }


                // $order->service_home = $request->input('service_home'); prebaceno na pocetak zbog finance reporta


                //dd($sbb_order);
                $sbb_order->save();
                //dd($request->all());
            });

        //dd($sbb_order);

      //  $id = DB::table('orders')->orderBy('order_id', 'desc')->first()->order_id;

        return redirect('/orders/' . $sbb_order->order_id . '/show')->with('success', 'Uspešno je kreiran novi nalog ' . $sbb_order->ordinal_number);

    }

    public function store_with_status(Request $request)
    {

        Validator::make($request->all(), [
            'order_number' => 'required | unique:orders',
            'opened_at_datetime' => 'required',
            /*'started_at_datetime' => 'required',*/
            'buyer' => 'required',
            'region' => 'required',
            'township' => 'required',
            'address' => 'required',
            'address_number' => 'required',
            'phone_number' => 'required',
            'service_type' => 'required',

        ], [
            'order_number.required' => 'Polje "Broj naloga" je obavezno',
            'order_number.unique' => 'Već postoji nalog sa brojem ' . $request->input('order_number'),
            'opened_at_datetime.required' => 'Polje "Datum i vreme prijave" je obavezno',
            /*'started_at_datetime.required' => 'Polje "Predvidjeni pocetak rada" je obavezno',*/
            'buyer.required' => 'Polje "Ime i prezime kupca" je obavezno',
            'region.required' => 'Polje "Region" je obavezno, izaberite region',
            'township.required' => 'Polje "Opština" je obavezno, izaberite opštinu',
            'address.required' => 'Polje "Adresa" je obavezno',
            'address_number.required' => 'Polje "broj" (Adresa) je obavezno',
            'phone_number.required' => 'Polje "Broj telefona" je obavezno',
            'service_type.required' => 'Polje "Tip posla" je obavezno, izaberite tip posla',
        ])->validate();

        /*if ($validator->fails()) {
            return redirect('post/create')
                ->withErrors($validator)
                ->withInput();
        }*/


        $new_ordinal_number = newSbbOrdinalNumber();

        $planned_equipment_array = notNullArray($request, $new_ordinal_number);

        DB::table('planned_equipment')->insert($planned_equipment_array);
        //dd($planned_equipment_array);
        /*dd((int)$request->input('order_number'));*/
        //create order
        $sbb_order = new Order;
        $sbb_order->ordinal_number = $new_ordinal_number;
        $sbb_order->order_type = 'sbb_order';
        $sbb_order->order_status = 'Otvoren';
        $sbb_order->order_number = $request->input('order_number');
        $sbb_order->opened_at = datetimeForDatabase($request->input('opened_at_datetime'));
        /*$sbb_order->started_at = datetimeForDatabase($request->input('started_at_datetime'));*/
        $sbb_order->buyer_id = $request->input('buyer_id');
        $sbb_order->treaty_id = $request->input('treaty_id');
        $sbb_order->buyer = $request->input('buyer');
        $sbb_order->area_code = $request->input('area_code');
        $sbb_order->city = $request->input('city');
        $sbb_order->region = $request->input('region');
        $sbb_order->township = $request->input('township');
        $sbb_order->address = $request->input('address');
        $sbb_order->address_number = $request->input('address_number');
        $sbb_order->floor = $request->input('floor');
        $sbb_order->apartment = $request->input('apartment');
        $sbb_order->phone_number = $request->input('phone_number');
        $sbb_order->mobile_number = $request->input('mobile_number');
        $sbb_order->service_type = $request->input('service_type');
        $sbb_order->note = $request->input('order_note');
        $sbb_order->order_updated_by = Auth::user()->name;
        $sbb_order->save();
        //dd($sbb_order);
        return redirect('/orders')->with('success', 'Uspešno je kreiran novi nalog ' . $sbb_order->ordinal_number);
    }

    public function store_file(Request $request, $id)
    {

        Validator::make($request->all(), [
            'file' => 'required | max:10240',
        ], [
            'file.required' => 'Niste izabrali fajl',
            'file.max' => 'Fajl nije sacuvan, maksimalna veličina fajla je 10MB',
        ])->validate();


        $order = Order::find($id);

        // Get file with the extension
        $filenameWithExt = $request->file('file')->getClientOriginalName();
        // Get jus filename
        // $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
        // Get jus ext
        //$extension = $request->file('cover_image')->getClientOriginalExtension();
        //FilenameToStore
        $fileNameToStore = $order->ordinal_number . '_' . $filenameWithExt;
        // Upload Image
        $path = $request->file('file')->storeAs('public/orders_files', $fileNameToStore);

        $order->file_name = $fileNameToStore;
        $order->save();

        return redirect('/orders/' . $id . '/show')->with('success', 'Uspešno dodat fajl za nalog ' . $order->ordinal_number);

    }

    public function delete_file($id)
    {

        $order = Order::find($id);
        $file_name = $order->file_name;
        $order->file_name = null;
        if ($order->save()) {
            Storage::delete('public/orders_files/' . $file_name);
        }

        return redirect('/orders/' . $id . '/show')->with('delete', 'Uspešno izbrisan fajl za nalog ' . $order->ordinal_number);

    }

    public function update_info(Request $request, $id)
    {

        $order_info = Input::all();

        $order = Order::find($id);

        // Call fill on the gift and pass in the data
        $order->fill($order_info);
        $order->order_updated_by = Auth::user()->name;
        $order->opened_at = datetimeForDatabase($order_info['opened_at_datetime']);
        /*$order->started_at = datetimeForDatabase($order_info['started_at_datetime']);*/
        //dd($order);

        $order->save();

        return redirect('/orders/' . $id . '/show')->with('success', 'Uspešno izmenjen nalog ' . $order->ordinal_number);
    }


    public function update(Request $request, $id)
    {
        //dd($request->all());
        // dd($id);

        if ($request->input('order_status') == "Otvoren"
            or $request->input('order_status') == "Odložen"
            or $request->input('order_status') == "Nekompletan") {
            $order = Order::find($id);
            $order->order_status = $request->input('order_status');
            $order->note = $request->input('order_comment');
            $order->order_updated_by = Auth::user()->name;
            $order->save();
            return redirect('/orders/' . $id . '/show')->with('success', 'Uspešno izmenjen nalog ' . $order->ordinal_number);
        } elseif ($request->input('order_status') == "Zakazan") {
            //dd($request->all());
            $order = Order::find($id);
            $order->order_status = $request->input('order_status');
            $order->scheduled_at = datetimeForDatabase($request->input('scheduled_at_datetime'));
            $order->note = $request->input('order_comment');
            $order->order_updated_by = Auth::user()->name;
            $order->save();
            return redirect('/orders/' . $id . '/show')->with('success', 'Uspešno izmenjen nalog ' . $order->ordinal_number);
        } elseif ($request->input('order_status') == "Otkazan") {
            //dd($request->all());
            $order = Order::find($id);

            $order->updated_at = date('Y-m-d H:i:s');

            $order->order_status = $request->input('order_status');
            $order->note = $request->input('order_comment');
            $order->order_updated_by = Auth::user()->name;
            $order->save();
            return redirect('/orders/' . $id . '/show')->with('success', 'Uspešno izmenjen nalog ' . $order->ordinal_number);
        } elseif ($request->input('order_status') == "Nerealizovan") {

            /* Validator::make($request->all(), [
                 'poruka' => 'required',

             ], [
                 'poruka.required' => 'Nije jos zavrsen program za status nerealizovan',
             ])->validate();*/


            $order = Order::find($id);

            if ($order->completed_at == null) {

                Validator::make($request->all(), [
                    'technician_1' => 'required',
                    'service_home' => 'required',
                    'completed_at' => 'required',
                ], [
                    'technician_1.required' => 'Polje "Prvi tehničar" je obavezno',
                    'service_home.required' => 'Polje "Vrsta objekta" je obavezno',
                    'completed_at.required' => 'Polje "Datum i vreme zatvaranja naloga" je obavezno',
                ])->validate();


                $order->service_home = $request->input('service_home'); //prebaceno na pocetak zbog finance reporta


                // dd($request->all());
                DB::transaction(function () use ($request, $order) {


                    //promena statusa opreme
                    updateEquipmentStatus($request, $order);

                    //upisivanje demontirane opreme
                    insertRemovedEquipment($request, $order);

                    // upisivanje upotrebljenog materijala i skidanje sa stanja iz spoljnog magacina
                    insertUsedWares($request, $order);

                    //upisivanje tehnicara koji su radili
                    insertTechnicians($request, $order);


                    if ($request->input('free') != 1) {

                        /* Validator::make($request->all(), [
                             'poruka' => 'required',

                         ], [
                             'poruka.required' => 'Bez naplate je čekiran',
                         ])->validate();*/

                        // create technicina_report

                        $service = DB::table('services')
                            ->where('service_select', $order->service_type)
                            ->get();

                        financeReport($service, $order);

                        //izracunavanje za isvestaj

                        //da li je korisceno privatno vizilo
                        $technicians_report_private_vehicle = isUsedPrivateVehicle($service, $request);

                        $technicians_report_sum = 400;

                        $technicians_report_apartment = null;
                        $technicians_report_house = null;
                        $technicians_report_first_equipment = null;
                        $technicians_report_additional_equipment = null;


                        $service_for_report = new Service;
                        $service_for_report->price_apartment = null;
                        $service_for_report->price_house = null;
                        $service_for_report->price_first_equipment = null;
                        $service_for_report->price_additional_equipment = null;
                        $service_for_report->price_private_vehicle = $technicians_report_private_vehicle;
                        $service_for_report->sum = $technicians_report_sum;

                        if ($request->input('technician_2') != null) {
                            //ako su dva tehnicara, upisuju se dva sloga u bazu i deli se suma na 2

                            //za drugog je private_vehicle null
                            techniciansReportUnrealized($order, $request, $technicians_report_sum, $technicians_report_private_vehicle, $service_for_report, 2);


                            if ($technicians_report_private_vehicle != null) {
                                $technicians_report_sum = ($technicians_report_sum / 2) + $technicians_report_private_vehicle;
                            } else {
                                $technicians_report_sum = $technicians_report_sum / 2;
                            }

                            techniciansReportUnrealized($order, $request, $technicians_report_sum, $technicians_report_private_vehicle, $service_for_report, 1);


                        } else {
                            if ($technicians_report_private_vehicle != null) {
                                $technicians_report_sum += $technicians_report_private_vehicle;
                            }

                            techniciansReportUnrealized($order, $request, $technicians_report_sum, $technicians_report_private_vehicle, $service_for_report, 1);

                        }


                    }


                    // update order
                    $order->order_status = $request->input('order_status');
                    //$order->service_home = $request->input('service_home'); prebaceno na pocetak zbog finance reporta

                    if ($request->input('private_vehicle') == '1') {
                        $order->private_vehicle = 1;
                    } else {
                        $order->private_vehicle = 0;
                    }

                    if (substr($order->order_number, -1) != 'N') {
                        $order->order_number = $order->order_number . 'N';
                    }

                    $order->note = $request->input('order_comment');
                    $order->service_type = $request->input('service_type');
                    $order->completed_at = datetimeForDatabase($request->input('completed_at'));
                    $order->order_updated_by = Auth::user()->name;
                    // dd($order);
                    $order->save();

                });

            } else {
                if (substr($order->order_number, -1) != 'N') {
                    $order->order_number = $order->order_number . 'N';
                }

                $order->note = $request->input('order_comment');
                $order->order_updated_by = Auth::user()->name;
                $order->save();
            }

            return redirect('/orders/' . $id . '/show')->with('success', 'Uspešno izmenjen nalog ' . $order->ordinal_number);

        } elseif ($request->input('order_status') == "Završen") {


        Validator::make($request->all(), [
            'technician_1' => 'required',
            'service_home' => 'required',
        ], [
            'technician_1.required' => 'Polje "Prvi tehničar" je obavezno',
            'service_home.required' => 'Polje "Vrsta objekta" je obavezno',
        ])->validate();


        //dd($request->all());
        $order = Order::find($id);
        // dd($order);
        $order->service_home = $request->input('service_home'); //prebaceno na pocetak zbog finance reporta

        if ($request->input('uncomplete') == '1') {
            $order->uncomplete = 1;
        } else {
            $order->uncomplete = 0;
        }

        if ($order->completed_at == null or $request->input('edit') == 1) {

            Validator::make($request->all(), [
                'technician_1' => 'required',
                'service_home' => 'required',
                'completed_at' => 'required',
                'completed_at' => 'required',
            ], [
                'technician_1.required' => 'Polje "Prvi tehničar" je obavezno',
                'service_home.required' => 'Polje "Vrsta objekta" je obavezno',
                'completed_at.required' => 'Polje "Datum i vreme zatvaranja naloga" je obavezno',
            ])->validate();

            //dd($request->input('order_status'));
            // dd($request->all());

            $order->service_home = $request->input('service_home'); //prebaceno na pocetak zbog finance reporta

            //dd($request->input('service_home'));


            DB::transaction(function () use ($request, $order) {
                // funkcije pisane u app/Helpers/functions.php
                //dd($request->input('edit'));
                if ($request->input('edit') == 1) {
                    orderCloseBack($order);
                }

                //promena statusa opreme

                updateEquipmentStatus($request, $order);

                //upisivanje demontirane opreme

                insertRemovedEquipment($request, $order);

                // upisivanje upotrebljenog materijala i skidanje sa stanja iz spoljnog magacina

                insertUsedWares($request, $order);

                //upisivanje tehnicara koji su radili

                insertTechnicians($request, $order);

                // create finance_report

                $service = DB::table('services')
                    ->where('service_select', $request->input('service_type'))
                    ->get();

                financeReport($service, $order);
                // create technicina_report


                //izracunavanje za isvestaj

                //da li je korisceno privatno vizilo
                $technicians_report_private_vehicle = isUsedPrivateVehicle($service, $request);


                $technicians_report_sum = 0;
                //dd($equipment_count);
                if ($service['0']->price_fixed == null) {

                    $technicians_report_price_fixed = null;

                    if ($request->input('service_home') == 'apartment') {
                        $technicians_report_apartment = $service['0']->price_apartment;
                        $technicians_report_house = null;
                        $technicians_report_sum += $technicians_report_apartment;
                    } else if ($request->input('service_home') == 'house') {
                        $technicians_report_house = $service['0']->price_house;
                        $technicians_report_apartment = null;
                        $technicians_report_sum += $technicians_report_house;
                    }

                    $equipment_count = getEquipmentCount($request);

                    if ($equipment_count >= 1) {
                        $technicians_report_first_equipment = $service['0']->price_first_equipment;
                        $technicians_report_sum += $technicians_report_first_equipment;
                    } else {
                        $technicians_report_first_equipment = null;
                    }

                    if ($equipment_count > 1) {
                        $additional_equipment = $equipment_count - 1;
                        $technicians_report_additional_equipment = $service['0']->price_additional_equipment * $additional_equipment;
                        $technicians_report_sum += $technicians_report_additional_equipment;
                    } else {
                        $technicians_report_additional_equipment = null;
                    }

                    $service_for_report = new Service;
                    $service_for_report->price_apartment = $technicians_report_apartment;
                    $service_for_report->price_house = $technicians_report_house;
                    $service_for_report->price_first_equipment = $technicians_report_first_equipment;
                    $service_for_report->price_additional_equipment = $technicians_report_additional_equipment;
                    $service_for_report->price_private_vehicle = $technicians_report_private_vehicle;
                    $service_for_report->sum = $technicians_report_sum;


                } else {
                    $service_for_report = new Service;
                    $service_for_report->price_apartment = null;
                    $service_for_report->price_house = null;
                    $service_for_report->price_first_equipment = null;
                    $service_for_report->price_additional_equipment = null;
                    $service_for_report->price_fixed = $service['0']->price_fixed;
                    $service_for_report->price_private_vehicle = $technicians_report_private_vehicle;
                    $service_for_report->sum = $service['0']->price_fixed;
                }

                if ($request->input('technician_2') != null) {
                    //ako su dva tehnicara, upisuju se dva sloga u bazu i deli se suma na 2

                    //za drugog je private_vehicle null

                    techniciansReport($order, $request, $service_for_report, 2);

                    if ($technicians_report_private_vehicle != null) {
                        $service_for_report->sum = ($service_for_report->sum / 2) + $technicians_report_private_vehicle;
                    } else {
                        $service_for_report->sum = $service_for_report->sum / 2;
                    }

                    //dd($service_for_report);
                    techniciansReport($order, $request, $service_for_report, 1);


                } else {

                    if ($technicians_report_private_vehicle != null) {
                        $service_for_report->sum += $technicians_report_private_vehicle;
                    }

                    //$service_for_report->sum = $technicians_report_sum;

                    techniciansReport($order, $request, $service_for_report, 1);
                }

                // update order
                $order->order_status = $request->input('order_status');
                // $order->service_home = $request->input('service_home'); prebaceno na pocetak zbog finance reporta

                if ($request->input('private_vehicle') == '1') {
                    $order->private_vehicle = 1;
                } else {
                    $order->private_vehicle = 0;
                }

                $order->note = $request->input('order_comment');
                $order->service_type = $request->input('service_type');
                $order->completed_at = datetimeForDatabase($request->input('completed_at'));
                $order->order_updated_by = Auth::user()->name;

                //dd($order);
                $order->save();
                //dd($request->all());
                /*$text = [];
                dd($text);*/
            });

        } else {
            $order->note = $request->input('order_comment');
            $order->order_updated_by = Auth::user()->name;
            $order->save();
        }

        return redirect('/orders/' . $id . '/show')->with('success', 'Uspešno izmenjen nalog ' . $order->ordinal_number);
    }
    }


    public function destroy($id)
    {
        $order = Order::find($id);
        $order_status = $order->order_status;

        if ($order_status == "Nerealizovan" or $order_status == "Završen") {
            DB::transaction(function () use ($order) {
                // funkcije pisane u app/Helpers/functions.php

                orderCloseBack($order);
                $order->delete();
            });

        } else {
            $order->delete();
        }

        return redirect('/orders')->with('delete', 'Uspešno izbrisan nalog ' . $order->ordinal_number);

    }

    public function test(Request $request)
    {


    }


}