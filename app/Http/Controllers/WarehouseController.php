<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Wares;
use App\Warehouse;
use App\Externalwarehouse;
use App\Technician;
use App\Models;
use App\Equipment;
use App\Subtype;
use App\Select;
use App\Order;
use Illuminate\Support\Facades\DB;
use Validator;
use Illuminate\Support\Facades\Auth;

class WarehouseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $warehouse = DB::table('warehouse')
            ->join('wares', 'warehouse.wares_id', '=', 'wares.wares_id')->select('warehouse.*', 'wares.wares_code', 'wares.wares_name', 'wares.default', 'wares.wares_type')->where('warehouse.quantity', '!=', 0)->get();

        /*dd($warehouse);*/

        return view('warehouse.index', compact('warehouse'));
    }

    public function external()
    {
        $externalwarehouse = DB::table('externalwarehouse')
            ->join('wares', 'externalwarehouse.wares_id', '=', 'wares.wares_id')->select('externalwarehouse.*', 'wares.wares_code', 'wares.wares_name', 'wares.default', 'wares.wares_type')->where('externalwarehouse.quantity', '!=', 0)->get();

       // dd($externalwarehouse);

        return view('warehouse.external', compact('externalwarehouse'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function add()
    {

        $wares = createWaresArray(DB::table('wares')->get());
        $last_added_wares = DB::table('added_wares')
            ->join('wares', 'added_wares.wares_id', '=', 'wares.wares_id')
            ->join('users', 'added_wares.user_id', '=', 'users.id')
            ->select(
                'added_wares.quantity',
                'added_wares.wares_dispatch_note',
                'added_wares.created_at',
                'wares.wares_code',
                'wares.wares_name',
                'wares.wares_type',
                'users.name')
            ->orderBy('added_wares.created_at', 'desc')
            ->limit(10)
            ->get();
/*dd($last_added_wares);*/

        return view('warehouse.add', compact('wares', 'last_added_wares'));
    }

    public function dispatch_note(Request $request)
    {

        if (empty($request->input('dispatch_note'))) {

            $added_wares = null;
            $print_date = null;
            $dispatch_note = null;
            return view('warehouse.dispatch_note', compact('added_wares', 'print_date', 'dispatch_note'));

        } else {

            $dispatch_note = $request->input('dispatch_note');

            $print_date = $request->input('print_date');

            $added_wares = DB::table('added_wares')
                ->join('wares', 'added_wares.wares_id', '=', 'wares.wares_id')
                ->join('users', 'added_wares.user_id', '=', 'users.id')
                ->select(
                    'added_wares.quantity',
                    'added_wares.wares_dispatch_note',
                    'added_wares.created_at',
                    'wares.wares_name',
                    'wares.wares_code',
                    'wares.wares_type',
                    'users.name')
                ->where('wares_dispatch_note', '=', $dispatch_note)->get();

            return view('warehouse.dispatch_note', compact('added_wares', 'dispatch_note', 'print_date'));
        }

    }



    public function assignment()
    {
        $technicians = createKeyArray(Technician::where('active', 1)->pluck('technician_name', 'technician_id'), 'technician_');

        $wares = createAssignmentWaresArray(DB::table('wares')->join('warehouse', 'wares.wares_id', '=', 'warehouse.wares_id')->where('warehouse.quantity',  '>', 0)->get());

        $assigned_wares = DB::table('assigned_wares')
            ->join('wares', 'assigned_wares.wares_id', '=', 'wares.wares_id')
            ->join('technicians', 'assigned_wares.technician_id', '=', 'technicians.technician_id')
            ->select(
                'assigned_wares.quantity',
                'assigned_wares.created_at',
                'wares.wares_name',
                'technicians.technician_name')
            ->orderBy('assigned_wares.created_at', 'desc')
            ->limit(10)
            ->get();

        $assignment = [
            'technicians' => $technicians,
            'wares' => $wares,
            ];

        return view('warehouse.assignment', compact('assignment', 'assigned_wares'));
    }

    public function assign(Request $request)
    {


        Validator::make($request->all(), [
            'wares_id' => 'required',
            'quantity' => 'required',
            'technician_id' => 'required',
        ], [
            'wares_id.required' => 'Polje "Materijal" je obavezno',
            'quantity.required' => 'Polje "Količina za zaduživanje" je obavezno',
            'technician_id.required' => 'Polje "Zaduzije se" je obavezno',
        ])->validate();

       /* dd($request->all());
        $quantity = $request->input('quantity');
        $exploded_wares_id = explode('_', $request->input('wares_id'));
        $warehouse = Warehouse::find($exploded_wares_id['2']);
        if ($quantity>$warehouse->quantity){
            return redirect('/warehouse/assignment');
        } else{
            $warehouse->quantity = $warehouse->quantity - $quantity;
            $warehouse->save();
        }
       */



        /*$warehouse->quantity = $warehouse->quantity - $quantity;

        dd($warehouse->quantity);*/
        DB::transaction(function() use ($request)
        {
            //uzimanje pdoataka iz $request
            $exploded_wares_id = explode('_', $request->input('wares_id'));
            $exploded_technician_id = explode('_', $request->input('technician_id'));
            $quantity = $request->input('quantity');

            //pronalazenje stavke u magacinu pod id
            $warehouse = Warehouse::find($exploded_wares_id['2']);
            if ($quantity>$warehouse->quantity){
                return redirect('/warehouse/assignment');
            } else{
                $warehouse->quantity = $warehouse->quantity - $quantity;
                $warehouse->save();
            }

            // dodavanje u spoljni magacin
            Externalwarehouse::where('wares_id',$exploded_wares_id['1'])->increment('quantity',$quantity);


            /*$externalwarehouse = new Externalwarehouse;
            $externalwarehouse->wares_id = $exploded_wares_id['1'];
            $externalwarehouse->quantity = $quantity;

            $externalwarehouse->save();*/


            DB::table('assigned_wares')->insert(
                ['technician_id' => $exploded_technician_id['1'],
                    'wares_id' => $exploded_wares_id['1'],
                    'quantity' => $quantity,
                    'created_at' => date('Y-m-d H:i:s'),
                ]);
        });


        return redirect('/warehouse/assignment');
    }



    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        Validator::make($request->all(), [
            'wares_id' => 'required',
            'quantity' => 'required',
        ], [
            'wares_id.required' => 'Polje "Materijal" je obavezno',
            'quantity.required' => 'Polje "Količina" je obavezno',
        ])->validate();
        /*dd($request->all());*/

        /*$exploded_wares = explode('_', $request->input('wares_id'));
        $quantity = $request->input('quantity');
        Warehouse::where('wares_id',$exploded_wares['1'])->increment('quantity',$quantity);

        return redirect('/warehouse/add');*/

        DB::transaction(function() use ($request)
        {
            //uzimanje pdoataka iz $request
            $exploded_wares_id = explode('_', $request->input('wares_id'));
            $quantity = $request->input('quantity');
            $wares_dispatch_note = $request->input('wares_dispatch_note');
            $wares_id = $exploded_wares_id['1'];
            $created_at = $request->input('created_at');

            // dodavanje na stanje
            Warehouse::where('wares_id',$wares_id)->increment('quantity',$quantity);

            //dodavanje u added_wares tabelu
            DB::table('added_wares')->insert(
                ['user_id' => Auth::user()->id,
                    'wares_id' => $wares_id,
                    'quantity' => $quantity,
                    'wares_dispatch_note' => $wares_dispatch_note,
                    'created_at' => $created_at,
                ]);



        });

        $session_data = [
            $wares_dispatch_note = $request->input('wares_dispatch_note'),
            $created_at = $request->input('created_at')
        ];


        return redirect('/warehouse/add')->with('session_data', $session_data)->with('success', 'Uspešno dodat materijal u magacin');;

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function unos_materijala_u_magacin()
    {
        $materijali = [
            [
                'Material' => '101001',
                'Material Description' => 'Kabl koaksijalni RG-11M',
                'Jed.mere' => 'met',
                'količina' => '1517'
            ],
            [
                'Material' => '101003',
                'Material Description' => 'Kabl koaksijalni  RG-6.',
            'Jed.mere' => 'met',
            'količina' => '410'
        ],
        [
            'Material' => '101008',
            'Material Description' => 'Kabl strujni PP/Y 3x1,5 (pun presek)',
            'Jed.mere' => 'met',
            'količina' => '385'
        ],
        [
            'Material' => '101014',
            'Material Description' => 'Provodnik P/F 4mm',
            'Jed.mere' => 'met',
            'količina' => '530'
        ],
        [
            'Material' => '101017',
            'Material Description' => 'UTP patch kabl CAT.5E',
            'Jed.mere' => 'met',
            'količina' => '5100'
        ],
        [
            'Material' => '101065',
            'Material Description' => 'Simplex Patchcord SC/APC - SC/PC 10m',
            'Jed.mere' => 'kom',
            'količina' => '2'
        ],
        [
            'Material' => '101069',
            'Material Description' => 'Slimpack patch. SC/APC-SC/APC 60m Monkey',
            'Jed.mere' => 'kom',
            'količina' => '30'
        ],
        [
            'Material' => '101070',
            'Material Description' => 'Slimpack patch. SC/APC-SC/APC 80m Monkey',
            'Jed.mere' => 'kom',
            'količina' => '38'
        ],
        [
            'Material' => '101071',
            'Material Description' => 'FTTH patchcord SC/APC - SC/APC 30m white',
            'Jed.mere' => 'kom',
            'količina' => '6'
        ],
        [
            'Material' => '101073',
            'Material Description' => 'Angle plug IEC  RG6cable 1,5m - Lup kabl',
            'Jed.mere' => 'kom',
            'količina' => '1160'
        ],
        [
            'Material' => '101200',
            'Material Description' => 'SmplxPatchco SC/APC-SC/APC 10m',
            'Jed.mere' => 'kom',
            'količina' => '50'
        ],
        [
            'Material' => '102003',
            'Material Description' => 'Konektor (T-32) 5/8-RG11',
            'Jed.mere' => 'kom',
            'količina' => '760'
        ],
        [
            'Material' => '102011',
            'Material Description' => 'Konektor, P28 za RG-11',
            'Jed.mere' => 'kom',
            'količina' => '45'
        ],
        [
            'Material' => '103002',
            'Material Description' => 'Završna imedansa 75 Ohm ',
            'Jed.mere' => 'kom',
            'količina' => '2250'
        ],
        [
            'Material' => '103004',
            'Material Description' => 'Adapter 5/8F-PG11M',
            'Jed.mere' => 'kom',
            'količina' => '60'
        ],
        [
            'Material' => '103007',
            'Material Description' => 'F-F nastavak indor',
            'Jed.mere' => 'kom',
            'količina' => '6900'
        ],
        [
            'Material' => '103008',
            'Material Description' => 'Adaptor,5/8-FF',
            'Jed.mere' => 'kom',
            'količina' => '98'
        ],
        [
            'Material' => '103015',
            'Material Description' => 'Outdoor zavrsna impedansa75Ohm',
            'Jed.mere' => 'kom',
            'količina' => '4'
        ],
        [
            'Material' => '103018',
            'Material Description' => 'Adapter 90° F ženski na  muški',
            'Jed.mere' => 'kom',
            'količina' => '1600'
        ],
        [
            'Material' => '104001',
            'Material Description' => 'Splitter 2-way,indoor',
            'Jed.mere' => 'kom',
            'količina' => '160'
        ],
        [
            'Material' => '104002',
            'Material Description' => 'Splitter 3-way,indoor',
            'Jed.mere' => 'kom',
            'količina' => '995'
        ],
        [
            'Material' => '104003',
            'Material Description' => 'Splitter 3-grani balansiran (WB),indoor',
            'Jed.mere' => 'kom',
            'količina' => '752'
        ],
        [
            'Material' => '104004',
            'Material Description' => 'Splitter 4 way, indoor',
            'Jed.mere' => 'kom',
            'količina' => '65'
        ],
        [
            'Material' => '104006',
            'Material Description' => 'Splitter 8-way,indoor',
            'Jed.mere' => 'kom',
            'količina' => '4'
        ],
        [
            'Material' => '104008',
            'Material Description' => 'High Pass Filter HPF-80',
            'Jed.mere' => 'kom',
            'količina' => '1415'
        ],
        [
            'Material' => '104017',
            'Material Description' => 'Splitter Outdoor SP-315UB Unbalanced',
            'Jed.mere' => 'kom',
            'količina' => '1'
        ],
        [
            'Material' => '104018',
            'Material Description' => 'Splitter Outdoor SP-315B balanced',
            'Jed.mere' => 'kom',
            'količina' => '8'
        ],
        [
            'Material' => '104021',
            'Material Description' => 'TAP IT-1W-6,indoor',
            'Jed.mere' => 'kom',
            'količina' => '185'
        ],
        [
            'Material' => '104022',
            'Material Description' => 'TAP, IT-1W-8,indoor',
            'Jed.mere' => 'kom',
            'količina' => '156'
        ],
        [
            'Material' => '104024',
            'Material Description' => 'TAP, IT-1W-10,indoor',
            'Jed.mere' => 'kom',
            'količina' => '80'
        ],
        [
            'Material' => '104026',
            'Material Description' => 'TAP,IT-1W-12,indoor',
            'Jed.mere' => 'kom',
            'količina' => '176'
        ],
        [
            'Material' => '104027',
            'Material Description' => 'TAP, IT-1W-16,indoor',
            'Jed.mere' => 'kom',
            'količina' => '25'
        ],
        [
            'Material' => '104029',
            'Material Description' => 'TAP, IT-1W-20,indoor',
            'Jed.mere' => 'kom',
            'količina' => '20'
        ],
        [
            'Material' => '104034',
            'Material Description' => 'TAP,IT-2W-8,indoor',
            'Jed.mere' => 'kom',
            'količina' => '18'
        ],
        [
            'Material' => '104038',
            'Material Description' => 'TAP, IT-2W-20,indoor',
            'Jed.mere' => 'kom',
            'količina' => '5'
        ],
        [
            'Material' => '104065',
            'Material Description' => 'TAP,outdoor, TP2W-8',
            'Jed.mere' => 'kom',
            'količina' => '16'
        ],
        [
            'Material' => '104066',
            'Material Description' => 'TAP,outdoor, TP2W-11',
            'Jed.mere' => 'kom',
            'količina' => '5'
        ],
        [
            'Material' => '104067',
            'Material Description' => 'TAP,outdoor, TP2W-14',
            'Jed.mere' => 'kom',
            'količina' => '4'
        ],
        [
            'Material' => '104073',
            'Material Description' => 'TP4W-8,outdoor',
            'Jed.mere' => 'kom',
            'količina' => '21'
        ],
        [
            'Material' => '104074',
            'Material Description' => 'TP4W-11,outdoor',
            'Jed.mere' => 'kom',
            'količina' => '23'
        ],
        [
            'Material' => '104075',
            'Material Description' => 'TP,4w-14,outdoor',
            'Jed.mere' => 'kom',
            'količina' => '22'
        ],
        [
            'Material' => '104076',
            'Material Description' => 'TP 4w-17,outdoor',
            'Jed.mere' => 'kom',
            'količina' => '24'
        ],
        [
            'Material' => '104077',
            'Material Description' => 'TP 4W-20db, outdoor',
            'Jed.mere' => 'kom',
            'količina' => '20'
        ],
        [
            'Material' => '104081',
            'Material Description' => 'Tap,TP8W-11,outdoor',
            'Jed.mere' => 'kom',
            'količina' => '25'
        ],
        [
            'Material' => '104082',
            'Material Description' => 'Tap,TP8W-14,outdoor',
            'Jed.mere' => 'kom',
            'količina' => '21'
        ],
        [
            'Material' => '104083',
            'Material Description' => 'Tap,TP8W-17,outdoor',
            'Jed.mere' => 'kom',
            'količina' => '19'
        ],
        [
            'Material' => '104084',
            'Material Description' => 'Odvodnik,outdoor,TP8W-20',
            'Jed.mere' => 'kom',
            'količina' => '25'
        ],
        [
            'Material' => '104092',
            'Material Description' => 'LPI 20A',
            'Jed.mere' => 'kom',
            'količina' => '25'
        ],
        [
            'Material' => '104101',
            'Material Description' => 'Razdelnik indoor 1,2 GHz 2-gr.',
            'Jed.mere' => 'kom',
            'količina' => '707'
        ],
        [
            'Material' => '104102',
            'Material Description' => 'Razdelnik indoor 1,2 GHz 3-grani',
            'Jed.mere' => 'kom',
            'količina' => '308'
        ],
        [
            'Material' => '104103',
            'Material Description' => 'Razdelnik indoor 1,2 GHz 3-grani balansi',
            'Jed.mere' => 'kom',
            'količina' => '900'
        ],
        [
            'Material' => '104104',
            'Material Description' => 'Razdelnik indoor 1,2 GHz 4-grani',
            'Jed.mere' => 'kom',
            'količina' => '470'
        ],
        [
            'Material' => '104105',
            'Material Description' => 'Razdelnik 6-grani,indoor',
            'Jed.mere' => 'kom',
            'količina' => '341'
        ],
        [
            'Material' => '104106',
            'Material Description' => 'Razdelnik 8-grani,indoor',
            'Jed.mere' => 'kom',
            'količina' => '331'
        ],
        [
            'Material' => '104107',
            'Material Description' => 'Odvodnik indoor 1,2 GHz IT-1W-06.',
            'Jed.mere' => 'kom',
            'količina' => '188'
        ],
        [
            'Material' => '104108',
            'Material Description' => 'Odvodnik indoor 1,2 GHz IT-1W-08',
            'Jed.mere' => 'kom',
            'količina' => '130'
        ],
        [
            'Material' => '104110',
            'Material Description' => 'Odv. indoor1,2GHzIT-1W-10',
            'Jed.mere' => 'kom',
            'količina' => '206'
        ],
        [
            'Material' => '104112',
            'Material Description' => 'Odv. indoor1,2GHzIT-1W-12',
            'Jed.mere' => 'kom',
            'količina' => '125'
        ],
        [
            'Material' => '104114',
            'Material Description' => 'Odvodnik, IT-1W-16,indoor',
            'Jed.mere' => 'kom',
            'količina' => '90'
        ],
        [
            'Material' => '104116',
            'Material Description' => 'Odvodnik, IT-1W-20,indoor',
            'Jed.mere' => 'kom',
            'količina' => '95'
        ],
        [
            'Material' => '104119',
            'Material Description' => 'Odvodnik, IT-2W-8,indoor',
            'Jed.mere' => 'kom',
            'količina' => '75'
        ],
        [
            'Material' => '104134',
            'Material Description' => 'Razdel.outdoor1,2GHZ2-grSP-215',
            'Jed.mere' => 'kom',
            'količina' => '24'
        ],
        [
            'Material' => '104135',
            'Material Description' => 'Razd.outdoor3-gr1,2GHzSP-315NBL',
            'Jed.mere' => 'kom',
            'količina' => '26'
        ],
        [
            'Material' => '104136',
            'Material Description' => 'Razd.outdoor3-gr1,2GHzSP-315BAL',
            'Jed.mere' => 'kom',
            'količina' => '32'
        ],
        [
            'Material' => '104169',
            'Material Description' => 'Uvodnik nap.outdoor 1,2GHz LPI',
            'Jed.mere' => 'kom',
            'količina' => '18'
        ],
        [
            'Material' => '106024',
            'Material Description' => 'Transformator 500VA,220V/65V',
            'Jed.mere' => 'kom',
            'količina' => '13'
        ],
        [
            'Material' => '106055',
            'Material Description' => 'Compact EGC MINI A932 40.10338 85',
            'Jed.mere' => 'kom',
            'količina' => '2'
        ],
        [
            'Material' => '106056',
            'Material Description' => 'Compact EGC MINI A93240.10 238 85',
            'Jed.mere' => 'kom',
            'količina' => '1'
        ],
        [
            'Material' => '106075',
            'Material Description' => 'Pojačavač Delta LHD 43-R GA',
            'Jed.mere' => 'kom',
            'količina' => '4'
        ],
        [
            'Material' => '106076',
            'Material Description' => 'Pojačavač Delta LHD 43 GA',
            'Jed.mere' => 'kom',
            'količina' => '3'
        ],
        [
            'Material' => '107002',
            'Material Description' => 'Plug-in atenuator 0db SA77140-00',
            'Jed.mere' => 'kom',
            'količina' => '10'
        ],
        [
            'Material' => '107003',
            'Material Description' => 'Plug-in atenuator 2db,SA77140-02',
            'Jed.mere' => 'kom',
            'količina' => '9'
        ],
        [
            'Material' => '107004',
            'Material Description' => 'Plug-in atenuator 4db,SA77140-04',
            'Jed.mere' => 'kom',
            'količina' => '10'
        ],
        [
            'Material' => '107005',
            'Material Description' => 'Plug-in atenuator 6db,SA77140-06',
            'Jed.mere' => 'kom',
            'količina' => '2'
        ],
        [
            'Material' => '107006',
            'Material Description' => 'Plug-in atenuator 8db,SA77140-08',
            'Jed.mere' => 'kom',
            'količina' => '3'
        ],
        [
            'Material' => '107007',
            'Material Description' => 'Plug-in atenuator 10db,SA77140-10',
            'Jed.mere' => 'kom',
            'količina' => '10'
        ],
        [
            'Material' => '107008',
            'Material Description' => 'Plug-in atenuator 12db,SA77140-12',
            'Jed.mere' => 'kom',
            'količina' => '3'
        ],
        [
            'Material' => '107010',
            'Material Description' => 'Plug-in splitter 3,5/3,5db, SA77041-10',
            'Jed.mere' => 'kom',
            'količina' => '5'
        ],
        [
            'Material' => '107011',
            'Material Description' => 'Plug-in splitter 2,0/6,0db,SA77042-10',
            'Jed.mere' => 'kom',
            'količina' => '5'
        ],
        [
            'Material' => '107012',
            'Material Description' => 'Plug-in splitter 1,5/10,0db,SA77043-10',
            'Jed.mere' => 'kom',
            'količina' => '3'
        ],
        [
            'Material' => '107013',
            'Material Description' => 'Plug-in splitter 0,6/14,0db,SA77044-10',
            'Jed.mere' => 'kom',
            'količina' => '5'
        ],
        [
            'Material' => '107019',
            'Material Description' => 'Plug-in Rev Eql,Eq at 65Mhz,SA74140-1065',
            'Jed.mere' => 'kom',
            'količina' => '5'
        ],
        [
            'Material' => '107020',
            'Material Description' => 'Rev amp md 65Mhz,21db gain,SA93144-10621',
            'Jed.mere' => 'kom',
            'količina' => '5'
        ],
        [
            'Material' => '107021',
            'Material Description' => 'Plug-in eql 9db 40-862Mhz,SA74100-10809',
            'Jed.mere' => 'kom',
            'količina' => '5'
        ],
        [
            'Material' => '107137',
            'Material Description' => 'Plug-in atenuator  14 db',
            'Jed.mere' => 'kom',
            'količina' => '5'
        ],
        [
            'Material' => '107138',
            'Material Description' => 'Plug-in atenuator  16 db',
            'Jed.mere' => 'kom',
            'količina' => '5'
        ],
        [
            'Material' => '107142',
            'Material Description' => 'A75130.1085105 Plug in Diplex Filter',
            'Jed.mere' => 'kom',
            'količina' => '5'
        ],
        [
            'Material' => '107144',
            'Material Description' => 'Plug-in Rev Equalizer 85Mhz SA74140-1085',
            'Jed.mere' => 'kom',
            'količina' => '5'
        ],
        [
            'Material' => '108005',
            'Material Description' => 'Kutija metalna 300x350x150',
            'Jed.mere' => 'kom',
            'količina' => '56'
        ],
        [
            'Material' => '108006',
            'Material Description' => 'Kutija metalna 550x450x180',
            'Jed.mere' => 'kom',
            'količina' => '17'
        ],
        [
            'Material' => '108019',
            'Material Description' => 'VVD kutija',
            'Jed.mere' => 'kom',
            'količina' => '93'
        ],
        [
            'Material' => '108021',
            'Material Description' => 'Cokla kanalica metalna 30x50x350',
            'Jed.mere' => 'kom',
            'količina' => '114'
        ],
        [
            'Material' => '108023',
            'Material Description' => 'PVC Kanalica 15x15x2000',
            'Jed.mere' => 'kom',
            'količina' => '1'
        ],
        [
            'Material' => '108024',
            'Material Description' => 'PVC Kanalica 20x30x2000',
            'Jed.mere' => 'kom',
            'količina' => '123'
        ],
        [
            'Material' => '108025',
            'Material Description' => 'PVC Kanalica 40x25x2000',
            'Jed.mere' => 'kom',
            'količina' => '112'
        ],
        [
            'Material' => '108027',
            'Material Description' => 'PSK - 1/300',
            'Jed.mere' => 'kom',
            'količina' => '67'
        ],
        [
            'Material' => '108051',
            'Material Description' => 'PSK - 10',
            'Jed.mere' => 'kom',
            'količina' => '115'
        ],
        [
            'Material' => '108055',
            'Material Description' => 'PSK - 13',
            'Jed.mere' => 'kom',
            'količina' => '10'
        ],
        [
            'Material' => '108056',
            'Material Description' => 'PSK - 16',
            'Jed.mere' => 'kom',
            'količina' => '10'
        ],
        [
            'Material' => '108057',
            'Material Description' => 'PSK - 17',
            'Jed.mere' => 'kom',
            'količina' => '18'
        ],
        [
            'Material' => '108059',
            'Material Description' => 'PSK - 18 M10',
            'Jed.mere' => 'kom',
            'količina' => '170'
        ],
        [
            'Material' => '108063',
            'Material Description' => 'PSK-26/76',
            'Jed.mere' => 'kom',
            'količina' => '14'
        ],
        [
            'Material' => '108064',
            'Material Description' => 'PSK-26/170',
            'Jed.mere' => 'kom',
            'količina' => '1'
        ],
        [
            'Material' => '108065',
            'Material Description' => 'PSK-26/170 - 4kuke',
            'Jed.mere' => 'kom',
            'količina' => '12'
        ],
        [
            'Material' => '108066',
            'Material Description' => 'PSK-26/230',
            'Jed.mere' => 'kom',
            'količina' => '13'
        ],
        [
            'Material' => '108067',
            'Material Description' => 'PSK-26/230 - 4kuke',
            'Jed.mere' => 'kom',
            'količina' => '12'
        ],
        [
            'Material' => '108069',
            'Material Description' => 'Stezaljka 4X16',
            'Jed.mere' => 'kom',
            'količina' => '36'
        ],
        [
            'Material' => '108070',
            'Material Description' => 'Stezaljka ugaona noska',
            'Jed.mere' => 'kom',
            'količina' => '565'
        ],
        [
            'Material' => '108071',
            'Material Description' => 'Kanalica metalna 30X50 Levi nastavak',
            'Jed.mere' => 'kom',
            'količina' => '20'
        ],
        [
            'Material' => '108110',
            'Material Description' => 'Rotor zateza?',
            'Jed.mere' => 'kom',
            'količina' => '226'
        ],
        [
            'Material' => '108113',
            'Material Description' => 'PSK-20 Sidro za beton',
            'Jed.mere' => 'kom',
            'količina' => '380'
        ],
        [
            'Material' => '108114',
            'Material Description' => 'PSK-26/170 sa T nosačem',
            'Jed.mere' => 'kom',
            'količina' => '5'
        ],
        [
            'Material' => '108115',
            'Material Description' => 'PSK-26/230 sa T nosačem',
            'Jed.mere' => 'kom',
            'količina' => '5'
        ],
        [
            'Material' => '108138',
            'Material Description' => 'Jajasti izolator Fi30/40',
            'Jed.mere' => 'kom',
            'količina' => '381'
        ],
        [
            'Material' => '108162',
            'Material Description' => 'Met.kanalica 50x50x2500/1,5mm+2ob',
            'Jed.mere' => 'kom',
            'količina' => '4'
        ],
        [
            'Material' => '108193',
            'Material Description' => 'PSK - 20 / 400 sidro za beton',
            'Jed.mere' => 'kom',
            'količina' => '9'
        ],
        [
            'Material' => '108266',
            'Material Description' => 'Met. kanalica 35x40x2000',
            'Jed.mere' => 'kom',
            'količina' => '152'
        ],
        [
            'Material' => '108267',
            'Material Description' => 'Metalna kanalica 35x40mm Levi',
            'Jed.mere' => 'kom',
            'količina' => '79'
        ],
        [
            'Material' => '108268',
            'Material Description' => 'Cokla kanalica metalna 35x40x350',
            'Jed.mere' => 'kom',
            'količina' => '148'
        ],
        [
            'Material' => '110002',
            'Material Description' => 'Crevo rebrasto  fi 11',
            'Jed.mere' => 'm',
            'količina' => '1375'
        ],
        [
            'Material' => '110003',
            'Material Description' => 'Crevo rebrasto fi 16',
            'Jed.mere' => 'm',
            'količina' => '100'
        ],
        [
            'Material' => '110004',
            'Material Description' => 'Crevo rebrasto fi 23',
            'Jed.mere' => 'm',
            'količina' => '350'
        ],
        [
            'Material' => '110014',
            'Material Description' => 'Osigurač autom. 10A 6KA (brzi)',
            'Jed.mere' => 'kom',
            'količina' => '24'
        ],
        [
            'Material' => '110034',
            'Material Description' => 'Tabla za jedan osigurač',
            'Jed.mere' => 'kom',
            'količina' => '37'
        ],
        [
            'Material' => '110040',
            'Material Description' => 'Traka perforirana ř6',
            'Jed.mere' => 'kom',
            'količina' => '14'
        ],
        [
            'Material' => '110142',
            'Material Description' => 'Utikač šuko',
            'Jed.mere' => 'kom',
            'količina' => '5'
        ],
        [
            'Material' => '110143',
            'Material Description' => 'Utičnica OG',
            'Jed.mere' => 'kom',
            'količina' => '45'
        ],
        [
            'Material' => '111057',
            'Material Description' => 'Remote control CiscoSetTopBox',
            'Jed.mere' => 'kom',
            'količina' => '32'
        ],
        [
            'Material' => '114003',
            'Material Description' => '24 FO Cable Pierlli Cater-D (4x6)',
            'Jed.mere' => 'kom',
            'količina' => '80'
        ],
        [
            'Material' => '114004',
            'Material Description' => 'OpticalCable SMG.657.A1 (4x12)',
            'Jed.mere' => 'kom',
            'količina' => '182'
        ],
        [
            'Material' => '114008',
            'Material Description' => 'Cevčica 45mm',
            'Jed.mere' => 'kom',
            'količina' => '350'
        ],
        [
            'Material' => '114022',
            'Material Description' => 'Pigtail  SC/APC 1.5m',
            'Jed.mere' => 'kom',
            'količina' => '119'
        ],
        [
            'Material' => '114064',
            'Material Description' => 'Simplex Patchcord SC/APC - SC/PC 3m',
            'Jed.mere' => 'kom',
            'količina' => '18'
        ],
        [
            'Material' => '114117',
            'Material Description' => 'Slimpack patch. SC/APC-SC/APC 40m Monkey',
            'Jed.mere' => 'kom',
            'količina' => '23'
        ],
        [
            'Material' => '114118',
            'Material Description' => 'EZ Box Long 12HP 6xSC/APC Duplex 2xPG36',
            'Jed.mere' => 'kom',
            'količina' => '4'
        ],
        [
            'Material' => '116039',
            'Material Description' => 'Cevčica  60 mm',
            'Jed.mere' => 'kom',
            'količina' => '300'
        ],
        [
            'Material' => '116054',
            'Material Description' => 'konektor komp.rg59 mini coax',
            'Jed.mere' => 'kom',
            'količina' => '530'
        ],
        [
            'Material' => '116126',
            'Material Description' => 'Adapter SCART / RCA 3 +SVHS',
            'Jed.mere' => 'kom',
            'količina' => '980'
        ],
        [
            'Material' => '116363',
            'Material Description' => 'Noseća tabla za orman 300x350x150mm',
            'Jed.mere' => 'kom',
            'količina' => '4'
        ],
        [
            'Material' => '116554',
            'Material Description' => 'Kolor kamera AVC687P/F36',
            'Jed.mere' => 'kom',
            'količina' => '8'
        ],
        [
            'Material' => '116635',
            'Material Description' => 'Konektor komp. za RG6',
            'Jed.mere' => 'kom',
            'količina' => '5850'
        ],
        [
            'Material' => '116636',
            'Material Description' => 'Connector compression for RG11',
            'Jed.mere' => 'kom',
            'količina' => '1140'
        ],
        [
            'Material' => '116717',
            'Material Description' => 'Opt.CableRP 96XSMG.657.A1 8x12',
            'Jed.mere' => 'met',
            'količina' => '80'
        ],
        [
            'Material' => '116882',
            'Material Description' => 'Adaptor IEC male - F female PCT-IM-FF',
            'Jed.mere' => 'kom',
            'količina' => '3500'
        ],
        [
            'Material' => '116883',
            'Material Description' => 'Adapter IEC ženski na F ženski PCT-IM-FF',
            'Jed.mere' => 'kom',
            'količina' => '1260'
        ],
        [
            'Material' => '116967',
            'Material Description' => 'OS 1300-B Dect telefon',
            'Jed.mere' => 'kom',
            'količina' => '152'
        ],
        [
            'Material' => '117072',
            'Material Description' => 'PCT-FAM-12 Oslabljivač 12 dB',
            'Jed.mere' => 'kom',
            'količina' => '520'
        ],
        [
            'Material' => '117086',
            'Material Description' => 'Simplex Patchcord LC/APC - SC/APC 7m',
            'Jed.mere' => 'kom',
            'količina' => '57'
        ],
        [
            'Material' => '117087',
            'Material Description' => 'Simplex Patchcord SC/APC - SC/APC 2m',
            'Jed.mere' => 'kom',
            'količina' => '0'
        ],
        [
            'Material' => '117184',
            'Material Description' => 'BEP orm.285x450x70 sa bravicom',
            'Jed.mere' => 'kom',
            'količina' => '11'
        ],
        [
            'Material' => '117195',
            'Material Description' => 'PCT-FAM-08 Oslabljivač 8 dB',
            'Jed.mere' => 'kom',
            'količina' => '1070'
        ],
        [
            'Material' => '117203',
            'Material Description' => 'Install. cableFTTH2fibers indoor',
            'Jed.mere' => 'met',
            'količina' => '312'
        ],
        [
            'Material' => '117295',
            'Material Description' => 'SimplexPatch. SC/APC-SC/APC 5 m',
            'Jed.mere' => 'kom',
            'količina' => '40'
        ],
        [
            'Material' => '117307',
            'Material Description' => 'PSK 26/170 sa nosa?em',
            'Jed.mere' => 'kom',
            'količina' => '4'
        ],
        [
            'Material' => '117308',
            'Material Description' => 'PSK 26/230 sa nosa?em',
            'Jed.mere' => 'kom',
            'količina' => '5'
        ],
        [
            'Material' => '117426',
            'Material Description' => 'Kaseta 24 optička vlakna',
            'Jed.mere' => 'kom',
            'količina' => '57'
        ],
        [
            'Material' => '117479',
            'Material Description' => 'Kabl MDC 24xSM G.657.A1 Micro',
            'Jed.mere' => 'met',
            'količina' => '72'
        ],
        [
            'Material' => '117489',
            'Material Description' => 'PCT-FAM-06 Oslabljivač 6 dB',
            'Jed.mere' => 'kom',
            'količina' => '590'
        ],
        [
            'Material' => '117538',
            'Material Description' => 'Plastični obeleživač za kabl',
            'Jed.mere' => 'kom',
            'količina' => '247'
        ],
        [
            'Material' => '117683',
            'Material Description' => 'Simplex Adapter SC/APC',
            'Jed.mere' => 'kom',
            'količina' => '39'
        ],
        [
            'Material' => '117686',
            'Material Description' => 'Simplex Patchcord SC/APC - SC/APC 1m',
            'Jed.mere' => 'kom',
            'količina' => '19'
        ],
        [
            'Material' => '118558',
            'Material Description' => 'Adapter 90° F male to F female',
            'Jed.mere' => 'kom',
            'količina' => '2810'
        ],
        [
            'Material' => '118561',
            'Material Description' => 'Adapter F male to F male',
            'Jed.mere' => 'kom',
            'količina' => '125'
        ],
        [
            'Material' => '118679',
            'Material Description' => 'SimplexPatch. SC/APC-SC/APC 3m',
            'Jed.mere' => 'kom',
            'količina' => '0'
        ],
        [
            'Material' => '118708',
            'Material Description' => 'Simplex Patchcord LC/APC - SC/APC 5m',
            'Jed.mere' => 'kom',
            'količina' => '2'
        ],
        [
            'Material' => '118725',
            'Material Description' => 'FTP eksterni kabl cat. 5E Draka UC300',
            'Jed.mere' => 'met',
            'količina' => '300'
        ],
        [
            'Material' => '118726',
            'Material Description' => 'RJ-45 konektor kat. 5E 8P8C 8-pinski',
            'Jed.mere' => 'kom',
            'količina' => '4660'
        ],
        [
            'Material' => '118947',
            'Material Description' => 'SFTP ext.samonoseći kabl CAT.5E wall',
            'Jed.mere' => 'met',
            'količina' => '836'
        ],
        [
            'Material' => '119201',
            'Material Description' => 'OG Razvodnik T sa 3 mesta',
            'Jed.mere' => 'kom',
            'količina' => '812'
        ],
        [
            'Material' => '119344',
            'Material Description' => 'RAI-65-06 SHS return path att.65MHz 06dB',
            'Jed.mere' => 'kom',
            'količina' => '3'
        ],
        [
            'Material' => '119345',
            'Material Description' => 'RAI-65-09 SHS return path att.65MHz 09dB',
            'Jed.mere' => 'kom',
            'količina' => '50'
        ],
        [
            'Material' => '119346',
            'Material Description' => 'RAI-65-12 SHS return path att.65MHz 12dB',
            'Jed.mere' => 'kom',
            'količina' => '48'
        ],
        [
            'Material' => '119376',
            'Material Description' => 'RG11 Feed Thru connector & splice block',
            'Jed.mere' => 'kom',
            'količina' => '1'
        ],
        [
            'Material' => '119377',
            'Material Description' => 'PSC-Shrink sleeves for RG11 connesctors',
            'Jed.mere' => 'kom',
            'količina' => '171'
        ],
        [
            'Material' => '119378',
            'Material Description' => 'PCS-RG11-S-6 RG11 drop slice',
            'Jed.mere' => 'kom',
            'količina' => '102'
        ],
        [
            'Material' => '119568',
            'Material Description' => 'Konektor RJ11 (6/49) Telefonski konektor',
            'Jed.mere' => 'kom',
            'količina' => '200'
        ],
        [
            'Material' => '119681',
            'Material Description' => 'Modulator za 66 kanal sa MF mešanjem',
            'Jed.mere' => 'kom',
            'količina' => '6'
        ],
        [
            'Material' => '108079',
            'Material Description' => 'Metalni nosač za rezervu optičkog kabla',
            'Jed.mere' => 'kom',
            'količina' => '3'
        ],
        [
            'Material' => '117472',
            'Material Description' => 'Optički kabl CTC ADSS 12xSM',
            'Jed.mere' => 'met',
            'količina' => '74'
        ],
        [
            'Material' => '119833',
            'Material Description' => '1-Way galvanic isolator GIS',
            'Jed.mere' => 'kom',
            'količina' => '2530'
        ],
        [
            'Material' => '120152',
            'Material Description' => 'Teleste L Amp CX3L085',
            'Jed.mere' => 'kom',
            'količina' => '10'
        ],
        [
            'Material' => '120200',
            'Material Description' => 'OTOut.1xSC/APCada1xSC/APCPigTl MINI ZOK',
            'Jed.mere' => 'kom',
            'količina' => '30'
        ],
        [
            'Material' => '120201',
            'Material Description' => 'Teleste D Amp CX3R085',
            'Jed.mere' => 'kom',
            'količina' => '19'
        ],
        [
            'Material' => '120466',
            'Material Description' => 'Monkey holder for pigt & patchcord',
            'Jed.mere' => 'kom',
            'količina' => '54'
        ],
        [
            'Material' => '120543',
            'Material Description' => 'Filter povratnog smera HPF 105',
            'Jed.mere' => 'kom',
            'količina' => '4460'
        ],
        [
            'Material' => '120555',
            'Material Description' => 'Poklopac kasete 180x130 mm',
            'Jed.mere' => 'kom',
            'količina' => '59'
        ],
        [
            'Material' => '120566',
            'Material Description' => 'Nazid.2redna tabla N2x18C IT',
            'Jed.mere' => 'kom',
            'količina' => '4'
        ],
        [
            'Material' => '120884',
            'Material Description' => 'Sapa crevo plastificirano ø16',
            'Jed.mere' => 'kom',
            'količina' => '100'
        ],
        [
            'Material' => '120980',
            'Material Description' => 'Pigtail SC/APC 30m',
            'Jed.mere' => 'kom',
            'količina' => '0'
        ],
        [
            'Material' => '120983',
            'Material Description' => 'EZBoxLong24HP12xSC/APCDupl.2xPG36',
            'Jed.mere' => 'kom',
            'količina' => '2'
        ],
        [
            'Material' => '120984',
            'Material Description' => 'Neptun fanout indoor12xSC/APC 15m',
            'Jed.mere' => 'kom',
            'količina' => '6'
        ],
        [
            'Material' => '120985',
            'Material Description' => 'Neptun fanout indoor 12xSC/APC 30m',
            'Jed.mere' => 'kom',
            'količina' => '0'
        ],
        [
            'Material' => '120987',
            'Material Description' => 'Neptun fanout indoor24xSC/APC15m',
            'Jed.mere' => 'kom',
            'količina' => '4'
        ],
        [
            'Material' => '120988',
            'Material Description' => 'Nept. fanout indoor24xSC/APC30m',
            'Jed.mere' => 'kom',
            'količina' => '3'
        ],
        [
            'Material' => '121012',
            'Material Description' => 'Optomer FTTH Nevidljivo optičko vlakno',
            'Jed.mere' => 'met',
            'količina' => '305'
        ],
        [
            'Material' => '121013',
            'Material Description' => 'Optomer FTTH štipaljka/spojnica spoljna',
            'Jed.mere' => 'kom',
            'količina' => '7,5'
        ],
        [
            'Material' => '121014',
            'Material Description' => 'OptomerFTTH štipaljka/spojnicaUnutrašnja',
            'Jed.mere' => 'kom',
            'količina' => '5'
        ],
        [
            'Material' => '340091',
            'Material Description' => 'AV cableforEONbox-3,5mm/3xRCA',
            'Jed.mere' => 'kom',
            'količina' => '520'
        ]
        ];


        $brojac = 0;
        foreach ($materijali as $materijal){

            $ware = Wares::where('wares_code', '=', $materijal['Material'])->first();
            //dd($ware->wares_id);
            if(is_object($ware)){
                $warehouse = Warehouse::where('wares_id', '=', $ware->wares_id)->first();
                if(array_key_exists('količina', $materijal)){
                    $warehouse->quantity = $materijal['količina'];
                }else{
                    dd($materijal);
                }
                $warehouse->quantity = $materijal['količina'];
                //dd($warehouse);
                $warehouse->save();

                $brojac = $brojac + 1;
                // dd($warehouse);
            }else{
                dd($materijal);
            }

        }

        dd($brojac);
    }

    public function  ubacivanje_opreme(){
        $oprema_niz = [
            [
                'Šifra' => '990121',
                'Tip' => 'D3 CAM',
                'Model' => 'Modul CAM Conax + SIM kartica',
                'S/N' => '02095962205'
            ],
            [
                'Šifra' => '990121',
                'Tip' => 'D3 CAM',
                'Model' => 'Modul CAM Conax + SIM kartica',
                'S/N' => '02095944477'
            ],
            [
                'Šifra' => '990121',
                'Tip' => 'D3 CAM',
                'Model' => 'Modul CAM Conax + SIM kartica',
                'S/N' => '02095958199'
            ],
            [
                'Šifra' => '990121',
                'Tip' => 'D3 CAM',
                'Model' => 'Modul CAM Conax + SIM kartica',
                'S/N' => '02095968017'
            ],
            [
                'Šifra' => '990121',
                'Tip' => 'D3 CAM',
                'Model' => 'Modul CAM Conax + SIM kartica',
                'S/N' => '02095950703'
            ],
            [
                'Šifra' => '990121',
                'Tip' => 'D3 CAM',
                'Model' => 'Modul CAM Conax + SIM kartica',
                'S/N' => '02095938692'
            ],
            [
                'Šifra' => '990121',
                'Tip' => 'D3 CAM',
                'Model' => 'Modul CAM Conax + SIM kartica',
                'S/N' => '02095953344'
            ],
            [
                'Šifra' => '990121',
                'Tip' => 'D3 CAM',
                'Model' => 'Modul CAM Conax + SIM kartica',
                'S/N' => '02095944375'
            ],
            [
                'Šifra' => '990121',
                'Tip' => 'D3 CAM',
                'Model' => 'Modul CAM Conax + SIM kartica',
                'S/N' => '02095946173'
            ],
            [
                'Šifra' => '990121',
                'Tip' => 'D3 CAM',
                'Model' => 'Modul CAM Conax + SIM kartica',
                'S/N' => '02095960551'
            ],
            [
                'Šifra' => '990121',
                'Tip' => 'D3 CAM',
                'Model' => 'Modul CAM Conax + SIM kartica',
                'S/N' => '02095938762'
            ],
            [
                'Šifra' => '990121',
                'Tip' => 'D3 CAM',
                'Model' => 'Modul CAM Conax + SIM kartica',
                'S/N' => '02095960434'
            ],
            [
                'Šifra' => '990121',
                'Tip' => 'D3 CAM',
                'Model' => 'Modul CAM Conax + SIM kartica',
                'S/N' => '02095935148'
            ],
            [
                'Šifra' => '990121',
                'Tip' => 'D3 CAM',
                'Model' => 'Modul CAM Conax + SIM kartica',
                'S/N' => '02095956597'
            ],
            [
                'Šifra' => '990121',
                'Tip' => 'D3 CAM',
                'Model' => 'Modul CAM Conax + SIM kartica',
                'S/N' => '02095964022'
            ],
            [
                'Šifra' => '990121',
                'Tip' => 'D3 CAM',
                'Model' => 'Modul CAM Conax + SIM kartica',
                'S/N' => '02095942556'
            ],
            [
                'Šifra' => '990121',
                'Tip' => 'D3 CAM',
                'Model' => 'Modul CAM Conax + SIM kartica',
                'S/N' => '02095941826'
            ],
            [
                'Šifra' => '990121',
                'Tip' => 'D3 CAM',
                'Model' => 'Modul CAM Conax + SIM kartica',
                'S/N' => '02095951954'
            ],
            [
                'Šifra' => '990121',
                'Tip' => 'D3 CAM',
                'Model' => 'Modul CAM Conax + SIM kartica',
                'S/N' => '02095938013'
            ],
            [
                'Šifra' => '990121',
                'Tip' => 'D3 CAM',
                'Model' => 'Modul CAM Conax + SIM kartica',
                'S/N' => '02095941217'
            ],
            [
                'Šifra' => '990121',
                'Tip' => 'D3 CAM',
                'Model' => 'Modul CAM Conax + SIM kartica',
                'S/N' => '02095962056'
            ],
            [
                'Šifra' => '990121',
                'Tip' => 'D3 CAM',
                'Model' => 'Modul CAM Conax + SIM kartica',
                'S/N' => '02095955769'
            ],
            [
                'Šifra' => '990121',
                'Tip' => 'D3 CAM',
                'Model' => 'Modul CAM Conax + SIM kartica',
                'S/N' => '02095945778'
            ],
            [
                'Šifra' => '990121',
                'Tip' => 'D3 CAM',
                'Model' => 'Modul CAM Conax + SIM kartica',
                'S/N' => '02095946165'
            ],
            [
                'Šifra' => '990121',
                'Tip' => 'D3 CAM',
                'Model' => 'Modul CAM Conax + SIM kartica',
                'S/N' => '02095965338'
            ],
            [
                'Šifra' => '990121',
                'Tip' => 'D3 CAM',
                'Model' => 'Modul CAM Conax + SIM kartica',
                'S/N' => '02095940073'
            ],
            [
                'Šifra' => '990121',
                'Tip' => 'D3 CAM',
                'Model' => 'Modul CAM Conax + SIM kartica',
                'S/N' => '02095954344'
            ],
            [
                'Šifra' => '990121',
                'Tip' => 'D3 CAM',
                'Model' => 'Modul CAM Conax + SIM kartica',
                'S/N' => '02095965345'
            ],
            [
                'Šifra' => '990121',
                'Tip' => 'D3 CAM',
                'Model' => 'Modul CAM Conax + SIM kartica',
                'S/N' => '02095935250'
            ],
            [
                'Šifra' => '990121',
                'Tip' => 'D3 CAM',
                'Model' => 'Modul CAM Conax + SIM kartica',
                'S/N' => '02095961836'
            ],
            [
                'Šifra' => '990121',
                'Tip' => 'D3 CAM',
                'Model' => 'Modul CAM Conax + SIM kartica',
                'S/N' => '02095940053'
            ],
            [
                'Šifra' => '990121',
                'Tip' => 'D3 CAM',
                'Model' => 'Modul CAM Conax + SIM kartica',
                'S/N' => '02095951788'
            ],
            [
                'Šifra' => '990121',
                'Tip' => 'D3 CAM',
                'Model' => 'Modul CAM Conax + SIM kartica',
                'S/N' => '02095862538'
            ],
            [
                'Šifra' => '990121',
                'Tip' => 'D3 CAM',
                'Model' => 'Modul CAM Conax + SIM kartica',
                'S/N' => '02095957144'
            ],
            [
                'Šifra' => '990121',
                'Tip' => 'D3 CAM',
                'Model' => 'Modul CAM Conax + SIM kartica',
                'S/N' => '02095965851'
            ],
            [
                'Šifra' => '990121',
                'Tip' => 'D3 CAM',
                'Model' => 'Modul CAM Conax + SIM kartica',
                'S/N' => '02095959605'
            ],
            [
                'Šifra' => '990121',
                'Tip' => 'D3 CAM',
                'Model' => 'Modul CAM Conax + SIM kartica',
                'S/N' => '02095941604'
            ],
            [
                'Šifra' => '990121',
                'Tip' => 'D3 CAM',
                'Model' => 'Modul CAM Conax + SIM kartica',
                'S/N' => '02095942208'
            ],
            [
                'Šifra' => '990121',
                'Tip' => 'D3 CAM',
                'Model' => 'Modul CAM Conax + SIM kartica',
                'S/N' => '02095967864'
            ],
            [
                'Šifra' => '990121',
                'Tip' => 'D3 CAM',
                'Model' => 'Modul CAM Conax + SIM kartica',
                'S/N' => '02095945270'
            ],
            [
                'Šifra' => '990121',
                'Tip' => 'D3 CAM',
                'Model' => 'Modul CAM Conax + SIM kartica',
                'S/N' => '02095944194'
            ],
            [
                'Šifra' => '990121',
                'Tip' => 'D3 CAM',
                'Model' => 'Modul CAM Conax + SIM kartica',
                'S/N' => '02095810735'
            ],
            [
                'Šifra' => '990121',
                'Tip' => 'D3 CAM',
                'Model' => 'Modul CAM Conax + SIM kartica',
                'S/N' => '02095945096'
            ],
            [
                'Šifra' => '990121',
                'Tip' => 'D3 CAM',
                'Model' => 'Modul CAM Conax + SIM kartica',
                'S/N' => '02095960917'
            ],
            [
                'Šifra' => '990121',
                'Tip' => 'D3 CAM',
                'Model' => 'Modul CAM Conax + SIM kartica',
                'S/N' => '02095948536'
            ],
            [
                'Šifra' => '990121',
                'Tip' => 'D3 CAM',
                'Model' => 'Modul CAM Conax + SIM kartica',
                'S/N' => '02095965841'
            ],
            [
                'Šifra' => '990121',
                'Tip' => 'D3 CAM',
                'Model' => 'Modul CAM Conax + SIM kartica',
                'S/N' => '02095944445'
            ],
            [
                'Šifra' => '990121',
                'Tip' => 'D3 CAM',
                'Model' => 'Modul CAM Conax + SIM kartica',
                'S/N' => '02095968407'
            ],
            [
                'Šifra' => '990121',
                'Tip' => 'D3 CAM',
                'Model' => 'Modul CAM Conax + SIM kartica',
                'S/N' => '02095943128'
            ],
            [
                'Šifra' => '990121',
                'Tip' => 'D3 CAM',
                'Model' => 'Modul CAM Conax + SIM kartica',
                'S/N' => '02095964021'
            ],
            [
                'Šifra' => '990121',
                'Tip' => 'D3 CAM',
                'Model' => 'Modul CAM Conax + SIM kartica',
                'S/N' => '02095853971'
            ],
            [
                'Šifra' => '990121',
                'Tip' => 'D3 CAM',
                'Model' => 'Modul CAM Conax + SIM kartica',
                'S/N' => '02095934791'
            ],
            [
                'Šifra' => '990121',
                'Tip' => 'D3 CAM',
                'Model' => 'Modul CAM Conax + SIM kartica',
                'S/N' => '02095964399'
            ],
            [
                'Šifra' => '990121',
                'Tip' => 'D3 CAM',
                'Model' => 'Modul CAM Conax + SIM kartica',
                'S/N' => '02095958376'
            ],
            [
                'Šifra' => '990121',
                'Tip' => 'D3 CAM',
                'Model' => 'Modul CAM Conax + SIM kartica',
                'S/N' => '02095967326'
            ],
            [
                'Šifra' => '990121',
                'Tip' => 'D3 CAM',
                'Model' => 'Modul CAM Conax + SIM kartica',
                'S/N' => '02095965838'
            ],
            [
                'Šifra' => '990121',
                'Tip' => 'D3 CAM',
                'Model' => 'Modul CAM Conax + SIM kartica',
                'S/N' => '02095955448'
            ],
            [
                'Šifra' => '990121',
                'Tip' => 'D3 CAM',
                'Model' => 'Modul CAM Conax + SIM kartica',
                'S/N' => '02095948223'
            ],
            [
                'Šifra' => '990121',
                'Tip' => 'D3 CAM',
                'Model' => 'Modul CAM Conax + SIM kartica',
                'S/N' => '02095950748'
            ],
            [
                'Šifra' => '990121',
                'Tip' => 'D3 CAM',
                'Model' => 'Modul CAM Conax + SIM kartica',
                'S/N' => '02095941632'
            ],
            [
                'Šifra' => '990121',
                'Tip' => 'D3 CAM',
                'Model' => 'Modul CAM Conax + SIM kartica',
                'S/N' => '02095966010'
            ],
            [
                'Šifra' => '990121',
                'Tip' => 'D3 CAM',
                'Model' => 'Modul CAM Conax + SIM kartica',
                'S/N' => '02095947711'
            ],
            [
                'Šifra' => '990121',
                'Tip' => 'D3 CAM',
                'Model' => 'Modul CAM Conax + SIM kartica',
                'S/N' => '02095946753'
            ],
            [
                'Šifra' => '990121',
                'Tip' => 'D3 CAM',
                'Model' => 'Modul CAM Conax + SIM kartica',
                'S/N' => '02095862916'
            ],
            [
                'Šifra' => '990121',
                'Tip' => 'D3 CAM',
                'Model' => 'Modul CAM Conax + SIM kartica',
                'S/N' => '02095967064'
            ],
            [
                'Šifra' => '990121',
                'Tip' => 'D3 CAM',
                'Model' => 'Modul CAM Conax + SIM kartica',
                'S/N' => '02095968109'
            ],
            [
                'Šifra' => '990121',
                'Tip' => 'D3 CAM',
                'Model' => 'Modul CAM Conax + SIM kartica',
                'S/N' => '02095954574'
            ],
            [
                'Šifra' => '990121',
                'Tip' => 'D3 CAM',
                'Model' => 'Modul CAM Conax + SIM kartica',
                'S/N' => '02095935784'
            ],
            [
                'Šifra' => '990121',
                'Tip' => 'D3 CAM',
                'Model' => 'Modul CAM Conax + SIM kartica',
                'S/N' => '02095935631'
            ],
            [
                'Šifra' => '990121',
                'Tip' => 'D3 CAM',
                'Model' => 'Modul CAM Conax + SIM kartica',
                'S/N' => '02095948731'
            ],
            [
                'Šifra' => '990121',
                'Tip' => 'D3 CAM',
                'Model' => 'Modul CAM Conax + SIM kartica',
                'S/N' => '02095876676'
            ],
            [
                'Šifra' => '990121',
                'Tip' => 'D3 CAM',
                'Model' => 'Modul CAM Conax + SIM kartica',
                'S/N' => '02095941316'
            ],
            [
                'Šifra' => '990121',
                'Tip' => 'D3 CAM',
                'Model' => 'Modul CAM Conax + SIM kartica',
                'S/N' => '02095942069'
            ],
            [
                'Šifra' => '990121',
                'Tip' => 'D3 CAM',
                'Model' => 'Modul CAM Conax + SIM kartica',
                'S/N' => '02095935910'
            ],
            [
                'Šifra' => '990121',
                'Tip' => 'D3 CAM',
                'Model' => 'Modul CAM Conax + SIM kartica',
                'S/N' => '02161720212'
            ],
            [
                'Šifra' => '990121',
                'Tip' => 'D3 CAM',
                'Model' => 'Modul CAM Conax + SIM kartica',
                'S/N' => '02095953288'
            ],
            [
                'Šifra' => '990121',
                'Tip' => 'D3 CAM',
                'Model' => 'Modul CAM Conax + SIM kartica',
                'S/N' => '02095876628'
            ],
            [
                'Šifra' => '990121',
                'Tip' => 'D3 CAM',
                'Model' => 'Modul CAM Conax + SIM kartica',
                'S/N' => '02095927003'
            ],
            [
                'Šifra' => '990121',
                'Tip' => 'D3 CAM',
                'Model' => 'Modul CAM Conax + SIM kartica',
                'S/N' => '02095810865'
            ],
            [
                'Šifra' => '990121',
                'Tip' => 'D3 CAM',
                'Model' => 'Modul CAM Conax + SIM kartica',
                'S/N' => '02095904773'
            ],
            [
                'Šifra' => '990121',
                'Tip' => 'D3 CAM',
                'Model' => 'Modul CAM Conax + SIM kartica',
                'S/N' => '02095979036'
            ],
            [
                'Šifra' => '990121',
                'Tip' => 'D3 CAM',
                'Model' => 'Modul CAM Conax + SIM kartica',
                'S/N' => '02161722854'
            ],
            [
                'Šifra' => '990121',
                'Tip' => 'D3 CAM',
                'Model' => 'Modul CAM Conax + SIM kartica',
                'S/N' => '02161726055'
            ],
            [
                'Šifra' => '990121',
                'Tip' => 'D3 CAM',
                'Model' => 'Modul CAM Conax + SIM kartica',
                'S/N' => '02095941202'
            ],
            [
                'Šifra' => '990121',
                'Tip' => 'D3 CAM',
                'Model' => 'Modul CAM Conax + SIM kartica',
                'S/N' => '02095806383'
            ],
            [
                'Šifra' => '990121',
                'Tip' => 'D3 CAM',
                'Model' => 'Modul CAM Conax + SIM kartica',
                'S/N' => '02095935139'
            ],
            [
                'Šifra' => '990121',
                'Tip' => 'D3 CAM',
                'Model' => 'Modul CAM Conax + SIM kartica',
                'S/N' => '02161724961'
            ],
            [
                'Šifra' => '990121',
                'Tip' => 'D3 CAM',
                'Model' => 'Modul CAM Conax + SIM kartica',
                'S/N' => '02095951210'
            ],
            [
                'Šifra' => '990121',
                'Tip' => 'D3 CAM',
                'Model' => 'Modul CAM Conax + SIM kartica',
                'S/N' => '02095812280'
            ],
            [
                'Šifra' => '990121',
                'Tip' => 'D3 CAM',
                'Model' => 'Modul CAM Conax + SIM kartica',
                'S/N' => '02095987298'
            ],
            [
                'Šifra' => '990121',
                'Tip' => 'D3 CAM',
                'Model' => 'Modul CAM Conax + SIM kartica',
                'S/N' => '02095935372'
            ],
            [
                'Šifra' => '990121',
                'Tip' => 'D3 CAM',
                'Model' => 'Modul CAM Conax + SIM kartica',
                'S/N' => '02095810579'
            ],
            [
                'Šifra' => '990121',
                'Tip' => 'D3 CAM',
                'Model' => 'Modul CAM Conax + SIM kartica',
                'S/N' => '02095986044'
            ],
            [
                'Šifra' => '990121',
                'Tip' => 'D3 CAM',
                'Model' => 'Modul CAM Conax + SIM kartica',
                'S/N' => '02095875557'
            ],
            [
                'Šifra' => '990121',
                'Tip' => 'D3 CAM',
                'Model' => 'Modul CAM Conax + SIM kartica',
                'S/N' => '02095983632'
            ],
            [
                'Šifra' => '990121',
                'Tip' => 'D3 CAM',
                'Model' => 'Modul CAM Conax + SIM kartica',
                'S/N' => '02095984702'
            ],
            [
                'Šifra' => '990121',
                'Tip' => 'D3 CAM',
                'Model' => 'Modul CAM Conax + SIM kartica',
                'S/N' => '02095853577'
            ],
            [
                'Šifra' => '990121',
                'Tip' => 'D3 CAM',
                'Model' => 'Modul CAM Conax + SIM kartica',
                'S/N' => '02095946639'
            ],
            [
                'Šifra' => '990121',
                'Tip' => 'D3 CAM',
                'Model' => 'Modul CAM Conax + SIM kartica',
                'S/N' => '02095947459'
            ],
            [
                'Šifra' => '990121',
                'Tip' => 'D3 CAM',
                'Model' => 'Modul CAM Conax + SIM kartica',
                'S/N' => '02095857713'
            ],
            [
                'Šifra' => '990121',
                'Tip' => 'D3 CAM',
                'Model' => 'Modul CAM Conax + SIM kartica',
                'S/N' => '02095941260'
            ],
            [
                'Šifra' => '990121',
                'Tip' => 'D3 CAM',
                'Model' => 'Modul CAM Conax + SIM kartica',
                'S/N' => '02095876185'
            ],
            [
                'Šifra' => '990121',
                'Tip' => 'D3 CAM',
                'Model' => 'Modul CAM Conax + SIM kartica',
                'S/N' => '02095963669'
            ],
            [
                'Šifra' => '990121',
                'Tip' => 'D3 CAM',
                'Model' => 'Modul CAM Conax + SIM kartica',
                'S/N' => '02095875397'
            ],
            [
                'Šifra' => '990121',
                'Tip' => 'D3 CAM',
                'Model' => 'Modul CAM Conax + SIM kartica',
                'S/N' => '02095979967'
            ],
            [
                'Šifra' => '990121',
                'Tip' => 'D3 CAM',
                'Model' => 'Modul CAM Conax + SIM kartica',
                'S/N' => '02095876840'
            ],
            [
                'Šifra' => '990121',
                'Tip' => 'D3 CAM',
                'Model' => 'Modul CAM Conax + SIM kartica',
                'S/N' => '02095853291'
            ],
            [
                'Šifra' => '990121',
                'Tip' => 'D3 CAM',
                'Model' => 'Modul CAM Conax + SIM kartica',
                'S/N' => '02095875289'
            ],
            [
                'Šifra' => '990121',
                'Tip' => 'D3 CAM',
                'Model' => 'Modul CAM Conax + SIM kartica',
                'S/N' => '02095949723'
            ],
            [
                'Šifra' => '990121',
                'Tip' => 'D3 CAM',
                'Model' => 'Modul CAM Conax + SIM kartica',
                'S/N' => '02095946786'
            ],
            [
                'Šifra' => '990121',
                'Tip' => 'D3 CAM',
                'Model' => 'Modul CAM Conax + SIM kartica',
                'S/N' => '02095982506'
            ],
            [
                'Šifra' => '990121',
                'Tip' => 'D3 CAM',
                'Model' => 'Modul CAM Conax + SIM kartica',
                'S/N' => '02095860416'
            ],
            [
                'Šifra' => '990121',
                'Tip' => 'D3 CAM',
                'Model' => 'Modul CAM Conax + SIM kartica',
                'S/N' => '02095981722'
            ],
            [
                'Šifra' => '990121',
                'Tip' => 'D3 CAM',
                'Model' => 'Modul CAM Conax + SIM kartica',
                'S/N' => '02095934365'
            ],
            [
                'Šifra' => '990121',
                'Tip' => 'D3 CAM',
                'Model' => 'Modul CAM Conax + SIM kartica',
                'S/N' => '02095934801'
            ],
            [
                'Šifra' => '990121',
                'Tip' => 'D3 CAM',
                'Model' => 'Modul CAM Conax + SIM kartica',
                'S/N' => '02095858369'
            ],
            [
                'Šifra' => '990121',
                'Tip' => 'D3 CAM',
                'Model' => 'Modul CAM Conax + SIM kartica',
                'S/N' => '02095812001'
            ],
            [
                'Šifra' => '990121',
                'Tip' => 'D3 CAM',
                'Model' => 'Modul CAM Conax + SIM kartica',
                'S/N' => '02095959599'
            ],
            [
                'Šifra' => '990121',
                'Tip' => 'D3 CAM',
                'Model' => 'Modul CAM Conax + SIM kartica',
                'S/N' => '02095980129'
            ],
            [
                'Šifra' => '990121',
                'Tip' => 'D3 CAM',
                'Model' => 'Modul CAM Conax + SIM kartica',
                'S/N' => '02095950813'
            ],
            [
                'Šifra' => '990121',
                'Tip' => 'D3 CAM',
                'Model' => 'Modul CAM Conax + SIM kartica',
                'S/N' => '02095941069'
            ],
            [
                'Šifra' => '990121',
                'Tip' => 'D3 CAM',
                'Model' => 'Modul CAM Conax + SIM kartica',
                'S/N' => '02095808647'
            ],
            [
                'Šifra' => '990121',
                'Tip' => 'D3 CAM',
                'Model' => 'Modul CAM Conax + SIM kartica',
                'S/N' => '02095976807'
            ],
            [
                'Šifra' => '990121',
                'Tip' => 'D3 CAM',
                'Model' => 'Modul CAM Conax + SIM kartica',
                'S/N' => '02095944651'
            ],
            [
                'Šifra' => '990121',
                'Tip' => 'D3 CAM',
                'Model' => 'Modul CAM Conax + SIM kartica',
                'S/N' => '02095812062'
            ],
            [
                'Šifra' => '990121',
                'Tip' => 'D3 CAM',
                'Model' => 'Modul CAM Conax + SIM kartica',
                'S/N' => '02095957732'
            ],
            [
                'Šifra' => '990121',
                'Tip' => 'D3 CAM',
                'Model' => 'Modul CAM Conax + SIM kartica',
                'S/N' => '02095937879'
            ],
            [
                'Šifra' => '990121',
                'Tip' => 'D3 CAM',
                'Model' => 'Modul CAM Conax + SIM kartica',
                'S/N' => '02095967948'
            ],
            [
                'Šifra' => '990121',
                'Tip' => 'D3 CAM',
                'Model' => 'Modul CAM Conax + SIM kartica',
                'S/N' => '02095863053'
            ],
            [
                'Šifra' => '990121',
                'Tip' => 'D3 CAM',
                'Model' => 'Modul CAM Conax + SIM kartica',
                'S/N' => '02095810810'
            ],
            [
                'Šifra' => '990121',
                'Tip' => 'D3 CAM',
                'Model' => 'Modul CAM Conax + SIM kartica',
                'S/N' => '02095982920'
            ],
            [
                'Šifra' => '990121',
                'Tip' => 'D3 CAM',
                'Model' => 'Modul CAM Conax + SIM kartica',
                'S/N' => '02095938702'
            ],
            [
                'Šifra' => '990121',
                'Tip' => 'D3 CAM',
                'Model' => 'Modul CAM Conax + SIM kartica',
                'S/N' => '02095978470'
            ],
            [
                'Šifra' => '990121',
                'Tip' => 'D3 CAM',
                'Model' => 'Modul CAM Conax + SIM kartica',
                'S/N' => '02095984273'
            ],
            [
                'Šifra' => '990121',
                'Tip' => 'D3 CAM',
                'Model' => 'Modul CAM Conax + SIM kartica',
                'S/N' => '02095962181'
            ],
            [
                'Šifra' => '990121',
                'Tip' => 'D3 CAM',
                'Model' => 'Modul CAM Conax + SIM kartica',
                'S/N' => '02095941389'
            ],
            [
                'Šifra' => '990121',
                'Tip' => 'D3 CAM',
                'Model' => 'Modul CAM Conax + SIM kartica',
                'S/N' => '02095875706'
            ],
            [
                'Šifra' => '990121',
                'Tip' => 'D3 CAM',
                'Model' => 'Modul CAM Conax + SIM kartica',
                'S/N' => '02095962759'
            ],
            [
                'Šifra' => '990121',
                'Tip' => 'D3 CAM',
                'Model' => 'Modul CAM Conax + SIM kartica',
                'S/N' => '02095945854'
            ],
            [
                'Šifra' => '990121',
                'Tip' => 'D3 CAM',
                'Model' => 'Modul CAM Conax + SIM kartica',
                'S/N' => '02095968094'
            ],
            [
                'Šifra' => '990121',
                'Tip' => 'D3 CAM',
                'Model' => 'Modul CAM Conax + SIM kartica',
                'S/N' => '02095811810'
            ],
            [
                'Šifra' => '990121',
                'Tip' => 'D3 CAM',
                'Model' => 'Modul CAM Conax + SIM kartica',
                'S/N' => '02095934744'
            ],
            [
                'Šifra' => '990121',
                'Tip' => 'D3 CAM',
                'Model' => 'Modul CAM Conax + SIM kartica',
                'S/N' => '02095812301'
            ],
            [
                'Šifra' => '990121',
                'Tip' => 'D3 CAM',
                'Model' => 'Modul CAM Conax + SIM kartica',
                'S/N' => '02095858925'
            ],
            [
                'Šifra' => '990121',
                'Tip' => 'D3 CAM',
                'Model' => 'Modul CAM Conax + SIM kartica',
                'S/N' => '02095905565'
            ],
            [
                'Šifra' => '990121',
                'Tip' => 'D3 CAM',
                'Model' => 'Modul CAM Conax + SIM kartica',
                'S/N' => '02095975405'
            ],
            [
                'Šifra' => '990121',
                'Tip' => 'D3 CAM',
                'Model' => 'Modul CAM Conax + SIM kartica',
                'S/N' => '02095966051'
            ],
            [
                'Šifra' => '990121',
                'Tip' => 'D3 CAM',
                'Model' => 'Modul CAM Conax + SIM kartica',
                'S/N' => '02095986634'
            ],
            [
                'Šifra' => '990121',
                'Tip' => 'D3 CAM',
                'Model' => 'Modul CAM Conax + SIM kartica',
                'S/N' => '02095979797'
            ],
            [
                'Šifra' => '990121',
                'Tip' => 'D3 CAM',
                'Model' => 'Modul CAM Conax + SIM kartica',
                'S/N' => '02095986411'
            ],
            [
                'Šifra' => '990121',
                'Tip' => 'D3 CAM',
                'Model' => 'Modul CAM Conax + SIM kartica',
                'S/N' => '02161748317'
            ],
            [
                'Šifra' => '990121',
                'Tip' => 'D3 CAM',
                'Model' => 'Modul CAM Conax + SIM kartica',
                'S/N' => '02095984949'
            ],
            [
                'Šifra' => '990121',
                'Tip' => 'D3 CAM',
                'Model' => 'Modul CAM Conax + SIM kartica',
                'S/N' => '02095810673'
            ],
            [
                'Šifra' => '990121',
                'Tip' => 'D3 CAM',
                'Model' => 'Modul CAM Conax + SIM kartica',
                'S/N' => '02095949123'
            ],
            [
                'Šifra' => '990121',
                'Tip' => 'D3 CAM',
                'Model' => 'Modul CAM Conax + SIM kartica',
                'S/N' => '02095976973'
            ],
            [
                'Šifra' => '990121',
                'Tip' => 'D3 CAM',
                'Model' => 'Modul CAM Conax + SIM kartica',
                'S/N' => '02095903609'
            ],
            [
                'Šifra' => '990121',
                'Tip' => 'D3 CAM',
                'Model' => 'Modul CAM Conax + SIM kartica',
                'S/N' => '02095955082'
            ],
            [
                'Šifra' => '990121',
                'Tip' => 'D3 CAM',
                'Model' => 'Modul CAM Conax + SIM kartica',
                'S/N' => '02095946692'
            ],
            [
                'Šifra' => '990121',
                'Tip' => 'D3 CAM',
                'Model' => 'Modul CAM Conax + SIM kartica',
                'S/N' => '02095978231'
            ],
            [
                'Šifra' => '990121',
                'Tip' => 'D3 CAM',
                'Model' => 'Modul CAM Conax + SIM kartica',
                'S/N' => '02095986016'
            ],
            [
                'Šifra' => '990121',
                'Tip' => 'D3 CAM',
                'Model' => 'Modul CAM Conax + SIM kartica',
                'S/N' => '02095906220'
            ],
            [
                'Šifra' => '990121',
                'Tip' => 'D3 CAM',
                'Model' => 'Modul CAM Conax + SIM kartica',
                'S/N' => '02095943766'
            ],
            [
                'Šifra' => '990121',
                'Tip' => 'D3 CAM',
                'Model' => 'Modul CAM Conax + SIM kartica',
                'S/N' => '02161726380'
            ],
            [
                'Šifra' => '990121',
                'Tip' => 'D3 CAM',
                'Model' => 'Modul CAM Conax + SIM kartica',
                'S/N' => '02095979842'
            ],
            [
                'Šifra' => '990121',
                'Tip' => 'D3 CAM',
                'Model' => 'Modul CAM Conax + SIM kartica',
                'S/N' => '02095942944'
            ],
            [
                'Šifra' => '990121',
                'Tip' => 'D3 CAM',
                'Model' => 'Modul CAM Conax + SIM kartica',
                'S/N' => '02161725860'
            ],
            [
                'Šifra' => '990121',
                'Tip' => 'D3 CAM',
                'Model' => 'Modul CAM Conax + SIM kartica',
                'S/N' => '02095979450'
            ],
            [
                'Šifra' => '990121',
                'Tip' => 'D3 CAM',
                'Model' => 'Modul CAM Conax + SIM kartica',
                'S/N' => '02095955325'
            ],
            [
                'Šifra' => '990121',
                'Tip' => 'D3 CAM',
                'Model' => 'Modul CAM Conax + SIM kartica',
                'S/N' => '02095976846'
            ],
            [
                'Šifra' => '990121',
                'Tip' => 'D3 CAM',
                'Model' => 'Modul CAM Conax + SIM kartica',
                'S/N' => '02095961831'
            ],
            [
                'Šifra' => '990121',
                'Tip' => 'D3 CAM',
                'Model' => 'Modul CAM Conax + SIM kartica',
                'S/N' => '02095975218'
            ],
            [
                'Šifra' => '990121',
                'Tip' => 'D3 CAM',
                'Model' => 'Modul CAM Conax + SIM kartica',
                'S/N' => '02095975039'
            ],
            [
                'Šifra' => '990121',
                'Tip' => 'D3 CAM',
                'Model' => 'Modul CAM Conax + SIM kartica',
                'S/N' => '02161724133'
            ],
            [
                'Šifra' => '990121',
                'Tip' => 'D3 CAM',
                'Model' => 'Modul CAM Conax + SIM kartica',
                'S/N' => '02095813334'
            ],
            [
                'Šifra' => '990121',
                'Tip' => 'D3 CAM',
                'Model' => 'Modul CAM Conax + SIM kartica',
                'S/N' => '02095811429'
            ],
            [
                'Šifra' => '990121',
                'Tip' => 'D3 CAM',
                'Model' => 'Modul CAM Conax + SIM kartica',
                'S/N' => '02095987387'
            ],
            [
                'Šifra' => '990121',
                'Tip' => 'D3 CAM',
                'Model' => 'Modul CAM Conax + SIM kartica',
                'S/N' => '02095956188'
            ],
            [
                'Šifra' => '990121',
                'Tip' => 'D3 CAM',
                'Model' => 'Modul CAM Conax + SIM kartica',
                'S/N' => '02095984259'
            ],
            [
                'Šifra' => '990121',
                'Tip' => 'D3 CAM',
                'Model' => 'Modul CAM Conax + SIM kartica',
                'S/N' => '02095966640'
            ],
            [
                'Šifra' => '990121',
                'Tip' => 'D3 CAM',
                'Model' => 'Modul CAM Conax + SIM kartica',
                'S/N' => '02161725571'
            ],
            [
                'Šifra' => '990121',
                'Tip' => 'D3 CAM',
                'Model' => 'Modul CAM Conax + SIM kartica',
                'S/N' => '02095979873'
            ],
            [
                'Šifra' => '990121',
                'Tip' => 'D3 CAM',
                'Model' => 'Modul CAM Conax + SIM kartica',
                'S/N' => '02095977823'
            ],
            [
                'Šifra' => '990121',
                'Tip' => 'D3 CAM',
                'Model' => 'Modul CAM Conax + SIM kartica',
                'S/N' => '02095961844'
            ],
            [
                'Šifra' => '990121',
                'Tip' => 'D3 CAM',
                'Model' => 'Modul CAM Conax + SIM kartica',
                'S/N' => '02095986113'
            ],
            [
                'Šifra' => '990121',
                'Tip' => 'D3 CAM',
                'Model' => 'Modul CAM Conax + SIM kartica',
                'S/N' => '02095978415'
            ],
            [
                'Šifra' => '990121',
                'Tip' => 'D3 CAM',
                'Model' => 'Modul CAM Conax + SIM kartica',
                'S/N' => '02095965004'
            ],
            [
                'Šifra' => '990121',
                'Tip' => 'D3 CAM',
                'Model' => 'Modul CAM Conax + SIM kartica',
                'S/N' => '02095853283'
            ],
            [
                'Šifra' => '990121',
                'Tip' => 'D3 CAM',
                'Model' => 'Modul CAM Conax + SIM kartica',
                'S/N' => '02095976748'
            ],
            [
                'Šifra' => '990121',
                'Tip' => 'D3 CAM',
                'Model' => 'Modul CAM Conax + SIM kartica',
                'S/N' => '02095980003'
            ],
            [
                'Šifra' => '990121',
                'Tip' => 'D3 CAM',
                'Model' => 'Modul CAM Conax + SIM kartica',
                'S/N' => '02095965648'
            ],
            [
                'Šifra' => '990121',
                'Tip' => 'D3 CAM',
                'Model' => 'Modul CAM Conax + SIM kartica',
                'S/N' => '02161748121'
            ],
            [
                'Šifra' => '990121',
                'Tip' => 'D3 CAM',
                'Model' => 'Modul CAM Conax + SIM kartica',
                'S/N' => '02095988852'
            ],
            [
                'Šifra' => '990121',
                'Tip' => 'D3 CAM',
                'Model' => 'Modul CAM Conax + SIM kartica',
                'S/N' => '02095987342'
            ],
            [
                'Šifra' => '990121',
                'Tip' => 'D3 CAM',
                'Model' => 'Modul CAM Conax + SIM kartica',
                'S/N' => '02095926511'
            ],
            [
                'Šifra' => '990121',
                'Tip' => 'D3 CAM',
                'Model' => 'Modul CAM Conax + SIM kartica',
                'S/N' => '02095984333'
            ],
            [
                'Šifra' => '990121',
                'Tip' => 'D3 CAM',
                'Model' => 'Modul CAM Conax + SIM kartica',
                'S/N' => '02095955782'
            ],
            [
                'Šifra' => '990121',
                'Tip' => 'D3 CAM',
                'Model' => 'Modul CAM Conax + SIM kartica',
                'S/N' => '02095935176'
            ],
            [
                'Šifra' => '990121',
                'Tip' => 'D3 CAM',
                'Model' => 'Modul CAM Conax + SIM kartica',
                'S/N' => '02095951497'
            ],
            [
                'Šifra' => '990121',
                'Tip' => 'D3 CAM',
                'Model' => 'Modul CAM Conax + SIM kartica',
                'S/N' => '02095980601'
            ],
            [
                'Šifra' => '990121',
                'Tip' => 'D3 CAM',
                'Model' => 'Modul CAM Conax + SIM kartica',
                'S/N' => '02095948060'
            ],
            [
                'Šifra' => '990121',
                'Tip' => 'D3 CAM',
                'Model' => 'Modul CAM Conax + SIM kartica',
                'S/N' => '02095955212'
            ],
            [
                'Šifra' => '990121',
                'Tip' => 'D3 CAM',
                'Model' => 'Modul CAM Conax + SIM kartica',
                'S/N' => '02095950090'
            ],
            [
                'Šifra' => '990121',
                'Tip' => 'D3 CAM',
                'Model' => 'Modul CAM Conax + SIM kartica',
                'S/N' => '02095876971'
            ],
            [
                'Šifra' => '990121',
                'Tip' => 'D3 CAM',
                'Model' => 'Modul CAM Conax + SIM kartica',
                'S/N' => '02095986193'
            ],
            [
                'Šifra' => '990121',
                'Tip' => 'D3 CAM',
                'Model' => 'Modul CAM Conax + SIM kartica',
                'S/N' => '02095953897'
            ],
            [
                'Šifra' => '990121',
                'Tip' => 'D3 CAM',
                'Model' => 'Modul CAM Conax + SIM kartica',
                'S/N' => '02095941078'
            ],
            [
                'Šifra' => '990121',
                'Tip' => 'D3 CAM',
                'Model' => 'Modul CAM Conax + SIM kartica',
                'S/N' => '02095807909'
            ],
            [
                'Šifra' => '990121',
                'Tip' => 'D3 CAM',
                'Model' => 'Modul CAM Conax + SIM kartica',
                'S/N' => '02095939724'
            ],
            [
                'Šifra' => '990121',
                'Tip' => 'D3 CAM',
                'Model' => 'Modul CAM Conax + SIM kartica',
                'S/N' => '02161725827'
            ],
            [
                'Šifra' => '990121',
                'Tip' => 'D3 CAM',
                'Model' => 'Modul CAM Conax + SIM kartica',
                'S/N' => '02095958723'
            ],
            [
                'Šifra' => '990121',
                'Tip' => 'D3 CAM',
                'Model' => 'Modul CAM Conax + SIM kartica',
                'S/N' => '02095807966'
            ],
            [
                'Šifra' => '990121',
                'Tip' => 'D3 CAM',
                'Model' => 'Modul CAM Conax + SIM kartica',
                'S/N' => '02095940280'
            ],
            [
                'Šifra' => '990121',
                'Tip' => 'D3 CAM',
                'Model' => 'Modul CAM Conax + SIM kartica',
                'S/N' => '02095982230'
            ],
            [
                'Šifra' => '990121',
                'Tip' => 'D3 CAM',
                'Model' => 'Modul CAM Conax + SIM kartica',
                'S/N' => '02095953560'
            ],
            [
                'Šifra' => '990121',
                'Tip' => 'D3 CAM',
                'Model' => 'Modul CAM Conax + SIM kartica',
                'S/N' => '02095975967'
            ],
            [
                'Šifra' => '990121',
                'Tip' => 'D3 CAM',
                'Model' => 'Modul CAM Conax + SIM kartica',
                'S/N' => '02095906491'
            ],
            [
                'Šifra' => '990121',
                'Tip' => 'D3 CAM',
                'Model' => 'Modul CAM Conax + SIM kartica',
                'S/N' => '02161722668'
            ],
            [
                'Šifra' => '990121',
                'Tip' => 'D3 CAM',
                'Model' => 'Modul CAM Conax + SIM kartica',
                'S/N' => '02095957386'
            ],
            [
                'Šifra' => '990121',
                'Tip' => 'D3 CAM',
                'Model' => 'Modul CAM Conax + SIM kartica',
                'S/N' => '02161721232'
            ],
            [
                'Šifra' => '990121',
                'Tip' => 'D3 CAM',
                'Model' => 'Modul CAM Conax + SIM kartica',
                'S/N' => '02095986032'
            ],
            [
                'Šifra' => '990121',
                'Tip' => 'D3 CAM',
                'Model' => 'Modul CAM Conax + SIM kartica',
                'S/N' => '02095982923'
            ],
            [
                'Šifra' => '990121',
                'Tip' => 'D3 CAM',
                'Model' => 'Modul CAM Conax + SIM kartica',
                'S/N' => '02095965459'
            ],
            [
                'Šifra' => '990121',
                'Tip' => 'D3 CAM',
                'Model' => 'Modul CAM Conax + SIM kartica',
                'S/N' => '02095950810'
            ],
            [
                'Šifra' => '990121',
                'Tip' => 'D3 CAM',
                'Model' => 'Modul CAM Conax + SIM kartica',
                'S/N' => '02095940312'
            ],
            [
                'Šifra' => '990121',
                'Tip' => 'D3 CAM',
                'Model' => 'Modul CAM Conax + SIM kartica',
                'S/N' => '02095985009'
            ],
            [
                'Šifra' => '990121',
                'Tip' => 'D3 CAM',
                'Model' => 'Modul CAM Conax + SIM kartica',
                'S/N' => '02095955979'
            ],
            [
                'Šifra' => '990121',
                'Tip' => 'D3 CAM',
                'Model' => 'Modul CAM Conax + SIM kartica',
                'S/N' => '02095967325'
            ],
            [
                'Šifra' => '990121',
                'Tip' => 'D3 CAM',
                'Model' => 'Modul CAM Conax + SIM kartica',
                'S/N' => '02095937761'
            ],
            [
                'Šifra' => '990121',
                'Tip' => 'D3 CAM',
                'Model' => 'Modul CAM Conax + SIM kartica',
                'S/N' => '02095943287'
            ],
            [
                'Šifra' => '990121',
                'Tip' => 'D3 CAM',
                'Model' => 'Modul CAM Conax + SIM kartica',
                'S/N' => '02095960317'
            ],
            [
                'Šifra' => '990121',
                'Tip' => 'D3 CAM',
                'Model' => 'Modul CAM Conax + SIM kartica',
                'S/N' => '02095984501'
            ],
            [
                'Šifra' => '990121',
                'Tip' => 'D3 CAM',
                'Model' => 'Modul CAM Conax + SIM kartica',
                'S/N' => '02095941010'
            ],
            [
                'Šifra' => '990121',
                'Tip' => 'D3 CAM',
                'Model' => 'Modul CAM Conax + SIM kartica',
                'S/N' => '02095977495'
            ],
            [
                'Šifra' => '990121',
                'Tip' => 'D3 CAM',
                'Model' => 'Modul CAM Conax + SIM kartica',
                'S/N' => '02095962197'
            ],
            [
                'Šifra' => '990121',
                'Tip' => 'D3 CAM',
                'Model' => 'Modul CAM Conax + SIM kartica',
                'S/N' => '02095812131'
            ],
            [
                'Šifra' => '990121',
                'Tip' => 'D3 CAM',
                'Model' => 'Modul CAM Conax + SIM kartica',
                'S/N' => '02095924856'
            ],
            [
                'Šifra' => '990121',
                'Tip' => 'D3 CAM',
                'Model' => 'Modul CAM Conax + SIM kartica',
                'S/N' => '02095979454'
            ],
            [
                'Šifra' => '990121',
                'Tip' => 'D3 CAM',
                'Model' => 'Modul CAM Conax + SIM kartica',
                'S/N' => '02161722493'
            ],
            [
                'Šifra' => '990121',
                'Tip' => 'D3 CAM',
                'Model' => 'Modul CAM Conax + SIM kartica',
                'S/N' => '02161724948'
            ],
            [
                'Šifra' => '990121',
                'Tip' => 'D3 CAM',
                'Model' => 'Modul CAM Conax + SIM kartica',
                'S/N' => '02095987178'
            ],
            [
                'Šifra' => '990121',
                'Tip' => 'D3 CAM',
                'Model' => 'Modul CAM Conax + SIM kartica',
                'S/N' => '02095813327'
            ],
            [
                'Šifra' => '990121',
                'Tip' => 'D3 CAM',
                'Model' => 'Modul CAM Conax + SIM kartica',
                'S/N' => '02095967617'
            ],
            [
                'Šifra' => '990121',
                'Tip' => 'D3 CAM',
                'Model' => 'Modul CAM Conax + SIM kartica',
                'S/N' => '02095857742'
            ],
            [
                'Šifra' => '990121',
                'Tip' => 'D3 CAM',
                'Model' => 'Modul CAM Conax + SIM kartica',
                'S/N' => '02161720484'
            ],
            [
                'Šifra' => '990121',
                'Tip' => 'D3 CAM',
                'Model' => 'Modul CAM Conax + SIM kartica',
                'S/N' => '02095942113'
            ],
            [
                'Šifra' => '990121',
                'Tip' => 'D3 CAM',
                'Model' => 'Modul CAM Conax + SIM kartica',
                'S/N' => '02095949538'
            ],
            [
                'Šifra' => '990121',
                'Tip' => 'D3 CAM',
                'Model' => 'Modul CAM Conax + SIM kartica',
                'S/N' => '02095983815'
            ],
            [
                'Šifra' => '990121',
                'Tip' => 'D3 CAM',
                'Model' => 'Modul CAM Conax + SIM kartica',
                'S/N' => '02095982181'
            ],
            [
                'Šifra' => '990121',
                'Tip' => 'D3 CAM',
                'Model' => 'Modul CAM Conax + SIM kartica',
                'S/N' => '02095959935'
            ],
            [
                'Šifra' => '990121',
                'Tip' => 'D3 CAM',
                'Model' => 'Modul CAM Conax + SIM kartica',
                'S/N' => '02095968507'
            ],
            [
                'Šifra' => '990121',
                'Tip' => 'D3 CAM',
                'Model' => 'Modul CAM Conax + SIM kartica',
                'S/N' => '02095934518'
            ],
            [
                'Šifra' => '990121',
                'Tip' => 'D3 CAM',
                'Model' => 'Modul CAM Conax + SIM kartica',
                'S/N' => '02095956259'
            ],
            [
                'Šifra' => '990121',
                'Tip' => 'D3 CAM',
                'Model' => 'Modul CAM Conax + SIM kartica',
                'S/N' => '02095977007'
            ],
            [
                'Šifra' => '990121',
                'Tip' => 'D3 CAM',
                'Model' => 'Modul CAM Conax + SIM kartica',
                'S/N' => '02095946990'
            ],
            [
                'Šifra' => '990121',
                'Tip' => 'D3 CAM',
                'Model' => 'Modul CAM Conax + SIM kartica',
                'S/N' => '02095854386'
            ],
            [
                'Šifra' => '990121',
                'Tip' => 'D3 CAM',
                'Model' => 'Modul CAM Conax + SIM kartica',
                'S/N' => '02095811191'
            ],
            [
                'Šifra' => '990121',
                'Tip' => 'D3 CAM',
                'Model' => 'Modul CAM Conax + SIM kartica',
                'S/N' => '02095950820'
            ],
            [
                'Šifra' => '990123',
                'Tip' => 'D3 CAM',
                'Model' => 'CAM cardless SC 5.0 CI+1.3 CP',
                'S/N' => '11451848016405'
            ],
            [
                'Šifra' => '990123',
                'Tip' => 'D3 CAM',
                'Model' => 'CAM cardless SC 5.0 CI+1.3 CP',
                'S/N' => '11451836003408'
            ],
            [
                'Šifra' => '990123',
                'Tip' => 'D3 CAM',
                'Model' => 'CAM cardless SC 5.0 CI+1.3 CP',
                'S/N' => '11451848029145'
            ],
            [
                'Šifra' => '990123',
                'Tip' => 'D3 CAM',
                'Model' => 'CAM cardless SC 5.0 CI+1.3 CP',
                'S/N' => '11451848033416'
            ],
            [
                'Šifra' => '990123',
                'Tip' => 'D3 CAM',
                'Model' => 'CAM cardless SC 5.0 CI+1.3 CP',
                'S/N' => '11451839012638'
            ],
            [
                'Šifra' => '990123',
                'Tip' => 'D3 CAM',
                'Model' => 'CAM cardless SC 5.0 CI+1.3 CP',
                'S/N' => '11451839009568'
            ],
            [
                'Šifra' => '990123',
                'Tip' => 'D3 CAM',
                'Model' => 'CAM cardless SC 5.0 CI+1.3 CP',
                'S/N' => '11451848016897'
            ],
            [
                'Šifra' => '990123',
                'Tip' => 'D3 CAM',
                'Model' => 'CAM cardless SC 5.0 CI+1.3 CP',
                'S/N' => '11451845018490'
            ],
            [
                'Šifra' => '990123',
                'Tip' => 'D3 CAM',
                'Model' => 'CAM cardless SC 5.0 CI+1.3 CP',
                'S/N' => '11451839014173'
            ],
            [
                'Šifra' => '990123',
                'Tip' => 'D3 CAM',
                'Model' => 'CAM cardless SC 5.0 CI+1.3 CP',
                'S/N' => '11451839017524'
            ],
            [
                'Šifra' => '990123',
                'Tip' => 'D3 CAM',
                'Model' => 'CAM cardless SC 5.0 CI+1.3 CP',
                'S/N' => '11451848008782'
            ],
            [
                'Šifra' => '990123',
                'Tip' => 'D3 CAM',
                'Model' => 'CAM cardless SC 5.0 CI+1.3 CP',
                'S/N' => '11451848036791'
            ],
            [
                'Šifra' => '990123',
                'Tip' => 'D3 CAM',
                'Model' => 'CAM cardless SC 5.0 CI+1.3 CP',
                'S/N' => '11451845016946'
            ],
            [
                'Šifra' => '990123',
                'Tip' => 'D3 CAM',
                'Model' => 'CAM cardless SC 5.0 CI+1.3 CP',
                'S/N' => '11451848035946'
            ],
            [
                'Šifra' => '990123',
                'Tip' => 'D3 CAM',
                'Model' => 'CAM cardless SC 5.0 CI+1.3 CP',
                'S/N' => '11451848006146'
            ],
            [
                'Šifra' => '990123',
                'Tip' => 'D3 CAM',
                'Model' => 'CAM cardless SC 5.0 CI+1.3 CP',
                'S/N' => '11451848033183'
            ],
            [
                'Šifra' => '990123',
                'Tip' => 'D3 CAM',
                'Model' => 'CAM cardless SC 5.0 CI+1.3 CP',
                'S/N' => '11451839005383'
            ],
            [
                'Šifra' => '990123',
                'Tip' => 'D3 CAM',
                'Model' => 'CAM cardless SC 5.0 CI+1.3 CP',
                'S/N' => '11451839001410'
            ],
            [
                'Šifra' => '990123',
                'Tip' => 'D3 CAM',
                'Model' => 'CAM cardless SC 5.0 CI+1.3 CP',
                'S/N' => '11451848037176'
            ],
            [
                'Šifra' => '990123',
                'Tip' => 'D3 CAM',
                'Model' => 'CAM cardless SC 5.0 CI+1.3 CP',
                'S/N' => '11451839019078'
            ],
            [
                'Šifra' => '990123',
                'Tip' => 'D3 CAM',
                'Model' => 'CAM cardless SC 5.0 CI+1.3 CP',
                'S/N' => '11451848037958'
            ],
            [
                'Šifra' => '990123',
                'Tip' => 'D3 CAM',
                'Model' => 'CAM cardless SC 5.0 CI+1.3 CP',
                'S/N' => '11451848014917'
            ],
            [
                'Šifra' => '990123',
                'Tip' => 'D3 CAM',
                'Model' => 'CAM cardless SC 5.0 CI+1.3 CP',
                'S/N' => '11451848008365'
            ],
            [
                'Šifra' => '990123',
                'Tip' => 'D3 CAM',
                'Model' => 'CAM cardless SC 5.0 CI+1.3 CP',
                'S/N' => '11451839003576'
            ],
            [
                'Šifra' => '990123',
                'Tip' => 'D3 CAM',
                'Model' => 'CAM cardless SC 5.0 CI+1.3 CP',
                'S/N' => '11451836003726'
            ],
            [
                'Šifra' => '990121',
                'Tip' => 'D3 CAM',
                'Model' => 'Modul CAM Conax + SIM kartica',
                'S/N' => '02095942702'
            ],
            [
                'Šifra' => '990121',
                'Tip' => 'D3 CAM',
                'Model' => 'Modul CAM Conax + SIM kartica',
                'S/N' => '02161720780'
            ],
            [
                'Šifra' => '990121',
                'Tip' => 'D3 CAM',
                'Model' => 'Modul CAM Conax + SIM kartica',
                'S/N' => '02095978618'
            ],
            [
                'Šifra' => '990121',
                'Tip' => 'D3 CAM',
                'Model' => 'Modul CAM Conax + SIM kartica',
                'S/N' => '02095983885'
            ],
            [
                'Šifra' => '990121',
                'Tip' => 'D3 CAM',
                'Model' => 'Modul CAM Conax + SIM kartica',
                'S/N' => '02095813347'
            ],
            [
                'Šifra' => '990121',
                'Tip' => 'D3 CAM',
                'Model' => 'Modul CAM Conax + SIM kartica',
                'S/N' => '02095977480'
            ],
            [
                'Šifra' => '990123',
                'Tip' => 'D3 CAM',
                'Model' => 'CAM cardless SC 5.0 CI+1.3 CP',
                'S/N' => '11451839018067'
            ],
            [
                'Šifra' => '990123',
                'Tip' => 'D3 CAM',
                'Model' => 'CAM cardless SC 5.0 CI+1.3 CP',
                'S/N' => '11451839020692'
            ],
            [
                'Šifra' => '990123',
                'Tip' => 'D3 CAM',
                'Model' => 'CAM cardless SC 5.0 CI+1.3 CP',
                'S/N' => '11451848029424'
            ],
            [
                'Šifra' => '990123',
                'Tip' => 'D3 CAM',
                'Model' => 'CAM cardless SC 5.0 CI+1.3 CP',
                'S/N' => '11451848017892'
            ],
            [
                'Šifra' => '990123',
                'Tip' => 'D3 CAM',
                'Model' => 'CAM cardless SC 5.0 CI+1.3 CP',
                'S/N' => '11451848023315'
            ],
            [
                'Šifra' => '990123',
                'Tip' => 'D3 CAM',
                'Model' => 'CAM cardless SC 5.0 CI+1.3 CP',
                'S/N' => '11451848028618'
            ],
            [
                'Šifra' => '990123',
                'Tip' => 'D3 CAM',
                'Model' => 'CAM cardless SC 5.0 CI+1.3 CP',
                'S/N' => '11451839010854'
            ],
            [
                'Šifra' => '990123',
                'Tip' => 'D3 CAM',
                'Model' => 'CAM cardless SC 5.0 CI+1.3 CP',
                'S/N' => '11451839018933'
            ],
            [
                'Šifra' => '990123',
                'Tip' => 'D3 CAM',
                'Model' => 'CAM cardless SC 5.0 CI+1.3 CP',
                'S/N' => '11451839008968'
            ],
            [
                'Šifra' => '990123',
                'Tip' => 'D3 CAM',
                'Model' => 'CAM cardless SC 5.0 CI+1.3 CP',
                'S/N' => '11451848033594'
            ],
            [
                'Šifra' => '990123',
                'Tip' => 'D3 CAM',
                'Model' => 'CAM cardless SC 5.0 CI+1.3 CP',
                'S/N' => '11451848018117'
            ],
            [
                'Šifra' => '990123',
                'Tip' => 'D3 CAM',
                'Model' => 'CAM cardless SC 5.0 CI+1.3 CP',
                'S/N' => '11451839014686'
            ],
            [
                'Šifra' => '990123',
                'Tip' => 'D3 CAM',
                'Model' => 'CAM cardless SC 5.0 CI+1.3 CP',
                'S/N' => '11451848036248'
            ],
            [
                'Šifra' => '990123',
                'Tip' => 'D3 CAM',
                'Model' => 'CAM cardless SC 5.0 CI+1.3 CP',
                'S/N' => '11451839019942'
            ],
            [
                'Šifra' => '990123',
                'Tip' => 'D3 CAM',
                'Model' => 'CAM cardless SC 5.0 CI+1.3 CP',
                'S/N' => '11451848030191'
            ],
            [
                'Šifra' => '990123',
                'Tip' => 'D3 CAM',
                'Model' => 'CAM cardless SC 5.0 CI+1.3 CP',
                'S/N' => '11451839014660'
            ],
            [
                'Šifra' => '990123',
                'Tip' => 'D3 CAM',
                'Model' => 'CAM cardless SC 5.0 CI+1.3 CP',
                'S/N' => '11451848038447'
            ],
            [
                'Šifra' => '990123',
                'Tip' => 'D3 CAM',
                'Model' => 'CAM cardless SC 5.0 CI+1.3 CP',
                'S/N' => '11451848018567'
            ],
            [
                'Šifra' => '990123',
                'Tip' => 'D3 CAM',
                'Model' => 'CAM cardless SC 5.0 CI+1.3 CP',
                'S/N' => '11451848013558'
            ],
            [
                'Šifra' => '990123',
                'Tip' => 'D3 CAM',
                'Model' => 'CAM cardless SC 5.0 CI+1.3 CP',
                'S/N' => '11451839008299'
            ],
            [
                'Šifra' => '990123',
                'Tip' => 'D3 CAM',
                'Model' => 'CAM cardless SC 5.0 CI+1.3 CP',
                'S/N' => '11451848035817'
            ],
            [
                'Šifra' => '990123',
                'Tip' => 'D3 CAM',
                'Model' => 'CAM cardless SC 5.0 CI+1.3 CP',
                'S/N' => '11451848036684'
            ],
            [
                'Šifra' => '990123',
                'Tip' => 'D3 CAM',
                'Model' => 'CAM cardless SC 5.0 CI+1.3 CP',
                'S/N' => '11451848022606'
            ],
            [
                'Šifra' => '990123',
                'Tip' => 'D3 CAM',
                'Model' => 'CAM cardless SC 5.0 CI+1.3 CP',
                'S/N' => '11451836001786'
            ],
            [
                'Šifra' => '990123',
                'Tip' => 'D3 CAM',
                'Model' => 'CAM cardless SC 5.0 CI+1.3 CP',
                'S/N' => '11451839019671'
            ],
            [
                'Šifra' => '990123',
                'Tip' => 'D3 CAM',
                'Model' => 'CAM cardless SC 5.0 CI+1.3 CP',
                'S/N' => '11451836004623'
            ],
            [
                'Šifra' => '990123',
                'Tip' => 'D3 CAM',
                'Model' => 'CAM cardless SC 5.0 CI+1.3 CP',
                'S/N' => '11451839003304'
            ],
            [
                'Šifra' => '990123',
                'Tip' => 'D3 CAM',
                'Model' => 'CAM cardless SC 5.0 CI+1.3 CP',
                'S/N' => '11451839007823'
            ],
            [
                'Šifra' => '990123',
                'Tip' => 'D3 CAM',
                'Model' => 'CAM cardless SC 5.0 CI+1.3 CP',
                'S/N' => '11451848020590'
            ],
            [
                'Šifra' => '990123',
                'Tip' => 'D3 CAM',
                'Model' => 'CAM cardless SC 5.0 CI+1.3 CP',
                'S/N' => '11451848026474'
            ],
            [
                'Šifra' => '990123',
                'Tip' => 'D3 CAM',
                'Model' => 'CAM cardless SC 5.0 CI+1.3 CP',
                'S/N' => '11451848016904'
            ],
            [
                'Šifra' => '990123',
                'Tip' => 'D3 CAM',
                'Model' => 'CAM cardless SC 5.0 CI+1.3 CP',
                'S/N' => '11451848023715'
            ],
            [
                'Šifra' => '990123',
                'Tip' => 'D3 CAM',
                'Model' => 'CAM cardless SC 5.0 CI+1.3 CP',
                'S/N' => '11451848036353'
            ],
            [
                'Šifra' => '990123',
                'Tip' => 'D3 CAM',
                'Model' => 'CAM cardless SC 5.0 CI+1.3 CP',
                'S/N' => '11451839017538'
            ],
            [
                'Šifra' => '990123',
                'Tip' => 'D3 CAM',
                'Model' => 'CAM cardless SC 5.0 CI+1.3 CP',
                'S/N' => '11451848004969'
            ],
            [
                'Šifra' => '990123',
                'Tip' => 'D3 CAM',
                'Model' => 'CAM cardless SC 5.0 CI+1.3 CP',
                'S/N' => '11451839008316'
            ],
            [
                'Šifra' => '990123',
                'Tip' => 'D3 CAM',
                'Model' => 'CAM cardless SC 5.0 CI+1.3 CP',
                'S/N' => '11451836000839'
            ],
            [
                'Šifra' => '990123',
                'Tip' => 'D3 CAM',
                'Model' => 'CAM cardless SC 5.0 CI+1.3 CP',
                'S/N' => '11451848037265'
            ],
            [
                'Šifra' => '990121',
                'Tip' => 'D3 CAM',
                'Model' => 'Modul CAM Conax + SIM kartica',
                'S/N' => '02095942260'
            ],
            [
                'Šifra' => '990125',
                'Tip' => 'EON BOX',
                'Model' => 'EON BOX',
                'S/N' => 'BS10060198010998'
            ],
            [
                'Šifra' => '990125',
                'Tip' => 'EON BOX',
                'Model' => 'EON BOX',
                'S/N' => 'BS10066143012162'
            ],
            [
                'Šifra' => '990125',
                'Tip' => 'EON BOX',
                'Model' => 'EON BOX',
                'S/N' => 'BS10070167002904'
            ],
            [
                'Šifra' => '990125',
                'Tip' => 'EON BOX',
                'Model' => 'EON BOX',
                'S/N' => 'BS10066143014082'
            ],
            [
                'Šifra' => '990125',
                'Tip' => 'EON BOX',
                'Model' => 'EON BOX',
                'S/N' => 'BS10066143014083'
            ],
            [
                'Šifra' => '990125',
                'Tip' => 'EON BOX',
                'Model' => 'EON BOX',
                'S/N' => 'BS10070167010137'
            ],
            [
                'Šifra' => '990125',
                'Tip' => 'EON BOX',
                'Model' => 'EON BOX',
                'S/N' => 'BS10070167002913'
            ],
            [
                'Šifra' => '990125',
                'Tip' => 'EON BOX',
                'Model' => 'EON BOX',
                'S/N' => 'BS10070167002905'
            ],
            [
                'Šifra' => '990125',
                'Tip' => 'EON BOX',
                'Model' => 'EON BOX',
                'S/N' => 'BS10070167010136'
            ],
            [
                'Šifra' => '990125',
                'Tip' => 'EON BOX',
                'Model' => 'EON BOX',
                'S/N' => 'BS10066143019062'
            ],
            [
                'Šifra' => '990125',
                'Tip' => 'EON BOX',
                'Model' => 'EON BOX',
                'S/N' => 'BS1006415C002166'
            ],
            [
                'Šifra' => '990125',
                'Tip' => 'EON BOX',
                'Model' => 'EON BOX',
                'S/N' => 'BS10066143014418'
            ],
            [
                'Šifra' => '990125',
                'Tip' => 'EON BOX',
                'Model' => 'EON BOX',
                'S/N' => 'BS10066143018699'
            ],
            [
                'Šifra' => '990125',
                'Tip' => 'EON BOX',
                'Model' => 'EON BOX',
                'S/N' => 'BS10068145003465'
            ],
            [
                'Šifra' => '990125',
                'Tip' => 'EON BOX',
                'Model' => 'EON BOX',
                'S/N' => 'BS1006415C001114'
            ],
            [
                'Šifra' => '990125',
                'Tip' => 'EON BOX',
                'Model' => 'EON BOX',
                'S/N' => 'BS1006415C001310'
            ],
            [
                'Šifra' => '990125',
                'Tip' => 'EON BOX',
                'Model' => 'EON BOX',
                'S/N' => 'BS1006415C001436'
            ],
            [
                'Šifra' => '990125',
                'Tip' => 'EON BOX',
                'Model' => 'EON BOX',
                'S/N' => 'BS1006415C001291'
            ],
            [
                'Šifra' => '990125',
                'Tip' => 'EON BOX',
                'Model' => 'EON BOX',
                'S/N' => 'BS1006337C027315'
            ],
            [
                'Šifra' => '990125',
                'Tip' => 'EON BOX',
                'Model' => 'EON BOX',
                'S/N' => 'BS10066143019581'
            ],
            [
                'Šifra' => '990125',
                'Tip' => 'EON BOX',
                'Model' => 'EON BOX',
                'S/N' => 'BS1006415C001309'
            ],
            [
                'Šifra' => '990125',
                'Tip' => 'EON BOX',
                'Model' => 'EON BOX',
                'S/N' => 'BS10066143014640'
            ],
            [
                'Šifra' => '990125',
                'Tip' => 'EON BOX',
                'Model' => 'EON BOX',
                'S/N' => 'BS10066143018543'
            ],
            [
                'Šifra' => '990125',
                'Tip' => 'EON BOX',
                'Model' => 'EON BOX',
                'S/N' => 'BS10066143018555'
            ],
            [
                'Šifra' => '990125',
                'Tip' => 'EON BOX',
                'Model' => 'EON BOX',
                'S/N' => 'BS10066143018973'
            ],
            [
                'Šifra' => '990125',
                'Tip' => 'EON BOX',
                'Model' => 'EON BOX',
                'S/N' => 'BS1006415C002167'
            ],
            [
                'Šifra' => '990125',
                'Tip' => 'EON BOX',
                'Model' => 'EON BOX',
                'S/N' => 'BS1006415C001110'
            ],
            [
                'Šifra' => '990125',
                'Tip' => 'EON BOX',
                'Model' => 'EON BOX',
                'S/N' => 'BS1006415C001312'
            ],
            [
                'Šifra' => '990125',
                'Tip' => 'EON BOX',
                'Model' => 'EON BOX',
                'S/N' => 'BS10066143018687'
            ],
            [
                'Šifra' => '990125',
                'Tip' => 'EON BOX',
                'Model' => 'EON BOX',
                'S/N' => 'BS1006415C001297'
            ],
            [
                'Šifra' => '990125',
                'Tip' => 'EON BOX',
                'Model' => 'EON BOX',
                'S/N' => 'BS10066143018207'
            ],
            [
                'Šifra' => '990125',
                'Tip' => 'EON BOX',
                'Model' => 'EON BOX',
                'S/N' => 'BS10066143018978'
            ],
            [
                'Šifra' => '990125',
                'Tip' => 'EON BOX',
                'Model' => 'EON BOX',
                'S/N' => 'BS10066143021230'
            ],
            [
                'Šifra' => '990125',
                'Tip' => 'EON BOX',
                'Model' => 'EON BOX',
                'S/N' => 'BS10066143021948'
            ],
            [
                'Šifra' => '990125',
                'Tip' => 'EON BOX',
                'Model' => 'EON BOX',
                'S/N' => 'BS10066143015756'
            ],
            [
                'Šifra' => '990125',
                'Tip' => 'EON BOX',
                'Model' => 'EON BOX',
                'S/N' => 'BS10066143018193'
            ],
            [
                'Šifra' => '990125',
                'Tip' => 'EON BOX',
                'Model' => 'EON BOX',
                'S/N' => 'BS10066143023456'
            ],
            [
                'Šifra' => '990125',
                'Tip' => 'EON BOX',
                'Model' => 'EON BOX',
                'S/N' => 'BS10066143019139'
            ],
            [
                'Šifra' => '990125',
                'Tip' => 'EON BOX',
                'Model' => 'EON BOX',
                'S/N' => 'BS10066143021227'
            ],
            [
                'Šifra' => '990125',
                'Tip' => 'EON BOX',
                'Model' => 'EON BOX',
                'S/N' => 'BS10066143019135'
            ],
            [
                'Šifra' => '990125',
                'Tip' => 'EON BOX',
                'Model' => 'EON BOX',
                'S/N' => 'BS10066143019137'
            ],
            [
                'Šifra' => '990125',
                'Tip' => 'EON BOX',
                'Model' => 'EON BOX',
                'S/N' => 'BS10066143019147'
            ],
            [
                'Šifra' => '990125',
                'Tip' => 'EON BOX',
                'Model' => 'EON BOX',
                'S/N' => 'BS10066143019165'
            ],
            [
                'Šifra' => '990125',
                'Tip' => 'EON BOX',
                'Model' => 'EON BOX',
                'S/N' => 'BS10066143021250'
            ],
            [
                'Šifra' => '990125',
                'Tip' => 'EON BOX',
                'Model' => 'EON BOX',
                'S/N' => 'BS10066143021231'
            ],
            [
                'Šifra' => '990125',
                'Tip' => 'EON BOX',
                'Model' => 'EON BOX',
                'S/N' => 'BS10066143021232'
            ],
            [
                'Šifra' => '990125',
                'Tip' => 'EON BOX',
                'Model' => 'EON BOX',
                'S/N' => 'BS10066143021239'
            ],
            [
                'Šifra' => '990125',
                'Tip' => 'EON BOX',
                'Model' => 'EON BOX',
                'S/N' => 'BS10066143019138'
            ],
            [
                'Šifra' => '990125',
                'Tip' => 'EON BOX',
                'Model' => 'EON BOX',
                'S/N' => 'BS10066143023458'
            ],
            [
                'Šifra' => '990125',
                'Tip' => 'EON BOX',
                'Model' => 'EON BOX',
                'S/N' => 'BS10066143019140'
            ],
            [
                'Šifra' => '990125',
                'Tip' => 'EON BOX',
                'Model' => 'EON BOX',
                'S/N' => 'BS10066143019144'
            ],
            [
                'Šifra' => '990125',
                'Tip' => 'EON BOX',
                'Model' => 'EON BOX',
                'S/N' => 'BS10066143019146'
            ],
            [
                'Šifra' => '990125',
                'Tip' => 'EON BOX',
                'Model' => 'EON BOX',
                'S/N' => 'BS10066143021226'
            ],
            [
                'Šifra' => '990125',
                'Tip' => 'EON BOX',
                'Model' => 'EON BOX',
                'S/N' => 'BS10066143019008'
            ],
            [
                'Šifra' => '990125',
                'Tip' => 'EON BOX',
                'Model' => 'EON BOX',
                'S/N' => 'BS10068215001216'
            ],
            [
                'Šifra' => '990125',
                'Tip' => 'EON BOX',
                'Model' => 'EON BOX',
                'S/N' => 'BS10070167005422'
            ],
            [
                'Šifra' => '990125',
                'Tip' => 'EON BOX',
                'Model' => 'EON BOX',
                'S/N' => 'BS10066143021051'
            ],
            [
                'Šifra' => '990125',
                'Tip' => 'EON BOX',
                'Model' => 'EON BOX',
                'S/N' => 'BS10066143021836'
            ],
            [
                'Šifra' => '990125',
                'Tip' => 'EON BOX',
                'Model' => 'EON BOX',
                'S/N' => 'BS10066143019145'
            ],
            [
                'Šifra' => '990125',
                'Tip' => 'EON BOX',
                'Model' => 'EON BOX',
                'S/N' => 'BS10066143023509'
            ],
            [
                'Šifra' => '990125',
                'Tip' => 'EON BOX',
                'Model' => 'EON BOX',
                'S/N' => 'BS10066143019143'
            ],
            [
                'Šifra' => '990125',
                'Tip' => 'EON BOX',
                'Model' => 'EON BOX',
                'S/N' => 'BS10066143018490'
            ],
            [
                'Šifra' => '990125',
                'Tip' => 'EON BOX',
                'Model' => 'EON BOX',
                'S/N' => 'BS10066143018628'
            ],
            [
                'Šifra' => '990125',
                'Tip' => 'EON BOX',
                'Model' => 'EON BOX',
                'S/N' => 'BS10066143019119'
            ],
            [
                'Šifra' => '990125',
                'Tip' => 'EON BOX',
                'Model' => 'EON BOX',
                'S/N' => 'BS10066143019104'
            ],
            [
                'Šifra' => '990125',
                'Tip' => 'EON BOX',
                'Model' => 'EON BOX',
                'S/N' => 'BS10066143021233'
            ],
            [
                'Šifra' => '990125',
                'Tip' => 'EON BOX',
                'Model' => 'EON BOX',
                'S/N' => 'BS10066143019122'
            ],
            [
                'Šifra' => '990125',
                'Tip' => 'EON BOX',
                'Model' => 'EON BOX',
                'S/N' => 'BS10066143018633'
            ],
            [
                'Šifra' => '990125',
                'Tip' => 'EON BOX',
                'Model' => 'EON BOX',
                'S/N' => 'BS10066143015045'
            ],
            [
                'Šifra' => '990125',
                'Tip' => 'EON BOX',
                'Model' => 'EON BOX',
                'S/N' => 'BS10066143018449'
            ],
            [
                'Šifra' => '990125',
                'Tip' => 'EON BOX',
                'Model' => 'EON BOX',
                'S/N' => 'BS10066143019149'
            ],
            [
                'Šifra' => '990125',
                'Tip' => 'EON BOX',
                'Model' => 'EON BOX',
                'S/N' => 'BS10068145009715'
            ],
            [
                'Šifra' => '990125',
                'Tip' => 'EON BOX',
                'Model' => 'EON BOX',
                'S/N' => 'BS10068215000383'
            ],
            [
                'Šifra' => '990125',
                'Tip' => 'EON BOX',
                'Model' => 'EON BOX',
                'S/N' => 'BS10070167002339'
            ],
            [
                'Šifra' => '990125',
                'Tip' => 'EON BOX',
                'Model' => 'EON BOX',
                'S/N' => 'BS10070167007031'
            ],
            [
                'Šifra' => '990125',
                'Tip' => 'EON BOX',
                'Model' => 'EON BOX',
                'S/N' => 'BS10070167007887'
            ],
            [
                'Šifra' => '990125',
                'Tip' => 'EON BOX',
                'Model' => 'EON BOX',
                'S/N' => 'BS10070167007877'
            ],
            [
                'Šifra' => '990125',
                'Tip' => 'EON BOX',
                'Model' => 'EON BOX',
                'S/N' => 'BS10070167003949'
            ],
            [
                'Šifra' => '990125',
                'Tip' => 'EON BOX',
                'Model' => 'EON BOX',
                'S/N' => 'BS10070167003955'
            ],
            [
                'Šifra' => '990125',
                'Tip' => 'EON BOX',
                'Model' => 'EON BOX',
                'S/N' => 'BS10070167004120'
            ],
            [
                'Šifra' => '990125',
                'Tip' => 'EON BOX',
                'Model' => 'EON BOX',
                'S/N' => 'BS10070167004114'
            ],
            [
                'Šifra' => '990125',
                'Tip' => 'EON BOX',
                'Model' => 'EON BOX',
                'S/N' => 'BS10070167007885'
            ],
            [
                'Šifra' => '990125',
                'Tip' => 'EON BOX',
                'Model' => 'EON BOX',
                'S/N' => 'BS10070167007708'
            ],
            [
                'Šifra' => '990125',
                'Tip' => 'EON BOX',
                'Model' => 'EON BOX',
                'S/N' => 'BS10070167000681'
            ],
            [
                'Šifra' => '990125',
                'Tip' => 'EON BOX',
                'Model' => 'EON BOX',
                'S/N' => 'BS10070167000676'
            ],
            [
                'Šifra' => '990125',
                'Tip' => 'EON BOX',
                'Model' => 'EON BOX',
                'S/N' => 'BS10070167004116'
            ],
            [
                'Šifra' => '990125',
                'Tip' => 'EON BOX',
                'Model' => 'EON BOX',
                'S/N' => 'BS10070167007876'
            ],
            [
                'Šifra' => '990125',
                'Tip' => 'EON BOX',
                'Model' => 'EON BOX',
                'S/N' => 'BS10070167000530'
            ],
            [
                'Šifra' => '990125',
                'Tip' => 'EON BOX',
                'Model' => 'EON BOX',
                'S/N' => 'BS10070167007880'
            ],
            [
                'Šifra' => '990125',
                'Tip' => 'EON BOX',
                'Model' => 'EON BOX',
                'S/N' => 'BS10070167003918'
            ],
            [
                'Šifra' => '990125',
                'Tip' => 'EON BOX',
                'Model' => 'EON BOX',
                'S/N' => 'BS10070167007870'
            ],
            [
                'Šifra' => '990125',
                'Tip' => 'EON BOX',
                'Model' => 'EON BOX',
                'S/N' => 'BS10070167007886'
            ],
            [
                'Šifra' => '990125',
                'Tip' => 'EON BOX',
                'Model' => 'EON BOX',
                'S/N' => 'BS10070167002322'
            ],
            [
                'Šifra' => '990125',
                'Tip' => 'EON BOX',
                'Model' => 'EON BOX',
                'S/N' => 'BS10070167009535'
            ],
            [
                'Šifra' => '990125',
                'Tip' => 'EON BOX',
                'Model' => 'EON BOX',
                'S/N' => 'BS10070167009536'
            ],
            [
                'Šifra' => '990125',
                'Tip' => 'EON BOX',
                'Model' => 'EON BOX',
                'S/N' => 'BS10070167002318'
            ],
            [
                'Šifra' => '990125',
                'Tip' => 'EON BOX',
                'Model' => 'EON BOX',
                'S/N' => 'BS10070167005424'
            ],
            [
                'Šifra' => '990125',
                'Tip' => 'EON BOX',
                'Model' => 'EON BOX',
                'S/N' => 'BS10070167002282'
            ],
            [
                'Šifra' => '990125',
                'Tip' => 'EON BOX',
                'Model' => 'EON BOX',
                'S/N' => 'BS10070167005420'
            ],
            [
                'Šifra' => '990125',
                'Tip' => 'EON BOX',
                'Model' => 'EON BOX',
                'S/N' => 'BS10070167009524'
            ],
            [
                'Šifra' => '990125',
                'Tip' => 'EON BOX',
                'Model' => 'EON BOX',
                'S/N' => 'BS10070167002326'
            ],
            [
                'Šifra' => '990125',
                'Tip' => 'EON BOX',
                'Model' => 'EON BOX',
                'S/N' => 'BS10070167002315'
            ],
            [
                'Šifra' => '990125',
                'Tip' => 'EON BOX',
                'Model' => 'EON BOX',
                'S/N' => 'BS10070167002306'
            ],
            [
                'Šifra' => '990125',
                'Tip' => 'EON BOX',
                'Model' => 'EON BOX',
                'S/N' => 'BS10070167002317'
            ],
            [
                'Šifra' => '990125',
                'Tip' => 'EON BOX',
                'Model' => 'EON BOX',
                'S/N' => 'BS10070167002319'
            ],
            [
                'Šifra' => '990125',
                'Tip' => 'EON BOX',
                'Model' => 'EON BOX',
                'S/N' => 'BS10070167009532'
            ],
            [
                'Šifra' => '990125',
                'Tip' => 'EON BOX',
                'Model' => 'EON BOX',
                'S/N' => 'BS10070167002320'
            ],
            [
                'Šifra' => '990125',
                'Tip' => 'EON BOX',
                'Model' => 'EON BOX',
                'S/N' => 'BS10070167005419'
            ],
            [
                'Šifra' => '990125',
                'Tip' => 'EON BOX',
                'Model' => 'EON BOX',
                'S/N' => 'BS10070167005423'
            ],
            [
                'Šifra' => '990125',
                'Tip' => 'EON BOX',
                'Model' => 'EON BOX',
                'S/N' => 'BS10070167009528'
            ],
            [
                'Šifra' => '990125',
                'Tip' => 'EON BOX',
                'Model' => 'EON BOX',
                'S/N' => 'BS10066143018301'
            ],
            [
                'Šifra' => '990125',
                'Tip' => 'EON BOX',
                'Model' => 'EON BOX',
                'S/N' => 'BS10066143018304'
            ],
            [
                'Šifra' => '990125',
                'Tip' => 'EON BOX',
                'Model' => 'EON BOX',
                'S/N' => 'BS10066143018975'
            ],
            [
                'Šifra' => '990125',
                'Tip' => 'EON BOX',
                'Model' => 'EON BOX',
                'S/N' => 'BS10066143018977'
            ],
            [
                'Šifra' => '990125',
                'Tip' => 'EON BOX',
                'Model' => 'EON BOX',
                'S/N' => 'BS10066143018406'
            ],
            [
                'Šifra' => '990125',
                'Tip' => 'EON BOX',
                'Model' => 'EON BOX',
                'S/N' => 'BS10066143015768'
            ],
            [
                'Šifra' => '990125',
                'Tip' => 'EON BOX',
                'Model' => 'EON BOX',
                'S/N' => 'BS10066143019064'
            ],
            [
                'Šifra' => '990125',
                'Tip' => 'EON BOX',
                'Model' => 'EON BOX',
                'S/N' => 'BS10066143015771'
            ],
            [
                'Šifra' => '990125',
                'Tip' => 'EON BOX',
                'Model' => 'EON BOX',
                'S/N' => 'BS10066143018305'
            ],
            [
                'Šifra' => '990125',
                'Tip' => 'EON BOX',
                'Model' => 'EON BOX',
                'S/N' => 'BS10066143021949'
            ],
            [
                'Šifra' => '990125',
                'Tip' => 'EON BOX',
                'Model' => 'EON BOX',
                'S/N' => 'BS10066143015783'
            ],
            [
                'Šifra' => '990125',
                'Tip' => 'EON BOX',
                'Model' => 'EON BOX',
                'S/N' => 'BS10066143015769'
            ],
            [
                'Šifra' => '990125',
                'Tip' => 'EON BOX',
                'Model' => 'EON BOX',
                'S/N' => 'BS10066143021951'
            ],
            [
                'Šifra' => '990125',
                'Tip' => 'EON BOX',
                'Model' => 'EON BOX',
                'S/N' => 'BS10066143018300'
            ],
            [
                'Šifra' => '990125',
                'Tip' => 'EON BOX',
                'Model' => 'EON BOX',
                'S/N' => 'BS10066143021708'
            ],
            [
                'Šifra' => '990125',
                'Tip' => 'EON BOX',
                'Model' => 'EON BOX',
                'S/N' => 'BS10066143019076'
            ],
            [
                'Šifra' => '990125',
                'Tip' => 'EON BOX',
                'Model' => 'EON BOX',
                'S/N' => 'BS10066143015770'
            ],
            [
                'Šifra' => '990125',
                'Tip' => 'EON BOX',
                'Model' => 'EON BOX',
                'S/N' => 'BS1006415C001265'
            ],
            [
                'Šifra' => '990125',
                'Tip' => 'EON BOX',
                'Model' => 'EON BOX',
                'S/N' => 'BS10066143019051'
            ],
            [
                'Šifra' => '990125',
                'Tip' => 'EON BOX',
                'Model' => 'EON BOX',
                'S/N' => 'BS10066143006999'
            ],
            [
                'Šifra' => '990125',
                'Tip' => 'EON BOX',
                'Model' => 'EON BOX',
                'S/N' => 'BS10056614027675'
            ],
            [
                'Šifra' => '990125',
                'Tip' => 'EON BOX',
                'Model' => 'EON BOX',
                'S/N' => 'BS10056614038591'
            ],
            [
                'Šifra' => '990125',
                'Tip' => 'EON BOX',
                'Model' => 'EON BOX',
                'S/N' => 'BS10055592026688'
            ],
            [
                'Šifra' => '990125',
                'Tip' => 'EON BOX',
                'Model' => 'EON BOX',
                'S/N' => 'BS10055592014026'
            ],
            [
                'Šifra' => '990125',
                'Tip' => 'EON BOX',
                'Model' => 'EON BOX',
                'S/N' => 'BS10066143021970'
            ],
            [
                'Šifra' => '990091',
                'Tip' => 'D3 mini',
                'Model' => 'Digital MPEG HD DVB-D3mini',
                'S/N' => '00677809245'
            ],
            [
                'Šifra' => '990091',
                'Tip' => 'D3 mini',
                'Model' => 'Digital MPEG HD DVB-D3mini',
                'S/N' => '00757992398'
            ],
            [
                'Šifra' => '990091',
                'Tip' => 'D3 mini',
                'Model' => 'Digital MPEG HD DVB-D3mini',
                'S/N' => '00743685184'
            ],
            [
                'Šifra' => '990091',
                'Tip' => 'D3 mini',
                'Model' => 'Digital MPEG HD DVB-D3mini',
                'S/N' => '00753153432'
            ],
            [
                'Šifra' => '990091',
                'Tip' => 'D3 mini',
                'Model' => 'Digital MPEG HD DVB-D3mini',
                'S/N' => '00743676054'
            ],
            [
                'Šifra' => '990091',
                'Tip' => 'D3 mini',
                'Model' => 'Digital MPEG HD DVB-D3mini',
                'S/N' => '00677927990'
            ],
            [
                'Šifra' => '990091',
                'Tip' => 'D3 mini',
                'Model' => 'Digital MPEG HD DVB-D3mini',
                'S/N' => '00677957283'
            ],
            [
                'Šifra' => '990091',
                'Tip' => 'D3 mini',
                'Model' => 'Digital MPEG HD DVB-D3mini',
                'S/N' => '00677850173'
            ],
            [
                'Šifra' => '990091',
                'Tip' => 'D3 mini',
                'Model' => 'Digital MPEG HD DVB-D3mini',
                'S/N' => '00753178291'
            ],
            [
                'Šifra' => '990091',
                'Tip' => 'D3 mini',
                'Model' => 'Digital MPEG HD DVB-D3mini',
                'S/N' => '00753184421'
            ],
            [
                'Šifra' => '990091',
                'Tip' => 'D3 mini',
                'Model' => 'Digital MPEG HD DVB-D3mini',
                'S/N' => '00760169101'
            ],
            [
                'Šifra' => '990091',
                'Tip' => 'D3 mini',
                'Model' => 'Digital MPEG HD DVB-D3mini',
                'S/N' => '00743726686'
            ],
            [
                'Šifra' => '990091',
                'Tip' => 'D3 mini',
                'Model' => 'Digital MPEG HD DVB-D3mini',
                'S/N' => '00724192744'
            ],
            [
                'Šifra' => '990091',
                'Tip' => 'D3 mini',
                'Model' => 'Digital MPEG HD DVB-D3mini',
                'S/N' => '00724000849'
            ],
            [
                'Šifra' => '990091',
                'Tip' => 'D3 mini',
                'Model' => 'Digital MPEG HD DVB-D3mini',
                'S/N' => '00753085752'
            ],
            [
                'Šifra' => '990091',
                'Tip' => 'D3 mini',
                'Model' => 'Digital MPEG HD DVB-D3mini',
                'S/N' => '00638469439'
            ],
            [
                'Šifra' => '990091',
                'Tip' => 'D3 mini',
                'Model' => 'Digital MPEG HD DVB-D3mini',
                'S/N' => '00753062218'
            ],
            [
                'Šifra' => '990091',
                'Tip' => 'D3 mini',
                'Model' => 'Digital MPEG HD DVB-D3mini',
                'S/N' => '00743690883'
            ],
            [
                'Šifra' => '990091',
                'Tip' => 'D3 mini',
                'Model' => 'Digital MPEG HD DVB-D3mini',
                'S/N' => '00677884864'
            ],
            [
                'Šifra' => '990091',
                'Tip' => 'D3 mini',
                'Model' => 'Digital MPEG HD DVB-D3mini',
                'S/N' => '00743699752'
            ],
            [
                'Šifra' => '990091',
                'Tip' => 'D3 mini',
                'Model' => 'Digital MPEG HD DVB-D3mini',
                'S/N' => '00677937180'
            ],
            [
                'Šifra' => '990091',
                'Tip' => 'D3 mini',
                'Model' => 'Digital MPEG HD DVB-D3mini',
                'S/N' => '00677952392'
            ],
            [
                'Šifra' => '990091',
                'Tip' => 'D3 mini',
                'Model' => 'Digital MPEG HD DVB-D3mini',
                'S/N' => '00677842841'
            ],
            [
                'Šifra' => '990091',
                'Tip' => 'D3 mini',
                'Model' => 'Digital MPEG HD DVB-D3mini',
                'S/N' => '00724175074'
            ],
            [
                'Šifra' => '990091',
                'Tip' => 'D3 mini',
                'Model' => 'Digital MPEG HD DVB-D3mini',
                'S/N' => '00638422431'
            ],
            [
                'Šifra' => '990091',
                'Tip' => 'D3 mini',
                'Model' => 'Digital MPEG HD DVB-D3mini',
                'S/N' => '00638399194'
            ],
            [
                'Šifra' => '990091',
                'Tip' => 'D3 mini',
                'Model' => 'Digital MPEG HD DVB-D3mini',
                'S/N' => '00758018842'
            ],
            [
                'Šifra' => '990091',
                'Tip' => 'D3 mini',
                'Model' => 'Digital MPEG HD DVB-D3mini',
                'S/N' => '00638396308'
            ],
            [
                'Šifra' => '990091',
                'Tip' => 'D3 mini',
                'Model' => 'Digital MPEG HD DVB-D3mini',
                'S/N' => '00677959763'
            ],
            [
                'Šifra' => '990091',
                'Tip' => 'D3 mini',
                'Model' => 'Digital MPEG HD DVB-D3mini',
                'S/N' => '00753179010'
            ],
            [
                'Šifra' => '990091',
                'Tip' => 'D3 mini',
                'Model' => 'Digital MPEG HD DVB-D3mini',
                'S/N' => '00724207404'
            ],
            [
                'Šifra' => '990091',
                'Tip' => 'D3 mini',
                'Model' => 'Digital MPEG HD DVB-D3mini',
                'S/N' => '00743707698'
            ],
            [
                'Šifra' => '990091',
                'Tip' => 'D3 mini',
                'Model' => 'Digital MPEG HD DVB-D3mini',
                'S/N' => '00760126506'
            ],
            [
                'Šifra' => '990091',
                'Tip' => 'D3 mini',
                'Model' => 'Digital MPEG HD DVB-D3mini',
                'S/N' => '00677887076'
            ],
            [
                'Šifra' => '990091',
                'Tip' => 'D3 mini',
                'Model' => 'Digital MPEG HD DVB-D3mini',
                'S/N' => '00724215581'
            ],
            [
                'Šifra' => '990091',
                'Tip' => 'D3 mini',
                'Model' => 'Digital MPEG HD DVB-D3mini',
                'S/N' => '00743794807'
            ],
            [
                'Šifra' => '990091',
                'Tip' => 'D3 mini',
                'Model' => 'Digital MPEG HD DVB-D3mini',
                'S/N' => '00743683768'
            ],
            [
                'Šifra' => '990091',
                'Tip' => 'D3 mini',
                'Model' => 'Digital MPEG HD DVB-D3mini',
                'S/N' => '00724197385'
            ],
            [
                'Šifra' => '990091',
                'Tip' => 'D3 mini',
                'Model' => 'Digital MPEG HD DVB-D3mini',
                'S/N' => '00677969688'
            ],
            [
                'Šifra' => '990091',
                'Tip' => 'D3 mini',
                'Model' => 'Digital MPEG HD DVB-D3mini',
                'S/N' => '00677964310'
            ],
            [
                'Šifra' => '990091',
                'Tip' => 'D3 mini',
                'Model' => 'Digital MPEG HD DVB-D3mini',
                'S/N' => '00753128203'
            ],
            [
                'Šifra' => '990091',
                'Tip' => 'D3 mini',
                'Model' => 'Digital MPEG HD DVB-D3mini',
                'S/N' => '00724183991'
            ],
            [
                'Šifra' => '990091',
                'Tip' => 'D3 mini',
                'Model' => 'Digital MPEG HD DVB-D3mini',
                'S/N' => '00743812910'
            ],
            [
                'Šifra' => '990091',
                'Tip' => 'D3 mini',
                'Model' => 'Digital MPEG HD DVB-D3mini',
                'S/N' => '00753189786'
            ],
            [
                'Šifra' => '990091',
                'Tip' => 'D3 mini',
                'Model' => 'Digital MPEG HD DVB-D3mini',
                'S/N' => '00724003644'
            ],
            [
                'Šifra' => '990091',
                'Tip' => 'D3 mini',
                'Model' => 'Digital MPEG HD DVB-D3mini',
                'S/N' => '00677934679'
            ],
            [
                'Šifra' => '990091',
                'Tip' => 'D3 mini',
                'Model' => 'Digital MPEG HD DVB-D3mini',
                'S/N' => '00753144150'
            ],
            [
                'Šifra' => '990091',
                'Tip' => 'D3 mini',
                'Model' => 'Digital MPEG HD DVB-D3mini',
                'S/N' => '00724215089'
            ],
            [
                'Šifra' => '990091',
                'Tip' => 'D3 mini',
                'Model' => 'Digital MPEG HD DVB-D3mini',
                'S/N' => '00760127133'
            ],
            [
                'Šifra' => '990091',
                'Tip' => 'D3 mini',
                'Model' => 'Digital MPEG HD DVB-D3mini',
                'S/N' => '00677924732'
            ],
            [
                'Šifra' => '990091',
                'Tip' => 'D3 mini',
                'Model' => 'Digital MPEG HD DVB-D3mini',
                'S/N' => '00743666612'
            ],
            [
                'Šifra' => '990091',
                'Tip' => 'D3 mini',
                'Model' => 'Digital MPEG HD DVB-D3mini',
                'S/N' => '00677899966'
            ],
            [
                'Šifra' => '990091',
                'Tip' => 'D3 mini',
                'Model' => 'Digital MPEG HD DVB-D3mini',
                'S/N' => '00677881013'
            ],
            [
                'Šifra' => '990091',
                'Tip' => 'D3 mini',
                'Model' => 'Digital MPEG HD DVB-D3mini',
                'S/N' => '00677851073'
            ],
            [
                'Šifra' => '990091',
                'Tip' => 'D3 mini',
                'Model' => 'Digital MPEG HD DVB-D3mini',
                'S/N' => '00677952146'
            ],
            [
                'Šifra' => '990091',
                'Tip' => 'D3 mini',
                'Model' => 'Digital MPEG HD DVB-D3mini',
                'S/N' => '00753072444'
            ],
            [
                'Šifra' => '990091',
                'Tip' => 'D3 mini',
                'Model' => 'Digital MPEG HD DVB-D3mini',
                'S/N' => '00677932702'
            ],
            [
                'Šifra' => '990091',
                'Tip' => 'D3 mini',
                'Model' => 'Digital MPEG HD DVB-D3mini',
                'S/N' => '00677909824'
            ],
            [
                'Šifra' => '990091',
                'Tip' => 'D3 mini',
                'Model' => 'Digital MPEG HD DVB-D3mini',
                'S/N' => '00677752573'
            ],
            [
                'Šifra' => '990091',
                'Tip' => 'D3 mini',
                'Model' => 'Digital MPEG HD DVB-D3mini',
                'S/N' => '00677753185'
            ],
            [
                'Šifra' => '990091',
                'Tip' => 'D3 mini',
                'Model' => 'Digital MPEG HD DVB-D3mini',
                'S/N' => '00677984202'
            ],
            [
                'Šifra' => '990091',
                'Tip' => 'D3 mini',
                'Model' => 'Digital MPEG HD DVB-D3mini',
                'S/N' => '00677939492'
            ],
            [
                'Šifra' => '990091',
                'Tip' => 'D3 mini',
                'Model' => 'Digital MPEG HD DVB-D3mini',
                'S/N' => '00677878353'
            ],
            [
                'Šifra' => '990091',
                'Tip' => 'D3 mini',
                'Model' => 'Digital MPEG HD DVB-D3mini',
                'S/N' => '00677909947'
            ],
            [
                'Šifra' => '990091',
                'Tip' => 'D3 mini',
                'Model' => 'Digital MPEG HD DVB-D3mini',
                'S/N' => '00753220301'
            ],
            [
                'Šifra' => '990091',
                'Tip' => 'D3 mini',
                'Model' => 'Digital MPEG HD DVB-D3mini',
                'S/N' => '00677951236'
            ],
            [
                'Šifra' => '990091',
                'Tip' => 'D3 mini',
                'Model' => 'Digital MPEG HD DVB-D3mini',
                'S/N' => '00743736587'
            ],
            [
                'Šifra' => '990091',
                'Tip' => 'D3 mini',
                'Model' => 'Digital MPEG HD DVB-D3mini',
                'S/N' => '00638430809'
            ],
            [
                'Šifra' => '990091',
                'Tip' => 'D3 mini',
                'Model' => 'Digital MPEG HD DVB-D3mini',
                'S/N' => '00753090527'
            ],
            [
                'Šifra' => '990091',
                'Tip' => 'D3 mini',
                'Model' => 'Digital MPEG HD DVB-D3mini',
                'S/N' => '00724004443'
            ],
            [
                'Šifra' => '990091',
                'Tip' => 'D3 mini',
                'Model' => 'Digital MPEG HD DVB-D3mini',
                'S/N' => '00677965746'
            ],
            [
                'Šifra' => '990091',
                'Tip' => 'D3 mini',
                'Model' => 'Digital MPEG HD DVB-D3mini',
                'S/N' => '00753192880'
            ],
            [
                'Šifra' => '990091',
                'Tip' => 'D3 mini',
                'Model' => 'Digital MPEG HD DVB-D3mini',
                'S/N' => '00677805008'
            ],
            [
                'Šifra' => '990091',
                'Tip' => 'D3 mini',
                'Model' => 'Digital MPEG HD DVB-D3mini',
                'S/N' => '00677948530'
            ],
            [
                'Šifra' => '990091',
                'Tip' => 'D3 mini',
                'Model' => 'Digital MPEG HD DVB-D3mini',
                'S/N' => '00677806119'
            ],
            [
                'Šifra' => '990091',
                'Tip' => 'D3 mini',
                'Model' => 'Digital MPEG HD DVB-D3mini',
                'S/N' => '00753132297'
            ],
            [
                'Šifra' => '990091',
                'Tip' => 'D3 mini',
                'Model' => 'Digital MPEG HD DVB-D3mini',
                'S/N' => '00638333528'
            ],
            [
                'Šifra' => '990091',
                'Tip' => 'D3 mini',
                'Model' => 'Digital MPEG HD DVB-D3mini',
                'S/N' => '00677951944'
            ],
            [
                'Šifra' => '990091',
                'Tip' => 'D3 mini',
                'Model' => 'Digital MPEG HD DVB-D3mini',
                'S/N' => '00677775121'
            ],
            [
                'Šifra' => '990091',
                'Tip' => 'D3 mini',
                'Model' => 'Digital MPEG HD DVB-D3mini',
                'S/N' => '00753198451'
            ],
            [
                'Šifra' => '990091',
                'Tip' => 'D3 mini',
                'Model' => 'Digital MPEG HD DVB-D3mini',
                'S/N' => '00753207154'
            ],
            [
                'Šifra' => '990091',
                'Tip' => 'D3 mini',
                'Model' => 'Digital MPEG HD DVB-D3mini',
                'S/N' => '00753198404'
            ],
            [
                'Šifra' => '990091',
                'Tip' => 'D3 mini',
                'Model' => 'Digital MPEG HD DVB-D3mini',
                'S/N' => '00724221679'
            ],
            [
                'Šifra' => '990091',
                'Tip' => 'D3 mini',
                'Model' => 'Digital MPEG HD DVB-D3mini',
                'S/N' => '00677903186'
            ],
            [
                'Šifra' => '990091',
                'Tip' => 'D3 mini',
                'Model' => 'Digital MPEG HD DVB-D3mini',
                'S/N' => '00758022144'
            ],
            [
                'Šifra' => '990091',
                'Tip' => 'D3 mini',
                'Model' => 'Digital MPEG HD DVB-D3mini',
                'S/N' => '00743734231'
            ],
            [
                'Šifra' => '990091',
                'Tip' => 'D3 mini',
                'Model' => 'Digital MPEG HD DVB-D3mini',
                'S/N' => '00677891738'
            ],
            [
                'Šifra' => '990091',
                'Tip' => 'D3 mini',
                'Model' => 'Digital MPEG HD DVB-D3mini',
                'S/N' => '00677956639'
            ],
            [
                'Šifra' => '990091',
                'Tip' => 'D3 mini',
                'Model' => 'Digital MPEG HD DVB-D3mini',
                'S/N' => '00677841374'
            ],
            [
                'Šifra' => '990091',
                'Tip' => 'D3 mini',
                'Model' => 'Digital MPEG HD DVB-D3mini',
                'S/N' => '00677768959'
            ],
            [
                'Šifra' => '990091',
                'Tip' => 'D3 mini',
                'Model' => 'Digital MPEG HD DVB-D3mini',
                'S/N' => '00677851193'
            ],
            [
                'Šifra' => '990091',
                'Tip' => 'D3 mini',
                'Model' => 'Digital MPEG HD DVB-D3mini',
                'S/N' => '00677922287'
            ],
            [
                'Šifra' => '990091',
                'Tip' => 'D3 mini',
                'Model' => 'Digital MPEG HD DVB-D3mini',
                'S/N' => '00743729769'
            ],
            [
                'Šifra' => '990091',
                'Tip' => 'D3 mini',
                'Model' => 'Digital MPEG HD DVB-D3mini',
                'S/N' => '00677775919'
            ],
            [
                'Šifra' => '990091',
                'Tip' => 'D3 mini',
                'Model' => 'Digital MPEG HD DVB-D3mini',
                'S/N' => '00724186668'
            ],
            [
                'Šifra' => '990091',
                'Tip' => 'D3 mini',
                'Model' => 'Digital MPEG HD DVB-D3mini',
                'S/N' => '00753166451'
            ],
            [
                'Šifra' => '990091',
                'Tip' => 'D3 mini',
                'Model' => 'Digital MPEG HD DVB-D3mini',
                'S/N' => '00677895626'
            ],
            [
                'Šifra' => '990091',
                'Tip' => 'D3 mini',
                'Model' => 'Digital MPEG HD DVB-D3mini',
                'S/N' => '00743818331'
            ],
            [
                'Šifra' => '990091',
                'Tip' => 'D3 mini',
                'Model' => 'Digital MPEG HD DVB-D3mini',
                'S/N' => '00743822520'
            ],
            [
                'Šifra' => '990091',
                'Tip' => 'D3 mini',
                'Model' => 'Digital MPEG HD DVB-D3mini',
                'S/N' => '00724014281'
            ],
            [
                'Šifra' => '990091',
                'Tip' => 'D3 mini',
                'Model' => 'Digital MPEG HD DVB-D3mini',
                'S/N' => '00724015447'
            ],
            [
                'Šifra' => '990091',
                'Tip' => 'D3 mini',
                'Model' => 'Digital MPEG HD DVB-D3mini',
                'S/N' => '00677757589'
            ],
            [
                'Šifra' => '990091',
                'Tip' => 'D3 mini',
                'Model' => 'Digital MPEG HD DVB-D3mini',
                'S/N' => '00677934171'
            ],
            [
                'Šifra' => '990091',
                'Tip' => 'D3 mini',
                'Model' => 'Digital MPEG HD DVB-D3mini',
                'S/N' => '00743734697'
            ],
            [
                'Šifra' => '990091',
                'Tip' => 'D3 mini',
                'Model' => 'Digital MPEG HD DVB-D3mini',
                'S/N' => '00753170052'
            ],
            [
                'Šifra' => '990091',
                'Tip' => 'D3 mini',
                'Model' => 'Digital MPEG HD DVB-D3mini',
                'S/N' => '00743704580'
            ],
            [
                'Šifra' => '990091',
                'Tip' => 'D3 mini',
                'Model' => 'Digital MPEG HD DVB-D3mini',
                'S/N' => '00677912177'
            ],
            [
                'Šifra' => '990091',
                'Tip' => 'D3 mini',
                'Model' => 'Digital MPEG HD DVB-D3mini',
                'S/N' => '00677947551'
            ],
            [
                'Šifra' => '990091',
                'Tip' => 'D3 mini',
                'Model' => 'Digital MPEG HD DVB-D3mini',
                'S/N' => '00724189054'
            ],
            [
                'Šifra' => '990091',
                'Tip' => 'D3 mini',
                'Model' => 'Digital MPEG HD DVB-D3mini',
                'S/N' => '00753076459'
            ],
            [
                'Šifra' => '990091',
                'Tip' => 'D3 mini',
                'Model' => 'Digital MPEG HD DVB-D3mini',
                'S/N' => '00677952887'
            ],
            [
                'Šifra' => '990091',
                'Tip' => 'D3 mini',
                'Model' => 'Digital MPEG HD DVB-D3mini',
                'S/N' => '00677958446'
            ],
            [
                'Šifra' => '990091',
                'Tip' => 'D3 mini',
                'Model' => 'Digital MPEG HD DVB-D3mini',
                'S/N' => '00677986904'
            ],
            [
                'Šifra' => '990091',
                'Tip' => 'D3 mini',
                'Model' => 'Digital MPEG HD DVB-D3mini',
                'S/N' => '00638486370'
            ],
            [
                'Šifra' => '990091',
                'Tip' => 'D3 mini',
                'Model' => 'Digital MPEG HD DVB-D3mini',
                'S/N' => '00743682372'
            ],
            [
                'Šifra' => '990091',
                'Tip' => 'D3 mini',
                'Model' => 'Digital MPEG HD DVB-D3mini',
                'S/N' => '00677918930'
            ],
            [
                'Šifra' => '990091',
                'Tip' => 'D3 mini',
                'Model' => 'Digital MPEG HD DVB-D3mini',
                'S/N' => '00753205893'
            ],
            [
                'Šifra' => '990091',
                'Tip' => 'D3 mini',
                'Model' => 'Digital MPEG HD DVB-D3mini',
                'S/N' => '00677935708'
            ],
            [
                'Šifra' => '990091',
                'Tip' => 'D3 mini',
                'Model' => 'Digital MPEG HD DVB-D3mini',
                'S/N' => '00677996926'
            ],
            [
                'Šifra' => '990091',
                'Tip' => 'D3 mini',
                'Model' => 'Digital MPEG HD DVB-D3mini',
                'S/N' => '00677865912'
            ],
            [
                'Šifra' => '990091',
                'Tip' => 'D3 mini',
                'Model' => 'Digital MPEG HD DVB-D3mini',
                'S/N' => '00724009517'
            ],
            [
                'Šifra' => '990091',
                'Tip' => 'D3 mini',
                'Model' => 'Digital MPEG HD DVB-D3mini',
                'S/N' => '00638486130'
            ],
            [
                'Šifra' => '990091',
                'Tip' => 'D3 mini',
                'Model' => 'Digital MPEG HD DVB-D3mini',
                'S/N' => '00677954331'
            ],
            [
                'Šifra' => '990091',
                'Tip' => 'D3 mini',
                'Model' => 'Digital MPEG HD DVB-D3mini',
                'S/N' => '00743689695'
            ],
            [
                'Šifra' => '990091',
                'Tip' => 'D3 mini',
                'Model' => 'Digital MPEG HD DVB-D3mini',
                'S/N' => '00677909748'
            ],
            [
                'Šifra' => '990091',
                'Tip' => 'D3 mini',
                'Model' => 'Digital MPEG HD DVB-D3mini',
                'S/N' => '00753066367'
            ],
            [
                'Šifra' => '990091',
                'Tip' => 'D3 mini',
                'Model' => 'Digital MPEG HD DVB-D3mini',
                'S/N' => '00677966232'
            ],
            [
                'Šifra' => '990091',
                'Tip' => 'D3 mini',
                'Model' => 'Digital MPEG HD DVB-D3mini',
                'S/N' => '00743823173'
            ],
            [
                'Šifra' => '990091',
                'Tip' => 'D3 mini',
                'Model' => 'Digital MPEG HD DVB-D3mini',
                'S/N' => '00724213756'
            ],
            [
                'Šifra' => '990091',
                'Tip' => 'D3 mini',
                'Model' => 'Digital MPEG HD DVB-D3mini',
                'S/N' => '00677968151'
            ],
            [
                'Šifra' => '990091',
                'Tip' => 'D3 mini',
                'Model' => 'Digital MPEG HD DVB-D3mini',
                'S/N' => '00638495211'
            ],
            [
                'Šifra' => '990091',
                'Tip' => 'D3 mini',
                'Model' => 'Digital MPEG HD DVB-D3mini',
                'S/N' => '00677752234'
            ],
            [
                'Šifra' => '990091',
                'Tip' => 'D3 mini',
                'Model' => 'Digital MPEG HD DVB-D3mini',
                'S/N' => '00743703334'
            ],
            [
                'Šifra' => '990091',
                'Tip' => 'D3 mini',
                'Model' => 'Digital MPEG HD DVB-D3mini',
                'S/N' => '00677964102'
            ],
            [
                'Šifra' => '990091',
                'Tip' => 'D3 mini',
                'Model' => 'Digital MPEG HD DVB-D3mini',
                'S/N' => '00724205987'
            ],
            [
                'Šifra' => '990091',
                'Tip' => 'D3 mini',
                'Model' => 'Digital MPEG HD DVB-D3mini',
                'S/N' => '00677946665'
            ],
            [
                'Šifra' => '990091',
                'Tip' => 'D3 mini',
                'Model' => 'Digital MPEG HD DVB-D3mini',
                'S/N' => '00724199792'
            ],
            [
                'Šifra' => '990091',
                'Tip' => 'D3 mini',
                'Model' => 'Digital MPEG HD DVB-D3mini',
                'S/N' => '00677989370'
            ],
            [
                'Šifra' => '990091',
                'Tip' => 'D3 mini',
                'Model' => 'Digital MPEG HD DVB-D3mini',
                'S/N' => '00753071839'
            ],
            [
                'Šifra' => '990091',
                'Tip' => 'D3 mini',
                'Model' => 'Digital MPEG HD DVB-D3mini',
                'S/N' => '00760179476'
            ],
            [
                'Šifra' => '990091',
                'Tip' => 'D3 mini',
                'Model' => 'Digital MPEG HD DVB-D3mini',
                'S/N' => '00753199486'
            ],
            [
                'Šifra' => '990091',
                'Tip' => 'D3 mini',
                'Model' => 'Digital MPEG HD DVB-D3mini',
                'S/N' => '00724203062'
            ],
            [
                'Šifra' => '990091',
                'Tip' => 'D3 mini',
                'Model' => 'Digital MPEG HD DVB-D3mini',
                'S/N' => '00677886567'
            ],
            [
                'Šifra' => '990091',
                'Tip' => 'D3 mini',
                'Model' => 'Digital MPEG HD DVB-D3mini',
                'S/N' => '00743713690'
            ],
            [
                'Šifra' => '990091',
                'Tip' => 'D3 mini',
                'Model' => 'Digital MPEG HD DVB-D3mini',
                'S/N' => '00753250574'
            ],
            [
                'Šifra' => '990091',
                'Tip' => 'D3 mini',
                'Model' => 'Digital MPEG HD DVB-D3mini',
                'S/N' => '00677947468'
            ],
            [
                'Šifra' => '990091',
                'Tip' => 'D3 mini',
                'Model' => 'Digital MPEG HD DVB-D3mini',
                'S/N' => '00677947707'
            ],
            [
                'Šifra' => '990091',
                'Tip' => 'D3 mini',
                'Model' => 'Digital MPEG HD DVB-D3mini',
                'S/N' => '00677907240'
            ],
            [
                'Šifra' => '990091',
                'Tip' => 'D3 mini',
                'Model' => 'Digital MPEG HD DVB-D3mini',
                'S/N' => '00753173382'
            ],
            [
                'Šifra' => '990091',
                'Tip' => 'D3 mini',
                'Model' => 'Digital MPEG HD DVB-D3mini',
                'S/N' => '00677906604'
            ],
            [
                'Šifra' => '990091',
                'Tip' => 'D3 mini',
                'Model' => 'Digital MPEG HD DVB-D3mini',
                'S/N' => '00724198202'
            ],
            [
                'Šifra' => '990091',
                'Tip' => 'D3 mini',
                'Model' => 'Digital MPEG HD DVB-D3mini',
                'S/N' => '00677905242'
            ],
            [
                'Šifra' => '990091',
                'Tip' => 'D3 mini',
                'Model' => 'Digital MPEG HD DVB-D3mini',
                'S/N' => '00677937130'
            ],
            [
                'Šifra' => '990091',
                'Tip' => 'D3 mini',
                'Model' => 'Digital MPEG HD DVB-D3mini',
                'S/N' => '00677912130'
            ],
            [
                'Šifra' => '990091',
                'Tip' => 'D3 mini',
                'Model' => 'Digital MPEG HD DVB-D3mini',
                'S/N' => '00753095599'
            ],
            [
                'Šifra' => '990091',
                'Tip' => 'D3 mini',
                'Model' => 'Digital MPEG HD DVB-D3mini',
                'S/N' => '00753194642'
            ],
            [
                'Šifra' => '990091',
                'Tip' => 'D3 mini',
                'Model' => 'Digital MPEG HD DVB-D3mini',
                'S/N' => '00743683669'
            ],
            [
                'Šifra' => '990091',
                'Tip' => 'D3 mini',
                'Model' => 'Digital MPEG HD DVB-D3mini',
                'S/N' => '00677752450'
            ],
            [
                'Šifra' => '990091',
                'Tip' => 'D3 mini',
                'Model' => 'Digital MPEG HD DVB-D3mini',
                'S/N' => '00724200433'
            ],
            [
                'Šifra' => '990091',
                'Tip' => 'D3 mini',
                'Model' => 'Digital MPEG HD DVB-D3mini',
                'S/N' => '00743819034'
            ],
            [
                'Šifra' => '990091',
                'Tip' => 'D3 mini',
                'Model' => 'Digital MPEG HD DVB-D3mini',
                'S/N' => '00677809319'
            ],
            [
                'Šifra' => '990091',
                'Tip' => 'D3 mini',
                'Model' => 'Digital MPEG HD DVB-D3mini',
                'S/N' => '00743720573'
            ],
            [
                'Šifra' => '990091',
                'Tip' => 'D3 mini',
                'Model' => 'Digital MPEG HD DVB-D3mini',
                'S/N' => '00677921449'
            ],
            [
                'Šifra' => '990091',
                'Tip' => 'D3 mini',
                'Model' => 'Digital MPEG HD DVB-D3mini',
                'S/N' => '00753072403'
            ],
            [
                'Šifra' => '990091',
                'Tip' => 'D3 mini',
                'Model' => 'Digital MPEG HD DVB-D3mini',
                'S/N' => '00753143652'
            ],
            [
                'Šifra' => '990091',
                'Tip' => 'D3 mini',
                'Model' => 'Digital MPEG HD DVB-D3mini',
                'S/N' => '00677877171'
            ],
            [
                'Šifra' => '990091',
                'Tip' => 'D3 mini',
                'Model' => 'Digital MPEG HD DVB-D3mini',
                'S/N' => '00743689532'
            ],
            [
                'Šifra' => '990091',
                'Tip' => 'D3 mini',
                'Model' => 'Digital MPEG HD DVB-D3mini',
                'S/N' => '00677945365'
            ],
            [
                'Šifra' => '990091',
                'Tip' => 'D3 mini',
                'Model' => 'Digital MPEG HD DVB-D3mini',
                'S/N' => '00753084010'
            ],
            [
                'Šifra' => '990091',
                'Tip' => 'D3 mini',
                'Model' => 'Digital MPEG HD DVB-D3mini',
                'S/N' => '00743702858'
            ],
            [
                'Šifra' => '990091',
                'Tip' => 'D3 mini',
                'Model' => 'Digital MPEG HD DVB-D3mini',
                'S/N' => '00743849457'
            ],
            [
                'Šifra' => '990091',
                'Tip' => 'D3 mini',
                'Model' => 'Digital MPEG HD DVB-D3mini',
                'S/N' => '00677963335'
            ],
            [
                'Šifra' => '990091',
                'Tip' => 'D3 mini',
                'Model' => 'Digital MPEG HD DVB-D3mini',
                'S/N' => '00743737663'
            ],
            [
                'Šifra' => '990091',
                'Tip' => 'D3 mini',
                'Model' => 'Digital MPEG HD DVB-D3mini',
                'S/N' => '00724199871'
            ],
            [
                'Šifra' => '990091',
                'Tip' => 'D3 mini',
                'Model' => 'Digital MPEG HD DVB-D3mini',
                'S/N' => '00753099067'
            ],
            [
                'Šifra' => '990091',
                'Tip' => 'D3 mini',
                'Model' => 'Digital MPEG HD DVB-D3mini',
                'S/N' => '00724195247'
            ],
            [
                'Šifra' => '990091',
                'Tip' => 'D3 mini',
                'Model' => 'Digital MPEG HD DVB-D3mini',
                'S/N' => '00743823000'
            ],
            [
                'Šifra' => '990091',
                'Tip' => 'D3 mini',
                'Model' => 'Digital MPEG HD DVB-D3mini',
                'S/N' => '00724203258'
            ],
            [
                'Šifra' => '990091',
                'Tip' => 'D3 mini',
                'Model' => 'Digital MPEG HD DVB-D3mini',
                'S/N' => '00724225590'
            ],
            [
                'Šifra' => '990091',
                'Tip' => 'D3 mini',
                'Model' => 'Digital MPEG HD DVB-D3mini',
                'S/N' => '00724204921'
            ],
            [
                'Šifra' => '990091',
                'Tip' => 'D3 mini',
                'Model' => 'Digital MPEG HD DVB-D3mini',
                'S/N' => '00638424863'
            ],
            [
                'Šifra' => '990091',
                'Tip' => 'D3 mini',
                'Model' => 'Digital MPEG HD DVB-D3mini',
                'S/N' => '00760183424'
            ],
            [
                'Šifra' => '990091',
                'Tip' => 'D3 mini',
                'Model' => 'Digital MPEG HD DVB-D3mini',
                'S/N' => '00638348367'
            ],
            [
                'Šifra' => '990091',
                'Tip' => 'D3 mini',
                'Model' => 'Digital MPEG HD DVB-D3mini',
                'S/N' => '00753173007'
            ],
            [
                'Šifra' => '990091',
                'Tip' => 'D3 mini',
                'Model' => 'Digital MPEG HD DVB-D3mini',
                'S/N' => '00753217079'
            ],
            [
                'Šifra' => '990091',
                'Tip' => 'D3 mini',
                'Model' => 'Digital MPEG HD DVB-D3mini',
                'S/N' => '00753184320'
            ],
            [
                'Šifra' => '990091',
                'Tip' => 'D3 mini',
                'Model' => 'Digital MPEG HD DVB-D3mini',
                'S/N' => '00753087525'
            ],
            [
                'Šifra' => '990091',
                'Tip' => 'D3 mini',
                'Model' => 'Digital MPEG HD DVB-D3mini',
                'S/N' => '00638423386'
            ],
            [
                'Šifra' => '990091',
                'Tip' => 'D3 mini',
                'Model' => 'Digital MPEG HD DVB-D3mini',
                'S/N' => '00753200588'
            ],
            [
                'Šifra' => '990091',
                'Tip' => 'D3 mini',
                'Model' => 'Digital MPEG HD DVB-D3mini',
                'S/N' => '00743812979'
            ],
            [
                'Šifra' => '990091',
                'Tip' => 'D3 mini',
                'Model' => 'Digital MPEG HD DVB-D3mini',
                'S/N' => '00743678321'
            ],
            [
                'Šifra' => '990091',
                'Tip' => 'D3 mini',
                'Model' => 'Digital MPEG HD DVB-D3mini',
                'S/N' => '00677993616'
            ],
            [
                'Šifra' => '990091',
                'Tip' => 'D3 mini',
                'Model' => 'Digital MPEG HD DVB-D3mini',
                'S/N' => '00743691269'
            ],
            [
                'Šifra' => '990091',
                'Tip' => 'D3 mini',
                'Model' => 'Digital MPEG HD DVB-D3mini',
                'S/N' => '00743723342'
            ],
            [
                'Šifra' => '990091',
                'Tip' => 'D3 mini',
                'Model' => 'Digital MPEG HD DVB-D3mini',
                'S/N' => '00677953340'
            ],
            [
                'Šifra' => '990091',
                'Tip' => 'D3 mini',
                'Model' => 'Digital MPEG HD DVB-D3mini',
                'S/N' => '00743725290'
            ],
            [
                'Šifra' => '990091',
                'Tip' => 'D3 mini',
                'Model' => 'Digital MPEG HD DVB-D3mini',
                'S/N' => '00753247719'
            ],
            [
                'Šifra' => '990091',
                'Tip' => 'D3 mini',
                'Model' => 'Digital MPEG HD DVB-D3mini',
                'S/N' => '00638427111'
            ],
            [
                'Šifra' => '990091',
                'Tip' => 'D3 mini',
                'Model' => 'Digital MPEG HD DVB-D3mini',
                'S/N' => '00638425727'
            ],
            [
                'Šifra' => '990091',
                'Tip' => 'D3 mini',
                'Model' => 'Digital MPEG HD DVB-D3mini',
                'S/N' => '00743792257'
            ],
            [
                'Šifra' => '990091',
                'Tip' => 'D3 mini',
                'Model' => 'Digital MPEG HD DVB-D3mini',
                'S/N' => '00753204133'
            ],
            [
                'Šifra' => '990091',
                'Tip' => 'D3 mini',
                'Model' => 'Digital MPEG HD DVB-D3mini',
                'S/N' => '00677930222'
            ],
            [
                'Šifra' => '990091',
                'Tip' => 'D3 mini',
                'Model' => 'Digital MPEG HD DVB-D3mini',
                'S/N' => '00677955241'
            ],
            [
                'Šifra' => '990091',
                'Tip' => 'D3 mini',
                'Model' => 'Digital MPEG HD DVB-D3mini',
                'S/N' => '00677890587'
            ],
            [
                'Šifra' => '990091',
                'Tip' => 'D3 mini',
                'Model' => 'Digital MPEG HD DVB-D3mini',
                'S/N' => '00753188248'
            ],
            [
                'Šifra' => '990091',
                'Tip' => 'D3 mini',
                'Model' => 'Digital MPEG HD DVB-D3mini',
                'S/N' => '00724004152'
            ],
            [
                'Šifra' => '990091',
                'Tip' => 'D3 mini',
                'Model' => 'Digital MPEG HD DVB-D3mini',
                'S/N' => '00753094937'
            ],
            [
                'Šifra' => '990091',
                'Tip' => 'D3 mini',
                'Model' => 'Digital MPEG HD DVB-D3mini',
                'S/N' => '00677871190'
            ],
            [
                'Šifra' => '990091',
                'Tip' => 'D3 mini',
                'Model' => 'Digital MPEG HD DVB-D3mini',
                'S/N' => '00743693533'
            ],
            [
                'Šifra' => '990091',
                'Tip' => 'D3 mini',
                'Model' => 'Digital MPEG HD DVB-D3mini',
                'S/N' => '00753081784'
            ],
            [
                'Šifra' => '990091',
                'Tip' => 'D3 mini',
                'Model' => 'Digital MPEG HD DVB-D3mini',
                'S/N' => '00760169593'
            ],
            [
                'Šifra' => '990091',
                'Tip' => 'D3 mini',
                'Model' => 'Digital MPEG HD DVB-D3mini',
                'S/N' => '00743825240'
            ],
            [
                'Šifra' => '990091',
                'Tip' => 'D3 mini',
                'Model' => 'Digital MPEG HD DVB-D3mini',
                'S/N' => '00760114915'
            ],
            [
                'Šifra' => '990091',
                'Tip' => 'D3 mini',
                'Model' => 'Digital MPEG HD DVB-D3mini',
                'S/N' => '00677895234'
            ],
            [
                'Šifra' => '990091',
                'Tip' => 'D3 mini',
                'Model' => 'Digital MPEG HD DVB-D3mini',
                'S/N' => '00743811989'
            ],
            [
                'Šifra' => '990091',
                'Tip' => 'D3 mini',
                'Model' => 'Digital MPEG HD DVB-D3mini',
                'S/N' => '00743681122'
            ],
            [
                'Šifra' => '990091',
                'Tip' => 'D3 mini',
                'Model' => 'Digital MPEG HD DVB-D3mini',
                'S/N' => '00753082973'
            ],
            [
                'Šifra' => '990091',
                'Tip' => 'D3 mini',
                'Model' => 'Digital MPEG HD DVB-D3mini',
                'S/N' => '00677944445'
            ],
            [
                'Šifra' => '990091',
                'Tip' => 'D3 mini',
                'Model' => 'Digital MPEG HD DVB-D3mini',
                'S/N' => '00638403001'
            ],
            [
                'Šifra' => '990091',
                'Tip' => 'D3 mini',
                'Model' => 'Digital MPEG HD DVB-D3mini',
                'S/N' => '00724170930'
            ],
            [
                'Šifra' => '990091',
                'Tip' => 'D3 mini',
                'Model' => 'Digital MPEG HD DVB-D3mini',
                'S/N' => '00677755406'
            ],
            [
                'Šifra' => '990091',
                'Tip' => 'D3 mini',
                'Model' => 'Digital MPEG HD DVB-D3mini',
                'S/N' => '00743824965'
            ],
            [
                'Šifra' => '990091',
                'Tip' => 'D3 mini',
                'Model' => 'Digital MPEG HD DVB-D3mini',
                'S/N' => '00677908697'
            ],
            [
                'Šifra' => '990091',
                'Tip' => 'D3 mini',
                'Model' => 'Digital MPEG HD DVB-D3mini',
                'S/N' => '00753095471'
            ],
            [
                'Šifra' => '990091',
                'Tip' => 'D3 mini',
                'Model' => 'Digital MPEG HD DVB-D3mini',
                'S/N' => '00638421029'
            ],
            [
                'Šifra' => '990091',
                'Tip' => 'D3 mini',
                'Model' => 'Digital MPEG HD DVB-D3mini',
                'S/N' => '00677942358'
            ],
            [
                'Šifra' => '990091',
                'Tip' => 'D3 mini',
                'Model' => 'Digital MPEG HD DVB-D3mini',
                'S/N' => '00677921393'
            ],
            [
                'Šifra' => '990091',
                'Tip' => 'D3 mini',
                'Model' => 'Digital MPEG HD DVB-D3mini',
                'S/N' => '00753188727'
            ],
            [
                'Šifra' => '990091',
                'Tip' => 'D3 mini',
                'Model' => 'Digital MPEG HD DVB-D3mini',
                'S/N' => '00743669145'
            ],
            [
                'Šifra' => '990091',
                'Tip' => 'D3 mini',
                'Model' => 'Digital MPEG HD DVB-D3mini',
                'S/N' => '00724173734'
            ],
            [
                'Šifra' => '990091',
                'Tip' => 'D3 mini',
                'Model' => 'Digital MPEG HD DVB-D3mini',
                'S/N' => '00753084481'
            ],
            [
                'Šifra' => '990091',
                'Tip' => 'D3 mini',
                'Model' => 'Digital MPEG HD DVB-D3mini',
                'S/N' => '00724209653'
            ],
            [
                'Šifra' => '990091',
                'Tip' => 'D3 mini',
                'Model' => 'Digital MPEG HD DVB-D3mini',
                'S/N' => '00677754496'
            ],
            [
                'Šifra' => '990091',
                'Tip' => 'D3 mini',
                'Model' => 'Digital MPEG HD DVB-D3mini',
                'S/N' => '00724215593'
            ],
            [
                'Šifra' => '990091',
                'Tip' => 'D3 mini',
                'Model' => 'Digital MPEG HD DVB-D3mini',
                'S/N' => '00743684847'
            ],
            [
                'Šifra' => '990091',
                'Tip' => 'D3 mini',
                'Model' => 'Digital MPEG HD DVB-D3mini',
                'S/N' => '00753172299'
            ],
            [
                'Šifra' => '990091',
                'Tip' => 'D3 mini',
                'Model' => 'Digital MPEG HD DVB-D3mini',
                'S/N' => '00638485914'
            ],
            [
                'Šifra' => '990091',
                'Tip' => 'D3 mini',
                'Model' => 'Digital MPEG HD DVB-D3mini',
                'S/N' => '00677951277'
            ],
            [
                'Šifra' => '990091',
                'Tip' => 'D3 mini',
                'Model' => 'Digital MPEG HD DVB-D3mini',
                'S/N' => '00743666615'
            ],
            [
                'Šifra' => '990091',
                'Tip' => 'D3 mini',
                'Model' => 'Digital MPEG HD DVB-D3mini',
                'S/N' => '00753143234'
            ],
            [
                'Šifra' => '990091',
                'Tip' => 'D3 mini',
                'Model' => 'Digital MPEG HD DVB-D3mini',
                'S/N' => '00743680342'
            ],
            [
                'Šifra' => '990091',
                'Tip' => 'D3 mini',
                'Model' => 'Digital MPEG HD DVB-D3mini',
                'S/N' => '00743742330'
            ],
            [
                'Šifra' => '990091',
                'Tip' => 'D3 mini',
                'Model' => 'Digital MPEG HD DVB-D3mini',
                'S/N' => '00677864052'
            ],
            [
                'Šifra' => '990091',
                'Tip' => 'D3 mini',
                'Model' => 'Digital MPEG HD DVB-D3mini',
                'S/N' => '00753136775'
            ],
            [
                'Šifra' => '990091',
                'Tip' => 'D3 mini',
                'Model' => 'Digital MPEG HD DVB-D3mini',
                'S/N' => '00753136775'
            ],
            [
                'Šifra' => '990091',
                'Tip' => 'D3 mini',
                'Model' => 'Digital MPEG HD DVB-D3mini',
                'S/N' => '00724180308'
            ],
            [
                'Šifra' => '990091',
                'Tip' => 'D3 mini',
                'Model' => 'Digital MPEG HD DVB-D3mini',
                'S/N' => '00677968957'
            ],
            [
                'Šifra' => '990091',
                'Tip' => 'D3 mini',
                'Model' => 'Digital MPEG HD DVB-D3mini',
                'S/N' => '00677947139'
            ],
            [
                'Šifra' => '990091',
                'Tip' => 'D3 mini',
                'Model' => 'Digital MPEG HD DVB-D3mini',
                'S/N' => '00724175687'
            ],
            [
                'Šifra' => '990091',
                'Tip' => 'D3 mini',
                'Model' => 'Digital MPEG HD DVB-D3mini',
                'S/N' => '00753093051'
            ],
            [
                'Šifra' => '990091',
                'Tip' => 'D3 mini',
                'Model' => 'Digital MPEG HD DVB-D3mini',
                'S/N' => '00677949328'
            ],
            [
                'Šifra' => '990091',
                'Tip' => 'D3 mini',
                'Model' => 'Digital MPEG HD DVB-D3mini',
                'S/N' => '00753205791'
            ],
            [
                'Šifra' => '990091',
                'Tip' => 'D3 mini',
                'Model' => 'Digital MPEG HD DVB-D3mini',
                'S/N' => '00753053456'
            ],
            [
                'Šifra' => '990091',
                'Tip' => 'D3 mini',
                'Model' => 'Digital MPEG HD DVB-D3mini',
                'S/N' => '00743809286'
            ],
            [
                'Šifra' => '990091',
                'Tip' => 'D3 mini',
                'Model' => 'Digital MPEG HD DVB-D3mini',
                'S/N' => '00724201006'
            ],
            [
                'Šifra' => '990091',
                'Tip' => 'D3 mini',
                'Model' => 'Digital MPEG HD DVB-D3mini',
                'S/N' => '00753235887'
            ],
            [
                'Šifra' => '990091',
                'Tip' => 'D3 mini',
                'Model' => 'Digital MPEG HD DVB-D3mini',
                'S/N' => '00743736012'
            ],
            [
                'Šifra' => '990091',
                'Tip' => 'D3 mini',
                'Model' => 'Digital MPEG HD DVB-D3mini',
                'S/N' => '00677799316'
            ],
            [
                'Šifra' => '990091',
                'Tip' => 'D3 mini',
                'Model' => 'Digital MPEG HD DVB-D3mini',
                'S/N' => '00753207613'
            ],
            [
                'Šifra' => '990091',
                'Tip' => 'D3 mini',
                'Model' => 'Digital MPEG HD DVB-D3mini',
                'S/N' => '00677984521'
            ],
            [
                'Šifra' => '990091',
                'Tip' => 'D3 mini',
                'Model' => 'Digital MPEG HD DVB-D3mini',
                'S/N' => '00638403485'
            ],
            [
                'Šifra' => '990091',
                'Tip' => 'D3 mini',
                'Model' => 'Digital MPEG HD DVB-D3mini',
                'S/N' => '00677938205'
            ],
            [
                'Šifra' => '990091',
                'Tip' => 'D3 mini',
                'Model' => 'Digital MPEG HD DVB-D3mini',
                'S/N' => '00677942163'
            ],
            [
                'Šifra' => '990091',
                'Tip' => 'D3 mini',
                'Model' => 'Digital MPEG HD DVB-D3mini',
                'S/N' => '00743814821'
            ],
            [
                'Šifra' => '990091',
                'Tip' => 'D3 mini',
                'Model' => 'Digital MPEG HD DVB-D3mini',
                'S/N' => '00677940785'
            ],
            [
                'Šifra' => '990091',
                'Tip' => 'D3 mini',
                'Model' => 'Digital MPEG HD DVB-D3mini',
                'S/N' => '00743815440'
            ],
            [
                'Šifra' => '990091',
                'Tip' => 'D3 mini',
                'Model' => 'Digital MPEG HD DVB-D3mini',
                'S/N' => '00677934149'
            ],
            [
                'Šifra' => '990091',
                'Tip' => 'D3 mini',
                'Model' => 'Digital MPEG HD DVB-D3mini',
                'S/N' => '00724180946'
            ],
            [
                'Šifra' => '990291',
                'Tip' => 'D3 mini',
                'Model' => 'D3 mini  GCH- 6720 CH',
                'S/N' => '00755705965'
            ],
            [
                'Šifra' => '990291',
                'Tip' => 'D3 mini',
                'Model' => 'D3 mini  GCH- 6720 CH',
                'S/N' => '00755700245'
            ],
            [
                'Šifra' => '990291',
                'Tip' => 'D3 mini',
                'Model' => 'D3 mini  GCH- 6720 CH',
                'S/N' => '00755703596'
            ],
            [
                'Šifra' => '990291',
                'Tip' => 'D3 mini',
                'Model' => 'D3 mini  GCH- 6720 CH',
                'S/N' => '00755694596'
            ],
            [
                'Šifra' => '990291',
                'Tip' => 'D3 mini',
                'Model' => 'D3 mini  GCH- 6720 CH',
                'S/N' => '00755705543'
            ],
            [
                'Šifra' => '990291',
                'Tip' => 'D3 mini',
                'Model' => 'D3 mini  GCH- 6720 CH',
                'S/N' => '00755694951'
            ],
            [
                'Šifra' => '990291',
                'Tip' => 'D3 mini',
                'Model' => 'D3 mini  GCH- 6720 CH',
                'S/N' => '00755706691'
            ],
            [
                'Šifra' => '990291',
                'Tip' => 'D3 mini',
                'Model' => 'D3 mini  GCH- 6720 CH',
                'S/N' => '00755696594'
            ],
            [
                'Šifra' => '990291',
                'Tip' => 'D3 mini',
                'Model' => 'D3 mini  GCH- 6720 CH',
                'S/N' => '00755706123'
            ],
            [
                'Šifra' => '990291',
                'Tip' => 'D3 mini',
                'Model' => 'D3 mini  GCH- 6720 CH',
                'S/N' => '00755697030'
            ],
            [
                'Šifra' => '990291',
                'Tip' => 'D3 mini',
                'Model' => 'D3 mini  GCH- 6720 CH',
                'S/N' => '00755696590'
            ],
            [
                'Šifra' => '990291',
                'Tip' => 'D3 mini',
                'Model' => 'D3 mini  GCH- 6720 CH',
                'S/N' => '00755178912'
            ],
            [
                'Šifra' => '990291',
                'Tip' => 'D3 mini',
                'Model' => 'D3 mini  GCH- 6720 CH',
                'S/N' => '00755697692'
            ],
            [
                'Šifra' => '990291',
                'Tip' => 'D3 mini',
                'Model' => 'D3 mini  GCH- 6720 CH',
                'S/N' => '00755697691'
            ],
            [
                'Šifra' => '990291',
                'Tip' => 'D3 mini',
                'Model' => 'D3 mini  GCH- 6720 CH',
                'S/N' => '00755700089'
            ],
            [
                'Šifra' => '990291',
                'Tip' => 'D3 mini',
                'Model' => 'D3 mini  GCH- 6720 CH',
                'S/N' => '00755695978'
            ],
            [
                'Šifra' => '990291',
                'Tip' => 'D3 mini',
                'Model' => 'D3 mini  GCH- 6720 CH',
                'S/N' => '00755696252'
            ],
            [
                'Šifra' => '990291',
                'Tip' => 'D3 mini',
                'Model' => 'D3 mini  GCH- 6720 CH',
                'S/N' => '00755695972'
            ],
            [
                'Šifra' => '990291',
                'Tip' => 'D3 mini',
                'Model' => 'D3 mini  GCH- 6720 CH',
                'S/N' => '00755708187'
            ],
            [
                'Šifra' => '990291',
                'Tip' => 'D3 mini',
                'Model' => 'D3 mini  GCH- 6720 CH',
                'S/N' => '00755706321'
            ],
            [
                'Šifra' => '990291',
                'Tip' => 'D3 mini',
                'Model' => 'D3 mini  GCH- 6720 CH',
                'S/N' => '00755703851'
            ],
            [
                'Šifra' => '990291',
                'Tip' => 'D3 mini',
                'Model' => 'D3 mini  GCH- 6720 CH',
                'S/N' => '00755705063'
            ],
            [
                'Šifra' => '990291',
                'Tip' => 'D3 mini',
                'Model' => 'D3 mini  GCH- 6720 CH',
                'S/N' => '00755703821'
            ],
            [
                'Šifra' => '990291',
                'Tip' => 'D3 mini',
                'Model' => 'D3 mini  GCH- 6720 CH',
                'S/N' => '00755708815'
            ],
            [
                'Šifra' => '990291',
                'Tip' => 'D3 mini',
                'Model' => 'D3 mini  GCH- 6720 CH',
                'S/N' => '00755706674'
            ],
            [
                'Šifra' => '990291',
                'Tip' => 'D3 mini',
                'Model' => 'D3 mini  GCH- 6720 CH',
                'S/N' => '00755703692'
            ],
            [
                'Šifra' => '990291',
                'Tip' => 'D3 mini',
                'Model' => 'D3 mini  GCH- 6720 CH',
                'S/N' => '00755705919'
            ],
            [
                'Šifra' => '990291',
                'Tip' => 'D3 mini',
                'Model' => 'D3 mini  GCH- 6720 CH',
                'S/N' => '00755695543'
            ],
            [
                'Šifra' => '990291',
                'Tip' => 'D3 mini',
                'Model' => 'D3 mini  GCH- 6720 CH',
                'S/N' => '00755705914'
            ],
            [
                'Šifra' => '990291',
                'Tip' => 'D3 mini',
                'Model' => 'D3 mini  GCH- 6720 CH',
                'S/N' => '00755705160'
            ],
            [
                'Šifra' => '990291',
                'Tip' => 'D3 mini',
                'Model' => 'D3 mini  GCH- 6720 CH',
                'S/N' => '00755703735'
            ],
            [
                'Šifra' => '990291',
                'Tip' => 'D3 mini',
                'Model' => 'D3 mini  GCH- 6720 CH',
                'S/N' => '00755705980'
            ],
            [
                'Šifra' => '990291',
                'Tip' => 'D3 mini',
                'Model' => 'D3 mini  GCH- 6720 CH',
                'S/N' => '00755708722'
            ],
            [
                'Šifra' => '990291',
                'Tip' => 'D3 mini',
                'Model' => 'D3 mini  GCH- 6720 CH',
                'S/N' => '00755708305'
            ],
            [
                'Šifra' => '990291',
                'Tip' => 'D3 mini',
                'Model' => 'D3 mini  GCH- 6720 CH',
                'S/N' => '00755706430'
            ],
            [
                'Šifra' => '990291',
                'Tip' => 'D3 mini',
                'Model' => 'D3 mini  GCH- 6720 CH',
                'S/N' => '00755706707'
            ],
            [
                'Šifra' => '990291',
                'Tip' => 'D3 mini',
                'Model' => 'D3 mini  GCH- 6720 CH',
                'S/N' => '00755708679'
            ],
            [
                'Šifra' => '990291',
                'Tip' => 'D3 mini',
                'Model' => 'D3 mini  GCH- 6720 CH',
                'S/N' => '00755706660'
            ],
            [
                'Šifra' => '990291',
                'Tip' => 'D3 mini',
                'Model' => 'D3 mini  GCH- 6720 CH',
                'S/N' => '00755703714'
            ],
            [
                'Šifra' => '990291',
                'Tip' => 'D3 mini',
                'Model' => 'D3 mini  GCH- 6720 CH',
                'S/N' => '00755703712'
            ],
            [
                'Šifra' => '990291',
                'Tip' => 'D3 mini',
                'Model' => 'D3 mini  GCH- 6720 CH',
                'S/N' => '00755703720'
            ],
            [
                'Šifra' => '990291',
                'Tip' => 'D3 mini',
                'Model' => 'D3 mini  GCH- 6720 CH',
                'S/N' => '00755706042'
            ],
            [
                'Šifra' => '990291',
                'Tip' => 'D3 mini',
                'Model' => 'D3 mini  GCH- 6720 CH',
                'S/N' => '00755700072'
            ],
            [
                'Šifra' => '990291',
                'Tip' => 'D3 mini',
                'Model' => 'D3 mini  GCH- 6720 CH',
                'S/N' => '00755701999'
            ],
            [
                'Šifra' => '990291',
                'Tip' => 'D3 mini',
                'Model' => 'D3 mini  GCH- 6720 CH',
                'S/N' => '00755705689'
            ],
            [
                'Šifra' => '990291',
                'Tip' => 'D3 mini',
                'Model' => 'D3 mini  GCH- 6720 CH',
                'S/N' => '00755700269'
            ],
            [
                'Šifra' => '990291',
                'Tip' => 'D3 mini',
                'Model' => 'D3 mini  GCH- 6720 CH',
                'S/N' => '00755702993'
            ],
            [
                'Šifra' => '990291',
                'Tip' => 'D3 mini',
                'Model' => 'D3 mini  GCH- 6720 CH',
                'S/N' => '00755700293'
            ],
            [
                'Šifra' => '990291',
                'Tip' => 'D3 mini',
                'Model' => 'D3 mini  GCH- 6720 CH',
                'S/N' => '00755704026'
            ],
            [
                'Šifra' => '990291',
                'Tip' => 'D3 mini',
                'Model' => 'D3 mini  GCH- 6720 CH',
                'S/N' => '00755708655'
            ],
            [
                'Šifra' => '990291',
                'Tip' => 'D3 mini',
                'Model' => 'D3 mini  GCH- 6720 CH',
                'S/N' => '00755694543'
            ],
            [
                'Šifra' => '990291',
                'Tip' => 'D3 mini',
                'Model' => 'D3 mini  GCH- 6720 CH',
                'S/N' => '00755708680'
            ],
            [
                'Šifra' => '990291',
                'Tip' => 'D3 mini',
                'Model' => 'D3 mini  GCH- 6720 CH',
                'S/N' => '00755705951'
            ],
            [
                'Šifra' => '990291',
                'Tip' => 'D3 mini',
                'Model' => 'D3 mini  GCH- 6720 CH',
                'S/N' => '00755705139'
            ],
            [
                'Šifra' => '990291',
                'Tip' => 'D3 mini',
                'Model' => 'D3 mini  GCH- 6720 CH',
                'S/N' => '00755699713'
            ],
            [
                'Šifra' => '990291',
                'Tip' => 'D3 mini',
                'Model' => 'D3 mini  GCH- 6720 CH',
                'S/N' => '00755706370'
            ],
            [
                'Šifra' => '990291',
                'Tip' => 'D3 mini',
                'Model' => 'D3 mini  GCH- 6720 CH',
                'S/N' => '00755705896'
            ],
            [
                'Šifra' => '990291',
                'Tip' => 'D3 mini',
                'Model' => 'D3 mini  GCH- 6720 CH',
                'S/N' => '00755705168'
            ],
            [
                'Šifra' => '990291',
                'Tip' => 'D3 mini',
                'Model' => 'D3 mini  GCH- 6720 CH',
                'S/N' => '00755154408'
            ],
            [
                'Šifra' => '990291',
                'Tip' => 'D3 mini',
                'Model' => 'D3 mini  GCH- 6720 CH',
                'S/N' => '00755705000'
            ],
            [
                'Šifra' => '990291',
                'Tip' => 'D3 mini',
                'Model' => 'D3 mini  GCH- 6720 CH',
                'S/N' => '00755706721'
            ],
            [
                'Šifra' => '990291',
                'Tip' => 'D3 mini',
                'Model' => 'D3 mini  GCH- 6720 CH',
                'S/N' => '00755696911'
            ],
            [
                'Šifra' => '990291',
                'Tip' => 'D3 mini',
                'Model' => 'D3 mini  GCH- 6720 CH',
                'S/N' => '00755704273'
            ],
            [
                'Šifra' => '990291',
                'Tip' => 'D3 mini',
                'Model' => 'D3 mini  GCH- 6720 CH',
                'S/N' => '00755696523'
            ],
            [
                'Šifra' => '990291',
                'Tip' => 'D3 mini',
                'Model' => 'D3 mini  GCH- 6720 CH',
                'S/N' => '00755696381'
            ],
            [
                'Šifra' => '990291',
                'Tip' => 'D3 mini',
                'Model' => 'D3 mini  GCH- 6720 CH',
                'S/N' => '00755695994'
            ],
            [
                'Šifra' => '990291',
                'Tip' => 'D3 mini',
                'Model' => 'D3 mini  GCH- 6720 CH',
                'S/N' => '00755696314'
            ],
            [
                'Šifra' => '990291',
                'Tip' => 'D3 mini',
                'Model' => 'D3 mini  GCH- 6720 CH',
                'S/N' => '00755696421'
            ],
            [
                'Šifra' => '990291',
                'Tip' => 'D3 mini',
                'Model' => 'D3 mini  GCH- 6720 CH',
                'S/N' => '00755695980'
            ],
            [
                'Šifra' => '990291',
                'Tip' => 'D3 mini',
                'Model' => 'D3 mini  GCH- 6720 CH',
                'S/N' => '00755695949'
            ],
            [
                'Šifra' => '990291',
                'Tip' => 'D3 mini',
                'Model' => 'D3 mini  GCH- 6720 CH',
                'S/N' => '00755696140'
            ],
            [
                'Šifra' => '990291',
                'Tip' => 'D3 mini',
                'Model' => 'D3 mini  GCH- 6720 CH',
                'S/N' => '00755694879'
            ],
            [
                'Šifra' => '990291',
                'Tip' => 'D3 mini',
                'Model' => 'D3 mini  GCH- 6720 CH',
                'S/N' => '00755706026'
            ],
            [
                'Šifra' => '990291',
                'Tip' => 'D3 mini',
                'Model' => 'D3 mini  GCH- 6720 CH',
                'S/N' => '00755694474'
            ],
            [
                'Šifra' => '990291',
                'Tip' => 'D3 mini',
                'Model' => 'D3 mini  GCH- 6720 CH',
                'S/N' => '00755694454'
            ],
            [
                'Šifra' => '990291',
                'Tip' => 'D3 mini',
                'Model' => 'D3 mini  GCH- 6720 CH',
                'S/N' => '00755694542'
            ],
            [
                'Šifra' => '990291',
                'Tip' => 'D3 mini',
                'Model' => 'D3 mini  GCH- 6720 CH',
                'S/N' => '00755708737'
            ],
            [
                'Šifra' => '990291',
                'Tip' => 'D3 mini',
                'Model' => 'D3 mini  GCH- 6720 CH',
                'S/N' => '00755694495'
            ],
            [
                'Šifra' => '990291',
                'Tip' => 'D3 mini',
                'Model' => 'D3 mini  GCH- 6720 CH',
                'S/N' => '00755694493'
            ],
            [
                'Šifra' => '990291',
                'Tip' => 'D3 mini',
                'Model' => 'D3 mini  GCH- 6720 CH',
                'S/N' => '00755705594'
            ],
            [
                'Šifra' => '990291',
                'Tip' => 'D3 mini',
                'Model' => 'D3 mini  GCH- 6720 CH',
                'S/N' => '00755694914'
            ],
            [
                'Šifra' => '990291',
                'Tip' => 'D3 mini',
                'Model' => 'D3 mini  GCH- 6720 CH',
                'S/N' => '00755694449'
            ],
            [
                'Šifra' => '990291',
                'Tip' => 'D3 mini',
                'Model' => 'D3 mini  GCH- 6720 CH',
                'S/N' => '00755695934'
            ],
            [
                'Šifra' => '990291',
                'Tip' => 'D3 mini',
                'Model' => 'D3 mini  GCH- 6720 CH',
                'S/N' => '00755695986'
            ],
            [
                'Šifra' => '990291',
                'Tip' => 'D3 mini',
                'Model' => 'D3 mini  GCH- 6720 CH',
                'S/N' => '00755154394'
            ],
            [
                'Šifra' => '990291',
                'Tip' => 'D3 mini',
                'Model' => 'D3 mini  GCH- 6720 CH',
                'S/N' => '00755696042'
            ],
            [
                'Šifra' => '990291',
                'Tip' => 'D3 mini',
                'Model' => 'D3 mini  GCH- 6720 CH',
                'S/N' => '00755695926'
            ],
            [
                'Šifra' => '990191',
                'Tip' => 'D3 mini',
                'Model' => 'D3 mini cardless GL-9C36 NG',
                'S/N' => '00757991131'
            ],
            [
                'Šifra' => '990191',
                'Tip' => 'D3 mini',
                'Model' => 'D3 mini cardless GL-9C36 NG',
                'S/N' => '00757997920'
            ],
            [
                'Šifra' => '990191',
                'Tip' => 'D3 mini',
                'Model' => 'D3 mini cardless GL-9C36 NG',
                'S/N' => '00757991060'
            ],
            [
                'Šifra' => '990191',
                'Tip' => 'D3 mini',
                'Model' => 'D3 mini cardless GL-9C36 NG',
                'S/N' => '00758019855'
            ],
            [
                'Šifra' => '990091',
                'Tip' => 'D3 mini',
                'Model' => 'Digital MPEG HD DVB-D3mini',
                'S/N' => '00760113873'
            ],
            [
                'Šifra' => '990091',
                'Tip' => 'D3 mini',
                'Model' => 'Digital MPEG HD DVB-D3mini',
                'S/N' => '00743694519'
            ],
            [
                'Šifra' => '990091',
                'Tip' => 'D3 mini',
                'Model' => 'Digital MPEG HD DVB-D3mini',
                'S/N' => '00753195695'
            ],
            [
                'Šifra' => '990091',
                'Tip' => 'D3 mini',
                'Model' => 'Digital MPEG HD DVB-D3mini',
                'S/N' => '00677945050'
            ],
            [
                'Šifra' => '990091',
                'Tip' => 'D3 mini',
                'Model' => 'Digital MPEG HD DVB-D3mini',
                'S/N' => '00753211189'
            ],
            [
                'Šifra' => '990091',
                'Tip' => 'D3 mini',
                'Model' => 'Digital MPEG HD DVB-D3mini',
                'S/N' => '00753171110'
            ],
            [
                'Šifra' => '990091',
                'Tip' => 'D3 mini',
                'Model' => 'Digital MPEG HD DVB-D3mini',
                'S/N' => '00753180756'
            ],
            [
                'Šifra' => '990091',
                'Tip' => 'D3 mini',
                'Model' => 'Digital MPEG HD DVB-D3mini',
                'S/N' => '00753091635'
            ],
            [
                'Šifra' => '990091',
                'Tip' => 'D3 mini',
                'Model' => 'Digital MPEG HD DVB-D3mini',
                'S/N' => '00677951892'
            ],
            [
                'Šifra' => '990091',
                'Tip' => 'D3 mini',
                'Model' => 'Digital MPEG HD DVB-D3mini',
                'S/N' => '00753207814'
            ],
            [
                'Šifra' => '990091',
                'Tip' => 'D3 mini',
                'Model' => 'Digital MPEG HD DVB-D3mini',
                'S/N' => '00753220699'
            ],
            [
                'Šifra' => '990091',
                'Tip' => 'D3 mini',
                'Model' => 'Digital MPEG HD DVB-D3mini',
                'S/N' => '00753170506'
            ],
            [
                'Šifra' => '990091',
                'Tip' => 'D3 mini',
                'Model' => 'Digital MPEG HD DVB-D3mini',
                'S/N' => '00638496988'
            ],
            [
                'Šifra' => '990091',
                'Tip' => 'D3 mini',
                'Model' => 'Digital MPEG HD DVB-D3mini',
                'S/N' => '00638494819'
            ],
            [
                'Šifra' => '990091',
                'Tip' => 'D3 mini',
                'Model' => 'Digital MPEG HD DVB-D3mini',
                'S/N' => '00753179495'
            ],
            [
                'Šifra' => '990091',
                'Tip' => 'D3 mini',
                'Model' => 'Digital MPEG HD DVB-D3mini',
                'S/N' => '00724169543'
            ],
            [
                'Šifra' => '990091',
                'Tip' => 'D3 mini',
                'Model' => 'Digital MPEG HD DVB-D3mini',
                'S/N' => '00753179201'
            ],
            [
                'Šifra' => '990091',
                'Tip' => 'D3 mini',
                'Model' => 'Digital MPEG HD DVB-D3mini',
                'S/N' => '00677963641'
            ],
            [
                'Šifra' => '990091',
                'Tip' => 'D3 mini',
                'Model' => 'Digital MPEG HD DVB-D3mini',
                'S/N' => '00753210915'
            ],
            [
                'Šifra' => '990091',
                'Tip' => 'D3 mini',
                'Model' => 'Digital MPEG HD DVB-D3mini',
                'S/N' => '00753185855'
            ],
            [
                'Šifra' => '990091',
                'Tip' => 'D3 mini',
                'Model' => 'Digital MPEG HD DVB-D3mini',
                'S/N' => '00753195369'
            ],
            [
                'Šifra' => '990091',
                'Tip' => 'D3 mini',
                'Model' => 'Digital MPEG HD DVB-D3mini',
                'S/N' => '00724156790'
            ],
            [
                'Šifra' => '990091',
                'Tip' => 'D3 mini',
                'Model' => 'Digital MPEG HD DVB-D3mini',
                'S/N' => '00677916587'
            ],
            [
                'Šifra' => '990091',
                'Tip' => 'D3 mini',
                'Model' => 'Digital MPEG HD DVB-D3mini',
                'S/N' => '00753193353'
            ],
            [
                'Šifra' => '990091',
                'Tip' => 'D3 mini',
                'Model' => 'Digital MPEG HD DVB-D3mini',
                'S/N' => '00753195494'
            ],
            [
                'Šifra' => '990091',
                'Tip' => 'D3 mini',
                'Model' => 'Digital MPEG HD DVB-D3mini',
                'S/N' => '00724168974'
            ],
            [
                'Šifra' => '990091',
                'Tip' => 'D3 mini',
                'Model' => 'Digital MPEG HD DVB-D3mini',
                'S/N' => '00677962918'
            ],
            [
                'Šifra' => '990191',
                'Tip' => 'D3 mini',
                'Model' => 'D3 mini cardless GL-9C36 NG',
                'S/N' => '00758011100'
            ],
            [
                'Šifra' => '990091',
                'Tip' => 'D3 mini',
                'Model' => 'Digital MPEG HD DVB-D3mini',
                'S/N' => '00753176638'
            ],
            [
                'Šifra' => '990091',
                'Tip' => 'D3 mini',
                'Model' => 'Digital MPEG HD DVB-D3mini',
                'S/N' => '00743670154'
            ],
            [
                'Šifra' => '990091',
                'Tip' => 'D3 mini',
                'Model' => 'Digital MPEG HD DVB-D3mini',
                'S/N' => '00743739748'
            ],
            [
                'Šifra' => '990091',
                'Tip' => 'D3 mini',
                'Model' => 'Digital MPEG HD DVB-D3mini',
                'S/N' => '00677964467'
            ],
            [
                'Šifra' => '990091',
                'Tip' => 'D3 mini',
                'Model' => 'Digital MPEG HD DVB-D3mini',
                'S/N' => '00743798556'
            ],
            [
                'Šifra' => '990091',
                'Tip' => 'D3 mini',
                'Model' => 'Digital MPEG HD DVB-D3mini',
                'S/N' => '00753167760'
            ],
            [
                'Šifra' => '990091',
                'Tip' => 'D3 mini',
                'Model' => 'Digital MPEG HD DVB-D3mini',
                'S/N' => '00677961214'
            ],
            [
                'Šifra' => '990091',
                'Tip' => 'D3 mini',
                'Model' => 'Digital MPEG HD DVB-D3mini',
                'S/N' => '00753209320'
            ],
            [
                'Šifra' => '990091',
                'Tip' => 'D3 mini',
                'Model' => 'Digital MPEG HD DVB-D3mini',
                'S/N' => '00753222958'
            ],
            [
                'Šifra' => '990091',
                'Tip' => 'D3 mini',
                'Model' => 'Digital MPEG HD DVB-D3mini',
                'S/N' => '00753195543'
            ],
            [
                'Šifra' => '990091',
                'Tip' => 'D3 mini',
                'Model' => 'Digital MPEG HD DVB-D3mini',
                'S/N' => '00753210981'
            ],
            [
                'Šifra' => '990091',
                'Tip' => 'D3 mini',
                'Model' => 'Digital MPEG HD DVB-D3mini',
                'S/N' => '00753168735'
            ],
            [
                'Šifra' => '990091',
                'Tip' => 'D3 mini',
                'Model' => 'Digital MPEG HD DVB-D3mini',
                'S/N' => '00760127579'
            ],
            [
                'Šifra' => '990091',
                'Tip' => 'D3 mini',
                'Model' => 'Digital MPEG HD DVB-D3mini',
                'S/N' => '00753206281'
            ],
            [
                'Šifra' => '990091',
                'Tip' => 'D3 mini',
                'Model' => 'Digital MPEG HD DVB-D3mini',
                'S/N' => '00753179252'
            ],
            [
                'Šifra' => '990091',
                'Tip' => 'D3 mini',
                'Model' => 'Digital MPEG HD DVB-D3mini',
                'S/N' => '00753186594'
            ],
            [
                'Šifra' => '990091',
                'Tip' => 'D3 mini',
                'Model' => 'Digital MPEG HD DVB-D3mini',
                'S/N' => '00753236768'
            ],
            [
                'Šifra' => '990091',
                'Tip' => 'D3 mini',
                'Model' => 'Digital MPEG HD DVB-D3mini',
                'S/N' => '00753194291'
            ],
            [
                'Šifra' => '990091',
                'Tip' => 'D3 mini',
                'Model' => 'Digital MPEG HD DVB-D3mini',
                'S/N' => '00677890489'
            ],
            [
                'Šifra' => '990091',
                'Tip' => 'D3 mini',
                'Model' => 'Digital MPEG HD DVB-D3mini',
                'S/N' => '00638426765'
            ],
            [
                'Šifra' => '990091',
                'Tip' => 'D3 mini',
                'Model' => 'Digital MPEG HD DVB-D3mini',
                'S/N' => '00753167767'
            ],
            [
                'Šifra' => '990091',
                'Tip' => 'D3 mini',
                'Model' => 'Digital MPEG HD DVB-D3mini',
                'S/N' => '00753215950'
            ],
            [
                'Šifra' => '990091',
                'Tip' => 'D3 mini',
                'Model' => 'Digital MPEG HD DVB-D3mini',
                'S/N' => '00677926141'
            ],
            [
                'Šifra' => '990091',
                'Tip' => 'D3 mini',
                'Model' => 'Digital MPEG HD DVB-D3mini',
                'S/N' => '00753211040'
            ],
            [
                'Šifra' => '990091',
                'Tip' => 'D3 mini',
                'Model' => 'Digital MPEG HD DVB-D3mini',
                'S/N' => '00753058486'
            ],
            [
                'Šifra' => '990091',
                'Tip' => 'D3 mini',
                'Model' => 'Digital MPEG HD DVB-D3mini',
                'S/N' => '00638498336'
            ],
            [
                'Šifra' => '990091',
                'Tip' => 'D3 mini',
                'Model' => 'Digital MPEG HD DVB-D3mini',
                'S/N' => '00753209655'
            ],
            [
                'Šifra' => '990091',
                'Tip' => 'D3 mini',
                'Model' => 'Digital MPEG HD DVB-D3mini',
                'S/N' => '00753170603'
            ],
            [
                'Šifra' => '990091',
                'Tip' => 'D3 mini',
                'Model' => 'Digital MPEG HD DVB-D3mini',
                'S/N' => '00753207768'
            ],
            [
                'Šifra' => '990091',
                'Tip' => 'D3 mini',
                'Model' => 'Digital MPEG HD DVB-D3mini',
                'S/N' => '00753167495'
            ],
            [
                'Šifra' => '990091',
                'Tip' => 'D3 mini',
                'Model' => 'Digital MPEG HD DVB-D3mini',
                'S/N' => '00753208091'
            ],
            [
                'Šifra' => '990091',
                'Tip' => 'D3 mini',
                'Model' => 'Digital MPEG HD DVB-D3mini',
                'S/N' => '00724003112'
            ],
            [
                'Šifra' => '990091',
                'Tip' => 'D3 mini',
                'Model' => 'Digital MPEG HD DVB-D3mini',
                'S/N' => '00677901269'
            ],
            [
                'Šifra' => '990091',
                'Tip' => 'D3 mini',
                'Model' => 'Digital MPEG HD DVB-D3mini',
                'S/N' => '00753171345'
            ],
            [
                'Šifra' => '990091',
                'Tip' => 'D3 mini',
                'Model' => 'Digital MPEG HD DVB-D3mini',
                'S/N' => '00753208226'
            ],
            [
                'Šifra' => '990091',
                'Tip' => 'D3 mini',
                'Model' => 'Digital MPEG HD DVB-D3mini',
                'S/N' => '00753201223'
            ],
            [
                'Šifra' => '990091',
                'Tip' => 'D3 mini',
                'Model' => 'Digital MPEG HD DVB-D3mini',
                'S/N' => '00677987698'
            ],
            [
                'Šifra' => '990091',
                'Tip' => 'D3 mini',
                'Model' => 'Digital MPEG HD DVB-D3mini',
                'S/N' => '00753196550'
            ],
            [
                'Šifra' => '990091',
                'Tip' => 'D3 mini',
                'Model' => 'Digital MPEG HD DVB-D3mini',
                'S/N' => '00753166226'
            ],
            [
                'Šifra' => '990126',
                'Tip' => 'EON ALL IP',
                'Model' => 'EON BOX ALL IP SDOTT0202',
                'S/N' => 'SBB020419006053'
            ],
            [
                'Šifra' => '990126',
                'Tip' => 'EON ALL IP',
                'Model' => 'EON BOX ALL IP SDOTT0202',
                'S/N' => 'UG191031057048'
            ],
            [
                'Šifra' => '990126',
                'Tip' => 'EON ALL IP',
                'Model' => 'EON BOX ALL IP SDOTT0202',
                'S/N' => 'UG191031052455'
            ],
            [
                'Šifra' => '990126',
                'Tip' => 'EON ALL IP',
                'Model' => 'EON BOX ALL IP SDOTT0202',
                'S/N' => 'UG191031050048'
            ],
            [
                'Šifra' => '990126',
                'Tip' => 'EON ALL IP',
                'Model' => 'EON BOX ALL IP SDOTT0202',
                'S/N' => 'UG191031050082'
            ],
            [
                'Šifra' => '990126',
                'Tip' => 'EON ALL IP',
                'Model' => 'EON BOX ALL IP SDOTT0202',
                'S/N' => 'UG191031052363'
            ],
            [
                'Šifra' => '990126',
                'Tip' => 'EON ALL IP',
                'Model' => 'EON BOX ALL IP SDOTT0202',
                'S/N' => 'UG191230065459'
            ],
            [
                'Šifra' => '990126',
                'Tip' => 'EON ALL IP',
                'Model' => 'EON BOX ALL IP SDOTT0202',
                'S/N' => 'UG191031052578'
            ],
            [
                'Šifra' => '990126',
                'Tip' => 'EON ALL IP',
                'Model' => 'EON BOX ALL IP SDOTT0202',
                'S/N' => 'UG191031055947'
            ],
            [
                'Šifra' => '990126',
                'Tip' => 'EON ALL IP',
                'Model' => 'EON BOX ALL IP SDOTT0202',
                'S/N' => 'UG191031050307'
            ],
            [
                'Šifra' => '990126',
                'Tip' => 'EON ALL IP',
                'Model' => 'EON BOX ALL IP SDOTT0202',
                'S/N' => 'UG191031052498'
            ],
            [
                'Šifra' => '990126',
                'Tip' => 'EON ALL IP',
                'Model' => 'EON BOX ALL IP SDOTT0202',
                'S/N' => 'UG191031052499'
            ],
            [
                'Šifra' => '990126',
                'Tip' => 'EON ALL IP',
                'Model' => 'EON BOX ALL IP SDOTT0202',
                'S/N' => 'UG191031052576'
            ],
            [
                'Šifra' => '990126',
                'Tip' => 'EON ALL IP',
                'Model' => 'EON BOX ALL IP SDOTT0202',
                'S/N' => 'UG191230065594'
            ],
            [
                'Šifra' => '990126',
                'Tip' => 'EON ALL IP',
                'Model' => 'EON BOX ALL IP SDOTT0202',
                'S/N' => 'UG191031050185'
            ],
            [
                'Šifra' => '990126',
                'Tip' => 'EON ALL IP',
                'Model' => 'EON BOX ALL IP SDOTT0202',
                'S/N' => 'UG191230063536'
            ],
            [
                'Šifra' => '990126',
                'Tip' => 'EON ALL IP',
                'Model' => 'EON BOX ALL IP SDOTT0202',
                'S/N' => 'UG191031052598'
            ],
            [
                'Šifra' => '990126',
                'Tip' => 'EON ALL IP',
                'Model' => 'EON BOX ALL IP SDOTT0202',
                'S/N' => 'UG191031052625'
            ],
            [
                'Šifra' => '990126',
                'Tip' => 'EON ALL IP',
                'Model' => 'EON BOX ALL IP SDOTT0202',
                'S/N' => 'UG191031052649'
            ],
            [
                'Šifra' => '990126',
                'Tip' => 'EON ALL IP',
                'Model' => 'EON BOX ALL IP SDOTT0202',
                'S/N' => 'UG191031052652'
            ],
            [
                'Šifra' => '990126',
                'Tip' => 'EON ALL IP',
                'Model' => 'EON BOX ALL IP SDOTT0202',
                'S/N' => 'UG191031050036'
            ],
            [
                'Šifra' => '990126',
                'Tip' => 'EON ALL IP',
                'Model' => 'EON BOX ALL IP SDOTT0202',
                'S/N' => 'UG191031050128'
            ],
            [
                'Šifra' => '990126',
                'Tip' => 'EON ALL IP',
                'Model' => 'EON BOX ALL IP SDOTT0202',
                'S/N' => 'UG191031050436'
            ],
            [
                'Šifra' => '990126',
                'Tip' => 'EON ALL IP',
                'Model' => 'EON BOX ALL IP SDOTT0202',
                'S/N' => 'UG191031052366'
            ],
            [
                'Šifra' => '990126',
                'Tip' => 'EON ALL IP',
                'Model' => 'EON BOX ALL IP SDOTT0202',
                'S/N' => 'UG191031052422'
            ],
            [
                'Šifra' => '990126',
                'Tip' => 'EON ALL IP',
                'Model' => 'EON BOX ALL IP SDOTT0202',
                'S/N' => 'UG191031052431'
            ],
            [
                'Šifra' => '990126',
                'Tip' => 'EON ALL IP',
                'Model' => 'EON BOX ALL IP SDOTT0202',
                'S/N' => 'UG191031052491'
            ],
            [
                'Šifra' => '990126',
                'Tip' => 'EON ALL IP',
                'Model' => 'EON BOX ALL IP SDOTT0202',
                'S/N' => 'UG191031052493'
            ],
            [
                'Šifra' => '990126',
                'Tip' => 'EON ALL IP',
                'Model' => 'EON BOX ALL IP SDOTT0202',
                'S/N' => 'UG191031052541'
            ],
            [
                'Šifra' => '990126',
                'Tip' => 'EON ALL IP',
                'Model' => 'EON BOX ALL IP SDOTT0202',
                'S/N' => 'UG191031052542'
            ],
            [
                'Šifra' => '990126',
                'Tip' => 'EON ALL IP',
                'Model' => 'EON BOX ALL IP SDOTT0202',
                'S/N' => 'UG191031051089'
            ],
            [
                'Šifra' => '990126',
                'Tip' => 'EON ALL IP',
                'Model' => 'EON BOX ALL IP SDOTT0202',
                'S/N' => 'UG191031052627'
            ],
            [
                'Šifra' => '990126',
                'Tip' => 'EON ALL IP',
                'Model' => 'EON BOX ALL IP SDOTT0202',
                'S/N' => 'UG191031052781'
            ],
            [
                'Šifra' => '990126',
                'Tip' => 'EON ALL IP',
                'Model' => 'EON BOX ALL IP SDOTT0202',
                'S/N' => 'UG191031053585'
            ],
            [
                'Šifra' => '990126',
                'Tip' => 'EON ALL IP',
                'Model' => 'EON BOX ALL IP SDOTT0202',
                'S/N' => 'UG191031053601'
            ],
            [
                'Šifra' => '990126',
                'Tip' => 'EON ALL IP',
                'Model' => 'EON BOX ALL IP SDOTT0202',
                'S/N' => 'UG191031053608'
            ],
            [
                'Šifra' => '990126',
                'Tip' => 'EON ALL IP',
                'Model' => 'EON BOX ALL IP SDOTT0202',
                'S/N' => 'UG191031053612'
            ],
            [
                'Šifra' => '990126',
                'Tip' => 'EON ALL IP',
                'Model' => 'EON BOX ALL IP SDOTT0202',
                'S/N' => 'UG191031053615'
            ],
            [
                'Šifra' => '990126',
                'Tip' => 'EON ALL IP',
                'Model' => 'EON BOX ALL IP SDOTT0202',
                'S/N' => 'UG191031053633'
            ],
            [
                'Šifra' => '990126',
                'Tip' => 'EON ALL IP',
                'Model' => 'EON BOX ALL IP SDOTT0202',
                'S/N' => 'UG191031053701'
            ],
            [
                'Šifra' => '990126',
                'Tip' => 'EON ALL IP',
                'Model' => 'EON BOX ALL IP SDOTT0202',
                'S/N' => 'UG191031049942'
            ],
            [
                'Šifra' => '990126',
                'Tip' => 'EON ALL IP',
                'Model' => 'EON BOX ALL IP SDOTT0202',
                'S/N' => 'UG191031050304'
            ],
            [
                'Šifra' => '990126',
                'Tip' => 'EON ALL IP',
                'Model' => 'EON BOX ALL IP SDOTT0202',
                'S/N' => 'UG191031052519'
            ],
            [
                'Šifra' => '990126',
                'Tip' => 'EON ALL IP',
                'Model' => 'EON BOX ALL IP SDOTT0202',
                'S/N' => 'UG191031052582'
            ],
            [
                'Šifra' => '990126',
                'Tip' => 'EON ALL IP',
                'Model' => 'EON BOX ALL IP SDOTT0202',
                'S/N' => 'UG191031052623'
            ],
            [
                'Šifra' => '990126',
                'Tip' => 'EON ALL IP',
                'Model' => 'EON BOX ALL IP SDOTT0202',
                'S/N' => 'UG191031052741'
            ],
            [
                'Šifra' => '990126',
                'Tip' => 'EON ALL IP',
                'Model' => 'EON BOX ALL IP SDOTT0202',
                'S/N' => 'UG191031052752'
            ],
            [
                'Šifra' => '990126',
                'Tip' => 'EON ALL IP',
                'Model' => 'EON BOX ALL IP SDOTT0202',
                'S/N' => 'UG191031052753'
            ],
            [
                'Šifra' => '990126',
                'Tip' => 'EON ALL IP',
                'Model' => 'EON BOX ALL IP SDOTT0202',
                'S/N' => 'UG191031052782'
            ],
            [
                'Šifra' => '990126',
                'Tip' => 'EON ALL IP',
                'Model' => 'EON BOX ALL IP SDOTT0202',
                'S/N' => 'UG191031052813'
            ],
            [
                'Šifra' => '990126',
                'Tip' => 'EON ALL IP',
                'Model' => 'EON BOX ALL IP SDOTT0202',
                'S/N' => 'UG191230063246'
            ],
            [
                'Šifra' => '990126',
                'Tip' => 'EON ALL IP',
                'Model' => 'EON BOX ALL IP SDOTT0202',
                'S/N' => 'UG191230063572'
            ],
            [
                'Šifra' => '990126',
                'Tip' => 'EON ALL IP',
                'Model' => 'EON BOX ALL IP SDOTT0202',
                'S/N' => 'UG191230063618'
            ],
            [
                'Šifra' => '990126',
                'Tip' => 'EON ALL IP',
                'Model' => 'EON BOX ALL IP SDOTT0202',
                'S/N' => 'UG191230063844'
            ],
            [
                'Šifra' => '990126',
                'Tip' => 'EON ALL IP',
                'Model' => 'EON BOX ALL IP SDOTT0202',
                'S/N' => 'UG191230063968'
            ],
            [
                'Šifra' => '990126',
                'Tip' => 'EON ALL IP',
                'Model' => 'EON BOX ALL IP SDOTT0202',
                'S/N' => 'UG191230064711'
            ],
            [
                'Šifra' => '990126',
                'Tip' => 'EON ALL IP',
                'Model' => 'EON BOX ALL IP SDOTT0202',
                'S/N' => 'UG191230065882'
            ],
            [
                'Šifra' => '990126',
                'Tip' => 'EON ALL IP',
                'Model' => 'EON BOX ALL IP SDOTT0202',
                'S/N' => 'UG191230066024'
            ],
            [
                'Šifra' => '990126',
                'Tip' => 'EON ALL IP',
                'Model' => 'EON BOX ALL IP SDOTT0202',
                'S/N' => 'UG191230066142'
            ],
            [
                'Šifra' => '990126',
                'Tip' => 'EON ALL IP',
                'Model' => 'EON BOX ALL IP SDOTT0202',
                'S/N' => 'UG191230066152'
            ],
            [
                'Šifra' => '990126',
                'Tip' => 'EON ALL IP',
                'Model' => 'EON BOX ALL IP SDOTT0202',
                'S/N' => 'UG191230063669'
            ],
            [
                'Šifra' => '990126',
                'Tip' => 'EON ALL IP',
                'Model' => 'EON BOX ALL IP SDOTT0202',
                'S/N' => 'UG191230063718'
            ],
            [
                'Šifra' => '990126',
                'Tip' => 'EON ALL IP',
                'Model' => 'EON BOX ALL IP SDOTT0202',
                'S/N' => 'UG191230063793'
            ],
            [
                'Šifra' => '990126',
                'Tip' => 'EON ALL IP',
                'Model' => 'EON BOX ALL IP SDOTT0202',
                'S/N' => 'UG191230063936'
            ],
            [
                'Šifra' => '990126',
                'Tip' => 'EON ALL IP',
                'Model' => 'EON BOX ALL IP SDOTT0202',
                'S/N' => 'UG191230065319'
            ],
            [
                'Šifra' => '990126',
                'Tip' => 'EON ALL IP',
                'Model' => 'EON BOX ALL IP SDOTT0202',
                'S/N' => 'UG191230065402'
            ],
            [
                'Šifra' => '990126',
                'Tip' => 'EON ALL IP',
                'Model' => 'EON BOX ALL IP SDOTT0202',
                'S/N' => 'UG191230065730'
            ],
            [
                'Šifra' => '990126',
                'Tip' => 'EON ALL IP',
                'Model' => 'EON BOX ALL IP SDOTT0202',
                'S/N' => 'UG191230066043'
            ],
            [
                'Šifra' => '990126',
                'Tip' => 'EON ALL IP',
                'Model' => 'EON BOX ALL IP SDOTT0202',
                'S/N' => 'UG191230066051'
            ],
            [
                'Šifra' => '990126',
                'Tip' => 'EON ALL IP',
                'Model' => 'EON BOX ALL IP SDOTT0202',
                'S/N' => 'UG191230066150'
            ],
            [
                'Šifra' => '990126',
                'Tip' => 'EON ALL IP',
                'Model' => 'EON BOX ALL IP SDOTT0202',
                'S/N' => 'UG191230061619'
            ],
            [
                'Šifra' => '990126',
                'Tip' => 'EON ALL IP',
                'Model' => 'EON BOX ALL IP SDOTT0202',
                'S/N' => 'UG191230062364'
            ],
            [
                'Šifra' => '990126',
                'Tip' => 'EON ALL IP',
                'Model' => 'EON BOX ALL IP SDOTT0202',
                'S/N' => 'UG191230063648'
            ],
            [
                'Šifra' => '990126',
                'Tip' => 'EON ALL IP',
                'Model' => 'EON BOX ALL IP SDOTT0202',
                'S/N' => 'UG191230063649'
            ],
            [
                'Šifra' => '990126',
                'Tip' => 'EON ALL IP',
                'Model' => 'EON BOX ALL IP SDOTT0202',
                'S/N' => 'UG191230063731'
            ],
            [
                'Šifra' => '990126',
                'Tip' => 'EON ALL IP',
                'Model' => 'EON BOX ALL IP SDOTT0202',
                'S/N' => 'UG191230063852'
            ],
            [
                'Šifra' => '990126',
                'Tip' => 'EON ALL IP',
                'Model' => 'EON BOX ALL IP SDOTT0202',
                'S/N' => 'UG191230063892'
            ],
            [
                'Šifra' => '990126',
                'Tip' => 'EON ALL IP',
                'Model' => 'EON BOX ALL IP SDOTT0202',
                'S/N' => 'UG191230065328'
            ],
            [
                'Šifra' => '990126',
                'Tip' => 'EON ALL IP',
                'Model' => 'EON BOX ALL IP SDOTT0202',
                'S/N' => 'UG191230065373'
            ],
            [
                'Šifra' => '990126',
                'Tip' => 'EON ALL IP',
                'Model' => 'EON BOX ALL IP SDOTT0202',
                'S/N' => 'UG191031057513'
            ],
            [
                'Šifra' => '990126',
                'Tip' => 'EON ALL IP',
                'Model' => 'EON BOX ALL IP SDOTT0202',
                'S/N' => 'UG191031053399'
            ],
            [
                'Šifra' => '990126',
                'Tip' => 'EON ALL IP',
                'Model' => 'EON BOX ALL IP SDOTT0202',
                'S/N' => 'UG191230065514'
            ],
            [
                'Šifra' => '990126',
                'Tip' => 'EON ALL IP',
                'Model' => 'EON BOX ALL IP SDOTT0202',
                'S/N' => 'UG191230063058'
            ],
            [
                'Šifra' => '990126',
                'Tip' => 'EON ALL IP',
                'Model' => 'EON BOX ALL IP SDOTT0202',
                'S/N' => 'UG191230063339'
            ],
            [
                'Šifra' => '990126',
                'Tip' => 'EON ALL IP',
                'Model' => 'EON BOX ALL IP SDOTT0202',
                'S/N' => 'UG191230063553'
            ],
            [
                'Šifra' => '990126',
                'Tip' => 'EON ALL IP',
                'Model' => 'EON BOX ALL IP SDOTT0202',
                'S/N' => 'UG191230063575'
            ],
            [
                'Šifra' => '990126',
                'Tip' => 'EON ALL IP',
                'Model' => 'EON BOX ALL IP SDOTT0202',
                'S/N' => 'UG191230063593'
            ],
            [
                'Šifra' => '990126',
                'Tip' => 'EON ALL IP',
                'Model' => 'EON BOX ALL IP SDOTT0202',
                'S/N' => 'UG191230063611'
            ],
            [
                'Šifra' => '990126',
                'Tip' => 'EON ALL IP',
                'Model' => 'EON BOX ALL IP SDOTT0202',
                'S/N' => 'UG191230063651'
            ],
            [
                'Šifra' => '990126',
                'Tip' => 'EON ALL IP',
                'Model' => 'EON BOX ALL IP SDOTT0202',
                'S/N' => 'UG191230063675'
            ],
            [
                'Šifra' => '990126',
                'Tip' => 'EON ALL IP',
                'Model' => 'EON BOX ALL IP SDOTT0202',
                'S/N' => 'UG191230066089'
            ],
            [
                'Šifra' => '990126',
                'Tip' => 'EON ALL IP',
                'Model' => 'EON BOX ALL IP SDOTT0202',
                'S/N' => 'UG191031053810'
            ],
            [
                'Šifra' => '990126',
                'Tip' => 'EON ALL IP',
                'Model' => 'EON BOX ALL IP SDOTT0202',
                'S/N' => 'UG191031055962'
            ],
            [
                'Šifra' => '990126',
                'Tip' => 'EON ALL IP',
                'Model' => 'EON BOX ALL IP SDOTT0202',
                'S/N' => 'UG191230065485'
            ],
            [
                'Šifra' => '990126',
                'Tip' => 'EON ALL IP',
                'Model' => 'EON BOX ALL IP SDOTT0202',
                'S/N' => 'UG191230065475'
            ],
            [
                'Šifra' => '990126',
                'Tip' => 'EON ALL IP',
                'Model' => 'EON BOX ALL IP SDOTT0202',
                'S/N' => 'UG191230066003'
            ],
            [
                'Šifra' => '990126',
                'Tip' => 'EON ALL IP',
                'Model' => 'EON BOX ALL IP SDOTT0202',
                'S/N' => 'UG191230064157'
            ],
            [
                'Šifra' => '990126',
                'Tip' => 'EON ALL IP',
                'Model' => 'EON BOX ALL IP SDOTT0202',
                'S/N' => 'SBB020419015461'
            ],
            [
                'Šifra' => '990950',
                'Tip' => 'Beogrid modem',
                'Model' => 'HGW VDSL2 POTS Innbox V45',
                'S/N' => '2817503910'
            ],
            [
                'Šifra' => '990950',
                'Tip' => 'Beogrid modem',
                'Model' => 'HGW VDSL2 POTS Innbox V45',
                'S/N' => '2817503800'
            ],
            [
                'Šifra' => '990950',
                'Tip' => 'Beogrid modem',
                'Model' => 'HGW VDSL2 POTS Innbox V45',
                'S/N' => '2817502668'
            ],
            [
                'Šifra' => '990950',
                'Tip' => 'Beogrid modem',
                'Model' => 'HGW VDSL2 POTS Innbox V45',
                'S/N' => '2817503001'
            ],
            [
                'Šifra' => '990950',
                'Tip' => 'Beogrid modem',
                'Model' => 'HGW VDSL2 POTS Innbox V45',
                'S/N' => '2817503135'
            ],
            [
                'Šifra' => '990950',
                'Tip' => 'Beogrid modem',
                'Model' => 'HGW VDSL2 POTS Innbox V45',
                'S/N' => '2817503134'
            ],
            [
                'Šifra' => '990950',
                'Tip' => 'Beogrid modem',
                'Model' => 'HGW VDSL2 POTS Innbox V45',
                'S/N' => '2817503703'
            ],
            [
                'Šifra' => '990950',
                'Tip' => 'Beogrid modem',
                'Model' => 'HGW VDSL2 POTS Innbox V45',
                'S/N' => '2817503706'
            ],
            [
                'Šifra' => '990950',
                'Tip' => 'Beogrid modem',
                'Model' => 'HGW VDSL2 POTS Innbox V45',
                'S/N' => '2817503794'
            ],
            [
                'Šifra' => '990950',
                'Tip' => 'Beogrid modem',
                'Model' => 'HGW VDSL2 POTS Innbox V45',
                'S/N' => '2817503896'
            ],
            [
                'Šifra' => '990950',
                'Tip' => 'Beogrid modem',
                'Model' => 'HGW VDSL2 POTS Innbox V45',
                'S/N' => '2817504038'
            ],
            [
                'Šifra' => '990950',
                'Tip' => 'Beogrid modem',
                'Model' => 'HGW VDSL2 POTS Innbox V45',
                'S/N' => '2817504119'
            ],
            [
                'Šifra' => '990950',
                'Tip' => 'Beogrid modem',
                'Model' => 'HGW VDSL2 POTS Innbox V45',
                'S/N' => '2817503130'
            ],
            [
                'Šifra' => '990950',
                'Tip' => 'Beogrid modem',
                'Model' => 'HGW VDSL2 POTS Innbox V45',
                'S/N' => '2817503320'
            ],
            [
                'Šifra' => '990950',
                'Tip' => 'Beogrid modem',
                'Model' => 'HGW VDSL2 POTS Innbox V45',
                'S/N' => '2817503479'
            ],
            [
                'Šifra' => '990950',
                'Tip' => 'Beogrid modem',
                'Model' => 'HGW VDSL2 POTS Innbox V45',
                'S/N' => '2817503545'
            ],
            [
                'Šifra' => '990950',
                'Tip' => 'Beogrid modem',
                'Model' => 'HGW VDSL2 POTS Innbox V45',
                'S/N' => '2817503569'
            ],
            [
                'Šifra' => '990950',
                'Tip' => 'Beogrid modem',
                'Model' => 'HGW VDSL2 POTS Innbox V45',
                'S/N' => '2817503579'
            ],
            [
                'Šifra' => '990950',
                'Tip' => 'Beogrid modem',
                'Model' => 'HGW VDSL2 POTS Innbox V45',
                'S/N' => '2817503604'
            ],
            [
                'Šifra' => '990950',
                'Tip' => 'Beogrid modem',
                'Model' => 'HGW VDSL2 POTS Innbox V45',
                'S/N' => '2817503611'
            ],
            [
                'Šifra' => '990950',
                'Tip' => 'Beogrid modem',
                'Model' => 'HGW VDSL2 POTS Innbox V45',
                'S/N' => '2817503619'
            ],
            [
                'Šifra' => '990950',
                'Tip' => 'Beogrid modem',
                'Model' => 'HGW VDSL2 POTS Innbox V45',
                'S/N' => '2817503654'
            ],
            [
                'Šifra' => '990950',
                'Tip' => 'Beogrid modem',
                'Model' => 'HGW VDSL2 POTS Innbox V45',
                'S/N' => '2817502899'
            ],
            [
                'Šifra' => '990950',
                'Tip' => 'Beogrid modem',
                'Model' => 'HGW VDSL2 POTS Innbox V45',
                'S/N' => '2817503574'
            ],
            [
                'Šifra' => '990950',
                'Tip' => 'Beogrid modem',
                'Model' => 'HGW VDSL2 POTS Innbox V45',
                'S/N' => '2817503620'
            ],
            [
                'Šifra' => '990950',
                'Tip' => 'Beogrid modem',
                'Model' => 'HGW VDSL2 POTS Innbox V45',
                'S/N' => '2817503625'
            ],
            [
                'Šifra' => '990950',
                'Tip' => 'Beogrid modem',
                'Model' => 'HGW VDSL2 POTS Innbox V45',
                'S/N' => '2817503741'
            ],
            [
                'Šifra' => '990950',
                'Tip' => 'Beogrid modem',
                'Model' => 'HGW VDSL2 POTS Innbox V45',
                'S/N' => '2817503780'
            ],
            [
                'Šifra' => '990950',
                'Tip' => 'Beogrid modem',
                'Model' => 'HGW VDSL2 POTS Innbox V45',
                'S/N' => '2817503848'
            ],
            [
                'Šifra' => '990950',
                'Tip' => 'Beogrid modem',
                'Model' => 'HGW VDSL2 POTS Innbox V45',
                'S/N' => '2817503852'
            ],
            [
                'Šifra' => '990950',
                'Tip' => 'Beogrid modem',
                'Model' => 'HGW VDSL2 POTS Innbox V45',
                'S/N' => '2817503853'
            ],
            [
                'Šifra' => '990950',
                'Tip' => 'Beogrid modem',
                'Model' => 'HGW VDSL2 POTS Innbox V45',
                'S/N' => '2817503868'
            ],
            [
                'Šifra' => '990950',
                'Tip' => 'Beogrid modem',
                'Model' => 'HGW VDSL2 POTS Innbox V45',
                'S/N' => '2817503798'
            ],
            [
                'Šifra' => '990151',
                'Tip' => 'Cable Advance Home Gateway',
                'Model' => 'Ubee EVW32C-0N',
                'S/N' => 'EVW32C0N00004431'
            ],
            [
                'Šifra' => '990151',
                'Tip' => 'Cable Advance Home Gateway',
                'Model' => 'Ubee EVW32C-0N',
                'S/N' => 'EVW32C0N00308897'
            ],
            [
                'Šifra' => '990151',
                'Tip' => 'Cable Advance Home Gateway',
                'Model' => 'Ubee EVW32C-0N',
                'S/N' => 'EVW32C0N00314256'
            ],
            [
                'Šifra' => '990151',
                'Tip' => 'Cable Advance Home Gateway',
                'Model' => 'Ubee EVW32C-0N',
                'S/N' => 'EVW32C0N00308888'
            ],
            [
                'Šifra' => '990151',
                'Tip' => 'Cable Advance Home Gateway',
                'Model' => 'Ubee EVW32C-0N',
                'S/N' => 'EVW32C0N00314499'
            ],
            [
                'Šifra' => '990151',
                'Tip' => 'Cable Advance Home Gateway',
                'Model' => 'Ubee EVW32C-0N',
                'S/N' => 'EVW32C0N00319133'
            ],
            [
                'Šifra' => '990151',
                'Tip' => 'Cable Advance Home Gateway',
                'Model' => 'Ubee EVW32C-0N',
                'S/N' => 'EVW32C0N00318981'
            ],
            [
                'Šifra' => '990151',
                'Tip' => 'Cable Advance Home Gateway',
                'Model' => 'Ubee EVW32C-0N',
                'S/N' => 'EVW32C0N00309037'
            ],
            [
                'Šifra' => '990151',
                'Tip' => 'Cable Advance Home Gateway',
                'Model' => 'Ubee EVW32C-0N',
                'S/N' => 'EVW32C0N00309122'
            ],
            [
                'Šifra' => '990151',
                'Tip' => 'Cable Advance Home Gateway',
                'Model' => 'Ubee EVW32C-0N',
                'S/N' => 'EVW32C0N00309090'
            ],
            [
                'Šifra' => '990151',
                'Tip' => 'Cable Advance Home Gateway',
                'Model' => 'Ubee EVW32C-0N',
                'S/N' => 'EVW32C0N00127761'
            ],
            [
                'Šifra' => '990151',
                'Tip' => 'Cable Advance Home Gateway',
                'Model' => 'Ubee EVW32C-0N',
                'S/N' => 'EVW32C0N00309095'
            ],
            [
                'Šifra' => '990151',
                'Tip' => 'Cable Advance Home Gateway',
                'Model' => 'Ubee EVW32C-0N',
                'S/N' => 'EVW32C0N00309110'
            ],
            [
                'Šifra' => '990151',
                'Tip' => 'Cable Advance Home Gateway',
                'Model' => 'Ubee EVW32C-0N',
                'S/N' => 'EVW32C0N00309120'
            ],
            [
                'Šifra' => '990151',
                'Tip' => 'Cable Advance Home Gateway',
                'Model' => 'Ubee EVW32C-0N',
                'S/N' => 'EVW32C0N00309086'
            ],
            [
                'Šifra' => '990151',
                'Tip' => 'Cable Advance Home Gateway',
                'Model' => 'Ubee EVW32C-0N',
                'S/N' => 'EVW32C0N00309084'
            ],
            [
                'Šifra' => '990151',
                'Tip' => 'Cable Advance Home Gateway',
                'Model' => 'Ubee EVW32C-0N',
                'S/N' => 'EVW32C0N00309097'
            ],
            [
                'Šifra' => '990151',
                'Tip' => 'Cable Advance Home Gateway',
                'Model' => 'Ubee EVW32C-0N',
                'S/N' => 'EVW32C0N00309105'
            ],
            [
                'Šifra' => '990151',
                'Tip' => 'Cable Advance Home Gateway',
                'Model' => 'Ubee EVW32C-0N',
                'S/N' => 'EVW32C0N00309072'
            ],
            [
                'Šifra' => '990151',
                'Tip' => 'Cable Advance Home Gateway',
                'Model' => 'Ubee EVW32C-0N',
                'S/N' => 'EVW32C0N00309071'
            ],
            [
                'Šifra' => '990151',
                'Tip' => 'Cable Advance Home Gateway',
                'Model' => 'Ubee EVW32C-0N',
                'S/N' => 'EVW32C0N00309109'
            ],
            [
                'Šifra' => '990151',
                'Tip' => 'Cable Advance Home Gateway',
                'Model' => 'Ubee EVW32C-0N',
                'S/N' => 'EVW32C0N00309076'
            ],
            [
                'Šifra' => '990151',
                'Tip' => 'Cable Advance Home Gateway',
                'Model' => 'Ubee EVW32C-0N',
                'S/N' => 'EVW32C0N00309070'
            ],
            [
                'Šifra' => '990151',
                'Tip' => 'Cable Advance Home Gateway',
                'Model' => 'Ubee EVW32C-0N',
                'S/N' => 'EVW32C0N00309079'
            ],
            [
                'Šifra' => '990151',
                'Tip' => 'Cable Advance Home Gateway',
                'Model' => 'Ubee EVW32C-0N',
                'S/N' => 'EVW32C0N00309093'
            ],
            [
                'Šifra' => '990151',
                'Tip' => 'Cable Advance Home Gateway',
                'Model' => 'Ubee EVW32C-0N',
                'S/N' => 'EVW32C0N00309073'
            ],
            [
                'Šifra' => '990151',
                'Tip' => 'Cable Advance Home Gateway',
                'Model' => 'Ubee EVW32C-0N',
                'S/N' => 'EVW32C0N00309112'
            ],
            [
                'Šifra' => '990151',
                'Tip' => 'Cable Advance Home Gateway',
                'Model' => 'Ubee EVW32C-0N',
                'S/N' => 'EVW32C0N00315474'
            ],
            [
                'Šifra' => '990151',
                'Tip' => 'Cable Advance Home Gateway',
                'Model' => 'Ubee EVW32C-0N',
                'S/N' => 'EVW32C0N00316552'
            ],
            [
                'Šifra' => '990151',
                'Tip' => 'Cable Advance Home Gateway',
                'Model' => 'Ubee EVW32C-0N',
                'S/N' => 'EVW32C0N00316629'
            ],
            [
                'Šifra' => '990151',
                'Tip' => 'Cable Advance Home Gateway',
                'Model' => 'Ubee EVW32C-0N',
                'S/N' => 'EVW32C0N00312560'
            ],
            [
                'Šifra' => '990151',
                'Tip' => 'Cable Advance Home Gateway',
                'Model' => 'Ubee EVW32C-0N',
                'S/N' => 'EVW32C0N00319109'
            ],
            [
                'Šifra' => '990151',
                'Tip' => 'Cable Advance Home Gateway',
                'Model' => 'Ubee EVW32C-0N',
                'S/N' => 'EVW32C0N00318837'
            ],
            [
                'Šifra' => '990151',
                'Tip' => 'Cable Advance Home Gateway',
                'Model' => 'Ubee EVW32C-0N',
                'S/N' => 'EVW32C0N00318685'
            ],
            [
                'Šifra' => '990151',
                'Tip' => 'Cable Advance Home Gateway',
                'Model' => 'Ubee EVW32C-0N',
                'S/N' => 'EVW32C0N00318802'
            ],
            [
                'Šifra' => '990151',
                'Tip' => 'Cable Advance Home Gateway',
                'Model' => 'Ubee EVW32C-0N',
                'S/N' => 'EVW32C0N00318715'
            ],
            [
                'Šifra' => '990151',
                'Tip' => 'Cable Advance Home Gateway',
                'Model' => 'Ubee EVW32C-0N',
                'S/N' => 'EVW32C0N00318947'
            ],
            [
                'Šifra' => '990151',
                'Tip' => 'Cable Advance Home Gateway',
                'Model' => 'Ubee EVW32C-0N',
                'S/N' => 'EVW32C0N00318936'
            ],
            [
                'Šifra' => '990151',
                'Tip' => 'Cable Advance Home Gateway',
                'Model' => 'Ubee EVW32C-0N',
                'S/N' => 'EVW32C0N00317427'
            ],
            [
                'Šifra' => '990151',
                'Tip' => 'Cable Advance Home Gateway',
                'Model' => 'Ubee EVW32C-0N',
                'S/N' => 'EVW32C0N00308998'
            ],
            [
                'Šifra' => '990151',
                'Tip' => 'Cable Advance Home Gateway',
                'Model' => 'Ubee EVW32C-0N',
                'S/N' => 'EVW32C0N00309012'
            ],
            [
                'Šifra' => '990151',
                'Tip' => 'Cable Advance Home Gateway',
                'Model' => 'Ubee EVW32C-0N',
                'S/N' => 'EVW32C0N00309232'
            ],
            [
                'Šifra' => '990151',
                'Tip' => 'Cable Advance Home Gateway',
                'Model' => 'Ubee EVW32C-0N',
                'S/N' => 'EVW32C0N00308997'
            ],
            [
                'Šifra' => '990151',
                'Tip' => 'Cable Advance Home Gateway',
                'Model' => 'Ubee EVW32C-0N',
                'S/N' => 'EVW32C0N00309014'
            ],
            [
                'Šifra' => '990151',
                'Tip' => 'Cable Advance Home Gateway',
                'Model' => 'Ubee EVW32C-0N',
                'S/N' => 'EVW32C0N00318812'
            ],
            [
                'Šifra' => '990151',
                'Tip' => 'Cable Advance Home Gateway',
                'Model' => 'Ubee EVW32C-0N',
                'S/N' => 'EVW32C0N00318801'
            ],
            [
                'Šifra' => '990151',
                'Tip' => 'Cable Advance Home Gateway',
                'Model' => 'Ubee EVW32C-0N',
                'S/N' => 'EVW32C0N00318658'
            ],
            [
                'Šifra' => '990151',
                'Tip' => 'Cable Advance Home Gateway',
                'Model' => 'Ubee EVW32C-0N',
                'S/N' => 'EVW32C0N00318628'
            ],
            [
                'Šifra' => '990151',
                'Tip' => 'Cable Advance Home Gateway',
                'Model' => 'Ubee EVW32C-0N',
                'S/N' => 'EVW32C0N00318638'
            ],
            [
                'Šifra' => '990151',
                'Tip' => 'Cable Advance Home Gateway',
                'Model' => 'Ubee EVW32C-0N',
                'S/N' => 'EVW32C0N00318709'
            ],
            [
                'Šifra' => '990151',
                'Tip' => 'Cable Advance Home Gateway',
                'Model' => 'Ubee EVW32C-0N',
                'S/N' => 'EVW32C0N00318967'
            ],
            [
                'Šifra' => '990151',
                'Tip' => 'Cable Advance Home Gateway',
                'Model' => 'Ubee EVW32C-0N',
                'S/N' => 'EVW32C0N00318957'
            ],
            [
                'Šifra' => '990151',
                'Tip' => 'Cable Advance Home Gateway',
                'Model' => 'Ubee EVW32C-0N',
                'S/N' => 'EVW32C0N00316907'
            ],
            [
                'Šifra' => '990151',
                'Tip' => 'Cable Advance Home Gateway',
                'Model' => 'Ubee EVW32C-0N',
                'S/N' => 'EVW32C0N00320186'
            ],
            [
                'Šifra' => '990151',
                'Tip' => 'Cable Advance Home Gateway',
                'Model' => 'Ubee EVW32C-0N',
                'S/N' => 'EVW32C0N00320187'
            ],
            [
                'Šifra' => '990151',
                'Tip' => 'Cable Advance Home Gateway',
                'Model' => 'Ubee EVW32C-0N',
                'S/N' => 'EVW32C0N00132216'
            ],
            [
                'Šifra' => '990151',
                'Tip' => 'Cable Advance Home Gateway',
                'Model' => 'Ubee EVW32C-0N',
                'S/N' => 'EVW32C0N00320188'
            ],
            [
                'Šifra' => '990151',
                'Tip' => 'Cable Advance Home Gateway',
                'Model' => 'Ubee EVW32C-0N',
                'S/N' => 'EVW32C0N00316432'
            ],
            [
                'Šifra' => '990151',
                'Tip' => 'Cable Advance Home Gateway',
                'Model' => 'Ubee EVW32C-0N',
                'S/N' => 'EVW32C0N00319123'
            ],
            [
                'Šifra' => '990151',
                'Tip' => 'Cable Advance Home Gateway',
                'Model' => 'Ubee EVW32C-0N',
                'S/N' => 'EVW32C0N00320212'
            ],
            [
                'Šifra' => '990151',
                'Tip' => 'Cable Advance Home Gateway',
                'Model' => 'Ubee EVW32C-0N',
                'S/N' => 'EVW32C0N00320237'
            ],
            [
                'Šifra' => '990151',
                'Tip' => 'Cable Advance Home Gateway',
                'Model' => 'Ubee EVW32C-0N',
                'S/N' => 'EVW32C0N00035545'
            ],
            [
                'Šifra' => '990151',
                'Tip' => 'Cable Advance Home Gateway',
                'Model' => 'Ubee EVW32C-0N',
                'S/N' => 'EVW32C0N00316870'
            ],
            [
                'Šifra' => '990151',
                'Tip' => 'Cable Advance Home Gateway',
                'Model' => 'Ubee EVW32C-0N',
                'S/N' => 'EVW32C0N00320413'
            ],
            [
                'Šifra' => '990151',
                'Tip' => 'Cable Advance Home Gateway',
                'Model' => 'Ubee EVW32C-0N',
                'S/N' => 'EVW32C0N00207319'
            ],
            [
                'Šifra' => '990151',
                'Tip' => 'Cable Advance Home Gateway',
                'Model' => 'Ubee EVW32C-0N',
                'S/N' => 'EVW32C0N00001101'
            ],
            [
                'Šifra' => '990151',
                'Tip' => 'Cable Advance Home Gateway',
                'Model' => 'Ubee EVW32C-0N',
                'S/N' => 'EVW32C0N00107569'
            ],
            [
                'Šifra' => '990151',
                'Tip' => 'Cable Advance Home Gateway',
                'Model' => 'Ubee EVW32C-0N',
                'S/N' => 'EVW32C0N00012251'
            ],
            [
                'Šifra' => '990144',
                'Tip' => 'D3 1 way receiver',
                'Model' => 'PDS2100 DVB-C IP Digital STB',
                'S/N' => '168850897'
            ],
            [
                'Šifra' => '990144',
                'Tip' => 'D3 1 way receiver',
                'Model' => 'PDS2100 DVB-C IP Digital STB',
                'S/N' => '178216944'
            ],
            [
                'Šifra' => '990144',
                'Tip' => 'D3 1 way receiver',
                'Model' => 'PDS2100 DVB-C IP Digital STB',
                'S/N' => '180997945'
            ],
            [
                'Šifra' => '990144',
                'Tip' => 'D3 1 way receiver',
                'Model' => 'PDS2100 DVB-C IP Digital STB',
                'S/N' => '180021978'
            ],
            [
                'Šifra' => '990144',
                'Tip' => 'D3 1 way receiver',
                'Model' => 'PDS2100 DVB-C IP Digital STB',
                'S/N' => '168795793'
            ],
            [
                'Šifra' => '990144',
                'Tip' => 'D3 1 way receiver',
                'Model' => 'PDS2100 DVB-C IP Digital STB',
                'S/N' => '169077820'
            ],
            [
                'Šifra' => '990144',
                'Tip' => 'D3 1 way receiver',
                'Model' => 'PDS2100 DVB-C IP Digital STB',
                'S/N' => '169280367'
            ],
            [
                'Šifra' => '990144',
                'Tip' => 'D3 1 way receiver',
                'Model' => 'PDS2100 DVB-C IP Digital STB',
                'S/N' => '168807410'
            ],
            [
                'Šifra' => '990144',
                'Tip' => 'D3 1 way receiver',
                'Model' => 'PDS2100 DVB-C IP Digital STB',
                'S/N' => '178505222'
            ],
            [
                'Šifra' => '990144',
                'Tip' => 'D3 1 way receiver',
                'Model' => 'PDS2100 DVB-C IP Digital STB',
                'S/N' => '168202290'
            ],
            [
                'Šifra' => '990144',
                'Tip' => 'D3 1 way receiver',
                'Model' => 'PDS2100 DVB-C IP Digital STB',
                'S/N' => '168851458'
            ],
            [
                'Šifra' => '990144',
                'Tip' => 'D3 1 way receiver',
                'Model' => 'PDS2100 DVB-C IP Digital STB',
                'S/N' => '168807756'
            ],
            [
                'Šifra' => '990144',
                'Tip' => 'D3 1 way receiver',
                'Model' => 'PDS2100 DVB-C IP Digital STB',
                'S/N' => '178425587'
            ],
            [
                'Šifra' => '990144',
                'Tip' => 'D3 1 way receiver',
                'Model' => 'PDS2100 DVB-C IP Digital STB',
                'S/N' => '178545651'
            ],
            [
                'Šifra' => '990144',
                'Tip' => 'D3 1 way receiver',
                'Model' => 'PDS2100 DVB-C IP Digital STB',
                'S/N' => '169021029'
            ],
            [
                'Šifra' => '990144',
                'Tip' => 'D3 1 way receiver',
                'Model' => 'PDS2100 DVB-C IP Digital STB',
                'S/N' => '169194712'
            ],
            [
                'Šifra' => '990144',
                'Tip' => 'D3 1 way receiver',
                'Model' => 'PDS2100 DVB-C IP Digital STB',
                'S/N' => '168210303'
            ],
            [
                'Šifra' => '990144',
                'Tip' => 'D3 1 way receiver',
                'Model' => 'PDS2100 DVB-C IP Digital STB',
                'S/N' => '169851337'
            ],
            [
                'Šifra' => '990144',
                'Tip' => 'D3 1 way receiver',
                'Model' => 'PDS2100 DVB-C IP Digital STB',
                'S/N' => '169268440'
            ],
            [
                'Šifra' => '990144',
                'Tip' => 'D3 1 way receiver',
                'Model' => 'PDS2100 DVB-C IP Digital STB',
                'S/N' => '169851325'
            ],
            [
                'Šifra' => '990144',
                'Tip' => 'D3 1 way receiver',
                'Model' => 'PDS2100 DVB-C IP Digital STB',
                'S/N' => '178493516'
            ],
            [
                'Šifra' => '990144',
                'Tip' => 'D3 1 way receiver',
                'Model' => 'PDS2100 DVB-C IP Digital STB',
                'S/N' => '167856677'
            ],
            [
                'Šifra' => '990144',
                'Tip' => 'D3 1 way receiver',
                'Model' => 'PDS2100 DVB-C IP Digital STB',
                'S/N' => '178441633'
            ],
            [
                'Šifra' => '990144',
                'Tip' => 'D3 1 way receiver',
                'Model' => 'PDS2100 DVB-C IP Digital STB',
                'S/N' => '00677967387'
            ],
            [
                'Šifra' => '990144',
                'Tip' => 'D3 1 way receiver',
                'Model' => 'PDS2100 DVB-C IP Digital STB',
                'S/N' => '169179658'
            ],
            [
                'Šifra' => '990144',
                'Tip' => 'D3 1 way receiver',
                'Model' => 'PDS2100 DVB-C IP Digital STB',
                'S/N' => '178555574'
            ],
            [
                'Šifra' => '990144',
                'Tip' => 'D3 1 way receiver',
                'Model' => 'PDS2100 DVB-C IP Digital STB',
                'S/N' => '169209888'
            ],
            [
                'Šifra' => '990144',
                'Tip' => 'D3 1 way receiver',
                'Model' => 'PDS2100 DVB-C IP Digital STB',
                'S/N' => '168185144'
            ],
            [
                'Šifra' => '990144',
                'Tip' => 'D3 1 way receiver',
                'Model' => 'PDS2100 DVB-C IP Digital STB',
                'S/N' => '180028397'
            ],
            [
                'Šifra' => '990144',
                'Tip' => 'D3 1 way receiver',
                'Model' => 'PDS2100 DVB-C IP Digital STB',
                'S/N' => '178555823'
            ],
            [
                'Šifra' => '990144',
                'Tip' => 'D3 1 way receiver',
                'Model' => 'PDS2100 DVB-C IP Digital STB',
                'S/N' => '169275134'
            ],
            [
                'Šifra' => '990144',
                'Tip' => 'D3 1 way receiver',
                'Model' => 'PDS2100 DVB-C IP Digital STB',
                'S/N' => '168200894'
            ],
            [
                'Šifra' => '990144',
                'Tip' => 'D3 1 way receiver',
                'Model' => 'PDS2100 DVB-C IP Digital STB',
                'S/N' => '168842700'
            ],
            [
                'Šifra' => '990144',
                'Tip' => 'D3 1 way receiver',
                'Model' => 'PDS2100 DVB-C IP Digital STB',
                'S/N' => '178492139'
            ],
            [
                'Šifra' => '990144',
                'Tip' => 'D3 1 way receiver',
                'Model' => 'PDS2100 DVB-C IP Digital STB',
                'S/N' => '169387416'
            ],
            [
                'Šifra' => '990144',
                'Tip' => 'D3 1 way receiver',
                'Model' => 'PDS2100 DVB-C IP Digital STB',
                'S/N' => '168099128'
            ],
            [
                'Šifra' => '990144',
                'Tip' => 'D3 1 way receiver',
                'Model' => 'PDS2100 DVB-C IP Digital STB',
                'S/N' => '169158948'
            ],
            [
                'Šifra' => '990144',
                'Tip' => 'D3 1 way receiver',
                'Model' => 'PDS2100 DVB-C IP Digital STB',
                'S/N' => '178543376'
            ],
            [
                'Šifra' => '990144',
                'Tip' => 'D3 1 way receiver',
                'Model' => 'PDS2100 DVB-C IP Digital STB',
                'S/N' => '169333866'
            ],
            [
                'Šifra' => '990144',
                'Tip' => 'D3 1 way receiver',
                'Model' => 'PDS2100 DVB-C IP Digital STB',
                'S/N' => '169192240'
            ],
            [
                'Šifra' => '990144',
                'Tip' => 'D3 1 way receiver',
                'Model' => 'PDS2100 DVB-C IP Digital STB',
                'S/N' => '169262962'
            ],
            [
                'Šifra' => '990144',
                'Tip' => 'D3 1 way receiver',
                'Model' => 'PDS2100 DVB-C IP Digital STB',
                'S/N' => '168841890'
            ],
            [
                'Šifra' => '990144',
                'Tip' => 'D3 1 way receiver',
                'Model' => 'PDS2100 DVB-C IP Digital STB',
                'S/N' => '178427123'
            ],
            [
                'Šifra' => '990144',
                'Tip' => 'D3 1 way receiver',
                'Model' => 'PDS2100 DVB-C IP Digital STB',
                'S/N' => '169078737'
            ],
            [
                'Šifra' => '990144',
                'Tip' => 'D3 1 way receiver',
                'Model' => 'PDS2100 DVB-C IP Digital STB',
                'S/N' => '169213887'
            ],
            [
                'Šifra' => '990144',
                'Tip' => 'D3 1 way receiver',
                'Model' => 'PDS2100 DVB-C IP Digital STB',
                'S/N' => '168048743'
            ],
            [
                'Šifra' => '990144',
                'Tip' => 'D3 1 way receiver',
                'Model' => 'PDS2100 DVB-C IP Digital STB',
                'S/N' => '169272998'
            ],
            [
                'Šifra' => '990144',
                'Tip' => 'D3 1 way receiver',
                'Model' => 'PDS2100 DVB-C IP Digital STB',
                'S/N' => '169851642'
            ],
            [
                'Šifra' => '990144',
                'Tip' => 'D3 1 way receiver',
                'Model' => 'PDS2100 DVB-C IP Digital STB',
                'S/N' => '169187110'
            ],
            [
                'Šifra' => '990144',
                'Tip' => 'D3 1 way receiver',
                'Model' => 'PDS2100 DVB-C IP Digital STB',
                'S/N' => '168182882'
            ],
            [
                'Šifra' => '990144',
                'Tip' => 'D3 1 way receiver',
                'Model' => 'PDS2100 DVB-C IP Digital STB',
                'S/N' => '169153127'
            ],
            [
                'Šifra' => '990144',
                'Tip' => 'D3 1 way receiver',
                'Model' => 'PDS2100 DVB-C IP Digital STB',
                'S/N' => '168093532'
            ],
            [
                'Šifra' => '990144',
                'Tip' => 'D3 1 way receiver',
                'Model' => 'PDS2100 DVB-C IP Digital STB',
                'S/N' => '181012124'
            ],
            [
                'Šifra' => '990144',
                'Tip' => 'D3 1 way receiver',
                'Model' => 'PDS2100 DVB-C IP Digital STB',
                'S/N' => '178621145'
            ],
            [
                'Šifra' => '990144',
                'Tip' => 'D3 1 way receiver',
                'Model' => 'PDS2100 DVB-C IP Digital STB',
                'S/N' => '169191040'
            ],
            [
                'Šifra' => '990144',
                'Tip' => 'D3 1 way receiver',
                'Model' => 'PDS2100 DVB-C IP Digital STB',
                'S/N' => '169079642'
            ],
            [
                'Šifra' => '990144',
                'Tip' => 'D3 1 way receiver',
                'Model' => 'PDS2100 DVB-C IP Digital STB',
                'S/N' => '167672724'
            ],
            [
                'Šifra' => '990144',
                'Tip' => 'D3 1 way receiver',
                'Model' => 'PDS2100 DVB-C IP Digital STB',
                'S/N' => '168809802'
            ],
            [
                'Šifra' => '990144',
                'Tip' => 'D3 1 way receiver',
                'Model' => 'PDS2100 DVB-C IP Digital STB',
                'S/N' => '169186713'
            ],
            [
                'Šifra' => '990144',
                'Tip' => 'D3 1 way receiver',
                'Model' => 'PDS2100 DVB-C IP Digital STB',
                'S/N' => '181080564'
            ],
            [
                'Šifra' => '990144',
                'Tip' => 'D3 1 way receiver',
                'Model' => 'PDS2100 DVB-C IP Digital STB',
                'S/N' => '178507424'
            ],
            [
                'Šifra' => '990144',
                'Tip' => 'D3 1 way receiver',
                'Model' => 'PDS2100 DVB-C IP Digital STB',
                'S/N' => '169116747'
            ],
            [
                'Šifra' => '990144',
                'Tip' => 'D3 1 way receiver',
                'Model' => 'PDS2100 DVB-C IP Digital STB',
                'S/N' => '169085738'
            ],
            [
                'Šifra' => '990144',
                'Tip' => 'D3 1 way receiver',
                'Model' => 'PDS2100 DVB-C IP Digital STB',
                'S/N' => '169225442'
            ],
            [
                'Šifra' => '990144',
                'Tip' => 'D3 1 way receiver',
                'Model' => 'PDS2100 DVB-C IP Digital STB',
                'S/N' => '167749873'
            ],
            [
                'Šifra' => '990144',
                'Tip' => 'D3 1 way receiver',
                'Model' => 'PDS2100 DVB-C IP Digital STB',
                'S/N' => '169268763'
            ],
            [
                'Šifra' => '990144',
                'Tip' => 'D3 1 way receiver',
                'Model' => 'PDS2100 DVB-C IP Digital STB',
                'S/N' => '181043024'
            ],
            [
                'Šifra' => '990144',
                'Tip' => 'D3 1 way receiver',
                'Model' => 'PDS2100 DVB-C IP Digital STB',
                'S/N' => '178309284'
            ],
            [
                'Šifra' => '990144',
                'Tip' => 'D3 1 way receiver',
                'Model' => 'PDS2100 DVB-C IP Digital STB',
                'S/N' => '169188113'
            ],
            [
                'Šifra' => '990144',
                'Tip' => 'D3 1 way receiver',
                'Model' => 'PDS2100 DVB-C IP Digital STB',
                'S/N' => '178247100'
            ],
            [
                'Šifra' => '990144',
                'Tip' => 'D3 1 way receiver',
                'Model' => 'PDS2100 DVB-C IP Digital STB',
                'S/N' => '168182956'
            ],
            [
                'Šifra' => '990144',
                'Tip' => 'D3 1 way receiver',
                'Model' => 'PDS2100 DVB-C IP Digital STB',
                'S/N' => '178495041'
            ],
            [
                'Šifra' => '990144',
                'Tip' => 'D3 1 way receiver',
                'Model' => 'PDS2100 DVB-C IP Digital STB',
                'S/N' => '169284562'
            ],
            [
                'Šifra' => '990144',
                'Tip' => 'D3 1 way receiver',
                'Model' => 'PDS2100 DVB-C IP Digital STB',
                'S/N' => '169179214'
            ],
            [
                'Šifra' => '990144',
                'Tip' => 'D3 1 way receiver',
                'Model' => 'PDS2100 DVB-C IP Digital STB',
                'S/N' => '168355594'
            ],
            [
                'Šifra' => '990144',
                'Tip' => 'D3 1 way receiver',
                'Model' => 'PDS2100 DVB-C IP Digital STB',
                'S/N' => '178499652'
            ],
            [
                'Šifra' => '990144',
                'Tip' => 'D3 1 way receiver',
                'Model' => 'PDS2100 DVB-C IP Digital STB',
                'S/N' => '168216431'
            ],
            [
                'Šifra' => '990144',
                'Tip' => 'D3 1 way receiver',
                'Model' => 'PDS2100 DVB-C IP Digital STB',
                'S/N' => '168974490'
            ],
            [
                'Šifra' => '990144',
                'Tip' => 'D3 1 way receiver',
                'Model' => 'PDS2100 DVB-C IP Digital STB',
                'S/N' => '169186022'
            ],
            [
                'Šifra' => '990144',
                'Tip' => 'D3 1 way receiver',
                'Model' => 'PDS2100 DVB-C IP Digital STB',
                'S/N' => '168859666'
            ],
            [
                'Šifra' => '990144',
                'Tip' => 'D3 1 way receiver',
                'Model' => 'PDS2100 DVB-C IP Digital STB',
                'S/N' => '178244574'
            ],
            [
                'Šifra' => '990144',
                'Tip' => 'D3 1 way receiver',
                'Model' => 'PDS2100 DVB-C IP Digital STB',
                'S/N' => '169178316'
            ],
            [
                'Šifra' => '990144',
                'Tip' => 'D3 1 way receiver',
                'Model' => 'PDS2100 DVB-C IP Digital STB',
                'S/N' => '169207711'
            ],
            [
                'Šifra' => '990144',
                'Tip' => 'D3 1 way receiver',
                'Model' => 'PDS2100 DVB-C IP Digital STB',
                'S/N' => '178431687'
            ],
            [
                'Šifra' => '990144',
                'Tip' => 'D3 1 way receiver',
                'Model' => 'PDS2100 DVB-C IP Digital STB',
                'S/N' => '168215425'
            ],
            [
                'Šifra' => '990144',
                'Tip' => 'D3 1 way receiver',
                'Model' => 'PDS2100 DVB-C IP Digital STB',
                'S/N' => '168655062'
            ],
            [
                'Šifra' => '990144',
                'Tip' => 'D3 1 way receiver',
                'Model' => 'PDS2100 DVB-C IP Digital STB',
                'S/N' => '167673611'
            ],
            [
                'Šifra' => '990144',
                'Tip' => 'D3 1 way receiver',
                'Model' => 'PDS2100 DVB-C IP Digital STB',
                'S/N' => '169220515'
            ],
            [
                'Šifra' => '990144',
                'Tip' => 'D3 1 way receiver',
                'Model' => 'PDS2100 DVB-C IP Digital STB',
                'S/N' => '169228861'
            ],
            [
                'Šifra' => '990144',
                'Tip' => 'D3 1 way receiver',
                'Model' => 'PDS2100 DVB-C IP Digital STB',
                'S/N' => '169326598'
            ],
            [
                'Šifra' => '990144',
                'Tip' => 'D3 1 way receiver',
                'Model' => 'PDS2100 DVB-C IP Digital STB',
                'S/N' => '181010154'
            ],
            [
                'Šifra' => '990144',
                'Tip' => 'D3 1 way receiver',
                'Model' => 'PDS2100 DVB-C IP Digital STB',
                'S/N' => '178502372'
            ],
            [
                'Šifra' => '990144',
                'Tip' => 'D3 1 way receiver',
                'Model' => 'PDS2100 DVB-C IP Digital STB',
                'S/N' => '169151992'
            ],
            [
                'Šifra' => '990144',
                'Tip' => 'D3 1 way receiver',
                'Model' => 'PDS2100 DVB-C IP Digital STB',
                'S/N' => '169021859'
            ],
            [
                'Šifra' => '990144',
                'Tip' => 'D3 1 way receiver',
                'Model' => 'PDS2100 DVB-C IP Digital STB',
                'S/N' => '178215863'
            ],
            [
                'Šifra' => '990144',
                'Tip' => 'D3 1 way receiver',
                'Model' => 'PDS2100 DVB-C IP Digital STB',
                'S/N' => '167965921'
            ],
            [
                'Šifra' => '990144',
                'Tip' => 'D3 1 way receiver',
                'Model' => 'PDS2100 DVB-C IP Digital STB',
                'S/N' => '178202877'
            ],
            [
                'Šifra' => '990144',
                'Tip' => 'D3 1 way receiver',
                'Model' => 'PDS2100 DVB-C IP Digital STB',
                'S/N' => '168798734'
            ],
            [
                'Šifra' => '990144',
                'Tip' => 'D3 1 way receiver',
                'Model' => 'PDS2100 DVB-C IP Digital STB',
                'S/N' => '168971735'
            ],
            [
                'Šifra' => '990144',
                'Tip' => 'D3 1 way receiver',
                'Model' => 'PDS2100 DVB-C IP Digital STB',
                'S/N' => '178223952'
            ],
            [
                'Šifra' => '990144',
                'Tip' => 'D3 1 way receiver',
                'Model' => 'PDS2100 DVB-C IP Digital STB',
                'S/N' => '168092362'
            ],
            [
                'Šifra' => '990144',
                'Tip' => 'D3 1 way receiver',
                'Model' => 'PDS2100 DVB-C IP Digital STB',
                'S/N' => '168112887'
            ],
            [
                'Šifra' => '990144',
                'Tip' => 'D3 1 way receiver',
                'Model' => 'PDS2100 DVB-C IP Digital STB',
                'S/N' => '178595477'
            ],
            [
                'Šifra' => '990144',
                'Tip' => 'D3 1 way receiver',
                'Model' => 'PDS2100 DVB-C IP Digital STB',
                'S/N' => '178224568'
            ],
            [
                'Šifra' => '990144',
                'Tip' => 'D3 1 way receiver',
                'Model' => 'PDS2100 DVB-C IP Digital STB',
                'S/N' => '169565091'
            ],
            [
                'Šifra' => '990144',
                'Tip' => 'D3 1 way receiver',
                'Model' => 'PDS2100 DVB-C IP Digital STB',
                'S/N' => '178056179'
            ],
            [
                'Šifra' => '990144',
                'Tip' => 'D3 1 way receiver',
                'Model' => 'PDS2100 DVB-C IP Digital STB',
                'S/N' => '178208256'
            ],
            [
                'Šifra' => '990144',
                'Tip' => 'D3 1 way receiver',
                'Model' => 'PDS2100 DVB-C IP Digital STB',
                'S/N' => '178203725'
            ],
            [
                'Šifra' => '990144',
                'Tip' => 'D3 1 way receiver',
                'Model' => 'PDS2100 DVB-C IP Digital STB',
                'S/N' => '178079569'
            ],
            [
                'Šifra' => '990144',
                'Tip' => 'D3 1 way receiver',
                'Model' => 'PDS2100 DVB-C IP Digital STB',
                'S/N' => '168047286'
            ],
            [
                'Šifra' => '990144',
                'Tip' => 'D3 1 way receiver',
                'Model' => 'PDS2100 DVB-C IP Digital STB',
                'S/N' => '178208085'
            ],
            [
                'Šifra' => '990144',
                'Tip' => 'D3 1 way receiver',
                'Model' => 'PDS2100 DVB-C IP Digital STB',
                'S/N' => '178230069'
            ],
            [
                'Šifra' => '990144',
                'Tip' => 'D3 1 way receiver',
                'Model' => 'PDS2100 DVB-C IP Digital STB',
                'S/N' => '178206304'
            ],
            [
                'Šifra' => '990144',
                'Tip' => 'D3 1 way receiver',
                'Model' => 'PDS2100 DVB-C IP Digital STB',
                'S/N' => '178235694'
            ],
            [
                'Šifra' => '990144',
                'Tip' => 'D3 1 way receiver',
                'Model' => 'PDS2100 DVB-C IP Digital STB',
                'S/N' => '178424871'
            ],
            [
                'Šifra' => '990144',
                'Tip' => 'D3 1 way receiver',
                'Model' => 'PDS2100 DVB-C IP Digital STB',
                'S/N' => '168208701'
            ],
            [
                'Šifra' => '990144',
                'Tip' => 'D3 1 way receiver',
                'Model' => 'PDS2100 DVB-C IP Digital STB',
                'S/N' => '178558078'
            ],
            [
                'Šifra' => '990144',
                'Tip' => 'D3 1 way receiver',
                'Model' => 'PDS2100 DVB-C IP Digital STB',
                'S/N' => '178499727'
            ],
            [
                'Šifra' => '990144',
                'Tip' => 'D3 1 way receiver',
                'Model' => 'PDS2100 DVB-C IP Digital STB',
                'S/N' => '169089511'
            ],
            [
                'Šifra' => '990144',
                'Tip' => 'D3 1 way receiver',
                'Model' => 'PDS2100 DVB-C IP Digital STB',
                'S/N' => '178073333'
            ],
            [
                'Šifra' => '990144',
                'Tip' => 'D3 1 way receiver',
                'Model' => 'PDS2100 DVB-C IP Digital STB',
                'S/N' => '169272681'
            ],
            [
                'Šifra' => '990144',
                'Tip' => 'D3 1 way receiver',
                'Model' => 'PDS2100 DVB-C IP Digital STB',
                'S/N' => '178219474'
            ],
            [
                'Šifra' => '990144',
                'Tip' => 'D3 1 way receiver',
                'Model' => 'PDS2100 DVB-C IP Digital STB',
                'S/N' => '178075188'
            ],
            [
                'Šifra' => '990144',
                'Tip' => 'D3 1 way receiver',
                'Model' => 'PDS2100 DVB-C IP Digital STB',
                'S/N' => '178201361'
            ],
            [
                'Šifra' => '990144',
                'Tip' => 'D3 1 way receiver',
                'Model' => 'PDS2100 DVB-C IP Digital STB',
                'S/N' => '178073048'
            ],
            [
                'Šifra' => '990144',
                'Tip' => 'D3 1 way receiver',
                'Model' => 'PDS2100 DVB-C IP Digital STB',
                'S/N' => '178200135'
            ],
            [
                'Šifra' => '990144',
                'Tip' => 'D3 1 way receiver',
                'Model' => 'PDS2100 DVB-C IP Digital STB',
                'S/N' => '180021800'
            ],
            [
                'Šifra' => '990144',
                'Tip' => 'D3 1 way receiver',
                'Model' => 'PDS2100 DVB-C IP Digital STB',
                'S/N' => '168294851'
            ],
            [
                'Šifra' => '990144',
                'Tip' => 'D3 1 way receiver',
                'Model' => 'PDS2100 DVB-C IP Digital STB',
                'S/N' => '178080106'
            ],
            [
                'Šifra' => '990144',
                'Tip' => 'D3 1 way receiver',
                'Model' => 'PDS2100 DVB-C IP Digital STB',
                'S/N' => '168345960'
            ],
            [
                'Šifra' => '990144',
                'Tip' => 'D3 1 way receiver',
                'Model' => 'PDS2100 DVB-C IP Digital STB',
                'S/N' => '169329141'
            ],
            [
                'Šifra' => '990144',
                'Tip' => 'D3 1 way receiver',
                'Model' => 'PDS2100 DVB-C IP Digital STB',
                'S/N' => '178544070'
            ],
            [
                'Šifra' => '990144',
                'Tip' => 'D3 1 way receiver',
                'Model' => 'PDS2100 DVB-C IP Digital STB',
                'S/N' => '168185417'
            ],
            [
                'Šifra' => '990144',
                'Tip' => 'D3 1 way receiver',
                'Model' => 'PDS2100 DVB-C IP Digital STB',
                'S/N' => '168351222'
            ],
            [
                'Šifra' => '990144',
                'Tip' => 'D3 1 way receiver',
                'Model' => 'PDS2100 DVB-C IP Digital STB',
                'S/N' => '178078161'
            ],
            [
                'Šifra' => '990144',
                'Tip' => 'D3 1 way receiver',
                'Model' => 'PDS2100 DVB-C IP Digital STB',
                'S/N' => '178435492'
            ],
            [
                'Šifra' => '990144',
                'Tip' => 'D3 1 way receiver',
                'Model' => 'PDS2100 DVB-C IP Digital STB',
                'S/N' => '178219431'
            ],
            [
                'Šifra' => '990144',
                'Tip' => 'D3 1 way receiver',
                'Model' => 'PDS2100 DVB-C IP Digital STB',
                'S/N' => '169158079'
            ],
            [
                'Šifra' => '990144',
                'Tip' => 'D3 1 way receiver',
                'Model' => 'PDS2100 DVB-C IP Digital STB',
                'S/N' => '178219792'
            ],
            [
                'Šifra' => '990144',
                'Tip' => 'D3 1 way receiver',
                'Model' => 'PDS2100 DVB-C IP Digital STB',
                'S/N' => '178229815'
            ],
            [
                'Šifra' => '990144',
                'Tip' => 'D3 1 way receiver',
                'Model' => 'PDS2100 DVB-C IP Digital STB',
                'S/N' => '178549734'
            ],
            [
                'Šifra' => '990144',
                'Tip' => 'D3 1 way receiver',
                'Model' => 'PDS2100 DVB-C IP Digital STB',
                'S/N' => '178495037'
            ],
            [
                'Šifra' => '990144',
                'Tip' => 'D3 1 way receiver',
                'Model' => 'PDS2100 DVB-C IP Digital STB',
                'S/N' => '169388510'
            ],
            [
                'Šifra' => '990144',
                'Tip' => 'D3 1 way receiver',
                'Model' => 'PDS2100 DVB-C IP Digital STB',
                'S/N' => '168805496'
            ],
            [
                'Šifra' => '990144',
                'Tip' => 'D3 1 way receiver',
                'Model' => 'PDS2100 DVB-C IP Digital STB',
                'S/N' => '168294822'
            ],
            [
                'Šifra' => '990144',
                'Tip' => 'D3 1 way receiver',
                'Model' => 'PDS2100 DVB-C IP Digital STB',
                'S/N' => '169286729'
            ],
            [
                'Šifra' => '990144',
                'Tip' => 'D3 1 way receiver',
                'Model' => 'PDS2100 DVB-C IP Digital STB',
                'S/N' => '178232111'
            ],
            [
                'Šifra' => '990144',
                'Tip' => 'D3 1 way receiver',
                'Model' => 'PDS2100 DVB-C IP Digital STB',
                'S/N' => '178225671'
            ],
            [
                'Šifra' => '990144',
                'Tip' => 'D3 1 way receiver',
                'Model' => 'PDS2100 DVB-C IP Digital STB',
                'S/N' => '178220556'
            ],
            [
                'Šifra' => '990144',
                'Tip' => 'D3 1 way receiver',
                'Model' => 'PDS2100 DVB-C IP Digital STB',
                'S/N' => '178220368'
            ],
            [
                'Šifra' => '990151',
                'Tip' => 'Cable Advance Home Gateway',
                'Model' => 'Ubee EVW32C-0N',
                'S/N' => 'EVW32C0N00176402'
            ],
            [
                'Šifra' => '990151',
                'Tip' => 'Cable Advance Home Gateway',
                'Model' => 'Ubee EVW32C-0N',
                'S/N' => 'EVW32C0N00204932'
            ],
            [
                'Šifra' => '990151',
                'Tip' => 'Cable Advance Home Gateway',
                'Model' => 'Ubee EVW32C-0N',
                'S/N' => 'EVW32C0N00207134'
            ],
            [
                'Šifra' => '990151',
                'Tip' => 'Cable Advance Home Gateway',
                'Model' => 'Ubee EVW32C-0N',
                'S/N' => 'EVW32C0N00207323'
            ],
            [
                'Šifra' => '990151',
                'Tip' => 'Cable Advance Home Gateway',
                'Model' => 'Ubee EVW32C-0N',
                'S/N' => 'EVW32C0N00178474'
            ],
            [
                'Šifra' => '990151',
                'Tip' => 'Cable Advance Home Gateway',
                'Model' => 'Ubee EVW32C-0N',
                'S/N' => 'EVW32C0N00178475'
            ],
            [
                'Šifra' => '990151',
                'Tip' => 'Cable Advance Home Gateway',
                'Model' => 'Ubee EVW32C-0N',
                'S/N' => 'EVW32C0N00207205'
            ],
            [
                'Šifra' => '990151',
                'Tip' => 'Cable Advance Home Gateway',
                'Model' => 'Ubee EVW32C-0N',
                'S/N' => 'EVW32C0N00207094'
            ],
            [
                'Šifra' => '990151',
                'Tip' => 'Cable Advance Home Gateway',
                'Model' => 'Ubee EVW32C-0N',
                'S/N' => 'EVW32C0N00208604'
            ],
            [
                'Šifra' => '990151',
                'Tip' => 'Cable Advance Home Gateway',
                'Model' => 'Ubee EVW32C-0N',
                'S/N' => 'EVW32C0N00178959'
            ],
            [
                'Šifra' => '990151',
                'Tip' => 'Cable Advance Home Gateway',
                'Model' => 'Ubee EVW32C-0N',
                'S/N' => 'EVW32C0N00207155'
            ],
            [
                'Šifra' => '990151',
                'Tip' => 'Cable Advance Home Gateway',
                'Model' => 'Ubee EVW32C-0N',
                'S/N' => 'EVW32C0N00207084'
            ],
            [
                'Šifra' => '990151',
                'Tip' => 'Cable Advance Home Gateway',
                'Model' => 'Ubee EVW32C-0N',
                'S/N' => 'EVW32C0N00207206'
            ],
            [
                'Šifra' => '990151',
                'Tip' => 'Cable Advance Home Gateway',
                'Model' => 'Ubee EVW32C-0N',
                'S/N' => 'EVW32C0N00204923'
            ],
            [
                'Šifra' => '990151',
                'Tip' => 'Cable Advance Home Gateway',
                'Model' => 'Ubee EVW32C-0N',
                'S/N' => 'EVW32C0N00009697'
            ],
            [
                'Šifra' => '990151',
                'Tip' => 'Cable Advance Home Gateway',
                'Model' => 'Ubee EVW32C-0N',
                'S/N' => 'EVW32C0N00207174'
            ],
            [
                'Šifra' => '990151',
                'Tip' => 'Cable Advance Home Gateway',
                'Model' => 'Ubee EVW32C-0N',
                'S/N' => 'EVW32C0N00204751'
            ],
            [
                'Šifra' => '990151',
                'Tip' => 'Cable Advance Home Gateway',
                'Model' => 'Ubee EVW32C-0N',
                'S/N' => 'EVW32C0N00204763'
            ],
            [
                'Šifra' => '990155',
                'Tip' => 'Tehnicolor',
                'Model' => 'HGW CGA2121 EuroDOCSIS 3.0',
                'S/N' => '294912020126103792'
            ],
            [
                'Šifra' => '990155',
                'Tip' => 'Tehnicolor',
                'Model' => 'HGW CGA2121 EuroDOCSIS 3.0',
                'S/N' => '294912029334106463'
            ],
            [
                'Šifra' => '990155',
                'Tip' => 'Tehnicolor',
                'Model' => 'HGW CGA2121 EuroDOCSIS 3.0',
                'S/N' => '201912018493104117'
            ],
            [
                'Šifra' => '990155',
                'Tip' => 'Tehnicolor',
                'Model' => 'HGW CGA2121 EuroDOCSIS 3.0',
                'S/N' => '294912020115100147'
            ],
            [
                'Šifra' => '990155',
                'Tip' => 'Tehnicolor',
                'Model' => 'HGW CGA2121 EuroDOCSIS 3.0',
                'S/N' => '294912029475101819'
            ],
            [
                'Šifra' => '990155',
                'Tip' => 'Tehnicolor',
                'Model' => 'HGW CGA2121 EuroDOCSIS 3.0',
                'S/N' => '294912029126102906'
            ],
            [
                'Šifra' => '990155',
                'Tip' => 'Tehnicolor',
                'Model' => 'HGW CGA2121 EuroDOCSIS 3.0',
                'S/N' => '201912018154102997'
            ],
            [
                'Šifra' => '990155',
                'Tip' => 'Tehnicolor',
                'Model' => 'HGW CGA2121 EuroDOCSIS 3.0',
                'S/N' => '294912029126103648'
            ],
            [
                'Šifra' => '990155',
                'Tip' => 'Tehnicolor',
                'Model' => 'HGW CGA2121 EuroDOCSIS 3.0',
                'S/N' => '201912018272100867'
            ],
            [
                'Šifra' => '990155',
                'Tip' => 'Tehnicolor',
                'Model' => 'HGW CGA2121 EuroDOCSIS 3.0',
                'S/N' => '294912029334103108'
            ],
            [
                'Šifra' => '990155',
                'Tip' => 'Tehnicolor',
                'Model' => 'HGW CGA2121 EuroDOCSIS 3.0',
                'S/N' => '201912018493103046'
            ],
            [
                'Šifra' => '990155',
                'Tip' => 'Tehnicolor',
                'Model' => 'HGW CGA2121 EuroDOCSIS 3.0',
                'S/N' => '201912018343102896'
            ],
            [
                'Šifra' => '990155',
                'Tip' => 'Tehnicolor',
                'Model' => 'HGW CGA2121 EuroDOCSIS 3.0',
                'S/N' => '201912018343102365'
            ],
            [
                'Šifra' => '990155',
                'Tip' => 'Tehnicolor',
                'Model' => 'HGW CGA2121 EuroDOCSIS 3.0',
                'S/N' => '294912029442104243'
            ],
            [
                'Šifra' => '990155',
                'Tip' => 'Tehnicolor',
                'Model' => 'HGW CGA2121 EuroDOCSIS 3.0',
                'S/N' => '201912018156101820'
            ],
            [
                'Šifra' => '990155',
                'Tip' => 'Tehnicolor',
                'Model' => 'HGW CGA2121 EuroDOCSIS 3.0',
                'S/N' => '294912029466105485'
            ],
            [
                'Šifra' => '990155',
                'Tip' => 'Tehnicolor',
                'Model' => 'HGW CGA2121 EuroDOCSIS 3.0',
                'S/N' => '294912029425103250'
            ],
            [
                'Šifra' => '990155',
                'Tip' => 'Tehnicolor',
                'Model' => 'HGW CGA2121 EuroDOCSIS 3.0',
                'S/N' => '294912029192105835'
            ],
            [
                'Šifra' => '990155',
                'Tip' => 'Tehnicolor',
                'Model' => 'HGW CGA2121 EuroDOCSIS 3.0',
                'S/N' => '294912029223103602'
            ],
            [
                'Šifra' => '990155',
                'Tip' => 'Tehnicolor',
                'Model' => 'HGW CGA2121 EuroDOCSIS 3.0',
                'S/N' => '201912018495100192'
            ],
            [
                'Šifra' => '990155',
                'Tip' => 'Tehnicolor',
                'Model' => 'HGW CGA2121 EuroDOCSIS 3.0',
                'S/N' => '294912029192105393'
            ],
            [
                'Šifra' => '990155',
                'Tip' => 'Tehnicolor',
                'Model' => 'HGW CGA2121 EuroDOCSIS 3.0',
                'S/N' => '201912018416105315'
            ],
            [
                'Šifra' => '990155',
                'Tip' => 'Tehnicolor',
                'Model' => 'HGW CGA2121 EuroDOCSIS 3.0',
                'S/N' => '294912020135106097'
            ],
            [
                'Šifra' => '990155',
                'Tip' => 'Tehnicolor',
                'Model' => 'HGW CGA2121 EuroDOCSIS 3.0',
                'S/N' => '294912029304101717'
            ],
            [
                'Šifra' => '990155',
                'Tip' => 'Tehnicolor',
                'Model' => 'HGW CGA2121 EuroDOCSIS 3.0',
                'S/N' => '294912029104100023'
            ],
            [
                'Šifra' => '990155',
                'Tip' => 'Tehnicolor',
                'Model' => 'HGW CGA2121 EuroDOCSIS 3.0',
                'S/N' => '294912029475100008'
            ],
            [
                'Šifra' => '990155',
                'Tip' => 'Tehnicolor',
                'Model' => 'HGW CGA2121 EuroDOCSIS 3.0',
                'S/N' => '201912018495102525'
            ],
            [
                'Šifra' => '990155',
                'Tip' => 'Tehnicolor',
                'Model' => 'HGW CGA2121 EuroDOCSIS 3.0',
                'S/N' => '294912029192102176'
            ],
            [
                'Šifra' => '990155',
                'Tip' => 'Tehnicolor',
                'Model' => 'HGW CGA2121 EuroDOCSIS 3.0',
                'S/N' => '294912029192105875'
            ],
            [
                'Šifra' => '990155',
                'Tip' => 'Tehnicolor',
                'Model' => 'HGW CGA2121 EuroDOCSIS 3.0',
                'S/N' => '294912029106100504'
            ],
            [
                'Šifra' => '990155',
                'Tip' => 'Tehnicolor',
                'Model' => 'HGW CGA2121 EuroDOCSIS 3.0',
                'S/N' => '201912018345102972'
            ],
            [
                'Šifra' => '990155',
                'Tip' => 'Tehnicolor',
                'Model' => 'HGW CGA2121 EuroDOCSIS 3.0',
                'S/N' => '294912020126105097'
            ],
            [
                'Šifra' => '990155',
                'Tip' => 'Tehnicolor',
                'Model' => 'HGW CGA2121 EuroDOCSIS 3.0',
                'S/N' => '294912029084102221'
            ],
            [
                'Šifra' => '990155',
                'Tip' => 'Tehnicolor',
                'Model' => 'HGW CGA2121 EuroDOCSIS 3.0',
                'S/N' => '201912018272105339'
            ],
            [
                'Šifra' => '990155',
                'Tip' => 'Tehnicolor',
                'Model' => 'HGW CGA2121 EuroDOCSIS 3.0',
                'S/N' => '294912029425101682'
            ],
            [
                'Šifra' => '990155',
                'Tip' => 'Tehnicolor',
                'Model' => 'HGW CGA2121 EuroDOCSIS 3.0',
                'S/N' => '294912029334104680'
            ],
            [
                'Šifra' => '990155',
                'Tip' => 'Tehnicolor',
                'Model' => 'HGW CGA2121 EuroDOCSIS 3.0',
                'S/N' => '201912019027100205'
            ],
            [
                'Šifra' => '990155',
                'Tip' => 'Tehnicolor',
                'Model' => 'HGW CGA2121 EuroDOCSIS 3.0',
                'S/N' => '201912019072102883'
            ],
            [
                'Šifra' => '990155',
                'Tip' => 'Tehnicolor',
                'Model' => 'HGW CGA2121 EuroDOCSIS 3.0',
                'S/N' => '294912020195104698'
            ],
            [
                'Šifra' => '990155',
                'Tip' => 'Tehnicolor',
                'Model' => 'HGW CGA2121 EuroDOCSIS 3.0',
                'S/N' => '294912020195104646'
            ],
            [
                'Šifra' => '990155',
                'Tip' => 'Tehnicolor',
                'Model' => 'HGW CGA2121 EuroDOCSIS 3.0',
                'S/N' => '294912020192103315'
            ],
            [
                'Šifra' => '990155',
                'Tip' => 'Tehnicolor',
                'Model' => 'HGW CGA2121 EuroDOCSIS 3.0',
                'S/N' => '294912029512101579'
            ],
            [
                'Šifra' => '990155',
                'Tip' => 'Tehnicolor',
                'Model' => 'HGW CGA2121 EuroDOCSIS 3.0',
                'S/N' => '294912020195105308'
            ],
            [
                'Šifra' => '990155',
                'Tip' => 'Tehnicolor',
                'Model' => 'HGW CGA2121 EuroDOCSIS 3.0',
                'S/N' => '294912020195105228'
            ],
            [
                'Šifra' => '990155',
                'Tip' => 'Tehnicolor',
                'Model' => 'HGW CGA2121 EuroDOCSIS 3.0',
                'S/N' => '294912020195106343'
            ],
            [
                'Šifra' => '990155',
                'Tip' => 'Tehnicolor',
                'Model' => 'HGW CGA2121 EuroDOCSIS 3.0',
                'S/N' => '294912020195105285'
            ],
            [
                'Šifra' => '990155',
                'Tip' => 'Tehnicolor',
                'Model' => 'HGW CGA2121 EuroDOCSIS 3.0',
                'S/N' => '294912020195105298'
            ],
            [
                'Šifra' => '990155',
                'Tip' => 'Tehnicolor',
                'Model' => 'HGW CGA2121 EuroDOCSIS 3.0',
                'S/N' => '294912029512101605'
            ],
            [
                'Šifra' => '990155',
                'Tip' => 'Tehnicolor',
                'Model' => 'HGW CGA2121 EuroDOCSIS 3.0',
                'S/N' => '294912029512101477'
            ],
            [
                'Šifra' => '990155',
                'Tip' => 'Tehnicolor',
                'Model' => 'HGW CGA2121 EuroDOCSIS 3.0',
                'S/N' => '294912029512101479'
            ],
            [
                'Šifra' => '990155',
                'Tip' => 'Tehnicolor',
                'Model' => 'HGW CGA2121 EuroDOCSIS 3.0',
                'S/N' => '294912029512101418'
            ],
            [
                'Šifra' => '990155',
                'Tip' => 'Tehnicolor',
                'Model' => 'HGW CGA2121 EuroDOCSIS 3.0',
                'S/N' => '294912029512101394'
            ],
            [
                'Šifra' => '990155',
                'Tip' => 'Tehnicolor',
                'Model' => 'HGW CGA2121 EuroDOCSIS 3.0',
                'S/N' => '294912020195106380'
            ],
            [
                'Šifra' => '990155',
                'Tip' => 'Tehnicolor',
                'Model' => 'HGW CGA2121 EuroDOCSIS 3.0',
                'S/N' => '294912020195105255'
            ],
            [
                'Šifra' => '990155',
                'Tip' => 'Tehnicolor',
                'Model' => 'HGW CGA2121 EuroDOCSIS 3.0',
                'S/N' => '294912020195105214'
            ],
            [
                'Šifra' => '990155',
                'Tip' => 'Tehnicolor',
                'Model' => 'HGW CGA2121 EuroDOCSIS 3.0',
                'S/N' => '294912020195105217'
            ],
            [
                'Šifra' => '990070',
                'Tip' => 'GPON ONT ADVANCE',
                'Model' => 'Huawei HG8247W5 GPON',
                'S/N' => '485754437B1E2AA3'
            ],
            [
                'Šifra' => '990070',
                'Tip' => 'GPON ONT ADVANCE',
                'Model' => 'Huawei HG8247W5 GPON',
                'S/N' => '485754437B1ED3A3'
            ],
            [
                'Šifra' => '990070',
                'Tip' => 'GPON ONT ADVANCE',
                'Model' => 'Huawei HG8247W5 GPON',
                'S/N' => '485754437B1EC6A3'
            ],
            [
                'Šifra' => '990070',
                'Tip' => 'GPON ONT ADVANCE',
                'Model' => 'Huawei HG8247W5 GPON',
                'S/N' => '485754437B1EB7A3'
            ],
            [
                'Šifra' => '990070',
                'Tip' => 'GPON ONT ADVANCE',
                'Model' => 'Huawei HG8247W5 GPON',
                'S/N' => '485754437B1EBDA3'
            ],
            [
                'Šifra' => '990070',
                'Tip' => 'GPON ONT ADVANCE',
                'Model' => 'Huawei HG8247W5 GPON',
                'S/N' => '485754437B1EC0A3'
            ],
            [
                'Šifra' => '990070',
                'Tip' => 'GPON ONT ADVANCE',
                'Model' => 'Huawei HG8247W5 GPON',
                'S/N' => '485754437B1EC7A3'
            ],
            [
                'Šifra' => '990070',
                'Tip' => 'GPON ONT ADVANCE',
                'Model' => 'Huawei HG8247W5 GPON',
                'S/N' => '485754436ED1FBA3'
            ],
            [
                'Šifra' => '990070',
                'Tip' => 'GPON ONT ADVANCE',
                'Model' => 'Huawei HG8247W5 GPON',
                'S/N' => '485754436ED209A3'
            ],
            [
                'Šifra' => '990070',
                'Tip' => 'GPON ONT ADVANCE',
                'Model' => 'Huawei HG8247W5 GPON',
                'S/N' => '485754436ED20DA3'
            ],
            [
                'Šifra' => '990070',
                'Tip' => 'GPON ONT ADVANCE',
                'Model' => 'Huawei HG8247W5 GPON',
                'S/N' => '485754436ED27FA3'
            ],
            [
                'Šifra' => '990070',
                'Tip' => 'GPON ONT ADVANCE',
                'Model' => 'Huawei HG8247W5 GPON',
                'S/N' => '485754436ED248A3'
            ],
            [
                'Šifra' => '990070',
                'Tip' => 'GPON ONT ADVANCE',
                'Model' => 'Huawei HG8247W5 GPON',
                'S/N' => '485754436ED3B6A3'
            ],
            [
                'Šifra' => '990070',
                'Tip' => 'GPON ONT ADVANCE',
                'Model' => 'Huawei HG8247W5 GPON',
                'S/N' => '485754436ED3DDA3'
            ],
            [
                'Šifra' => '990070',
                'Tip' => 'GPON ONT ADVANCE',
                'Model' => 'Huawei HG8247W5 GPON',
                'S/N' => '485754436ED2CDA3'
            ],
            [
                'Šifra' => '990070',
                'Tip' => 'GPON ONT ADVANCE',
                'Model' => 'Huawei HG8247W5 GPON',
                'S/N' => '485754436ED288A3'
            ],
            [
                'Šifra' => '990070',
                'Tip' => 'GPON ONT ADVANCE',
                'Model' => 'Huawei HG8247W5 GPON',
                'S/N' => '485754436ED27AA3'
            ],
            [
                'Šifra' => '990070',
                'Tip' => 'GPON ONT ADVANCE',
                'Model' => 'Huawei HG8247W5 GPON',
                'S/N' => '485754436ED261A3'
            ],
            [
                'Šifra' => '990070',
                'Tip' => 'GPON ONT ADVANCE',
                'Model' => 'Huawei HG8247W5 GPON',
                'S/N' => '485754436ED268A3'
            ],
            [
                'Šifra' => '990070',
                'Tip' => 'GPON ONT ADVANCE',
                'Model' => 'Huawei HG8247W5 GPON',
                'S/N' => '485754436ED241A3'
            ],
            [
                'Šifra' => '990070',
                'Tip' => 'GPON ONT ADVANCE',
                'Model' => 'Huawei HG8247W5 GPON',
                'S/N' => '485754436ED25EA3'
            ],
            [
                'Šifra' => '990070',
                'Tip' => 'GPON ONT ADVANCE',
                'Model' => 'Huawei HG8247W5 GPON',
                'S/N' => '485754436ED273A3'
            ],
            [
                'Šifra' => '990070',
                'Tip' => 'GPON ONT ADVANCE',
                'Model' => 'Huawei HG8247W5 GPON',
                'S/N' => '485754436ED26BA3'
            ],
            [
                'Šifra' => '990070',
                'Tip' => 'GPON ONT ADVANCE',
                'Model' => 'Huawei HG8247W5 GPON',
                'S/N' => '485754436ED267A3'
            ],
            [
                'Šifra' => '990070',
                'Tip' => 'GPON ONT ADVANCE',
                'Model' => 'Huawei HG8247W5 GPON',
                'S/N' => '485754436ED1F9A3'
            ],
            [
                'Šifra' => '990070',
                'Tip' => 'GPON ONT ADVANCE',
                'Model' => 'Huawei HG8247W5 GPON',
                'S/N' => '485754436ED1F0A3'
            ],
            [
                'Šifra' => '990070',
                'Tip' => 'GPON ONT ADVANCE',
                'Model' => 'Huawei HG8247W5 GPON',
                'S/N' => '485754436ED263A3'
            ],
            [
                'Šifra' => '990070',
                'Tip' => 'GPON ONT ADVANCE',
                'Model' => 'Huawei HG8247W5 GPON',
                'S/N' => '485754437B1CFCA3'
            ],
            [
                'Šifra' => '990070',
                'Tip' => 'GPON ONT ADVANCE',
                'Model' => 'Huawei HG8247W5 GPON',
                'S/N' => '485754437B1D16A3'
            ],
            [
                'Šifra' => '990070',
                'Tip' => 'GPON ONT ADVANCE',
                'Model' => 'Huawei HG8247W5 GPON',
                'S/N' => '485754437B1D1EA3'
            ],
            [
                'Šifra' => '990070',
                'Tip' => 'GPON ONT ADVANCE',
                'Model' => 'Huawei HG8247W5 GPON',
                'S/N' => '485754437B1D22A3'
            ],
            [
                'Šifra' => '990070',
                'Tip' => 'GPON ONT ADVANCE',
                'Model' => 'Huawei HG8247W5 GPON',
                'S/N' => '485754437B1D2AA3'
            ],
            [
                'Šifra' => '990070',
                'Tip' => 'GPON ONT ADVANCE',
                'Model' => 'Huawei HG8247W5 GPON',
                'S/N' => '485754437B1D39A3'
            ],
            [
                'Šifra' => '990070',
                'Tip' => 'GPON ONT ADVANCE',
                'Model' => 'Huawei HG8247W5 GPON',
                'S/N' => '485754437B1D3CA3'
            ],
            [
                'Šifra' => '990070',
                'Tip' => 'GPON ONT ADVANCE',
                'Model' => 'Huawei HG8247W5 GPON',
                'S/N' => '485754437B1D4AA3'
            ],
            [
                'Šifra' => '990070',
                'Tip' => 'GPON ONT ADVANCE',
                'Model' => 'Huawei HG8247W5 GPON',
                'S/N' => '485754437B1D4BA3'
            ],
            [
                'Šifra' => '990070',
                'Tip' => 'GPON ONT ADVANCE',
                'Model' => 'Huawei HG8247W5 GPON',
                'S/N' => '485754437B1D4EA3'
            ],
            [
                'Šifra' => '990331',
                'Tip' => 'HGW',
                'Model' => 'EPC3928S V2.5 GaTeWaY',
                'S/N' => '280209274'
            ],
            [
                'Šifra' => '990331',
                'Tip' => 'HGW',
                'Model' => 'EPC3928S V2.5 GaTeWaY',
                'S/N' => '285516256'
            ],
            [
                'Šifra' => '990331',
                'Tip' => 'HGW',
                'Model' => 'EPC3928S V2.5 GaTeWaY',
                'S/N' => '330759482'
            ],
            [
                'Šifra' => '990331',
                'Tip' => 'HGW',
                'Model' => 'EPC3928S V2.5 GaTeWaY',
                'S/N' => '280490006'
            ],
            [
                'Šifra' => '990331',
                'Tip' => 'HGW',
                'Model' => 'EPC3928S V2.5 GaTeWaY',
                'S/N' => '285550663'
            ],
            [
                'Šifra' => '990331',
                'Tip' => 'HGW',
                'Model' => 'EPC3928S V2.5 GaTeWaY',
                'S/N' => '281114737'
            ],
            [
                'Šifra' => '990331',
                'Tip' => 'HGW',
                'Model' => 'EPC3928S V2.5 GaTeWaY',
                'S/N' => '286289884'
            ],
            [
                'Šifra' => '990331',
                'Tip' => 'HGW',
                'Model' => 'EPC3928S V2.5 GaTeWaY',
                'S/N' => '286302720'
            ],
            [
                'Šifra' => '990331',
                'Tip' => 'HGW',
                'Model' => 'EPC3928S V2.5 GaTeWaY',
                'S/N' => '279112881'
            ],
            [
                'Šifra' => '990331',
                'Tip' => 'HGW',
                'Model' => 'EPC3928S V2.5 GaTeWaY',
                'S/N' => '286341618'
            ],
            [
                'Šifra' => '990331',
                'Tip' => 'HGW',
                'Model' => 'EPC3928S V2.5 GaTeWaY',
                'S/N' => '280220105'
            ],
            [
                'Šifra' => '990331',
                'Tip' => 'HGW',
                'Model' => 'EPC3928S V2.5 GaTeWaY',
                'S/N' => '331019350'
            ],
            [
                'Šifra' => '990331',
                'Tip' => 'HGW',
                'Model' => 'EPC3928S V2.5 GaTeWaY',
                'S/N' => '330288566'
            ],
            [
                'Šifra' => '990331',
                'Tip' => 'HGW',
                'Model' => 'EPC3928S V2.5 GaTeWaY',
                'S/N' => '280159415'
            ],
            [
                'Šifra' => '990331',
                'Tip' => 'HGW',
                'Model' => 'EPC3928S V2.5 GaTeWaY',
                'S/N' => '330762326'
            ],
            [
                'Šifra' => '990331',
                'Tip' => 'HGW',
                'Model' => 'EPC3928S V2.5 GaTeWaY',
                'S/N' => '330278604'
            ],
            [
                'Šifra' => '990331',
                'Tip' => 'HGW',
                'Model' => 'EPC3928S V2.5 GaTeWaY',
                'S/N' => '330532816'
            ],
            [
                'Šifra' => '990331',
                'Tip' => 'HGW',
                'Model' => 'EPC3928S V2.5 GaTeWaY',
                'S/N' => '281114816'
            ],
            [
                'Šifra' => '990331',
                'Tip' => 'HGW',
                'Model' => 'EPC3928S V2.5 GaTeWaY',
                'S/N' => '286817649'
            ],
            [
                'Šifra' => '990331',
                'Tip' => 'HGW',
                'Model' => 'EPC3928S V2.5 GaTeWaY',
                'S/N' => '285219056'
            ],
            [
                'Šifra' => '990331',
                'Tip' => 'HGW',
                'Model' => 'EPC3928S V2.5 GaTeWaY',
                'S/N' => '281100223'
            ],
            [
                'Šifra' => '990331',
                'Tip' => 'HGW',
                'Model' => 'EPC3928S V2.5 GaTeWaY',
                'S/N' => '286343611'
            ],
            [
                'Šifra' => '990331',
                'Tip' => 'HGW',
                'Model' => 'EPC3928S V2.5 GaTeWaY',
                'S/N' => '284699836'
            ],
            [
                'Šifra' => '990331',
                'Tip' => 'HGW',
                'Model' => 'EPC3928S V2.5 GaTeWaY',
                'S/N' => '286462253'
            ],
            [
                'Šifra' => '990331',
                'Tip' => 'HGW',
                'Model' => 'EPC3928S V2.5 GaTeWaY',
                'S/N' => '285590583'
            ],
            [
                'Šifra' => '990131',
                'Tip' => 'HGW',
                'Model' => 'EPC3928S GaTeWaY',
                'S/N' => '273233107'
            ],
            [
                'Šifra' => '990131',
                'Tip' => 'HGW',
                'Model' => 'EPC3928S GaTeWaY',
                'S/N' => '268468392'
            ],
            [
                'Šifra' => '990131',
                'Tip' => 'HGW',
                'Model' => 'EPC3928S GaTeWaY',
                'S/N' => '271805980'
            ],
            [
                'Šifra' => '990131',
                'Tip' => 'HGW',
                'Model' => 'EPC3928S GaTeWaY',
                'S/N' => '286827795'
            ],
            [
                'Šifra' => '990131',
                'Tip' => 'HGW',
                'Model' => 'EPC3928S GaTeWaY',
                'S/N' => '278929924'
            ],
            [
                'Šifra' => '990331',
                'Tip' => 'HGW',
                'Model' => 'EPC3928S V2.5 GaTeWaY',
                'S/N' => '330756005'
            ],
            [
                'Šifra' => '990331',
                'Tip' => 'HGW',
                'Model' => 'EPC3928S V2.5 GaTeWaY',
                'S/N' => '331000002'
            ],
            [
                'Šifra' => '990331',
                'Tip' => 'HGW',
                'Model' => 'EPC3928S V2.5 GaTeWaY',
                'S/N' => '281114816'
            ],
            [
                'Šifra' => '990331',
                'Tip' => 'HGW',
                'Model' => 'EPC3928S V2.5 GaTeWaY',
                'S/N' => '285200012'
            ],
            [
                'Šifra' => '990331',
                'Tip' => 'HGW',
                'Model' => 'EPC3928S V2.5 GaTeWaY',
                'S/N' => '330289642'
            ],
            [
                'Šifra' => '990331',
                'Tip' => 'HGW',
                'Model' => 'EPC3928S V2.5 GaTeWaY',
                'S/N' => '281266981'
            ],
            [
                'Šifra' => '990231',
                'Tip' => 'HGW',
                'Model' => 'EPC3928S V2 GaTeWaY',
                'S/N' => '277170439'
            ],
            [
                'Šifra' => '990331',
                'Tip' => 'HGW',
                'Model' => 'EPC3928S V2.5 GaTeWaY',
                'S/N' => '279239148'
            ],
            [
                'Šifra' => '990331',
                'Tip' => 'HGW',
                'Model' => 'EPC3928S V2.5 GaTeWaY',
                'S/N' => '330755350'
            ],
            [
                'Šifra' => '990331',
                'Tip' => 'HGW',
                'Model' => 'EPC3928S V2.5 GaTeWaY',
                'S/N' => '279232324'
            ],
            [
                'Šifra' => '990331',
                'Tip' => 'HGW',
                'Model' => 'EPC3928S V2.5 GaTeWaY',
                'S/N' => '330282374'
            ],
            [
                'Šifra' => '990331',
                'Tip' => 'HGW',
                'Model' => 'EPC3928S V2.5 GaTeWaY',
                'S/N' => '330530433'
            ],
            [
                'Šifra' => '990331',
                'Tip' => 'HGW',
                'Model' => 'EPC3928S V2.5 GaTeWaY',
                'S/N' => '278790323'
            ],
            [
                'Šifra' => '990331',
                'Tip' => 'HGW',
                'Model' => 'EPC3928S V2.5 GaTeWaY',
                'S/N' => '280200304'
            ],
            [
                'Šifra' => '990331',
                'Tip' => 'HGW',
                'Model' => 'EPC3928S V2.5 GaTeWaY',
                'S/N' => '282876217'
            ],
            [
                'Šifra' => '990331',
                'Tip' => 'HGW',
                'Model' => 'EPC3928S V2.5 GaTeWaY',
                'S/N' => '286338818'
            ],
            [
                'Šifra' => '990331',
                'Tip' => 'HGW',
                'Model' => 'EPC3928S V2.5 GaTeWaY',
                'S/N' => '330289619'
            ],
            [
                'Šifra' => '990331',
                'Tip' => 'HGW',
                'Model' => 'EPC3928S V2.5 GaTeWaY',
                'S/N' => '284748158'
            ],
            [
                'Šifra' => '990331',
                'Tip' => 'HGW',
                'Model' => 'EPC3928S V2.5 GaTeWaY',
                'S/N' => '280844324'
            ],
            [
                'Šifra' => '990331',
                'Tip' => 'HGW',
                'Model' => 'EPC3928S V2.5 GaTeWaY',
                'S/N' => '330282330'
            ],
            [
                'Šifra' => '990331',
                'Tip' => 'HGW',
                'Model' => 'EPC3928S V2.5 GaTeWaY',
                'S/N' => '286341514'
            ],
            [
                'Šifra' => '990331',
                'Tip' => 'HGW',
                'Model' => 'EPC3928S V2.5 GaTeWaY',
                'S/N' => '278792010'
            ],
            [
                'Šifra' => '990331',
                'Tip' => 'HGW',
                'Model' => 'EPC3928S V2.5 GaTeWaY',
                'S/N' => '279237036'
            ],
            [
                'Šifra' => '990331',
                'Tip' => 'HGW',
                'Model' => 'EPC3928S V2.5 GaTeWaY',
                'S/N' => '330279340'
            ],
            [
                'Šifra' => '990331',
                'Tip' => 'HGW',
                'Model' => 'EPC3928S V2.5 GaTeWaY',
                'S/N' => '286300862'
            ],
            [
                'Šifra' => '990331',
                'Tip' => 'HGW',
                'Model' => 'EPC3928S V2.5 GaTeWaY',
                'S/N' => '283471934'
            ],
            [
                'Šifra' => '990331',
                'Tip' => 'HGW',
                'Model' => 'EPC3928S V2.5 GaTeWaY',
                'S/N' => '286450869'
            ],
            [
                'Šifra' => '990331',
                'Tip' => 'HGW',
                'Model' => 'EPC3928S V2.5 GaTeWaY',
                'S/N' => '279718811'
            ],
            [
                'Šifra' => '990331',
                'Tip' => 'HGW',
                'Model' => 'EPC3928S V2.5 GaTeWaY',
                'S/N' => '285560235'
            ],
            [
                'Šifra' => '990331',
                'Tip' => 'HGW',
                'Model' => 'EPC3928S V2.5 GaTeWaY',
                'S/N' => '286282866'
            ],
            [
                'Šifra' => '990231',
                'Tip' => 'HGW',
                'Model' => 'EPC3928S V2 GaTeWaY',
                'S/N' => '277146530'
            ],
            [
                'Šifra' => '990331',
                'Tip' => 'HGW',
                'Model' => 'EPC3928S V2.5 GaTeWaY',
                'S/N' => '287177558'
            ],
            [
                'Šifra' => '990331',
                'Tip' => 'HGW',
                'Model' => 'EPC3928S V2.5 GaTeWaY',
                'S/N' => '286326503'
            ],
            [
                'Šifra' => '990131',
                'Tip' => 'HGW',
                'Model' => 'EPC3928S GaTeWaY',
                'S/N' => '269254480'
            ],
            [
                'Šifra' => '990331',
                'Tip' => 'HGW',
                'Model' => 'EPC3928S V2.5 GaTeWaY',
                'S/N' => '280472334'
            ],
            [
                'Šifra' => '990331',
                'Tip' => 'HGW',
                'Model' => 'EPC3928S V2.5 GaTeWaY',
                'S/N' => '330294872'
            ],
            [
                'Šifra' => '990331',
                'Tip' => 'HGW',
                'Model' => 'EPC3928S V2.5 GaTeWaY',
                'S/N' => '286313604'
            ],
            [
                'Šifra' => '990331',
                'Tip' => 'HGW',
                'Model' => 'EPC3928S V2.5 GaTeWaY',
                'S/N' => '284620959'
            ],
            [
                'Šifra' => '990331',
                'Tip' => 'HGW',
                'Model' => 'EPC3928S V2.5 GaTeWaY',
                'S/N' => '331018885'
            ],
            [
                'Šifra' => '990131',
                'Tip' => 'HGW',
                'Model' => 'EPC3928S GaTeWaY',
                'S/N' => '270138714'
            ],
            [
                'Šifra' => '990231',
                'Tip' => 'HGW',
                'Model' => 'EPC3928S V2 GaTeWaY',
                'S/N' => '276527552'
            ],
            [
                'Šifra' => '990331',
                'Tip' => 'HGW',
                'Model' => 'EPC3928S V2.5 GaTeWaY',
                'S/N' => '279709211'
            ],
            [
                'Šifra' => '990331',
                'Tip' => 'HGW',
                'Model' => 'EPC3928S V2.5 GaTeWaY',
                'S/N' => '280497672'
            ],
            [
                'Šifra' => '990331',
                'Tip' => 'HGW',
                'Model' => 'EPC3928S V2.5 GaTeWaY',
                'S/N' => '280744634'
            ],
            [
                'Šifra' => '990331',
                'Tip' => 'HGW',
                'Model' => 'EPC3928S V2.5 GaTeWaY',
                'S/N' => '280834367'
            ],
            [
                'Šifra' => '990331',
                'Tip' => 'HGW',
                'Model' => 'EPC3928S V2.5 GaTeWaY',
                'S/N' => '281992093'
            ],
            [
                'Šifra' => '990131',
                'Tip' => 'HGW',
                'Model' => 'EPC3928S GaTeWaY',
                'S/N' => '271810243'
            ],
            [
                'Šifra' => '990331',
                'Tip' => 'HGW',
                'Model' => 'EPC3928S V2.5 GaTeWaY',
                'S/N' => '278792013'
            ],
            [
                'Šifra' => '990331',
                'Tip' => 'HGW',
                'Model' => 'EPC3928S V2.5 GaTeWaY',
                'S/N' => '286443209'
            ],
            [
                'Šifra' => '990231',
                'Tip' => 'HGW',
                'Model' => 'EPC3928S V2 GaTeWaY',
                'S/N' => '274537906'
            ],
            [
                'Šifra' => '990331',
                'Tip' => 'HGW',
                'Model' => 'EPC3928S V2.5 GaTeWaY',
                'S/N' => '330533887'
            ],
            [
                'Šifra' => '990331',
                'Tip' => 'HGW',
                'Model' => 'EPC3928S V2.5 GaTeWaY',
                'S/N' => '279232462'
            ],
            [
                'Šifra' => '990231',
                'Tip' => 'HGW',
                'Model' => 'EPC3928S V2 GaTeWaY',
                'S/N' => '274690297'
            ],
            [
                'Šifra' => '990331',
                'Tip' => 'HGW',
                'Model' => 'EPC3928S V2.5 GaTeWaY',
                'S/N' => '281992806'
            ],
            [
                'Šifra' => '990231',
                'Tip' => 'HGW',
                'Model' => 'EPC3928S V2 GaTeWaY',
                'S/N' => '274540613'
            ],
            [
                'Šifra' => '990131',
                'Tip' => 'HGW',
                'Model' => 'EPC3928S GaTeWaY',
                'S/N' => '268816584'
            ],
            [
                'Šifra' => '990231',
                'Tip' => 'HGW',
                'Model' => 'EPC3928S V2 GaTeWaY',
                'S/N' => '277243361'
            ],
            [
                'Šifra' => '990331',
                'Tip' => 'HGW',
                'Model' => 'EPC3928S V2.5 GaTeWaY',
                'S/N' => '278153018'
            ],
            [
                'Šifra' => '990131',
                'Tip' => 'HGW',
                'Model' => 'EPC3928S GaTeWaY',
                'S/N' => '269930203'
            ],
            [
                'Šifra' => '990231',
                'Tip' => 'HGW',
                'Model' => 'EPC3928S V2 GaTeWaY',
                'S/N' => '275872445'
            ],
            [
                'Šifra' => '990231',
                'Tip' => 'HGW',
                'Model' => 'EPC3928S V2 GaTeWaY',
                'S/N' => '276419070'
            ],
            [
                'Šifra' => '990231',
                'Tip' => 'HGW',
                'Model' => 'EPC3928S V2 GaTeWaY',
                'S/N' => '276520117'
            ],
            [
                'Šifra' => '990331',
                'Tip' => 'HGW',
                'Model' => 'EPC3928S V2.5 GaTeWaY',
                'S/N' => '279093438'
            ],
            [
                'Šifra' => '990331',
                'Tip' => 'HGW',
                'Model' => 'EPC3928S V2.5 GaTeWaY',
                'S/N' => '286816616'
            ],
            [
                'Šifra' => '990331',
                'Tip' => 'HGW',
                'Model' => 'EPC3928S V2.5 GaTeWaY',
                'S/N' => '281989437'
            ],
            [
                'Šifra' => '990331',
                'Tip' => 'HGW',
                'Model' => 'EPC3928S V2.5 GaTeWaY',
                'S/N' => '286188708'
            ],
            [
                'Šifra' => '990331',
                'Tip' => 'HGW',
                'Model' => 'EPC3928S V2.5 GaTeWaY',
                'S/N' => '330537164'
            ],
            [
                'Šifra' => '990331',
                'Tip' => 'HGW',
                'Model' => 'EPC3928S V2.5 GaTeWaY',
                'S/N' => '280820606'
            ],
            [
                'Šifra' => '990331',
                'Tip' => 'HGW',
                'Model' => 'EPC3928S V2.5 GaTeWaY',
                'S/N' => '280168556'
            ],
            [
                'Šifra' => '990331',
                'Tip' => 'HGW',
                'Model' => 'EPC3928S V2.5 GaTeWaY',
                'S/N' => '280223010'
            ],
            [
                'Šifra' => '990331',
                'Tip' => 'HGW',
                'Model' => 'EPC3928S V2.5 GaTeWaY',
                'S/N' => '278930849'
            ],
            [
                'Šifra' => '990331',
                'Tip' => 'HGW',
                'Model' => 'EPC3928S V2.5 GaTeWaY',
                'S/N' => '283460397'
            ],
            [
                'Šifra' => '990331',
                'Tip' => 'HGW',
                'Model' => 'EPC3928S V2.5 GaTeWaY',
                'S/N' => '282372785'
            ],
            [
                'Šifra' => '990331',
                'Tip' => 'HGW',
                'Model' => 'EPC3928S V2.5 GaTeWaY',
                'S/N' => '285520800'
            ],
            [
                'Šifra' => '990331',
                'Tip' => 'HGW',
                'Model' => 'EPC3928S V2.5 GaTeWaY',
                'S/N' => '281114312'
            ],
            [
                'Šifra' => '990331',
                'Tip' => 'HGW',
                'Model' => 'EPC3928S V2.5 GaTeWaY',
                'S/N' => '279232303'
            ],
            [
                'Šifra' => '990331',
                'Tip' => 'HGW',
                'Model' => 'EPC3928S V2.5 GaTeWaY',
                'S/N' => '282358810'
            ],
            [
                'Šifra' => '990331',
                'Tip' => 'HGW',
                'Model' => 'EPC3928S V2.5 GaTeWaY',
                'S/N' => '330288984'
            ],
            [
                'Šifra' => '990331',
                'Tip' => 'HGW',
                'Model' => 'EPC3928S V2.5 GaTeWaY',
                'S/N' => '279710289'
            ],
            [
                'Šifra' => '990331',
                'Tip' => 'HGW',
                'Model' => 'EPC3928S V2.5 GaTeWaY',
                'S/N' => '285522537'
            ],
            [
                'Šifra' => '990331',
                'Tip' => 'HGW',
                'Model' => 'EPC3928S V2.5 GaTeWaY',
                'S/N' => '330288534'
            ],
            [
                'Šifra' => '990231',
                'Tip' => 'HGW',
                'Model' => 'EPC3928S V2 GaTeWaY',
                'S/N' => '277047802'
            ],
            [
                'Šifra' => '990331',
                'Tip' => 'HGW',
                'Model' => 'EPC3928S V2.5 GaTeWaY',
                'S/N' => '330295218'
            ],
            [
                'Šifra' => '990331',
                'Tip' => 'HGW',
                'Model' => 'EPC3928S V2.5 GaTeWaY',
                'S/N' => '286343981'
            ],
            [
                'Šifra' => '990331',
                'Tip' => 'HGW',
                'Model' => 'EPC3928S V2.5 GaTeWaY',
                'S/N' => '278757810'
            ],
            [
                'Šifra' => '990331',
                'Tip' => 'HGW',
                'Model' => 'EPC3928S V2.5 GaTeWaY',
                'S/N' => '330053233'
            ],
            [
                'Šifra' => '990331',
                'Tip' => 'HGW',
                'Model' => 'EPC3928S V2.5 GaTeWaY',
                'S/N' => '281086058'
            ],
            [
                'Šifra' => '990331',
                'Tip' => 'HGW',
                'Model' => 'EPC3928S V2.5 GaTeWaY',
                'S/N' => '280223202'
            ],
            [
                'Šifra' => '990331',
                'Tip' => 'HGW',
                'Model' => 'EPC3928S V2.5 GaTeWaY',
                'S/N' => '330533159'
            ],
            [
                'Šifra' => '990331',
                'Tip' => 'HGW',
                'Model' => 'EPC3928S V2.5 GaTeWaY',
                'S/N' => '278928376'
            ],
            [
                'Šifra' => '990331',
                'Tip' => 'HGW',
                'Model' => 'EPC3928S V2.5 GaTeWaY',
                'S/N' => '280216174'
            ],
            [
                'Šifra' => '990331',
                'Tip' => 'HGW',
                'Model' => 'EPC3928S V2.5 GaTeWaY',
                'S/N' => '286290160'
            ],
            [
                'Šifra' => '990331',
                'Tip' => 'HGW',
                'Model' => 'EPC3928S V2.5 GaTeWaY',
                'S/N' => '281114508'
            ],
            [
                'Šifra' => '990331',
                'Tip' => 'HGW',
                'Model' => 'EPC3928S V2.5 GaTeWaY',
                'S/N' => '278758615'
            ],
            [
                'Šifra' => '990331',
                'Tip' => 'HGW',
                'Model' => 'EPC3928S V2.5 GaTeWaY',
                'S/N' => '286330828'
            ],
            [
                'Šifra' => '990331',
                'Tip' => 'HGW',
                'Model' => 'EPC3928S V2.5 GaTeWaY',
                'S/N' => '330753711'
            ],
            [
                'Šifra' => '990331',
                'Tip' => 'HGW',
                'Model' => 'EPC3928S V2.5 GaTeWaY',
                'S/N' => '285589394'
            ],
            [
                'Šifra' => '990331',
                'Tip' => 'HGW',
                'Model' => 'EPC3928S V2.5 GaTeWaY',
                'S/N' => '287184220'
            ],
            [
                'Šifra' => '990331',
                'Tip' => 'HGW',
                'Model' => 'EPC3928S V2.5 GaTeWaY',
                'S/N' => '286304501'
            ],
            [
                'Šifra' => '990331',
                'Tip' => 'HGW',
                'Model' => 'EPC3928S V2.5 GaTeWaY',
                'S/N' => '330055126'
            ],
            [
                'Šifra' => '990331',
                'Tip' => 'HGW',
                'Model' => 'EPC3928S V2.5 GaTeWaY',
                'S/N' => '285568794'
            ],
            [
                'Šifra' => '990908',
                'Tip' => 'GPON ONT ADVANCE',
                'Model' => 'MOCA854G-2 GigaCenter 2Pots4GE',
                'S/N' => '262003036408'
            ],
            [
                'Šifra' => '990908',
                'Tip' => 'GPON ONT ADVANCE',
                'Model' => 'MOCA854G-2 GigaCenter 2Pots4GE',
                'S/N' => '262003036542'
            ],
            [
                'Šifra' => '990908',
                'Tip' => 'GPON ONT ADVANCE',
                'Model' => 'MOCA854G-2 GigaCenter 2Pots4GE',
                'S/N' => '262003036355'
            ],
            [
                'Šifra' => '990908',
                'Tip' => 'GPON ONT ADVANCE',
                'Model' => 'MOCA854G-2 GigaCenter 2Pots4GE',
                'S/N' => '262003036578'
            ],
            [
                'Šifra' => '990908',
                'Tip' => 'GPON ONT ADVANCE',
                'Model' => 'MOCA854G-2 GigaCenter 2Pots4GE',
                'S/N' => '262003036492'
            ],
            [
                'Šifra' => '990908',
                'Tip' => 'GPON ONT ADVANCE',
                'Model' => 'MOCA854G-2 GigaCenter 2Pots4GE',
                'S/N' => '262003035882'
            ],
            [
                'Šifra' => '990908',
                'Tip' => 'GPON ONT ADVANCE',
                'Model' => 'MOCA854G-2 GigaCenter 2Pots4GE',
                'S/N' => '262003036320'
            ],
            [
                'Šifra' => '990908',
                'Tip' => 'GPON ONT ADVANCE',
                'Model' => 'MOCA854G-2 GigaCenter 2Pots4GE',
                'S/N' => '262003035938'
            ],
            [
                'Šifra' => '990908',
                'Tip' => 'GPON ONT ADVANCE',
                'Model' => 'MOCA854G-2 GigaCenter 2Pots4GE',
                'S/N' => '262003035897'
            ],
            [
                'Šifra' => '990908',
                'Tip' => 'GPON ONT ADVANCE',
                'Model' => 'MOCA854G-2 GigaCenter 2Pots4GE',
                'S/N' => '262003035943'
            ],
            [
                'Šifra' => '990331',
                'Tip' => 'HGW',
                'Model' => 'EPC3928S V2.5 GaTeWaY',
                'S/N' => '286292930'
            ],
            [
                'Šifra' => '990331',
                'Tip' => 'HGW',
                'Model' => 'EPC3928S V2.5 GaTeWaY',
                'S/N' => '281983133'
            ],
            [
                'Šifra' => '990331',
                'Tip' => 'HGW',
                'Model' => 'EPC3928S V2.5 GaTeWaY',
                'S/N' => '286331323'
            ],
            [
                'Šifra' => '990331',
                'Tip' => 'HGW',
                'Model' => 'EPC3928S V2.5 GaTeWaY',
                'S/N' => '282846461'
            ],
            [
                'Šifra' => '990331',
                'Tip' => 'HGW',
                'Model' => 'EPC3928S V2.5 GaTeWaY',
                'S/N' => '278927869'
            ],
            [
                'Šifra' => '990331',
                'Tip' => 'HGW',
                'Model' => 'EPC3928S V2.5 GaTeWaY',
                'S/N' => '330755172'
            ],
            [
                'Šifra' => '990331',
                'Tip' => 'HGW',
                'Model' => 'EPC3928S V2.5 GaTeWaY',
                'S/N' => '289808019'
            ],
            [
                'Šifra' => '990331',
                'Tip' => 'HGW',
                'Model' => 'EPC3928S V2.5 GaTeWaY',
                'S/N' => '281233588'
            ],
            [
                'Šifra' => '990331',
                'Tip' => 'HGW',
                'Model' => 'EPC3928S V2.5 GaTeWaY',
                'S/N' => '282354204'
            ],
            [
                'Šifra' => '990331',
                'Tip' => 'HGW',
                'Model' => 'EPC3928S V2.5 GaTeWaY',
                'S/N' => '286292903'
            ],
            [
                'Šifra' => '990331',
                'Tip' => 'HGW',
                'Model' => 'EPC3928S V2.5 GaTeWaY',
                'S/N' => '330278732'
            ],
            [
                'Šifra' => '990331',
                'Tip' => 'HGW',
                'Model' => 'EPC3928S V2.5 GaTeWaY',
                'S/N' => '282367641'
            ],
            [
                'Šifra' => '990331',
                'Tip' => 'HGW',
                'Model' => 'EPC3928S V2.5 GaTeWaY',
                'S/N' => '280495981'
            ],
            [
                'Šifra' => '990331',
                'Tip' => 'HGW',
                'Model' => 'EPC3928S V2.5 GaTeWaY',
                'S/N' => '331013065'
            ],
            [
                'Šifra' => '990331',
                'Tip' => 'HGW',
                'Model' => 'EPC3928S V2.5 GaTeWaY',
                'S/N' => '330055520'
            ],
            [
                'Šifra' => '990331',
                'Tip' => 'HGW',
                'Model' => 'EPC3928S V2.5 GaTeWaY',
                'S/N' => '278913067'
            ],
            [
                'Šifra' => '990331',
                'Tip' => 'HGW',
                'Model' => 'EPC3928S V2.5 GaTeWaY',
                'S/N' => '286167460'
            ],
            [
                'Šifra' => '990331',
                'Tip' => 'HGW',
                'Model' => 'EPC3928S V2.5 GaTeWaY',
                'S/N' => '330534016'
            ],
            [
                'Šifra' => '990331',
                'Tip' => 'HGW',
                'Model' => 'EPC3928S V2.5 GaTeWaY',
                'S/N' => '331012866'
            ],
            [
                'Šifra' => '990331',
                'Tip' => 'HGW',
                'Model' => 'EPC3928S V2.5 GaTeWaY',
                'S/N' => '280169924'
            ],
            [
                'Šifra' => '990331',
                'Tip' => 'HGW',
                'Model' => 'EPC3928S V2.5 GaTeWaY',
                'S/N' => '289795570'
            ],
            [
                'Šifra' => '990331',
                'Tip' => 'HGW',
                'Model' => 'EPC3928S V2.5 GaTeWaY',
                'S/N' => '286445971'
            ],
            [
                'Šifra' => '990331',
                'Tip' => 'HGW',
                'Model' => 'EPC3928S V2.5 GaTeWaY',
                'S/N' => '330294751'
            ],
            [
                'Šifra' => '990331',
                'Tip' => 'HGW',
                'Model' => 'EPC3928S V2.5 GaTeWaY',
                'S/N' => '330286457'
            ],
            [
                'Šifra' => '990331',
                'Tip' => 'HGW',
                'Model' => 'EPC3928S V2.5 GaTeWaY',
                'S/N' => '278930447'
            ],
            [
                'Šifra' => '990331',
                'Tip' => 'HGW',
                'Model' => 'EPC3928S V2.5 GaTeWaY',
                'S/N' => '283472113'
            ],
            [
                'Šifra' => '990331',
                'Tip' => 'HGW',
                'Model' => 'EPC3928S V2.5 GaTeWaY',
                'S/N' => '286174967'
            ],
            [
                'Šifra' => '990331',
                'Tip' => 'HGW',
                'Model' => 'EPC3928S V2.5 GaTeWaY',
                'S/N' => '278949642'
            ],
            [
                'Šifra' => '990331',
                'Tip' => 'HGW',
                'Model' => 'EPC3928S V2.5 GaTeWaY',
                'S/N' => '287180178'
            ],
            [
                'Šifra' => '990331',
                'Tip' => 'HGW',
                'Model' => 'EPC3928S V2.5 GaTeWaY',
                'S/N' => '280483498'
            ],
            [
                'Šifra' => '990331',
                'Tip' => 'HGW',
                'Model' => 'EPC3928S V2.5 GaTeWaY',
                'S/N' => '281984432'
            ],
            [
                'Šifra' => '990331',
                'Tip' => 'HGW',
                'Model' => 'EPC3928S V2.5 GaTeWaY',
                'S/N' => '286451865'
            ],
            [
                'Šifra' => '990331',
                'Tip' => 'HGW',
                'Model' => 'EPC3928S V2.5 GaTeWaY',
                'S/N' => '286331079'
            ],
            [
                'Šifra' => '990331',
                'Tip' => 'HGW',
                'Model' => 'EPC3928S V2.5 GaTeWaY',
                'S/N' => '278949115'
            ],
            [
                'Šifra' => '990331',
                'Tip' => 'HGW',
                'Model' => 'EPC3928S V2.5 GaTeWaY',
                'S/N' => '330286279'
            ],
            [
                'Šifra' => '990331',
                'Tip' => 'HGW',
                'Model' => 'EPC3928S V2.5 GaTeWaY',
                'S/N' => '278766296'
            ],
            [
                'Šifra' => '990331',
                'Tip' => 'HGW',
                'Model' => 'EPC3928S V2.5 GaTeWaY',
                'S/N' => '278928184'
            ],
            [
                'Šifra' => '990331',
                'Tip' => 'HGW',
                'Model' => 'EPC3928S V2.5 GaTeWaY',
                'S/N' => '280185620'
            ],
            [
                'Šifra' => '990331',
                'Tip' => 'HGW',
                'Model' => 'EPC3928S V2.5 GaTeWaY',
                'S/N' => '330053973'
            ],
            [
                'Šifra' => '990331',
                'Tip' => 'HGW',
                'Model' => 'EPC3928S V2.5 GaTeWaY',
                'S/N' => '280499944'
            ],
            [
                'Šifra' => '990331',
                'Tip' => 'HGW',
                'Model' => 'EPC3928S V2.5 GaTeWaY',
                'S/N' => '280193837'
            ],
            [
                'Šifra' => '990331',
                'Tip' => 'HGW',
                'Model' => 'EPC3928S V2.5 GaTeWaY',
                'S/N' => '286819941'
            ],
            [
                'Šifra' => '990331',
                'Tip' => 'HGW',
                'Model' => 'EPC3928S V2.5 GaTeWaY',
                'S/N' => '280837835'
            ],
            [
                'Šifra' => '990331',
                'Tip' => 'HGW',
                'Model' => 'EPC3928S V2.5 GaTeWaY',
                'S/N' => '286324343'
            ],
            [
                'Šifra' => '990331',
                'Tip' => 'HGW',
                'Model' => 'EPC3928S V2.5 GaTeWaY',
                'S/N' => '285572683'
            ],
            [
                'Šifra' => '990331',
                'Tip' => 'HGW',
                'Model' => 'EPC3928S V2.5 GaTeWaY',
                'S/N' => '330053823'
            ],
            [
                'Šifra' => '990331',
                'Tip' => 'HGW',
                'Model' => 'EPC3928S V2.5 GaTeWaY',
                'S/N' => '280483005'
            ],
            [
                'Šifra' => '990331',
                'Tip' => 'HGW',
                'Model' => 'EPC3928S V2.5 GaTeWaY',
                'S/N' => '286181878'
            ],
            [
                'Šifra' => '990331',
                'Tip' => 'HGW',
                'Model' => 'EPC3928S V2.5 GaTeWaY',
                'S/N' => '280185618'
            ],
            [
                'Šifra' => '990331',
                'Tip' => 'HGW',
                'Model' => 'EPC3928S V2.5 GaTeWaY',
                'S/N' => '281994765'
            ],
            [
                'Šifra' => '990140',
                'Tip' => 'D3  1 way receiver',
                'Model' => 'STB Cisco 4682 DVB dual tuner',
                'S/N' => 'SACCMWXQR'
            ],
            [
                'Šifra' => '990140',
                'Tip' => 'D3  1 way receiver',
                'Model' => 'STB Cisco 4682 DVB dual tuner',
                'S/N' => 'SACCGMQSC'
            ],
            [
                'Šifra' => '990140',
                'Tip' => 'D3  1 way receiver',
                'Model' => 'STB Cisco 4682 DVB dual tuner',
                'S/N' => 'SACDNVQRH'
            ],
            [
                'Šifra' => '990140',
                'Tip' => 'D3  1 way receiver',
                'Model' => 'STB Cisco 4682 DVB dual tuner',
                'S/N' => 'SACFBBHCS'
            ],
            [
                'Šifra' => '990140',
                'Tip' => 'D3  1 way receiver',
                'Model' => 'STB Cisco 4682 DVB dual tuner',
                'S/N' => 'SACBRVRGB'
            ],
            [
                'Šifra' => '990140',
                'Tip' => 'D3  1 way receiver',
                'Model' => 'STB Cisco 4682 DVB dual tuner',
                'S/N' => 'SACFNGSXM'
            ],
            [
                'Šifra' => '990140',
                'Tip' => 'D3  1 way receiver',
                'Model' => 'STB Cisco 4682 DVB dual tuner',
                'S/N' => 'SACDBZKRF'
            ],
            [
                'Šifra' => '990140',
                'Tip' => 'D3  1 way receiver',
                'Model' => 'STB Cisco 4682 DVB dual tuner',
                'S/N' => 'SACFXWRDX'
            ],
            [
                'Šifra' => '990140',
                'Tip' => 'D3  1 way receiver',
                'Model' => 'STB Cisco 4682 DVB dual tuner',
                'S/N' => 'SACGSLBQN'
            ],
            [
                'Šifra' => '990140',
                'Tip' => 'D3  1 way receiver',
                'Model' => 'STB Cisco 4682 DVB dual tuner',
                'S/N' => 'SACFBBMWQ'
            ],
            [
                'Šifra' => '990140',
                'Tip' => 'D3  1 way receiver',
                'Model' => 'STB Cisco 4682 DVB dual tuner',
                'S/N' => 'SACFWVHLV'
            ],
            [
                'Šifra' => '990140',
                'Tip' => 'D3  1 way receiver',
                'Model' => 'STB Cisco 4682 DVB dual tuner',
                'S/N' => 'SACCMWGQW'
            ],
            [
                'Šifra' => '990140',
                'Tip' => 'D3  1 way receiver',
                'Model' => 'STB Cisco 4682 DVB dual tuner',
                'S/N' => 'SACFXHLZD'
            ],
            [
                'Šifra' => '990134',
                'Tip' => 'D3  2 way receiver',
                'Model' => 'STB PDS 3121 3k set top box',
                'S/N' => 'SACJDWSKN'
            ],
            [
                'Šifra' => '990134',
                'Tip' => 'D3  2 way receiver',
                'Model' => 'STB PDS 3121 3k set top box',
                'S/N' => 'SACJDWFJP'
            ],
            [
                'Šifra' => '990134',
                'Tip' => 'D3  2 way receiver',
                'Model' => 'STB PDS 3121 3k set top box',
                'S/N' => 'SACJDWMKV'
            ],
            [
                'Šifra' => '990134',
                'Tip' => 'D3  2 way receiver',
                'Model' => 'STB PDS 3121 3k set top box',
                'S/N' => '125228352'
            ],
            [
                'Šifra' => '990134',
                'Tip' => 'D3  2 way receiver',
                'Model' => 'STB PDS 3121 3k set top box',
                'S/N' => 'SACKVLFCF'
            ],
            [
                'Šifra' => '990134',
                'Tip' => 'D3  2 way receiver',
                'Model' => 'STB PDS 3121 3k set top box',
                'S/N' => 'SACKVKJYG'
            ],
            [
                'Šifra' => '990134',
                'Tip' => 'D3  2 way receiver',
                'Model' => 'STB PDS 3121 3k set top box',
                'S/N' => 'SACKVKHZB'
            ],
            [
                'Šifra' => '990134',
                'Tip' => 'D3  2 way receiver',
                'Model' => 'STB PDS 3121 3k set top box',
                'S/N' => 'SACKXLQFZ'
            ],
            [
                'Šifra' => '990134',
                'Tip' => 'D3  2 way receiver',
                'Model' => 'STB PDS 3121 3k set top box',
                'S/N' => 'SACJZFTCL'
            ],
            [
                'Šifra' => '990134',
                'Tip' => 'D3  2 way receiver',
                'Model' => 'STB PDS 3121 3k set top box',
                'S/N' => 'SACHTZPVW'
            ],
            [
                'Šifra' => '990134',
                'Tip' => 'D3  2 way receiver',
                'Model' => 'STB PDS 3121 3k set top box',
                'S/N' => 'SACKVNGDF'
            ],
            [
                'Šifra' => '990134',
                'Tip' => 'D3  2 way receiver',
                'Model' => 'STB PDS 3121 3k set top box',
                'S/N' => '125249619'
            ],
            [
                'Šifra' => '990134',
                'Tip' => 'D3  2 way receiver',
                'Model' => 'STB PDS 3121 3k set top box',
                'S/N' => '125188684'
            ],
            [
                'Šifra' => '990134',
                'Tip' => 'D3  2 way receiver',
                'Model' => 'STB PDS 3121 3k set top box',
                'S/N' => 'SACHLFTDR'
            ],
            [
                'Šifra' => '990134',
                'Tip' => 'D3  2 way receiver',
                'Model' => 'STB PDS 3121 3k set top box',
                'S/N' => 'SACKHQVKN'
            ],
            [
                'Šifra' => '990134',
                'Tip' => 'D3  2 way receiver',
                'Model' => 'STB PDS 3121 3k set top box',
                'S/N' => 'SACKCDMLV'
            ],
            [
                'Šifra' => '990134',
                'Tip' => 'D3  2 way receiver',
                'Model' => 'STB PDS 3121 3k set top box',
                'S/N' => 'SACKCDZDJ'
            ],
            [
                'Šifra' => '990134',
                'Tip' => 'D3  2 way receiver',
                'Model' => 'STB PDS 3121 3k set top box',
                'S/N' => '125264785'
            ],
            [
                'Šifra' => '990134',
                'Tip' => 'D3  2 way receiver',
                'Model' => 'STB PDS 3121 3k set top box',
                'S/N' => '125273882'
            ],
            [
                'Šifra' => '990134',
                'Tip' => 'D3  2 way receiver',
                'Model' => 'STB PDS 3121 3k set top box',
                'S/N' => '125201968'
            ],
            [
                'Šifra' => '990134',
                'Tip' => 'D3  2 way receiver',
                'Model' => 'STB PDS 3121 3k set top box',
                'S/N' => 'SACKTQGNV'
            ],
            [
                'Šifra' => '990134',
                'Tip' => 'D3  2 way receiver',
                'Model' => 'STB PDS 3121 3k set top box',
                'S/N' => '125222595'
            ],
            [
                'Šifra' => '990134',
                'Tip' => 'D3  2 way receiver',
                'Model' => 'STB PDS 3121 3k set top box',
                'S/N' => 'SACHQXLPM'
            ],
            [
                'Šifra' => '990134',
                'Tip' => 'D3  2 way receiver',
                'Model' => 'STB PDS 3121 3k set top box',
                'S/N' => '125274074'
            ],
            [
                'Šifra' => '990134',
                'Tip' => 'D3  2 way receiver',
                'Model' => 'STB PDS 3121 3k set top box',
                'S/N' => '125228411'
            ],
            [
                'Šifra' => '990134',
                'Tip' => 'D3  2 way receiver',
                'Model' => 'STB PDS 3121 3k set top box',
                'S/N' => '125262093'
            ],
            [
                'Šifra' => '990134',
                'Tip' => 'D3  2 way receiver',
                'Model' => 'STB PDS 3121 3k set top box',
                'S/N' => 'SACKTFJHM'
            ],
            [
                'Šifra' => '990134',
                'Tip' => 'D3  2 way receiver',
                'Model' => 'STB PDS 3121 3k set top box',
                'S/N' => 'SACKTDNKT'
            ],
            [
                'Šifra' => '990134',
                'Tip' => 'D3  2 way receiver',
                'Model' => 'STB PDS 3121 3k set top box',
                'S/N' => 'SACJLNZDV'
            ],
            [
                'Šifra' => '990134',
                'Tip' => 'D3  2 way receiver',
                'Model' => 'STB PDS 3121 3k set top box',
                'S/N' => 'SACJMXBVX'
            ],
            [
                'Šifra' => '990134',
                'Tip' => 'D3  2 way receiver',
                'Model' => 'STB PDS 3121 3k set top box',
                'S/N' => 'SACHLDHJS'
            ],
            [
                'Šifra' => '990134',
                'Tip' => 'D3  2 way receiver',
                'Model' => 'STB PDS 3121 3k set top box',
                'S/N' => 'SACKXLGHP'
            ],
            [
                'Šifra' => '990134',
                'Tip' => 'D3  2 way receiver',
                'Model' => 'STB PDS 3121 3k set top box',
                'S/N' => 'SACKXLQJV'
            ],
            [
                'Šifra' => '990134',
                'Tip' => 'D3  2 way receiver',
                'Model' => 'STB PDS 3121 3k set top box',
                'S/N' => 'SACKXLGBZ'
            ],
            [
                'Šifra' => '990134',
                'Tip' => 'D3  2 way receiver',
                'Model' => 'STB PDS 3121 3k set top box',
                'S/N' => 'SACKXLQFX'
            ],
            [
                'Šifra' => '990134',
                'Tip' => 'D3  2 way receiver',
                'Model' => 'STB PDS 3121 3k set top box',
                'S/N' => 'SACKXLQJL'
            ],
            [
                'Šifra' => '990134',
                'Tip' => 'D3  2 way receiver',
                'Model' => 'STB PDS 3121 3k set top box',
                'S/N' => 'SACKTQJQD'
            ],
            [
                'Šifra' => '990134',
                'Tip' => 'D3  2 way receiver',
                'Model' => 'STB PDS 3121 3k set top box',
                'S/N' => 'SACKTDSCM'
            ],
            [
                'Šifra' => '990134',
                'Tip' => 'D3  2 way receiver',
                'Model' => 'STB PDS 3121 3k set top box',
                'S/N' => 'SACKTDSWJ'
            ],
            [
                'Šifra' => '990134',
                'Tip' => 'D3  2 way receiver',
                'Model' => 'STB PDS 3121 3k set top box',
                'S/N' => 'SACKTDTGK'
            ],
            [
                'Šifra' => '990134',
                'Tip' => 'D3  2 way receiver',
                'Model' => 'STB PDS 3121 3k set top box',
                'S/N' => 'SACKXLQNW'
            ],
            [
                'Šifra' => '990134',
                'Tip' => 'D3  2 way receiver',
                'Model' => 'STB PDS 3121 3k set top box',
                'S/N' => 'SACKXLPQB'
            ],
            [
                'Šifra' => '990134',
                'Tip' => 'D3  2 way receiver',
                'Model' => 'STB PDS 3121 3k set top box',
                'S/N' => 'SACKXLQPN'
            ],
            [
                'Šifra' => '990134',
                'Tip' => 'D3  2 way receiver',
                'Model' => 'STB PDS 3121 3k set top box',
                'S/N' => 'SACKXLQMM'
            ],
            [
                'Šifra' => '990134',
                'Tip' => 'D3  2 way receiver',
                'Model' => 'STB PDS 3121 3k set top box',
                'S/N' => 'SACKXLQPQ'
            ],
            [
                'Šifra' => '990134',
                'Tip' => 'D3  2 way receiver',
                'Model' => 'STB PDS 3121 3k set top box',
                'S/N' => 'SACKTQJLD'
            ],
            [
                'Šifra' => '990134',
                'Tip' => 'D3  2 way receiver',
                'Model' => 'STB PDS 3121 3k set top box',
                'S/N' => 'SACKVKKNC'
            ],
            [
                'Šifra' => '990134',
                'Tip' => 'D3  2 way receiver',
                'Model' => 'STB PDS 3121 3k set top box',
                'S/N' => 'SACKXLQMW'
            ],
            [
                'Šifra' => '990134',
                'Tip' => 'D3  2 way receiver',
                'Model' => 'STB PDS 3121 3k set top box',
                'S/N' => 'SACKXLQLX'
            ],
            [
                'Šifra' => '990134',
                'Tip' => 'D3  2 way receiver',
                'Model' => 'STB PDS 3121 3k set top box',
                'S/N' => 'SACKXLQNB'
            ],
            [
                'Šifra' => '990134',
                'Tip' => 'D3  2 way receiver',
                'Model' => 'STB PDS 3121 3k set top box',
                'S/N' => 'SACKXLPWD'
            ],
            [
                'Šifra' => '990134',
                'Tip' => 'D3  2 way receiver',
                'Model' => 'STB PDS 3121 3k set top box',
                'S/N' => 'SACKTDSXJ'
            ],
            [
                'Šifra' => '990134',
                'Tip' => 'D3  2 way receiver',
                'Model' => 'STB PDS 3121 3k set top box',
                'S/N' => 'SACKTDTKK'
            ],
            [
                'Šifra' => '990134',
                'Tip' => 'D3  2 way receiver',
                'Model' => 'STB PDS 3121 3k set top box',
                'S/N' => '125194490'
            ],
            [
                'Šifra' => '990134',
                'Tip' => 'D3  2 way receiver',
                'Model' => 'STB PDS 3121 3k set top box',
                'S/N' => 'SACKTDTJN'
            ],
            [
                'Šifra' => '990134',
                'Tip' => 'D3  2 way receiver',
                'Model' => 'STB PDS 3121 3k set top box',
                'S/N' => 'SACKTQJFJ'
            ],
            [
                'Šifra' => '990134',
                'Tip' => 'D3  2 way receiver',
                'Model' => 'STB PDS 3121 3k set top box',
                'S/N' => 'SACKVKHXJ'
            ],
            [
                'Šifra' => '990134',
                'Tip' => 'D3  2 way receiver',
                'Model' => 'STB PDS 3121 3k set top box',
                'S/N' => 'SACKVKKPT'
            ],
            [
                'Šifra' => '990020',
                'Tip' => 'WiFi',
                'Model' => 'Wireless Mesh Extender Air4920',
                'S/N' => 'AW2621934001363'
            ],
            [
                'Šifra' => '990020',
                'Tip' => 'WiFi',
                'Model' => 'Wireless Mesh Extender Air4920',
                'S/N' => 'AW2621934001364'
            ],
            [
                'Šifra' => '990020',
                'Tip' => 'WiFi',
                'Model' => 'Wireless Mesh Extender Air4920',
                'S/N' => 'AW2621934000503'
            ],
            [
                'Šifra' => '990020',
                'Tip' => 'WiFi',
                'Model' => 'Wireless Mesh Extender Air4920',
                'S/N' => 'AW2621934000504'
            ],
            [
                'Šifra' => '990020',
                'Tip' => 'WiFi',
                'Model' => 'Wireless Mesh Extender Air4920',
                'S/N' => 'AW2621934001337'
            ],
            [
                'Šifra' => '990020',
                'Tip' => 'WiFi',
                'Model' => 'Wireless Mesh Extender Air4920',
                'S/N' => 'AW2621934001338'
            ],
            [
                'Šifra' => '990020',
                'Tip' => 'WiFi',
                'Model' => 'Wireless Mesh Extender Air4920',
                'S/N' => 'AW2621934001855'
            ],
            [
                'Šifra' => '990020',
                'Tip' => 'WiFi',
                'Model' => 'Wireless Mesh Extender Air4920',
                'S/N' => 'AW2621934001856'
            ],
            [
                'Šifra' => '990020',
                'Tip' => 'WiFi',
                'Model' => 'Wireless Mesh Extender Air4920',
                'S/N' => 'AW2621934001402'
            ],
            [
                'Šifra' => '990020',
                'Tip' => 'WiFi',
                'Model' => 'Wireless Mesh Extender Air4920',
                'S/N' => 'AW2621934001401'
            ],
            [
                'Šifra' => '990020',
                'Tip' => 'WiFi',
                'Model' => 'Wireless Mesh Extender Air4920',
                'S/N' => 'AW2621934001303'
            ],
            [
                'Šifra' => '990020',
                'Tip' => 'WiFi',
                'Model' => 'Wireless Mesh Extender Air4920',
                'S/N' => 'AW2621934001304'
            ],
            [
                'Šifra' => '990020',
                'Tip' => 'WiFi',
                'Model' => 'Wireless Mesh Extender Air4920',
                'S/N' => 'AW2621934001301'
            ],
            [
                'Šifra' => '990020',
                'Tip' => 'WiFi',
                'Model' => 'Wireless Mesh Extender Air4920',
                'S/N' => 'AW2621934001302'
            ],
            [
                'Šifra' => '990020',
                'Tip' => 'WiFi',
                'Model' => 'Wireless Mesh Extender Air4920',
                'S/N' => 'AW2621934001257'
            ],
            [
                'Šifra' => '990020',
                'Tip' => 'WiFi',
                'Model' => 'Wireless Mesh Extender Air4920',
                'S/N' => 'AW2621934001258'
            ],
            [
                'Šifra' => '990020',
                'Tip' => 'WiFi',
                'Model' => 'Wireless Mesh Extender Air4920',
                'S/N' => 'AW2621934001711'
            ],
            [
                'Šifra' => '990020',
                'Tip' => 'WiFi',
                'Model' => 'Wireless Mesh Extender Air4920',
                'S/N' => 'AW2621934001712'
            ],
            [
                'Šifra' => '990020',
                'Tip' => 'WiFi',
                'Model' => 'Wireless Mesh Extender Air4920',
                'S/N' => 'AW2621934001599'
            ],
            [
                'Šifra' => '990020',
                'Tip' => 'WiFi',
                'Model' => 'Wireless Mesh Extender Air4920',
                'S/N' => 'AW2621934001600'
            ],
            [
                'Šifra' => '990020',
                'Tip' => 'WiFi',
                'Model' => 'Wireless Mesh Extender Air4920',
                'S/N' => 'AW2621934000817'
            ],
            [
                'Šifra' => '990020',
                'Tip' => 'WiFi',
                'Model' => 'Wireless Mesh Extender Air4920',
                'S/N' => 'AW2621934000818'
            ],
            [
                'Šifra' => '990020',
                'Tip' => 'WiFi',
                'Model' => 'Wireless Mesh Extender Air4920',
                'S/N' => 'AW2621934001355'
            ],
            [
                'Šifra' => '990020',
                'Tip' => 'WiFi',
                'Model' => 'Wireless Mesh Extender Air4920',
                'S/N' => 'AW2621934001356'
            ],
            [
                'Šifra' => '990020',
                'Tip' => 'WiFi',
                'Model' => 'Wireless Mesh Extender Air4920',
                'S/N' => 'AW2621934001181'
            ],
            [
                'Šifra' => '990020',
                'Tip' => 'WiFi',
                'Model' => 'Wireless Mesh Extender Air4920',
                'S/N' => 'AW2621934001182'
            ],
            [
                'Šifra' => '990020',
                'Tip' => 'WiFi',
                'Model' => 'Wireless Mesh Extender Air4920',
                'S/N' => 'AW2621934001350'
            ],
            [
                'Šifra' => '990020',
                'Tip' => 'WiFi',
                'Model' => 'Wireless Mesh Extender Air4920',
                'S/N' => 'AW2621934001349'
            ],
            [
                'Šifra' => '990020',
                'Tip' => 'WiFi',
                'Model' => 'Wireless Mesh Extender Air4920',
                'S/N' => 'AW2621934000831'
            ],
            [
                'Šifra' => '990020',
                'Tip' => 'WiFi',
                'Model' => 'Wireless Mesh Extender Air4920',
                'S/N' => 'AW2621934000832'
            ],
            [
                'Šifra' => '990020',
                'Tip' => 'WiFi',
                'Model' => 'Wireless Mesh Extender Air4920',
                'S/N' => 'AW2621934000465'
            ],
            [
                'Šifra' => '990020',
                'Tip' => 'WiFi',
                'Model' => 'Wireless Mesh Extender Air4920',
                'S/N' => 'AW2621934000466'
            ],
            [
                'Šifra' => '990020',
                'Tip' => 'WiFi',
                'Model' => 'Wireless Mesh Extender Air4920',
                'S/N' => 'AW2621934001321'
            ],
            [
                'Šifra' => '990020',
                'Tip' => 'WiFi',
                'Model' => 'Wireless Mesh Extender Air4920',
                'S/N' => 'AW2621934001322'
            ],
            [
                'Šifra' => '990020',
                'Tip' => 'WiFi',
                'Model' => 'Wireless Mesh Extender Air4920',
                'S/N' => 'AW2621934001327'
            ],
            [
                'Šifra' => '990020',
                'Tip' => 'WiFi',
                'Model' => 'Wireless Mesh Extender Air4920',
                'S/N' => 'AW2621934001328'
            ],
            [
                'Šifra' => '990020',
                'Tip' => 'WiFi',
                'Model' => 'Wireless Mesh Extender Air4920',
                'S/N' => 'AW2621934001325'
            ],
            [
                'Šifra' => '990020',
                'Tip' => 'WiFi',
                'Model' => 'Wireless Mesh Extender Air4920',
                'S/N' => 'AW2621934001326'
            ],
            [
                'Šifra' => '990141',
                'Tip' => 'HGW',
                'Model' => 'EPC3208 EU Docsis3.0 EMTA',
                'S/N' => '261176013'
            ],
            [
                'Šifra' => '990141',
                'Tip' => 'HGW',
                'Model' => 'EPC3208 EU Docsis3.0 EMTA',
                'S/N' => '249282580'
            ],
            [
                'Šifra' => '990141',
                'Tip' => 'HGW',
                'Model' => 'EPC3208 EU Docsis3.0 EMTA',
                'S/N' => '243638883'
            ],
            [
                'Šifra' => '990141',
                'Tip' => 'HGW',
                'Model' => 'EPC3208 EU Docsis3.0 EMTA',
                'S/N' => '248142983'
            ],
            [
                'Šifra' => '990141',
                'Tip' => 'HGW',
                'Model' => 'EPC3208 EU Docsis3.0 EMTA',
                'S/N' => '246802142'
            ],
            [
                'Šifra' => '990950',
                'Tip' => 'Beogrid modem',
                'Model' => 'HGW VDSL2 POTS Innbox V45',
                'S/N' => '2915500355'
            ],
            [
                'Šifra' => '990950',
                'Tip' => 'Beogrid modem',
                'Model' => 'HGW VDSL2 POTS Innbox V45',
                'S/N' => '2817503908'
            ],
            [
                'Šifra' => '990950',
                'Tip' => 'Beogrid modem',
                'Model' => 'HGW VDSL2 POTS Innbox V45',
                'S/N' => '2822503498'
            ],
            [
                'Šifra' => '990950',
                'Tip' => 'Beogrid modem',
                'Model' => 'HGW VDSL2 POTS Innbox V45',
                'S/N' => '2820500346'
            ],
            [
                'Šifra' => '990950',
                'Tip' => 'Beogrid modem',
                'Model' => 'HGW VDSL2 POTS Innbox V45',
                'S/N' => '2821502510'
            ],
            [
                'Šifra' => '990950',
                'Tip' => 'Beogrid modem',
                'Model' => 'HGW VDSL2 POTS Innbox V45',
                'S/N' => '2821502708'
            ],
            [
                'Šifra' => '990950',
                'Tip' => 'Beogrid modem',
                'Model' => 'HGW VDSL2 POTS Innbox V45',
                'S/N' => '2820500406'
            ],
            [
                'Šifra' => '990950',
                'Tip' => 'Beogrid modem',
                'Model' => 'HGW VDSL2 POTS Innbox V45',
                'S/N' => '2820500522'
            ],
            [
                'Šifra' => '990950',
                'Tip' => 'Beogrid modem',
                'Model' => 'HGW VDSL2 POTS Innbox V45',
                'S/N' => '2815502552'
            ],
            [
                'Šifra' => '990950',
                'Tip' => 'Beogrid modem',
                'Model' => 'HGW VDSL2 POTS Innbox V45',
                'S/N' => '2820500645'
            ],
            [
                'Šifra' => '990950',
                'Tip' => 'Beogrid modem',
                'Model' => 'HGW VDSL2 POTS Innbox V45',
                'S/N' => '2820500724'
            ],
            [
                'Šifra' => '990950',
                'Tip' => 'Beogrid modem',
                'Model' => 'HGW VDSL2 POTS Innbox V45',
                'S/N' => '2820500105'
            ],
            [
                'Šifra' => '990950',
                'Tip' => 'Beogrid modem',
                'Model' => 'HGW VDSL2 POTS Innbox V45',
                'S/N' => '2821502607'
            ],
            [
                'Šifra' => '990950',
                'Tip' => 'Beogrid modem',
                'Model' => 'HGW VDSL2 POTS Innbox V45',
                'S/N' => '2820500418'
            ],
            [
                'Šifra' => '990950',
                'Tip' => 'Beogrid modem',
                'Model' => 'HGW VDSL2 POTS Innbox V45',
                'S/N' => '2820500331'
            ],
            [
                'Šifra' => '990950',
                'Tip' => 'Beogrid modem',
                'Model' => 'HGW VDSL2 POTS Innbox V45',
                'S/N' => '2820500622'
            ],
            [
                'Šifra' => '990950',
                'Tip' => 'Beogrid modem',
                'Model' => 'HGW VDSL2 POTS Innbox V45',
                'S/N' => '2821502445'
            ],
            [
                'Šifra' => '990950',
                'Tip' => 'Beogrid modem',
                'Model' => 'HGW VDSL2 POTS Innbox V45',
                'S/N' => '2815502553'
            ],
            [
                'Šifra' => '990950',
                'Tip' => 'Beogrid modem',
                'Model' => 'HGW VDSL2 POTS Innbox V45',
                'S/N' => '2815502570'
            ],
            [
                'Šifra' => '990950',
                'Tip' => 'Beogrid modem',
                'Model' => 'HGW VDSL2 POTS Innbox V45',
                'S/N' => '2820501681'
            ],
            [
                'Šifra' => '990950',
                'Tip' => 'Beogrid modem',
                'Model' => 'HGW VDSL2 POTS Innbox V45',
                'S/N' => '2821502616'
            ],
            [
                'Šifra' => '990950',
                'Tip' => 'Beogrid modem',
                'Model' => 'HGW VDSL2 POTS Innbox V45',
                'S/N' => '2820500393'
            ],
            [
                'Šifra' => '990950',
                'Tip' => 'Beogrid modem',
                'Model' => 'HGW VDSL2 POTS Innbox V45',
                'S/N' => '2821502256'
            ],
            [
                'Šifra' => '990950',
                'Tip' => 'Beogrid modem',
                'Model' => 'HGW VDSL2 POTS Innbox V45',
                'S/N' => '2815502572'
            ],
            [
                'Šifra' => '990950',
                'Tip' => 'Beogrid modem',
                'Model' => 'HGW VDSL2 POTS Innbox V45',
                'S/N' => '2815502501'
            ],
            [
                'Šifra' => '990950',
                'Tip' => 'Beogrid modem',
                'Model' => 'HGW VDSL2 POTS Innbox V45',
                'S/N' => '2821502671'
            ],
            [
                'Šifra' => '990950',
                'Tip' => 'Beogrid modem',
                'Model' => 'HGW VDSL2 POTS Innbox V45',
                'S/N' => '2815502530'
            ],
            [
                'Šifra' => '990950',
                'Tip' => 'Beogrid modem',
                'Model' => 'HGW VDSL2 POTS Innbox V45',
                'S/N' => '2820500525'
            ],
            [
                'Šifra' => '990950',
                'Tip' => 'Beogrid modem',
                'Model' => 'HGW VDSL2 POTS Innbox V45',
                'S/N' => '2820500899'
            ],
            [
                'Šifra' => '990950',
                'Tip' => 'Beogrid modem',
                'Model' => 'HGW VDSL2 POTS Innbox V45',
                'S/N' => '2815502520'
            ],
            [
                'Šifra' => '990950',
                'Tip' => 'Beogrid modem',
                'Model' => 'HGW VDSL2 POTS Innbox V45',
                'S/N' => '2820500394'
            ],
            [
                'Šifra' => '990950',
                'Tip' => 'Beogrid modem',
                'Model' => 'HGW VDSL2 POTS Innbox V45',
                'S/N' => '2820500164'
            ],
            [
                'Šifra' => '990950',
                'Tip' => 'Beogrid modem',
                'Model' => 'HGW VDSL2 POTS Innbox V45',
                'S/N' => '2815502580'
            ],
            [
                'Šifra' => '990950',
                'Tip' => 'Beogrid modem',
                'Model' => 'HGW VDSL2 POTS Innbox V45',
                'S/N' => '2820500352'
            ],
            [
                'Šifra' => '990950',
                'Tip' => 'Beogrid modem',
                'Model' => 'HGW VDSL2 POTS Innbox V45',
                'S/N' => '2820501071'
            ],
            [
                'Šifra' => '990331',
                'Tip' => 'HGW',
                'Model' => 'EPC3928S V2.5 GaTeWaY',
                'S/N' => '280724008'
            ],
            [
                'Šifra' => '990331',
                'Tip' => 'HGW',
                'Model' => 'EPC3928S V2.5 GaTeWaY',
                'S/N' => '280204182'
            ],
            [
                'Šifra' => '990331',
                'Tip' => 'HGW',
                'Model' => 'EPC3928S V2.5 GaTeWaY',
                'S/N' => '280201234'
            ],
            [
                'Šifra' => '990331',
                'Tip' => 'HGW',
                'Model' => 'EPC3928S V2.5 GaTeWaY',
                'S/N' => '281279151'
            ],
            [
                'Šifra' => '990131',
                'Tip' => 'HGW',
                'Model' => 'EPC3928S GaTeWaY',
                'S/N' => '330277763'
            ],
            [
                'Šifra' => '990131',
                'Tip' => 'HGW',
                'Model' => 'EPC3928S GaTeWaY',
                'S/N' => '281985122'
            ],
            [
                'Šifra' => '990231',
                'Tip' => 'HGW',
                'Model' => 'EPC3928S V2 GaTeWaY',
                'S/N' => '276525096'
            ]
        ];

        $brojac = 0;
        foreach ($oprema_niz as $oprema){
            $eq = Models::where('equipment_code', '=', $oprema['Šifra'])->first();

            $eq_type = Subtype::where('equipment_subtype_id', '=', $eq->equipment_subtype_id)->first();

           // dd($eq->equipment_model_id);
           // dd($eq->equipment_subtype_id);
            // dd($eq_type->equipment_type_id);
            $equipment = new Equipment;
            $equipment->equipment_type_id = $eq_type->equipment_type_id;
            $equipment->equipment_subtype_id = $eq->equipment_subtype_id;
            $equipment->equipment_model_id = $eq->equipment_model_id;
            $equipment->equipment_serial1 = $oprema['S/N'];
            $equipment->created_at = '2020-10-11 10:00:00';
            $equipment->equipment_status = "U magacinu";
            $equipment->equipment_dispatch_note = 'Popis 11.10.2020.';
            $equipment->equipment_added_by = 'Popis 11.10.2020.';
            //dd($equipment);
            $equipment->save();

            $brojac = $brojac + 1;
        }

        dd($brojac);

    }

    public function test()
    {



        /*$towships = Select::where('select_name', '=' , 'region')
            ->update(['flag' => 1] );
            dd($towships);*/
        /*  praznenje tabele

        Externalwarehouse::where('wares_id', '>' , 1)
            ->update(['updated_at' => null] );*/


        /* ubacivanje iz zaduzivanja
         *
         * $assigned_wares = DB::table('assigned_wares')->get();
        dd($used_wares);

        foreach($assigned_wares as $ware){
            $wares_id = $ware->wares_id;
            $quantity = $ware->quantity;
            dd([$wares_id => $quantity]);
       Externalwarehouse::where('wares_id',$wares_id)->increment('quantity',$quantity);
        }*/

        /* skidanje iz kroiscenja
         *
         * $used_wares = DB::table('used_wares')->get();
         *
         * foreach($used_wares as $ware){
            $wares_id = $ware->wares_id;
            $quantity = $ware->quantity;
            // dd([$wares_id => $quantity]);
            Externalwarehouse::where('wares_id',$wares_id)->decrement('quantity',$quantity);
        }*/


        // sredjivanje technician reporta

        /*$techniciansreports = DB::table('technicians_report')->get();

        foreach ($techniciansreports as $techniciansreport) {


            $order = Order::where('ordinal_number', $techniciansreport->ordinal_number)->first();
            DB::table('technicians_report')
                ->where('technicians_report_id', $techniciansreport->technicians_report_id)
                ->update(['completed_at' => $order->completed_at]);
        }*/

        // sredjivanje finance reporta

        /*$financereports = DB::table('finance_report')->get();

        foreach ($financereports as $financereport) {

           $order = Order::where('ordinal_number', $financereport->ordinal_number)->first();


            DB::table('finance_report')
                ->where('finance_report_id', $financereport->finance_report_id)
                ->update(['completed_at' => $order->completed_at]);
        }*/

        //serdjivanje izvestaja za materijal
        $used_wares = DB::table('used_wares')->get();

       // dd($used_wares);

        /*foreach ($used_wares as $ware){

            $order = Order::where('ordinal_number', $ware->ordinal_number)->first();
           // dd($order);

            DB::table('used_wares')
                ->where('used_wares_id', $ware->used_wares_id)
                ->update(['completed_at' => $order->completed_at]);
        }*/

        dd('sve ok');


    }
}
