<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Validator;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $users = User::all();
        return view('users.index', compact('users'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if ($id == "update_user") {

            Validator::make($request->all(), [
                'name' => 'required',
                'email' => 'email|required|unique:users',
                'role' => 'required',
            ], [
                'name.required' => 'Polje "Ime i prezime" je obavezno',
                'email.required' => 'Polje "E-Mail adresa" je obavezno',
                'email.email' => 'E-Mail adresa nije u odgovarajućem formatu',
                'email.unique' => 'Nalog sa upisanim e-mail-om već postoji',
                'role.required' => 'Polje "Uloga" je obavezno, izaberite ulogu',
            ])->validate();


            $user = User::find($request->input('user_id'));
            $user->name = $request->input('name');
            $user->email = $request->input('email');
            $user->role = $request->input('role');

            //dd($user);
            $user->save();

            return redirect()->back();
        }elseif($id == "update_pass"){
            Validator::make($request->all(), [
                'password' => 'required|confirmed|min:6',
                'password_confirmation' => 'required',
            ], [
                'password.required' => 'Polje "Lozinka" je obavezno',
                'password.confirmed' => 'Lozinke se ne podudaraju',
                'password.min' => 'Lozinka mora imati minumum 6 karaktera',
                'password_confirmation.required' => 'Polje "Ponovite lozinku" je obavezno',
            ])->validate();


            $user = User::find($request->input('user_id'));
            $user->password = Hash::make($request->input('password'));

            //dd($user);
            $user->save();

            return redirect()->back();
        } elseif($id == "delete"){

            User::destroy($request->input('user_id'));

            return redirect()->back();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
