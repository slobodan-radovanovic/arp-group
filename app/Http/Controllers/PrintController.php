<?php

namespace App\Http\Controllers;

use App\Order;
use Illuminate\Http\Request;

class PrintController extends Controller
{
    public function print_open_order_preview($id)
    {
        $order = Order::find($id);
        return view('print.open_order_preview', compact('order'));
    }

    public function print_open_order($id)
    {
        $order = Order::find($id);
        return view('print.open_order', compact('order'));
    }


    public function print_close_order_preview($id)
    {
        $order = Order::find($id);
        return view('print.close_order_preview', compact('order'));
    }

    public function print_close_order($id)
    {
        $order = Order::find($id);
        return view('print.close_order', compact('order'));
    }


}
