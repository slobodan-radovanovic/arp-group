<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Wares;
use App\Equipment;
use App\Order;
use App\Select;
use App\Externalwarehouse;
use App\Technician;
use Carbon\Carbon;
use Validator;

class DigitizationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if (empty($request->input('date_filter'))) {

            $digitization_orders = Order::where('order_type', 'digitization')->orderBy('order_id', 'desc')->get();
            return view('digitization.index', compact('digitization_orders'));

        } else {

            $date_filter = $request->input('date_filter');
            $exploded_date_filter = explode(' - ', $request->input('date_filter'));
            $exploded_start_date = explode('/', $exploded_date_filter[0]);
            $exploded_end_date = explode('/', $exploded_date_filter[1]);
            $start_date = $exploded_start_date['2'] . '-'
                . $exploded_start_date['1'] . '-'
                . $exploded_start_date['0'] . ' 00:00:01';
            $end_date = $exploded_end_date['2'] . '-'
                . $exploded_end_date['1'] . '-'
                . $exploded_end_date['0'] . ' 23:59:59';


            $digitization_orders = Order::where([
                ['created_at', '>', $start_date],
                ['created_at', '<', $end_date],
                ['order_type', '=', 'digitization']])->orderBy('order_id', 'desc')->get();
            return view('digitization.index', compact('digitization_orders', 'date_filter'));

        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $subtype = createKeyArray(DB::table('equipment_subtype')
            ->rightJoin('equipment', 'equipment_subtype.equipment_subtype_id', '=', 'equipment.equipment_subtype_id')
            ->where('equipment_status', 'Kod tehničara')
            ->pluck('equipment_subtype.equipment_subtype_name', 'equipment_subtype.equipment_subtype_id'));

        $all_serials = createAllSerialsArray(DB::table('equipment')->select('equipment_id', 'equipment_serial1', 'equipment_serial2', 'equipment_serial3')
            ->where('equipment_status', 'Kod tehničara')
            ->get());

        $all_wares = Wares::all();
        $default_wares = $all_wares->where('default', 1);
        $wares = createWaresArray($all_wares->where('default', 0));

        $technicians = createKeyArray(Technician::where('active', 1)->pluck('technician_name', 'technician_id'), 'technician_');

        $city = createValueArray(Select::where('select_name', 'city')->where('flag', '1')->pluck('select_value'));
        $township = createValueArray(Select::where('select_name', 'township')->where('flag', '1')->pluck('select_value'));

        $equipment_for_select = DB::table('equipment_subtype')
            ->join('equipment', 'equipment_subtype.equipment_subtype_id', '=', 'equipment.equipment_subtype_id')
            ->select('equipment.equipment_id', 'equipment_subtype.equipment_subtype_id', 'equipment_subtype.equipment_subtype_name', 'equipment.equipment_serial1', 'equipment.equipment_serial2', 'equipment.equipment_serial3')->where('equipment.equipment_status', 'Kod tehničara')->get();

        $data = [
            'subtype' => $subtype,
            'all_serials' => $all_serials,
            'equipment_for_select' => $equipment_for_select,
            'default_wares' => $default_wares,
            'wares' => $wares,
            'technicians' => $technicians,
            'township' => $township,
            'city' => $city
        ];


        //dd($data);

        return view('digitization.create', compact('data'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */

    public function store(Request $request)
    {
        // dd($request->all());

        Validator::make($request->all(), [
            'buyer' => 'required',
            'address' => 'required',
            'address_number' => 'required',
            'phone_number' => 'required',
            'technician_1' => 'required',
        ], [
            'buyer.required' => 'Polje "Ime i prezime kupca" je obavezno',
            'address.required' => 'Polje "Adresa" je obavezno',
            'address_number.required' => 'Polje "broj" (Adresa) je obavezno',
            'phone_number.required' => 'Polje "Broj telefona" je obavezno',
            'technician_1.required' => 'Polje "Prvi tehničar" je obavezno',
        ])->validate();



        DB::transaction(function () use ($request) {





            $new_ordinal_number = newDigitizationOrdinalNumber();



            //promena statusa opreme

            $equipment_count = 0; //broj opreme koja je ugradjena

            for ($i = 0; $i <= 5; $i++) {
                if ($request->input('equipment_serial_' . $i) != null) {
                    //$text .= explode('_', $request->input('equipment_serial_' . $i))['1'] . "  ";
                    $equipment_id = explode('_', $request->input('equipment_serial_' . $i))['1'];
                    $equipment = Equipment::find($equipment_id);

                    $equipment->equipment_status = "Kod kupca";
                    $equipment->equipment_ordinal_number = $new_ordinal_number;
                    $equipment->save();
                    ++$equipment_count;
                    //dd($equipment);
                }
            }


            // upisivanje upotrebljenog materijala i skidanje sa stanja iz spoljnog magacina
            foreach ($request->all() as $wares_id => $quantity) {
                if (strpos($wares_id, 'wares_') === 0 and $quantity != null) {
                    $wares_id = explode('_', $wares_id)[1];
                    DB::table('used_wares')->insert(
                        ['ordinal_number' => $new_ordinal_number,
                            'wares_id' => $wares_id,
                            'quantity' => $quantity,
                            'created_at' => date('Y-m-d H:i:s'),
                            'completed_at' => date('Y-m-d H:i:s')
                        ]);
                    Externalwarehouse::where('wares_id', $wares_id)->decrement('quantity', $quantity);
                }
            }


            //upisivanje tehnicara koji su radili
            if ($request->input('technician_1') != null) {

                DB::table('order_technician')->insert(
                    ['ordinal_number' => $new_ordinal_number,
                        'technician_id' => explode('_', $request->input('technician_1'))[1],
                        'created_at' => date('Y-m-d H:i:s'),
                    ]);
            }


            if ($request->input('technician_2') != null) {
                DB::table('order_technician')->insert(
                    ['ordinal_number' => $new_ordinal_number,
                        'technician_id' => explode('_', $request->input('technician_2'))[1],
                        'created_at' => date('Y-m-d H:i:s'),
                    ]);
            }

            // create technicina_report

            $service = DB::table('services')
                ->where('service_select', 'digitalizacija')
                ->get();

            //upisivanje u tebelu finansijjski izvestaj
            DB::table('finance_report')->insert(
                [   'order_type' => 'digitization',
                    'ordinal_number' => $new_ordinal_number,
                    'service_name' => 'Digitalizacija',
                    'sum' => $service['0']->for_invoicing,
                    'completed_at' => date('Y-m-d H:i:s')
                ]);

            //izracunavanje za isvestaj

            //da li je korisceno privatno vizilo
            if ($request->input('private_vehicle') == '1') {
                $technicians_report_private_vehicle = $service['0']->price_private_vehicle;
            } else {
                $technicians_report_private_vehicle = null;
            }

            $technicians_report_sum = 0;

            $technicians_report_price_fixed = $service['0']->price_fixed;
            $technicians_report_sum = $technicians_report_price_fixed;


            if ($request->input('technician_2') != null) {
                //ako su dva tehnicara, upisuju se dva sloga u bazu i deli se suma na 2

                //za drugog je private_vehicle null
                DB::table('technicians_report')->insert(
                    ['technician_id' => explode('_', $request->input('technician_2'))[1],
                        'order_type' => 'digitization',
                        'ordinal_number' => $new_ordinal_number,
                        'service_name' => 'digitalizacija',
                        'price_apartment' => null,
                        'price_house' => null,
                        'price_first_equipment' => null,
                        'price_additional_equipment' => null,
                        'price_fixed' => $technicians_report_price_fixed,
                        'price_private_vehicle' => null,
                        'sum' => $technicians_report_sum / 2,
                        'created_at' => date('Y-m-d H:i:s'),
                        'completed_at' => date('Y-m-d H:i:s')
                    ]);


                if ($technicians_report_private_vehicle != null) {
                    $technicians_report_sum = ($technicians_report_sum / 2) + $technicians_report_private_vehicle;
                } else {
                    $technicians_report_sum = $technicians_report_sum / 2;
                }

                DB::table('technicians_report')->insert(
                    ['technician_id' => explode('_', $request->input('technician_1'))[1],
                        'order_type' => 'digitization',
                        'ordinal_number' => $new_ordinal_number,
                        'service_name' => 'digitalizacija',
                        'price_apartment' => null,
                        'price_house' => null,
                        'price_first_equipment' => null,
                        'price_additional_equipment' => null,
                        'price_fixed' => $technicians_report_price_fixed,
                        'price_private_vehicle' => $technicians_report_private_vehicle,
                        'sum' => $technicians_report_sum,
                        'created_at' => date('Y-m-d H:i:s'),
                        'completed_at' => date('Y-m-d H:i:s')
                    ]);


            } else {
                if ($technicians_report_private_vehicle != null) {
                    $technicians_report_sum += $technicians_report_private_vehicle;
                }


                DB::table('technicians_report')->insert(
                    ['technician_id' => explode('_', $request->input('technician_1'))[1],
                        'order_type' => 'digitization',
                        'ordinal_number' => $new_ordinal_number,
                        'service_name' => 'digitalizacija',
                        'price_apartment' => null,
                        'price_house' => null,
                        'price_first_equipment' => null,
                        'price_additional_equipment' => null,
                        'price_fixed' => $technicians_report_price_fixed,
                        'price_private_vehicle' => $technicians_report_private_vehicle,
                        'sum' => $technicians_report_sum,
                        'created_at' => date('Y-m-d H:i:s'),
                        'completed_at' => date('Y-m-d H:i:s')
                    ]);
            }

            // insert order

            $digitization = new Order;
            $digitization->ordinal_number = $new_ordinal_number;
            $digitization->order_type = 'digitization';
            $digitization->order_status = 'Završen';
            $digitization->buyer_id = $request->input('buyer_id');
            $digitization->treaty_id = $request->input('treaty_id');
            $digitization->buyer = $request->input('buyer');
            $digitization->area_code = $request->input('area_code');
            $digitization->city = $request->input('city');
            $digitization->township = $request->input('township');
            $digitization->address = $request->input('address');
            $digitization->address_number = $request->input('address_number');
            $digitization->floor = $request->input('floor');
            $digitization->apartment = $request->input('apartment');
            $digitization->phone_number = $request->input('phone_number');
            $digitization->mobile_number = $request->input('mobile_number');
            $digitization->service_type = 'digitization';
            $digitization->note = $request->input('order_comment');

            if ($request->input('private_vehicle') == '1') {
                $digitization->private_vehicle = 1;
            } else {
                $digitization->private_vehicle = 0;
            }

            $digitization->created_at = date('Y-m-d H:i:s');
            $digitization->completed_at = date('Y-m-d H:i:s');

            $digitization->save();
        });

        $id = DB::table('orders')->orderBy('order_id', 'desc')->first()->order_id;

        //dd($id);
        return redirect('/digitization/' . $id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */

    public function show($id)
    {

        $order = Order::find($id);


        $technicians = createKeyArray(Technician::pluck('technician_name', 'technician_id'), 'technician_');
        $all_wares = Wares::all();
        $default_wares = $all_wares->where('default', 1);
        $wares = createWaresArray($all_wares->where('default', 0));

        $used_equipment = DB::table('equipment')
            ->join('equipment_subtype', 'equipment.equipment_subtype_id', '=', 'equipment_subtype.equipment_subtype_id')
            ->join('equipment_model', 'equipment.equipment_model_id', '=', 'equipment_model.equipment_model_id')
            ->select('equipment.*', 'equipment_subtype.equipment_subtype_name', 'equipment_model.equipment_model_name')
            ->where('equipment.equipment_ordinal_number', '=', $order->ordinal_number)
            ->get();


        $used_wares = DB::table('used_wares')
            ->join('wares', 'used_wares.wares_id', '=', 'wares.wares_id')
            ->select('wares.wares_code', 'wares.wares_name', 'used_wares.quantity')
            ->where('used_wares.ordinal_number', '=', $order->ordinal_number)
            ->get();


        //treba resiti preko relacije!!!!
        $order_technician = DB::table('order_technician')
            ->join('technicians', 'order_technician.technician_id', '=', 'technicians.technician_id')
            ->join('orders', 'order_technician.ordinal_number', '=', 'orders.ordinal_number')
            ->select('technicians.technician_name')
            ->where('order_technician.ordinal_number', '=', $order->ordinal_number)
            ->get();

        //dd($order_technician);
        $data = [
            'order' => $order,
            'wares' => $wares,
            'technicians' => $technicians,
            'used_equipment' => $used_equipment,
            'used_wares' => $used_wares,
            'order_technician' => $order_technician,
        ];

        //dd($data);

        return view('digitization.show', compact('data', 'id'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

