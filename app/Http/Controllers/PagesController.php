<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Equipment;
use App\Technician;
use DB;
use App\User;
use App\Order;
use App\Select;
use App\Service;
use App\Wares;
use Validator;

class PagesController extends Controller
{

    public function index()
    {
        return view('pages.index');
    }

    public function scheduling()
    {
        return view('pages.scheduling');
    }

    public function financereport(Request $request)
    {
        if (!empty($request->input('date_filter'))) {

            $date_filter = $request->input('date_filter');
            $date_period = str_replace("/","-", $date_filter);
            /*$region_filter = $request->input('region');*/
            $exploded_date_filter = explode(' - ', $request->input('date_filter'));
            $exploded_start_date = explode('/', $exploded_date_filter[0]);
            $exploded_end_date = explode('/', $exploded_date_filter[1]);
            $start_date = $exploded_start_date['2'] . '-'
                . $exploded_start_date['1'] . '-'
                . $exploded_start_date['0'] . ' 00:00:01';
            $end_date = $exploded_end_date['2'] . '-'
                . $exploded_end_date['1'] . '-'
                . $exploded_end_date['0'] . ' 23:59:59';

            //$order_type = $request->input('order_type');
            $financereport = DB::table('finance_report')->where([
                ['completed_at', '>', $start_date],
                ['completed_at', '<', $end_date]
            ])->get();

            $sum = $financereport->sum('sum');

            //dd($sum);

            $sbb_order = $financereport->where('order_type', 'sbb_order');
            $sbb_order_sum = $sbb_order->sum('sum');
            $digitization = $financereport->where('order_type', 'digitization');
            $digitization_sum = $digitization->sum('sum');
            $reclamation = $financereport->where('order_type', 'reclamation');
            $reclamation_sum = $reclamation->sum('sum');

            $order_type = Select::where('flag', 'order_type')->pluck('select_value', 'select_name');

           // dd($order_type);

            //dd($reclamation);


            /*[
                'sbb_order' => $sbb_order,
                'reclamation' => $reclamation,
                'digitization' => $digitization,
            ];*/

            //dd($orders);

            return view('pages.financereport', compact('financereport', 'date_filter', 'sum', 'sbb_order', 'digitization', 'reclamation', 'sbb_order_sum', 'digitization_sum', 'reclamation_sum', 'date_period', 'order_type'));

        } else {
            return view('pages.financereport');
        }
    }

    public function waresreport(Request $request)
    {
        if (!empty($request->input('date_filter'))) {

            $date_filter = $request->input('date_filter');
            $date_period = str_replace("/","-", $date_filter);
            /*$region_filter = $request->input('region');*/
            $exploded_date_filter = explode(' - ', $request->input('date_filter'));
            $exploded_start_date = explode('/', $exploded_date_filter[0]);
            $exploded_end_date = explode('/', $exploded_date_filter[1]);
            $start_date = $exploded_start_date['2'] . '-'
                . $exploded_start_date['1'] . '-'
                . $exploded_start_date['0'] . ' 00:00:01';
            $end_date = $exploded_end_date['2'] . '-'
                . $exploded_end_date['1'] . '-'
                . $exploded_end_date['0'] . ' 23:59:59';

            $waresreport1 = DB::table('used_wares')
                ->join('wares', 'used_wares.wares_id', '=', 'wares.wares_id')
                ->select('used_wares.*', 'wares.wares_name', 'wares.wares_type' , DB::raw('SUM(quantity) as total_quantity'))
                ->where([
                    ['used_wares.completed_at', '>', $start_date],
                    ['used_wares.completed_at', '<', $end_date]
                ])
                ->groupBy('used_wares.wares_id')
                ->get();

               // dd($waresreport);
            //$order_type = $request->input('order_type');
            $waresreport2 = DB::table('used_wares')
                ->join('wares', 'used_wares.wares_id', '=', 'wares.wares_id')
                ->select('used_wares.*', 'wares.wares_name', 'wares.wares_type')
                ->where([
                ['used_wares.completed_at', '>', $start_date],
                ['used_wares.completed_at', '<', $end_date]
            ])->get();

            /*$waresreport_group = DB::table('used_wares')
                ->join('wares', 'used_wares.wares_id', '=', 'wares.wares_id')
                ->select('used_wares.*', 'wares.wares_name', 'wares.wares_type')
                ->where([
                    ['used_wares.created_at', '>', $start_date],
                    ['used_wares.created_at', '<', $end_date]
                ])
                ->groupBy('used_wares.wares_id')
                ->get();*/

            //dd($waresreport_group);

            return view('pages.waresreport', compact('waresreport1', 'waresreport2','date_filter', 'date_period'));

        } else {
            return view('pages.waresreport');
        }
    }

    public function techniciansreport_post(Request $request)
    {

        Validator::make($request->all(), [
            'technician' => 'required',
        ], [
            'technician.required' => 'Polje "Tehničar" je obavezno, izaberite tehničara',
        ])->validate();

        $dismantled_date_filter = $request->input('dismantled_date_filter');
        $date_period = str_replace("/","-", $dismantled_date_filter);
        $exploded_date_filter = explode(' - ', $dismantled_date_filter);
        $exploded_start_date = explode('/', $exploded_date_filter[0]);
        $exploded_end_date = explode('/', $exploded_date_filter[1]);
        $start_date = $exploded_start_date['2'] . '-' . $exploded_start_date['1'] . '-' . $exploded_start_date['0'] . ' 00:00:01';
        $end_date = $exploded_end_date['2'] . '-' . $exploded_end_date['1'] . '-' . $exploded_end_date['0'] . ' 23:59:59';

        $technician_id = explode('_', $request->input('technician'))[1];
        $technician = $request->input('technician');
        $sum = DB::table('technicians_report')->where([
            ['completed_at', '>', $start_date],
            ['completed_at', '<', $end_date],
            ['technician_id', '=', $technician_id]
        ])->sum('sum');

        $services = DB::table('services')->pluck('service_name', 'service_select');


        $techniciansreport = DB::table('technicians_report')->where([
            ['completed_at', '>', $start_date],
            ['completed_at', '<', $end_date],
            ['technician_id', '=', $technician_id]
        ])->get();

        // dd($techniciansreport);

        $technicians = createKeyArray(Technician::pluck('technician_name', 'technician_id'), 'technician_');

        return view('pages.techniciansreport', compact('techniciansreport', 'technicians', 'sum', 'dismantled_date_filter', 'technician', 'services', 'date_period'));
    }

    public function techniciansreport()
    {
        $techniciansreport = [];
        $technician = null;
        $technicians = createKeyArray(Technician::pluck('technician_name', 'technician_id'), 'technician_');
        return view('pages.techniciansreport', compact('techniciansreport', 'technicians', 'technician'));
    }

    public function techniciansreport_all_post(Request $request)
    {

        /*Validator::make($request->all(), [
            'technician' => 'required',
        ], [
            'technician.required' => 'Polje "Tehničar" je obavezno, izaberite tehničara',
        ])->validate();*/

        $dismantled_date_filter = $request->input('dismantled_date_filter');
        $date_period = str_replace("/","-", $dismantled_date_filter);
        $exploded_date_filter = explode(' - ', $dismantled_date_filter);
        $exploded_start_date = explode('/', $exploded_date_filter[0]);
        $exploded_end_date = explode('/', $exploded_date_filter[1]);
        $start_date = $exploded_start_date['2'] . '-'
            . $exploded_start_date['1'] . '-'
            . $exploded_start_date['0'] . ' 00:00:01';
        $end_date = $exploded_end_date['2'] . '-'
            . $exploded_end_date['1'] . '-'
            . $exploded_end_date['0'] . ' 23:59:59';


        $services = DB::table('services')->pluck('service_name', 'service_select');


        $techniciansreport = DB::table('technicians_report')->where([
            ['completed_at', '>', $start_date],
            ['completed_at', '<', $end_date]
        ])->get();

        $sum = DB::table('technicians_report')->where([
            ['completed_at', '>', $start_date],
            ['completed_at', '<', $end_date]
        ])->sum('sum');

       // dd($services);
       // dd($techniciansreport);

        $technicians = createKeyArray(Technician::pluck('technician_name', 'technician_id'));

       // dd($techniciansreport);

        return view('pages.techniciansreport_all', compact('techniciansreport', 'technicians', 'dismantled_date_filter', 'services', 'date_period', 'sum'));
    }

    public function techniciansreport_all()
    {
        $techniciansreport = [];
        $technicians = createKeyArray(Technician::pluck('technician_name', 'technician_id'), 'technician_');
        return view('pages.techniciansreport_all', compact('techniciansreport', 'technicians'));
    }

    public function sbbreport(Request $request)
    {

        //dd(empty($request->input('date_filter')))    ;
        if (!empty($request->input('date_filter'))) {
            //dd($request->all());

            $date_filter = $request->input('date_filter');
            $date_period = str_replace("/","-", $date_filter);
            $region_filter = $request->input('region');
            //dd($region_filter);
            $exploded_date_filter = explode(' - ', $request->input('date_filter'));
            $exploded_start_date = explode('/', $exploded_date_filter[0]);
            $exploded_end_date = explode('/', $exploded_date_filter[1]);
            $start_date = $exploded_start_date['2'] . '-'
                . $exploded_start_date['1'] . '-'
                . $exploded_start_date['0'] . ' 00:00:01';
            $end_date = $exploded_end_date['2'] . '-'
                . $exploded_end_date['1'] . '-'
                . $exploded_end_date['0'] . ' 23:59:59';


            $digitization = Order::where([
                ['created_at', '>', $start_date],
                ['created_at', '<', $end_date],
                ['order_type', '=', 'digitization']
            ])->orderBy('order_id', 'desc')->get();

            $sbb_order = Order::where([
                ['created_at', '>', $start_date],
                ['created_at', '<', $end_date],
                ['order_type', '=', 'sbb_order']
            ])->where(function ($query) {
                $query->where('order_status', 'Zavrsen')
                    ->orWhere('order_status', 'Nekompletan');
            })->orderBy('order_id', 'desc')->get();

            $reclamation = Order::where([
                ['created_at', '>', $start_date],
                ['created_at', '<', $end_date],
                ['order_type', '=', 'reclamation']
            ])->where(function ($query) {
                $query->where('order_status', 'Zavrsen')
                    ->orWhere('order_status', 'Nekompletan');
            })->orderBy('order_id', 'desc')->get();

            //dd($reclamation);


            if ($region_filter != null) {

                $sbb_order = $sbb_order->where('region', $region_filter);
                $reclamation = $reclamation->where('region', $region_filter);
                $digitization = [];

            } /*else {

                dd($region_filter);

                $sbb_order = Order::where([
                    ['created_at', '>', $start_date],
                    ['created_at', '<', $end_date],
                    ['order_type', '=', 'sbb_order'].
                    ['region', '=', $region_filter]
                ])->where(function ($query) {
                    $query->where('order_status', 'Završen')
                        ->orWhere('order_status', 'Nekompletan');
                })->orderBy('order_id', 'desc')->get();

                $reclamation = Order::where([
                    ['created_at', '>', $start_date],
                    ['created_at', '<', $end_date],
                    ['order_type', '=', 'reclamation'].
                    ['region', '=', $region_filter]
                ])->where(function ($query) {
                    $query->where('order_status', 'Završen')
                        ->orWhere('order_status', 'Nekompletan');
                })->orderBy('order_id', 'desc')->get();

            }*/


            //dd($active_orders);

            //return view('equipment.dismantled', compact('removed_equipment', 'dismantled_date_filter'));

            $region = createValueArray(Select::where('select_name', 'region')->pluck('select_value'));

            $services = createKeyArray(DB::table('services')->pluck('service_name', 'service_select'));

            $orders = [
                'sbb_order' => $sbb_order,
                'reclamation' => $reclamation,
                'digitization' => $digitization,
                'services' => $services,
            ];

            //dd($orders);

            return view('pages.sbbreport', compact('orders', 'date_filter', 'region_filter', 'region', 'date_period'));

        } else {
            $region = createValueArray(Select::where('select_name', 'region')->pluck('select_value'));
            return view('pages.sbbreport', compact('region'));
        }


        /*dd($orders);

        if (empty($request->input('date_filter'))) {
            return view('orders.index', compact('orders'));
        }else{

        }*/

    }

    public function techniciansreport_show($id)
    {

        $techniciansreport = DB::table('technicians_report')
            ->join('technicians', 'technicians_report.technician_id', '=', 'technicians.technician_id')
            ->join('orders', 'technicians_report.ordinal_number', '=', 'orders.ordinal_number')
            ->select('technicians_report.*', 'technicians.technician_name', 'orders.order_status')
            ->where('technicians_report_id', $id)
            ->get();


        $services = DB::table('services')->pluck('service_name', 'service_select');

        $order_reports = DB::table('technicians_report')
            ->join('technicians', 'technicians_report.technician_id', '=', 'technicians.technician_id')
            ->select('technicians_report.*', 'technicians.technician_name')
            ->where('ordinal_number', $techniciansreport['0']->ordinal_number)
            ->get();


        //dd($order_reports);

        $count = $order_reports->count();

        /*$count = DB::table('technicians_report')
            ->where('ordinal_number', $techniciansreport['0']->ordinal_number)
            ->count('ordinal_number');*/

        $report = $techniciansreport['0'];

        // dd($report);
        //dd($techniciansreport['0']);
        $order_types = DB::table('selects')
            ->where('flag', 'order_type')
            ->pluck('select_value', 'select_name');

        //dd($order_types);

        if ($report->order_type == 'reclamation') {

            $order_note = DB::table('orders')
                ->select('order_id', 'note')
                ->where('ordinal_number', $techniciansreport['0']->ordinal_number)
                ->get();

            $finance_report = DB::table('finance_report')
                ->select('finance_report_id', 'sum')
                ->where('ordinal_number', $techniciansreport['0']->ordinal_number)
                ->get();


            //dd($finance_report['0']->sum);

            return view('pages.techniciansreport_show', compact('report', 'count', 'order_types', 'services', 'order_note', 'order_reports', 'finance_report'));
        }

        return view('pages.techniciansreport_show', compact('report', 'count', 'order_types', 'services', 'order_reports'));
    }

    public function update(Request $request, $id)
    {
        //dd($request->all());
        if ($id == "update_reports") {

            DB::transaction(function () use ($request) {
            foreach ($request->all() as $id_for_update => $new_value) {
                if (strpos($id_for_update, 'edit_report') === 0) {
                    $report_id_for_update = explode('_', $id_for_update)[2];

                    DB::table('technicians_report')
                        ->where('technicians_report_id', $report_id_for_update)
                        ->update(['sum' => $new_value]);
                }

                if (strpos($id_for_update, 'finance_report') === 0) {
                    $finance_id_for_update = explode('_', $id_for_update)[2];

                    DB::table('finance_report')
                        ->where('finance_report_id', $finance_id_for_update)
                        ->update(['sum' => $new_value]);
                }

                if (strpos($id_for_update, 'order_note') === 0) {
                    $order_id_for_update = explode('_', $id_for_update)[2];

                        DB::table('orders')
                            ->where('order_id', $order_id_for_update)
                            ->update(['note' => $new_value]);
                }
            }
            }); //kraj transakcije

            return redirect()->back();


        }



    }

    public function update_city(Request $request, $id)
    {

        $select = Select::find($request->input('select_id_city'));

        if($request->input('active_city') == null){
            $active_city = 0;
        }elseif ($request->input('active_city') == 1){
            $active_city = 1;
        }

        if($active_city == $select->flag){
            return redirect()->back()->with('success', 'Bez izmena');
        }

        $select->flag = $active_city;
        $select->save();

        if ($active_city == 0){
            return redirect()->back()->with('success', 'Grad "' . $select->select_value . '" postavljen kao neaktivan');
        }elseif($active_city == 1){
            return redirect()->back()->with('success', 'Grad "' . $select->select_value . '" postavljen kao aktivan');
        }

    }

    public function update_township(Request $request, $id)
    {

        $select = Select::find($request->input('select_id_township'));

        if($request->input('active_township') == null){
            $active_township = 0;
        }elseif ($request->input('active_township') == 1){
            $active_township = 1;
        }

        if($active_township == $select->flag){
            return redirect()->back()->with('success', 'Bez izmena');
        }

        $select->flag = $active_township;
        $select->save();

        if ($active_township == 0){
            return redirect()->back()->with('success', 'Opština "' . $select->select_value . '" postavljena kao neaktivna');
        }elseif($active_township == 1){
            return redirect()->back()->with('success', 'Opština "' . $select->select_value . '" postavljena kao aktivana');
        }

    }

    public function update_region(Request $request, $id)
    {
        $select = Select::find($request->input('select_id_region'));

        if($request->input('active_region') == null){
            $active_region = 0;
        }elseif ($request->input('active_region') == 1){
            $active_region = 1;
        }

        if($active_region == $select->flag){
            return redirect()->back()->with('success', 'Bez izmena');
        }

        $select->flag = $active_region;
        $select->save();

        if ($active_region == 0){
            return redirect()->back()->with('success', 'Region "' . $select->select_value . '" postavljen kao neaktivan');
        }elseif($active_region == 1){
            return redirect()->back()->with('success', 'Region "' . $select->select_value . '" postavljena kao aktivan');
        }

    }


    public function home()
    {
        return redirect()->route('login');
    }

    public function users()
    {
        $users = User::all();
        return view('pages.users', compact('users'));

    }

    public function townships()
    {

        return view('pages.add_select');

    }

    public function active_townships()
    {
        $cities = Select::where('select_name', '=', 'city')->get();
        $townships = Select::where('select_name', '=', 'township')->get();
        $regions = Select::where('select_name', '=', 'region')->get();

        return view('pages.active_townships', compact('cities', 'townships', 'regions'));
    }

    public function add_city(Request $request)
    {

        Validator::make($request->all(), [
            'city' => 'required',
        ], [
            'city.required' => 'Polje "Ime grada" je obavezno',
        ])->validate();

        DB::table('selects')->insert(
            ['select_name' => 'city',
                'select_value' => $request->input('city'),
                'flag' => 1,
            ]);
        return redirect('/townships')->with('success', 'Uspešno dodat novi grad "' . $request->input('city') . '"');
    }

    public function add_township(Request $request)
    {

        Validator::make($request->all(), [
            'township' => 'required',
        ], [
            'township.required' => 'Polje "Ime opštine" je obavezno',
        ])->validate();

        DB::table('selects')->insert(
            ['select_name' => 'township',
                'select_value' => $request->input('township'),
                'flag' => 1,
            ]);
        return redirect('/townships')->with('success', 'Uspešno dodata nova opština "' . $request->input('township') . '"');
    }

    public function add_region(Request $request)
    {

        Validator::make($request->all(), [
            'region' => 'required',

        ], [
            'region.required' => 'Polje "Ime regiona" je obavezno',
        ])->validate();

        DB::table('selects')->insert(
            ['select_name' => 'region',
                'select_value' => $request->input('region'),
                'flag' => 1,
            ]);
        return redirect('/townships')->with('success', 'Uspešno dodat novi region "' . $request->input('region') . '"');
    }

    public function tabela()
    {
        return view('pages.tabela');
    }

    public function sbbreport_completed(Request $request)
    {

        if (empty($request->input('date_filter'))) {

            $sbbreport_completed = Order::where([
                ['order_type', '=', 'sbb_order'],
                ['order_status', '=', 'Završen']
            ])->limit(100)->get();

            /*dd($sbbreport_completed);*/

        } else {

            $date_filter = $request->input('date_filter');
            $exploded_date_filter = explode(' - ', $request->input('date_filter'));
            $exploded_start_date = explode('/', $exploded_date_filter[0]);
            $exploded_end_date = explode('/', $exploded_date_filter[1]);
            $start_date = $exploded_start_date['2'] . '-'
                . $exploded_start_date['1'] . '-'
                . $exploded_start_date['0'] . ' 00:00:01';
            $end_date = $exploded_end_date['2'] . '-'
                . $exploded_end_date['1'] . '-'
                . $exploded_end_date['0'] . ' 23:59:59';
            /* dd($end_date);*/


            $sbbreport_completed = Order::where([
                    ['completed_at', '>', $start_date],
                    ['completed_at', '<', $end_date],
                    ['order_type', '=', 'sbb_order'],
                    ['order_status', '=', 'Završen']
                ])->get();


        }

        $orders = [
            'sbbreport_completed' => $sbbreport_completed,
            'sbbreport_completed_count' => $sbbreport_completed->count()
        ];

        if (empty($request->input('date_filter'))) {
            return view('pages.sbbreport_completed', compact('orders'));
        }else{
            return view('pages.sbbreport_completed', compact('orders', 'date_filter' ));
        }


    }

    public function sbbreport_canceled(Request $request)
    {

        if (empty($request->input('date_filter'))) {

            $sbbreport_canceled = Order::where('order_type', 'sbb_order')
                    ->where('order_status', 'Otkazan')
                    ->orWhere('order_status', 'Nerealizovan')
                    ->get();


           // dd($sbbreport_canceled);

        } else {

            $date_filter = $request->input('date_filter');
            $exploded_date_filter = explode(' - ', $request->input('date_filter'));
            $exploded_start_date = explode('/', $exploded_date_filter[0]);
            $exploded_end_date = explode('/', $exploded_date_filter[1]);
            $start_date = $exploded_start_date['2'] . '-'
                . $exploded_start_date['1'] . '-'
                . $exploded_start_date['0'] . ' 00:00:01';
            $end_date = $exploded_end_date['2'] . '-'
                . $exploded_end_date['1'] . '-'
                . $exploded_end_date['0'] . ' 23:59:59';
            /* dd($end_date);*/


            $sbbreport_canceled = Order::where([
                ['created_at', '>', $start_date],
                ['created_at', '<', $end_date],
                ['order_type', '=', 'sbb_order']
            ])->get();


        }

        $orders = [
            'sbbreport_canceled' => $sbbreport_canceled,
            'sbbreport_canceled_count' => $sbbreport_canceled->count()
        ];

        if (empty($request->input('date_filter'))) {
            return view('pages.sbbreport_canceled', compact('orders'));
        }else{
            return view('pages.sbbreport_canceled', compact('orders', 'date_filter' ));
        }


    }

    public function sbbreport_in_progress(Request $request)
    {

        if (empty($request->input('date_filter'))) {

            $sbbreport_in_progress = Order::where('order_type', 'sbb_order')
                ->where('order_status', 'Otvoren')
                ->orWhere('order_status', 'Zakazan')
                ->orWhere('order_status', 'Odložen')
                ->get();


            // dd($sbbreport_canceled);

        } else {

            $date_filter = $request->input('date_filter');
            $exploded_date_filter = explode(' - ', $request->input('date_filter'));
            $exploded_start_date = explode('/', $exploded_date_filter[0]);
            $exploded_end_date = explode('/', $exploded_date_filter[1]);
            $start_date = $exploded_start_date['2'] . '-'
                . $exploded_start_date['1'] . '-'
                . $exploded_start_date['0'] . ' 00:00:01';
            $end_date = $exploded_end_date['2'] . '-'
                . $exploded_end_date['1'] . '-'
                . $exploded_end_date['0'] . ' 23:59:59';
            /* dd($end_date);*/


            $sbbreport_in_progress = Order::where([
                ['created_at', '>', $start_date],
                ['created_at', '<', $end_date],
                ['order_type', '=', 'sbb_order']
            ])->get();


        }

        $orders = [
            'sbbreport_in_progress' => $sbbreport_in_progress,
            'sbbreport_in_progress_count' => $sbbreport_in_progress->count()
        ];

        if (empty($request->input('date_filter'))) {
            return view('pages.sbbreport_in_progress', compact('orders'));
        }else{
            return view('pages.sbbreport_in_progress', compact('orders', 'date_filter' ));
        }



    }





}
