<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Technician;
use App\Equipment;
use Validator;
use Illuminate\Support\Facades\DB;

class TechniciansController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $technicians = Technician::where('active', 1)->get();
        return view('technicians.index', compact('technicians'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('technicians.create');
    }

    public function deleted()
    {
        $technicians = Technician::where('active', 0)->get();
        return view('technicians.deleted', compact('technicians'));
    }

    public function assigned()
    {
        $technicians = Technician::all();
        return view('technicians.assigned', compact('technicians'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        Validator::make($request->all(), [
        'technician_name' => 'required',
        'mobile_number' => 'required',
    ], [
        'technician_name.required' => 'Polje "Ime tehničara" je obavezno',
        'mobile_number.required' => 'Polje "Broje telefona" je obavezno',
    ])->validate();
       /* dd($request->all());*/

        $technician = new Technician;
        $technician->technician_name = $request->input('technician_name');
        $technician->mobile_number = $request->input('mobile_number');
        $technician->active = 1;

        $technician->save();

       /* dd($technician);*/
        return redirect('/technicians');



    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show_old($id)
    {

        $assigned_equipments = DB::table('equipment')
            ->join('equipment_subtype', 'equipment.equipment_subtype_id', '=', 'equipment_subtype.equipment_subtype_id')
            ->join('equipment_model', 'equipment.equipment_model_id', '=', 'equipment_model.equipment_model_id')
            ->select(
                'equipment.equipment_id',
                'equipment.equipment_serial1',
                'equipment.equipment_serial2',
                'equipment.equipment_serial3',
                'equipment.equipment_assign_date',
                'equipment_subtype.equipment_subtype_name',
                'equipment_model.equipment_model_name',
                'equipment.equipment_technician')
            ->where('equipment.equipment_technician', '=', $id)
            ->where('equipment.equipment_status', '=', 'Kod tehničara')
            ->get();



        $assigned_wares = DB::table('assigned_wares')
            ->join('wares', 'assigned_wares.wares_id', '=', 'wares.wares_id')
            ->select(
                'assigned_wares.quantity',
                'assigned_wares.created_at',
                'wares.wares_name')
            ->where('assigned_wares.technician_id', '=', $id)->get();
       /* dd($assigned_wares);*/

        $technician = Technician::find($id);
        $technician_name = $technician->technician_name;
        return view('technicians.show', compact('assigned_equipments', 'assigned_wares','technician_name'));
    }

    public function show($id)
    {

        $assigned_equipments = DB::table('equipment')
            ->join('equipment_subtype', 'equipment.equipment_subtype_id', '=', 'equipment_subtype.equipment_subtype_id')
            ->join('equipment_model', 'equipment.equipment_model_id', '=', 'equipment_model.equipment_model_id')
            ->select(
                'equipment.equipment_id',
                'equipment.equipment_serial1',
                'equipment.equipment_serial2',
                'equipment.equipment_serial3',
                'equipment.equipment_assign_date',
                'equipment_subtype.equipment_subtype_name',
                'equipment_model.equipment_model_name',
                'equipment.equipment_technician')
            ->where('equipment.equipment_technician', '=', $id)
            ->where('equipment.equipment_status', '=', 'Kod tehničara')
            ->get();



        $assigned_wares = DB::table('assigned_wares')
            ->join('wares', 'assigned_wares.wares_id', '=', 'wares.wares_id')
            ->select(
                'assigned_wares.quantity',
                'assigned_wares.created_at',
                'wares.wares_name')
            ->where('assigned_wares.technician_id', '=', $id)->get();
        /* dd($assigned_wares);*/

        $assigned_wares_sum = /*createKeyArray(*/DB::table('assigned_wares')
            ->join('wares', 'assigned_wares.wares_id', '=', 'wares.wares_id')
            ->select(DB::raw('sum(assigned_wares.quantity) as assigned_wares_sum, wares.wares_name'))
            ->where('assigned_wares.technician_id', '=', $id)
            ->groupBy('assigned_wares.wares_id')->get();
        //->pluck('sum(assigned_wares.quantity) as assigned_wares_sum', 'wares.wares_name'));

        $order_tehnicain = DB::table('order_technician')->select('ordinal_number')->where('technician_id', '=', $id )->get();


        $wares_by_order = [];
        foreach($order_tehnicain as $ordinal_number){
            //  dd($order_tehnicain);
            $order_tehnicain_count =  DB::table('order_technician')
                ->where('ordinal_number', '=', $ordinal_number->ordinal_number)->count();

            if ($order_tehnicain_count == 2){
                $order_tehnicain_first =  DB::table('order_technician')
                    ->select('technician_id')
                    ->where('ordinal_number', '=', $ordinal_number->ordinal_number )
                    ->first();

                if($order_tehnicain_first->technician_id == $id){

                    $wares_by_order[$ordinal_number->ordinal_number] = createKeyArray(DB::table('used_wares')
                        ->join('wares', 'used_wares.wares_id', '=', 'wares.wares_id')
                        ->select('used_wares.quantity','wares.wares_name')
                        ->where('used_wares.ordinal_number', '=', $ordinal_number->ordinal_number)
                        ->pluck('used_wares.quantity', 'wares.wares_name'));
                }

            }else{
                $wares_by_order[$ordinal_number->ordinal_number] = createKeyArray(DB::table('used_wares')
                    ->join('wares', 'used_wares.wares_id', '=', 'wares.wares_id')
                    ->select('used_wares.quantity','wares.wares_name')
                    ->where('used_wares.ordinal_number', '=', $ordinal_number->ordinal_number)
                    ->pluck('used_wares.quantity', 'wares.wares_name'));
            }

            //dd($order_tehnicain_count['0']->order_tehnicain_count);
        }

        $wares_by_order = array_filter($wares_by_order, function($value) { return $value != []; });

        //dd($wares_by_order);
        $wares_by_technician = [];
        foreach ($wares_by_order as $wares){
            foreach ($wares as $key => $value){
                if(array_key_exists ( $key , $wares_by_technician)){
                    $wares_by_technician[$key] += $value;
                }else{
                    $wares_by_technician[$key] = $value;
                }
            }
        }

        //dd($wares_by_technicians);

        /*$used_wares = DB::table('used_wares')
            ->join('wares', 'used_wares.wares_id', '=', 'wares.wares_id')
            ->join('orders', 'used_wares.ordinal_number', '=', 'orders.ordinal_number')
            ->select(
                'used_wares.quantity',
                'used_wares.ordinal_number',
                'used_wares.created_at',
                'wares.wares_name')
            ->where('orders.technician_id', '=', $id)->get();*/

        //dd($assigned_wares_sum);

        $technician = Technician::find($id);
        $technician_name = $technician->technician_name;
        return view('technicians.show', compact('assigned_equipments', 'assigned_wares','technician_name','assigned_wares_sum', 'wares_by_order', 'wares_by_technician'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        /*dd($request->all());*/
        /*dd($id);*/
        if($id=="update_active"){

            Validator::make($request->all(), [
                'technician_name' => 'required',
                'mobile_number' => 'required',
            ], [
                'technician_name.required' => 'Polje "Ime tehničara" je obavezno',
                'mobile_number.required' => 'Polje "Broje telefona" je obavezno',
            ])->validate();


            $technician = Technician::find($request->input('technician_id'));
            $technician->technician_name = $request->input('technician_name');
            $technician->mobile_number = $request->input('mobile_number');
            $technician->save();
            return redirect('/technicians');
        }elseif ($id=="update_inactive"){

            Validator::make($request->all(), [
                'technician_name' => 'required',
                'mobile_number' => 'required',
            ], [
                'technician_name.required' => 'Polje "Ime tehničara" je obavezno',
                'mobile_number.required' => 'Polje "Broje telefona" je obavezno',
            ])->validate();


            $technician = Technician::find($request->input('technician_id'));
            $technician->technician_name = $request->input('technician_name');
            $technician->mobile_number = $request->input('mobile_number');
            $technician->save();
            return redirect('/technicians/deleted');
        }elseif ($id=="delete"){
            $technician = Technician::find($request->input('technician_id'));
            $technician->active = 0;
            $technician->save();
            return redirect('/technicians');
        }elseif ($id=="activate"){
            $technician = Technician::find($request->input('technician_id'));
            $technician->active = 1;
            $technician->save();
            return redirect('/technicians');
        }


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
