<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/users';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data,[
            'name' => 'required',
            'role' => 'required',
            'password' => 'required|confirmed|min:6',
            'password_confirmation' => 'required',
            'email' => 'email|required|unique:users',
        ], [
            'name.required' => 'Polje "Ime i prezime" je obavezno',
            'email.required' => 'Polje "E-Mail adresa" je obavezno',
            'email.email' => 'E-Mail adresa nije u odgovarajućem formatu',
            'email.unique' => 'Nalog sa upisanim e-mail-om već postoji',
            'role.required' => 'Polje "Uloga" je obavezno, izaberite ulogu',
            'password.required' => 'Polje "Lozinka" je obavezno',
            'password.confirmed' => 'Lozinke se ne podudaraju',
            'password.min' => 'Lozinka mora imati minumum 6 karaktera',
            'password_confirmation.required' => 'Polje "Ponovite lozinku" je obavezno',

        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        //dd($data['role']);
        $user = new User;
        $user->name = $data['name'];
        $user->email = $data['email'];
        $user->role = $data['role'];
        $user->password = Hash::make($data['password']);
       //dd($user);
        $user->save();
        /*
        return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'role' => $data['role'],
            'password' => Hash::make($data['password']),
        ]);*/
    }
}
