<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Service;
use DB;
use Illuminate\Support\Facades\Validator;

class ServicesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $sbb_order = Service::all()->where('order_type', 'sbb_order');
        $digitization_type = Service::all()->where('order_type', 'digitization');
        $reclamation = Service::all()->where('order_type', 'reclamation')/*->where('service_select', '!=','reklamacija')*/;

        /*$reclamation_type = Service::all()->where('order_type', 'reclamation')->where('service_select','reklamacija');*/

        //dd($services);

        $data = [
            'sbb_order' => $sbb_order,
            'reclamation' => $reclamation,
            'digitization_type' => $digitization_type,
           /* 'reclamation_type' => $reclamation_type,*/
                ];

        //dd($data);

        return view('service.index', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        //dd($request->all());


        if(isset($request->price_fixed_checkbox)){

            Validator::make($request->all(), [
                'service_name' => 'required',
                'price_fixed' => 'required',
                'price_private_vehicle' => 'required',
                'for_invoicing_apartment' => 'required',
                'for_invoicing_house' => 'required',
            ], [
                'service_name.required' => 'Polje "Ime tipa posla" je obavezno',
                'price_fixed.required' => 'Polje "Cena" je obavezno',
                'price_private_vehicle.required' => 'Polje "Kada se koristi privatno vozilo" je obavezno',
                'for_invoicing_apartment.required' => 'Polje "Za fakturisanje (stan)" je obavezno',
                'for_invoicing_house.required' => 'Polje "Za fakturisanje (kuća)" je obavezno',
            ])->validate();


        }else {

            Validator::make($request->all(), [
                'service_name' => 'required',
                'price_apartment' => 'required',
                'price_house' => 'required',
                'price_first_equipment' => 'required',
                'price_additional_equipment' => 'required',
                'price_private_vehicle' => 'required',
                'for_invoicing_apartment' => 'required',
                'for_invoicing_house' => 'required',
            ], [
                'service_name.required' => 'Polje "Ime tipa posla" je obavezno',
                'price_apartment.required' => 'Polje "Cena ako je stan" je obavezno',
                'price_house.required' => 'Polje "Cena ako je kuća" je obavezno',
                'price_first_equipment.required' => 'Polje "Cena za ugradjenu prvu opremu" je obavezno',
                'price_additional_equipment.required' => 'Polje "Cena za svaku sledecu opremu" je obavezno',
                'price_private_vehicle.required' => 'Polje "Kada se koristi privatno vozilo" je obavezno',
                'for_invoicing_apartment.required' => 'Polje "Za fakturisanje (stan)" je obavezno',
                'for_invoicing_house.required' => 'Polje "Za fakturisanje (kuća)" je obavezno',
            ])->validate();

        };

       //
        $service = new Service;
        $service->service_name = $request->input('service_name');
        $service->price_private_vehicle = $request->input('price_private_vehicle');
        $service->for_invoicing_apartment = $request->input('for_invoicing_apartment');
        $service->for_invoicing_house = $request->input('for_invoicing_house');
        $service->order_type = 'sbb_order';
        $service->service_select = strtolower(str_replace(" ","_", $request->input('service_name')));

        if(isset($request->price_fixed_checkbox)){

            $service->price_fixed = $request->input('price_fixed');

        }else {

            $service->price_apartment = $request->input('price_apartment');
            $service->price_house = $request->input('price_house');
            $service->price_first_equipment = $request->input('price_first_equipment');
            $service->price_additional_equipment = $request->input('price_additional_equipment');


        };

        $service->save();
        return redirect('/service');

    }

    public function store_reclamation(Request $request)
    {

        Validator::make($request->all(), [
            'service_name' => 'required',
        ], [
            'service_name.required' => 'Polje "Ime tipa posla" je obavezno',
        ])->validate();
        //dd($request->all());
        $service = new Service;
        $service->service_name = $request->input('service_name');
        $service->order_type = 'reclamation';
        $service->service_select = strtolower(str_replace(" ","_", $request->input('service_name')));
        $service->save();
        return redirect('/service');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if($id=="update_sbb") {

            $service = Service::find($request->input('service_id'));
            $service->service_name = $request->input('service_name');
            $service->price_private_vehicle = $request->input('price_private_vehicle');
            $service->for_invoicing_apartment = $request->input('for_invoicing_apartment');
            $service->for_invoicing_house = $request->input('for_invoicing_house');

            if ($service->price_fixed != null) {

                $service->price_fixed = $request->input('price_fixed');

            } else {
                $service->price_apartment = $request->input('price_apartment');
                $service->price_house = $request->input('price_house');
                $service->price_first_equipment = $request->input('price_first_equipment');
                $service->price_additional_equipment = $request->input('price_additional_equipment');
            };

            $service->save();
            return redirect('/service');


        }elseif ($id=="update_reclamation"){

            $service = Service::find($request->input('service_id'));
            $service->price_private_vehicle = $request->input('price_private_vehicle');
            $service->for_invoicing = $request->input('for_invoicing');
            $service->price_fixed = $request->input('price_fixed');

            $service->save();
            return redirect('/service');

        }elseif ($id=="update_reklamacija_po_stavkama"){


            $service = Service::find($request->input('service_id'));
            $service->price_private_vehicle = $request->input('price_private_vehicle');

            $service->save();
            return redirect('/service');

        }elseif ($id=="update_digitization_type"){

            $service = Service::find($request->input('service_id'));
            $service->service_name = $request->input('service_name');
            $service->price_private_vehicle = $request->input('price_private_vehicle');
            $service->for_invoicing = $request->input('for_invoicing');
            $service->price_fixed = $request->input('price_fixed');

            $service->save();
            return redirect('/service');

        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
