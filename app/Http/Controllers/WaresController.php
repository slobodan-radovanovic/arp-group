<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Wares;
use App\Warehouse;
use App\Externalwarehouse;
use Illuminate\Support\Facades\DB;
use Validator;

class WaresController extends Controller/**/
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $wares = Wares::all();
        return view('wares.index', compact('wares'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('wares.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Validator::make($request->all(), [
            'wares_code' => 'required|unique:wares,wares_code',
            'wares_name' => 'required',
            'wares_type' => 'required',
        ], [
            'wares_code.required' => 'Polje "Kod vrste materijala" je obavezno',
            'wares_code.unique' => 'Već postoji materijal sa kôdom "'.$request->input('wares_code').'"',
            'wares_name.required' => 'Polje "Ime vrste materijala" je obavezno',
            'wares_type.required' => 'Polje "Jedinica mere" je obavezno',
        ])->validate();

        DB::transaction(function() use ($request)
        {

        $wares = new Wares;
        $wares->wares_code = $request->input('wares_code');
        $wares->wares_name = $request->input('wares_name');
        $wares->wares_type = $request->input('wares_type');

        if($request->input('default') == 1){
            $wares->default = 1;
        }else{
            $wares->default = 0;
        }
        $wares->save();

        $wares_id = $wares->wares_id;

        $warehouse = new Warehouse;
        $warehouse->wares_id = $wares_id;
        $warehouse->quantity = 0;
        $warehouse->save();

        $externalwarehouse = new Externalwarehouse;
        $externalwarehouse->wares_id = $wares_id;
        $externalwarehouse->quantity = 0;
        $externalwarehouse->save();
        });

        return redirect('/wares');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
       // dd($request->all());
        /* dd($id);*/
        Validator::make($request->all(), [
            'wares_code' => 'required',
            'wares_name' => 'required',
            'wares_type_edit' => 'required',
        ], [
            'wares_code.required' => 'Polje "Kod vrste materijala" je obavezno',
            'wares_name.required' => 'Polje "Ime vrste materijala" je obavezno',
            'wares_type_edit.required' => 'Polje "Jedinica mere" je obavezno',
        ])->validate();

        $wares = Wares::find($request->input('wares_id'));
        $wares->wares_code = $request->input('wares_code');
        $wares->wares_name = $request->input('wares_name');
        $wares->wares_type = $request->input('wares_type_edit');
        $wares->default = $request->input('default_edit');
        /*dd($wares);*/
        $wares->save();
        return redirect('/wares');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function test()
    {
        $all_serials = DB::table('equipment')
            ->where('equipment_subtype_id', '=', 'subtype_19' )
            ->get();
           // ->update(['equipment_subtype_id' => 'subtype_8']);
        //->update(['votes' => 1]);

        dd($all_serials);
    }


}
