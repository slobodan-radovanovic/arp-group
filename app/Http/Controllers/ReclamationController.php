<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Select;
use App\Order;
use App\Equipment;
use Illuminate\Support\Facades\DB;
use App\Service;
use Validator;
use App\Externalwarehouse;
use App\Technician;
use App\Wares;
use Carbon\Carbon;

class ReclamationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    /*public function index()
    {
        $reclamation_orders = Order::where('order_type', 'reclamation')->orderBy('order_id', 'desc')->get();
        return view('reclamation.index', compact('reclamation_orders'));
    }*/

    public function index(Request $request)
    {
        if (empty($request->input('date_filter'))) {

            $reclamation_orders = Order::where('order_type', 'reclamation')->orderBy('order_id', 'desc')->get();
            return view('reclamation.index', compact('reclamation_orders'));

        } else {

            $date_filter = $request->input('date_filter');
            $exploded_date_filter = explode(' - ', $request->input('date_filter'));
            $exploded_start_date = explode('/', $exploded_date_filter[0]);
            $exploded_end_date = explode('/', $exploded_date_filter[1]);
            $start_date = $exploded_start_date['2'] . '-'
                . $exploded_start_date['1'] . '-'
                . $exploded_start_date['0'] . ' 00:00:01';
            $end_date = $exploded_end_date['2'] . '-'
                . $exploded_end_date['1'] . '-'
                . $exploded_end_date['0'] . ' 23:59:59';


            $reclamation_orders = Order::where([
                ['created_at', '>', $start_date],
                ['created_at', '<', $end_date],
                ['order_type', '=', 'reclamation']])->orderBy('order_id', 'desc')->get();
            return view('reclamation.index', compact('reclamation_orders', 'date_filter'));

        }


    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $city = createValueArray(Select::where('select_name', 'city')->where('flag', '1')->pluck('select_value'));
        $township = createValueArray(Select::where('select_name', 'township')->where('flag', '1')->pluck('select_value'));
        $region = createValueArray(Select::where('select_name', 'region')->where('flag', '1')->pluck('select_value'));
        $services = createKeyArray(DB::table('services')->where('order_type', 'reclamation')->pluck('service_name', 'service_select'));

        $subtype = createKeyArray(DB::table('equipment_subtype')
            ->rightJoin('equipment', 'equipment_subtype.equipment_subtype_id', '=', 'equipment.equipment_subtype_id')
            ->where('equipment_status', 'Kod tehničara')
            ->pluck('equipment_subtype.equipment_subtype_name', 'equipment_subtype.equipment_subtype_id'));
        $all_serials = createAllSerialsArray(DB::table('equipment')->select('equipment_id', 'equipment_serial1', 'equipment_serial2', 'equipment_serial3')
            ->where('equipment_status', 'Kod tehničara')
            ->get());

        $all_wares = Wares::all();
        $default_wares = $all_wares->where('default', 1);
        $wares = createWaresArray($all_wares->where('default', 0));
        $technicians = createKeyArray(Technician::where('active', 1)->pluck('technician_name', 'technician_id'), 'technician_');
        $equipment_for_select = DB::table('equipment_subtype')
            ->join('equipment', 'equipment_subtype.equipment_subtype_id', '=', 'equipment.equipment_subtype_id')
            ->select('equipment.equipment_id', 'equipment_subtype.equipment_subtype_id', 'equipment_subtype.equipment_subtype_name', 'equipment.equipment_serial1', 'equipment.equipment_serial2', 'equipment.equipment_serial3')->where('equipment.equipment_status', 'Kod tehničara')->get();

        $data = [
            'city' => $city,
            'township' => $township,
            'region' => $region,
            'services' => $services,
            'subtype' => $subtype,
            'all_serials' => $all_serials,
            'equipment_for_select' => $equipment_for_select,
            'default_wares' => $default_wares,
            'wares' => $wares,
            'technicians' => $technicians
        ];

        return view('reclamation.create', compact('data'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store_old(Request $request)
    {

        Validator::make($request->all(), [
            'order_number' => 'required|unique:orders',
            'address' => 'required',
            'address_number' => 'required',
            'service_type' => 'required',
        ], [
            'order_number.required' => 'Polje "SR broj naloga" je obavezno',
            'order_number.unique' => 'Nalog sa upisanim SR broj-em već postoji',
            'address.required' => 'Polje "Adresa" je obavezno',
            'address_number.required' => 'Polje "broj" (Adresa) je obavezno',
            'service_type.required' => 'Polje "Tip posla" je obavezno, izaberite tip posla',
        ])->validate();

       // dd($request->all());
        $new_ordinal_number = newReclamationOrdinalNumber();

        $reclamation = new Order;
        $reclamation->ordinal_number = $new_ordinal_number;
        $reclamation->order_number = $request->input('order_number');
        $reclamation->order_type = 'reclamation';
        $reclamation->order_status = 'Otvoren';
        $reclamation->buyer_id = $request->input('buyer_id');
        $reclamation->treaty_id = $request->input('treaty_id');
        $reclamation->buyer = $request->input('buyer');
        $reclamation->area_code = $request->input('area_code');
        $reclamation->city = $request->input('city');
        $reclamation->region = $request->input('region');
        $reclamation->township = $request->input('township');
        $reclamation->address = $request->input('address');
        $reclamation->address_number = $request->input('address_number');
        $reclamation->floor = $request->input('floor');
        $reclamation->apartment = $request->input('apartment');
        $reclamation->phone_number = $request->input('phone_number');
        $reclamation->mobile_number = $request->input('mobile_number');
        $reclamation->service_type = $request->input('service_type');
        $reclamation->note = $request->input('order_comment');

        $reclamation->save();

        return redirect('/reclamation');
    }

    public function store(Request $request)
    {
       // dd($request->all());

        Validator::make($request->all(), [
            /*'buyer' => 'required',*/
            'city' => 'required',
            'address' => 'required',
            'address_number' => 'required',
           /* 'phone_number' => 'required',*/
            'service_type' => 'required',
            'technician_1' => 'required',
            'completed_at' => 'required',
        ], [
            /*'buyer.required' => 'Polje "Ime i prezime kupca" je obavezno',*/
            'city.required' => 'Polje "Grad" je obavezno',
            'address.required' => 'Polje "Adresa" je obavezno',
            'address_number.required' => 'Polje "broj" (Adresa) je obavezno',
            /*'phone_number.required' => 'Polje "Broj telefona" je obavezno',*/
            'service_type.required' => 'Polje "Tip reklamacije" je obavezno',
            'technician_1.required' => 'Polje "Prvi tehničar" je obavezno',
            'completed_at.required' => 'Polje "Datum i vreme zatvaranja naloga" je obavezno',
        ])->validate();

        $service_type = $request->input('service_type');

        if($service_type == 'reklamacija_po_stavkama'){
            Validator::make($request->all(), [
                'price_fixed' => 'required',
                'for_invoicing' => 'required',
            ], [
                'price_fixed.required' => 'Polje "Ukupna cena za tehnicara/tehnicare" je obavezno',
                'for_invoicing.required' => 'Polje "Ukupna cena za fakturaisanje" je obavezno',
            ])->validate();
        }

        if($service_type == 'izgradnja_po_etazi'){
            Validator::make($request->all(), [
                'multiplication_number' => 'required',
            ], [
                'multiplication_number.required' => 'Polje "Broj etaža" je obavezno'
            ])->validate();
        }

        if($service_type == 'rekonstrukcija_korisnika'){
            Validator::make($request->all(), [
                'multiplication_number' => 'required',
            ], [
                'multiplication_number.required' => 'Polje "Broj korisnika" je obavezno'
            ])->validate();
        }

        if($service_type == 'izgradnja_po_metru'){
            Validator::make($request->all(), [
                'multiplication_number' => 'required',
            ], [
                'multiplication_number.required' => 'Polje "Broj metara" je obavezno'
            ])->validate();
        }

        DB::transaction(function () use ($request, $service_type) {

            $new_ordinal_number = newReclamationOrdinalNumber();

            //promena statusa opreme

            $equipment_count = 0; //broj opreme koja je ugradjena

            for ($i = 0; $i <= 3; $i++) {
                if ($request->input('equipment_serial_' . $i) != null) {
                    //$text .= explode('_', $request->input('equipment_serial_' . $i))['1'] . "  ";
                    $equipment_id = explode('_', $request->input('equipment_serial_' . $i))['1'];
                    $equipment = Equipment::find($equipment_id);

                    $equipment->equipment_status = "Kod kupca";
                    $equipment->equipment_ordinal_number = $new_ordinal_number;
                    $equipment->save();
                    ++$equipment_count;
                    //dd($equipment);
                }
            }


            // upisivanje upotrebljenog materijala i skidanje sa stanja iz spoljnog magacina
            foreach ($request->all() as $wares_id => $quantity) {
                if (strpos($wares_id, 'wares_') === 0 and $quantity != null) {
                    $wares_id = explode('_', $wares_id)[1];
                    DB::table('used_wares')->insert(
                        ['ordinal_number' => $new_ordinal_number,
                            'wares_id' => $wares_id,
                            'quantity' => $quantity,
                            'created_at' => date('Y-m-d H:i:s'),
                            'completed_at' => $request->input('completed_at')
                        ]);
                    Externalwarehouse::where('wares_id', $wares_id)->decrement('quantity', $quantity);
                }
            }


            //upisivanje tehnicara koji su radili
            if ($request->input('technician_1') != null) {

                DB::table('order_technician')->insert(
                    ['ordinal_number' => $new_ordinal_number,
                        'technician_id' => explode('_', $request->input('technician_1'))[1],
                        'created_at' => date('Y-m-d H:i:s'),
                    ]);
            }

            if ($request->input('technician_2') != null) {
                DB::table('order_technician')->insert(
                    ['ordinal_number' => $new_ordinal_number,
                        'technician_id' => explode('_', $request->input('technician_2'))[1],
                        'created_at' => date('Y-m-d H:i:s'),
                    ]);
            }

            // create technician_report

            $service = DB::table('services')
                ->where('service_select', $service_type)
                ->get();

            //upisivanje u tebelu finansijjski izvestaj



            if($service_type == 'reklamacija'){
                DB::table('finance_report')->insert(
                    [   'order_type' => 'Reklamacija',
                        'ordinal_number' => $new_ordinal_number,
                        'service_name' => $service['0']->service_name,
                        'sum' => $service['0']->for_invoicing,
                        'completed_at' => $request->input('completed_at')

                    ]);
            } elseif($service_type == 'reklamacija_po_stavkama'){

                //za ovaj tip naloga se upisuje za svaki nalog posebno

                DB::table('finance_report')->insert(
                    [   'order_type' => 'reclamation',
                        'ordinal_number' => $new_ordinal_number,
                        'service_name' => $service_type,
                        'sum' => $request->input('for_invoicing'),
                        'completed_at' => $request->input('completed_at')
                    ]);
            }else{
                $multiplication_number = $request->input('multiplication_number');
                $sum = $multiplication_number * $service['0']->for_invoicing;

                DB::table('finance_report')->insert(
                    [   'order_type' => 'reclamation',
                        'ordinal_number' => $new_ordinal_number,
                        'service_name' => $service_type,
                        'sum' => $sum,
                        'completed_at' => $request->input('completed_at')
                    ]);
            }


            //izracunavanje za isvestaj

            //da li je korisceno privatno vizilo

           // dd($request->input('private_vehicle'));

            $technicians_report_sum = 0;


            if ($request->input('private_vehicle') == '1') {
                $technicians_report_private_vehicle = $service['0']->price_private_vehicle;
            } else {
                $technicians_report_private_vehicle = null;
            }
            //dd($technicians_report_private_vehicle);


            if($service_type == 'reklamacija'){
                $technicians_report_price_fixed = $service['0']->price_fixed;
            } elseif($service_type == 'reklamacija_po_stavkama'){
                //za ovaj tip naloga se upisuje za svaki nalog posebno
                $technicians_report_price_fixed = $request->input('price_fixed');
            }else{
                $multiplication_number = $request->input('multiplication_number');
                $technicians_report_price_fixed = $multiplication_number * $service['0']->price_fixed;
            }

            $technicians_report_sum = $technicians_report_price_fixed;


            if ($request->input('technician_2') != null) {
                //ako su dva tehnicara, upisuju se dva sloga u bazu i deli se suma na 2

                //za drugog je private_vehicle null
                DB::table('technicians_report')->insert(
                    ['technician_id' => explode('_', $request->input('technician_2'))[1],
                        'order_type' => 'reclamation',
                        'ordinal_number' => $new_ordinal_number,
                        'service_name' => $service_type,
                        'price_apartment' => null,
                        'price_house' => null,
                        'price_first_equipment' => null,
                        'price_additional_equipment' => null,
                        'price_fixed' => $technicians_report_price_fixed,
                        'price_private_vehicle' => null,
                        'sum' => $technicians_report_sum / 2,
                        'created_at' => date('Y-m-d H:i:s'),
                        'completed_at' => $request->input('completed_at')
                    ]);


                if ($technicians_report_private_vehicle != null) {
                    $technicians_report_sum = ($technicians_report_sum / 2) + $technicians_report_private_vehicle;
                } else {
                    $technicians_report_sum = $technicians_report_sum / 2;
                }

                DB::table('technicians_report')->insert(
                    ['technician_id' => explode('_', $request->input('technician_1'))[1],
                        'order_type' => 'reclamation',
                        'ordinal_number' => $new_ordinal_number,
                        'service_name' => $service_type,
                        'price_apartment' => null,
                        'price_house' => null,
                        'price_first_equipment' => null,
                        'price_additional_equipment' => null,
                        'price_fixed' => $technicians_report_price_fixed,
                        'price_private_vehicle' => $technicians_report_private_vehicle,
                        'sum' => $technicians_report_sum,
                        'created_at' => date('Y-m-d H:i:s'),
                        'completed_at' => $request->input('completed_at')
                    ]);


            } else {


                if ($technicians_report_private_vehicle != null) {
                    $technicians_report_sum += $technicians_report_private_vehicle;
                }

                DB::table('technicians_report')->insert(
                    ['technician_id' => explode('_', $request->input('technician_1'))[1],
                        'order_type' => 'reclamation',
                        'ordinal_number' => $new_ordinal_number,
                        'service_name' => $service_type,
                        'price_apartment' => null,
                        'price_house' => null,
                        'price_first_equipment' => null,
                        'price_additional_equipment' => null,
                        'price_fixed' => $technicians_report_price_fixed,
                        'price_private_vehicle' => $technicians_report_private_vehicle,
                        'sum' => $technicians_report_sum,
                        'created_at' => date('Y-m-d H:i:s'),
                        'completed_at' => $request->input('completed_at')
                    ]);
            }

            // insert reclamation order

            $reclamation = new Order;
            $reclamation->ordinal_number = $new_ordinal_number;
            $reclamation->order_type = 'reclamation';
            $reclamation->order_status = 'Završen';
            $reclamation->buyer_id = $request->input('buyer_id');
            $reclamation->treaty_id = $request->input('treaty_id');
            $reclamation->buyer = $request->input('buyer');
            $reclamation->area_code = $request->input('area_code');
            $reclamation->city = $request->input('city');
            $reclamation->township = $request->input('township');
            $reclamation->address = $request->input('address');
            $reclamation->address_number = $request->input('address_number');
            $reclamation->floor = $request->input('floor');
            $reclamation->apartment = $request->input('apartment');
            $reclamation->phone_number = $request->input('phone_number');
            $reclamation->mobile_number = $request->input('mobile_number');
            $reclamation->service_type = $service_type;
            $reclamation->note = $request->input('order_comment');

            if ($request->input('private_vehicle') == '1') {
                $reclamation->private_vehicle = 1;
            } else {
                $reclamation->private_vehicle = 0;
            }

            $reclamation->created_at = date('Y-m-d H:i:s');
            $reclamation->completed_at =  datetimeForDatabase($request->input('completed_at'));
            //dd($reclamation);
            $reclamation->save();
        });

        $id = DB::table('orders')->orderBy('order_id', 'desc')->first()->order_id;

        //dd($id);
        return redirect('/reclamation/' . $id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

        $order = Order::find($id);

        $technicians = createKeyArray(Technician::where('active', 1)->pluck('technician_name', 'technician_id'), 'technician_');
        $all_wares = Wares::all();
        $default_wares = $all_wares->where('default', 1);
        $wares = createWaresArray($all_wares->where('default', 0));
        $service = $order->service;
        $services = createKeyArray(DB::table('services')->where('order_type', 'reclamation')->pluck('service_name', 'service_select'));

        $used_equipment = DB::table('equipment')
            ->join('equipment_subtype', 'equipment.equipment_subtype_id', '=', 'equipment_subtype.equipment_subtype_id')
            ->join('equipment_model', 'equipment.equipment_model_id', '=', 'equipment_model.equipment_model_id')
            ->select('equipment.*', 'equipment_subtype.equipment_subtype_name', 'equipment_model.equipment_model_name')
            ->where('equipment.equipment_ordinal_number', '=', $order->ordinal_number)
            ->get();

            $used_wares = DB::table('used_wares')
                ->join('wares', 'used_wares.wares_id', '=', 'wares.wares_id')
                ->select('wares.wares_code', 'wares.wares_name', 'used_wares.quantity')
                ->where('used_wares.ordinal_number', '=', $order->ordinal_number)
                ->get();

            //treba resiti preko relacije!!!!
            $order_technician = DB::table('order_technician')
                ->join('technicians', 'order_technician.technician_id', '=', 'technicians.technician_id')
                ->join('orders', 'order_technician.ordinal_number', '=', 'orders.ordinal_number')
                ->select('technicians.technician_name')
                ->where('order_technician.ordinal_number', '=', $order->ordinal_number)
                ->get();


        //dd($order_technician);
        $data = [
            'order' => $order,
            'default_wares' => $default_wares,
            'wares' => $wares,
            'technicians' => $technicians,
            'used_wares' => $used_wares,
            'used_equipment' => $used_equipment,
            'order_technician' => $order_technician,
            'service' => $service,
            'services' => $services,
        ];

        //dd($data);

        return view('reclamation.show', compact('data', 'id'));
        /* $orders = Order::orderBy('order_id', 'desc')->paginate(20);
         return view('orders.show')->with('orders', $orders);*/
    }


    public function show_order($id, $print)
    {

        dd(Auth::user());

        $township = Select::where('select_name', 'township')->pluck('select_value');
        $region = Select::where('select_name', 'region')->pluck('select_value');

        $select = [
            'township' => createValueArray($township),
            'region' => createValueArray($region),
        ];

        $order = Order::find($id);

        $service = $order->service;
        //dd($order);
        $services = createKeyArray(DB::table('services')->where('order_type', 'sbb_order')->pluck('service_name', 'service_select'));
        $service_name = $order->service->service_name;


        $subtype = createKeyArray(DB::table('equipment_subtype')
            ->rightJoin('equipment', 'equipment_subtype.equipment_subtype_id', '=', 'equipment.equipment_subtype_id')
            ->where('equipment_status', 'Kod tehničara')
            ->pluck('equipment_subtype.equipment_subtype_name', 'equipment_subtype.equipment_subtype_id'));


        $all_serials = createAllSerialsArray(DB::table('equipment')->select('equipment_id', 'equipment_serial1', 'equipment_serial2', 'equipment_serial3')
            ->where('equipment_status', 'Kod tehničara')
            ->get());

        $planned_mounted = DB::table('planned_equipment')
            ->join('equipment_subtype', 'planned_equipment.planned_equipment_subtype', '=', 'equipment_subtype.equipment_subtype_id')
            ->select('equipment_subtype.equipment_subtype_name')
            ->where('planned_equipment.planned_equipment_status', '=', 'mounted')
            ->where('planned_equipment.planned_equipment_ordinal_number', '=', $order->ordinal_number)
            ->get();

        $planned_dismantled = DB::table('planned_equipment')
            ->join('dismantled_eq_subtype', 'planned_equipment.planned_equipment_subtype', '=', 'dismantled_eq_subtype.dismantled_eq_subtype_id')
            ->select('dismantled_eq_subtype.dismantled_eq_subtype_name')
            ->where('planned_equipment.planned_equipment_status', '=', 'dismantled')
            ->where('planned_equipment.planned_equipment_ordinal_number', '=', $order->ordinal_number)
            ->get();

        //dd($planned_dismantled->count());

        $technicians = createKeyArray(Technician::where('active', 1)->pluck('technician_name', 'technician_id'), 'technician_');
        $all_wares = Wares::all();
        $default_wares = $all_wares->where('default', 1);
        $wares = createWaresArray($all_wares->where('default', 0));
        $dismantled_subtype = createKeyArray(DB::table('dismantled_eq_subtype')->pluck('dismantled_eq_subtype_name', 'dismantled_eq_subtype_id'));
        $dismantled_model = createKeyArray(DB::table('dismantled_eq_model')->pluck('dismantled_eq_model_name', 'dismantled_eq_model_id'));

        $equipment_for_select = DB::table('equipment_subtype')
            ->join('equipment', 'equipment_subtype.equipment_subtype_id', '=', 'equipment.equipment_subtype_id')
            ->select('equipment.equipment_id', 'equipment_subtype.equipment_subtype_id', 'equipment_subtype.equipment_subtype_name', 'equipment.equipment_serial1', 'equipment.equipment_serial2', 'equipment.equipment_serial3')->where('equipment.equipment_status', 'Kod tehničara')->get();

        $dismantled_eq_for_select = DB::table('dismantled_eq_subtype')
            ->join('dismantled_eq_model', 'dismantled_eq_subtype.dismantled_eq_subtype_id', '=', 'dismantled_eq_model.dismantled_eq_subtype_id')
            ->select('dismantled_eq_subtype.dismantled_eq_subtype_id', 'dismantled_eq_subtype.dismantled_eq_subtype_name', 'dismantled_eq_model.dismantled_eq_model_id', 'dismantled_eq_model.dismantled_eq_model_name')->get();

        if ($order->order_status == 'Nerealizovan' or $order->order_status == 'Završen') {

            $used_equipment = DB::table('equipment')
                ->join('equipment_subtype', 'equipment.equipment_subtype_id', '=', 'equipment_subtype.equipment_subtype_id')
                ->join('equipment_model', 'equipment.equipment_model_id', '=', 'equipment_model.equipment_model_id')
                ->select('equipment.*', 'equipment_subtype.equipment_subtype_name', 'equipment_model.equipment_model_name', 'equipment_model.equipment_code')
                ->where('equipment.equipment_ordinal_number', '=', $order->ordinal_number)
                ->get();


            $used_wares = DB::table('used_wares')
                ->join('wares', 'used_wares.wares_id', '=', 'wares.wares_id')
                ->select('wares.wares_code', 'wares.wares_name', 'used_wares.quantity')
                ->where('used_wares.ordinal_number', '=', $order->ordinal_number)
                ->get();

            $removed_equipment = DB::table('removed_equipment')
                ->join('dismantled_eq_subtype', 'removed_equipment.removed_equipment_subtype', '=', 'dismantled_eq_subtype.dismantled_eq_subtype_id')
                ->join('dismantled_eq_model', 'removed_equipment.removed_equipment_model', '=', 'dismantled_eq_model.dismantled_eq_model_id')
                ->select('dismantled_eq_subtype.dismantled_eq_subtype_name', 'dismantled_eq_model.dismantled_eq_model_name', 'dismantled_eq_model.equipment_code', 'removed_equipment.removed_equipment_serial1', 'removed_equipment.removed_equipment_serial2')
                ->where('removed_equipment.ordinal_number', '=', $order->ordinal_number)
                ->get();

            //treba resiti preko relacije!!!!
            $order_technician = DB::table('order_technician')
                ->join('technicians', 'order_technician.technician_id', '=', 'technicians.technician_id')
                ->join('orders', 'order_technician.ordinal_number', '=', 'orders.ordinal_number')
                ->select('technicians.technician_name')
                ->where('order_technician.ordinal_number', '=', $order->ordinal_number)
                ->get();

        } else {
            $used_equipment = '';
            $used_wares = '';
            $removed_equipment = '';
            $order_technician = '';
        }

        $date = date('Y-m-d H:i:s');
        //dd($order_technician);
        $data = [
            'order' => $order,
            'subtype' => $subtype,
            'default_wares' => $default_wares,
            'wares' => $wares,
            'dismantled_subtype' => $dismantled_subtype,
            'dismantled_model' => $dismantled_model,
            'all_serials' => $all_serials,
            'technicians' => $technicians,
            'planned_mounted' => $planned_mounted,
            'planned_dismantled' => $planned_dismantled,
            'equipment_for_select' => $equipment_for_select,
            'dismantled_eq_for_select' => $dismantled_eq_for_select,
            'used_equipment' => $used_equipment,
            'used_wares' => $used_wares,
            'removed_equipment' => $removed_equipment,
            'order_technician' => $order_technician,
            'service' => $service,
            'services' => $services,
            'service_name' => $service_name,
            'date' => $date,

        ];

        // dd($data);

        if ($print == 'print') {
            return view('print.open_order', compact('data', 'id'));
        }

        return view('reclamation.show', compact('data', 'select', 'id'));

    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //dd($request->all());
        // dd($id);


        if ($request->input('order_status') == "Otvoren"
            or $request->input('order_status') == "Odložen"
            or $request->input('order_status') == "Nekompletan") {
            $order = Order::find($id);
            $order->order_status = $request->input('order_status');
            $order->note = $request->input('order_comment');
            $order->save();
            return redirect('/reclamation/' . $id);
        } elseif ($request->input('order_status') == "Zakazan") {
            //dd($request->all());
            $order = Order::find($id);
            $order->order_status = $request->input('order_status');
            $order->scheduled_at = datetimeForDatabase($request->input('scheduled_at_datetime'));
            $order->note = $request->input('order_comment');
            $order->save();
            return redirect('/reclamation/' . $id);
        } elseif ($request->input('order_status') == "Otkazan") {
            //dd($request->all());
            $order = Order::find($id);
            if ($order->completed_at == null) {
                $order->completed_at = date('Y-m-d H:i:s');
            }
            $order->order_status = $request->input('order_status');
            $order->note = $request->input('order_comment');
            $order->save();
            return redirect('/reclamation/' . $id);
        } elseif ($request->input('order_status') == "Nerealizovan") {
            Validator::make($request->all(), [
                'poruka' => 'required',

            ], [
                'poruka.required' => 'Nismo jos definisali kako ce se ponasati za status nerealizovan kada je reklamacija u pitanju',

            ])->validate();

            /*Validator::make($request->all(), [
                'technician_1' => 'required',
                'service_home' => 'required',
            ], [
                'technician_1.required' => 'Polje "Prvi tehničar" je obavezno',
                'service_home.required' => 'Polje "Vrsta objekta" je obavezno',
            ])->validate();;


            $order = Order::find($id);
            if ($order->completed_at == null) {


                DB::transaction(function () use ($request, $order) {
                    //promena statusa opreme
                    for ($i = 0; $i <= 5; $i++) {
                        if ($request->input('equipment_serial_' . $i) != null) {
                            // $text .= explode('_', $request->input('equipment_serial_' . $i))['1'] . "  ";
                            $equipment_id = explode('_', $request->input('equipment_serial_' . $i))['1'];
                            $equipment = Equipment::find($equipment_id);

                            $equipment->equipment_status = "Kod kupca";
                            $equipment->equipment_ordinal_number = $order->ordinal_number;
                            $equipment->save();
                            //dd($equipment);
                        }
                    }

                    //upisivanje demontirane opreme
                    for ($i = 1; $i <= 5; $i++) {
                        if ($request->input('dismantled_eq' . $i . '_subtype') != null
                            and $request->input('dismantled_eq' . $i . '_serial1') != null) {
                            DB::table('removed_equipment')->insert(
                                ['ordinal_number' => $order->ordinal_number,
                                    'removed_equipment_subtype' => $request->input('dismantled_eq' . $i . '_subtype'),
                                    'removed_equipment_serial1' => $request->input('dismantled_eq' . $i . '_serial1'),
                                    'removed_equipment_serial2' => $request->input('dismantled_eq' . $i . '_serial2'),
                                    'created_at' => date('Y-m-d H:i:s'),
                                ]);
                        }
                    }

                    // upisivanje upotrebljenog materijala i skidanje sa stanja iz spoljnog magacina
                    foreach ($request->all() as $wares_id => $quantity) {
                        if (strpos($wares_id, 'wares_') === 0 and $quantity != null) {
                            $wares_id = explode('_', $wares_id)[1];
                            DB::table('used_wares')->insert(
                                ['ordinal_number' => $order->ordinal_number,
                                    'wares_id' => $wares_id,
                                    'quantity' => $quantity,
                                    'created_at' => date('Y-m-d H:i:s'),
                                ]);
                            Externalwarehouse::where('wares_id', $wares_id)->decrement('quantity', $quantity);
                        }
                    }


                    //upisivanje tehnicara koji su radili
                    if ($request->input('technician_1') != null) {

                        DB::table('order_technician')->insert(
                            ['ordinal_number' => $order->ordinal_number,
                                'technician_id' => explode('_', $request->input('technician_1'))[1],
                                'created_at' => date('Y-m-d H:i:s'),
                            ]);
                    }


                    if ($request->input('technician_2') != null) {

                        DB::table('order_technician')->insert(
                            ['ordinal_number' => $order->ordinal_number,
                                'technician_id' => explode('_', $request->input('technician_2'))[1],
                                'created_at' => date('Y-m-d H:i:s'),
                            ]);
                    }

                    // create technicina_report

                    $service = DB::table('services')
                        ->where('service_select', $order->service_type)
                        ->get();

                    //izracunavanje za isvestaj

                    //da li je korisceno privatno vizilo
                    if ($request->input('private_vehicle') == '1') {
                        $technicians_report_private_vehicle = $service['0']->price_private_vehicle;
                    } else {
                        $technicians_report_private_vehicle = null;
                    }


                    $technicians_report_sum = 400;

                    $technicians_report_apartment = null;
                    $technicians_report_house = null;
                    $technicians_report_first_equipment = null;
                    $technicians_report_additional_equipment = null;


                    if ($request->input('technician_2') != null) {
                        //ako su dva tehnicara, upisuju se dva sloga u bazu i deli se suma na 2

                        //za drugog je private_vehicle null
                        DB::table('technicians_report')->insert(
                            ['technician_id' => explode('_', $request->input('technician_2'))[1],
                                'order_type' => $order->order_type,
                                'ordinal_number' => $order->ordinal_number,
                                'service_name' => $order->service_type . '(Nerealizovan)',
                                'price_apartment' => $technicians_report_apartment,
                                'price_house' => $technicians_report_house,
                                'price_first_equipment' => $technicians_report_first_equipment,
                                'price_additional_equipment' => $technicians_report_additional_equipment,
                                'price_fixed' => 400,
                                'price_private_vehicle' => null,
                                'sum' => $technicians_report_sum / 2,
                                'created_at' => date('Y-m-d H:i:s'),
                            ]);


                        if ($technicians_report_private_vehicle != null) {
                            $technicians_report_sum = ($technicians_report_sum / 2) + $technicians_report_private_vehicle;
                        } else {
                            $technicians_report_sum = $technicians_report_sum / 2;
                        }

                        DB::table('technicians_report')->insert(
                            ['technician_id' => explode('_', $request->input('technician_1'))[1],
                                'order_type' => $order->order_type,
                                'ordinal_number' => $order->ordinal_number,
                                'service_name' => $order->service_type . '(Nerealizovan)',
                                'price_apartment' => $technicians_report_apartment,
                                'price_house' => $technicians_report_house,
                                'price_first_equipment' => $technicians_report_first_equipment,
                                'price_additional_equipment' => $technicians_report_additional_equipment,
                                'price_fixed' => 400,
                                'price_private_vehicle' => $technicians_report_private_vehicle,
                                'sum' => $technicians_report_sum,
                                'created_at' => date('Y-m-d H:i:s'),
                            ]);


                    } else {
                        if ($technicians_report_private_vehicle != null) {
                            $technicians_report_sum += $technicians_report_private_vehicle;
                        }


                        DB::table('technicians_report')->insert(
                            ['technician_id' => explode('_', $request->input('technician_1'))[1],
                                'order_type' => $order->order_type,
                                'ordinal_number' => $order->ordinal_number,
                                'service_name' => $order->service_type . '(Nerealizovan)',
                                'price_apartment' => $technicians_report_apartment,
                                'price_house' => $technicians_report_house,
                                'price_first_equipment' => $technicians_report_first_equipment,
                                'price_additional_equipment' => $technicians_report_additional_equipment,
                                'price_fixed' => 400,
                                'price_private_vehicle' => $technicians_report_private_vehicle,
                                'sum' => $technicians_report_sum,
                                'created_at' => date('Y-m-d H:i:s'),
                            ]);
                    }

                    // update order
                    $order->order_status = $request->input('order_status');
                    $order->service_home = $request->input('service_home');

                    if ($request->input('private_vehicle') == '1') {
                        $order->private_vehicle = 1;
                    } else {
                        $order->private_vehicle = 0;
                    }

                    $order->note = $request->input('order_comment');
                    $order->completed_at = date('Y-m-d H:i:s');

                    $order->save();

                });

            } else {
                $order->note = $request->input('order_comment');
                $order->save();
            }

            return redirect('/orders/' . $id);*/
        } elseif ($request->input('order_status') == "Završen") {

            Validator::make($request->all(), [
                'technician_1' => 'required',

            ], [
                'technician_1.required' => 'Polje "Prvi tehničar" je obavezno',

            ])->validate();

            $order = Order::find($id);
            if ($order->completed_at == null) {

                // dd($request->all());
                DB::transaction(function () use ($request, $order) {

                    $used_cable = 0;
                    // upisivanje upotrebljenog materijala i skidanje sa stanja iz spoljnog magacina
                    foreach ($request->all() as $wares_id => $quantity) {
                        if (strpos($wares_id, 'wares_') === 0 and $quantity != null) {
                            $wares_id = explode('_', $wares_id)[1];
                            $current_material = Wares::find($wares_id);
                            if($current_material->wares_type == 'm'){
                                $used_cable += $quantity;
                            }

                            DB::table('used_wares')->insert(
                                ['ordinal_number' => $order->ordinal_number,
                                    'wares_id' => $wares_id,
                                    'quantity' => $quantity,
                                    'created_at' => date('Y-m-d H:i:s'),
                                    'completed_at' => $request->input('completed_at')
                                ]);
                            Externalwarehouse::where('wares_id', $wares_id)->decrement('quantity', $quantity);
                        }
                    }

                    //upisivanje tehnicara koji su radili
                    if ($request->input('technician_1') != null) {
                        /*$text[$order->ordinal_number] = explode('_', $request->input('technician_1'))[1];*/
                        DB::table('order_technician')->insert(
                            ['ordinal_number' => $order->ordinal_number,
                                'technician_id' => explode('_', $request->input('technician_1'))[1],
                                'created_at' => date('Y-m-d H:i:s'),
                            ]);
                    }


                    if ($request->input('technician_2') != null) {
                        /* $text[$order->ordinal_number] = explode('_', $request->input('technician_2'))[1];*/
                        DB::table('order_technician')->insert(
                            ['ordinal_number' => $order->ordinal_number,
                                'technician_id' => explode('_', $request->input('technician_2'))[1],
                                'created_at' => date('Y-m-d H:i:s'),
                            ]);
                    }

                    // create technicina_report

                    $service = DB::table('services')
                        ->where('service_select', 'reklamacija')
                        ->get();

                    DB::table('finance_report')->insert(
                        [   'order_type' => 'Reklamacija',
                            'ordinal_number' => $order->ordinal_number,
                            'service_name' => $order->service_type,
                            'sum' => $service['0']->for_invoicing,
                            'completed_at' => $request->input('completed_at')
                        ]);

                    //izracunavanje za isvestaj

                    //da li je korisceno privatno vizilo
                    if ($request->input('private_vehicle') == '1') {
                        $technicians_report_private_vehicle = $service['0']->price_private_vehicle;
                    } else {
                        $technicians_report_private_vehicle = null;
                    }


                    $technicians_report_sum = 0;

                    if ($used_cable < $service['0']->price_apartment){
                        $technicians_report_sum = $service['0']->price_fixed;
                        $technicians_report_price_fixed = $service['0']->price_fixed;
                    } else {
                        $technicians_report_sum = $used_cable * $service['0']->price_house;
                        $technicians_report_price_fixed = null;
                    }

                        $technicians_report_apartment = null;
                        $technicians_report_house = null;
                        $technicians_report_first_equipment = null;
                        $technicians_report_additional_equipment = null;


                    if ($request->input('technician_2') != null) {
                        //ako su dva tehnicara, upisuju se dva sloga u bazu i deli se suma na 2

                        //za drugog je private_vehicle null
                        DB::table('technicians_report')->insert(
                            ['technician_id' => explode('_', $request->input('technician_2'))[1],
                                'order_type' => $order->order_type,
                                'ordinal_number' => $order->ordinal_number,
                                'service_name' => $order->service_type,
                                'price_apartment' => $technicians_report_apartment,
                                'price_house' => $technicians_report_house,
                                'price_first_equipment' => $technicians_report_first_equipment,
                                'price_additional_equipment' => $technicians_report_additional_equipment,
                                'price_fixed' => $technicians_report_price_fixed,
                                'price_private_vehicle' => null,
                                'sum' => $technicians_report_sum / 2,
                                'created_at' => date('Y-m-d H:i:s'),
                                'completed_at' => $request->input('completed_at')
                            ]);


                        if ($technicians_report_private_vehicle != null) {
                            $technicians_report_sum = ($technicians_report_sum / 2) + $technicians_report_private_vehicle;
                        } else {
                            $technicians_report_sum = $technicians_report_sum / 2;
                        }

                        DB::table('technicians_report')->insert(
                            ['technician_id' => explode('_', $request->input('technician_1'))[1],
                                'order_type' => $order->order_type,
                                'ordinal_number' => $order->ordinal_number,
                                'service_name' => $order->service_type,
                                'price_apartment' => $technicians_report_apartment,
                                'price_house' => $technicians_report_house,
                                'price_first_equipment' => $technicians_report_first_equipment,
                                'price_additional_equipment' => $technicians_report_additional_equipment,
                                'price_fixed' => $technicians_report_price_fixed,
                                'price_private_vehicle' => $technicians_report_private_vehicle,
                                'sum' => $technicians_report_sum,
                                'created_at' => date('Y-m-d H:i:s'),
                                'completed_at' => $request->input('completed_at')
                            ]);


                    } else {
                        if ($technicians_report_private_vehicle != null) {
                            $technicians_report_sum += $technicians_report_private_vehicle;
                        }


                        DB::table('technicians_report')->insert(
                            ['technician_id' => explode('_', $request->input('technician_1'))[1],
                                'order_type' => $order->order_type,
                                'ordinal_number' => $order->ordinal_number,
                                'service_name' => $order->service_type,
                                'price_apartment' => $technicians_report_apartment,
                                'price_house' => $technicians_report_house,
                                'price_first_equipment' => $technicians_report_first_equipment,
                                'price_additional_equipment' => $technicians_report_additional_equipment,
                                'price_fixed' => $technicians_report_price_fixed,
                                'price_private_vehicle' => $technicians_report_private_vehicle,
                                'sum' => $technicians_report_sum,
                                'created_at' => date('Y-m-d H:i:s'),
                                'completed_at' => $request->input('completed_at')
                            ]);
                    }

                    // update order
                    $order->order_status = $request->input('order_status');
                    //$order->service_home = $request->input('service_home');

                    if ($request->input('private_vehicle') == '1') {
                        $order->private_vehicle = 1;
                    } else {
                        $order->private_vehicle = 0;
                    }

                    $order->note = $request->input('order_comment');
                    $order->completed_at = date('Y-m-d H:i:s');

                    $order->save();

                });

            } else {
                $order->note = $request->input('order_comment');
                $order->save();
            }

            return redirect('/reclamation/' . $id);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
