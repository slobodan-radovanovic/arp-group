<?php

namespace App\Http\Middleware;

use Closure;

class isKordinator
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    protected function redirectTo()
    {
        if (Auth::user()->role != 'Kordinator') {
            return route('login');
        }
    }
}
