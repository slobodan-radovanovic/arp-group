<?php

namespace App;


use Illuminate\Database\Eloquent\Model;

class Type extends Model
{
    public $table = 'equipment_type';
    protected $primaryKey = 'equipment_type_id';

    public function equipment()
    {
        return $this->hasOne(Equipment::class, 'equipment_type_id', 'equipment_type_id');
    }

}
