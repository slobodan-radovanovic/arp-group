<?php

namespace App;


use Illuminate\Database\Eloquent\Model;

class DischargedEquipment extends Model
{
    public $table = 'discharged_equipment';
    protected $primaryKey = 'discharged_equipment_id';

    public function equipment()
    {
        return $this->hasOne(Equipment::class, 'equipment_id', 'equipment_id');
    }

    public function technician()
    {
        return $this->belongsTo(Technician::class, 'equipment_technician', 'technician_id');
    }



}
