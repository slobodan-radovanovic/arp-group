<?php
use Carbon\Carbon;
use App\Equipment;
use App\Externalwarehouse;
use App\Service;
use App\Order;
use Illuminate\Support\Facades\DB;

function createKeyArray($array, $prefix = null){
    $select_array = [];

    foreach ($array as $key => $value){
        if($prefix != null){
            $newkey = $prefix.$key;
        } else{
            $newkey = $key;
        }
        $select_array[$newkey] = $value;
    }
    return $select_array;
}



function createValueArray($array){
    $select_array = [];
    foreach ($array as $value){
        $select_array[$value] = $value;
    }
    return $select_array;
}



function createAllSerialsArray($collection){
    $select_array = [];
    foreach ($collection as $array){
        $id = $array->equipment_id;
        foreach ($array as $key => $value){
            if(strpos($key, 'serial')> 0){
                $key_exploded = explode('_', $key);
                if ($value != null){
                    $select_array[$key_exploded[1].'_'.$id] = $value;
                }
            }
        }
    }
    return $select_array;
}



function createWaresArray($collection){
    $wares_array = [];

    foreach ($collection as $array){
        $id = $array->wares_id;
        $wares_code = $array->wares_code;
        $wares_name = $array->wares_name;
        $wares_type = $array->wares_type;

        $wares_array['wares_'.$id]=$wares_code.' - '.$wares_name.' ('.$wares_type.')';
    }

    return $wares_array;
}



function createAssignmentWaresArray($collection){
    $wares_array = [];

    foreach ($collection as $array){
        $wares_id = $array->wares_id;
        $warehouse_id = $array->warehouse_id;
        $wares_code = $array->wares_code;
        $wares_name = $array->wares_name;
        $wares_type = $array->wares_type;
        $quantity = $array->quantity;

        $wares_array['wares_'.$wares_id.'_'.$warehouse_id]=$wares_code.' - '.$wares_name.' (Dostupno: '.$quantity.$wares_type.')';
    }

    return $wares_array;
}


/*function datetimeForView($datetime){
    $exploded_datetime = explode( ' ', $datetime);
    $exploded_date = explode( '-', $exploded_datetime[0]);
    $exploded_time = explode( ':', $exploded_datetime[1]);
    $datetimeForView = $exploded_date[2].'/'.$exploded_date[1].'/'.$exploded_date[0].'  '.$exploded_time[0].':'.$exploded_time[1];
    return $datetimeForView;

}*/




function datetimeForView($datetime){
    $exploded_datetime = explode( ' ', $datetime);
    $exploded_date = explode( '-', $exploded_datetime[0]);
    $exploded_time = explode( ':', $exploded_datetime[1]);
    $datetimeForView = $exploded_date[0].'-'.$exploded_date[1].'-'.$exploded_date[2].'  '.$exploded_time[0].':'.$exploded_time[1];
    return $datetimeForView;
    /*if(isset($exploded_datetime[1])){
        return $exploded_datetime[0].' '.$exploded_datetime[1];
    }else{
        return '111111111';
    }*/

    /*return $datetime;*/
}

function datetimeForSort($datetime){
    $exploded_datetime = explode( ' ', $datetime);
    $exploded_date = explode( '-', $exploded_datetime[0]);
    $exploded_time = explode( ':', $exploded_datetime[1]);
    $datetimeForView = $exploded_date[0].'-'.$exploded_date[1].'-'.$exploded_date[2].'  '.$exploded_time[0].':'.$exploded_time[1];
    return $datetimeForView;
    /*if(isset($exploded_datetime[1])){
        return $exploded_datetime[0].' '.$exploded_datetime[1];
    }else{
        return '111111111';
    }*/

    /*return $datetime;*/
}


function datetimeForDatabase($datetime){
    $exploded_datetime = explode( '  ', $datetime);
    $exploded_date = explode( '/', $exploded_datetime[0]);
    $datetimeForDatabase = $exploded_date[2].'-'.$exploded_date[1].'-'.$exploded_date[0].' '.$exploded_datetime[1].':00';
    return $datetimeForDatabase;
}

function datetimeForPicker($datetime)
{
    $exploded_datetime = explode(' ', $datetime);
    $exploded_date = explode('-', $exploded_datetime[0]);
    $exploded_time = explode(':', $exploded_datetime[1]);

    /*$datetimeForView = $exploded_date[0] . '-' . $exploded_date[1] . '-' . $exploded_date[2] . '  ' . $exploded_time[0] . ':' . $exploded_time[1];*/

    $datetimeForView = $exploded_date[2] . '/' . $exploded_date[1] . '/' . $exploded_date[0] . '  ' . $exploded_time[0] . ':' . $exploded_time[1];
    return $datetimeForView;
}


function newSbbOrdinalNumber(){


    $ordinal_number = DB::table('orders')->select('ordinal_number')->where('order_type', 'sbb_order')
        ->orderBy('order_id', 'desc')->first()/*)->ordinal_number*/;

    //dd($ordinal_number);
    $month = Carbon::now()->format('m');
    $year = Carbon::now()->format('y');

    if($ordinal_number == null){
        return $new_ordinal_number = $year.'-'.$month.'-00001';
    }else{
        $ordinal_number = $ordinal_number->ordinal_number;
    }

    $exploded_ordinal = explode( '-', $ordinal_number);

    if($month>$exploded_ordinal[1]){
        $new_ordinal_number = $year.'-'.$month.'-00001';
    }else{
        $new_order = ++$exploded_ordinal[2];

        switch (strlen($new_order)) {
            case '1':
                $order ='0000'.$new_order;
                break;
            case '2':
                $order ='000'.$new_order;
                break;
            case '3':
                $order ='00'.$new_order;
                break;
            case '4':
                $order ='0'.$new_order;
                break;
            case '5':
                $order = $new_order;
                break;
        }
         $new_ordinal_number = $year.'-'.$month.'-'.$order;
    }
    return $new_ordinal_number;
}

function newDigitizationOrdinalNumber(){

    $ordinal_number = DB::table('orders')->select('ordinal_number')->where('order_type', 'digitization')
        ->orderBy('order_id', 'desc')->first();
    //dd($ordinal_number);
    $month = Carbon::now()->format('m');
    $year = Carbon::now()->format('y');

    if($ordinal_number == null){
        return $new_ordinal_number = 'D'.$year.'-'.$month.'-00001';
    }else{
        $ordinal_number = $ordinal_number->ordinal_number;
    }


    $exploded_ordinal = explode( '-', $ordinal_number);

    if($month>$exploded_ordinal[1]){
        $new_ordinal_number = 'D'.$year.'-'.$month.'-00001';
    }else{
        $new_order = ++$exploded_ordinal[2];

        switch (strlen($new_order)) {
            case '1':
                $order ='0000'.$new_order;
                break;
            case '2':
                $order ='000'.$new_order;
                break;
            case '3':
                $order ='00'.$new_order;
                break;
            case '4':
                $order ='0'.$new_order;
                break;
            case '5':
                $order = $new_order;
                break;
        }
        $new_ordinal_number = 'D'.$year.'-'.$month.'-'.$order;
    }
    return $new_ordinal_number;
}

function newReclamationOrdinalNumber(){
    $ordinal_number = DB::table('orders')->select('ordinal_number')->where('order_type', 'reclamation')
        ->orderBy('order_id', 'desc')->first();
    //dd($ordinal_number);
    $month = Carbon::now()->format('m');
    $year = Carbon::now()->format('y');



    if($ordinal_number == null){

        $new_ordinal_number = 'R'.$year.'-'.$month.'-00001';
        return $new_ordinal_number;
    }else{
        $ordinal_number = $ordinal_number->ordinal_number;
    }

    $exploded_ordinal = explode( '-', $ordinal_number);

    if($month>$exploded_ordinal[1]){
        $new_ordinal_number = 'R'.$year.'-'.$month.'-00001';
    }else{
        $new_order = ++$exploded_ordinal[2];

        switch (strlen($new_order)) {
            case '1':
                $order ='0000'.$new_order;
                break;
            case '2':
                $order ='000'.$new_order;
                break;
            case '3':
                $order ='00'.$new_order;
                break;
            case '4':
                $order ='0'.$new_order;
                break;
            case '5':
                $order = $new_order;
                break;
        }
        $new_ordinal_number = 'R'.$year.'-'.$month.'-'.$order;
    }
    return $new_ordinal_number;
}

function notNullArray($request, $ordinal_number){
    $not_null_array = [];
    foreach ($request->all() as $key => $value){
         if(!is_null($request->input($key))){
             if(strpos($key,'m_eq_type')===0){
                 array_push($not_null_array,
                             ['planned_equipment_ordinal_number'=>$ordinal_number,                                                            'planned_equipment_subtype'=>$request->input($key),                                                                'planned_equipment_status'=>'mounted']);
             } elseif (strpos($key,'d_eq_type')===0){
                 array_push($not_null_array,
                     ['planned_equipment_ordinal_number'=>$ordinal_number,'planned_equipment_subtype'=>$request->input($key),                                              'planned_equipment_status'=>'dismantled']);
             }
         }

    }
    return $not_null_array;
}

function updateEquipmentStatus($request, $order)
{

    for ($i = 0; $i <= 5; $i++) {
        if ($request->input('equipment_serial_' . $i) != null) {
            // $text .= explode('_', $request->input('equipment_serial_' . $i))['1'] . "  ";
            $equipment_id = explode('_', $request->input('equipment_serial_' . $i))['1'];
            $equipment = Equipment::find($equipment_id);

            $equipment->equipment_status = "Kod kupca";
            $equipment->equipment_ordinal_number = $order->ordinal_number;
            $equipment->save();
        }
    }


}

function updateEquipmentStatusBack($order)
{
    $equipments = Equipment::where('equipment_ordinal_number', $order->ordinal_number)->get();

    foreach ($equipments as $equipment){

        $equipment->equipment_status = "Kod tehničara";
        $equipment->equipment_ordinal_number = null;
        $equipment->save();
    }
}

function getEquipmentCount($request)
{
    $equipment_count = 0;
    for ($i = 0; $i <= 5; $i++) {
        if ($request->input('equipment_serial_' . $i) != null) {
            $equipment_count++;
        }
    }

    return $equipment_count;
}


function insertRemovedEquipment($request, $order)
{

    for ($i = 1; $i <= 5; $i++) {  //dismantled_eq_subtype_id_1
        if ($request->input('dismantled_eq_subtype_id_' . $i) != null
            and $request->input('dismantled_eq_model_id_' . $i) != null
            and $request->input('dismantled_eq' . $i . '_serial1') != null) {
            DB::table('removed_equipment')->insert(
                ['ordinal_number' => $order->ordinal_number,
                    'removed_equipment_subtype' => $request->input('dismantled_eq_subtype_id_' . $i),
                    'removed_equipment_model' => $request->input('dismantled_eq_model_id_' . $i),
                    'removed_equipment_serial1' => $request->input('dismantled_eq' . $i . '_serial1'),
                    'removed_equipment_serial2' => $request->input('dismantled_eq' . $i . '_serial2'),
                    'created_at' => date('Y-m-d H:i:s'),
                ]);
        }
    }
}

function insertRemovedEquipmentBack($order)
{
    DB::table('removed_equipment')
        ->where('ordinal_number', '=', $order->ordinal_number)
        ->where('removed_equipment_status', '=', 'magacin')
        ->delete();
}


function insertUsedWares($request, $order)
{
    foreach ($request->all() as $wares_id => $quantity) {
        if (strpos($wares_id, 'wares_') === 0 and $quantity != null) {
            $wares_id = explode('_', $wares_id)[1];
            DB::table('used_wares')->insert(
                ['ordinal_number' => $order->ordinal_number,
                    'wares_id' => $wares_id,
                    'quantity' => $quantity,
                    'created_at' => date('Y-m-d H:i:s'),
                    'completed_at' => $order->completed_at
                ]);
            Externalwarehouse::where('wares_id', $wares_id)->decrement('quantity', $quantity);
        }
    }
}

function insertNotDefaultUsedWares($request, $order){

for($i = 1; $i <= 20; $i++){
        $wares_id = $request->input('not_default_select_'.$i);
        $quantity  = $request->input('not_default_input_'.$i);

        if ($wares_id != null and $quantity != null) {

            $wares_id = explode('_', $wares_id)[1];

            DB::table('used_wares')->insert(
                ['ordinal_number' => $order->ordinal_number,
                    'wares_id' => $wares_id,
                    'quantity' => $quantity,
                    'created_at' => date('Y-m-d H:i:s'),
                    'completed_at' => $order->completed_at
                ]);
            Externalwarehouse::where('wares_id', $wares_id)->decrement('quantity', $quantity);
        }
    }
}


function insertUsedWaresBack($order)
{
    $used_wares = DB::table('used_wares')->where('ordinal_number', '=', $order->ordinal_number)->get();
    foreach($used_wares as $used_ware){
        Externalwarehouse::where('wares_id', $used_ware->wares_id)->increment('quantity', $used_ware->quantity);
    }

    DB::table('used_wares')->where('ordinal_number', '=', $order->ordinal_number)->delete();
}

function insertTechnicians($request, $order)
{

    if ($request->input('technician_1') != null) {
        /*$text[$order->ordinal_number] = explode('_', $request->input('technician_1'))[1];*/
        DB::table('order_technician')->insert(
            ['ordinal_number' => $order->ordinal_number,
                'technician_id' => explode('_', $request->input('technician_1'))[1],
                'created_at' => date('Y-m-d H:i:s'),
            ]);
    }

    if ($request->input('technician_2') != null) {
        /* $text[$order->ordinal_number] = explode('_', $request->input('technician_2'))[1];*/
        DB::table('order_technician')->insert(
            ['ordinal_number' => $order->ordinal_number,
                'technician_id' => explode('_', $request->input('technician_2'))[1],
                'created_at' => date('Y-m-d H:i:s'),
            ]);
    }


}

function insertTechniciansBack($order){
    DB::table('order_technician')->where('ordinal_number', '=', $order->ordinal_number)->delete();
}

function financeReport($service, $order)
{

    //dd($order);
    if($order->service_home == 'apartment' ){
        $for_invoicing = $service['0']->for_invoicing_apartment;
    } else if($order->service_home == 'house' ) {
        $for_invoicing = $service['0']->for_invoicing_house;
    }

    DB::table('finance_report')->insert(
        ['order_type' => $order->order_type,
            'ordinal_number' => $order->ordinal_number,
            'service_name' => $order->service_type,
            'sum' => $for_invoicing,
            'completed_at' => $order->completed_at
        ]);
}


function financeReportBack($order)
{
    DB::table('finance_report')->where('ordinal_number', '=', $order->ordinal_number)->delete();
}


function isUsedPrivateVehicle($service, $request)
{
    if ($request->input('private_vehicle') == '1') {
        return $technicians_report_private_vehicle = $service['0']->price_private_vehicle;
    } else {
        return $technicians_report_private_vehicle = null;
    }
}


function techniciansReportUnrealized($order, $request, $sum, $technicians_report_private_vehicle, $service_for_report, $technicians)
{
    DB::table('technicians_report')->insert(
        ['technician_id' => explode('_', $request->input('technician_'.$technicians))[1],
            'order_type' => $order->order_type,
            'ordinal_number' => $order->ordinal_number,
            'service_name' => $order->service_type,
            'price_apartment' => $service_for_report->price_apartment,
            'price_house' => $service_for_report->price_house,
            'price_first_equipment' => $service_for_report->price_first_equipment,
            'price_additional_equipment' => $service_for_report->price_additional_equipment,
            'price_fixed' => 400,
            'price_private_vehicle' => $technicians_report_private_vehicle,
            'sum' => $sum / $technicians,
            'completed_at' => $order->completed_at,
            'created_at' => date('Y-m-d H:i:s'),
        ]);
}



function techniciansReport($order, $request, $service_for_report, $technicians)
{
    DB::table('technicians_report')->insert(
        ['technician_id' => explode('_', $request->input('technician_'.$technicians))[1],
            'order_type' => $order->order_type,
            'ordinal_number' => $order->ordinal_number,
            'service_name' => $order->service_type,
            'price_apartment' => $service_for_report->price_apartment,
            'price_house' => $service_for_report->price_house,
            'price_first_equipment' => $service_for_report->price_first_equipment,
            'price_additional_equipment' => $service_for_report->price_additional_equipment,
            'price_fixed' => $service_for_report->price_fixed,
            'price_private_vehicle' => $service_for_report->price_private_vehicle,
            'sum' => $service_for_report->sum / $technicians,
            'completed_at' => $order->completed_at,
            'created_at' => date('Y-m-d H:i:s'),
        ]);

}

function techniciansReportBack($order)
{
    DB::table('technicians_report')->where('ordinal_number', '=', $order->ordinal_number)->delete();
}

function orderCloseBack($order){
    insertRemovedEquipmentBack($order);
    updateEquipmentStatusBack($order);
    insertUsedWaresBack($order);
    insertTechniciansBack($order);
    financeReportBack($order);
    techniciansReportBack($order);
}

function updateOrderById($order_id)
{
    $order = Order::find($order_id);


    $service = Service::where('service_select', $order->service_type)
        ->get();


    DB::table('finance_report')->update(
        [
            'service_name' => $order->service_type,
            'sum' => $service['0']->for_invoicing,
        ])->where('ordinal_number', $order->ordinal_number);

}



