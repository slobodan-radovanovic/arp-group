<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Externalwarehouse extends Model
{
    public $table = "externalwarehouse";

    public function wares()
    {
        return $this->hasMany(Wares::class, 'wares_id');
    }
}
