<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Warehouse extends Model
{
    protected $primaryKey = 'warehouse_id';
    public $table = "warehouse";

    public function wares()
    {
        return $this->hasMany(Wares::class, 'wares_id');
    }
}
