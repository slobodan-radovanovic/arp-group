// Date picker
/*$('.opened_at').pickadate({
    // Strings and translations
    monthsFull: ['Januar', 'Februar', 'Mart', 'April', 'Maj', 'Jun', 'Jul', 'Avgust', 'Septembar', 'Oktobar', 'Novembar', 'Decembar'],
    monthsShort: ['Jan', 'Feb', 'Mar', 'Apr', 'Maj', 'Jun', 'Jul', 'Avg', 'Sep', 'Okt', 'Nov', 'Dec'],
    weekdaysFull: ['Nedelja', 'Ponedeljak', 'Utorak', 'Sreda', 'Četvrtak', 'Petak', 'Subota'],
    weekdaysShort: ['Ned', 'Pon', 'Uto', 'Sre', 'Čet', 'Pet', 'Sub'],
    showMonthsShort: undefined,
    showWeekdaysFull: undefined,
    // Buttons
    today: 'Danas',
    clear: 'Obriši',
    close: 'Zatvori',
    // Accessibility labels
    labelMonthNext: 'Sledeći mesec',
    labelMonthPrev: 'Prethodni mesec',
    labelMonthSelect: 'Izaberi mesec',
    labelYearSelect: 'Izaberi godinu',
});

$('.started_at').pickadate({
    // Strings and translations
    monthsFull: ['Januar', 'Februar', 'Mart', 'April', 'Maj', 'Jun', 'Jul', 'Avgust', 'Septembar', 'Oktobar', 'Novembar', 'Decembar'],
    monthsShort: ['Jan', 'Feb', 'Mar', 'Apr', 'Maj', 'Jun', 'Jul', 'Avg', 'Sep', 'Okt', 'Nov', 'Dec'],
    weekdaysFull: ['Nedelja', 'Ponedeljak', 'Utorak', 'Sreda', 'Četvrtak', 'Petak', 'Subota'],
    weekdaysShort: ['Ned', 'Pon', 'Uto', 'Sre', 'Čet', 'Pet', 'Sub'],
    showMonthsShort: undefined,
    showWeekdaysFull: undefined,
    // Buttons
    today: 'Danas',
    clear: 'Obriši',
    close: 'Zatvori',
    // Accessibility labels
    labelMonthNext: 'Sledeći mesec',
    labelMonthPrev: 'Prethodni mesec',
    labelMonthSelect: 'Izaberi mesec',
    labelYearSelect: 'Izaberi godinu',
});*/

// Time picker
/*$('.pickatime').pickatime({
    // options
});

$(document).ready(function() {
    $('.js-example-basic-single').select2();
    });



    */

$('#opened_at_datetime').AnyTime_picker({
    format: '%d/%m/%Y  %H:%i',
});

$('#started_at_datetime').AnyTime_picker({
    format: '%d/%m/%Y  %H:%i',
});

$('#scheduled_at_datetime').AnyTime_picker({
    format: '%d/%m/%Y  %H:%i',
});

$('#completed_at').AnyTime_picker({
    format: '%d/%m/%Y  %H:%i',
});

$('#created_at').AnyTime_picker({
    format: '%d/%m/%Y  %H:%i',
});

$('#anytime-month-numeric').AnyTime_picker({
    format: '%d/%m/%Y'
});

// Basic initialization


var DateTimePickers = function () {
//
// Date range
//

// Options


    var oneDay = 24 * 60 * 60 * 1000;
    var rangeDemoFormat = '%e-%b-%Y';
    var rangeDemoConv = new AnyTime.Converter({format: rangeDemoFormat});

// Clear dates
    $('#rangeDemoClear').on('click', function (e) {
        $('#rangeDemoStart').val('').trigger('change');
    });

// Start date
    $('#rangeDemoStart').AnyTime_picker({
        format: rangeDemoFormat
    });

// On value change
    $('#rangeDemoStart').on('change', function (e) {
        try {
            var fromDay = rangeDemoConv.parse($('#rangeDemoStart').val()).getTime();

            var dayLater = new Date(fromDay + oneDay);
            dayLater.setHours(0, 0, 0, 0);

            var ninetyDaysLater = new Date(fromDay + (90 * oneDay));
            ninetyDaysLater.setHours(23, 59, 59, 999);

            // End date
            $('#rangeDemoFinish')
                .AnyTime_noPicker()
                .removeAttr('disabled')
                .val(rangeDemoConv.format(dayLater))
                .AnyTime_picker({
                    earliest: dayLater,
                    format: rangeDemoFormat,
                    latest: ninetyDaysLater
                });
        }

        catch (e) {

            // Disable End date field
            $('#rangeDemoFinish').val('').attr('disabled', 'disabled');
        }
    });

   /* return {
        init: function () {
            _componentDaterange();
            _componentPickadate();
            _componentPickatime();
            _componentAnytime();
        }
    }*/
}();


// Initialize module
// ------------------------------
/*
document.addEventListener('DOMContentLoaded', function () {
    DateTimePickers.init();
});*/
