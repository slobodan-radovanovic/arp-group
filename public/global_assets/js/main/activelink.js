var link = window.location.pathname;


function changeActive(menu,activelink){
    $( "#"+menu ).addClass( "nav-item-open" );
    $("li#"+menu+" ul").css("display", "block");
    var active = $("a[href='"+activelink+"']");
    active.addClass("active");
}

var menuLinks = new Object();
menuLinks["/orders/create"] = 'orders';
menuLinks["/orders/create_with_status"] = 'orders';
menuLinks["/orders"] = 'orders';
menuLinks["/all_orders"] = 'orders';
menuLinks["/group_search"] = 'orders';
menuLinks["/digitization/create"] = 'digitization';
menuLinks["/digitization"] = 'digitization';
menuLinks["/reclamation/create"] = 'reclamation';

/*menuLinks["\/reclamation\/[0-9]{1,}"] = 'reclamation';*/

menuLinks["/reclamation"] = 'reclamation';
menuLinks["/technicians"] = 'technicians';
menuLinks["/technicians/deleted"] = 'technicians';
menuLinks["/technicians/assigned"] = 'technicians';
menuLinks["/scheduling"] = 'x';
menuLinks["/"] = 'x';
menuLinks["/equipment"] = 'equipment';
menuLinks["/equipment/create"] = 'equipment';
menuLinks["/equipment/dispatch_note"] = 'equipment';
menuLinks["/equipment/assignment"] = 'equipment';
menuLinks["/equipment/assigned"] = 'equipment';
menuLinks["/equipment/discharge"] = 'equipment';
menuLinks["/equipment/discharged_equipment"] = 'equipment';
menuLinks["/equipment/dismantled"] = 'equipment';
menuLinks["/equipment/installed_equipment"] = 'equipment';
menuLinks["/equipment/deleted_equipment"] = 'equipment';
menuLinks["/equipment/sbb_warehouse"] = 'equipment';
menuLinks["/equipment/add"] = 'equipment';
menuLinks["/equipment/edit"] = 'equipment';
menuLinks["/wares"] = 'wares';
menuLinks["/wares/create"] = 'wares';
menuLinks["/warehouse"] = 'warehouse';
menuLinks["/warehouse/external"] = 'warehouse';
menuLinks["/warehouse/add"] = 'warehouse';
menuLinks["/warehouse/assignment"] = 'warehouse';
menuLinks["/warehouse/dispatch_note"] = 'warehouse';
menuLinks["/financereport"] = 'report';
menuLinks["/techniciansreport"] = 'report';
menuLinks["/techniciansreport_all"] = 'report';
menuLinks["/sbbreport"] = 'report';
menuLinks["/sbbreport_in_progress"] = 'report';
menuLinks["/sbbreport_canceled"] = 'report';
menuLinks["/sbbreport_completed"] = 'report';
menuLinks["/waresreport"] = 'report';

menuLinks["/register"] = 'users';
menuLinks["/users"] = 'users';
menuLinks["/service"] = 'service';
menuLinks["/townships"] = 'add_select';
menuLinks["/active_townships"] = 'add_select';
menuLinks["/users/deleted"] = 'users';


for(var activelink in menuLinks)
{
    if (link == activelink){
        changeActive(menuLinks[activelink],activelink);
    }
}


if(link.match(/\/reclamation\/[0-9]{1,}/)){

        $( "#reclamation").addClass( "nav-item-open" );
        $("li#reclamation  ul").css("display", "block");
        var active = $("a[href='/reclamation']");
        active.addClass("active");

}

if(link.match(/\/orders\/[0-9]{1,}/)){

    $( "#orders").addClass( "nav-item-open" );
    $("li#orders  ul").css("display", "block");
    var active = $("a[href='/orders']");
    active.addClass("active");

}

if(link.match(/\/digitization\/[0-9]{1,}/)){

    $( "#digitization").addClass( "nav-item-open" );
    $("li#digitization  ul").css("display", "block");
    var active = $("a[href='/digitization']");
    active.addClass("active");

}

if(link.match(/\/technicians\/[0-9]{1,}/)){

    $( "#technicians").addClass( "nav-item-open" );
    $("li#technicians  ul").css("display", "block");
    var active = $("a[href='/technicians/assigned']");
    active.addClass("active");
}
